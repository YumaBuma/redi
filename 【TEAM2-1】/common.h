#pragma once

#include <d3d11.h>
#include "SimpleMath.h"
#include <cmath>
#include <memory>
#include <vector>
#include <algorithm>
#include <DirectXMath.h>
#include "src\imgui.h"
#include "FunctionDxLib.h"


//If you use SimpleMath, you need wright the next sentence on the first of the source code you want to use SimpleMath.  
using namespace DirectX::SimpleMath;



#define PI	(3.141592f)
#define Rad90	(PI/2)
//--------------------------------
//	定数
//--------------------------------
enum Enemy_ID
{
	E_N_SLIME,
	E_N_SKELETON,//knight
	E_B_SKELETON,//bowman
	E_BOSS,
	E_END
};

struct Imgui_Windows_Flg
{
	bool pl			= false;
	bool bg			= false;
	bool objBG		= false;
	bool cam		= false;
	bool efc		= false;
	bool anmefc		= false;
	bool enemy 		= false;
	bool minimap	= false;
	bool postfx		= false;
	bool e_info[Enemy_ID::E_END];
	bool obstacle 	= false;
};
extern Imgui_Windows_Flg Iwin_flg;
extern ImVec4 clear_col;

extern bool show_debug;
extern bool bossProd;
extern int font_c;
extern Vector2 camera_pos;
extern Vector2 tmp_camera_pos;
extern float camera_move_rate;
extern float camera_max_speed;
extern bool gameexit;
extern int framerate;

extern float screen_ext_rate;

double ToRadian(double deg);

//--------------------------------
//	マクロ関数
//--------------------------------
#define _SCREEN_WIDTH	(1920)
#define _SCREEN_HEIGHT	(1080)
#define SCREEN_CX (960)
#define SCREEN_CY (540)

#define TEMP_SCREEN_WIDTH	(640)
#define TEMP_SCREEN_HEIGHT	(360)
#define TEMP_SCREEN_CX (320)
#define TEMP_SCREEN_CY (180)
#define TEMP_SCREEN_EXT_RATE_MAX (3.f)
#define ENABLE_SMALL_DRAW 1 // 縮小表示への対応

/* ==========  角度変換用  ========== */
//#define ToRadian(x) XMConvertToRadians(x)
//#define ToDegree(x) XMConvertToDegrees(x)


//-------------------------------------------------
//
//-------------------------------------------------

inline float ToRadians(float fDegrees)
{ return fDegrees * (PI / 180.0f); }

inline float ToDegrees(float fRadians)
{ return fRadians * (180.0f / PI); }

inline float clamp(float& v, const float& lo, const float& hi)
{
	assert(hi >= lo);

	v = (std::max)((std::min)(v, hi), lo);

	return v;
}

inline float wrap(const float v, const float lo, const float hi)
{
	assert(hi > lo);
	const float n = std::fmod(v - lo, hi - lo);
	return n >= 0 ? (n + lo) : (n + hi);
}

namespace fileProcessing
{
	//大きさのわからないファイルの読み込み用(char*に格納)
	char* readTextFile(const char* filename);
}

inline DirectX::SimpleMath::Color GetColor(const int r, const int g, const int b)
{
	DirectX::SimpleMath::Color result;
	DirectX::XMStoreFloat4(&result, DirectX::XMVectorScale(DirectX::XMVectorSet(r, g, b, 255.f), 1.f / 255.f));
	return result;
}

inline DirectX::SimpleMath::Color GetColor(const int r, const int g, const int b, const int a)
{
	DirectX::SimpleMath::Color result;
	DirectX::XMStoreFloat4(&result, DirectX::XMVectorScale(DirectX::XMVectorSet(r, g, b, a), 1.f / 255.f));
	return result;
}
