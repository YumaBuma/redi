#pragma once
#include "scene.h"

class Title : public BaseScene
{
private:
	int temp_screen;
	int		bg_col[4];

public:
	Title();
	~Title();
	void Init();
	void Update();
	void Draw();
	void ImGui();

public:
	int fade_alpha;
	int fogTimer;

	bool scene_change;
	sceneManager::SCENE next_scene;
};