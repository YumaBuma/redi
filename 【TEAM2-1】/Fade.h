#pragma once
#include "EaseMove.h"

class Fade
{
private:
	EaseMover					timer;
	DirectX::SimpleMath::Color	color;
private:
	Fade() {}
	Fade(Fade& copy) {}

public:
	void Init();

	const bool& Update();
	void In	(const float& _time = 60.f, const DirectX::SimpleMath::Color& _color = GetColor(0, 0, 0));// だんだん明るく
	void Out(const float& _time = 60.f, const DirectX::SimpleMath::Color& _color = GetColor(0, 0, 0));// だんだん暗く
	void Draw();
	const bool& IsUpdateCompleted() { return timer.IsUpdateCompleted(); }
public:

	static Fade* getInstance()
	{
		static Fade instance;
		return &instance;
	}
};

#define pFade (Fade::getInstance())