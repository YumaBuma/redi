#include "Object.h"


//unsigned int max_num = 0;

Object::Object()
{
	//max_num++;
	//id = max_num;
	_PASS_DATA = false;
}

void Object::PosUpdateX()
{
	old_pos.x = pos.x;
	pos.x += speed.x;
	delta.x = old_pos.x - pos.x;
}

void Object::PosUpdateY()
{
	old_pos.y = pos.y;
	pos.y += speed.y;
	delta.y = old_pos.y - pos.y;
}

void Object::ObjHoseiDown(shared_ptr<Object> &obj)
{
	obj->pos.y = pos.y - size.y;
	obj->speed.y = 0.f;
}
void Object::ObjHoseiLeft(shared_ptr<Object> &obj, float size)
{
	obj->pos.x = pos.x - size;
	obj->speed.y = 0.f;
}
void Object::ObjHoseiRight(shared_ptr<Object> &obj, float size)
{
	obj->pos.x = pos.x + size;
	obj->speed.y = 0.f;
}

void Object::knockBack()
{
	noHitTime--;

	if (noHitTime < 0)
	{
		noHitTime = 0;
	}
	//点滅処理
	//if (!step)
	//{
	//	if (noHitTime <= 0)
	//	{
	//		noHitTime = 0;
	//		alpha = 255;
	//	}
	//	else
	//	{
	//		alpha = 255;
	//		if (noHitTime / 8 % 2 == 0)
	//		{
	//			alpha = 50;
	//		}
	//	}
	//}

	//ダメージ時ノックバック
	if (noHitTime > NO_MOVE)
	{
		if (damage_direction)
		{
			speed.y = -Knck_SPD.y;
			speed.x = Knck_SPD.x;
		}
		else
		{
			speed.y = -Knck_SPD.y;
			speed.x = -Knck_SPD.x;
		}
	}
	else if (noHitTime > NO_DAMAGE)
	{

		if (damage_direction)
		{
			speed.x = Knck_SPD.x;
		}
		else
		{
			speed.x = -Knck_SPD.x;
		}
	}
	//else if (noHitTime > 85)
	//{

	//}
	else
	{
		isDamage = false;
	}
}
