#pragma once
#include "Animation.h"

class Drawable : public Animation
{
public:
	bool m_isVisible;
	int layer;
	bool isErase;
public:
	Drawable() : m_isVisible(true) {};
	virtual ~Drawable() = default;

public:
	virtual void Draw() const  = 0;

	void show()
	{
		m_isVisible = true;
	}

	void hide()
	{
		m_isVisible = false;
	}

	bool isVisible() const
	{
		return m_isVisible;
	}
};
