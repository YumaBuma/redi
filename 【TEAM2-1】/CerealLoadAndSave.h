#pragma once
#include <fstream>

#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/types/polymorphic.hpp>

enum eArchiveTypes : int { BINARY, JSON, INI, XML, ARTYPE_END };

//class CerealLoadAndSave
//{
//public:
//	CerealLoadAndSave() {}
//	virtual ~CerealLoadAndSave() {}
//
//	enum eArchiveTypes : int { BINARY, JSON, INI, XML, ARTYPE_END };
//
//	// 各形式からデータを読み込みます 
//	// 例: LoadDataFromFile(".\\SomeDir\\file.json", eArchiveTypes::JSON);
//	virtual void LoadDataFromFile(const std::string& _path, eArchiveTypes _file_type)
//	{
//		std::ifstream
//			ifs;
//			ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
//		if(!ifs)return; // 読み込み失敗
//
//		switch (_file_type)
//		{
//		case CerealLoadAndSave::BINARY:
//		{
//			cereal::BinaryInputArchive
//				i_archive(ifs);
//				i_archive(cereal::make_nvp(_path, *this));
//			break;
//		}
//		case CerealLoadAndSave::JSON:
//		case CerealLoadAndSave::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
//		{
//			cereal::JSONInputArchive
//				i_archive(ifs);
//				i_archive(cereal::make_nvp(_path, *this));
//			break;
//		}
//		case CerealLoadAndSave::XML:
//		{
//			cereal::XMLInputArchive
//				i_archive(ifs);
//				i_archive(cereal::make_nvp(_path, *this));
//			break;
//		}
//		default:
//			break;
//		}
//	}
//
//	// 各形式でデータを保存します
//	// 同じファイル名が存在した場合は上書き保存されます
//	// 例: SaveDataToFile(".\\SomeDir\\file.json", eArchiveTypes::JSON);
//	virtual void SaveDataToFile(const std::string& _path, eArchiveTypes _file_type)
//	{
//		std::ofstream
//			ofs;
//			ofs.open(_path, std::ios::binary);
//		if(!ofs)return; // 読み込み失敗
//
//		switch (_file_type)
//		{
//		case CerealLoadAndSave::BINARY:
//		{
//			cereal::BinaryOutputArchive
//				o_archive(ofs);
//				o_archive(cereal::make_nvp(_path, *this));
//			break;
//		}
//		case CerealLoadAndSave::JSON:
//		case CerealLoadAndSave::INI:
//		{
//			cereal::JSONOutputArchive
//				o_archive(ofs);
//				o_archive(cereal::make_nvp(_path, *this));
//			break;
//		}
//		case CerealLoadAndSave::XML:
//		{
//			cereal::XMLOutputArchive
//				o_archive(ofs);
//				o_archive(cereal::make_nvp(_path, *this));
//			break;
//		}
//		default:
//			break;
//		}
//	}
//};
