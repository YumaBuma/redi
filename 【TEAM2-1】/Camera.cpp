#include "EaseMove.h"
#include "Function.h"
#include "Camera.h"
#include "Input.h"
#include "FunctionDxLib.h"
#include "FunctionImGui.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <d3d11.h>
#include "SimpleMath.h"
using namespace DirectX::SimpleMath;

#include "./src/imgui.h"

#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>

const static int IS_LOAD_CAM_DATA = 1;
const static int IS_LOAD_SHAKES_DATA = 1;

Camera::Camera(const float& _screen_w, const float& _screen_h)
{
	Init(0, _screen_w, _screen_h);
}

inline Camera::~Camera() 
{
	p_follow_pos_x = nullptr; p_follow_pos_y = nullptr;
	p_lock_on_pos_x = nullptr; p_lock_on_pos_y = nullptr;
}

void Camera::Init(const int& _stage_num, const float& _screen_w, const float& _screen_h)
{
	camera				= { 0.f,0.f };
	camera_prev			= { 0.f,0.f };
	camera_delta		= { 0.f,0.f };
	camera_delta_prev	= { 0.f,0.f };
	target_pos_prev			= { 0.f,0.f };
	target_pos_delta		= { 0.f,0.f };
	target_pos_delta_prev	= { 0.f,0.f };
	angle_deg = DelayMover();

	screen_zoom	= EaseMover();
	screen_zoom.SetMoveSpeed(0.1f);
	screen_zoom_pos	= DelayMoverVec2();
	screen_zoom_pos.SetMoveSpeed(0.1f);


	chip_extrate				= EaseMover();
	chip_extrate.SetMoveSpeed(0.2f);
	chip_extrate.SetDifferenceMargin(0.00125f / 4.f);
	chip_extrate.val			= 1.f;
	chip_extrate.target_val		= 1.f;
	is_settable_chip_ext_pivot	= false;
				chip_ext_pivot	= { 0.f,0.f };

	delay_move_speed_additional_coefficient = 0.f;

	target_move_margin_pos 						= DelayMoverVec2();
	target_move_margin_pos.difference_margin	= FLT_EPSILON * 8.f;
	target_move_margin_pos.SetMoveSpeed(0.05f);
	target_move_margin_maxmin 					= { 0.f,0.f };
	target_move_margin_speed_coefficient 		= 0.05f;
	target_move_margin_speed_coefficient_tmp	= 0.05f;

	area_max_pos = { 640000.f,640000.f };

	p_follow_pos_x = nullptr;
	p_follow_pos_y = nullptr;

	screen_w 		= _screen_w;
	screen_w_center = _screen_w*0.5f;
	screen_h 		= _screen_h;
	screen_h_center = _screen_h*0.5f;

	scroll_margin = EaseMoverVec2();
	scroll_margin.SwitchIsUseEasing();
	scroll_margin.SetEasingFunction(Easing::CIRCINOUT);
	scroll_margin.y			= -100.f;
	scroll_margin.target_y	= -100.f;

	lock_on_margin 		= DelayMoverVec2();
	is_use_lock_on 		= false;
	is_lock_on_trigger 	= false;
	p_lock_on_pos_x 	= nullptr;
	p_lock_on_pos_y 	= nullptr;

	r_stick_move		= DelayMoverVec2();
	r_stick_move_maxmin = { 160.f, 160.f };	// +-
	r_stick_move_test	= { 0.f, 0.f };	// +-
	r_stick_move.SetMoveSpeed(0.7f);
	is_lock_r_stick_move = false;

	counter = 0.f;

	shake_pos 				= DelayMoverVec2();
	shake_pos.SetMoveSpeed(0.2f);
	shake.time 				= EaseMover();
	shake.time.val 			= 1.f;
	shake.time.target_val 	= 1.f;
	shake.time.SetIsUseEasing(true);
	shake.speed 			= 0.f;
	shake.power 			= 0.f;
	shake.octaves 			= 1.f;
	shake.amp_mult 			= 0.5f;
	shake.update_interval 	= 1.f;

	swing_pos 			= DelayMoverVec2();
	swing.time 			= EaseMover();
	swing.time.SetIsUseEasing(true);
	swing.speed 		= 0.f;
	swing.power 		= 0.f;
	swing.octaves 		= 0;
	swing.persistence 	= 1.f;

	is_always_swing 			= false;
	always_swing_type			= eAlSwing::AlSwing_End;
	always_swing.speed 			= 0.8f;
	always_swing.power 			= EaseMover();
	always_swing.power.SetIsUseEasing(true);
	always_swing.octaves 		= 0;
	always_swing.persistence 	= DelayMover();

	is_on_fixed_cam_pos					= false;
	is_settable_fixed_pos				= false;
	is_update_fixed_camera				= true;
	fixed_camera_pos					= { 0,0 };
	delay_move_speed_tmp				= delay_move_speed;
	fixed_camera.fixed_pos				= { 0,0 };
	fixed_camera.fixed_rect_size		= { 0,0 };
	fixed_camera.is_use_ease_fade_in	= true;
	fixed_camera.is_use_ease_fade_out	= false;
	fixed_camera.fade_in_ease_algo		= Easing::CUBICINOUT;
	fixed_camera.fade_out_ease_algo		= Easing::CUBICINOUT;
	fixed_camera.fade_in_time			= 90.f;
	fixed_camera.fade_out_time			= 90.f;
	fixed_camera.fade_out_delay_move_speed_subtract_val = 0.f;

	is_follow_to_mouse		= false;
	mouse_pos 				= { 0,0 };
	mouse_pos_old 			= { 0,0 };
	mouse_scroll_threshold 	= { 7,7 };

	noise = PerlinNoise(GetRand(RAND_MAX));


	if (IS_LOAD_CAM_DATA && _stage_num != 0)
		LoadDataFromFile(".\\Data\\Configurations\\Camera\\camera_" + std::to_string(_stage_num) + ".bin", eArchiveTypes::BINARY);
	if (IS_LOAD_SHAKES_DATA)
		LoadAll(_stage_num);

	always_swing = always_swing_param_data[0];

	r_stick_move.SetMoveSpeed(0.07f);
}


//--------------------------------------------------------------
//
//	TargetPos
//
//--------------------------------------------------------------
void Camera::SetTargetPosPtr(const float* _x, const float* _y)
{
	if (_x == nullptr || _y == nullptr)return;
	p_follow_pos_x = _x;
	p_follow_pos_y = _y;
}

void Camera::SwitchIsUsePtr(int _state)
{
	if (_state == -1)
		is_use_ptr = !is_use_ptr;
	else
		is_use_ptr = (_state != 0);
}

//--------------------------------------------------------------
//
//	ScrollMargin
//
//--------------------------------------------------------------
void Camera::SetScrollMergin(const float & _x, const float & _y, const float& _time)
{
	scroll_margin.StartEasing(_x, _y, 1.0f / _time);
}

//--------------------------------------------------------------
//
//	LockOn
//
//--------------------------------------------------------------
void Camera::SwitchIsUseLockOnTragetPosPtr(int _state)
{
	if (_state == -1)
		is_use_lock_on = !is_use_lock_on;
	else
		is_use_lock_on = (_state != 0);
}

void Camera::SetLockOnTragetPosPtr(const float* _x, const float* _y)
{
	if (_x == nullptr || _y == nullptr)return;
	p_lock_on_pos_x = _x;
	p_lock_on_pos_y = _y;
}

void Camera::LockOn()
{
	is_lock_on_trigger = true;
}

void Camera::LockOn(const float* _x, const float* _y)
{
	if (_x == nullptr || _y == nullptr)return;
	is_lock_on_trigger = true;
	p_lock_on_pos_x = _x;
	p_lock_on_pos_y = _y;
}

void Camera::LockOn(const DirectX::SimpleMath::Vector2* _pos_ptr)
{
	if (_pos_ptr == nullptr)return;
	is_lock_on_trigger = true;
	p_lock_on_pos_x = &_pos_ptr->x;
	p_lock_on_pos_y = &_pos_ptr->y;
}

//--------------------------------------------------------------
//
//	SetAreaMaxPos
//
//--------------------------------------------------------------
void Camera::SetAreaMaxPos(const float& _x, const float& _y)
{
	area_max_pos.x = _x;
	area_max_pos.y = _y;
}

//--------------------------------------------------------------
//
//	Update
//
//--------------------------------------------------------------
bool Camera::Update()
{
	counter += 1.f;
	camera_prev			= camera;
	camera_delta_prev	= camera_delta;
	target_pos_prev			= GetTargetPosAtVector2();
	target_pos_delta_prev	= target_pos_delta;

	if (p_follow_pos_x == nullptr || p_follow_pos_y == nullptr)
	{
		is_use_ptr = false;
	}
	if (p_lock_on_pos_x == nullptr || p_lock_on_pos_y == nullptr)
	{
		is_use_lock_on = false;
		is_lock_on_trigger = false;
	}

	if (is_follow_to_mouse)
	{
		FollowMouse();
	}
	else
	{
		//メイン追従位置のポインタ
		if (is_use_ptr)
		{
			target_x = *p_follow_pos_x;
			target_y = *p_follow_pos_y;
		}
		target_pos_delta = GetTargetPosAtVector2() - target_pos_prev;

		//ロックオン位置のポインタ
		if (is_use_lock_on || is_lock_on_trigger)
		{
			Vector2 length = Vector2(*p_lock_on_pos_x, *p_lock_on_pos_y) - GetPosAtVector2();
			lock_on_margin.SetTargetPosAtVector2(length);
		}
		else
		{
			lock_on_margin.target_x = 0.f;
			lock_on_margin.target_y = 0.f;
		}

	}

	//揺れ系
	{
		if (scasi(shake.update_interval) != 0 && scasi(counter) % scasi(shake.update_interval) == 0)
		{
			float wigglex_tmp = WiggleX(shake.speed, shake.power, counter, shake.octaves, shake.amp_mult)*(1.f - shake.time.val);
			float wiggley_tmp = WiggleY(shake.speed, shake.power, counter, shake.octaves, shake.amp_mult)*(1.f - shake.time.val);
			shake_pos.target_x = wigglex_tmp;
			shake_pos.target_y = wiggley_tmp;
		}

		/*
			noise.OctavePerlin
			(
				x, y, z,
				swing_octaves,	 //ノイズ反復回数
				swing_persistence//反復ごとの倍率
			)
		*/
		//swing
		{
			swing_pos.target_x =
				swing.power *
				(
					-0.5f +// OctavePerlinが0~1を返すので
					noise.OctavePerlin
					(
						0.f, 0.f, cosf(GetRadian(counter * swing.speed)),
						1 + swing.octaves,
						swing.persistence
					)
				);
			swing_pos.target_y =
				swing.power *
				(
					-0.5f +
					noise.OctavePerlin
					(
						0.f, 0.f, sinf(GetRadian(counter * swing.speed)),
						1 + swing.octaves,
						swing.persistence
					)
				);
			swing_pos.target_x *= (1.f - swing.time.val);
			swing_pos.target_y *= (1.f - swing.time.val);
		}

		//always_swing
		{
			swing_pos.target_x +=
				always_swing.power.val *
				(
					-0.5f +// OctavePerlinが0~1を返すので
					noise.OctavePerlin
					(
						cosf(GetRadian(counter*always_swing.speed)), 0.f, GetRadian(counter*always_swing.speed),
						1 + always_swing.octaves,
						always_swing.persistence.val
					)
				);
			swing_pos.target_y +=
				always_swing.power.val *
				(
					-0.5f +
					noise.OctavePerlin
					(
						0.f, sinf(GetRadian(counter*always_swing.speed)), GetRadian(counter*always_swing.speed),
						1 + always_swing.octaves,
						always_swing.persistence.val
					)
				);
		}
	}

	//target_move_margin_pos
	{
		// 向きが変わった場合
		//if (key[KEY_INPUT_LEFT] == 1)
		{
			// x
			//if (target_pos_delta.x < target_pos_delta_prev.x)
			if (key[KEY_INPUT_LEFT] == 1)
			{
				if (target_move_margin_pos.IsUpdateCompleted())
				{
					target_move_margin_speed_coefficient_tmp = target_move_margin_speed_coefficient;
				}
					target_move_margin_speed_coefficient = 0.f;
				//else
				//	target_move_margin_speed_coefficient = target_move_margin_speed_coefficient * 0.4f;

				target_move_margin_pos.target_x = -target_move_margin_maxmin.x;
			}
			else
				//if (target_pos_delta.x > target_pos_delta_prev.x)
				if (key[KEY_INPUT_RIGHT] == 1)
				{
					if (target_move_margin_pos.IsUpdateCompleted())
					{
						target_move_margin_speed_coefficient_tmp = target_move_margin_speed_coefficient;
					}
						target_move_margin_speed_coefficient = 0.f;
					//else
					//	target_move_margin_speed_coefficient = target_move_margin_speed_coefficient * 0.4f;

					target_move_margin_pos.target_x = target_move_margin_maxmin.x;
				}
			//// y
			//if (target_pos_delta.y < target_pos_delta_prev.y)
			//{
			//	if (target_move_margin_pos.IsUpdateCompleted())
			//		target_move_margin_speed_coefficient_tmp = target_move_margin_speed_coefficient;
			//	target_move_margin_speed_coefficient = 0.f;
			//	target_move_margin_pos.target_y = target_move_margin_maxmin.y;
			//}
			//else if (target_pos_delta.y > target_pos_delta_prev.y)
			//{
			//	if (target_move_margin_pos.IsUpdateCompleted())
			//		target_move_margin_speed_coefficient_tmp = target_move_margin_speed_coefficient;
			//	target_move_margin_speed_coefficient = 0.f;
			//	target_move_margin_pos.target_y = -target_move_margin_maxmin.y;
			//}
		}

		if (target_move_margin_speed_coefficient < target_move_margin_speed_coefficient_tmp)
			target_move_margin_speed_coefficient += target_move_margin_speed_coefficient_tmp * 0.02f;
		else 
			if (target_move_margin_speed_coefficient > target_move_margin_speed_coefficient_tmp)
				target_move_margin_speed_coefficient = target_move_margin_speed_coefficient_tmp;

		target_move_margin_pos.SetMoveSpeed(target_move_margin_speed_coefficient);
	}

	// rスティック
	if (is_lock_r_stick_move == true)
	{
		r_stick_move.target_x = 0.f;
		r_stick_move.target_y = 0.f;
	}
	else
	{
		Vector2 r = { scasf(GetXInput().ThumbRX), scasf(GetXInput().ThumbRY) };
				//r = r_stick_move_test;

		FitTo(r.x, 32767.f, r_stick_move_maxmin.x, -r_stick_move_maxmin.x, -32768.f);
		FitTo(r.y, 32767.f, r_stick_move_maxmin.y, -r_stick_move_maxmin.y, -32768.f);
		r_stick_move.target_x = floorf(r.x);
		r_stick_move.target_y = -floorf(r.y);
	}

	UpdateFixedCamera();

	//is_updated = !this->EaseMoverVec2::Update();
	//ロックオン範囲の制限(p_lock_on_pos と メインの追従位置が画面内に収まるように)
	{
		float dist		= GetDistanceAtoB(GetPosAtVector2(), GetTargetPosAtVector2());
		float move_speed;

		if (is_follow_to_mouse)
			move_speed	= this->delay_move_speed;
		else
		{
			move_speed	= this->delay_move_speed;
			move_speed += (dist * dist) * delay_move_speed_additional_coefficient;
			move_speed *= Fit(delay_move_speed, delay_move_speed_tmp, 1.f, 0.f);
		}
		
		DelayMove(x, target_x, move_speed);
		DelayMove(y, target_y, move_speed);
	}

	//拡大位置制限
	{
		if (screen_zoom.target_val < 0.f)screen_zoom.target_val = 0.0f;
		float clamp_x = scasf(TEMP_SCREEN_CX)*screen_zoom.target_val;
		float clamp_y = scasf(TEMP_SCREEN_CY)*screen_zoom.target_val;
		clamp(screen_zoom_pos.target_x, -clamp_x, clamp_x);
		clamp(screen_zoom_pos.target_y, -clamp_y, clamp_y);
	}

	bool is_updated;
	is_updated = !shake_pos.Update();
	is_updated = !shake.time.Update();
	is_updated = !swing_pos.Update();
	is_updated = !swing.time.Update();
	is_updated = !always_swing.power.Update();
	is_updated = !always_swing.persistence.Update();
	is_updated = !scroll_margin.Update();
	is_updated = !lock_on_margin.Update();
	is_updated = !target_move_margin_pos.Update();
	is_updated = !angle_deg.Update();
	is_updated = !screen_zoom.Update();
	is_updated = !screen_zoom_pos.Update();
	is_updated = !chip_extrate.Update();
	is_updated = !r_stick_move.Update();
	is_updated = !fixed_camera_pos.Update();

	//演出用の位置やプレイヤーの位置などをまとめる
	if (is_on_fixed_cam_pos)
	{
		camera.x = fixed_camera_pos.x + shake_pos.x + swing_pos.x;
		camera.y = fixed_camera_pos.y + shake_pos.y + swing_pos.y;
	}												  
	else
	{
		camera.x = x + scroll_margin.x + lock_on_margin.x - screen_w_center + shake_pos.x + swing_pos.x + r_stick_move.x;
		camera.y = y + scroll_margin.y + lock_on_margin.y - screen_h_center + shake_pos.y + swing_pos.y + r_stick_move.y;
		if (is_follow_to_mouse == false)
		{
			camera.x += target_move_margin_pos.x;
			camera.y += target_move_margin_pos.y;
		}
	}


	//範囲制限
	// boss_area_min_x = 1024
	// boss_area_max_x = 2576
	if (area_max_pos.x >= 0.f)//area_max_posが負数の場合処理を行わない
	{
		if (target_x < screen_w_center	- scroll_margin.x	+ area_min_pos.x)	target_x = screen_w_center	- scroll_margin.x + area_min_pos.x;
		if (target_y < screen_h_center	- scroll_margin.y	+ area_min_pos.y)	target_y = screen_h_center	- scroll_margin.y + area_min_pos.y;
		if (target_x > screen_w			+ scroll_margin.x	+ area_max_pos.x)	target_x = screen_w			+ scroll_margin.x + area_max_pos.x;
		if (target_y > screen_h			+ scroll_margin.y	+ area_max_pos.y)	target_y = screen_h			+ scroll_margin.y + area_max_pos.y;

		clamp(camera.x, area_min_pos.x, area_max_pos.x - screen_w);
		clamp(camera.y, area_min_pos.y, area_max_pos.y - screen_h);
	}

	camera_delta = camera - camera_prev;

	is_lock_on_trigger = false;
	return true;
}

void Camera::UpdateFixedCamera()
{
	if (is_settable_fixed_pos)
	{
		if (pMOUSE->left_ref().pressing_count == 1)
		{
			fixed_camera.fixed_pos = GetWorldPosFromCursorPos();
		}
		if (pMOUSE->left_ref().is_release)
		{
			fixed_camera.fixed_rect_size = GetWorldPosFromCursorPos();
			is_settable_fixed_pos = false;
		}
	}

	if (is_update_fixed_camera == false) return;

	if (p_follow_pos_x)
	{
		int counter = 0;
		for (auto it_fixed_camera : fixed_camera_data)
		{
			// 固定カメラの範囲内にいる場合
			if (Coll::AABB(	Vector2(*p_follow_pos_x - 16.f, *p_follow_pos_y - 64.f)	, Vector2(*p_follow_pos_x + 16.f, *p_follow_pos_y)	,
							it_fixed_camera.fixed_pos								, it_fixed_camera.fixed_rect_size					)
			)
			{
				// 固定位置への遷移
				if (is_on_fixed_cam_pos == false)
				{
					fixed_camera = it_fixed_camera;
					if (fixed_camera.fade_out_delay_move_speed_subtract_val > delay_move_speed)
						fixed_camera.fade_out_delay_move_speed_subtract_val = delay_move_speed;

					fixed_camera_pos.SetPosAtVector2(camera);
					if (fixed_camera.is_use_ease_fade_in)
					{
						fixed_camera_pos.SetIsUseEasing(fixed_camera.is_use_ease_fade_in);
						fixed_camera_pos.SetEasingFunction(fixed_camera.fade_in_ease_algo);
						fixed_camera_pos.StartEasingAtVector2(fixed_camera.fixed_pos, 1.f / fixed_camera.fade_in_time);
					}
					else
						fixed_camera_pos = fixed_camera.fixed_pos;
				}
				is_on_fixed_cam_pos = true;
				break;
			}
			else
			{
				// 追従カメラに戻す
				if (is_on_fixed_cam_pos && counter >= fixed_camera_data.size() - 1)// どの固定位置にも入っていなかった場合
				{
					x = fixed_camera_pos.x - scroll_margin.x - lock_on_margin.x + screen_w_center - shake_pos.x - swing_pos.x - r_stick_move.x - target_move_margin_pos.x;
					y = fixed_camera_pos.y - scroll_margin.y - lock_on_margin.y + screen_h_center - shake_pos.y - swing_pos.y - r_stick_move.y - target_move_margin_pos.y;
					if(fixed_camera_pos.GetEaseTimeNow() == 1.0f)// イージングが完了していた場合
						delay_move_speed_tmp = delay_move_speed;
					
					is_on_fixed_cam_pos = false;
				}
			}

			counter++;
		}
	}

	if (is_on_fixed_cam_pos)
	{
		//if (Coll::AABB(	Vector2(*p_follow_pos_x - 16.f, *p_follow_pos_y - 64.f)	, Vector2(*p_follow_pos_x + 16.f, *p_follow_pos_y	),
		//				fixed_camera.fixed_pos									, fixed_camera.fixed_rect_size						)
		//)
		//{

		//}
	}
	else
	{
		if (fixed_camera.fade_out_delay_move_speed_subtract_val > 0.f)
		{
			delay_move_speed = delay_move_speed_tmp - fixed_camera.fade_out_delay_move_speed_subtract_val;
			fixed_camera.fade_out_delay_move_speed_subtract_val -= delay_move_speed_tmp * 0.014f;
		}
		else
		{
			if (fixed_camera.fade_out_delay_move_speed_subtract_val <= 0.f)
				delay_move_speed = delay_move_speed_tmp;
			fixed_camera.fade_out_delay_move_speed_subtract_val = -1.f;
		}
	}
}

void Camera::Draw()
{
	Vector2 pos_tmp;
			pos_tmp = GetCursorPosRelatedZoom();

	//draw margin area
	if (is_follow_to_mouse)
	{
		if (pMOUSE->middle_ref().pressing_count == 0)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 42);
			{
				//x
				DrawBox(0, 0, mouse_scroll_threshold.x, (int)screen_h, GetColor(16, 16, 16), 1);
				DrawBox((int)(screen_w - (mouse_scroll_threshold.x)), 0, (int)screen_w, (int)screen_h, GetColor(16, 16, 16), 1);
				//y
				DrawBox(mouse_scroll_threshold.x, 0											, ((int)screen_w - mouse_scroll_threshold.x), mouse_scroll_threshold.y	, GetColor(16, 16, 16), 1);
				DrawBox(mouse_scroll_threshold.x, (int)(screen_h - mouse_scroll_threshold.y), ((int)screen_w - mouse_scroll_threshold.x), (int)screen_h				, GetColor(16, 16, 16), 1);
			}
		}

		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 64);
		{
			// cursor pos
			//DrawBox(scasi(pos_tmp.x), scasi(pos_tmp.y), scasi(pos_tmp.x) + 4, scasi(pos_tmp.y) + 4, GetColor(200, 160, 120), true);
			//DrawBox
			//(
			//	scasi(pos_tmp.x), scasi(pos_tmp.y),
			//	scasi(pos_tmp.x) + 8.f, scasi(pos_tmp.y) + 8.f,
			//	GetColor(200, 160, 120), 1
			//);

			// chip_ext_pivot
			{
				if (is_settable_chip_ext_pivot)
					DrawCircle(scasi(pos_tmp.x), scasi(pos_tmp.y), 5, GetColor(242, 225, 120), false, 2);

				Vector2 view;
				view.x  = chip_ext_pivot.x;
				view.y  = chip_ext_pivot.y;
				view.x *= chip_extrate.val;
				view.y *= chip_extrate.val;
				DrawCircle(xi(view.x), yi(view.y), 5, GetColor(242, 225, 120), false, 2);
			}
		}
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
	}

	if (is_settable_fixed_pos)
	{
		if (ImGui::IsMouseHoveringAnyWindow() == false)
		{
			// ドラッグ中選択範囲を表示
			if (pMOUSE->left_ref().pressing_count)
			{
				Vector2 min_pos = cam.GetWorldPosFromScreenPos(Vector2((float)pMOUSE->left_ref().clicked_pos_log_x, (float)pMOUSE->left_ref().clicked_pos_log_y));
				Vector2 max_pos = cam.GetWorldPosFromScreenPos(Vector2((float)pMOUSE->pos_x_ref(), (float)pMOUSE->pos_y_ref()));
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 40);
				DrawBox
				(
					cam.xi_ext(min_pos.x),
					cam.yi_ext(min_pos.y),
					cam.xi_ext(max_pos.x),
					cam.yi_ext(max_pos.y),
					GetColor(100, 100, 200), 1
				);
			}
		}
	}

	if (show_debug)
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 20);
		for (auto it_fixcam : fixed_camera_data)
		{
			DrawBoxCam(it_fixcam.fixed_pos, it_fixcam.fixed_rect_size, GetColor(224, 180, 180), 0);
			DrawCircleCam(it_fixcam.fixed_pos, 2.f, GetColor(224, 180, 180), 0);
		}
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
	}
}

//--------------------------------------------------------------
//
//	Shake
//
//--------------------------------------------------------------
void Camera::ShakeCamera(const float& _shake_speed, const float& _shake_power, const float& _shake_time, const float& _shake_update_interval, const float& _shake_octaves, const float& _shake_amp_mult)
{
	shake.speed = _shake_speed;
	shake.power = _shake_power;
	shake.update_interval = _shake_update_interval;
	shake.octaves = _shake_octaves;
	shake.amp_mult = _shake_amp_mult;
	if (_shake_time > 0.0f)
	{
		shake.time.val = 0.f;
		shake.time.StartEasing(1.f, 1.0f / _shake_time);
	}
}
void Camera::ShakeCamera(const Camera::eShake& _e_shake)
{
	if (_e_shake >= shake_param_data.size()) return;
	shake = shake_param_data[_e_shake];
	shake_pos.SetMoveSpeed(shake.move_speed);
	shake.time.SetIsUseEasing(true);
	shake.time.val = 0.f;
	shake.time.StartEasing(1.f, shake.time.ease_time_addition_rate);
}

//--------------------------------------------------------------
//
//	Swing
//
//--------------------------------------------------------------
void Camera::SwingCamera(const float& _swing_speed, const float& _swing_power, const int& _swing_octaves, const float& _swing_persistence, const float& _swing_time)
{
	swing.speed = _swing_speed;
	swing.power = _swing_power;
	swing.octaves = _swing_octaves;
	swing.persistence = _swing_persistence;
	if (_swing_time > 0.f)
	{
		swing.time.val = 0.f;
		swing.time.StartEasing(1.f, 1.0f / _swing_time);
	}
}
void Camera::SwingCamera(const Camera::eSwing& _e_swing)
{
	if (_e_swing >= swing_param_data.size()) return;
	swing = swing_param_data[_e_swing];
	swing.time.val = 0.f;
	swing.time.SetIsUseEasing(true);
	swing.time.StartEasing(1.f, swing.time.ease_time_addition_rate);
}

//--------------------------------------------------------------
//
//	AlwaysSwing
//
//--------------------------------------------------------------
void Camera::AlwaysSwingCamera(const float& _always_swing_speed, const float& _always_swing_power, const int& _always_swing_octaves, const float& _always_swing_persistence)
{
	always_swing.speed = _always_swing_speed;
	always_swing.power.StartEasing(_always_swing_power, 1.f / 180.f);
	always_swing.octaves = _always_swing_octaves;
	always_swing.persistence.target_val = _always_swing_persistence;
}
void Camera::AlwaysSwingCamera(const Camera::eAlSwing& _e_alswing)
{
	if (_e_alswing >= always_swing_param_data.size()) return;
	if (always_swing_type == _e_alswing) return;
	always_swing_type = _e_alswing;
	const ALWAYS_SWING& tmp = always_swing_param_data[_e_alswing];
	always_swing.speed = tmp.speed;
	always_swing.power.SetIsUseEasing(true);
	always_swing.power.StartEasing(tmp.power.target_val, 1.f / 180.f);
	always_swing.octaves = tmp.octaves;
	always_swing.persistence.target_val = tmp.persistence.target_val;
}

void Camera::SwitchIsAlwaysSwing(int _state)
{
	if (is_always_swing == (_state >= 1 ? true : false)) return;
	if (always_swing_type >= always_swing_param_data.size() || always_swing_type < 0) return;
	if (_state == -1)
		is_always_swing = !is_always_swing;
	else
		is_always_swing = (_state >= 1 ? true : false);

	always_swing.power.StartEasing(always_swing_param_data[always_swing_type].power.target_val * float(is_always_swing), 1.f / 180.f);
}

void Camera::StartAlwaysSwing()
{
	if (is_always_swing == true) return;
	if (always_swing_type >= always_swing_param_data.size() || always_swing_type < 0) return;
	is_always_swing = true;

	always_swing.power.StartEasing(always_swing_param_data[always_swing_type].power.target_val, 1.f / 180.f);
}

void Camera::StopAlwaysSwing()
{
	if (is_always_swing == false) return;
	is_always_swing = false;

	always_swing.power.StartEasing(0.f, 1.f / 180.f);
}

bool Camera::IsInCamera(const DirectX::SimpleMath::Vector2 & _pos, const DirectX::SimpleMath::Vector2 & _size)
{
	//Vector2 camera_max = { camera.x + screen_w + _size.x, camera.y + screen_h + _size.y };
	//Vector2 pos_max = { _pos.x + _size.x, _pos.y + _size.y };

	//return Coll::AABB(camera, camera_max, _pos, pos_max);
	return Coll::AABBf
	(
		camera.x								, camera.y,
		camera.x + screen_w + _size.x			, camera.y + screen_h + _size.y,
		_pos.x* chip_extrate.val				, _pos.y* chip_extrate.val,
		(_pos.x + _size.x) * chip_extrate.val	, (_pos.y + _size.y) * chip_extrate.val
	);
}

bool Camera::IsInCameraCenter(const DirectX::SimpleMath::Vector2 & _pos, const DirectX::SimpleMath::Vector2 & _size)
{
	Vector2 camera_max  = { camera.x + screen_w + _size.x, camera.y + screen_h + _size.y };
	Vector2 pos_min_tmp = { _pos.x - _size.x, _pos.y - _size.y };
	Vector2 pos_max_tmp = { _pos.x + _size.x, _pos.y + _size.y };

	if (Coll::AABB(camera, camera_max, pos_min_tmp, pos_max_tmp))return true;

	return false;
}

bool Camera::IsInCameraFoot(const DirectX::SimpleMath::Vector2 & _pos, const DirectX::SimpleMath::Vector2 & _size)
{
	//Vector2 camera_min = { camera.x			, camera.y };
	//Vector2 camera_max = { camera.x + screen_w + _size.x, camera.y + screen_h + _size.y };
	//Vector2 pos_min_tmp = { _pos.x - _size.x	, _pos.y - _size.y };
	//Vector2 pos_max_tmp = { _pos.x + _size.x	, _pos.y };

	return Coll::AABBf
	(
		camera.x								, camera.y,
		camera.x + screen_w + _size.x			, camera.y + screen_h + _size.y,
		(_pos.x - _size.x) * chip_extrate.val	, (_pos.y - _size.y) * chip_extrate.val,
		(_pos.x + _size.x) * chip_extrate.val	, _pos.y * chip_extrate.val
	);
}

void Camera::SetScreenZoomSpeed(const float& _delay_move_speed) 
{
	screen_zoom.SetMoveSpeed(_delay_move_speed);
	screen_zoom_pos.SetMoveSpeed(_delay_move_speed);
}


//--------------------------------------------------------------
//
//	For Editor
//
//--------------------------------------------------------------
void Camera::FollowMouse()
{
	mouse_pos_old.x = mouse_pos.x;
	mouse_pos_old.y = mouse_pos.y;
	GetMousePoint(&mouse_pos.x, &mouse_pos.y);

	if (key[KEY_INPUT_Z])
	{
		if (pMOUSE->left_ref().pressing_count)
		{
			screen_zoom.target_val += static_cast<float>(mouse_pos.x - mouse_pos_old.x) *0.01f;

			if (mouse_pos.x - mouse_pos_old.x > 0.f)
			{
				screen_zoom_pos.target_x += static_cast<float>(pMOUSE->left_ref().clicked_pos_log_x - SCREEN_CX) * 0.1f;
				screen_zoom_pos.target_y += static_cast<float>(pMOUSE->left_ref().clicked_pos_log_y - SCREEN_CY) * 0.1f;
			}
		}
	}

	if (pMOUSE->middle_ref().pressing_count)
	{
		target_x -= static_cast<float>(mouse_pos.x - mouse_pos_old.x) / TEMP_SCREEN_EXT_RATE_MAX;
		target_y -= static_cast<float>(mouse_pos.y - mouse_pos_old.y) / TEMP_SCREEN_EXT_RATE_MAX;
		x = target_x;
		y = target_y;
	}
	else if(key[KEY_INPUT_Z] == 0)
	{
		int ext = static_cast<int>(TEMP_SCREEN_EXT_RATE_MAX);
		if (mouse_pos.x > (screen_w - mouse_scroll_threshold.x)*ext)
		{
			SetMousePoint(int((screen_w - (float)mouse_scroll_threshold.x)*ext), mouse_pos.y);
			target_x += static_cast<float>(mouse_pos.x - mouse_pos_old.x);
			GetMousePoint(&mouse_pos.x, &mouse_pos.y);

		}
		if (mouse_pos.x < mouse_scroll_threshold.x*ext)
		{
			SetMousePoint(int((float)mouse_scroll_threshold.x*ext), mouse_pos.y);
			target_x += static_cast<float>(mouse_pos.x - mouse_pos_old.x);
			GetMousePoint(&mouse_pos.x, &mouse_pos.y);
		}
		if (mouse_pos.y > (screen_h - mouse_scroll_threshold.y)*ext)
		{
			SetMousePoint(mouse_pos.x, int((float)(screen_h - mouse_scroll_threshold.y)*ext));
			target_y += static_cast<float>(mouse_pos.y - mouse_pos_old.y);
			GetMousePoint(&mouse_pos.x, &mouse_pos.y);
		}
		if (mouse_pos.y < mouse_scroll_threshold.y*ext)
		{
			SetMousePoint(mouse_pos.x, int((float)mouse_scroll_threshold.y * ext));
			target_y += static_cast<float>(mouse_pos.y - mouse_pos_old.y);
			GetMousePoint(&mouse_pos.x, &mouse_pos.y);
		}
	}

	if (is_settable_chip_ext_pivot)
	{
		if (ImGui::IsAnyWindowHovered() == false)
		{
			if (pMOUSE->left_ref().pressing_count == 1)
			{
				float	extrate = TEMP_SCREEN_EXT_RATE_MAX + screen_zoom.val;
				float	clamp_x = scasf(TEMP_SCREEN_CX)*screen_zoom.val;
				float	clamp_y = scasf(TEMP_SCREEN_CY)*screen_zoom.val;
				Vector2 view;
				view.x = (scasf(pMOUSE->pos_x_ref()) + clamp_x + screen_zoom_pos.x) / extrate;
				view.y = (scasf(pMOUSE->pos_y_ref()) + clamp_y + screen_zoom_pos.y) / extrate;
				Vector2 world = view;
				world.x += camera.x;
				world.y += camera.y;
				world.x *= 1.f / chip_extrate.val;
				world.y *= 1.f / chip_extrate.val;
				chip_ext_pivot = world;
				is_settable_chip_ext_pivot = false;
			}
		}
	}
}

const DirectX::SimpleMath::Vector2 Camera::GetCursorPosRelatedZoom()
{
	Vector2 result	= { 0.f,0.f };
	float	extrate = TEMP_SCREEN_EXT_RATE_MAX + screen_zoom.val;
	float	clamp_x = scasf(TEMP_SCREEN_CX)*screen_zoom.val;
	float	clamp_y = scasf(TEMP_SCREEN_CY)*screen_zoom.val;
	result.x = (scasf(pMOUSE->pos_x_ref()) + clamp_x + screen_zoom_pos.x) / extrate;
	result.y = (scasf(pMOUSE->pos_y_ref()) + clamp_y + screen_zoom_pos.y) / extrate;

	return result;
}

const Vector2 Camera::GetWorldPosFromTempScreenPos(const DirectX::SimpleMath::Vector2 & _temp_screen_pos)
{
	Vector2 world = _temp_screen_pos;
			world.x += camera.x;
			world.y += camera.y;
			world.x *= 1.f / chip_extrate.val;
			world.y *= 1.f / chip_extrate.val;
	
	return	world;
}

const DirectX::SimpleMath::Vector2 Camera::GetWorldPosFromScreenPos(const DirectX::SimpleMath::Vector2 & _screen_pos)
{
	float	extrate = TEMP_SCREEN_EXT_RATE_MAX + screen_zoom.val;
	float	clamp_x = scasf(TEMP_SCREEN_CX)*screen_zoom.val;
	float	clamp_y = scasf(TEMP_SCREEN_CY)*screen_zoom.val;
	Vector2 view;
	view.x = (_screen_pos.x + clamp_x + screen_zoom_pos.x) / extrate;
	view.y = (_screen_pos.y + clamp_y + screen_zoom_pos.y) / extrate;
	Vector2 world = view;
	world.x += camera.x;
	world.y += camera.y;
	world.x *= 1.f / chip_extrate.val;
	world.y *= 1.f / chip_extrate.val;

	return world;
}

const DirectX::SimpleMath::Vector2 Camera::GetWorldPosFromCursorPos()
{
	Vector2 result;
	result = GetCursorPosRelatedZoom();
	result = GetWorldPosFromTempScreenPos(result);

	return result;
}

void Camera::ImGui()
{
	//各スライダーやインプットの第一引数 const char* label は内部でその文字の情報を保存してるらしいので
	//同じ名前のSliderとInputを作ると競合して正しく動かせなくなる模様

	if (!Iwin_flg.cam)return;

	ImGui::Begin("Camera", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar | ImGuiWindowFlags_MenuBar);
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File##Camera"))
			{
				std::array<std::string, eArchiveTypes::ARTYPE_END>
					archive_types = { ".bin", ".json", ".ini", ".xml" };

				if (ImGui::BeginMenu("Save as...##CameraSave"))
				{
					if (ImGui::BeginMenu(std::string("Camera##CameraSave").c_str()))
					{
						for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
						{
							if (ImGui::MenuItem(std::string("Camera" + archive_types[ar_i] + " file##CameraSave").c_str()))
							{
								std::string save_filename = GetSaveFileNameWithExplorer(true, archive_types[ar_i]);
								if (save_filename.empty() == false)
									SaveDataToFile(save_filename, (eArchiveTypes)ar_i);
							}
						}
						ImGui::EndMenu();
					}

					std::array<std::string, 3>		shakes_data = { "SHAKE", "SWING", "ALWAYS_SWING" };
					for (int sdata_i = 0; sdata_i < (int)shakes_data.size(); sdata_i++)
					{
						if (ImGui::BeginMenu(std::string(shakes_data[sdata_i] + "##Camera" + shakes_data[sdata_i] + "Save").c_str()))
						{
							for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
							{
								if (ImGui::MenuItem(std::string(archive_types[ar_i] + " file##" + shakes_data[sdata_i] + "Save").c_str()))
								{
									std::string save_filename = GetSaveFileNameWithExplorer(true, archive_types[ar_i]);
									if (save_filename.empty() == false)
										SaveShakesData(save_filename, (eArchiveTypes)ar_i, sdata_i);
								}
							}

							ImGui::EndMenu();
						}
					}

					if (ImGui::BeginMenu(std::string("FIXED_CAMERA##FixedCamSave").c_str()))
					{
						for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
						{
							if (ImGui::MenuItem(std::string("FixedCamera" + archive_types[ar_i] + " file##FixedCameraSave").c_str()))
							{
								std::string save_filename = GetSaveFileNameWithExplorer(true, archive_types[ar_i]);
								if (save_filename.empty() == false)
									SaveFixdCameraData(save_filename, (eArchiveTypes)ar_i);
							}
						}
						ImGui::EndMenu();
					}

					ImGui::EndMenu();
				}

				if (ImGui::BeginMenu("Load##CameraLoad"))
				{
					if (ImGui::BeginMenu(std::string("Camera##CameraLoad").c_str()))
					{
						for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
						{
							if (ImGui::MenuItem(std::string("Camera" + archive_types[ar_i] + " file##CameraLoad").c_str()))
							{
								std::string load_filename = GetOpenFileNameWithExplorer(true, archive_types[ar_i]);
								if (load_filename.empty() == false)
									LoadDataFromFile(load_filename, (eArchiveTypes)ar_i);
							}
						}

						ImGui::EndMenu();
					}

					std::array<std::string, 3>		shakes_data = { "SHAKE", "SWING", "ALWAYS_SWING" };
					for (int sdata_i = 0; sdata_i < (int)shakes_data.size(); sdata_i++)
					{
						if (ImGui::BeginMenu(std::string(shakes_data[sdata_i] + "##Camera" + shakes_data[sdata_i] + "Load").c_str()))
						{
							for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
							{
								if (ImGui::MenuItem(std::string(archive_types[ar_i] + " file##" + shakes_data[sdata_i] + "Load").c_str()))
								{
									std::string load_filename = GetOpenFileNameWithExplorer(true, archive_types[ar_i]);
									if (load_filename.empty() == false)
										LoadShakesData(load_filename, (eArchiveTypes)ar_i, sdata_i);
								}
							}

							ImGui::EndMenu();
						}
					}

					if (ImGui::BeginMenu(std::string("FIXED_CAMERA##FixedCamLoad").c_str()))
					{
						for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
						{
							if (ImGui::MenuItem(std::string("FixedCamera" + archive_types[ar_i] + " file##FixedCameraLoad").c_str()))
							{
								std::string load_filename = GetOpenFileNameWithExplorer(true, archive_types[ar_i]);
								if (load_filename.empty() == false)
									LoadFixdCameraData(load_filename, (eArchiveTypes)ar_i);
							}
						}
						ImGui::EndMenu();
					}

					ImGui::EndMenu();
				}

				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}

		//General
		{
			//camra texts
#pragma region camratexts
			{
				ImGui::BeginChild("camera params", ImVec2(ImGui::GetWindowContentRegionWidth(), 300),false,
					ImGuiWindowFlags_AlwaysVerticalScrollbar | ImGuiWindowFlags_HorizontalScrollbar); // Leave room for 1 line below us
				ImGui::Text("camera.x: %f y: %f", camera.x, camera.y);

				ImGui::Text("x  : %f y: %f", x, y);
				ImGui::Text("xT : %f y: %f", target_x, target_y);
				ImGui::Text("delay_move_speed : %f", delay_move_speed);
				ImGui::Text("target_move_margin_speed_coefficient: %f", delay_move_speed_additional_coefficient);
				ImGui::NewLine();


				ImGui::Text("angle : %f", angle_deg.val);
				ImGui::Text("angleT: %f", angle_deg.target_val);
				ImGui::Text("delay_move_speed : %f", angle_deg.GetDelayMoveSpeed());
				ImGui::NewLine();

				ImGui::Text("chip_extrate : %f", chip_extrate.val); //delaymover
				ImGui::Text("chip_extrateT: %f", chip_extrate.target_val);
				ImGui::Text("delay_move_speed : %f", chip_extrate.GetDelayMoveSpeed());
				ImGui::NewLine();

				ImGui::Text("chip_ext_pivot.x: %f", chip_ext_pivot.x);// vec2
				ImGui::Text("chip_ext_pivot.y: %f", chip_ext_pivot.y);
				ImGui::NewLine();

				ImGui::Text("is_settable_chip_ext_pivot : %d", is_settable_chip_ext_pivot == true ? 1 : 0);// bool
				ImGui::NewLine();

				ImGui::Text("target_move_margin  x: %f y: %f", target_move_margin_pos.x, target_move_margin_pos.y);
				ImGui::Text("target_move_marginT x: %f y: %f", target_move_margin_pos.target_x, target_move_margin_pos.target_y);
				ImGui::Text("delay_move_speed : %f", target_move_margin_pos.GetDelayMoveSpeed());
				ImGui::Text("target_move_margin  x: %f y: %f", target_move_margin_maxmin.x, target_move_margin_maxmin.y);
				ImGui::Text("target_move_margin_speed_coefficient: %f", target_move_margin_speed_coefficient);
				ImGui::NewLine();

				ImGui::Text("area_max_pos x: %f y: %f", area_max_pos.x, area_max_pos.y);
				ImGui::NewLine();

				ImGui::Text("scroll_margin  x: %f y: %f", scroll_margin.x, scroll_margin.y);
				ImGui::Text("scroll_marginT x: %f y: %f", scroll_margin.target_x, scroll_margin.target_y);
				ImGui::Text("delay_move_speed : %f", scroll_margin.GetDelayMoveSpeed());
				ImGui::NewLine();

				ImGui::Text("is_use_lock_on: %d", is_use_lock_on == true ? 1 : 0);// bool
				ImGui::Text("is_lock_on_trigger: %d", is_lock_on_trigger == true ? 1 : 0);// bool
				ImGui::Text("lock_on_margin  x: %f y: %f", lock_on_margin.x, lock_on_margin.y);
				ImGui::Text("lock_on_marginT x: %f y: %f", lock_on_margin.target_x, lock_on_margin.target_y);
				ImGui::Text("delay_move_speed : %f", lock_on_margin.GetDelayMoveSpeed());
				ImGui::NewLine();

				ImGui::Text("shake_pos  x: %f y: %f", shake_pos.x, shake_pos.y);
				ImGui::Text("shake_posT x: %f y: %f", shake_pos.target_x, shake_pos.target_y);
				ImGui::Text("delay_move_speed : %f", shake_pos.GetDelayMoveSpeed());
				ImGui::NewLine();

				ImGui::Text("swing_pos  x: %f y: %f", swing_pos.x, swing_pos.y);
				ImGui::Text("swing_posT x: %f y: %f", swing_pos.target_x, swing_pos.target_y);
				ImGui::Text("delay_move_speed : %f", swing_pos.GetDelayMoveSpeed());
				ImGui::NewLine();

				ImGui::Text("is_always_swing: %d", is_always_swing == true ? 1 : 0);// bool
				ImGui::NewLine();

				ImGui::Text("is_follow_to_mouse: %d", is_follow_to_mouse == true ? 1 : 0);// bool

				ImGui::EndChild();
			}
#pragma endregion

			ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
			if (ImGui::TreeNode("General"))
			{
				ImGui::Checkbox("is_use_ptr", &is_use_ptr);
				if (!is_use_ptr)
				{
					float* pos_tmp[2];
					pos_tmp[0] = &target_x;
					pos_tmp[1] = &target_y;
					ImGui::InputFloat2("target_position", *pos_tmp, 1);
				}

				//メインの移動速度
				{
					ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
					if (ImGui::TreeNode("Camera move speed to target position"))
					{
						static float move_speed = 0.1f;
						if (ImGui::SliderFloat("move_speed", &move_speed, 0.0f, 1.2f)) SetMoveSpeed(move_speed);
						move_speed = delay_move_speed;

						ImGui::DragFloat("additional_coefficient", &delay_move_speed_additional_coefficient, 0.0000001f, 0.f, 0.1f, "%.7f");

						ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
						if (ImGui::TreeNode("Target Move Margin"))
						{
							ImGui::DragFloat("target_move_margin_maxmin.x", &target_move_margin_maxmin.x, 1.f, 0.f, screen_w_center);
							ImGui::DragFloat("target_move_margin_maxmin.y", &target_move_margin_maxmin.y, 1.f, 0.f, screen_h_center);

							static float margin_move_speed = 0.1f;
							if (ImGui::DragFloat("target_move_margin_move_speed", &margin_move_speed, 0.000001f, 0.f, 1.0f, "%.6f"))target_move_margin_pos.SetMoveSpeed(margin_move_speed);
							ImGui::DragFloat("target_move_margin_speed_coefficient", &target_move_margin_speed_coefficient, 0.0001f, 0.f, 1.0f, "%.6f");

							ImGui::TreePop();
						}

						ImGui::TreePop();
					}
				}

				//Area Max Pos
				{
					ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
					if (ImGui::TreeNode("Area MaxMin Pos"))
					{
						ImGui::DragFloat("area_max_pos.x", &area_max_pos.x, 32.f, -1.f, 640000.f);
						ImGui::DragFloat("area_max_pos.y", &area_max_pos.y, 32.f, -1.f, 640000.f);
						ImGui::DragFloat("area_min_pos.x", &area_min_pos.x, 32.f, 0.f, 640000.f);
						ImGui::DragFloat("area_min_pos.y", &area_min_pos.y, 32.f, 0.f, 640000.f);

						ImGui::TreePop();
					}
				}

				// r_stick_move
				{
					ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
					if (ImGui::TreeNode("r_stick_move"))
					{
						r_stick_move.ImGuiTreeNode("r_stick_move");
						ImGui::DragFloat("r_stick_move_maxmin.x", &r_stick_move_maxmin.x, 1.f, -1000.f, 1000.f);
						ImGui::DragFloat("r_stick_move_maxmin.y", &r_stick_move_maxmin.y, 1.f, -1000.f, 1000.f);

						// 344行目をコメント解除して使用
						//ImGui::Text("r stick test");
						//r_stick_move_test = { 0,0 };
						//ImGui::SliderFloat("stick test x", &r_stick_move_test.x, -32768.f, 32767.f);
						//ImGui::SliderFloat("stick test y", &r_stick_move_test.y, -32768.f, 32767.f);

						ImGui::TreePop();
					}
				}

				// 角度
				{
					ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
					if (ImGui::TreeNode("angle"))
					{
						if (ImGui::Button("reset angle"))angle_deg.target_val = 0.f;
						ImGui::SameLine();
						ImGui::SliderFloat("angle", &angle_deg.target_val, -180.f, 180.f);
						ImGui::TreePop();
					}
				}

				// 拡大率
				{
					ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
					if (ImGui::TreeNode("zoom"))
					{
						if (ImGui::Button("reset zoom"))screen_zoom.target_val = 0.f;
						ImGui::SameLine();
						ImGui::DragFloat("screen_zoom", &screen_zoom.target_val, 0.01f, 0.0f, 24.f);
						ImGui::NewLine();

						if (ImGui::Button("reset zoompos"))
						{
							screen_zoom_pos.target_x = 0.f;
							screen_zoom_pos.target_y = 0.f;
						}
						float clamp_x = scasf(TEMP_SCREEN_CX)*screen_zoom.target_val;
						float clamp_y = scasf(TEMP_SCREEN_CY)*screen_zoom.target_val;
						ImGui::SliderFloat("zoom_posx", &screen_zoom_pos.target_x, -clamp_x, clamp_x);
						ImGui::SliderFloat("zoom_posy", &screen_zoom_pos.target_y, -clamp_y, clamp_y);

						ImGui::TreePop();
					}
				}

				ImGui::NewLine();

				//マウスに追従
				{
					ImGui::Checkbox("Follow to mouse position", &is_follow_to_mouse);
					if (is_follow_to_mouse)
					{
						ImGui::Text("mouse middle click to move or\nbring cursor to edges of the screen to move");
						ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
						if (ImGui::TreeNode("mouse_scroll_threshold"))
						{
							ImGui::Text("mouse_scroll_threshold");
							ImGui::SliderInt("mouse_scroll_threshold.x (+-)", &mouse_scroll_threshold.x, 0, scasi(screen_w_center / 2.f));
							ImGui::SliderInt("mouse_scroll_threshold.y (+-)", &mouse_scroll_threshold.y, 0, scasi(screen_h_center / 2.f));

							ImGui::TreePop();
						}

						ImGui::Text("Z + LeftClick to zoom screen");
						ImGui::NewLine();

						//chip extrate
						{
							ImGui::Text("Mapchip ExtRate");
							ImGui::Checkbox("is_settable_chip_ext_pivot", &is_settable_chip_ext_pivot);

							if (is_settable_chip_ext_pivot)
							{
								ImGui::Text("LeftClick to set chip_ext_pivot position");
							}
							// labelに##を付けるとgui側には表示されない追加のidを付けることが出来る
							if (ImGui::Button("-##chip_extrate"))
							{
								if (chip_extrate.target_val > 0.03125f)
									chip_extrate.target_val /= 2.f;
								chip_extrate.SetMoveSpeed(delay_move_speed);
								target_x = (chip_ext_pivot.x * chip_extrate.target_val) - scroll_margin.target_x;
								target_y = (chip_ext_pivot.y * chip_extrate.target_val) - scroll_margin.target_y;
							}
							ImGui::SameLine();
							if (ImGui::Button("+##chip_extrate"))
							{
								if (chip_extrate.target_val < 1.f)
									chip_extrate.target_val *= 2.f;
								chip_extrate.SetMoveSpeed(delay_move_speed);
								target_x = (chip_ext_pivot.x * chip_extrate.target_val) - scroll_margin.target_x;
								target_y = (chip_ext_pivot.y * chip_extrate.target_val) - scroll_margin.target_y;
							}
							ImGui::SameLine();
							if (ImGui::BeginCombo("chip_extrate", std::to_string(chip_extrate.target_val).c_str())) // コンボメニューに表示する文字
							{
								float rate = 1.f;
								for (int n = 1; n <= 6; n++)
								{
									bool is_selected = (chip_extrate.target_val == (1.f / (float)n)); // You can store your selection however you want, outside or inside your objects
									if (ImGui::Selectable(std::to_string(rate).c_str(), is_selected))
									{
										chip_extrate.SetMoveSpeed(delay_move_speed);
										chip_extrate.target_val = rate;
										target_x = (chip_ext_pivot.x * rate) - scroll_margin.target_x;
										target_y = (chip_ext_pivot.y * rate) - scroll_margin.target_y;
									}
									if (is_selected)
										ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)

									rate /= 2.f;
								}
								ImGui::EndCombo();
							}
						}
					}
					else
					{
						is_settable_chip_ext_pivot = false;
						chip_extrate.target_val = 1.f;
					}
				}
				ImGui::NewLine();//改行

				ImGui::TreePop();
			}
		}

		//Scroll Margin
		{
			static float time = 48.f;

			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("Scroll Margin"))
			{
				//ease algo
				{
					ImGui::Text("Ease algo");
					if (ImGui::BeginCombo("ease algo", Easing::algo_names[ease_algo].c_str())) // コンボメニューに表示する文字
					{
						for (int n = 0; n < (int)Easing::algo_names.size(); n++)
						{
							bool is_selected = (ease_algo == n); // You can store your selection however you want, outside or inside your objects
							if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
								ease_algo = n;
							if (is_selected)
								ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
						}
						ImGui::EndCombo();
					}
				}

				//margin
				{
					bool value_changed = false;
					value_changed |= ImGui::SliderFloat("scroll_margin.x", &scroll_margin.target_x, -screen_w, screen_w);
					value_changed |= ImGui::SliderFloat("scroll_margin.y", &scroll_margin.target_y, -screen_h, screen_h);
					value_changed |= ImGui::SliderFloat("time", &time, 1.f, 240.f);
					if (value_changed)
					{
						scroll_margin.SetEasingFunction(ease_algo);
						SetScrollMergin(scroll_margin.target_x, scroll_margin.target_y, time);
					}
				}

				ImGui::TreePop();
			}
		}

		//ShakeCamera
		{
			static float time = 1.f;
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("ShakeCamera"))
			{
				static bool use_slider = true;
				ImGui::Checkbox("Use Slider", &use_slider);
				if (use_slider)
				{
					ImGui::SliderFloat("time", &time, 1.f, 100.f);
					ImGui::SliderFloat("power", &shake.power, 0.f, 200.f);
					ImGui::SliderFloat("speed", &shake.speed, 0.f, 32.f);
					ImGui::SliderFloat("octaves", &shake.octaves, -32.f, 32.f);
					ImGui::SliderFloat("amp_mult", &shake.amp_mult, -32.f, 32.f);
				}
				else
				{
					ImGui::InputFloat("time", &time, 1.f, 10.f, true);
					ImGui::InputFloat("power", &shake.power, 0.1f, 1.f, true);
					ImGui::InputFloat("speed", &shake.speed, 0.1f, 1.f, true);
					ImGui::InputFloat("octaves", &shake.octaves, 0.1f, 1.f, true);
					ImGui::InputFloat("amp_mult", &shake.amp_mult, 0.1f, 1.f, true);
				}
				ImGui::InputFloat("update_interval", &shake.update_interval, 1.f, 5.f, true);
				if (ImGui::SliderFloat("pos.move_speed", &shake.move_speed, 0.0f, 1.0f)) shake_pos.SetMoveSpeed(shake.move_speed);
				if (ImGui::Button("ShakeCamera"))
				{
					ShakeCamera(shake.speed, shake.power, time, shake.update_interval, shake.octaves, shake.amp_mult);
					//shake.time.target_val = time;
				}
				ImGui::TreePop();
			}
		}

		//always_swing
		{
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("Swing"))
			{
				static float time = 0.f;
				ImGui::SliderFloat("time", &time, 1.f, 1000.f);
				ImGui::SliderFloat("speed", &swing.speed, 0.f, 8.f);
				ImGui::SliderFloat("power", &swing.power, 0.f, 1280.f);
				ImGui::SliderInt("octaves", &swing.octaves, 0, 23);
				ImGui::SliderFloat("persistence", &swing.persistence, -2.f, 2.f);
				if (ImGui::Button("SwingCamera")) SwingCamera(swing.speed, swing.power, swing.octaves, swing.persistence, time);
				ImGui::TreePop();
			}
		}

		//always_swing
		{
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("Always Swing"))
			{
				if (ImGui::Checkbox("IsAlwaysSwing", &is_always_swing))
				{
					SwitchIsAlwaysSwing(is_always_swing);
					AlwaysSwingCamera(always_swing.speed, always_swing.power.target_val, always_swing.octaves, always_swing.persistence.target_val);
				}
				ImGui::SliderFloat("speed", &always_swing.speed, 0.f, 8.f);
				if (ImGui::SliderFloat("power", &always_swing.power.target_val, 0.f, 1280.f)) always_swing.power.StartEasing(always_swing.power.target_val, 1.f / 180.f);
				ImGui::SliderInt("octaves", &always_swing.octaves, 0, 23);
				ImGui::SliderFloat("persistence", &always_swing.persistence.target_val, -2.f, 2.f);
				ImGui::TreePop();
			}
		}

		// fixed_camera
		{
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("Fixed Camera"))
			{
				ImGui::Checkbox("is_update_fixed_camera(usual true)", &is_update_fixed_camera);
				ImGui::Checkbox("is_settable_fixed_pos", &is_settable_fixed_pos);
				if (is_settable_fixed_pos)
				{
					ImGui::Text("Left drag to set fixed_pos");
					ImGui::Text("then if you release a left mouse button, set fixed_rect_size");
				}

				// parameters
				{
					float* arrayed_pos[2] = { &fixed_camera.fixed_pos.x, &fixed_camera.fixed_pos.y };
					ImGui::SliderFloat2("fixed_pos"			, *arrayed_pos, 0.f, (std::max)(area_max_pos.x, area_max_pos.y));
				}
				{
					float* arrayed_pos[2] = { &fixed_camera.fixed_rect_size.x, &fixed_camera.fixed_rect_size.y };
					ImGui::SliderFloat2("fixed_rect_size"	, *arrayed_pos, 0.f, (std::max)(area_max_pos.x, area_max_pos.y));
				}

				ImGui::Checkbox("is_use_ease_fade_in"	, &fixed_camera.is_use_ease_fade_in);
				ImGui::Checkbox("is_use_ease_fade_out"	, &fixed_camera.is_use_ease_fade_out);
				ImGui::SliderFloat("fade_in_time"		, &fixed_camera.fade_in_time	, 1.f, 360.f);
				ImGui::SliderFloat("fade_out_time"		, &fixed_camera.fade_out_time	, 1.f, 360.f);

				ImGui::MyImGui::ComboMenu<(int)Easing::algo_names.size()>("fade_in_ease_algo"	, &fixed_camera.fade_in_ease_algo	, Easing::algo_names);
				ImGui::MyImGui::ComboMenu<(int)Easing::algo_names.size()>("fade_out_ease_algo"	, &fixed_camera.fade_out_ease_algo	, Easing::algo_names);

				ImGui::SliderFloat("fade_out_delay_move_speed_subtract_val", &fixed_camera.fade_out_delay_move_speed_subtract_val, 0.f, 0.6f);

				ImGui::TreePop();
			}

		}
	}
	ImGui::End();
}

//--------------------------------------------------------------
//
//	Serialize
//
//--------------------------------------------------------------
#pragma region cereal

template<class Archive>
void Camera::SHAKE::serialize(Archive & _ar, const uint32_t _version)
{
	_ar
	(
		CEREAL_NVP(time),
		CEREAL_NVP(speed),
		CEREAL_NVP(power),
		CEREAL_NVP(octaves),
		CEREAL_NVP(amp_mult),
		CEREAL_NVP(update_interval)
	);
	if (_version >= 2)
	{
		_ar(move_speed);
	}
}

template<class Archive>
void Camera::SWING::serialize(Archive & _ar, const uint32_t _version)
{
	_ar
	(
		CEREAL_NVP(time),
		CEREAL_NVP(speed),
		CEREAL_NVP(power),
		CEREAL_NVP(octaves),
		CEREAL_NVP(persistence)
	);
	if (_version)
	{

	}
}

template<class Archive>
void Camera::ALWAYS_SWING::serialize(Archive & _ar, const uint32_t _version)
{
	_ar
	(
		CEREAL_NVP(speed),
		CEREAL_NVP(power),
		CEREAL_NVP(octaves),
		CEREAL_NVP(persistence)
	);
	if (_version)
	{

	}
}

template<class Archive>
void Camera::FIXED_CAMERA::serialize(Archive& _ar, const uint32_t _version)
{
	_ar
	(
		CEREAL_NVP(fixed_pos.x)			, CEREAL_NVP(fixed_pos.y),
		CEREAL_NVP(fixed_rect_size.x)	, CEREAL_NVP(fixed_rect_size.y),
		CEREAL_NVP(is_use_ease_fade_in),
		CEREAL_NVP(is_use_ease_fade_out),
		CEREAL_NVP(fade_in_ease_algo),
		CEREAL_NVP(fade_out_ease_algo),
		CEREAL_NVP(fade_in_time),
		CEREAL_NVP(fade_out_time),
		CEREAL_NVP(fade_out_delay_move_speed_subtract_val) // 固定カメラから通常カメラに戻った際のdelay_move_speedの減算量
	);
	if (_version)
	{

	}
}
std::vector<Camera::FIXED_CAMERA>	Camera::fixed_camera_data;

std::vector<Camera::SHAKE>			Camera::shake_param_data;
std::vector<Camera::SWING>			Camera::swing_param_data;
std::vector<Camera::ALWAYS_SWING>	Camera::always_swing_param_data;

void Camera::LoadAll(const int& _stage_num)
{
	static bool loaded = false;
	std::vector<std::string> files;
	std::string c_dir;
	if (loaded == false)
	{
		c_dir = GetCurrentDir();
		files = GetAllFileNamesInDir(c_dir + "\\Data\\Configurations\\Camera\\Shake", "bin");
		for (int file_i = 0; file_i < (int)files.size(); file_i++)
		{
			Camera::SHAKE shake_data;
			std::ifstream ifs;

			ifs.open(".\\Data\\Configurations\\Camera\\Shake\\" + files[file_i], std::ios::binary);

			cereal::BinaryInputArchive i_archive(ifs);
			i_archive(cereal::make_nvp(".\\Data\\Configurations\\Camera\\Shake\\" + files[file_i], shake_data));

			shake_param_data.push_back(shake_data);
		}
		files = GetAllFileNamesInDir(c_dir + "\\Data\\Configurations\\Camera\\Swing", "bin");
		for (int file_i = 0; file_i < (int)files.size(); file_i++)
		{
			Camera::SWING swing_data;
			std::ifstream ifs;

			ifs.open(".\\Data\\Configurations\\Camera\\Swing\\" + files[file_i], std::ios::binary);

			cereal::BinaryInputArchive i_archive(ifs);
			i_archive(cereal::make_nvp(".\\Data\\Configurations\\Camera\\Swing\\" + files[file_i], swing_data));

			swing_param_data.push_back(swing_data);
		}
		files = GetAllFileNamesInDir(c_dir + "\\Data\\Configurations\\Camera\\AlwaysSwing", "bin");
		for (int file_i = 0; file_i < (int)files.size(); file_i++)
		{
			Camera::ALWAYS_SWING always_swing_data;
			std::ifstream ifs;

			ifs.open(".\\Data\\Configurations\\Camera\\AlwaysSwing\\" + files[file_i], std::ios::binary);

			cereal::BinaryInputArchive i_archive(ifs);
			i_archive(cereal::make_nvp(".\\Data\\Configurations\\Camera\\AlwaysSwing\\" + files[file_i], always_swing_data));

			always_swing_param_data.push_back(always_swing_data);
		}
	}

	files = GetAllFileNamesInDir(c_dir + "\\Data\\Configurations\\Camera\\FixedCamera\\stage_" + std::to_string(_stage_num) + "\\", "bin");
	std::vector<FIXED_CAMERA>().swap(fixed_camera_data);
	for (int file_i = 0; file_i < (int)files.size(); file_i++)
	{
		Camera::FIXED_CAMERA fixed_cam;
		std::ifstream ifs;

		ifs.open(".\\Data\\Configurations\\Camera\\FixedCamera\\stage_" + std::to_string(_stage_num) + "\\" + files[file_i], std::ios::binary);

		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(cereal::make_nvp(".\\Data\\Configurations\\Camera\\FixedCamera\\stage_" + std::to_string(_stage_num) + "\\" + files[file_i], fixed_cam));

		fixed_camera_data.push_back(fixed_cam);
	}
	loaded = true;
}

void Camera::SaveShakesData(const std::string & _path, eArchiveTypes _file_type, int _shakes_data)
{
	switch (_shakes_data)
	{
	case _SHAKE:
		SaveShakeData(_path, _file_type);
		break;
	case _SWING:
		SaveSwingData(_path, _file_type);
		break;
	case _ALWAYS_SWING:
		SaveAlwaysSwingData(_path, _file_type);
		break;
	default:
		break;
	}
}
void Camera::SaveShakeData(const std::string& _path, eArchiveTypes _file_type)
{
	std::ofstream
		ofs;
		ofs.open(_path, std::ios::binary);
	if (!ofs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, shake));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI:
	{
		cereal::JSONOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, shake));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, shake));
		break;
	}
	default:
		break;
	}
}
void Camera::SaveSwingData(const std::string& _path, eArchiveTypes _file_type)
{
	std::ofstream
		ofs;
		ofs.open(_path, std::ios::binary);
	if (!ofs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, swing));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI:
	{
		cereal::JSONOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, swing));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, swing));
		break;
	}
	default:
		break;
	}
}
void Camera::SaveAlwaysSwingData(const std::string& _path, eArchiveTypes _file_type)
{
	std::ofstream
		ofs;
		ofs.open(_path, std::ios::binary);
	if (!ofs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, always_swing));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI:
	{
		cereal::JSONOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, always_swing));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, always_swing));
		break;
	}
	default:
		break;
	}
}

void Camera::SaveFixdCameraData(const std::string& _path, eArchiveTypes _file_type)
{
		std::ofstream
		ofs;
		ofs.open(_path, std::ios::binary);
	if (!ofs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, fixed_camera));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI:
	{
		cereal::JSONOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, fixed_camera));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, fixed_camera));
		break;
	}
	default:
		break;
	}
}

void Camera::LoadShakesData(const std::string & _path, eArchiveTypes _file_type, int _shakes_data)
{
	switch (_shakes_data)
	{
	case _SHAKE:
		LoadShakeData(_path, _file_type);
		break;
	case _SWING:
		LoadSwingData(_path, _file_type);
		break;
	case _ALWAYS_SWING:
		LoadAlwaysSwingData(_path, _file_type);
		break;
	default:
		break;
	}
}
void Camera::LoadShakeData(const std::string& _path, eArchiveTypes _file_type)
{
	SHAKE shake_tmp;

	std::ifstream
		ifs;
		ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
	if (!ifs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, shake_tmp));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
	{
		cereal::JSONInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, shake_tmp));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, shake_tmp));
		break;
	}
	default:
		break;
	}

	shake = shake_tmp;
}
void Camera::LoadSwingData(const std::string& _path, eArchiveTypes _file_type)
{
	SWING swing_tmp;

	std::ifstream
		ifs;
		ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
	if (!ifs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, swing_tmp));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
	{
		cereal::JSONInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, swing_tmp));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, swing_tmp));
		break;
	}
	default:
		break;
	}

	swing = swing_tmp;
}
void Camera::LoadAlwaysSwingData(const std::string& _path, eArchiveTypes _file_type)
{
	ALWAYS_SWING always_swing_tmp;

	std::ifstream
		ifs;
		ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
	if (!ifs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, always_swing_tmp));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
	{
		cereal::JSONInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, always_swing_tmp));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, always_swing_tmp));
		break;
	}
	default:
		break;
	}

	always_swing = always_swing_tmp;
}

void Camera::LoadFixdCameraData(const std::string& _path, eArchiveTypes _file_type)
{
	FIXED_CAMERA fixed_camera_tmp;

	std::ifstream
		ifs;
		ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
	if (!ifs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, fixed_camera_tmp));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
	{
		cereal::JSONInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, fixed_camera_tmp));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, fixed_camera_tmp));
		break;
	}
	default:
		break;
	}

	fixed_camera = fixed_camera_tmp;
}

#pragma endregion

Camera cam;

//const float Cxf(const float & _x) { return cam.xf(_x); }
//
//const float Cyf(const float & _y) { return cam.yf(_y); }
//
//const int Cxi(const float & _x) { return cam.xi(_x); }
//
//const int Cyi(const float & _y) { return cam.xi(_y); }
