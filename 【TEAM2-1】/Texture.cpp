#include "Texture.h"
#include <stdio.h>
#include "FunctionDxLib.h"

void Texture::ReloadAllGraphHandles()
{
	DeleteAllGraphHandles();
	loadGrapahs();
}

void Texture::DeleteAllGraphHandles()
{
	//for (auto&& it_ghnd : Graphs)
	//{
	//	DeleteGraph(it_ghnd);
	//	it_ghnd = -1;
	//}
}

void Texture::loadGrapahs()
{
	Graphs[PLAYER] 		  = LoadGraph(L"Data/Images/Player/player.png"		  );
	Graphs[PL_LARGESWORD] = LoadGraph(L"Data/Images/Player/P_LargeSword.png"  );
	Graphs[PL_SPEAR] 	  = LoadGraph(L"Data/Images/Player/P_Spear.png"		  );
	Graphs[PL_LONGBOW] 	  = LoadGraph(L"Data/Images/Player/P_Bow.png"		  );
	Graphs[PL_HUMMER] 	  = LoadGraph(L"Data/Images/Player/P_Hammer.png"	  );
	Graphs[PL_TOMAHAWK]   = LoadGraph(L"Data/Images/Player/P_Tomahawk.png"	  );
	Graphs[BGCHIP] 		  = LoadGraph(L"Data/Images/MapChips/mapchip.png"	  );
	Graphs[BGCHIP_MINI]   = LoadGraph(L"Data/Images/MapChips/mapchip_mini.png");
	Graphs[ENEMY] 		  = LoadGraph(L"Data/Images/Enemies/enemy_st1.png"	  );
	Graphs[BOSS] 		  = LoadGraph(L"Data/Images/Enemies/boss.png"		  );
	Graphs[OBJECT] 		  = LoadGraph(L"Data/Images/Objects/object.png"		  );
	Graphs[OBJECT2] 	  = LoadGraph(L"Data/Images/Objects/object2.png"	  );
	Graphs[ARROW] 		  = LoadGraph(L"Data/Images/Enemies/arrow.png"		  );
	Graphs[E_SIGN] 		  = LoadGraph(L"Data/Images/Enemies/enemy_signs.png"  );
	Graphs[ROCKS] 		  = LoadGraph(L"Data/Images/Enemies/rock.png"		  );
	Graphs[UI] 			  = LoadGraph(L"Data/Images/UI/UI.png"				  );
	Graphs[UI_FONT] 	  = LoadGraph(L"Data/Images/UI/font.png"			  );
	Graphs[UI_BUTTON] 	  = LoadGraph(L"Data/Images/UI/button.png"			  );
	Graphs[UI_PORCH] 	  = LoadGraph(L"Data/Images/UI/porch.png"			  );
	Graphs[UI_MAP] 		  = LoadGraph(L"Data/Images/UI/map.png"				  );
	Graphs[UI_SYSTEM] 	  = LoadGraph(L"Data/Images/UI/system.png"			  );
	Graphs[UI_BLOOD] 	  = LoadGraph(L"Data/Images/UI/blood.png"			  );
	Graphs[UI_PL_HP_LOW]  = LoadGraph(L"Data/Images/UI/low_hp.png"			  );
	Graphs[SHOP_BUY] 	  = LoadGraph(L"Data/Images/UI/shop.png"			  );
	Graphs[SHOP_SELL] 	  = LoadGraph(L"Data/Images/UI/shopssell.png"		  );
	Graphs[TITLE_BUTTON]  = LoadGraph(L"Data/Images/UI/title_button.png"	  );
	Graphs[BLODD_EFC] 	  = LoadGraph(L"Data/Images/UI/blood.png"			  );
	Graphs[ENDROLL] 	  = LoadGraph(L"Data/Images/UI/clear.png"			  );
	Graphs[LOADING] 	  = LoadGraph(L"Data/Images/UI/loading.png"			  );
	Graphs[TITLE_1] 	  = LoadGraph(L"Data/Images/UI/title1.png"			  );
	Graphs[TITLE_2] 	  = LoadGraph(L"Data/Images/UI/title2.png"			  );
	Graphs[TITLE_3] 	  = LoadGraph(L"Data/Images/UI/title3.png"			  );
	Graphs[TITLE_4] 	  = LoadGraph(L"Data/Images/UI/title4.png"			  );
	Graphs[END_1] 		  = LoadGraph(L"Data/Images/UI/end1.png"			  );
	Graphs[END_2] 		  = LoadGraph(L"Data/Images/UI/end2.png"			  );
	Graphs[END_3] 		  = LoadGraph(L"Data/Images/UI/end3.png"			  );
	Graphs[END_4] 		  = LoadGraph(L"Data/Images/UI/end4.png"			  );
	Graphs[BAG_INFO1] 	  = LoadGraph(L"Data/Images/UI/bag001.png"			  );
	Graphs[BAG_INFO2] 	  = LoadGraph(L"Data/Images/UI/bag002.png"			  );
	Graphs[UI_TELEPORTER] = LoadGraph(L"Data/Images/UI/teleportbar.png"		  );

	wchar_t fileName[35] = { 0 };
	for (int i = BG0; i != BG8 + 1; i++)
	{
		swprintf_s(fileName, L"Data/Images/BG/stage1_Tbg/%d.png", i + 1 - BG0);
		Graphs[i] = LoadGraph(fileName);
	}
	for (int i = U_BG0; i < U_BG6; i++)
	{
		swprintf_s(fileName, L"Data/Images/BG/stage1_Ubg/%d.png", i + 1 - U_BG0);
		Graphs[i] = LoadGraph(fileName);
	}

	//for (int i_tex = 0; i_tex < END; i_tex++)
	//{
	//	if (Graphs[i_tex] == -1)
	//	{
	//		setString({ 0,0 }, L"%d not found", i_tex + 1);
	//	}
	//}
}
