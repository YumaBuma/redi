#pragma once

#include "Function.h"
#include "easing.h"

#include "CerealLoadAndSave.h"

class DelayMover /*:public CerealLoadAndSave*/
{
public:
	DelayMover() :val(0.f), target_val(0.f), delay_move_speed(0.1f), difference_margin(FLT_EPSILON * 2.f), is_update_completed(true) {};
	virtual ~DelayMover() {};


	//デフォルトで0.1f
	void SetMoveSpeed(const float& _delay_move_speed) { delay_move_speed = _delay_move_speed; }
	const float& GetDelayMoveSpeed()const { return delay_move_speed; }


	void SetDifferenceMargin(const float& _difference_margin = FLT_EPSILON * 2.f) { difference_margin = _difference_margin; }


	//呼び出すとtarget_valへ徐々に移動します
	//移動完了時にfalseを返します
	virtual bool Update() 
	{
		if (fabsf(target_val - val) > difference_margin)
		{
			DelayMove(val, target_val, delay_move_speed);
			is_update_completed = false;
			return true;
		}
		else
		{
			//移動完了
			val = target_val;
			is_update_completed = true;
			return false;
		}
	}
	const bool& IsUpdateCompleted()const { return is_update_completed; }

	void operator= (const float& _target_val) { target_val = _target_val; }

	void ImGuiTreeNode(const std::string& _tree_node_rabel);

	template<class Archive>
	void serialize(Archive& _ar, std::uint32_t const version)
	{
		_ar
		(
			CEREAL_NVP(val),
			CEREAL_NVP(target_val),
			CEREAL_NVP(delay_move_speed),
			CEREAL_NVP(difference_margin)
		);
	}

public:
	float val;
	float target_val;
	float delay_move_speed;
	float difference_margin;// 現在値と目標値の差分を同じと見なすしきい値
	bool is_update_completed;
};
CEREAL_CLASS_VERSION(DelayMover, 1);

class EaseMover :public DelayMover
{
public:
	EaseMover(const bool& _is_use_ease = false) :DelayMover(),
		ease_algo(Easing::LINEAR),
		is_use_ease(_is_use_ease),
		ease_val_min(0.f),
		ease_time_now(0.f), ease_totaltime(1.0f), ease_time_addition_rate(0.04f)
	{}
	~EaseMover() {}



	//移動にイージングを適用する
	void SwitchIsUseEasing()				{ is_use_ease = !is_use_ease; }//自分の状態の反転
	void SetIsUseEasing(bool _is_use_ease)	{ is_use_ease = _is_use_ease; }
	const bool& GetIsUseEasing()const { return is_use_ease; }
	void SetEasingFunction(const int& _ease_algo);
	void StartEasing(const float& _target_val, const float& _ease_time_addition_rate);
	// イージングの進行度を設定します
	void SetEasingProgress(const float& _ease_time);


	//イージング変数取得系
	const float& GetEaseTimeNow()const { return ease_time_now; }



	virtual bool Update() override;

	void operator= (const float& _target_val) { target_val = _target_val; }

	void ImGuiTreeNode(const std::string& _tree_node_rabel);

	template<class Archive>
	void serialize(Archive& _archive, std::uint32_t const version)
	{
		_archive
		(
			CEREAL_NVP(val),
			CEREAL_NVP(target_val),
			CEREAL_NVP(delay_move_speed),
			CEREAL_NVP(difference_margin),
			CEREAL_NVP(is_use_ease),
			CEREAL_NVP(ease_val_min),
			CEREAL_NVP(ease_time_now),
			CEREAL_NVP(ease_totaltime),
			CEREAL_NVP(ease_algo),
			CEREAL_NVP(ease_time_addition_rate)
		);
	}
	

private:
	bool  is_use_ease;
	float ease_val_min;
	float ease_time_now;
	float ease_totaltime;
public:
	int   ease_algo;
	float ease_time_addition_rate;
};
CEREAL_CLASS_VERSION(EaseMover, 1);

class DelayMoverVec2 /*:public CerealLoadAndSave*/
{
public:
	DelayMoverVec2() 
		: x(0.f), y(0.f), target_x(0.f), target_y(0.f),
		delay_move_speed(0.1f), difference_margin(0.08f), is_update_completed(true)
	{}
	virtual ~DelayMoverVec2() {}



	//デフォルトで0.1f
	void SetMoveSpeed(const float& _delay_move_speed) { delay_move_speed = _delay_move_speed; }



	//DirectX::SimpleMath::Vector2型で位置をセット
	void SetPosAtVector2	  (const DirectX::SimpleMath::Vector2& _pos)		{ x = _pos.x; y = _pos.y; }
	void SetTargetPosAtVector2(const DirectX::SimpleMath::Vector2& _target_pos) { target_x = _target_pos.x; target_y = _target_pos.y; }



	//呼び出すとtarget_x,yへ徐々に移動します
	//移動完了時にfalseを返します
	virtual bool Update();
	const bool& IsUpdateCompleted()const { return is_update_completed; }

	const float GetDelayMoveSpeed()const { return delay_move_speed; }

	//DirectX::SimpleMath::Vector2に変換して取得
	const DirectX::SimpleMath::Vector2 GetPosAtVector2()	  const { return DirectX::SimpleMath::Vector2(x			, y); }
	const DirectX::SimpleMath::Vector2 GetTargetPosAtVector2()const { return DirectX::SimpleMath::Vector2(target_x	, target_y); }

	void operator= (const DirectX::SimpleMath::Vector2& _target_pos) { SetTargetPosAtVector2(_target_pos); }

	void ImGuiTreeNode(const std::string& _tree_node_rabel);

	template<class Archive>
	void serialize(Archive& _archive, std::uint32_t const version)
	{
		_archive
		(
			CEREAL_NVP(x),
			CEREAL_NVP(y),
			CEREAL_NVP(target_x),
			CEREAL_NVP(target_y),
			CEREAL_NVP(delay_move_speed),
			CEREAL_NVP(difference_margin)
		);
	}
	

public:
	float x;
	float y;
	float target_x;
	float target_y;
	float difference_margin;

protected:
	float delay_move_speed;
	bool is_update_completed;
};
CEREAL_CLASS_VERSION(DelayMoverVec2, 1);

class EaseMoverVec2 :public DelayMoverVec2
{
public:
	EaseMoverVec2(const bool& _is_use_ease = false) :DelayMoverVec2(),
		ease_algo(Easing::LINEAR),
		is_use_ease(_is_use_ease), 
		ease_min_x(0.f), ease_min_y(0.f),
		ease_time_now(0.f), ease_time_addition_rate(0.04f)
	{}
	virtual ~EaseMoverVec2() {}



	//移動にイージングを適用する
	void SwitchIsUseEasing()				{ is_use_ease = !is_use_ease; }//自分の状態の反転
	void SetIsUseEasing(bool _is_use_ease)	{ is_use_ease = _is_use_ease; }
	const bool& GetIsUseEasing()const { return is_use_ease; }

	void SetEasingFunction(const int& _ease_algo);
	void StartEasing
	(
		const float& _target_x,
		const float& _target_y,
		const float& _ease_time_addition_rate = 0.04f
	);
	void StartEasingAtVector2
	(
		const DirectX::SimpleMath::Vector2& _pos,
		const float& _ease_time_addition_rate = 0.04f
	);
	// イージングの進行度を設定します
	void SetEasingProgress(const float& _ease_time);


	//イージング変数取得系
	const float& GetEaseTimeNow()const { return ease_time_now; }


	virtual bool Update()override;

	void operator= (const DirectX::SimpleMath::Vector2& _target_pos) { SetTargetPosAtVector2(_target_pos); }

	// 他のImGuiウィンドウ内でTreeNodeとして使用する想定なのでこのままでは使用できません
	void ImGuiTreeNode(const std::string& _tree_node_rabel);

	template<class Archive>
	void serialize(Archive& _archive, std::uint32_t const version)
	{
		_archive
		(
			CEREAL_NVP(x),
			CEREAL_NVP(y),
			CEREAL_NVP(target_x),
			CEREAL_NVP(target_y),
			CEREAL_NVP(delay_move_speed),
			CEREAL_NVP(difference_margin),
			CEREAL_NVP(ease_algo),
			CEREAL_NVP(is_use_ease),
			CEREAL_NVP(ease_min_x),
			CEREAL_NVP(ease_min_y),
			CEREAL_NVP(ease_time_now),
			CEREAL_NVP(ease_totaltime),
			CEREAL_NVP(ease_time_addition_rate)
		);
	}
	

private:
	int   ease_algo;
	bool  is_use_ease;
	float ease_min_x;
	float ease_min_y;
	float ease_time_now;
	float ease_totaltime = 1.000000f;
	float ease_time_addition_rate;
};
CEREAL_CLASS_VERSION(EaseMoverVec2, 1);

class DelayColor /*:public CerealLoadAndSave*/
{
public:
	DelayColor() :col(0.f, 0.f, 0.f, 0.f), target_col(0.f, 0.f, 0.f, 0.f), delay_move_speed(0.1f), is_update_completed(true) {}
	DelayColor(const DirectX::SimpleMath::Vector4& _col) :delay_move_speed(0.1f), is_update_completed(true) { col = _col; target_col = _col; }
	~DelayColor() {}



	bool Update();
	const bool& IsUpdateCompleted()const { return is_update_completed; }
	void SetMoveSpeed(const float& _move_speed)								{ this->delay_move_speed = _move_speed; }
	void SetTargetCol(const float& _r, const float& _g, const float& _b)	{ target_col.x = _r; target_col.y = _g; target_col.z = _b; }
	void SetTargetAlpha(const float& _a) { target_col.w = _a; }
	DirectX::SimpleMath::Color GetCol()
	{
		int r_i = static_cast<int>(col.x);
		int g_i = static_cast<int>(col.y);
		int b_i = static_cast<int>(col.z);
		return GetColor(r_i, g_i, b_i);
	};
	DirectX::SimpleMath::Color GetTargetCol()
	{
		int r_i = static_cast<int>(target_col.x);
		int g_i = static_cast<int>(target_col.y);
		int b_i = static_cast<int>(target_col.z);
		return GetColor(r_i, g_i, b_i);
	};
	int GetAlpha()			{ int a_i = static_cast<int>(col.w); return a_i; }
	int GetTargetAlpha()	{ int a_i = static_cast<int>(target_col.w); return a_i; }

	void ImGuiTreeNode(const std::string& _tree_node_rabel);

	template<class Archive>
	void serialize(Archive& _archive, std::uint32_t const version)
	{
		_archive
		(
			CEREAL_NVP(col.x),
			CEREAL_NVP(col.y),
			CEREAL_NVP(col.z),
			CEREAL_NVP(col.w),
			CEREAL_NVP(target_col.x),
			CEREAL_NVP(target_col.y),
			CEREAL_NVP(target_col.z),
			CEREAL_NVP(target_col.w),
			CEREAL_NVP(delay_move_speed),
			CEREAL_NVP(difference_margin)
		);
	}
	
	void operator= (const DirectX::SimpleMath::Vector4& _target_col) { target_col = _target_col; }

public:
	DirectX::SimpleMath::Vector4	col;
	DirectX::SimpleMath::Vector4	target_col;
	float							difference_margin;
private:
	float delay_move_speed;
	bool is_update_completed;
};
CEREAL_CLASS_VERSION(DelayColor, 1);
