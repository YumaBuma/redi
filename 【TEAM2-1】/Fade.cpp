#include "fade.h"
#include "common.h"


void Fade::Init()
{
	timer = EaseMover();
	timer.SetIsUseEasing(true);
	timer.SetEasingFunction(Easing::LINEAR);
	color = 0;
}

const bool& Fade::Update()
{
	return timer.Update();
}

void Fade::In(const float& _time, const DirectX::SimpleMath::Color& _color)
{
	//if (timer.IsUpdateCompleted() == false)return;
	timer.val = 255.f;
	timer.StartEasing(0.f, 1.f / _time);
	color = _color;
	timer.Update();
}
void Fade::Out	(const float& _time, const DirectX::SimpleMath::Color& _color)
{
	//if (timer.IsUpdateCompleted() == false)return;
	timer.val = 0.f;
	timer.StartEasing(255.f, 1.f / _time);
	color = _color;
	timer.Update();
}

//void Fade::Out(int& _alpha,int _time)
//{
//	timer++;
//	//if (timer == _time * 60)
//	{
//		_alpha += (int)(float)(255 / _time);
//		if (_alpha > 255)
//		{
//			_alpha = 255;
//		}
//		timer = 0;
//	}
//}

//void Fade::In(int& _alpha, int _time)
//{
//	timer++;
//	//if (timer == _time * 60)
//	{
//		_alpha -= (int)(float)(255 / _time);
//		if (_alpha < 0)
//		{
//			_alpha = 0;
//		}
//		timer = 0;
//	}
//}

void Fade::Draw()
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, SCASI(timer.val));
	DrawBox(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, color, TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}