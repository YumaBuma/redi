#pragma once
#include "common.h"
#include "Drawable.h"


struct ID
{
	int CLASS;
	int PERSONAL;
};

class Object : public Drawable
{
public:
	enum CLASS_ID
	{
		PLAYER = 0,
		PL_WEAPON,
		ENEMY,
		EFECT,
		OBSTACLE,
		START,
		GOAL,
		SHOPMAN,
		TEREPORTER,
		TREASUREBOX,
		ELEVATOR,
		STOPPER,
		TRAP,
		DOOR,
		OBJECT,
		MISSILE,
		END,
	};

	bool _PASS_DATA;
	ID id;
	Vector2 pos;
	Vector2 speed;
	Vector2 MAX_SPEED;
	Vector2 INITIAL_SPEED;
	Vector2 delta;
	Vector2 old_pos;
	Vector2 size;
	float angle;
	bool onGround;
	//bool isFlipX;
	bool isGrip;
	bool isActive;
	//knockback
public:
	int noHitTime;
	bool isDamage;
	int NO_MOVE;
	int NO_DAMAGE;
	bool damage_direction;
	Vector2 Knck_SPD;

	int money;

	
public:
	Object();
	Object(float _x, float _y, bool _isFlipX);
	virtual ~Object() = default;

public:
	virtual void Init() = 0;
	virtual bool Update() = 0;
	virtual void Draw() const = 0;
	virtual void ImGui() {};
	virtual void DrawUpdate() {};

	virtual int getHP(int _val) { return 0; }
	virtual int getStep(int _val) { return 0; }
	virtual int getNoHitTime() { return 0; }
	virtual Vector2 getATK(int _val) { return Vector2(0, 0); }
	virtual void setDamage(int _noHitTime, int _damage, Vector2 _patarn) {};
	virtual bool get_PASS_DATA() { return _PASS_DATA; }
	virtual void setknockBack(Vector2 _power) {}
	virtual void addHP(int _plus) {};
	//virtual bool SaveSystemData(shared_ptr<Object> obj) { return false; };
	//virtual bool LoadSystemData(shared_ptr<Object> obj) { return false; };
	//virtual void unInit() = 0;

	void addMoney(int _money)
	{
		money += _money;
	}

	void subMoney(int _money)
	{
		money -= _money;
	}
	void PosUpdateX();
	void PosUpdateY();
	void knockBack();
	void ObjHoseiDown(shared_ptr<Object> &obj);
	void ObjHoseiLeft(shared_ptr<Object> &obj, float size);
	void ObjHoseiRight(shared_ptr<Object> &obj, float size);
};
