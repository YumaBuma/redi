#include "bgObject.h"
#include "ObjectManager.h"

//静止オブジェクト
void  BGObject::Init()
{
	isFlipX = false;
	tex_size = { 32,32 };
	tex_pos = { 32,32 };
	isErase = false;
	isGrip = false;
	stop_anim = true;
	textureNum = 0;
}

bool BGObject::Update()
{
	if (cam.IsInCamera(pos, Vector2((float)tex_size.x, (float)tex_size.y + 10)) == true)
	{
		m_isVisible = true;
	}
	else
		m_isVisible = false;

	if (!m_isVisible)return true;
	// 外 false

	if (animParam.size())
		stop_anim = false;


	if (stop_anim == false)
		playAnimation();

	return true;
}

void BGObject::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

#if ENABLE_SMALL_DRAW
	for (int i = Texture::OBJECT; i < Texture::OBJECT3; i++)
	{
		if (textureNum != i - Texture::OBJECT)
			continue;
		if (animParam.size())
		{
			DrawRectRotaGraphFast2
			(
				cam.xi_ext(pos.x - tex_size.x * 0.5f)/* - camera_pos.x*/,
				cam.yi_ext(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x * aFrame,
				tex_pos.y + tex_size.y * now_anim,
				tex_size.x, tex_size.y,
				0, 0,
				cam.chipextrate(),
				0.f,
				pTexture->getGraphs(i),
				TRUE, isFlipX
			);
		}
		else
			DrawRectRotaGraphFast2(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
				tex_pos.x, tex_pos.y, tex_size.x, tex_size.y,
				0, 0, cam.chipextrate(), 0.f, pTexture->getGraphs(i), TRUE, isFlipX);

	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
#else
	if ((id.CLASS == Lamp && id.PERSONAL == animLamp) || id.CLASS == Door)
	{
		if (!isFlipX)
			DrawRectGraph
			(
				cam.xi(pos.x - tex_size.x*0.5f)/* - camera_pos.x*/,
				cam.yi(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x*aFrame,
				tex_pos.y + tex_size.y*now_anim,
				tex_size.x, tex_size.y,
				pTexture->getGraphs(Texture::OBJECT),
				TRUE
			);
		else
			DrawRectGraph
			(
				cam.xi(pos.x - tex_size.x*0.5f)/* - camera_pos.x*/,
				cam.yi(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x*aFrame,
				tex_pos.y + tex_size.y*now_anim,
				tex_size.x, tex_size.y,
				pTexture->getGraphs(Texture::OBJECT),
				TRUE, true
			);

	}
	else
		DrawRectGraph(cam.xi(pos.x - tex_size.x*0.5f), cam.yi(pos.y - tex_size.y),
			tex_pos.x, tex_pos.y, tex_size.x, tex_size.y, pTexture->getGraphs(Texture::OBJECT), TRUE, isFlipX);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

#endif
}


void BGObject::ImGui()
{
}


int Obstacles::ATK[2] = { 0 };
Vector2 Obstacles::distPos[9][5] = { { { 0.f,0.f } } };
Vector2 Obstacles::ColSize[9][5] = { { { 0.f,0.f } } };

Vector2 Obstacles::BreakPos[3] = { { 0.f,0.f } };
Vector2 Obstacles::BreakSize[3] = { { 0.f,0.f } };



void Obstacles::Init()
{
	isFlipX = false;
	tex_size = { 32,32 };
	tex_pos = { 32,32 };
	isErase = false;
	isGrip = false;
	stop_anim = true;
	textureNum = 0;
	centerPos = { 0,0 };
	angle = 0;
	stop_anim = true;
	animFin = false;
	breakPos = { 0,0 };
	breakSize = { 0,0 };
	timer = 0;
}

bool Obstacles::Update()
{
	if (cam.IsInCamera(pos, Vector2((float)tex_size.x, (float)tex_size.y + 10)) == true)
	{
		m_isVisible = true;
	}
	else
		m_isVisible = false;

	if (!m_isVisible)return true;
	// 外 false

	setInfo();

	IndiUpdate();


	return true;
}

void Obstacles::IndiUpdate()
{
	switch (id.CLASS)
	{
	case Door:
		DoorUpdate();
		break;

	case Trap:
	case Needle:
		TrapUpdate();
		break;

	case ELEVATOR_G:
		elevatorUpdate();
		break;

	case Teleport:
		TereporterUpdate();
		break;

	case Start:
		startUpdate();
	default:
		break;
	}
	if (stop_anim == false)
		playAnimation();

}

void Obstacles::DoorUpdate()
{

}

void Obstacles::TrapUpdate()
{
	if (id.CLASS == bgObjectGroup::Trap && id.PERSONAL == trapType::axe)
	{
		if (angle >= 360)
			angle = 0;
		else
			angle++;
		colPos = { colPos.x*sinf(angle / 180 * 3.14),colPos.y*cosf(angle / 180 * 3.14) };

	}
	else if (id.CLASS == bgObjectGroup::Trap && id.PERSONAL == trapType::moveFloor)
	{
		if (stop_anim)
		{
			if (_PASS_DATA)
			{
				if (timer)
					timer--;
				else
				{
					pSound->playSE(pSound->SE_OBJ_DROPFLOOR, false);
					stop_anim = false;
				}
			}
			else
				timer = 60;
		}

		if (!animFin)
		{
			if (aFrame == 4)
			{
				timer = 60;
				animFin = true;
			}
		}
		else
		{
			if (timer)
				timer--;
			else
			{
				timer = 60;
				aFrame = 0;
				aCnt = 0;
				stop_anim = true;
				animFin = false;
			}
		}
	}

}

void Obstacles::TereporterUpdate()
{
	//static float dist = 0;
	//static Vector2 vec = { 0,0 };
	//vec = pObjManager->ObjList[PLAYER].back()->pos - pos;
	//dist = sqrtf(vec.x*vec.x + vec.y*vec.y);
	if (aFrame == 7)
		animFin = true;
}

void Obstacles::elevatorUpdate()
{
	if (stop_anim)
		return;
	static int timer = 0;
	if (id.PERSONAL == elevatorType::lever)
	{
		if (!animFin)
		{
			timer = 30;
			animFin = true;
			for (auto& p = pObjManager->ObjList[Object::ELEVATOR].begin(); p != pObjManager->ObjList[Object::ELEVATOR].end(); p++)
			{
				if (p->get()->id.PERSONAL == elevatorType::elevator)
					p->get()->isActive = true;
			}
		}


		if (aFrame == 3)
		{
			if (timer)
				timer--;
			else
			{
				stop_anim = true;
				aFrame = 0;
				animFin = false;
			}
		}
	}
}

void Obstacles::startUpdate()
{
	if (!animFin)
	{
		if (aFrame == 11)
		{
			animFin = true;
			pObjManager->ObjList[Object::PLAYER].back()->show();
		}
	}
}

void Obstacles::setInfo()
{
	if (id.CLASS == bgObjectGroup::Trap && id.PERSONAL == trapType::axe)
		attackPoint = ATK[1];
	else if (id.CLASS == bgObjectGroup::Needle)
		attackPoint = ATK[0];
	else
		attackPoint = 0;

	switch (id.CLASS)
	{
	case bgObjectGroup::Needle:
		colPos = distPos[0][id.PERSONAL];
		colSize = ColSize[0][id.PERSONAL];
		break;
	case bgObjectGroup::Trap:
		colPos = distPos[1][id.PERSONAL];
		colSize = ColSize[1][id.PERSONAL];
		size = colSize;
		break;
	case bgObjectGroup::Door:
		colPos = distPos[2][id.PERSONAL];
		colSize = ColSize[2][id.PERSONAL];
		breakPos = BreakPos[0];
		breakSize = BreakSize[0];
		break;
	case bgObjectGroup::TreasureBox:
		colPos = distPos[3][0];
		colSize = ColSize[3][0];
		breakPos = BreakPos[1];
		breakSize = BreakSize[1];
		break;
	case bgObjectGroup::Teleport:
		colPos = distPos[4][id.PERSONAL];
		colSize = ColSize[4][id.PERSONAL];
		breakPos = BreakPos[2];
		breakSize = BreakSize[2];
		break;
	case bgObjectGroup::ELEVATOR_G:

		if (id.PERSONAL == elevatorType::lever)
		{
			colPos = distPos[5][0];
			colSize = ColSize[5][0];
		}
		if (id.PERSONAL == elevatorType::stopper)
		{
			colPos = distPos[5][1];
			colSize = ColSize[5][1];
			size = colSize;
		}
		break;
	case bgObjectGroup::ShopMaster:
		colPos = distPos[6][id.PERSONAL];
		colSize = ColSize[6][id.PERSONAL];
		break;
	case bgObjectGroup::Goal:
		colPos = distPos[7][0];
		colSize = ColSize[7][0];

		break;
	case bgObjectGroup::Start:
		stop_anim = false;
		break;
	}
}

Vector2 Obstacles::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		return colPos;
		break;
	case 1:
		return colSize;
		break;
	case 2:
		return Vector2(attackPoint, animFin);
		break;
	case 3:
		return breakPos;
		break;
	case 4:
		return breakSize;
		break;

	}

	return Vector2();
}

void Obstacles::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

#if ENABLE_SMALL_DRAW
	for (int i = Texture::OBJECT; i < Texture::OBJECT3; i++)
	{
		if (id.CLASS == bgObjectGroup::ELEVATOR_G && id.PERSONAL == elevatorType::stopper)
			continue;
		if (textureNum != i - Texture::OBJECT)
			continue;
		if (animParam.size())
		{
			DrawRectRotaGraphFast2
			(
				cam.xi_ext(pos.x - tex_size.x * 0.5f)/* - camera_pos.x*/,
				cam.yi_ext(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x * aFrame,
				tex_pos.y + tex_size.y * now_anim,
				tex_size.x, tex_size.y,
				0, 0,
				cam.chipextrate(),
				0.f,
				pTexture->getGraphs(i),
				TRUE, isFlipX
			);
		}
		else if (id.CLASS == bgObjectGroup::Trap && id.PERSONAL == trapType::axe)
		{
			DrawRectRotaGraphFast2(cam.xi_ext(pos.x), cam.yi_ext(pos.y),
				tex_pos.x, tex_pos.y, tex_size.x, tex_size.y,
				centerPos.x, centerPos.y, cam.chipextrate(), angle*3.14 / 180, pTexture->getGraphs(i), TRUE, isFlipX);
		}
		else
			DrawRectRotaGraphFast2(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
				tex_pos.x, tex_pos.y, tex_size.x, tex_size.y,
				centerPos.x, centerPos.y, cam.chipextrate(), angle*3.14 / 180, pTexture->getGraphs(i), TRUE, isFlipX);

	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
#else
	if ((id.CLASS == Lamp && id.PERSONAL == animLamp) || id.CLASS == Door)
	{
		if (!isFlipX)
			DrawRectGraph
			(
				cam.xi(pos.x - tex_size.x*0.5f)/* - camera_pos.x*/,
				cam.yi(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x*aFrame,
				tex_pos.y + tex_size.y*now_anim,
				tex_size.x, tex_size.y,
				pTexture->getGraphs(Texture::OBJECT),
				TRUE
			);
		else
			DrawRectGraph
			(
				cam.xi(pos.x - tex_size.x*0.5f)/* - camera_pos.x*/,
				cam.yi(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x*aFrame,
				tex_pos.y + tex_size.y*now_anim,
				tex_size.x, tex_size.y,
				pTexture->getGraphs(Texture::OBJECT),
				TRUE, true
			);

	}
	else
		DrawRectGraph(cam.xi(pos.x - tex_size.x*0.5f), cam.yi(pos.y - tex_size.y),
			tex_pos.x, tex_pos.y, tex_size.x, tex_size.y, pTexture->getGraphs(Texture::OBJECT), TRUE, isFlipX);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

#endif
	if (!show_debug)
		return;

	if (id.CLASS == bgObjectGroup::Trap && id.PERSONAL == trapType::axe)
		DrawLineBox
		(
			cam.xi_ext(pos.x + colPos.x - colSize.x / 2),
			cam.yi_ext(pos.y - colPos.y - colSize.y / 2),
			cam.xi_ext(pos.x + colPos.x + colSize.x / 2),
			cam.yi_ext(pos.y - colPos.y + colSize.y / 2),
			GetColor(255, 5, 80));
	else
		DrawLineBox
		(
			cam.xi_ext(pos.x + colPos.x - colSize.x / 2),
			cam.yi_ext(pos.y - colPos.y - colSize.y),
			cam.xi_ext(pos.x + colPos.x + colSize.x / 2),
			cam.yi_ext(pos.y - colPos.y),
			GetColor(255, 5, 80));


	DrawLineBox
	(
		cam.xi_ext(pos.x + breakPos.x - breakSize.x / 2),
		cam.yi_ext(pos.y - breakPos.y - breakSize.y),
		cam.xi_ext(pos.x + breakPos.x + breakSize.x / 2),
		cam.yi_ext(pos.y - breakPos.y),
		GetColor(255, 5, 250));


}

void Obstacles::ImGui()
{
}

void Obstacles::obImGui()
{
	if (Iwin_flg.obstacle == false)
		return;
	static const char* groupe =
	{ "needle\0trap\0door\0treasureBox\0teleport\0elevator\0shop\0goal\0\0" };
	static const char* personal[] =
	{
		"up\0right\0down\0left\0\0",
		"axe\0moveFloor\0\0",
		"door\0\0",
		"treasureBox\0\0",
		"teleporter\0\0",
		"lever\0stopPos\0\0",
		"shop\0\0",
		"goal\0\0",

	};
	static int selectAct = 0;
	static int selectID = 0;

	ImGui::Begin("obstacle", &Iwin_flg.obstacle, ImGuiWindowFlags_MenuBar);

	if (ImGui::Combo("selectAct", &selectAct, groupe))
	{
		selectID = 0;
	}

	ImGui::Combo("selectPersonal", &selectID, personal[selectAct]);

	string range = "pos" + std::to_string(selectAct) + std::to_string(selectID);
	string size = "size" + std::to_string(selectAct) + std::to_string(selectID);
	string b_range = "breakPos" + std::to_string(selectAct) + std::to_string(selectID);
	string b_size = "breakSize" + std::to_string(selectAct) + std::to_string(selectID);
	string x = "x##";
	string y = "y##";
	for (int i = 0; i < 9; i++)
	{
		if (i != selectAct)
			continue;
		for (int j = 0; j < 5; j++)
		{
			if (j != selectID)
				continue;

			string s_range[2] = { x + range ,y + range };
			ImGui::Text(range.c_str());
			ImGui::DragFloat(s_range[0].c_str(), &distPos[i][j].x);
			ImGui::DragFloat(s_range[1].c_str(), &distPos[i][j].y);
			string s_Size[2] = { x + size ,y + size };

			ImGui::Text(size.c_str());
			ImGui::DragFloat(s_Size[0].c_str(), &ColSize[i][j].x);
			ImGui::DragFloat(s_Size[1].c_str(), &ColSize[i][j].y);

		}
	}

	if (selectAct == 0)
		ImGui::DragInt("ATK##needle", &ATK[0]);
	else if (selectAct == 1 && selectID == 0)
		ImGui::DragInt("ATK##axe", &ATK[1]);
	else if (selectAct == 2)
	{
		string s_range[2] = { x + b_range ,y + b_range };
		ImGui::Text(b_range.c_str());
		ImGui::DragFloat(s_range[0].c_str(), &BreakPos[0].x);
		ImGui::DragFloat(s_range[1].c_str(), &BreakPos[0].y);
		string s_Size[2] = { x + b_size ,y + b_size };

		ImGui::Text(b_size.c_str());
		ImGui::DragFloat(s_Size[0].c_str(), &BreakSize[0].x);
		ImGui::DragFloat(s_Size[1].c_str(), &BreakSize[0].y);

	}
	else if (selectAct == 3)
	{
		string s_range[2] = { x + b_range ,y + b_range };
		ImGui::Text(b_range.c_str());
		ImGui::DragFloat(s_range[0].c_str(), &BreakPos[1].x);
		ImGui::DragFloat(s_range[1].c_str(), &BreakPos[1].y);
		string s_Size[2] = { x + b_size ,y + b_size };

		ImGui::Text(b_size.c_str());
		ImGui::DragFloat(s_Size[0].c_str(), &BreakSize[1].x);
		ImGui::DragFloat(s_Size[1].c_str(), &BreakSize[1].y);

	}
	else if (selectAct == 4)
	{
		string s_range[2] = { x + b_range ,y + b_range };
		ImGui::Text(b_range.c_str());
		ImGui::DragFloat(s_range[0].c_str(), &BreakPos[2].x);
		ImGui::DragFloat(s_range[1].c_str(), &BreakPos[2].y);
		string s_Size[2] = { x + b_size ,y + b_size };

		ImGui::Text(b_size.c_str());
		ImGui::DragFloat(s_Size[0].c_str(), &BreakSize[2].x);
		ImGui::DragFloat(s_Size[1].c_str(), &BreakSize[2].y);

	}



	ImGui::NewLine();

	if (ImGui::Button("save"))
	{
		saveFile();
	}

	ImGui::End();


}

void Obstacles::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Object/obstacle_info.txt");

	fopen_s(&file, fileName, "w");

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			fprintf_s(file, "%f\n", distPos[i][j].x);
			fprintf_s(file, "%f\n", distPos[i][j].y);

			fprintf_s(file, "%f\n", ColSize[i][j].x);
			fprintf_s(file, "%f\n", ColSize[i][j].y);
		}
	}


	for (int i = 0; i < 2; i++)
	{
		fprintf_s(file, "%d\n", ATK[i]);
	}
	for (int i = 0; i < 3; i++)
	{
		fprintf_s(file, "%f\n", BreakPos[i].x);
		fprintf_s(file, "%f\n", BreakPos[i].y);

		fprintf_s(file, "%f\n", BreakSize[i].x);
		fprintf_s(file, "%f\n", BreakSize[i].y);
	}


	fclose(file);
}

void Obstacles::loadFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Object/obstacle_info.txt");

	fopen_s(&file, fileName, "r");

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			fscanf_s(file, "%f", &distPos[i][j].x);
			fscanf_s(file, "%f", &distPos[i][j].y);

			fscanf_s(file, "%f", &ColSize[i][j].x);
			fscanf_s(file, "%f", &ColSize[i][j].y);
		}
	}
	for (int i = 0; i < 2; i++)
	{
		fscanf_s(file, "%d", &ATK[i]);
	}
	for (int i = 0; i < 3; i++)
	{
		fscanf_s(file, "%f", &BreakPos[i].x);
		fscanf_s(file, "%f", &BreakPos[i].y);

		fscanf_s(file, "%f", &BreakSize[i].x);
		fscanf_s(file, "%f", &BreakSize[i].y);
	}


	fclose(file);
}
