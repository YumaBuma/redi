#pragma once
#include "Object.h"
#include "Camera.h"
#include "EffectRoot.h"
#include "Texture.h"

// 前方宣言
class EffectRoot;

enum eRange
{
	move = 0,
	track,
	attack,
	backStep,
	end
};

class E_SIGN : public Object
{
public:
	void Init()
	{
		isErase = false;
		loadAnimFile("enemy_signs");
	}
	bool Update()
	{

		playAnimation();

		if (aFrame == 9)
			isErase = true;

		return true;
	}
	void Draw()const
	{
		DrawRectRotaGraphFast2
		(
			cam.xi_ext(pos.x - tex_size.x * 0.5f),
			cam.yi_ext(pos.y - tex_size.y),
			scasi(tex_pos.x) + tex_size.x * aFrame,
			scasi(tex_pos.y) + tex_size.y * now_anim,
			tex_size.x,
			tex_size.y,
			0, 0,
			cam.chipextrate_d(),
			0.0,
			pTexture->getGraphs(Texture::E_SIGN),
			TRUE, isFlipX
		);

	}
};

//-------------------------------------
//		間に挟むべき。。。？
//-------------------------------------
class Enemy : public Object
{
public:
	float GRAVITY = 1.f;

	int hp;
	float temp_hp;
	int walkCount;
	int act_interval;
	int notFound;
	int checkSpan;
	int damageCounter;
	int stayTimer = 0; 
	int nowState;
	int attackState;
	int behaviorState;
	bool isAnim;
	bool isStay;
	bool isAttack;
	char inAction;
	int plAttackNum;
	int noDamageTimer;
	bool isFind;
	Vector2 knockBackPower;
	int knockBackState;
	int maxHP;
	int upperHP;

protected:
	std::shared_ptr<EffectRoot> damage_efc_root;
	static std::string damage_efc_filename[Enemy_ID::E_END];

public:
	virtual void Gravity() {}
	virtual void checkAreaX() {}
	virtual void checkAreaY() {}
	virtual void walkAlg(const Object* pl) = 0;
	virtual void Attack() {};
	virtual void searchAlg(float _dist) {};
	virtual void Behavior(float _dist) {};
	virtual void animUpdate() = 0;
	void setknockBack(Vector2 _power);
	//{
	//	knockBackPower = _power;
	//}
	void setDamage(int _noHitTime, int _damage, Vector2 _patarn);
	//{
	//	noHitTime = _noHitTime;
	//	noDamageTimer = _patarn.y;
	//	hp -= _damage;
	//	isDamage = true;
	//	Knck_SPD = { 2,6 };
	//	NO_MOVE = 6;
	//	NO_DAMAGE = 1;
	//	nowState = 0;
	//	plAttackNum = _patarn.x;
	//	knockBackState = 0;

	//}
	void knockBackEnm();
	//{
	//	switch (knockBackState)
	//	{
	//	case 0:
	//		speed = { knockBackPower.x,-knockBackPower.y };
	//		if (!damage_direction)
	//		{
	//			speed.x *= -1;
	//		}

	//		stayTimer = 60;
	//		changeAnimation(0);
	//		knockBackState++;
	//		break;

	//	case 1:
	//		if (onGround)
	//		{
	//			knockBackState++;
	//			speed.x = 0;
	//		}
	//		break;

	//	case 2:
	//		isDamage = false;
	//	}

	//}
	bool moveDirection(const Vector2 _pos)
	{
		if (pos.x - _pos.x < 0)
			return false;
		else
			return true;
	}
	//falseは右に対象が存在

	int getHP(int _val) 
	{
		switch (_val)
		{
		case 0:
			return hp;
			break;
		case 1:
			return temp_hp;
			break;
		case 2:
			return maxHP;
			break;
		case 3:
			return upperHP;
			break;
		}
		return 0;
	}

public:
	virtual ~Enemy() {}
};

class E_N_Slime : public Enemy
{
public:
	static int personal_counter;
	enum nSlimeAnim
	{
		stay = 0,
		walk,
		attack,
		jump,
		landing,
		damage,
	};

	int chargeTime;
	static int maxChargeTime;
	//static int 
	static float range[eRange::end];
	static float walkSpeed;
	static Vector2 jumpPower;
	static int attackPoint;
	static int max_hp;
	static int intarval;
	static Vector2 barPos;
	static Vector2 signPos;
	E_N_Slime();
	~E_N_Slime();
public:
	void Init();
	bool Update();
	void Draw() const;
	static void eImGui();
public:
	void Gravity();
	void checkAreaX();
	void checkAreaY();
	void Behavior(float _dist);
	void walkAlg(const Object* pl);
	void Attack(const float _dist);
	void searchAlg(float _dist);
	void animUpdate();
	Vector2 getATK(int _val);
	static void saveFile();
	static void loadData();
};

class E_N_Skeleton : public Enemy
{
public:
	static int personal_counter;
	enum nSkeletonAnim
	{
		stay = 0,
		walk,
		attack,
		damage,
	};
	static float range[eRange::end];
	static float walkSpeed;
	static float jumpPower;
	static int max_hp;
	static int attackPoint;
	static int intarval;
	int stopTimer;
	static int attackTimer;
	static Vector2 barPos;
	static Vector2 signPos;
	static Vector2 attackPos;
	static Vector2 attackSize;

	E_N_Skeleton();
	~E_N_Skeleton();
public:
	void Init();
	bool Update();
	void Draw() const;
	static void eImGui();
public:
	void Gravity();
	void checkAreaX();
	void checkAreaY();
	void Behavior(float _dist);
	void Attack(float _dist);
	Vector2 getATK(int _val);
	static void saveFile();
	void walkAlg(const Object* pl);
	void searchAlg(float _dist);
	void animUpdate();
	static void loadData();

};

class E_B_Skeleton : public Enemy
{
public:
	static int personal_counter;
	enum bSkeletonAnim
	{
		stay = 0,
		walk,
		attack,
		damage,
	};
	static Vector2 backPower;
	static Vector2 barPos;
	static Vector2 signPos;
	bool backStepflg;
	static float range[eRange::end];
	static float walkSpeed;
	static float jumpPower;
	static int attackPoint;
	static int intarval;
	static int max_hp;
	bool behindWall;
	bool canBackStep;
	bool backFlg;
	E_B_Skeleton();
	~E_B_Skeleton();
private:
	std::shared_ptr<EffectRoot>  back_step_efc_root;
public:
	void Init();
	bool Update();
	void Draw() const;
	static void eImGui();
public:
	void Gravity();
	void checkAreaX();
	void checkAreaY();
	void Behavior(float _dist);
	void Attack(float _dist);
	Vector2 getATK(int _val);
	static void saveFile();
	void walkAlg(const Object* pl);
	void searchAlg(float _dist);
	void backStep();
	void animUpdate();
	static void loadData();
};

class E_Boss : public Enemy
{
public:
	static int personal_counter;
	enum bossAnim
	{
		stay = 0,
		walk,
		Attack_Up,
		Attack_Down, 
		Attack_Dash, 
		end_anim,
	};
	enum bossRange
	{
		attack_down = 0,
		attack_up,
		attack_dash,
		track,
		end_range,

	};
	bool isFin;
	int stopTimer;
	static float dashSpeed;
	static int chargeTime[5];
	static Vector2 distPos[2];
	static Vector2 atkSize[2];
	static float range[bossRange::end_range];
	static float walkSpeed;
	static float jumpPower;
	static int attackPoint[3];
	static int intarval;
	static int max_hp;
	static int rate[4][3];
	int ATK;
	static int random;
	int state;
	bool canBackStep;
	Vector2 attackPos;
	Vector2 attackSize;
	int timer;
private:
	std::shared_ptr<EffectRoot> anmefc_head_root;
	std::shared_ptr<EffectRoot> anmefc_atk_root;
public:
	E_Boss();
	~E_Boss();
public:
	void Init();
	bool Update();
	void Draw() const;
	static void eImGui();
public:
	void Gravity();
	void checkAreaX();
	void checkAreaY();
	void Behavior(const Object* _pl);
	void Attack_up(const Object* _pl);
	void Attack_down(const Object* _pl);
	void Attack_dash(const Object* _pl);
	Vector2 getATK(int _val);
	static void saveFile();
	void walkAlg(const Object* pl);
	void searchAlg(float _dist);
	//void backStep();
	void animUpdate();
	static void loadData();

};
