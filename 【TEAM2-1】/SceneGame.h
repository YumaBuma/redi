#pragma once

#include "scene.h"
#include "Camera.h"

#include "EffectEditor.h"
#include "AnimationEffectEditor.h"
#include "MiniMap.h"
#include "EnemyManager.h"
#include "bgObjectManager.h"
#include <thread>

class Game : public BaseScene
{
private:
	//Camera cam;
	int timer;
	float pl_low_hp_offset;
	DelayMover pl_low_hp;
	DelayMover pl_low_hp_alpha;
	int temp_screen;
	int temp_screen_ui;
	MiniMap mini_map;
	std::thread* load[2];
	EffectEditor 			efc_editor;
	AnimationEffectEditor 	a_efc_editor;
	bool finLinear;
	bool finButtle;
	float	prev_col[4];
	int		bg_col[4];

	int fade_alpha;
	bool loadFin[2];
	Vector2 pl_StartPos;
	int pl_dead_state = 0;
	int pl_dead_timer = 310;
public:
	Game();
	~Game();
	void Init();
	void Update();
	void Draw();
	void ImGui();
	void loadThread1(bool& _flg){
		pBG->LoadFile(stageNumber);
		_flg = true;
	}
	void loadThread2(bool& _flg) {
		penmManager->loadFile();
		pobjBGManager->loadFile();
		_flg = true;
	}

	void setStartPos();
	void saveStartPos();
	void colUpdate();
	void bossProduction();
};
