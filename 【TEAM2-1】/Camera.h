#pragma once

#include "common.h"
#include "EaseMove.h"
#include "PerlinNoise.h"
#include "CerealLoadAndSave.h"


class Camera :public EaseMoverVec2
{
public:
	Camera(const float& _screen_w = 1280.f, const float& _screen_h = 720.f);
	~Camera();

	// _stage_num: カメラデータを読み込む際の番号
	void Init(const int& _stage_num, const float& _screen_w = 1280.f, const float& _screen_h = 720.f);

	
	
	//カメラに追従させる位置をポインタで設定します
	//	位置をポインタで取得しない場合、
	//	メンバ関数の
	//	EaseMoverVec2::SetTargetPos(...); か EaseMoverVec2::SetTargetPosAtVector2(...);
	//	で位置を代入して追従位置を設定してください
	void SetTargetPosPtr(const float* _x = nullptr, const float* _y = nullptr);
	//追従位置にポインタを使用するか切り替えます(ポインタは保持されます)
	void SwitchIsUsePtr(int _state = -1);
	// カメラに追従させる位置を返します
	const DirectX::SimpleMath::Vector2 GetTargetPos() { return p_follow_pos_x ? DirectX::SimpleMath::Vector2(*p_follow_pos_x, *p_follow_pos_y) : DirectX::SimpleMath::Vector2(0, 0); };


	//マップの最大位置を設定します
	//-1の場合位置の制限は適用されなくなります
	void SetAreaMaxPos(const float& _x = -1.f, const float& _y = -1.f);


	bool Update();
	void UpdateFixedCamera();
	void Draw();

private:
	//===========================================================================================
	//カメラを揺らします
	void ShakeCamera(const float& _shake_speed, const float& _shake_power, const float& _shake_time, const float& _shake_update_interval = 1.f, const float& _shake_octaves = 1.f, const float& _shake_amp_mult = 0.5f);
	//カメラを波打つように揺らします
	void SwingCamera(const float& _swing_speed = 2.f, const float& _swing_power = 500.f, const int& _swing_octaves = 0, const float& _swing_persistence = 1.0f, const float& _swing_time = 180.0f);
	//カメラを常に波打つように揺らします
	void AlwaysSwingCamera(const float& _always_swing_speed = 0.2f, const float& _always_swing_power = 500.f, const int& _always_swing_octaves = 0, const float& _always_swing_persistence = 1.0f);
public:
	enum eShake : int { Shake_Pl_Damage, Shake_Enm_Dead, Shake_Pl_Hammer, Shake_End };
	enum eSwing : int { Swing_End };
	enum eAlSwing : int { AlSwing_Pl_Life_Low, AlSwing_End };
	enum eFixedCamera : int { FixCam_End };
	//カメラを揺らします
	void ShakeCamera(const Camera::eShake& _e_shake);
	//カメラを波打つように揺らします
	void SwingCamera(const Camera::eSwing& _e_swing);
	//カメラを常に波打つように揺らします
	void AlwaysSwingCamera(const Camera::eAlSwing& _e_alswing);
	//カメラを常に波打つように揺らすかを設定します
	void SwitchIsAlwaysSwing(int _state = -1);
	//カメラを常に波打つように揺らすかを設定します
	void StartAlwaysSwing();
	//カメラを常に波打つように揺らすかを設定します
	void StopAlwaysSwing();

	//===========================================================================================
	//カメラで映すキャラの中心からどれだけ離れるか設定します
	//	0.fだとカメラの中心にプレイキャラクターが映ります(screen_w,h_center - scroll_mergin_x,y)
	//	_timeで移動時間(フレーム数)を設定します
	void SetScrollMergin(const float& _x, const float& _y, const float& _time = 40.f);
	
	
	//ロックオン位置にポインタを使用するか切り替えます(ポインタは保持されます)
	//	位置をポインタで取得しない場合、
	//	メンバ変数 lock_on_marginの
	//	EaseMoverVec2::SetTargetPos(...); か EaseMoverVec2::SetTargetPosAtVector2(...);
	//	で位置を代入して追従位置を設定してください
	void SwitchIsUseLockOnTragetPosPtr(int _state = -1);
	//ロックオン位置をポインタで設定します
	void SetLockOnTragetPosPtr(const float* _x = nullptr, const float* _y = nullptr);
	
	//呼び出している間はSetLockOnTragetPosPtrなどで設定したポインタの位置でロックオンします
	void LockOn();
	//呼び出している間は引数のポインタの位置の位置でロックオンします
	//	if (Key[ KEY_INPUT_(ボタン) ])cam.LockOn(位置のポインタ);
	//	e.g. 移動速度の設定は lock_on_margin.SetMoveSpeed();で設定します
	void LockOn	(const float* _x = nullptr, const float* _y = nullptr);
	//呼び出している間は引数のポインタの位置でロックオンします
	//	if (Key[ KEY_INPUT_(ボタン) ])cam.LockOn(位置のポインタ);
	//	e.g. 移動速度の設定は lock_on_margin.SetMoveSpeed();で設定します
	void LockOn	(const DirectX::SimpleMath::Vector2* _pos_ptr = nullptr);
	//===========================================================================================

	//===========================================================================================
	//カメラx座標に変換して返します(float)
	const float xf(const float& _x)const { return _x - camera.x; }
	//カメラy座標に変換して返します(float)
	const float yf(const float& _y)const { return _y - camera.y; }
	//マップチップの拡大率を考慮してカメラx座標に変換して返します(float)
	const float xf_ext(const float& _x)const { return (_x * chip_extrate.val) - camera.x; }
	//マップチップの拡大率を考慮してカメラy座標に変換して返します(float)
	const float yf_ext(const float& _y)const { return (_y * chip_extrate.val) - camera.y; }
	
	//カメラx座標に変換して返します(int)
	const int xi(const float& _x)const { return static_cast<int>(_x - camera.x); }
	//カメラy座標に変換して返します(int)
	const int yi(const float& _y)const { return static_cast<int>(_y - camera.y); }
	//マップチップの拡大率を考慮してカメラx座標に変換して返します(float)
	const int xi_ext(const float& _x)const { return static_cast<int>((_x * chip_extrate.val) - camera.x); }
	//マップチップの拡大率を考慮してカメラy座標に変換して返します(float)
	const int yi_ext(const float& _y)const { return static_cast<int>((_y * chip_extrate.val) - camera.y); }
	
	//カメラ座標に変換してDirectX::SimpleMath::Vector2で返します
	const DirectX::SimpleMath::Vector2 pos(const DirectX::SimpleMath::Vector2& _pos)const { return _pos - camera; }
	const float& shake_xf()const { return shake_pos.x; }
	const float& shake_yf()const { return shake_pos.y; }
	const float& swing_xf()const { return swing_pos.x; }
	const float& swing_yf()const { return swing_pos.y; }
	const int shake_xi()const { return static_cast<int>(shake_pos.x); }
	const int shake_yi()const { return static_cast<int>(shake_pos.y); }
	const int swing_xi()const { return static_cast<int>(swing_pos.x); }
	const int swing_yi()const { return static_cast<int>(swing_pos.y); }
	const float& scroll_margin_x()const { return scroll_margin.x; }
	const float& scroll_margin_y()const { return scroll_margin.y; }
	
	//カメラ座標を返します
	const DirectX::SimpleMath::Vector2& GetCamPos()const { return camera; }
	//カメラx座標のみを返します
	const float& GetCamXF()const { return camera.x; }
	//カメラy座標のみを返します
	const float& GetCamYF()const { return camera.y; }
	//カメラx座標のみを返します(int)
	const int GetCamXI()const { return static_cast<int>(camera.x); }
	//カメラy座標のみを返します(int)
	const int GetCamYI()const { return static_cast<int>(camera.y); }
	//===========================================================================================

	//===========================================================================================
	//角度(degeree)を返します
	const double angle()const  { return static_cast<double>(angle_deg.val); }
	//角度(degeree)を返します(float)
	const float& anglef()const { return angle_deg.val; }
	//===========================================================================================

	//===========================================================================================
	//拡大速度を設定します
	void SetScreenZoomSpeed(const float& _delay_move_speed);
	//拡大率を返します
	const double	zoom()const	{ return static_cast<double>(screen_zoom.val); }
	//拡大時の描画位置を返します
	//	これは仮画面を裏画面に描画する際に使用します
	const float&	zxf()const	{ return screen_zoom_pos.x; }
	//拡大時の描画位置を返します
	//	これは仮画面を裏画面に描画する際に使用します
	const float&	zyf()const	{ return screen_zoom_pos.y; }
	//拡大時の描画位置を返します(int)
	//	これは仮画面を裏画面に描画する際に使用します
	const int		zxi()const	{ return static_cast<int>(screen_zoom_pos.x); }
	//拡大時の描画位置を返します(int)
	//	これは仮画面を裏画面に描画する際に使用します
	const int		zyi()const	{ return static_cast<int>(screen_zoom_pos.y); }
	//===========================================================================================

	//===========================================================================================
	//チップの拡大率を返します(float)
	const float& chipextrate()		const { return chip_extrate.val; }
	//チップの拡大率の目標値を返します(float)
	const float& chipextrate_target()const { return chip_extrate.target_val; }
	//チップの拡大率を返します(double)
	const double chipextrate_d()		const { return static_cast<double>(chip_extrate.val); }
	//チップの拡大率の目標値を返します(double)
	const double chipextrate_target_d()	const { return static_cast<double>(chip_extrate.target_val); }
	//===========================================================================================

	//===========================================================================================
	//カメラ内に写っているかの判定を返します
	//_size: _pos(左上)からのサイズ
	bool IsInCamera(const DirectX::SimpleMath::Vector2& _pos, const DirectX::SimpleMath::Vector2& _size = { 0.f,0.f });
	//カメラ内に写っているかの判定を返します
	//_size: _pos(中心点)からのサイズ(キャラのサイズの半分)
	bool IsInCameraCenter(const DirectX::SimpleMath::Vector2& _pos, const DirectX::SimpleMath::Vector2& _size = { 0.f,0.f });
	//カメラ内に写っているかの判定を返します
	//_size: _pos(足元)からのサイズ
	bool IsInCameraFoot(const DirectX::SimpleMath::Vector2& _pos, const DirectX::SimpleMath::Vector2& _size = { 0.f,0.f });
	//===========================================================================================

	//===========================================================================================
	//呼び出している間はマウスに追従します
	void FollowMouse();
	//ズーム値を考慮したカーソル位置を返します
	const DirectX::SimpleMath::Vector2 GetCursorPosRelatedZoom();
	//===========================================================================================

	//===========================================================================================
	//カメラ内の座標をワールド座標に変換して返します
	//	引数の座標は temp_screenの解像度に対しての座標
	//	({ 0.f, 0.f } ~ { TEMPS_SCREEN_WIDHT, TEMPS_SCREEN_HEIGHT })である必要があります
	const DirectX::SimpleMath::Vector2 GetWorldPosFromTempScreenPos(const DirectX::SimpleMath::Vector2& _temp_screen_pos);
	//カメラ内の座標をワールド座標に変換して返します
	//	引数の座標は 実際の解像度に対しての座標
	//	({ 0.f, 0.f } ~ { SCREEN_WIDHT, SCREEN_HEIGHT })である必要があります
	const DirectX::SimpleMath::Vector2 GetWorldPosFromScreenPos(const DirectX::SimpleMath::Vector2& _screen_pos);
	//マウスカーソルの位置をワールド座標に変換して返します
	const DirectX::SimpleMath::Vector2 GetWorldPosFromCursorPos();
	//===========================================================================================

	// Rスティックでのカメラ操作をロックするか設定します
	void SetIsLockRStickMove(const bool _is_lock) { is_lock_r_stick_move = _is_lock; }

	void ImGui();

	template< class Archive >
	void serialize(Archive& _ar, const uint32_t _version)
	{
		_ar
		(
			CEREAL_NVP(x),
			CEREAL_NVP(y),
			CEREAL_NVP(target_x),
			CEREAL_NVP(target_y),
			CEREAL_NVP(delay_move_speed),
			CEREAL_NVP(angle_deg),
			CEREAL_NVP(screen_zoom),
			CEREAL_NVP(screen_zoom_pos),
			CEREAL_NVP(chip_extrate),
			CEREAL_NVP(delay_move_speed_additional_coefficient),
			CEREAL_NVP(target_move_margin_pos),
			CEREAL_NVP(target_move_margin_maxmin.x),
			CEREAL_NVP(target_move_margin_maxmin.y),
			CEREAL_NVP(target_move_margin_speed_coefficient),
			CEREAL_NVP(screen_w),
			CEREAL_NVP(screen_w_center),
			CEREAL_NVP(screen_h),
			CEREAL_NVP(screen_h_center),
			CEREAL_NVP(scroll_margin),
			CEREAL_NVP(ease_algo),
			CEREAL_NVP(lock_on_margin),
			CEREAL_NVP(r_stick_move),
			CEREAL_NVP(r_stick_move_maxmin.x),
			CEREAL_NVP(r_stick_move_maxmin.y)
		);
	}

	void LoadDataFromFile(const std::string& _path, eArchiveTypes _file_type)
	{
		const float*						p_follow_pos_x_tmp	= this->GetPFollowPosX();
		const float*						p_follow_pos_y_tmp	= this->GetPFollowPosY();
		const float*						p_lock_on_pos_x_tmp = this->GetPLockOnPosX();
		const float*						p_lock_on_pos_y_tmp = this->GetPLockOnPosY();
		DirectX::SimpleMath::Vector2		pos_tmp				= this->GetPosAtVector2();
		DirectX::SimpleMath::Vector2		target_pos_tmp		= this->GetTargetPosAtVector2();

		Camera camera_tmp;

		std::ifstream
			ifs;
			ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
		if (!ifs)return; // 読み込み失敗

		switch (_file_type)
		{
		case eArchiveTypes::BINARY:
		{
			cereal::BinaryInputArchive
				i_archive(ifs);
				i_archive(cereal::make_nvp(_path, camera_tmp));
			break;
		}
		case eArchiveTypes::JSON:
		case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
		{
			cereal::JSONInputArchive
				i_archive(ifs);
				i_archive(cereal::make_nvp(_path, camera_tmp));
			break;
		}
		case eArchiveTypes::XML:
		{
			cereal::XMLInputArchive
				i_archive(ifs);
				i_archive(cereal::make_nvp(_path, camera_tmp));
			break;
		}
		default:
			break;
		}

		*this = camera_tmp;
		this->SetPosAtVector2(pos_tmp);
		this->SetTargetPosAtVector2(target_pos_tmp);
		this->SwitchIsUsePtr(true);
		this->SetTargetPosPtr(p_follow_pos_x_tmp, p_follow_pos_y_tmp);
		this->SetLockOnTragetPosPtr(p_lock_on_pos_x_tmp, p_lock_on_pos_y_tmp);
	}
	void SaveDataToFile(const std::string& _path, eArchiveTypes _file_type)
	{
		Camera camera_tmp = *this;
		camera_tmp.SetTargetPosPtr();
		camera_tmp.SetLockOnTragetPosPtr();

		std::ofstream
			ofs;
			ofs.open(_path, std::ios::binary);
		if (!ofs)return; // 読み込み失敗

		switch (_file_type)
		{
		case eArchiveTypes::BINARY:
		{
			cereal::BinaryOutputArchive
				o_archive(ofs);
				o_archive(cereal::make_nvp(_path, camera_tmp));
			break;
		}
		case eArchiveTypes::JSON:
		case eArchiveTypes::INI:
		{
			cereal::JSONOutputArchive
				o_archive(ofs);
				o_archive(cereal::make_nvp(_path, camera_tmp));
			break;
		}
		case eArchiveTypes::XML:
		{
			cereal::XMLOutputArchive
				o_archive(ofs);
				o_archive(cereal::make_nvp(_path, camera_tmp));
			break;
		}
		default:
			break;
		}
	}


private:
	//カメラ座標の結果(演出などの位置を含める)
	DirectX::SimpleMath::Vector2	camera;
	DirectX::SimpleMath::Vector2	camera_prev;
	DirectX::SimpleMath::Vector2	camera_delta;
	DirectX::SimpleMath::Vector2	camera_delta_prev;
	DirectX::SimpleMath::Vector2	target_pos_prev;
	DirectX::SimpleMath::Vector2	target_pos_delta;
	DirectX::SimpleMath::Vector2	target_pos_delta_prev;
	DelayMover						angle_deg;//degree
	
	//追加の拡大率
	EaseMover						screen_zoom;
	DelayMoverVec2					screen_zoom_pos;

	//マップチップの拡大率
	EaseMover						chip_extrate;
	// 拡大、縮小時に原点とする座標
	DirectX::SimpleMath::Vector2	chip_ext_pivot;
	bool							is_settable_chip_ext_pivot;

	//メインの移動スピードに加算する値の係数
	float							delay_move_speed_additional_coefficient;

	//進行方向を広く見せるために

	DelayMoverVec2					target_move_margin_pos;
	DirectX::SimpleMath::Vector2	target_move_margin_maxmin;//(+-)
	float							target_move_margin_speed_coefficient;
	float							target_move_margin_speed_coefficient_tmp;

	//マップの最大最小位置
public:
	DirectX::SimpleMath::Vector2	area_min_pos = { 0,0 };
	DirectX::SimpleMath::Vector2	area_max_pos;
private:

	//追従する位置のポインタ
	bool							is_use_ptr;
	const float*					p_follow_pos_x;
	const float*					p_follow_pos_y;
	const float*					GetPFollowPosX() { return p_follow_pos_x; }
	const float*					GetPFollowPosY() { return p_follow_pos_y; }

	//スクリーン情報
	float							screen_w;
	float							screen_w_center;
	float							screen_h;
	float							screen_h_center;

	//中心
	EaseMoverVec2					scroll_margin;
	int								ease_algo;

	//ロックオン
	DelayMoverVec2					lock_on_margin;
	bool							is_use_lock_on;
	bool							is_lock_on_trigger;
	const float*					p_lock_on_pos_x;
	const float*					p_lock_on_pos_y;
	const float*					GetPLockOnPosX() { return p_lock_on_pos_x; }
	const float*					GetPLockOnPosY() { return p_lock_on_pos_y; }

	// Rスティックでの操作
	DelayMoverVec2					r_stick_move;
	DirectX::SimpleMath::Vector2	r_stick_move_maxmin; // +-
	DirectX::SimpleMath::Vector2	r_stick_move_test; // +-
	bool							is_lock_r_stick_move;

	//汎用
	float							counter;

public:
	//画面揺れ
	struct SHAKE
	{
		EaseMover					time;
		float						speed;
		float						power;
		float						octaves;
		float						amp_mult;
		float						update_interval;
		float						move_speed;

		template< class Archive >	void serialize(Archive& _ar, const uint32_t _version);
	};
	//画面揺れ(Swing)
	struct SWING
	{
		EaseMover					time;
		float						speed;
		float						power;
		int							octaves;
		float						persistence;

		template< class Archive >	void serialize(Archive& _ar, const uint32_t _version);
	};
	//画面揺れ(AlwaysSwing)
	struct ALWAYS_SWING
	{
		float						speed;
		EaseMover					power;
		int							octaves;
		DelayMover					persistence;

		template< class Archive >	void serialize(Archive& _ar, const uint32_t _version);
	};
	// 固定カメラ
	struct FIXED_CAMERA
	{
		DirectX::SimpleMath::Vector2	fixed_pos;		// 固定位置(左上原点)
		DirectX::SimpleMath::Vector2	fixed_rect_size;// 固定位置 + 固定する領域(プレイヤー位置との判定)
		bool							is_use_ease_fade_in;
		bool							is_use_ease_fade_out;
		int								fade_in_ease_algo;
		int								fade_out_ease_algo;
		float							fade_in_time;
		float							fade_out_time;
		float							fade_out_delay_move_speed_subtract_val; // 固定カメラから通常カメラに戻った際のdelay_move_speedの減算量

		template< class Archive >	void serialize(Archive& _ar, const uint32_t _version);
	};
private:

	//画面揺れ
	SHAKE							shake;
	DelayMoverVec2					shake_pos;

	//画面揺れ(Swing)
	SWING							swing;
	DelayMoverVec2					swing_pos;

	//画面揺れ(AlwaysSwing)
	bool							is_always_swing;
	eAlSwing						always_swing_type;
	ALWAYS_SWING					always_swing;

	// 固定カメラ
	bool							is_update_fixed_camera;
	bool							is_on_fixed_cam_pos;
	bool							is_settable_fixed_pos;
	EaseMoverVec2					fixed_camera_pos;
	float							delay_move_speed_tmp;
	FIXED_CAMERA						fixed_camera;
	static std::vector<FIXED_CAMERA>	fixed_camera_data;

	//マウス
	bool							is_follow_to_mouse;
	DirectX::SimpleMath::INT2		mouse_pos;
	DirectX::SimpleMath::INT2		mouse_pos_old;
	DirectX::SimpleMath::INT2		mouse_scroll_threshold;

	static void LoadAll(const int& _stage_num);
	enum eShakesData : int { _SHAKE, _SWING, _ALWAYS_SWING };
	void SaveShakesData		(const std::string& _path, eArchiveTypes _file_type, int _shakes_data);
	void SaveShakeData		(const std::string& _path, eArchiveTypes _file_type);
	void SaveSwingData		(const std::string& _path, eArchiveTypes _file_type);
	void SaveAlwaysSwingData(const std::string& _path, eArchiveTypes _file_type);
	void SaveFixdCameraData	(const std::string& _path, eArchiveTypes _file_type);

	void LoadShakesData		(const std::string& _path, eArchiveTypes _file_type, int _shakes_data);
	void LoadShakeData		(const std::string& _path, eArchiveTypes _file_type);
	void LoadSwingData		(const std::string& _path, eArchiveTypes _file_type);
	void LoadAlwaysSwingData(const std::string& _path, eArchiveTypes _file_type);
	void LoadFixdCameraData	(const std::string& _path, eArchiveTypes _file_type);

	static std::vector<SHAKE>			shake_param_data;
	static std::vector<SWING>			swing_param_data;
	static std::vector<ALWAYS_SWING>	always_swing_param_data;

	PerlinNoise noise;
};

extern Camera cam;

#define CAM_ARGS(_pos) cam.xi_ext((_pos.x)), cam.yi_ext((_pos.y))

//const float	Cxf(const float& _x);
//const float	Cyf(const float& _y);
//const int	Cxi(const float& _x);
//const int	Cyi(const float& _y);

CEREAL_CLASS_VERSION(Camera, 1);
CEREAL_CLASS_VERSION(Camera::SHAKE, 2);
CEREAL_CLASS_VERSION(Camera::SWING, 1);
CEREAL_CLASS_VERSION(Camera::ALWAYS_SWING, 1);
CEREAL_CLASS_VERSION(Camera::FIXED_CAMERA, 1);
