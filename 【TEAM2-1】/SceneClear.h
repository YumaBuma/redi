#pragma once

#include "scene.h"
#include "Animation.h"
#include "EffectManager.h"

class EffectRoot;

class Clear : public BaseScene
{
private:
	static int fogTimer;
	int temp_screen;
	int		bg_col[4];
	bool start;
	int loading_alpha;
	bool isincliment;

	Animation clear_Gr;
	std::shared_ptr<EffectRoot> clear_efc;

public:
	Clear();
	~Clear();
	void Init();
	void Update();
	void Draw();
	void ImGui();


	int fade_alpha;

};