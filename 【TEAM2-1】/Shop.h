#pragma once

#include "Object.h"
#include "Weapon.h"

//class ShopMaster : Object
//{
//public:
//	ShopMaster::ShopMaster();
//
//	void Init();
//	bool Update();
//	void Draw() const;
//
//};

class Shop
{
public:
	bool isOpen;
	enum MENUBOX_ID
	{
		BACKGROUND = 0,
		BAG,
		MENUBAR,
		MENUBAR_CURSOR,
		END,
	};

	bool isShopping;
	int heal_hp;
	int act;
	int mode;
	bool sellmode;
	Vector2 pos[END];
	int cursor;
	int tmp_cursor = -1;
	WEAPON pick_weapon;
	WEAPON equipments[5];
	int heal[3];
	int howmuch[8];

public:
	Shop()
	{
		isOpen = false;
		for (int i = 0; i < END; i++)
		{
			pos[i] = { 0,0 };
		}
	}
	~Shop() {};
public:
	void Init();
	void Update();
	void Draw() const;

	void buy_Update();
	void sell_Update();

	void buy_Draw() const;
	void sell_Draw()const;

};

extern Shop shop;