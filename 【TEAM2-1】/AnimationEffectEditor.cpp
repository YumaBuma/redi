#include "Function.h"
#include "FunctionDxLib.h"
#include "AnimationEffectEditor.h"
#include "Input.h"
#include "Effect.h"

#include "Texture.h"

#include "src\\imgui.h"

#include <iostream>
#include <cereal\cereal.hpp>
#include <cereal\archives\binary.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\archives\xml.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\types\string.hpp>


ANIM_EMIT_DATASET emit_data;

namespace ImGui
{
	bool SliderAndDragFloat
	(
		const std::string&	_label,
		float*				_v,
		const float&		_v_speed_drag = 1.0f,
		const float&		_v_min = 0.0f,
		const float&		_v_max = 1.0f,
		const std::string&	_format = "%.3f",
		const float&		_power = 1.0f
	);
	bool SliderAndDragInt
	(
		const std::string&	_label,
		int*				_v,
		const float&		_v_speed_drag = 1.0f,
		const int&			_v_min = 0,
		const int&			_v_max = 1,
		const char*			_format = "%d"
	);
}

AnimationEffectEditor::AnimationEffectEditor()
{
	efc_states					= 0;
	is_now_playing				= false;
	is_able_to_use_tools		= false;
	can_move_multiple_effect	= false;
	can_select_multiple_effect	= false;
	can_set_effect				= false;
	is_inherit_emt				= true;

	anim_num	= 0;
	now_anim	= 0;
	nowanim		= 0;
	anim_frame	= 0;
	aFrame_tmp	= 0;

	is_now_playing = false;

	// 既存のghndを追加
	{
		for (int i = 0; i < Texture::END; i++)
		{
			int ghnd = pTexture->getGraphs(i);
			if (ghnd != -1)
			{
				ghnds.push_back(ghnd);

				TCHAR path[MAX_PATH];
				std::string relative_path;
				GetGraphFilePath(ghnd, path);
				relative_path = path;
				ConvertRelativePath(relative_path);
				ghnds_path.push_back(relative_path);
			}
		}

		for (auto&& it : EFCDATA->images_efc_ref())
		{
			int ghnd = it.second;
			if (ghnd != -1)
			{
				ghnds.push_back(ghnd);

				TCHAR path[MAX_PATH];
				std::string relative_path;
				GetGraphFilePath(ghnd, path);
				relative_path = path;
				ConvertRelativePath(relative_path);
				ghnds_path.push_back(relative_path);
			}
		}
	}

	ghnd = -1;
	ghnd_path = "";

	{
		efc_types =
		{
			VAR_TO_STR(EFC_TYPE::EFC_DEFAULT),
			VAR_TO_STR(EFC_TYPE::EFC_RANDOM_IMAGE_PARTICLE),
			VAR_TO_STR(EFC_TYPE::EFC_JEWERLY),
			VAR_TO_STR(EFC_TYPE::EFC_END)
		};
		
		efc_angle_types =
		{
			VAR_TO_STR(EffectParams::eAngleTypes::ANGLE_NONE),
			VAR_TO_STR(EffectParams::eAngleTypes::DELAY_MOVE),
			VAR_TO_STR(EffectParams::eAngleTypes::FOLLOW_TO_VEL),
			VAR_TO_STR(EffectParams::eAngleTypes::FOLLOW_TO_DURATION),
			VAR_TO_STR(EffectParams::eAngleTypes::ANGLE_END)
		};

		efc_prim_types =
		{
			VAR_TO_STR(EffectParams::ePrimitiveTypes::CIRCLE),
			VAR_TO_STR(EffectParams::ePrimitiveTypes::PIXEL),
			VAR_TO_STR(EffectParams::ePrimitiveTypes::BOX),
			VAR_TO_STR(EffectParams::ePrimitiveTypes::TRIANGLE),
			VAR_TO_STR(EffectParams::ePrimitiveTypes::PRIMTYPE_NONE)
		};

#if 0
		// https://stackoverflow.com/questions/27669200/how-should-i-brace-initialize-an-stdarray-of-stdpairs
		blend_modes =
		{
			{
				{DX_BLENDMODE_NOBLEND	, "DX_BLENDMODE_NOBLEND"},
				{DX_BLENDMODE_ALPHA		, "DX_BLENDMODE_ALPHA"},
				{DX_BLENDMODE_ADD		, "DX_BLENDMODE_ADD"},
				{DX_BLENDMODE_SUB		, "DX_BLENDMODE_SUB"},
				{DX_BLENDMODE_MUL		, "DX_BLENDMODE_MUL"}
			}
		};
#else
		blend_modes[0] = { DX_BLENDMODE_NOBLEND	, "DX_BLENDMODE_NOBLEND" };
		blend_modes[1] = { DX_BLENDMODE_ALPHA	, "DX_BLENDMODE_ALPHA" };
		blend_modes[2] = { DX_BLENDMODE_ADD		, "DX_BLENDMODE_ADD" };
		blend_modes[3] = { DX_BLENDMODE_SUB		, "DX_BLENDMODE_SUB" };
		blend_modes[4] = { DX_BLENDMODE_MUL		, "DX_BLENDMODE_MUL" };
#endif
	}

	can_set_effect = false;
	is_inherit_emt = true;

	now_edt_emit_data = nullptr;
	now_edt_efcparam = nullptr;
	now_edt_efc_tmp = nullptr;

	anim_emit_dset	= std::make_shared<ANIM_EMIT_DATASET>();
	efc_roots		= std::make_shared<EffectRoot>();
	anim			= std::make_shared<Animation>();
}

AnimationEffectEditor::~AnimationEffectEditor()
{
	{ std::vector<ANIM_EMIT_DATA>().swap(emit_data.dataset); }
	{ std::vector<std::string>().swap(ghnds_path); }
	Init();
}

void AnimationEffectEditor::Init()
{
	anim_num	= 0;
	now_anim	= 0;
	nowanim		= 0;
	anim_frame	= 0;
	aFrame_tmp	= 0;

	is_now_playing = false;

	ghnd = -1;
	ghnd_path = "";

	efc_states = 0;
	is_able_to_use_tools = false;
	can_move_multiple_effect = false;
	can_select_multiple_effect = false;
	can_set_effect = false;
	is_inherit_emt = true;

	now_edt_emit_data = nullptr;
	now_edt_efcparam = nullptr;
	now_edt_efc_tmp = nullptr;
}

void AnimationEffectEditor::Update()
{
	cam.Update();

	if (efc_roots)efc_roots->root_pos = pos;
	if (efc_roots && is_now_playing)
	{
		if (use_Animation)
		{
			anim->playAnimation();
			aFrame_tmp = anim_frame = (int)anim->getaFrame();
			nowanim = now_anim = anim->now_anim;
		}
		else
		{
			aFrame_tmp = anim_frame;
			nowanim = now_anim;
		}

		if (efc_roots->Update() == false)
		{
			is_now_playing = false;

			{
				anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
				efc_roots.reset();
				if (emit_data.dataset.empty() == false)
				{
					efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
				}
			}
		}
	}

	if (emit_data.dataset.empty())return;
	//if (emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.empty())return;
	if (now_edt_efcparam == nullptr)return;
	// クリックしてEffect追加
	{
		if (can_set_effect)
		{
			if (ImGui::IsMouseHoveringAnyWindow() == false)
			{
				if (now_edt_emit_data != nullptr)
				{
					//追加
					if (pMOUSE->left_ref().pressing_count == 1 || key[KEY_INPUT_LALT] && pMOUSE->left_ref().pressing_count)
					{
						EmitterParams tmp = now_edt_emit_data->anim_emit_data;

						tmp.pos += cam.GetWorldPosFromCursorPos();
						tmp.pos.x = floorf(tmp.pos.x) - pos.x;
						tmp.pos.y = floorf(tmp.pos.y) - pos.y;

						if (is_inherit_emt)
							now_edt_efcparam->push_back(tmp.InheritFromEmitterParams());
						else
							now_edt_efcparam->push_back(tmp.InheritFromEffectParams());

						{
							anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
							efc_roots.reset();
							if (emit_data.dataset.empty() == false)
							{
								efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
							}
						}
					}

					//削除
					if (pMOUSE->right_ref().pressing_count == 1 || (key[KEY_INPUT_LALT] && pMOUSE->right_ref().pressing_count))
					{
						auto& it_find_efc = std::find_if
						(
							now_edt_efcparam->begin(),
							now_edt_efcparam->end(),
							[&](EffectParams& _efcparams)
							{
							Vector2 pos = { floorf(_efcparams.pos.x),floorf(_efcparams.pos.y) };
							Vector2 cursor_pos = cam.GetWorldPosFromCursorPos();
									cursor_pos.x = floorf(cursor_pos.x);
									cursor_pos.y = floorf(cursor_pos.y);
							return pos == cursor_pos;
							}
						);
						now_edt_efcparam->erase(it_find_efc, now_edt_efcparam->end());

						{
							anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
							efc_roots.reset();
							if (emit_data.dataset.empty() == false)
							{
								efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
							}
						}
					}
				}
			}
		}
	}

	GetMultipleSelectEffects();

}

void AnimationEffectEditor::Draw()
{
	EffectParams::DRAW draw;
	int xi;
	int yi;

	if (use_Animation)
	{
		draw.srcx = anim->tex_size.x;
		draw.srcy = anim->tex_size.y;
		draw.cx   = scasi(anim->tex_pos.x);
		draw.cy	  = scasi(anim->tex_pos.y);
		draw.anmx = anim->tex_size.x*anim->getaFrame();
		draw.anmy = anim->tex_size.y*now_anim;

		xi = cam.xi_ext(pos.x - scasf(draw.srcx) * 0.5f);
		yi = cam.yi_ext(pos.y - scasf(draw.srcy));
	}
	else
	{
		draw = play_param;
		draw.anmx = draw.srcx * draw.anmx;
		draw.anmy = draw.srcy * draw.anmy;
		xi = cam.xi_ext(pos.x - scasf(draw.srcx) * 0.5f);
		yi = cam.yi_ext(pos.y - scasf(draw.srcy));
	}

	DrawRectRotaGraph2
	(
		xi,
		yi,
		draw.cx + draw.anmx,
		draw.cy + draw.anmy,
		draw.srcx,
		draw.srcy,
		0, 0,
		cam.chipextrate_d(),
		0.0,
		ghnd,
		TRUE
	);


	//for (auto& it_emitter : anim_emitters)
	//{
	//	it_emitter->Draw();
	//}

	if (efc_roots)efc_roots->Draw();

	EFC_MGR->Draw();

	if (can_set_effect)
	{
		Vector2 cursor_pos = { 0,0 };/* = cam.GetCursorPosRelatedZoom();*/
		cursor_pos += cam.GetWorldPosFromCursorPos();
		cursor_pos.x = floorf(cursor_pos.x);
		cursor_pos.y = floorf(cursor_pos.y);

		DrawPixel
		(
			cam.xi_ext(cursor_pos.x),
			cam.yi_ext(cursor_pos.y),
			GetColor(100, 120, 200)
		);
	}

	if (can_select_multiple_effect)
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 14);
		{
			if (ImGui::IsMouseHoveringAnyWindow() == false)
			{
				// ドラッグ中選択範囲を表示
				// 追加
				if (pMOUSE->left_ref().pressing_count)
				{
					Vector2 min_pos = cam.GetWorldPosFromScreenPos(Vector2((float)pMOUSE->left_ref().clicked_pos_log_x, (float)pMOUSE->left_ref().clicked_pos_log_y));
					Vector2 max_pos = cam.GetWorldPosFromScreenPos(Vector2((float)pMOUSE->pos_x_ref(), (float)pMOUSE->pos_y_ref()));
					DrawBox
					(
						cam.xi_ext(min_pos.x),
						cam.yi_ext(min_pos.y),
						cam.xi_ext(max_pos.x),
						cam.yi_ext(max_pos.y),
						GetColor(100, 100, 200), 1
					);
				}
				// 除外
				else if (pMOUSE->right_ref().pressing_count)
				{
					Vector2 min_pos = cam.GetWorldPosFromScreenPos(Vector2((float)pMOUSE->right_ref().clicked_pos_log_x, (float)pMOUSE->right_ref().clicked_pos_log_y));
					Vector2 max_pos = cam.GetWorldPosFromScreenPos(Vector2((float)pMOUSE->pos_x_ref(), (float)pMOUSE->pos_y_ref()));
					DrawBox
					(
						cam.xi_ext(min_pos.x),
						cam.yi_ext(min_pos.y),
						cam.xi_ext(max_pos.x),
						cam.yi_ext(max_pos.y),
						GetColor(200, 100, 100), 1
					);
				}
			}
			// エフェクトの位置に円描画
			for (auto&& it_efcnum : now_selecting_efcs_tmp)
			{
				EffectParams& tmp = now_edt_efcparam->at(it_efcnum);
				DrawCircleCam(tmp.pos + pos, tmp.radius + 4, GetColor(100, 100, 245), 0);
			}
		}
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
	}

	cam.Draw();
}

void AnimationEffectEditor::FitEmitDataWithAnimationData()
{
	for (auto& it_emit : emit_data.dataset)
	{
		it_emit.anim_efc_data.clear();
	}

	// max_animation分 emplaceして、そのアニメーションのフレーム数分 keyを emplace
	int anim_param_counter = 0;
	for (auto& it_anim : anim->animParam) 
	{
		for (auto& it_emit : emit_data.dataset) // 存在する emitter全てにイテレート
		{
			it_emit.anim_efc_data.emplace_back();		// animParam分 emplace
			it_emit.is_emit_once.emplace_back(false);	// animParam分 emplace

			for (int anim_frame_i = 0; anim_frame_i < it_anim.max_aFrame; anim_frame_i++)
			{
				if (it_emit.anim_efc_data.at(anim_param_counter).find(anim_frame_i) == it_emit.anim_efc_data.at(anim_param_counter).end()) // キーが存在しなければ
				{
					it_emit.anim_efc_data.at(anim_param_counter).emplace(anim_frame_i, std::vector<EffectParams>());
				}
			}
		}

		anim_param_counter++;
	}
}

void AnimationEffectEditor::ImGui(const int& _ghnd)
{
	if (Iwin_flg.anmefc == false)return;
	//cam.ImGui();

	ImGuiImageViewer();
	ImGuiGetPixelColor(_ghnd);

	ImGui::Begin("Effect", nullptr, ImGuiWindowFlags_MenuBar);
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::BeginMenu("Save as...##animefcedit"))
				{
					if (ImGui::MenuItem("BinaryData##save##save"))
					{
						std::string save_filename = GetSaveFileNameWithExplorer(true, ".bin");
						if (save_filename.empty() == false)
							SaveAnimEmitDataset(save_filename, eArchiveTypes::BINARY);
					}
					if (ImGui::MenuItem("JsonData##save"))
					{
						std::string save_filename = GetSaveFileNameWithExplorer(true, ".json");
						if (save_filename.empty() == false)
							SaveAnimEmitDataset(save_filename, eArchiveTypes::JSON);
					}
					if (ImGui::MenuItem("XMLData##save"))
					{
						std::string save_filename = GetSaveFileNameWithExplorer(true, ".xml");
						if (save_filename.empty() == false)
							SaveAnimEmitDataset(save_filename, eArchiveTypes::XML);
					}

					ImGui::EndMenu();
				}
				if (ImGui::BeginMenu("Load##animefcedit"))
				{
					if (ImGui::MenuItem("BinaryData##load"))
					{
						std::string load_filename = GetSaveFileNameWithExplorer(true, ".bin");
						if (load_filename.empty() == false)
							LoadAnimEmitDataset(load_filename, eArchiveTypes::BINARY);
					}
					if (ImGui::MenuItem("JsonData"))
					{
						std::string load_filename = GetSaveFileNameWithExplorer(true, ".json");
						if (load_filename.empty() == false)
							LoadAnimEmitDataset(load_filename, eArchiveTypes::JSON);
					}
					if (ImGui::MenuItem("XMLData"))
					{
						std::string load_filename = GetSaveFileNameWithExplorer(true, ".xml");
						if (load_filename.empty() == false)
							LoadAnimEmitDataset(load_filename, eArchiveTypes::XML);
					}

					ImGui::EndMenu();
				}

				//if (ImGui::MenuItem("Load##animefcedit"))
				//{
				//	std::string save_filename = GetOpenFileNameWithExplorer(true, ".json");
				//	if (save_filename.empty()) return;
				//	LoadAnimEmitDataset(save_filename);
				//}

				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
		}

		// pos
		{
			float* ppos[2] = { &pos.x,&pos.y };
			ImGui::DragFloat2("pos#effec", *ppos, 1.f, 0.f, 1024.f);
		}

		//now_edt_efcparam = &now_edt_emit_data->anim_efc_data.at(nowanim).find(aFrame_tmp)->second;

		// エフェクト設置を可能にするか切り替え
		ImGui::Checkbox("is_able_to_use_tools", &is_able_to_use_tools);
		if(is_able_to_use_tools)
		{
			std::string efc_states_str[3] = { "can_set_effect", "can_select_multiple_effect", "can_move_multiple_effect" };
			for (int i = 0; i < 3; i++)
			{
				bool activate = (efc_states == i);
				if (ImGui::RadioButton(efc_states_str[i].c_str(), activate))
					efc_states = i;
			}

			switch (efc_states)
			{
			case 0: 	
			{
				can_set_effect				= true;
				can_select_multiple_effect	= false;
				can_move_multiple_effect	= false;
				break;
			}
			case 1:
			{
				can_set_effect				= false;
				can_select_multiple_effect	= true;
				can_move_multiple_effect	= false;
				break;
			}
			case 2:
			{
				can_set_effect				= false;
				can_select_multiple_effect	= false;
				can_move_multiple_effect	= true;
				break;
			}
			default:
			{
				can_set_effect				= false;
				can_select_multiple_effect	= false;
				can_move_multiple_effect	= false;
			}
				break;
			}

			if (can_set_effect)
			{
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("Emitter Num"))
				{
					ImGui::Text("LeftClick to set a effect");
					ImGui::Text("LeftAlt + LeftClick to set multiple effects");
					ImGui::Text("RightClick to erase a effect");
					ImGui::Text("LeftAlt + RightClick to erase multiple effects");
					ImGui::TreePop();
				}
			}
			else if (can_select_multiple_effect)
			{
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("Select multiple effect"))
				{
					ImGui::Text("LeftDrag to select effects in dragged range");
					ImGui::Text("RightDrag to unselect effects in dragged range");
					ImGui::TreePop();
				}
			}
			// todo: can_move_multiple_effect
		}
		else
		{
			can_set_effect				= false;
			can_select_multiple_effect	= false;
			can_move_multiple_effect	= false;
		}

		// Animationを使うか設定
		ImGui::Checkbox("use_Animation", &use_Animation);
		if (use_Animation)
		{
			ImGuiAnimation();
			ImGuiPlayOptions();
			if (ImGui::Button("stop"))
			{
				is_now_playing = false;
				anim->setaFrame(0);

				{
					anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
					efc_roots.reset();
					if (emit_data.dataset.empty() == false)
					{
						efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
					}
				}
			}
			ImGui::SameLine();
			if (is_now_playing)
			{
				if (ImGui::Button("pause##EEdt_playbtn") || ButtonPressing(key[KEY_INPUT_SPACE], 28, 1))
					is_now_playing = !is_now_playing;
			}
			else
			{
				if (ImGui::Button("play ##EEdt_playbtn") || ButtonPressing(key[KEY_INPUT_SPACE], 28, 1))
					is_now_playing = !is_now_playing;
			}
			ImGui::SameLine();
			if (anim->animParam.empty() == false)
			{
				if (ImGui::SliderInt("aFrame", &anim_frame, 0, anim->animParam[nowanim].max_aFrame - 1))
				{
					aFrame_tmp = anim_frame;
					anim->setaFrame((char)aFrame_tmp);
				}
				if (ButtonPressing(key[KEY_INPUT_LEFT], 28, 14))
				{
					anim_frame -= anim_frame > 0 ? 1 : 0;
					aFrame_tmp = anim_frame;
					anim->setaFrame((char)aFrame_tmp);
				}
				if (ButtonPressing(key[KEY_INPUT_RIGHT], 28, 14))
				{
					int max_aFrame = (int)anim->animParam[nowanim].max_aFrame;
					anim_frame += anim_frame + 1 < max_aFrame ? 1 : 0;
					aFrame_tmp = anim_frame;
					anim->setaFrame((char)aFrame_tmp);
				}
			}
		}
		else
		{
			ImGuiPlayOptions();
			if (ImGui::Button("stop"))
			{
				is_now_playing = false;
				aFrame_tmp = anim_frame = 0;
				anim->setaFrame(0);

				{
					anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
					efc_roots.reset();
					if (emit_data.dataset.empty() == false)
					{
						efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
					}
				}
			}
			ImGui::SameLine();
			if (is_now_playing)
			{
				if (ImGui::Button("pause##EEdt_playbtn") || ButtonPressing(key[KEY_INPUT_SPACE], 28, 1))
					is_now_playing = !is_now_playing;
			}
			else
			{
				if (ImGui::Button("play ##EEdt_playbtn") || ButtonPressing(key[KEY_INPUT_SPACE], 28, 1))
					is_now_playing = !is_now_playing;
			}
			ImGui::SameLine();
			ImGui::SliderInt("anim_frame", &anim_frame, 0, 100);
			aFrame_tmp = anim_frame;
			if (ButtonPressing(key[KEY_INPUT_LEFT], 28, 14))
			{
				anim_frame -= anim_frame > 0 ? 1 : 0;
				aFrame_tmp = anim_frame;
			}
			if (ButtonPressing(key[KEY_INPUT_RIGHT], 28, 14))
			{
				int max_aFrame = (int)anim->animParam[nowanim].max_aFrame;
				anim_frame += anim_frame + 1 < max_aFrame ? 1 : 0;
				aFrame_tmp = anim_frame;
			}
		}

		if (is_now_playing == false)
		{
			if (ImGui::Button("Set Play Data"))
			{
				{
					anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
					efc_roots.reset();
					if (emit_data.dataset.empty() == false)
					{
						efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
					}
				}
			}
		}
		ImGui::SameLine();
		if (is_now_playing == false)
		{
			if (ImGui::Button("Reset Play Data"))
			{
				{ std::shared_ptr<EffectRoot>().swap(efc_roots); }
			}
		}

		if (is_now_playing == false)
		{
			if (use_Animation)
			{
				if (ImGui::Button("Fit to Animation data"))
				{
					FitEmitDataWithAnimationData();
				}
				ImGui::SameLine();
				ImGui::Text("WARN: When this button is clicked, previous emit_data will all cleared");
			}
		}

		// エミッターの数
		if (is_able_to_use_tools == false)
			ImGuiEmitterNum();

		// アニメーションの数
		if (is_able_to_use_tools == false)
			ImGuiAnimationNum();

		//キーフレームの設定
		ImGuiKeyFrame();

		// エフェクトの一覧
		if (is_able_to_use_tools == false)
			ImGuiEffectList();

		//emitterのパラメータ
		ImGuiEffectEmitter();

		//各種パラメータ
		ImGuiEffect();

		// エフェクトの複数選択
		ImGuiMultipleSelectEffects();

	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiImageViewer()
{
	ImGui::Begin("Effect_Images", nullptr, ImGuiWindowFlags_None | ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		ImGui::TextWrapped("Display Images");

		ImGuiIO& io = ImGui::GetIO();

		static int tex_x = 0;
		static int tex_y = 0;
		float my_tex_w = 0;
		float my_tex_h = 0;
		static ImTextureID my_tex_id = nullptr;
		static std::string filename = "";

		if (ImGui::Button("Open Images"))
		{
			std::vector<std::string> ghnd_path_tmp = GetOpenMultipleFileNameWithExplorer(true);
			for (auto& it_file : ghnd_path_tmp)
			{
				// 同じファイル名を探索
				auto& same_dir = std::find_if
				(
					ghnds_path.begin(),
					ghnds_path.end(),
					[&](std::string& _loaded_name)
				{
					return it_file == _loaded_name;
				}
				);

				// 無かった場合追加
				if (same_dir == ghnds_path.end())
				{
					int ghnd_tmp = LoadGraph(it_file.c_str());
					if (ghnd_tmp == -1) continue;
					ghnds.push_back(ghnd_tmp);
					ghnds_path.push_back(it_file);
				}
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("Close Images"))
		{
			std::vector<std::string> ghnd_path_tmp = GetOpenMultipleFileNameWithExplorer(true);
			auto&								it_file = ghnd_path_tmp.begin();
			std::vector<std::string>::iterator same_dir;
			for (; it_file != ghnd_path_tmp.end(); ++it_file)
			{
				// 同じファイル名を探索
				same_dir = std::find_if
				(
					ghnds_path.begin(),
					ghnds_path.end(),
					[&](std::string& _loaded_name)
				{
					return *it_file == _loaded_name;
				}
				);

				// 見つかった場合追加
				if (same_dir != ghnds_path.end())
				{
					size_t dist = std::distance(ghnds_path.begin(), same_dir);
					DeleteGraph(ghnds.at(dist));
					ghnds.erase(ghnds.begin() + dist);
					ghnds_path.erase(same_dir);
				}

			}
			filename = "no selected";
			tex_x = 0;
			tex_y = 0;
		}

		ImGui::NewLine();

		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode("Images List"))
		{
			ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
			ImGui::BeginChild("Child1", ImVec2(ImGui::GetWindowContentRegionWidth() - 16.f, 64.f * 3.f), false, window_flags);
			{
				int sameline = scasi(ImGui::GetWindowWidth() / (64.f + ImGui::GetItemsLineHeightWithSpacing()));
				for (int i = 0; i < (int)ghnds.size(); i++)
				{
					ImTextureID tex_id = (void *)(ghnds.at(i) + 1);
					float tex_x_tmpf = 0;
					float tex_y_tmpf = 0;
					int tex_x_tmp = 0;
					int tex_y_tmp = 0;
					GetGraphSize(ghnds.at(i), &tex_x_tmp, &tex_y_tmp);
					tex_x_tmpf = (float)tex_x_tmp;
					tex_y_tmpf = (float)tex_y_tmp;
					ImVec2 uv1 = { 1.f, GetRatio(tex_y_tmpf,tex_x_tmpf) };// 64:64 : 1:1 だと縦に伸びるのでuv1.yをyに対するxの比率に置き換える
					if (ImGui::ImageButton(tex_id, ImVec2(64, 64), ImVec2(0, 0), uv1, -1, ImColor(0, 0, 0, 255)))
					{
						my_tex_id = (void *)(ghnds.at(i) + 1);
						filename = ghnds_path.at(i);
						GetGraphSize(ghnds.at(i), &tex_x, &tex_y);
					}
					if (sameline == 0) sameline = 1;
					if (i % sameline < sameline - 1)
					{
						ImGui::SameLine();
					}
				}

			}
			ImGui::EndChild();

			ImGui::TreePop();
		}

		static float tex_ext_rate = 1.0f;
		my_tex_w = (float)tex_x;
		my_tex_h = (float)tex_y;
		my_tex_w *= tex_ext_rate;
		my_tex_h *= tex_ext_rate;

		ImGui::Text(filename.c_str());
		ImGui::SliderFloat("Display Size", &tex_ext_rate, 0.01f, 1.0f);
		ImGui::Text("Original Size: %dx%d", tex_x, tex_y);
		ImGui::Text("Display Size: %.0fx%.0f", my_tex_w, my_tex_h);
		ImVec2 pos = ImGui::GetCursorScreenPos();
		ImGui::Image(my_tex_id, ImVec2(my_tex_w, my_tex_h), ImVec2(0, 0), ImVec2(1, 1), ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 255));
		if (ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			float region_sz = 32.0f;
			float region_x = io.MousePos.x - pos.x - region_sz * 0.5f; if (region_x < 0.0f) region_x = 0.0f; else if (region_x > my_tex_w - region_sz) region_x = my_tex_w - region_sz;
			float region_y = io.MousePos.y - pos.y - region_sz * 0.5f; if (region_y < 0.0f) region_y = 0.0f; else if (region_y > my_tex_h - region_sz) region_y = my_tex_h - region_sz;
			float zoom = 4.0f;
			ImGui::Text("Min: (%.2f, %.2f)", region_x, region_y);
			ImGui::Text("Max: (%.2f, %.2f)", region_x + region_sz, region_y + region_sz);
			ImVec2 uv0 = ImVec2((region_x) / my_tex_w, (region_y) / my_tex_h);
			ImVec2 uv1 = ImVec2((region_x + region_sz) / my_tex_w, (region_y + region_sz) / my_tex_h);
			ImGui::Image(my_tex_id, ImVec2(region_sz * zoom, region_sz * zoom), uv0, uv1, ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));
			ImGui::EndTooltip();
		}
	}
	ImGui::End();
}

bool AnimationEffectEditor::ImGuiImagesList(const std::string& _hidden_label, int* _p_ghnd, std::string* _p_ghnd_path)
{
	bool is_image_button_pressed = false;
	ImGui::Text("Left click to set image");
	ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(("Images List##" + _hidden_label).c_str()))
	{
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_AlwaysHorizontalScrollbar | ImGuiWindowFlags_AlwaysVerticalScrollbar;
		ImGui::BeginChild(("ImagesListChild##" + _hidden_label).c_str(), ImVec2(ImGui::GetWindowContentRegionWidth() - 16.f, 64.f * 3.f), false, window_flags);
		{
			int sameline = scasi(ImGui::GetWindowWidth() / (64.f + ImGui::GetItemsLineHeightWithSpacing()));
			for (int i = 0; i < (int)ghnds.size(); i++)
			{
				ImTextureID tex_id	= (void*)(ghnds.at(i) + 1);
				float	tex_x_tmpf	= 0;
				float	tex_y_tmpf	= 0;
				int		tex_x_tmp	= 0;
				int		tex_y_tmp	= 0;
				GetGraphSize(ghnds.at(i), &tex_x_tmp, &tex_y_tmp);
						tex_x_tmpf	= (float)tex_x_tmp;
						tex_y_tmpf	= (float)tex_y_tmp;
				ImVec2	uv1			= { 1.f, GetRatio(tex_y_tmpf, tex_x_tmpf) };// 64:64 : 1:1 だと縦に伸びるのでuv1.yをyに対するxの比率に置き換える
				if (ImGui::ImageButton(tex_id, ImVec2(64, 64), ImVec2(0, 0), uv1, -1, ImColor(0, 0, 0, 255)))
				{
					if (_p_ghnd)		*_p_ghnd		= ghnds.at(i);
					if (_p_ghnd_path)	*_p_ghnd_path	= ghnds_path.at(i);
					is_image_button_pressed				= true;
				}
				if (sameline == 0) sameline = 1;
				if (i % sameline < sameline - 1) ImGui::SameLine();
			}

		}
		ImGui::EndChild();

		if (ImGui::Button("Unset Image"))
		{
			if (_p_ghnd)		*_p_ghnd		= -1;
			if (_p_ghnd_path)	*_p_ghnd_path	= "";
		}

		ImGui::TreePop();
	}

	return is_image_button_pressed;
}

void AnimationEffectEditor::ImGuiEmitterNum()
{
	// エミッターの数
	ImGui::Begin("Effect_Emitter Num", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode("Emitter Num"))
		{
			// Emitterを追加
			if (ImGui::Button("Add Emitter"))
			{
				emit_data.dataset.emplace_back();
				now_edt_emit_data = nullptr;
				now_edt_efcparam = nullptr;
				now_edt_efc_tmp = nullptr;
				now_anim = 0;
				nowanim = 0;
				if (now_edt_emit_data == nullptr)
				{
					now_edt_emit_data = &emit_data.dataset.at(now_edit_efcparam_num);
                }
				ImGui::TreePop();
				ImGui::End();
				return;
			}

			// Emitterがない場合存在し追加をするよう施すメッセージを出してreturn;
			if (!emit_data.dataset.empty())
			{
				// 存在する全てのEmitterを列挙
				{
					ImGui::BeginChild("Emitter List", ImVec2(ImGui::GetWindowContentRegionWidth(), 300), false,
						ImGuiWindowFlags_AlwaysVerticalScrollbar | ImGuiWindowFlags_HorizontalScrollbar);
					{
						int copy_emt_i = -1;
						for (int emt_i = 0; emt_i < (int)emit_data.dataset.size(); ++emt_i)
						{
							std::string name = "emitter: " + std::to_string(emt_i);
							if (ImGui::RadioButton(name.c_str(), &now_edit_emitparam_num, emt_i)) // emt_i番目のボタンが押された時 now_edit_emitparam_numに emt_iを代入
							{
								now_edt_emit_data = &emit_data.dataset.at(now_edit_emitparam_num);
								now_anim = 0;
								nowanim = 0;
							}
							ImGui::SameLine();
							// 複製
							name = "copy: " + std::to_string(emt_i);
							if (ImGui::Button(name.c_str()))
							{
								copy_emt_i = emt_i;
							}
							ImGui::SameLine();
							// Emitterを削除
							name = "erase: " + std::to_string(emt_i);
							if (ImGui::Button(name.c_str()))
							{
								if (now_edt_emit_data == &emit_data.dataset.at(emt_i))now_edt_emit_data = nullptr;
								emit_data.dataset.erase(emit_data.dataset.begin() + emt_i);
								now_edit_emitparam_num = emt_i - 1;
								if (now_edit_emitparam_num < 0)now_edit_emitparam_num = 0;
								now_edt_efcparam = nullptr;
								now_edt_efc_tmp = nullptr;
							}
						}
						if (copy_emt_i >= 0)
						{
							emit_data.dataset.push_back(emit_data.dataset.at(copy_emt_i));
							now_edt_emit_data = &emit_data.dataset.at(now_edit_emitparam_num);
							now_edt_efcparam = nullptr;
							now_edt_efc_tmp = nullptr;
							now_anim = 0;
							nowanim = 0;
						}
					}
					ImGui::EndChild();
				}
			}
			else
			{
				ImGui::Text("No emitter");
				ImGui::Text("Please Add Emitter");
			}

			ImGui::TreePop();
		}
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiAnimationNum()
{
	ImGui::Begin("Effect_Animation Num", nullptr, ImGuiWindowFlags_None);
	{
		if (emit_data.dataset.empty())
		{
			ImGui::Text("No emitter");
			ImGui::Text("Please add emitter first");
			ImGui::End();
			return;
		}

		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode("Animation Num"))
		{
			// アニメーションを追加 emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data == アニメーションの種類のvector
			if (ImGui::Button("Add Animation"))
			{
				emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.emplace_back();
				emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.emplace_back();
			}

			if (!emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.empty())
			{

				ImGui::Text("Animation List");
				ImGui::BeginChild("animation list", ImVec2(0.0f, 0.0f), false, ImGuiWindowFlags_AlwaysAutoResize);
				{
					int cnt = 0;
					for
						(
							auto& anm_it = emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.begin();
							anm_it != emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.end();
							++anm_it
						)
					{
						std::string name = "animation: " + std::to_string(cnt);
						if (ImGui::RadioButton(name.c_str(), &now_anim, cnt)) // i番目のボタンが押された時 now_animに iを代入
						{
							use_Animation = false;// use_Animationフラグをfalseにして主導権をもらう
							nowanim = now_anim;

							now_edt_emit_data = &emit_data.dataset.at(now_edit_emitparam_num);
						}

						ImGui::SameLine();

						// Animationを削除
						name = "erase: " + std::to_string(cnt);
						if (ImGui::Button(name.c_str()))
						{
							// now_edt_efcparam が消す要素のvector*を持っていた場合
							for (auto& it_find = anm_it->begin(); it_find != anm_it->end(); ++it_find)
							{
								if (&it_find->second == now_edt_efcparam)
								{
									now_edt_efcparam = nullptr;
									break;
								}
							}

							anm_it =	emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.erase(anm_it);
										emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.erase
										(
											emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.begin() + cnt
										);

							nowanim = now_anim = now_anim - 1;
							if (now_anim - 1 < 0)nowanim = now_anim = 0;

							if (anm_it == emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.end())
								break;
						}

						cnt++;
					}
				}
				ImGui::EndChild();
			}
			else
			{
				ImGui::Text("No animation");
				ImGui::Text("Please Add animation");
			}

			ImGui::TreePop();
		}
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiKeyFrame()
{
	ImGui::Begin("Effect_Key Frame", nullptr, ImGuiWindowFlags_None);
	{
		if (emit_data.dataset.empty())
		{
			ImGui::Text("No emitter");
			ImGui::Text("Please add emitter first");
			ImGui::Text("No animation");
			ImGui::Text("Please Add animation first");
			ImGui::End();
			return;
		}
		if (emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.empty())
		{
			ImGui::Text("No animation");
			ImGui::Text("Please Add animation first");
			ImGui::End();
			return;
		}

		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			bool state = false;

			ImGui::Text("Set Keyframe at this frame(%d)", aFrame_tmp);

			if (!emit_data.dataset.empty())
			{
				//同じフレームにキーが存在するか捜索
				if (!emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.empty())
				{
					auto& find_it			= emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).find(aFrame_tmp);
					auto& setonce_find_it	= emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.begin() + nowanim;

					if (find_it != emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).end())
					{
						state = true;// 存在した場合true
						now_edt_efcparam = &emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).at(aFrame_tmp);
					}
					else
					{
						state = false;
						now_edt_efcparam = nullptr;
						now_edt_efc_tmp = nullptr;
						now_edit_efcparam_num = 0;
					}

					ImGui::Text("state: %d", state);
					if (ImGui::Checkbox("SetKey", &state))
					{
						if (!state)// チェック後は反転するので
						{
							if (now_edt_efcparam == &find_it._Ptr->_Myval.second)
							{
								now_edt_efcparam = nullptr;
								now_edt_efc_tmp = nullptr;
							}
							emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).erase(find_it);//キーフレームを削除
							emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.erase(setonce_find_it);//キーフレームを削除
							now_edit_efcparam_num = 0;
						}
						else
						{
							emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).emplace(aFrame_tmp, std::vector<EffectParams>());
							now_edt_efcparam = &emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).at(aFrame_tmp);
							emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.emplace_back(false);
						}
					}
					ImGui::SameLine();
					ImGui::Checkbox("is_emit_once", &emit_data.dataset.at(now_edit_emitparam_num).is_emit_once.at(nowanim));
				}
				else
				{
					ImGui::Text("no effect in this frame");
				}
			}
			else
			{
				ImGui::Text("no emitter in this frame");
			}

			// キーフレームを列挙
			{
				ImGui::BeginChild("key list", ImVec2(0.0f, 0.0f), false, ImGuiWindowFlags_AlwaysAutoResize);
				for (auto& it_key : emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim))
				{
					std::string name = "goto key: " + std::to_string(it_key.first);
					if (ImGui::RadioButton(name.c_str(), &anim_frame, it_key.first))
					{
						anim_frame = it_key.first;
						aFrame_tmp = it_key.first;
						anim->setaFrame(aFrame_tmp);
					}
				}
				ImGui::EndChild();
			}
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiEffectList()
{
	ImGui::Begin("Effect_Effect list", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		if (emit_data.dataset.empty())
		{
			ImGui::Text("No emitter");
			ImGui::Text("Please add emitter first");
			ImGui::Text("No animation");
			ImGui::Text("Please Add animation first");
			ImGui::End();
			return;
		}
		if (emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.empty())
		{
			ImGui::Text("No animation");
			ImGui::Text("Please Add animation first");
			ImGui::End();
			return;
		}
		if (emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).empty())
		{
			ImGui::TextWrapped("emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).empty() == true");
			ImGui::End();
			return;
		}

		if (now_edt_emit_data == nullptr || now_edt_efcparam == nullptr || now_edt_efc_tmp == nullptr)
		{
			if (now_edt_emit_data == nullptr)
				ImGui::Text("now_edt_emit_data == nullptr");
			if (now_edt_efcparam == nullptr)
				ImGui::Text("now_edt_efcparam == nullptr");
			if (now_edt_efc_tmp == nullptr)
				ImGui::Text("now_edt_efc_tmp == nullptr");
		}
		else
		{
			// left
			ImGui::BeginChild("left pane", ImVec2(150, 0), true);
			for (int i = 0; i < (int)now_edt_efcparam->size(); ++i)
			{
				std::string name = "effect: " + std::to_string(i);
				if (ImGui::RadioButton(name.c_str(), &now_edit_efcparam_num, i))//i番目のボタンが押された時 now_edit_efcparam_numに iを代入
				{
					now_edt_efc_tmp = &now_edt_efcparam->at(now_edit_efcparam_num);
				}
				ImGui::SameLine();
				// 削除
				name = "erase: " + std::to_string(i);
				if (ImGui::Button(name.c_str()))
				{
					now_edt_efcparam->erase(now_edt_efcparam->begin() + i);
					now_edit_efcparam_num = i - 1;
					if (now_edit_efcparam_num < 0)
					{
						now_edit_efcparam_num = 0;
						now_edt_efc_tmp = nullptr;
					}
					else
						now_edt_efc_tmp = &now_edt_efcparam->at(now_edit_efcparam_num);
					break;
				}
			}
			ImGui::EndChild();
			ImGui::SameLine();

			// right
			ImGui::BeginGroup();
			ImGui::BeginChild("item view", ImVec2(0, -ImGui::GetFrameHeightWithSpacing())); // Leave room for 1 line below us
			ImGui::Text("effect: %d", now_edit_efcparam_num);
			ImGui::Separator();
			if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None))
			{
				if (ImGui::BeginTabItem("Description"))
				{
					if (now_edt_emit_data == nullptr || now_edt_efcparam == nullptr || now_edt_efc_tmp == nullptr)
					{
						if (now_edt_emit_data == nullptr)
							ImGui::Text("now_edt_emit_data == nullptr");
						if (now_edt_efcparam == nullptr)
							ImGui::Text("now_edt_efcparam == nullptr");
						if (now_edt_efc_tmp == nullptr)
							ImGui::Text("now_edt_efc_tmp == nullptr");
					}
					else
					{
						ImGui::Text("pos x: %f", now_edt_efc_tmp->pos.x);
						ImGui::Text("pos y: %f", now_edt_efc_tmp->pos.y);
						ImGui::Text("vel x: %f", now_edt_efc_tmp->vel.x);
						ImGui::Text("vel y: %f", now_edt_efc_tmp->vel.y);
						ImGui::Text("deceleration_rate_mul: %f", now_edt_efc_tmp->deceleration_rate_mul);
						ImGui::Text("gravity: %f", now_edt_efc_tmp->gravity);
						ImGui::Text("noise_scale: %f", now_edt_efc_tmp->noise_scale);
						ImGui::Text("curl_speed: %f", now_edt_efc_tmp->curl_speed);
						ImGui::Text("attract_vel.x: %f", now_edt_efc_tmp->attract_vel.x);
						ImGui::Text("attract_vel.y: %f", now_edt_efc_tmp->attract_vel.y);
						ImGui::Text("angle: %f", now_edt_efc_tmp->angle);
						ImGui::NewLine();
						ImGui::Text("draw");
						ImGui::Text("draw.srcx: %d", now_edt_efc_tmp->draw.srcx);
						ImGui::Text("draw.srcy: %d", now_edt_efc_tmp->draw.srcy);
						ImGui::Text("draw.anmx: %d", now_edt_efc_tmp->draw.anmx);
						ImGui::Text("draw.anmy: %d", now_edt_efc_tmp->draw.anmy);
						ImGui::Text("draw.cx: %d", now_edt_efc_tmp->draw.cx);
						ImGui::Text("draw.cy: %d", now_edt_efc_tmp->draw.cy);
						ImGui::Text("draw.interval: %d", now_edt_efc_tmp->draw.interval);
						ImGui::Text("draw.is_loop: %d", now_edt_efc_tmp->draw.is_loop);

						ImGui::Text("col.x(r): %.0f, ", now_edt_efc_tmp->col.target_col.x);
						ImGui::Text("col.y(g): %.0f, ", now_edt_efc_tmp->col.target_col.y);
						ImGui::Text("col.z(b): %.0f, ", now_edt_efc_tmp->col.target_col.z);
						ImGui::Text("col.w(a): %.0f"  , now_edt_efc_tmp->col.target_col.w);
					}
					ImGui::EndTabItem();
				}

				if (now_edt_emit_data == nullptr || now_edt_efcparam == nullptr || now_edt_efc_tmp == nullptr)
				{
					if (now_edt_emit_data == nullptr)
						ImGui::Text("now_edt_emit_data == nullptr");
					if (now_edt_efcparam == nullptr)
						ImGui::Text("now_edt_efcparam == nullptr");
					if (now_edt_efc_tmp == nullptr)
						ImGui::Text("now_edt_efc_tmp == nullptr");
				}
				else if (ImGui::BeginTabItem("Effect list"))
				{
					if (now_edt_emit_data == nullptr || now_edt_efcparam == nullptr)
					{
						if (now_edt_emit_data == nullptr)
							ImGui::Text("now_edt_emit_data == nullptr");
						if (now_edt_efcparam == nullptr)
							ImGui::Text("now_edt_efcparam == nullptr");
					}
					else
					{
						//ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
						//if (ImGui::TreeNode("list"))
						{
							if (now_edt_efcparam == nullptr)
							{
								ImGui::Text("the key is not setted");
							}
							else
							{
								if (ImGui::Button("Add Effect"))
								{
									now_edt_efcparam->emplace_back();
									// 以下のパラメータに反映するEffectParamsを代入
									now_edt_efc_tmp = &now_edt_efcparam->at(now_edit_efcparam_num);
								}
									static bool is_hold_pos = true;
									static bool is_hold_vel = true;
									ImGui::Checkbox("is_hold_pos", &is_hold_pos);
									ImGui::SameLine();
									ImGui::Checkbox("is_hold_vel", &is_hold_vel);
								if (ImGui::Button("Reset all EfcParams with EmitterParams"))
								{
									for (int i = 0; i < (int)now_edt_efcparam->size(); ++i)
									{

										Vector2 pos = {};
										Vector2 vel = {};
										if (is_hold_pos)pos = now_edt_efcparam->at(i).pos;
										if (is_hold_vel)vel = now_edt_efcparam->at(i).vel;
										EffectParams tmp = now_edt_emit_data->anim_emit_data.InheritFromEmitterParams(pos);
										tmp.vel += vel;
										now_edt_efcparam->at(i) = tmp;
									}
								}
								// 編集するエフェクトのvector選択
								{
									// そのキーフレーム中に存在する全てのEffectParamsを列挙
									ImGui::BeginChild("efc list", ImVec2(0.0f, 0.0f), false, ImGuiWindowFlags_AlwaysAutoResize);
									for (int i = 0; i < (int)now_edt_efcparam->size(); ++i)
									{
										std::string name = "effect: " + std::to_string(i);
										if (ImGui::RadioButton(name.c_str(), &now_edit_efcparam_num, i))//i番目のボタンが押された時 now_edit_efcparam_numに iを代入
										{
											now_edt_efc_tmp = &now_edt_efcparam->at(now_edit_efcparam_num);
											//now_edt_efcparam = &emit_data.dataset.at(now_edit_emitparam_num).anim_efc_data.at(nowanim).at(aFrame_tmp);
										}
									}
									ImGui::EndChild();
								}
							}

							//ImGui::TreePop();
						}
					}
					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}
			ImGui::EndChild();
			if (ImGui::Button("Revert")) {}
			ImGui::SameLine();
			if (ImGui::Button("Save")) {}
			ImGui::EndGroup();

		}

		
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiEffectEmitter()
{
	ImGui::Begin("Effect_Emitter Parameters", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		if (now_edt_emit_data == nullptr)
		{
			ImGui::Text("now_edt_emit_data == nullptr");
			ImGui::End();
			return;
		}
		//if (now_edt_efcparam == nullptr)
		//{
		//	ImGui::Text("now_edt_efcparam == nullptr");
		//	ImGui::End();
		//	return;
		//}

		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode("Emitter Parameters"))
		{
			//type
			ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
			if (ImGui::TreeNode("type##emitter"))
			{
				int* p_type = &now_edt_emit_data->anim_emit_data.type;
				if (ImGui::BeginCombo("type", efc_types[*p_type].c_str())) // コンボメニューに表示する文字
				{
					for (int n = 0; n < (int)efc_types.size(); n++)
					{
						bool is_selected = (*p_type == n);
						if (ImGui::Selectable(efc_types[n].c_str(), is_selected))
							* p_type = n;
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}

				ImGui::TreePop();
			}

			// pos
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("pos##emitter"))
			{
				ImGui::Text("emitter pos (Parent position)");
				ImGui::SliderAndDragFloat("emitter.pos.x", &now_edt_emit_data->anim_emit_data.pos.x, 1.f, -200.f, 200.f);
				ImGui::SliderAndDragFloat("emitter.pos.y", &now_edt_emit_data->anim_emit_data.pos.y, 1.f, -200.f, 200.f);
				ImGui::Checkbox("emitter.is_follow_to_root", &now_edt_emit_data->anim_emit_data.is_follow_to_root);

				ImGui::TreePop();
			}

			// vel
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("vel##emitter"))
			{
				ImGui::SliderAndDragFloat("emitter.vel.x", &now_edt_emit_data->anim_emit_data.vel.x, 1.f, -100.f, 100.f);
				ImGui::SliderAndDragFloat("emitter.vel.y", &now_edt_emit_data->anim_emit_data.vel.y, 1.f, -100.f, 100.f);

				ImGui::TreePop();
			}

			// deceleration_rate_mul
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("deceleration_rate_mul##emitter"))
			{
				ImGui::SliderAndDragFloat("emitter.deceleration_rate_mul", &now_edt_emit_data->anim_emit_data.deceleration_rate_mul, 0.01f, -1.f, 1.f);
				ImGui::NewLine();

				ImGui::TreePop();
			}

			// gravity
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("gravity##emitter"))
			{
				ImGui::SliderAndDragFloat("emitter.gravity", &now_edt_emit_data->anim_emit_data.gravity, 0.01f, -10.f, 10.f);
				ImGui::NewLine();

				ImGui::TreePop();
			}

			// duration
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("duration##emitter"))
			{
				ImGui::SliderAndDragFloat("emitter.set_speed_per_frame", &now_edt_emit_data->anim_emit_data.set_speed_per_frame, 1.f, -1.f, 120.f);
				if (ImGui::SliderAndDragFloat("emitter.duration", &now_edt_emit_data->anim_emit_data.duration, 1.f, 1.f, 1000.f))
				{
					now_edt_emit_data->anim_emit_data.duration_max = now_edt_emit_data->anim_emit_data.duration;
				}
				ImGui::NewLine();

				ImGui::SliderAndDragFloat("emitter.duration_subtraction_rate", &now_edt_emit_data->anim_emit_data.duration_subtraction_rate, 0.1f, -1.f, 100.f);
				ImGui::NewLine();

				ImGui::TreePop();
			}

			// 普段は1.0f
			// emit_num
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("emit_num##emitter"))
			{
				ImGui::SliderAndDragInt("emitter.emit_num", &now_edt_emit_data->anim_emit_data.emit_num, 1.f, 0, 256);
				ImGui::SliderAndDragInt("emitter.emit_num_randomize", &now_edt_emit_data->anim_emit_data.emit_num_randomize, 1.f, 0, 256);
				ImGui::NewLine();

				ImGui::TreePop();
			}

			// randomize
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("efc_randomize##emitter"))
			{
				// pos
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("efc_pos_randomize##emitter"))
				{
					ImGui::Text("x, y is minus value");
					ImGui::Text("z, w is plus value");
					ImGui::SliderAndDragFloat("emitter.efc_pos_randomize.min_x(x)", &now_edt_emit_data->anim_emit_data.efc_pos_randomize.x, 1.f, -0.f, -200.f);
					ImGui::SliderAndDragFloat("emitter.efc_pos_randomize.min_y(y)", &now_edt_emit_data->anim_emit_data.efc_pos_randomize.y, 1.f, -0.f, -200.f);
					ImGui::SliderAndDragFloat("emitter.efc_pos_randomize.max_x(z)", &now_edt_emit_data->anim_emit_data.efc_pos_randomize.z, 1.f, 0.f, 200.f);
					ImGui::SliderAndDragFloat("emitter.efc_pos_randomize.max_y(w)", &now_edt_emit_data->anim_emit_data.efc_pos_randomize.w, 1.f, 0.f, 200.f);

					ImGui::TreePop();
				}

				// vel
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("efc_vel_randomize##emitter"))
				{
					ImGui::Text("x, y is minus value");
					ImGui::Text("z, w is plus value");
					ImGui::SliderAndDragFloat("emitter.efc_vel_randomize.min_x(x)", &now_edt_emit_data->anim_emit_data.efc_vel_randomize.x, 1.f, -0.f, -10.f);
					ImGui::SliderAndDragFloat("emitter.efc_vel_randomize.min_y(y)", &now_edt_emit_data->anim_emit_data.efc_vel_randomize.y, 1.f, -0.f, -10.f);
					ImGui::SliderAndDragFloat("emitter.efc_vel_randomize.max_x(z)", &now_edt_emit_data->anim_emit_data.efc_vel_randomize.z, 1.f, 0.f, 10.f);
					ImGui::SliderAndDragFloat("emitter.efc_vel_randomize.max_y(w)", &now_edt_emit_data->anim_emit_data.efc_vel_randomize.w, 1.f, 0.f, 10.f);

					ImGui::TreePop();
				}
				ImGui::NewLine();

				// col
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("efc_color_randomize##emitter"))
				{
					ImGui::Text("+only");
					ImGui::SliderAndDragFloat("emitter.efc_color_randomize.x(r)", &now_edt_emit_data->anim_emit_data.efc_color_randomize.x, 1.f, 0.f, 255.f);
					ImGui::SliderAndDragFloat("emitter.efc_color_randomize.y(g)", &now_edt_emit_data->anim_emit_data.efc_color_randomize.y, 1.f, 0.f, 255.f);
					ImGui::SliderAndDragFloat("emitter.efc_color_randomize.z(b)", &now_edt_emit_data->anim_emit_data.efc_color_randomize.z, 1.f, 0.f, 255.f);
					ImGui::SliderAndDragFloat("emitter.efc_color_randomize.w(a)", &now_edt_emit_data->anim_emit_data.efc_color_randomize.w, 1.f, 0.f, 255.f);

					ImGui::TreePop();
				}
				ImGui::NewLine();

				// efc_angle_randomize
				ImGui::SliderAndDragFloat("emitter.efc_angle_randomize", &now_edt_emit_data->anim_emit_data.efc_angle_randomize, 1.f, -3600.f, 3600.f);
				ImGui::NewLine();

				// efc_angle_randomize
				ImGui::SliderAndDragFloat("emitter.efc_radius_randomize", &now_edt_emit_data->anim_emit_data.efc_radius_randomize, 1.f, 0.f, 256.f);
				ImGui::NewLine();

				// efc_duration_randomize
				ImGui::SliderAndDragFloat("emitter.efc_duration_randomize", &now_edt_emit_data->anim_emit_data.efc_duration_randomize, 1.f, -100.f, 100.f);
				ImGui::NewLine();

				// efc_deceleration_rate_mul_randomize 
				ImGui::SliderAndDragFloat("emitter.efc_deceleration_rate_mul_randomize", &now_edt_emit_data->anim_emit_data.efc_deceleration_rate_mul_randomize, 0.002f, -1.f, 1.f);
				ImGui::NewLine();

				// efc_noise_scale_randomize
				ImGui::SliderAndDragFloat("emitter.efc_noise_scale_randomize", &now_edt_emit_data->anim_emit_data.efc_noise_scale_randomize, 1.f, 0.f, 20.f);
				ImGui::NewLine();

				// efc_curl_speed_randomize
				ImGui::SliderAndDragFloat("emitter.efc_curl_speed_randomize", &now_edt_emit_data->anim_emit_data.efc_curl_speed_randomize, 0.01f, 0.f, 1.f);
				ImGui::NewLine();

				ImGui::TreePop();
			}

			// radius
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("radius##emitter"))
			{
				ImGui::SliderAndDragFloat("emitter.radius", &now_edt_emit_data->anim_emit_data.radius, 1.f, 0.f, 180.f);
				ImGui::SliderAndDragFloat
				(
					"emitter.efc.radius_radius_decreae_interval",
					&now_edt_emit_data->anim_emit_data.radius_decreae_interval,
					1.f,
					0.f, now_edt_emit_data->anim_emit_data.duration_max
				);

				ImGui::TreePop();
			}

			// image
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("graph_filename"))
			{
				std::string* path = &now_edt_emit_data->anim_emit_data.graph_filename;
				if (ImGuiImagesList("ImGuiEffeEmitterImage", &now_edt_emit_data->anim_emit_data.efc.draw.graph_hadle, path))
				{
					std::string	efc_dir = ".\\Data\\Images\\Effects\\";
					if (path->compare(efc_dir) == 1)
					{
						*path = GetExtractedFileNameFromPath(*path);
					}
				}

				ImGui::Text(now_edt_emit_data->anim_emit_data.graph_filename.c_str());

				int	tex_x = 0, tex_y = 0;
				ImVec2		size = { 0.f,0.f };
				ImTextureID imtexid = (void*)(now_edt_emit_data->anim_emit_data.efc.draw.graph_hadle + 1);
				int graph_size_result = GetGraphSize(now_edt_emit_data->anim_emit_data.efc.draw.graph_hadle, &tex_x, &tex_y);
				if (graph_size_result == 0)
				{
					float ratio = GetRatio(scasf(tex_x), ImGui::GetWindowWidth() - 64.f);
					size = { scasf(tex_x) * ratio, scasf(tex_y) * ratio };
				}

				ImGui::Image
				(
					imtexid,
					size,
					ImVec2(0, 0), ImVec2(1, 1),
					ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 255)
				);

				ImGui::TreePop();
			}

			// efc
			ImGuiTreeNodeEffectParams("emitter.efc", now_edt_emit_data->anim_emit_data.efc);

			ImGui::TreePop();
		}
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiEffect()
{
	ImGui::Begin("Effect_Effect Parameters", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		if (now_edt_emit_data == nullptr)
		{
			ImGui::Text("now_edt_emit_data == nullptr");
			ImGui::End();
			return;
		}
		if (now_edt_efcparam == nullptr)
		{
			ImGui::Text("now_edt_efcparam == nullptr");
			ImGui::End();
			return;
		}

		if (now_edt_efcparam->empty())
		{
			ImGui::Text("no effect");
			ImGui::End();
			return;
		}

		// 以下のパラメータに反映するEffectParamsを代入
		now_edt_efc_tmp = &now_edt_efcparam->at(now_edit_efcparam_num);

		ImGuiTreeNodeEffectParams("ImGuiEffect()", *now_edt_efc_tmp);
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiMultipleSelectEffects()
{
	if (!can_select_multiple_effect)return;
	ImGui::Begin("Effect_Multiple Select Effects", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		static EffectParams efc_tmp;
		static bool is_hold_pos = true;
		static bool is_hold_vel = true;
		static bool is_randomize = true;
		static bool is_realtime_update = false;

		static EmitterParams emt_tmp;
		{
			emt_tmp.deceleration_rate_mul = 0.f;
			emt_tmp.duration = emt_tmp.duration_max = 0.f;
			emt_tmp.radius = 0.f;
			emt_tmp.efc.col.col = Vector4(0.f, 0.f, 0.f, 0.f);
			emt_tmp.efc.col.target_col = Vector4(0.f, 0.f, 0.f, 0.f);
		}

		ImGui::Checkbox("is_realtime_update##MultipleSelectEffects", &is_realtime_update);
		ImGui::Checkbox("is_randomize##MultipleSelectEffects", &is_randomize);
		ImGui::SameLine();
		ImGui::Checkbox("is_hold_pos##MultipleSelectEffects", &is_hold_pos);
		ImGui::SameLine();
		ImGui::Checkbox("is_hold_vel##MultipleSelectEffects", &is_hold_vel);
		if (ImGui::Button("Set efc parameters") || is_realtime_update)
		{
			int ghnd = EFCDATA->GetEfcRefSafe(now_edt_emit_data->anim_emit_data.graph_filename);
			for (auto&& it_efc : now_selecting_efcs_tmp)
			{
				Vector2 pos = {};
				Vector2 vel = {};
				pos = now_edt_efcparam->at(it_efc).pos;
				vel = now_edt_efcparam->at(it_efc).vel;
				EffectParams tmp = efc_tmp;
				EffectParams tmp2;
				if (is_randomize)
				{
					emt_tmp.efc = efc_tmp;
					tmp2 = emt_tmp.InheritFromEmitterParams();
					{
						tmp.pos += tmp2.pos;
						tmp.vel += tmp2.vel;
						tmp.deceleration_rate_mul += tmp2.deceleration_rate_mul;
						tmp.duration += tmp2.duration;
						tmp.duration_max += tmp2.duration;
						tmp.radius += tmp2.radius;
						tmp.angle.val += tmp2.angle.val;
						tmp.angle.target_val += tmp2.angle.target_val;
						tmp.noise_scale += tmp2.noise_scale;
						tmp.curl_speed += tmp2.curl_speed;
						tmp.col.col.x += tmp2.col.col.x;
						tmp.col.target_col.x += tmp2.col.target_col.x;
						tmp.col.col.y += tmp2.col.col.y;
						tmp.col.target_col.y += tmp2.col.target_col.y;
						tmp.col.col.z += tmp2.col.col.z;
						tmp.col.target_col.z += tmp2.col.target_col.z;
						tmp.col.col.w += tmp2.col.col.w;
						tmp.col.target_col.w += tmp2.col.target_col.w;
					}
				}
				if (is_hold_pos)tmp.pos = pos + tmp2.pos;
				if (is_hold_vel)tmp.vel = vel + tmp2.vel;
				tmp.draw.graph_hadle = ghnd;
				now_edt_efcparam->at(it_efc) = tmp;
			}

			if (is_realtime_update)
			{
				anim_emit_dset = std::make_shared<ANIM_EMIT_DATASET>(emit_data);
				efc_roots.reset();
				if (emit_data.dataset.empty() == false)
				{
					efc_roots = make_shared<EffectRoot>(anim_emit_dset, *anim, false, pos);
				}
			}
		}
		if (ImGui::Button("Inherit emitter parameters"))
		{
			for (auto&& it_efc : now_selecting_efcs_tmp)
			{
				Vector2 pos = {};
				Vector2 vel = {};
				if (is_hold_pos)pos = now_edt_efcparam->at(it_efc).pos;
				if (is_hold_vel)vel = now_edt_efcparam->at(it_efc).vel;
				EffectParams tmp = now_edt_emit_data->anim_emit_data.InheritFromEmitterParams(pos);
				tmp.vel += vel;
				now_edt_efcparam->at(it_efc) = now_edt_emit_data->anim_emit_data.InheritFromEmitterParams();
			}
		}

		if (ImGui::Button("Delete parameters"))
		{
			auto it_efc = now_edt_efcparam->begin();
			for (int i = 0; i < now_selecting_efcs_tmp.size(); i++)
			{
				auto i1 = now_selecting_efcs_tmp.at(i); // 
				it_efc = now_edt_efcparam->begin() + i1;
				if (it_efc == now_edt_efcparam->begin() + now_edit_efcparam_num)
				{
					now_edt_efc_tmp = nullptr;
					now_edit_efcparam_num = 0;
				}
				now_edt_efcparam->erase(it_efc);

				for (int i2 = 0; i2 < now_selecting_efcs_tmp.size(); i2++)
				{
					if (now_selecting_efcs_tmp[i2] >= i1 && now_selecting_efcs_tmp[i2] > 0)
						now_selecting_efcs_tmp[i2] -= 1;
				}
			}
			now_selecting_efcs_tmp.clear();
		}

		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode("Effect Parameters##MultipleSelectEffects"))
		{
			// randomize
			ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
			if (ImGui::TreeNode("efc_randomize##MultipleSelectEffects"))
			{
				// pos
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("efc_pos_randomize##MultipleSelectEffects"))
				{
					ImGui::Text("x, y is minus value");
					ImGui::Text("z, w is plus value");
					ImGui::SliderAndDragFloat("efc_pos_randomize.min_x(x)##MultipleSelectEffects", &emt_tmp.efc_pos_randomize.x, 1.f, -200.f, -0.f);
					ImGui::SliderAndDragFloat("efc_pos_randomize.min_y(y)##MultipleSelectEffects", &emt_tmp.efc_pos_randomize.y, 1.f, -200.f, -0.f);
					ImGui::SliderAndDragFloat("efc_pos_randomize.max_x(z)##MultipleSelectEffects", &emt_tmp.efc_pos_randomize.z, 1.f, 0.f, 200.f);
					ImGui::SliderAndDragFloat("efc_pos_randomize.max_y(w)##MultipleSelectEffects", &emt_tmp.efc_pos_randomize.w, 1.f, 0.f, 200.f);

					ImGui::TreePop();
				}

				// vel
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("efc_vel_randomize##MultipleSelectEffects"))
				{
					ImGui::Text("x, y is minus value");
					ImGui::Text("z, w is plus value");
					ImGui::SliderAndDragFloat("efc_vel_randomize.min_x(x)##MultipleSelectEffects", &emt_tmp.efc_vel_randomize.x, 1.f, -10.f, 0.f);
					ImGui::SliderAndDragFloat("efc_vel_randomize.min_y(y)##MultipleSelectEffects", &emt_tmp.efc_vel_randomize.y, 1.f, -10.f, 0.f);
					ImGui::SliderAndDragFloat("efc_vel_randomize.max_x(z)##MultipleSelectEffects", &emt_tmp.efc_vel_randomize.z, 1.f, 0.f, 10.f);
					ImGui::SliderAndDragFloat("efc_vel_randomize.max_y(w)##MultipleSelectEffects", &emt_tmp.efc_vel_randomize.w, 1.f, 0.f, 10.f);

					ImGui::TreePop();
				}

				// col
				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("efc_color_randomize##MultipleSelectEffects"))
				{
					ImGui::Text("+only");
					ImGui::SliderAndDragFloat("efc_color_randomize.x(r)##MultipleSelectEffects", &emt_tmp.efc_color_randomize.x, 1.f, 0.f, 255.f);
					ImGui::SliderAndDragFloat("efc_color_randomize.y(g)##MultipleSelectEffects", &emt_tmp.efc_color_randomize.y, 1.f, 0.f, 255.f);
					ImGui::SliderAndDragFloat("efc_color_randomize.z(b)##MultipleSelectEffects", &emt_tmp.efc_color_randomize.z, 1.f, 0.f, 255.f);
					ImGui::SliderAndDragFloat("efc_color_randomize.w(a)##MultipleSelectEffects", &emt_tmp.efc_color_randomize.w, 1.f, 0.f, 255.f);

					ImGui::TreePop();
				}

				ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
				if (ImGui::TreeNode("other_param##MultipleSelectEffects"))
				{
					// efc_angle_randomize
					ImGui::SliderAndDragFloat("efc_angle_randomize##MultipleSelectEffects", &emt_tmp.efc_angle_randomize, 1.f, -3600.f, 3600.f);
					ImGui::NewLine();

					// efc_angle_randomize
					ImGui::SliderAndDragFloat("efc_radius_randomize##MultipleSelectEffects", &emt_tmp.efc_radius_randomize, 1.f, 0.f, 256.f);
					ImGui::NewLine();

					// efc_duration_randomize
					ImGui::SliderAndDragFloat("efc_duration_randomize##MultipleSelectEffects", &emt_tmp.efc_duration_randomize, 1.f, -100.f, 100.f);
					ImGui::NewLine();

					// efc_deceleration_rate_mul_randomize 
					ImGui::SliderAndDragFloat("efc_deceleration_rate_mul_randomize##MultipleSelectEffects", &emt_tmp.efc_deceleration_rate_mul_randomize, 0.002f, -1.f, 1.f);
					ImGui::NewLine();

					// efc_noise_scale_randomize
					ImGui::SliderAndDragFloat("efc_noise_scale_randomize##MultipleSelectEffects", &emt_tmp.efc_noise_scale_randomize, 1.f, 0.f, 20.f);
					ImGui::NewLine();

					// efc_curl_speed_randomize
					ImGui::SliderAndDragFloat("efc_curl_speed_randomize##MultipleSelectEffects", &emt_tmp.efc_curl_speed_randomize, 0.01f, 0.f, 1.f);

					ImGui::TreePop();
				}

				ImGui::TreePop();
			}

			ImGuiTreeNodeEffectParams("MultipleSelectEffects", efc_tmp);

			ImGui::TreePop();
		}
	}
	ImGui::End();
}

void AnimationEffectEditor::GetMultipleSelectEffects()
{
	if (!can_select_multiple_effect)
	{
		now_selecting_efcs_tmp.clear();
		return;
	}

	if (ImGui::IsMouseHoveringAnyWindow()) return;

	// 追加
	if (pMOUSE->left_ref().is_release)
	{
		Vector2 drag_pos_min = Vector2
		(
			(float)pMOUSE->left_ref().clicked_pos_log_x,
			(float)pMOUSE->left_ref().clicked_pos_log_y
		);
		Vector2 drag_pos_max = Vector2
		(
			(float)pMOUSE->pos_x_ref(),
			(float)pMOUSE->pos_y_ref()
		);
		drag_pos_min = cam.GetWorldPosFromScreenPos(drag_pos_min);
		drag_pos_max = cam.GetWorldPosFromScreenPos(drag_pos_max);

		int counter = 0;
		for (auto& it_efc : *now_edt_efcparam)
		{
			if (Coll::AABB(drag_pos_min, drag_pos_max, it_efc.pos + pos, it_efc.pos + pos + Vector2(1.f, 1.f)))
			{
				auto it_find = std::find_if
				(
					now_selecting_efcs_tmp.begin(),
					now_selecting_efcs_tmp.end(),
					[counter](const int& _selected_num)
					{
						return _selected_num == counter;
					}
				);
				if (it_find == now_selecting_efcs_tmp.end())// 同じ番号が無かった場合追加する
					now_selecting_efcs_tmp.push_back(counter);
			}
			counter++;
		}
	}
	// 除外
	else if (pMOUSE->right_ref().is_release)
	{
		Vector2 drag_pos_min = Vector2
		(
			(float)pMOUSE->right_ref().clicked_pos_log_x,
			(float)pMOUSE->right_ref().clicked_pos_log_y
		);
		Vector2 drag_pos_max = Vector2
		(
			(float)pMOUSE->pos_x_ref(),
			(float)pMOUSE->pos_y_ref()
		);
		drag_pos_min = cam.GetWorldPosFromScreenPos(drag_pos_min);
		drag_pos_max = cam.GetWorldPosFromScreenPos(drag_pos_max);

		int counter = 0;
		for (auto it_efc : *now_edt_efcparam)
		{
			if (Coll::AABB(drag_pos_min, drag_pos_max, it_efc.pos + pos, it_efc.pos + pos + Vector2(1.f, 1.f)))
			{
				auto it_find = std::find_if
				(
					now_selecting_efcs_tmp.begin(),
					now_selecting_efcs_tmp.end(),
					[counter](const int& _val)
					{
						return _val == counter;
					}
				);

				if (it_find != now_selecting_efcs_tmp.end())
					now_selecting_efcs_tmp.erase(it_find);
			}
			counter++;
		}
	}
}

void AnimationEffectEditor::ImGuiTreeNodeEffectParams(const std::string& _hidden_label, EffectParams& _params)
{
	ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(("EffectParams##" + _hidden_label).c_str()))
	{
		//type
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("type##" + _hidden_label).c_str()))
		{
			if (ImGui::BeginCombo(("type##combo" + _hidden_label).c_str(), efc_types[_params.type].c_str())) // コンボメニューに表示する文字
			{
				for (int n = 0; n < (int)efc_types.size(); n++)
				{
					bool is_selected = (_params.type == n); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(efc_types[n].c_str(), is_selected))
						_params.type = n;
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}
			ImGui::NewLine();

			ImGui::Checkbox(("is_attract_to_parent_emt##" + _hidden_label).c_str(), &_params.is_attract_to_parent_emt);

			ImGui::TreePop();
		}

		//primitive_type
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("primitive_type##" + _hidden_label).c_str()))
		{
			int* p_prim_rype = &_params.primitive_type;
			if (ImGui::BeginCombo(("primitive_type##combo" + _hidden_label).c_str(), efc_prim_types[*p_prim_rype].c_str())) // コンボメニューに表示する文字
			{
				for (int n = 0; n < (int)efc_prim_types.size(); n++)
				{
					bool is_selected = (*p_prim_rype == n);
					if (ImGui::Selectable(efc_prim_types[n].c_str(), is_selected))
						* p_prim_rype = n;
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
			ImGui::NewLine();
			//is_draw_line
			ImGui::Checkbox(("is_draw_line##" + _hidden_label).c_str(), &_params.is_draw_line);

			ImGui::TreePop();
		}

		// blend_mode
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("blend_mode##" + _hidden_label).c_str()))
		{
			if (ImGui::BeginCombo(("blend_mode##combo" + _hidden_label).c_str(), blend_modes[_params.blend_mode].second.c_str())) // コンボメニューに表示する文字
			{
				for (int blend_select_i = 0; blend_select_i < (int)blend_modes.size(); blend_select_i++)
				{
					bool is_selected;
					is_selected = (blend_modes[_params.blend_mode].first == blend_select_i); // You can store your selection however you want, outside or inside your objects

					if (ImGui::Selectable(blend_modes[blend_select_i].second.c_str(), is_selected))
					{
						_params.blend_mode = blend_select_i;
					}

					if (is_selected)
					{
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
					}
				}
				ImGui::EndCombo();
			}

			ImGui::SliderAndDragFloat
			(
				("blend_fade_interval##" + _hidden_label).c_str(),
				&_params.blend_fade_interval,
				0.f,
				1.f, _params.duration_max
			);
			ImGui::SliderAndDragFloat
			(
				("blend_decreae_interval##" + _hidden_label).c_str(),
				&_params.blend_decreae_interval,
				0.f,
				1.f, _params.duration_max - 1.f
			);

			// blend_mode_ease
			int* p_ease_algo = &_params.blend_mode_ease;
			if (ImGui::BeginCombo(("blend_mode_ease##combo" + _hidden_label).c_str(), Easing::algo_names[*p_ease_algo].c_str())) // コンボメニューに表示する文字
				{
					for (int n = 0; n < (int)Easing::algo_names.size(); n++)
					{
						bool is_selected = (*p_ease_algo == n);
						if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
							* p_ease_algo = n;
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}

			ImGui::TreePop();
		}

		// pos
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("pos##" + _hidden_label).c_str()))
		{
			ImGui::SliderAndDragFloat(("pos.x##" + _hidden_label).c_str(), &_params.pos.x, 1.f, -200.f, 200.f);
			ImGui::SliderAndDragFloat(("pos.y##" + _hidden_label).c_str(), &_params.pos.y, 1.f, -200.f, 200.f);
			ImGui::NewLine();

			ImGui::TreePop();
		}

		// vel
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("vel##" + _hidden_label).c_str()))
		{
			ImGui::SliderAndDragFloat(("vel.x##" + _hidden_label).c_str(), &_params.vel.x, 1.f, -30.f, 30.f);
			ImGui::SliderAndDragFloat(("vel.y##" + _hidden_label).c_str(), &_params.vel.y, 1.f, -30.f, 30.f);
			ImGui::NewLine();

			ImGui::TreePop();
		}

		// deceleration_rate_mul
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("deceleration_rate_mul##" + _hidden_label).c_str()))
		{
			ImGui::SliderAndDragFloat(("deceleration_rate_mul##" + _hidden_label).c_str(), &_params.deceleration_rate_mul, 0.01f, -1.f, 1.f);
			ImGui::NewLine();

			ImGui::TreePop();
		}

		// duration
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("duration##" + _hidden_label).c_str()))
		{
			if (ImGui::SliderAndDragFloat(("duration##" + _hidden_label).c_str(), &_params.duration, 1.f, 1.f, 1000.f))
			{
				_params.duration_max = _params.duration;
			}
			ImGui::NewLine();

			ImGui::TreePop();
		}

		// radius
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("radius##" + _hidden_label).c_str()))
		{
			ImGui::SliderAndDragFloat(("radius##" + _hidden_label).c_str(), &_params.radius, 1.f, 0.f, 180.f);

			ImGui::SliderAndDragFloat
			(
				("radius_radius_fade_interval##" + _hidden_label).c_str(),
				&_params.radius_fade_interval,
				1.f,
				0.f, _params.duration_max
			);
			ImGui::SliderAndDragFloat
			(
				("radius_radius_decreae_interval##" + _hidden_label).c_str(),
				&_params.radius_decreae_interval,
				0.f,
				1.f, _params.duration_max
			);

			//ease algo
			{
				int* p_ease_algo = &_params.easing_algo;
				ImGui::Text("Ease algo");
				if (ImGui::BeginCombo(("easing_algo##combo" + _hidden_label).c_str(), Easing::algo_names[*p_ease_algo].c_str())) // コンボメニューに表示する文字
				{
					for (int n = 0; n < (int)Easing::algo_names.size(); n++)
					{
						bool is_selected = (*p_ease_algo == n);
						if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
							* p_ease_algo = n;
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}
			}

			ImGui::TreePop();
		}

		// gravity
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("gravity##" + _hidden_label).c_str()))
		{
			ImGui::SliderAndDragFloat(("gravity##" + _hidden_label).c_str(), &_params.gravity, 0.01f, -10.f, 10.f);
			ImGui::NewLine();

			ImGui::TreePop();
		}

		// noise
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("noise##" + _hidden_label).c_str()))
		{
			// is_use_curl_noise
			ImGui::Checkbox(("is_use_curl_noise##" + _hidden_label).c_str(), &_params.is_use_curl_noise);

			// noise_scale
			ImGui::SliderAndDragFloat(("noise_scale##" + _hidden_label).c_str(), &_params.noise_scale, 1.f, 1.f, 20.f);
			ImGui::NewLine();

			// curl_speed
			ImGui::SliderAndDragFloat(("curl_speed##" + _hidden_label).c_str(), &_params.curl_speed, 0.01f, 0.f, 1.f);
			ImGui::NewLine();
			ImGui::TreePop();
		}

		// angle
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("angle##" + _hidden_label).c_str()))
		{
			// angle type
			if (ImGui::TreeNode(("angle_type##" + _hidden_label).c_str()))
			{
				if (ImGui::BeginCombo(("angle_type##combo" + _hidden_label).c_str(), efc_angle_types[_params.angle_type].c_str())) // コンボメニューに表示する文字
				{
					for (int n = 0; n < (int)efc_angle_types.size(); n++)
					{
						bool is_selected = (_params.angle_type == n);
						if (ImGui::Selectable(efc_angle_types[n].c_str(), is_selected))
							_params.angle_type = n;
						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}
				ImGui::NewLine();

				ImGui::TreePop();
			}

			ImGui::SliderAndDragFloat(("angle##" + _hidden_label).c_str(), &_params.angle.target_val, 0.1f, -3600.f, 3600.f);
			_params.angle.ImGuiTreeNode(("angle_delay_move##" + _hidden_label).c_str());
			ImGui::NewLine();
			ImGui::TreePop();
		}

		//col
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("color##" + _hidden_label).c_str()))
		{
			_params.col.ImGuiTreeNode(("color##" + _hidden_label).c_str());
			ImGui::TreePop();
		}

		// draw
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(("draw##" + _hidden_label).c_str()))
		{
			// src
			ImGui::Text("src");
			ImGui::SliderAndDragInt(("draw.srcx##" + _hidden_label).c_str(), &_params.draw.srcx, 1, 0, 1024);
			ImGui::SliderAndDragInt(("draw.srcy##" + _hidden_label).c_str(), &_params.draw.srcy, 1, 0, 1024);
			ImGui::NewLine();

			// anm
			ImGui::Text("anm");
			ImGui::SliderAndDragInt(("draw.anmx##" + _hidden_label).c_str(), &_params.draw.anmx, 1, 0, 32);
			ImGui::SliderAndDragInt(("draw.anmy##" + _hidden_label).c_str(), &_params.draw.anmy, 1, 0, 32);
			ImGui::NewLine();

			// texpos
			ImGui::Text("tex begin pos");
			ImGui::SliderAndDragInt(("draw.texx##" + _hidden_label).c_str(), &_params.draw.texx, 1, 0, 6400);
			ImGui::SliderAndDragInt(("draw.texy##" + _hidden_label).c_str(), &_params.draw.texy, 1, 0, 6400);
			ImGui::NewLine();

			// center
			ImGui::Text("center");
			ImGui::SliderAndDragInt(("draw.cx##" + _hidden_label).c_str(), &_params.draw.cx, 1, 0, 1024);
			ImGui::SliderAndDragInt(("draw.cy##" + _hidden_label).c_str(), &_params.draw.cy, 1, 0, 1024);
			ImGui::NewLine();

			// interval
			ImGui::Text("interval");
			ImGui::SliderAndDragInt(("draw.interval##" + _hidden_label).c_str(), &_params.draw.interval, 1, 1, 1200);
			ImGui::NewLine();

			// is_loop
			ImGui::Text("is_loop");
			ImGui::Checkbox(("draw.is_loop##" + _hidden_label).c_str(), &_params.draw.is_loop);
			ImGui::NewLine();

			ImGui::TreePop();
		}

		ImGui::TreePop();
	}
}

void AnimationEffectEditor::ImGuiAnimation()
{
	ImGui::Begin("Animation", nullptr, ImGuiWindowFlags_None);
	{
		ImGui::BeginChild("Animation Description", ImVec2(ImGui::GetWindowContentRegionWidth(), 300), false, 
			ImGuiWindowFlags_AlwaysVerticalScrollbar | ImGuiWindowFlags_HorizontalScrollbar);
		{
			if (anim->animParam.empty())
			{
				ImGui::Text("no animation");
			}
			else
			{
				ImGui::Text("now_anim: %d", anim->now_anim);
				ImGui::Text("isAnimation: %d", (int)anim->isAnimation);
				ImGui::Text("aFrame: %d", (int)anim->getaFrame());
				ImGui::Text("tex_pos.x: %.2f", anim->tex_pos.x);
				ImGui::Text("tex_pos.y: %.2f", anim->tex_pos.y);
				ImGui::Text("tex_size.x: %d", anim->tex_size.x);
				ImGui::Text("tex_size.y: %d", anim->tex_size.y);

				if (ImGui::BeginTabBar("anim_param##animtab", ImGuiTabBarFlags_None))
				{
					int aparam_i = 0;
					for (const auto& it_anim_param : anim->animParam)
					{
						if (ImGui::BeginTabItem(std::string("animParam[" + std::to_string(aparam_i) + "]").c_str()))
						{
							ImGui::Text("max_aFrame: %d", it_anim_param.max_aFrame);
							ImGui::Text("animSpeed: %d", it_anim_param.animSpeed);
							ImGui::Text("isLoop: %d", it_anim_param.isLoop);

							ImGui::EndTabItem();
						}

						aparam_i++;
					}

					ImGui::EndTabBar();
				}
			}
		}
		ImGui::EndChild();

		ImGui::Text("Animation text");
		if (ImGui::Button("Open"))
		{
			std::string anim_txt_path;
			anim_txt_path = GetOpenFileNameWithExplorer(true, ".txt");
			if (!anim_txt_path.empty())
			{
				if (anim == false)anim = std::make_shared<Animation>(Animation());
				*anim.get() = Animation();

				anim_txt_path = GetExtractedFileNameFromPath(anim_txt_path, false);
				anim->loadAnimFile(anim_txt_path);
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("Reset"))
		{
			*anim.get() = Animation();
		}

		if (!anim->animParam.empty())
		{
			int now_anim_tmp = (int)anim->now_anim;
			if (ImGui::SliderInt("anim->now_anim", &now_anim_tmp, 0, anim->animParam.size() - 1))
			{
				anim->changeAnimation(now_anim_tmp);
				anim->setaFrame(0);
			}

				aFrame_tmp = anim_frame = anim->getaFrame();
			if (aFrame_tmp < 0)
				aFrame_tmp = 0;
				nowanim = now_anim = anim->now_anim;
		}
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiPlayOptions()
{
	ImGui::Begin("Play Options", nullptr, ImGuiWindowFlags_None);
	{
		// image
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode("graph_handle"))
		{
			static bool is_auto_reload_animation_txt = false;
			if (use_Animation)ImGui::Checkbox("Animation txet auto reload", &is_auto_reload_animation_txt);

			if (ImGuiImagesList("PlayOptions", &ghnd, &ghnd_path))
			{
				if (use_Animation && is_auto_reload_animation_txt)
				{
					std::string anim_txt_path = ghnd_path;
					if (anim == false)anim = std::make_shared<Animation>(Animation());
					*anim.get() = Animation();
					anim->loadAnimFile(GetExtractedFileNameFromPath(anim_txt_path, false));
				}
			}

			ImGui::Text("graph_filename: "); ImGui::SameLine(); ImGui::Text(ghnd_path.c_str());
			ImTextureID imtexid = (void*)(ghnd + 1);
			ImVec2		size = { 0.f,0.f };
			int			tex_x = 0, tex_y = 0;

			if (GetGraphSize(ghnd, &tex_x, &tex_y) == 0)
			{
				size = {
				(ImGui::GetWindowWidth() - 64.f),
				scasf(tex_y) * GetRatio(scasf(tex_x),ImGui::GetWindowWidth() - 64.f) };
			}

			ImGui::Image
			(
				imtexid,
				size,
				ImVec2(0, 0), ImVec2(1, 1),
				ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 255)
			);

			ImGui::TreePop();
		}

		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode("play_params"))
		{
			// src
			ImGui::Text("src");
			ImGui::SliderAndDragInt("play_param.srcx", &play_param.srcx, 1, 0, 1024);
			ImGui::SliderAndDragInt("play_param.srcy", &play_param.srcy, 1, 0, 1024);
			ImGui::NewLine();

			// anm
			ImGui::Text("anm");
			ImGui::SliderAndDragInt("play_param.anmx", &play_param.anmx, 1, 0, 32);
			ImGui::SliderAndDragInt("play_param.anmy", &play_param.anmy, 1, 0, 32);
			ImGui::NewLine();

			// center
			ImGui::Text("center");
			ImGui::SliderAndDragInt("play_param.cx", &play_param.cx, 1, 0, 1024);
			ImGui::SliderAndDragInt("play_param.cy", &play_param.cy, 1, 0, 1024);
			ImGui::NewLine();

			// interval
			ImGui::Text("interval");
			ImGui::SliderAndDragInt("play_param.interval", &play_param.interval, 1, 1, 60);
			ImGui::NewLine();

			// is_loop
			ImGui::Text("is_loop");
			ImGui::Checkbox("play_param.is_loop", &play_param.is_loop);
			ImGui::NewLine();

			ImGui::TreePop();
		}
	}
	ImGui::End();
}

void AnimationEffectEditor::ImGuiGetPixelColor(const int& _ghnd)
{
	ImGui::Begin("GetPixelColor", nullptr, ImGuiWindowFlags_None);
	{
		static bool is_able_to_getpixel = false;
		static float col[4] = {};

		ImGui::Checkbox("is_able_to_getpixel", &is_able_to_getpixel);

		if (is_able_to_getpixel)
		{
			int prev_ghnd = GetDrawScreen();
			SetDrawScreen(_ghnd);
			{
				ImGui::Text("left click to get color at mouse position");
				if (ImGui::IsMouseHoveringAnyWindow() == false)
				{
					if (pMOUSE->left_ref().pressing_count == 1)
					{
						Vector2 cursor_pos = { 0,0 };/* = cam.GetCursorPosRelatedZoom();*/
						cursor_pos += cam.GetWorldPosFromCursorPos();
						cursor_pos.x = floorf(cursor_pos.x);
						cursor_pos.y = floorf(cursor_pos.y);

						unsigned int coli = GetPixel(cam.xi_ext(cursor_pos.x), cam.yi_ext(cursor_pos.y));

						//各色要素を返す
						col[0] = scasf((coli >> CHAR_BIT * 2) & 0x000000ff);
						col[1] = scasf((coli >> CHAR_BIT * 1) & 0x000000ff);
						col[2] = scasf((coli >> CHAR_BIT * 0) & 0x000000ff);
						col[3] = scasf((coli >> CHAR_BIT * 3) & 0x000000ff);

						is_able_to_getpixel = false;
					}
				}
			}
			SetDrawScreen(prev_ghnd);
		}

		FitTo(col[0], 255.f, 1.f, 0.f);
		FitTo(col[1], 255.f, 1.f, 0.f);
		FitTo(col[2], 255.f, 1.f, 0.f);
		FitTo(col[3], 255.f, 1.f, 0.f);

		if (ImGui::ColorPicker4("emitter.efc.draw.col", col, ImGuiColorEditFlags__OptionsDefault))
		{
			FitTo(col[0], 1.f, 255.f, 0.f);
			FitTo(col[1], 1.f, 255.f, 0.f);
			FitTo(col[2], 1.f, 255.f, 0.f);
			FitTo(col[3], 1.f, 255.f, 0.f);
		}
		else
		{
			FitTo(col[0], 1.f, 255.f, 0.f);
			FitTo(col[1], 1.f, 255.f, 0.f);
			FitTo(col[2], 1.f, 255.f, 0.f);
			FitTo(col[3], 1.f, 255.f, 0.f);
		}
	}
	ImGui::End();
}
 
void AnimationEffectEditor::SaveAnimEmitDataset(const std::string & _filename, const int& _archive_type)
{
	switch (_archive_type)
	{
	case AnimationEffectEditor::BINARY:
	{
		std::ofstream ofs;
		ofs.open(_filename, std::ios::binary);

		cereal::BinaryOutputArchive o_archive(ofs);
		o_archive(cereal::make_nvp(_filename, anim_emit_dset));

		break;
	}
	case AnimationEffectEditor::JSON:
	{
		std::ofstream ofs;
		ofs.open(_filename, std::ios::binary);

		cereal::JSONOutputArchive o_archive(ofs);
		o_archive(cereal::make_nvp(_filename, anim_emit_dset));

		break;
	}
	case AnimationEffectEditor::XML:
	{
		std::ofstream ofs;
		ofs.open(_filename, std::ios::binary);

		cereal::XMLOutputArchive o_archive(ofs);
		o_archive(cereal::make_nvp(_filename, anim_emit_dset));

		break;
	}
	default:
		break;
	}
}

void AnimationEffectEditor::LoadAnimEmitDataset(const std::string & _filename, const int& _archive_type)
{
	{
		efc_states					= 0;
		is_now_playing				= false;
		is_able_to_use_tools		= false;
		can_move_multiple_effect	= false;
		can_select_multiple_effect	= false;
		can_set_effect				= false;
		is_inherit_emt				= true;

		anim_num	= 0;
		now_anim	= 0;
		nowanim		= 0;
		anim_frame	= 0;
		aFrame_tmp	= 0;

		is_now_playing = false;

		can_set_effect = false;

		now_edt_emit_data = nullptr;
		now_edt_efcparam = nullptr;
		now_edt_efc_tmp = nullptr;
	}

	std::shared_ptr<ANIM_EMIT_DATASET> anim_emit_dset_tmp;
	switch (_archive_type)
	{
	case AnimationEffectEditor::BINARY:
	{
		std::ifstream ifs;
		ifs.open(_filename, std::ios::binary);

		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(cereal::make_nvp(_filename, anim_emit_dset_tmp));

		break;
	}
	case AnimationEffectEditor::JSON:
	{
		std::ifstream ifs;
		ifs.open(_filename, std::ios::binary);

		cereal::JSONInputArchive i_archive(ifs);
		i_archive(cereal::make_nvp(_filename, anim_emit_dset_tmp));

		break;
	}
	case AnimationEffectEditor::XML:
	{
		std::ifstream ifs;
		ifs.open(_filename, std::ios::binary);

		cereal::XMLInputArchive i_archive(ifs);
		i_archive(cereal::make_nvp(_filename, anim_emit_dset_tmp));

		break;
	}
	default:
		break;
	}

	// load
	anim_emit_dset.reset();
	anim_emit_dset = std::move(anim_emit_dset_tmp);
	{ std::vector<ANIM_EMIT_DATA>().swap(emit_data.dataset); }
	emit_data = *anim_emit_dset;

	//for (auto& it_emit : emit_data.dataset) // 存在する emitter全てにイテレート
	//{
	//	for (auto& it_efc : it_emit.anim_efc_data)// アニメーションの数
	//	{
	//		it_emit.is_emit_once.emplace_back(false);// animParam分 emplace
	//	}
	//}
}

namespace ImGui
{
	bool SliderAndDragFloat
	(
		const std::string&	_label,
		float*				_v,
		const float&		_v_speed_drag,
		const float&		_v_min,
		const float&		_v_max,
		const std::string&	_format,
		const float&		_power
	)
	{
		bool slide_result, drag_result;
		slide_result = SliderFloat(_label.c_str(), _v, _v_min, _v_max, _format.c_str(), _power);
		drag_result = DragFloat((_label + " drag").c_str(), _v, _v_speed_drag, _v_min, _v_max, _format.c_str(), _power);

		return slide_result || drag_result;
	}

	bool SliderAndDragInt
	(
		const std::string&	_label,
		int*				_v,
		const float&		_v_speed_drag,
		const int&			_v_min,
		const int&			_v_max,
		const char*			_format
	)
	{
		bool slide_result, drag_result;
		slide_result = SliderInt(_label.c_str(), _v, _v_min, _v_max, _format);
		drag_result = DragInt((_label + " drag").c_str(), _v, _v_speed_drag, _v_min, _v_max, _format);

		return slide_result || drag_result;
	}
}
