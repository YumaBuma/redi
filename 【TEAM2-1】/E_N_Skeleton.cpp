#include "Enemy.h"
#include "Mo2_BG.h"
#include "ObjectManager.h"
#include "Texture.h"
#include "Sound.h"
#include "EffectManager.h"

int E_N_Skeleton::personal_counter = 0;
int E_N_Skeleton::attackPoint = 0;
int E_N_Skeleton::max_hp = 0;
int E_N_Skeleton::intarval = 0;
int E_N_Skeleton::attackTimer = 0;
float E_N_Skeleton::range[eRange::end] = { 0 };
float E_N_Skeleton::walkSpeed = 0;
float E_N_Skeleton::jumpPower = 0;
Vector2 E_N_Skeleton::attackPos = { 0,0 };
Vector2 E_N_Skeleton::attackSize = { 0,0 };

void E_N_Skeleton::loadData()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/nSkeleton_info.txt");

	fopen_s(&file, fileName, "r");

	for (int i = 0; i < eRange::end; i++)
	{

		fscanf_s(file, "%f", &range[i]);
	}



	fscanf_s(file, "%f", &walkSpeed);
	fscanf_s(file, "%d", &attackPoint);
	fscanf_s(file, "%d", &max_hp);
	fscanf_s(file, "%d", &intarval);
	fscanf_s(file, "%d", &attackTimer);
	fscanf_s(file, "%f", &attackPos.x);
	fscanf_s(file, "%f", &attackPos.y);
	fscanf_s(file, "%f", &attackSize.x);
	fscanf_s(file, "%f", &attackSize.y);

	fclose(file);
	//TODO::HP倍率
	if (pBG->stageNumber == 3)
	{
		max_hp *= 4;
	}

}


E_N_Skeleton::E_N_Skeleton()
{
	id.CLASS = E_N_SKELETON;
	id.PERSONAL = personal_counter;
	personal_counter++;

}

E_N_Skeleton::~E_N_Skeleton()
{
	personal_counter--;
	EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("E_N_Skeleton_dead.bin"), false, pos);
	if (pObjManager->ObjList.empty() == false)
	{
		Vector2* player_pos_ptr = &pObjManager->ObjList[Object::PLAYER].back()->pos;
		for (int i_jew = 0; i_jew < money / 3/*(各敵のジュエリー発生量)*/; i_jew++)
		{
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("jewerly.bin"), false, pos)->Update();
		}
	}
}

void E_N_Skeleton::Init()
{
	plAttackNum = 0;
	nowState = 0;
	noDamageTimer = 0;
	isDamage = false;
	stayTimer = 0;
	speed = { 0,0 };
	MAX_SPEED.x = 10.f;
	MAX_SPEED.y = 15.f;
	INITIAL_SPEED.x = 2.f;
	INITIAL_SPEED.y = 0.f;
	act_interval = 0;
	notFound = 500;
	checkSpan = 10;
	walkCount = 0;
	attackState = 0;
	isStay = false;
	isGrip = false;
	isErase = false;
	isAnim = true;
	isDamage = false;
	isStay = false;
	isAttack = false;
	isFind = false;

	onGround = false;
	size = { 30,70 };
	loadAnimFile("E_SkeletonKnight");
	hp = max_hp;
	maxHP = max_hp;
	temp_hp = hp;
}

bool E_N_Skeleton::Update()
{
	if (Iwin_flg.objBG || Iwin_flg.enemy) return true;

	if (cam.IsInCameraFoot(pos, Vector2((float)tex_size.x * 0.5f, (float)tex_size.y)) == false)
	{
		hide();
		return true;
	}
	else
		show();

	if (noDamageTimer)
	{
		noDamageTimer--;
	}
	else
	{

		plAttackNum = 0;
	}

	searchAlg(pObjManager->distanceToPlayer(*this));

	if (nowState == -1)
	{
		m_isVisible = false;
		return true;
	}
	else
		m_isVisible = true;

	if (temp_hp > hp)
		temp_hp -= 2.0f;

	Gravity();

	PosUpdateY();

	checkAreaY();

	Behavior(pObjManager->distanceToPlayer(*this));

	PosUpdateX();


	checkAreaX();

	animUpdate();
	return 1;
}

void E_N_Skeleton::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

#if ENABLE_SMALL_DRAW // 縮小表示への対応
	if (noDamageTimer)
		SetDrawBright(255, 0, 0);
	//SetDrawBlendMode(DX_BLENDMODE_ADD, 200);

	DrawRectRotaGraphFast2
	(
		cam.xi_ext(pos.x - tex_size.x * 0.5f),
		cam.yi_ext(pos.y - tex_size.y),
		scasi(tex_pos.x) + tex_size.x * aFrame,
		scasi(tex_pos.y) + tex_size.y * now_anim,
		tex_size.x,
		tex_size.y,
		0, 0,
		cam.chipextrate_d(),
		0.0,
		pTexture->getGraphs(Texture::ENEMY),
		TRUE, isFlipX
	);
	SetDrawBright(255, 255, 255);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);


#else
	DrawRectGraph(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
		tex_pos.x + tex_size.x * aFrame,
		tex_pos.y + tex_size.y * now_anim,
		tex_size.x, tex_size.y, pTexture->getGraphs(Texture::ENEMY), TRUE, isFlipX);
#endif
	float distPos = pos.x;
	if (isFlipX)
		distPos -= attackPos.x;
	else
		distPos += attackPos.x;
	if (show_debug)
		DrawLineBox
		(
			cam.xi_ext(distPos - attackSize.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y - attackPos.y - attackSize.y)/* - camera_pos.y*/,
			cam.xi_ext(distPos + attackSize.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y - attackPos.y) /*- camera_pos.y*/,
			GetColor(255, 5, 80));
}

void E_N_Skeleton::walkAlg(const Object* _pl)
{
	if (!onGround)return;

	if (_pl)
	{
		if (isStay)
		{
			if ((isFlipX && pos.x > _pl->pos.x) || (!isFlipX && pos.x < _pl->pos.x))
			{
				changeAnimation(nSkeletonAnim::stay);

				return;
			}
			else
				isStay = false;
		}
		else
			changeAnimation(nSkeletonAnim::walk);

	}
	else
	{
		isStay = false;
		changeAnimation(nSkeletonAnim::walk);
	}
	if (_pl == nullptr)
	{
		walkCount = 0;
	}

	else
	{
		walkCount++;

		isFlipX = moveDirection(_pl->pos);
	}

	if (isFlipX)
	{
		speed.x = -walkSpeed;
	}
	else
	{
		speed.x = walkSpeed;
	}
}

void E_N_Skeleton::searchAlg(float _dist)
{
	static Vector2 plPos = { 0,0 };
	plPos = pObjManager->ObjList[PLAYER].back()->pos;

	if (isAttack)return;

	if (_dist == -1)
	{
		nowState = -1;
		m_isVisible = false;
		return;
	}

	if (stayTimer)
	{
		nowState = 0;
		return;
	}

	if (isFlipX && pos.x + size.x * 2 > plPos.x)
		isFind = true;
	else if (!isFlipX && pos.x - size.x * 2 < plPos.x)
		isFind = true;
	else
		isFind = false;

	if ((pos.y >= plPos.y && pos.y - size.y <= plPos.y) && isFind)
	{
		if (_dist <= range[eRange::attack])
			nowState = 3;
		else if (_dist <= range[eRange::track])
			nowState = 2;
	}
	else if (_dist <= range[eRange::move])
		nowState = 1;
	else
		nowState = -1;
}

void E_N_Skeleton::Gravity()
{
	onGround = false;

	speed.y += GRAVITY;
	if (speed.y >= MAX_SPEED.y)
	{
		speed.y = MAX_SPEED.y;
	}
}

void E_N_Skeleton::checkAreaX()
{
	if (delta.x < 0)
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			mapHoseiRight(this);
			if (!walkCount)
			{
				isStay = false;
				isFlipX = true;
			}
			else isStay = true;
		}
	}

	if (delta.x > 0)
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			mapHoseiLeft(this);
			if (!walkCount)
			{
				isStay = false;
				isFlipX = false;
			}
			else isStay = true;
		}

	}

}

void E_N_Skeleton::checkAreaY()
{
	if (delta.y > 0)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			mapHoseiUp(this);
		}
	}
	if (delta.y < 0)
	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}

		if (isUpperFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}
	}

	if (onGround)
	{
		if (isFlipX && speed.x < 0)
		{
			if ((isFloor(pos.x, pos.y+1, size.x*0.5f) || (isUpperFloor(pos.x, pos.y+1, size.x*0.5f))) && isNone(pos.x - 1, pos.y + 1, size.x*0.5f))
			{
				isFlipX = false;
			}

		}
		else if (!isFlipX && speed.x > 0)
		{
			if ((isFloor(pos.x, pos.y+1, size.x*0.5f) || (isUpperFloor(pos.x, pos.y+1, size.x*0.5f))) && isNone(pos.x + 1, pos.y + 1, size.x*0.5f))
			{
				isFlipX = true;
			}

		}

	}

}

void E_N_Skeleton::animUpdate()
{
	playAnimation();

}



void E_N_Skeleton::Behavior(float _dist)
{
	if (isAttack && isDamage)
	{
		
		isDamage = false;
	}
	if (noHitTime)
	{
		noHitTime--;
		isDamage = false;
	}
	if (isDamage)
	{
		knockBackEnm();
		changeAnimation(nSkeletonAnim::damage);
		attackState = 0;
		return;
	}
	if (!onGround)
	{
		return;
	}

	switch (nowState)
	{
	case -1:
		break;
	case 0:
		speed = { 0,0 };
		changeAnimation(nSkeletonAnim::stay);
		if (stayTimer > 0)
		{
			stayTimer--;
		}
		break;

	case 1:
		walkAlg(nullptr);
		break;

	case 2:
		walkAlg(pObjManager->ObjList[Object::PLAYER].begin()->get());
		break;

	case 3:
		Attack(_dist);
		break;

	}

}

void E_N_Skeleton::Attack(float _dist)
{
	speed.x = 0;

	switch (attackState)
	{
	case 0:
		changeAnimation(nSkeletonAnim::attack);
		isFlipX = moveDirection(pObjManager->ObjList[Object::PLAYER].back()->pos);
		isAttack = true;
		attackState++;
		break;
	case 1:
		if (aFrame == 3)
		{
			stopTimer = attackTimer;
			attackState++;
			stop_anim = true;
		}
		break;
	case 2:
		if (stopTimer)
			stopTimer--;
		else
		{
			pSound->playSE(Sound::SE_E_N_Skel, false);
			stop_anim = false;
			attackState++;
		}
		break;
	case 3:
		if (aFrame == 8)
		{
			stayTimer = intarval;
			isAttack = false;
			attackState = 0;
		}
		break;
	}
	//if (stayTimer)
	//{
	//	changeAnimation(nSkeletonAnim::stay);
	//	stayTimer--;
	//	return;
	//}

	//if (now_anim != nSkeletonAnim::attack)
	//{
	//	isAction = true;
	//	changeAnimation(nSkeletonAnim::attack);
	//	isFlipX = moveDirection(pObjManager->ObjList[Object::PLAYER].back()->pos);
	//}
	//else if (aFrame == 8)
	//{
	//	if (_dist <= range[eRange::attack])
	//	{
	//		aFrame = 0;
	//		aCnt = 0;
	//	}
	//	else
	//		isAction = false;
	//	stayTimer = intarval;

}

Vector2 E_N_Skeleton::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		if (now_anim == nSkeletonAnim::attack && (aFrame > 3 && aFrame < 8))
		{
			if (!isFlipX)
				return Vector2(attackPos.x, attackPos.y);
			else
				return Vector2(-attackPos.x, attackPos.y);

		}
		else
			return Vector2(0, 0);
		break;

	case 1:
		if (now_anim == nSkeletonAnim::attack && (aFrame > 3 && aFrame < 8))
		{
			return attackSize;
		}
		else
			return Vector2(-1, -1);

		break;
	case 2://xは攻撃力を返す(仮)
		return Vector2(attackPoint, plAttackNum);

	}
}

void E_N_Skeleton::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/nSkeleton_info.txt");

	fopen_s(&file, fileName, "w");

	for (int i = 0; i < eRange::end; i++)
	{

		fprintf_s(file, "%f ", range[i]);
	}

	fprintf_s(file, "%f\n", walkSpeed);
	fprintf_s(file, "%d\n", attackPoint);
	fprintf_s(file, "%d\n", max_hp);
	fprintf_s(file, "%d\n", intarval);
	fprintf_s(file, "%d\n", attackTimer);
	fprintf_s(file, "%f\n", attackPos.x);
	fprintf_s(file, "%f\n", attackPos.y);
	fprintf_s(file, "%f\n", attackSize.x);
	fprintf_s(file, "%f\n", attackSize.y);

	fclose(file);

}

void E_N_Skeleton::eImGui()
{
	if (Iwin_flg.e_info[Enemy_ID::E_N_SKELETON] == false)
		return;

	ImGui::Begin("nSkeleton", &Iwin_flg.e_info[Enemy_ID::E_N_SKELETON], ImGuiWindowFlags_MenuBar);

	static const char* rKind[3] =
	{ "move","track","attack" };

	for (int i = 0; i < 3; i++)
	{
		ImGui::DragFloat(rKind[i], &range[i]);
	}
	ImGui::DragFloat("walkSpeed", &walkSpeed);
	ImGui::DragInt("AttackPoint", &attackPoint);
	ImGui::DragInt("maxHP", &max_hp);
	ImGui::DragInt("intarval", &intarval);
	ImGui::DragInt("attackTimer", &attackTimer);

	ImGui::Text("attackArea");
	ImGui::Text("pos");
	ImGui::DragFloat("x##pos", &attackPos.x);
	ImGui::DragFloat("y##pos", &attackPos.y);
	ImGui::Text("size");
	ImGui::DragFloat("x##size", &attackSize.x);
	ImGui::DragFloat("y##size", &attackSize.y);

	ImGui::NewLine();

	if (ImGui::Button("save"))
	{
		saveFile();
	}


	ImGui::End();
}
