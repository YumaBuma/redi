#pragma once
#include "ObjectManager.h"
#include "Object.h"
#include "EffectManager.h"
#include "Sound.h"
#include "Weapon.h"

#include <cereal\cereal.hpp>
#include <cereal\archives\binary.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\archives\xml.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\types\string.hpp>

#define NODAMAGE 100
#define NOMOVE 10
//#define NOHIT		(120)

#define INIT_MAX_PL_HP (1000)

struct ATK
{
	int power;
	int cnt;//attack number
	int time;//attack time
	int next;//whether to continue next attack
	INT2 move_begin;//attack reaction timme
	INT2 move_end;//attack reaction timme
	Vector2 SPEED;//attack reaction
	int hitTime_begin;//attack reaction timme
	int hitTime_end;//attack reaction timme
	Vector2 size;//hit box size
	Vector2 margin;//hit box position

	ATK()
	{
		power = 0;
		cnt = 0;
		time = 0;
		next = 0;
		move_begin = { 0,0 };
		move_end = { 0,0 };
		SPEED = { 0.f,0.f };
		hitTime_begin = 0;
		hitTime_end = 0;
		size = { 0.f,0.f };
		margin = { 0.f,0.f };
	}
	ATK(int _power, int _cnt, int _time, int _next, INT2 _move_begin, INT2 _move_end,
		Vector2 _speed, int _hit_begin, int _hit_end, Vector2 _size, Vector2 _margin) :
		power(_power), cnt(_cnt), time(_time), next(_next), 
		move_begin(_move_begin), move_end(_move_end), SPEED(_speed),hitTime_begin(_hit_begin), hitTime_end(_hit_end), size(_size), margin(_margin) {}

	template<class T>
	void serialize(T& archive)
	{
		archive(
			cereal::base_class<ATK>(this), //上位クラスのシリアライズ関数を実行出来る
			cereal::make_nvp("ATK_POWER", power),
			cereal::make_nvp("ATK_CNT", cnt),
			cereal::make_nvp("ATK_TIME", time),
			cereal::make_nvp("NEXT_ATK_TIME", next),
			cereal::make_nvp("MOVE_TIME_X", move_t.x),
			cereal::make_nvp("MOVE_TIME_Y", move_t.y),
			cereal::make_nvp("ATK_SPEED_X", SPEED.x),
			cereal::make_nvp("ATK_SPEED_X", SPEED.y),
			cereal::make_nvp("ATK_BOX_X", size.x),
			cereal::make_nvp("ATK_BOX_Y", size.y),
			cereal::make_nvp("ATK_MARGIN_X", margin.x),
			cereal::make_nvp("ATK_MARGIN_Y", margin.y)
			);
	}
};
//CEREAL_REGISTER_TYPE(ATK);


class Player : public Object
{
private:
	static int personal_counter;
public:
	int jumpTimer;
	int MAX_JTIMER;
	int MAX_JCOUNT;
	int MAX_AIRJTIMER;
	int MAX_DOWNJTIMER;
	float JUMP_SPEED_Y;
	float GRAVITY;

public:
	int step;
	int MAX_STEP;
	int STEP_MARGIN;
	float STEP_SPEED_X;

public:
	bool isCrouch;
	int max_hp;
	int hp;
	WEAPON weapon;
	WEAPON weaponX;
	WEAPON weaponY;
	ATK atk;
	ATK atkX;
	ATK atkY;

	int jumpCnt;
	bool air_Jump;
	char down_jump;
	bool mode_ladder;

public:
	bool debug_box;
	float prev_col[4];
	int fontcolor[4];
	int fontedgecolor[4];
public:
	enum WEAPON_ID
	{
		L_SWORD = 0,
		SPEAR,
		LONGBOW,

		MAX_WEPON_NUM,
	};

	std::vector<Animation> pl_atk_anim;
	
private:
	int system_saved;

private:
	enum eAnmEfcsAtk : int { ATKANM_P_LargeSword = 0, ATKANM_P_Spear, ATKANM_P_Bow, ATKANM_P_Hummer, ATKANM_P_Tomahawk					, ATKANM_END };
	std::vector<std::shared_ptr<EffectRoot>>	anmefc_atk_roots;

	enum eAnmEfcsEye : int { EYEANM_P_LargeSword = 0, EYEANM_P_Spear, EYEANM_P_Bow, EYEANM_P_Hummer, EYEANM_P_Tomahawk, EYEANM_player	, EYEANM_END };
	std::vector<std::shared_ptr<EffectRoot>>	anmefc_eye_roots;

public:
	Player();
	~Player() 
	{
		personal_counter--;

		for (auto& it : anmefc_atk_roots)
		{
			if (it)it->SetIsStaticEffect(false);
			if (it)it->ClearEmitters();
		}
		for (auto& it : anmefc_eye_roots)
		{
			if (it)it->SetIsStaticEffect(false);
			if (it)it->ClearEmitters();
		}
	};
	
public:
	void Init();
	bool Update();
	void Draw() const;
	void ImGui();

public:
	int getHP(int _val)
	{
		switch (_val)
		{
		case 0:return hp;
		case 1:return max_hp;
		case 2:return weaponX.id;
		default:
			break;
		}
	}
	void addHP(int _addhp)
	{
		hp += _addhp;
		if (hp > max_hp)
		{
			hp = max_hp;
		}
	}

	int getStep(int _val)
	{
		switch (_val)
		{
		case 0:
			return step;
			break;
		default:
			break;
		}
	}

	int getNoHitTime()
	{
		return noHitTime;
	}

	Vector2 getATK(int _val)
	{
		switch (_val)
		{
		case 0:return atk.size;
		case 1:return atk.margin;
		case 2:return Vector2(atk.power, 0);
		case 3:return Vector2(atk.cnt, atk.time);
		default:
			break;
		}
	}

	void setDamage(int _noHitTime, int _damage, Vector2 _patarn)
	{
		noHitTime = _noHitTime;
		hp -= _damage;
		isDamage = true;
		pSound->playSE(pSound->SE_PL_DAMAGE, false);
	}

public:
	void DrawUpdate();
	void Move();
	void Gravity();
	void Jump();
	void Ladder();
	void Attack();
	void Step();
	void pickoutWeapon();

	void AttackUpdate();
	void RectUpdate();//ポリモーフィズム

	void checkAreaX();
	void checkAreaY();

	bool SaveSystemData();
	bool LoadSystemData();

	template<class T>
	void serialize(T &archive/*, const std::uint32_t _version*/)
	{
		archive(
			//cereal::base_class<Object>(this),
			cereal::make_nvp("INITIAL_SPEED.x##Player", INITIAL_SPEED.x),
			cereal::make_nvp("INITIAL_SPEED.y##Player", INITIAL_SPEED.y),
			cereal::make_nvp("MAX_SPEED.x##Player", MAX_SPEED.x),
			cereal::make_nvp("MAX_SPEED.y##Player", MAX_SPEED.y),
			cereal::make_nvp("GRAVITY##Player", GRAVITY),
			cereal::make_nvp("JUMP_SPEED_Y##Player", JUMP_SPEED_Y),
			cereal::make_nvp("MAX_DOWNJTIMER##Player", MAX_DOWNJTIMER),
			cereal::make_nvp("MAX_JCOUNT##Player", MAX_JCOUNT),
			cereal::make_nvp("MAX_AIRJTIMER##Player", MAX_AIRJTIMER),
			cereal::make_nvp("MAX_STEP##Player", MAX_STEP),
			cereal::make_nvp("STEP_SPEED_X##Player", STEP_SPEED_X),
			cereal::make_nvp("size,x##Player", size.x),
			cereal::make_nvp("size.y##Player", size.y),
			cereal::make_nvp("max_hp##Player", max_hp),
			cereal::make_nvp("hp##Player", hp),
			cereal::make_nvp("atk##Player", atk)
			);
	}
};
//CEREAL_REGISTER_TYPE(1, Player);
