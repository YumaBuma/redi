#pragma once

#include "common.h"
#include "Input.h"
#include "Mo2_BG.h"
#include "Texture.h"
#include "Sound.h"
#include "Camera.h"
#include "EffectManager.h"
#include "Object.h"

enum bgObjectGroup
{
	Lamp,
	WoodBox1,
	WoodBox2,
	WoodBoxSet,
	Needle,
	Door,
	Spiderweb,
	Sand,
	Ladder,
	Wall,
	Grass,
	NetWork,
	Grave,
	TreasureBox,
	ShopMaster,
	Goal,
	Start,
	Teleport,
	ELEVATOR_G,
	Trap,
};

enum lampType
{
	animLamp,
	lamp1,
	lamp2,
	lamp3,
	lamp4,
};

enum woodBoxType
{
	smallWoodBox1,
	smallWoodBox2,
	smallWoodBox3,
	bigWoodBox1,
	bigWoodBox2,
	bigWoodBox3,
	smallBarrel,
	bigBarrel,
};

enum woodBoxSet
{
	set1,
	set2,
	set3,
	set4,
	set5,
	set6,
	set7,
	set8,
	set9,
	set10,
	set11,
};

enum needleType
{
	u_needle,
	r_needle,
	d_needle,
	l_needle,
};

enum doorType
{
	door,
};

enum spiderWebType
{
	smallSpiderWeb1,
	smallSpiderWeb2,
	smallSpiderWeb3,
	smallSpiderWeb4,
	midiumSpiderWeb1,
	midiumSpiderWeb2,
	bigSpiderWeb1,
	bigSpiderWeb2,
	bigSpiderWeb3,
	bigSpiderWeb4,
};

enum sandType
{
	sand1,
	sabd2,
	sand3,
	sand4,
	sand5,
	sand6,

};

enum ladderType
{
	u_ladder,
	g_ladder,
	marge_ladder,
};

enum wallType
{
	fillWall_under,
	fillWall_ground,
	holeWall1,
	holeWall2,
	holeWall3,
	holeWall4,
};

enum grassType
{
	grass1,
	grass2,
	grass3,
	grass4,
	grass5,
	grass6,
};

enum netType
{
	net1,
	net2,
};

enum GraveType
{
	grave1,
	grave2,
	grave3,
	grave4,
	grave5,
	grave6,
	grave7,
	grave8,
	grave9,
};

enum TreasureBox
{
	bronds,
	silver,
	gorld,
};

enum shopType
{
	shopMen,
};

enum GoalType
{
	groundGoal,
	underGoal,
};

enum StartType
{
	groundStart,
	underStart,
};

enum teleportType
{
	teleporter,
};

enum elevatorType
{
	rope,
	elevator,
	lever,
	stopper,
};

enum trapType
{
	axe,
	moveFloor,
};

class BGObject : public Object
{
public:
	BGObject() {}
	~BGObject() = default;
	void Init();
	bool Update();
	void Draw() const;
	void ImGui();


private:
};

class Obstacles : public Object
{
public:
	Obstacles() {}
	~Obstacles() = default;
	void Init();
	bool Update();
	void IndiUpdate();
	void DoorUpdate();
	void TrapUpdate();
	void TereporterUpdate();
	void elevatorUpdate();
	void startUpdate();
	void setInfo();
	Vector2 getATK(int _val);
	void Draw() const;
	void ImGui();
	static void obImGui();
	static void saveFile();
	static void loadFile();

private:
	int timer;
	bool animFin;
	int attackPoint;
	Vector2 colPos;
	Vector2 colSize;
	Vector2 breakPos;
	Vector2 breakSize;
	static int ATK[2];
	static Vector2 BreakPos[3];
	static Vector2 BreakSize[3];
	static Vector2 distPos[9][5];
	static Vector2 ColSize[9][5];
};
