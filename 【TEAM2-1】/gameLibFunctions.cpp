#include "gameLib.h"
#include "GameLibFunctions.h"

//library//
void startUpLibrary(LPCTSTR caption, HINSTANCE instance, int width, int height, bool isFullscreen, int iconNum, double frameRate)
{
	GameLib::init(caption, instance, width, height, iconNum, isFullscreen, frameRate);
}
void startUpLibrary(LPCTSTR caption, HINSTANCE instance, DirectX::XMINT2 screenSize, bool isFullscreen, int iconNum, double frameRate)
{
	GameLib::init(caption, instance, screenSize.x, screenSize.y, iconNum, isFullscreen, frameRate);
}

void shutDownLibrary()
{
	GameLib::uninit();
}

DirectX::XMFLOAT2 getWindowSize()
{
	return GameLib::getWindowSize();
}

bool gameLoop(bool isShowFrameRate)
{
	GameLib::gameLoop(isShowFrameRate);
}

//ClearWindow//
void clearWindow(const float r, const float g, const float b)
{
	GameLib::clear({ r,g,b,1.0f });
}


//BLENDMODE//

void setBlendMode_NONE(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::NONE,alpha);
}

void setBlendMode_ALPHA(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::ALPHA,alpha);
}

void setBlendMode_ADD(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::ADD,alpha);
}

void setBlendMode_SUBTRACT(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::SUBTRACT,alpha);
}

void setBlendMode_REPLACE(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::REPLACE,alpha);
}

void setBlendMode_MULTIPLY(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::MULTIPLY,alpha);
}

void setBlendMode_LIGHTEN(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::LIGHTEN,alpha);
}

void setBlendMode_DARKEN(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::DARKEN,alpha);
}

void setBlendMode_SCREEN(const float alpha)
{
	GameLib::blend::setBlendMode(GameLib::blend::SCREEN,alpha);
}


//PRIMITIVE//
void drawRect(const float x, const float y, const float w, const float h, const float cx, const float cy, const float angle, const float r, const float g, const float b, const float a)
{
	GameLib::primitive::rect(x, y, w, h, cx, cy, angle, r, g, b);
}
void drawRect(const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& size, const DirectX::XMFLOAT2& center, const float angle, const DirectX::XMFLOAT3& color)
{
	GameLib::primitive::rect(pos, size, center, angle, color);
}

void drawLine(const float x1, const float y1, const float x2, const float y2, const float width, const float r, const float g, const float b)
{
	GameLib::primitive::line(x1, y1, x2, y2, r, g, b, width);
}
void drawLine(const DirectX::XMFLOAT2& begin, const DirectX::XMFLOAT2& end, const float width, const DirectX::XMFLOAT3& color)
{
	GameLib::primitive::line(begin, end, color, width);
}

void drawCircle(const float x, const float y, const float radius, const float r, const float g, const float b, int n)
{
	GameLib::primitive::circle(x, y, radius, r, g, b, n);
}
void drawCircle(const DirectX::XMFLOAT2& pos, const float radius, const DirectX::XMFLOAT3& color, const int n)
{
	GameLib::primitive::circle(pos, radius, color, n);
}

void drawQuad(const DirectX::XMFLOAT2(&v)[4], const float r, const float g, const float b)
{
	GameLib::primitive::quad(v, r, g, b);
}

//void drawQuad(const DirectX::XMFLOAT2(&v)[4], const DirectX::XMFLOAT4& color)
//{
//	GameLib::primitive::quad(v, color);
//}


//SPRITE//

void spriteLoad(Sprite* _sprite, wchar_t* _spriteName)
{
	GameLib::texture::SpriteRoad(_sprite, _spriteName);
}

void spriterender(Sprite* _sprite, const float pos_x, const float pos_y, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,_sprite->getSize().x,_sprite->getSize().y,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);

}
void spriteRender(Sprite* _sprite, const DirectX::XMFLOAT2& pos, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y, _sprite->getSize().x, _sprite->getSize().y,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRect(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y, texSize_x, texSize_y,
		texPos_x, texPos_y, texSize_x, texSize_y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRect(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y, texSize.x, texSize.y,
		texPos.x, texPos.y, texSize.x, texSize.y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderExtend(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y, (_sprite->getSize().x*Magnification), (_sprite->getSize().y*Magnification),
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderExtend(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const float Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y, (_sprite->getSize().x*Magnification), (_sprite->getSize().y*Magnification),
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderExtend2(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification_x, const float Magnification_y, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y, (_sprite->getSize().x*Magnification_x), (_sprite->getSize().y*Magnification_y),
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderExtend2(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y, (_sprite->getSize().x*Magnification.x), (_sprite->getSize().y*Magnification.y),
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderExtend3(Sprite* _sprite, const float pos_x, const float pos_y, const float centerPos_x, const float centerPos_y, const float Magnification_x, const float Magnification_y, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		(_sprite->getSize().x - Magnification_x), (_sprite->getSize().y - Magnification_y),
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, centerPos_x, centerPos_y, _tempX, _tempY);
}
void spriteRenderExtend3(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&centerPos, const DirectX::XMFLOAT2& Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		(_sprite->getSize().x - Magnification.x), (_sprite->getSize().y - Magnification.y),
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		0, 1, 1, 1, centerPos.x, centerPos.y, _tempX, _tempY);
}

void spriteRenderRectExtend(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		 (texSize_x*Magnification), (texSize_y*Magnification),
		texPos_x, texPos_y, texSize_x, texSize_y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRectExtend(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&texPos, const DirectX::XMFLOAT2&texSize, const float Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		(texSize.x*Magnification), (texSize.y*Magnification),
		texPos.x, texPos.y, texSize.x, texSize.y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRectExtend2(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float Magnification_x, const float Magnification_y, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		(texSize_x*Magnification_x), (texSize_y*Magnification_y),
		texPos_x, texPos_y, texSize_x, texSize_y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRectExtend2(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&texPos, const DirectX::XMFLOAT2&texSize, const DirectX::XMFLOAT2& Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		(texSize.x*Magnification.x), (texSize.y*Magnification.y),
		texPos.x, texPos.y, texSize.x, texSize.y,
		0, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRectExtend3(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float centerPos_x, const float centerPos_y, const float Magnification_x, const float Magnification_y, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		texSize_x*Magnification_x, texSize_y*Magnification_y,
		texPos_x, texPos_y, texSize_x, texSize_y,
		0, 1, 1, 1, centerPos_x, centerPos_y, _tempX, _tempY);
}
void spriteRenderRectExtend3(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&texPos, const DirectX::XMFLOAT2&texSize, const DirectX::XMFLOAT2& centerPos, const DirectX::XMFLOAT2& Magnification, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		texSize.x*Magnification.x, texSize.y*Magnification.y,
		texPos.x, texPos.y, texSize.x, texSize.y,
		0, 1, 1, 1, centerPos.x, centerPos.y, _tempX, _tempY);
}

void spriteRenderRota(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification, float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y, _sprite->getSize().x*Magnification, _sprite->getSize().y*Magnification,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRota(Sprite* _sprite, const DirectX::XMFLOAT2 pos, const float Magnification, float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y, _sprite->getSize().x*Magnification, _sprite->getSize().y*Magnification,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRota2(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification_x, const float Magnification_y, float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		_sprite->getSize().x*Magnification_x, _sprite->getSize().y*Magnification_y,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRota2(Sprite* _sprite, const DirectX::XMFLOAT2 pos, const DirectX::XMFLOAT2 Magnification, float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		_sprite->getSize().x*Magnification.x, _sprite->getSize().y*Magnification.y,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRota3(Sprite* _sprite, const float pos_x, const float pos_y, const float centerPos_x, const float centerPos_y, const float Magnification_x, const float Magnification_y, float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		_sprite->getSize().x*Magnification_x, _sprite->getSize().y*Magnification_y,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		angle, 1, 1, 1, centerPos_x, centerPos_y, _tempX, _tempY);
}
void spriteRenderRota3(Sprite* _sprite, const DirectX::XMFLOAT2 pos, const DirectX::XMFLOAT2& centerPos, const DirectX::XMFLOAT2 Magnification, float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		_sprite->getSize().x*Magnification.x, _sprite->getSize().y*Magnification.y,
		0, 0, _sprite->getSize().x, _sprite->getSize().y,
		angle, 1, 1, 1, centerPos.x, centerPos.y, _tempX, _tempY);
}

void spriteRenderRectRota(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y, texSize_x, texSize_y,
		texPos_x, texPos_y, texSize_x, texSize_y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRectRota(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, const float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y, texSize.x, texSize.y,
		texPos.x, texPos.y, texSize.x, texSize.y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRectRota2(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float Magnification_x, const float Magnification_y, const float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		texSize_x*Magnification_x, texSize_y*Magnification_y,
		texPos_x, texPos_y, texSize_x, texSize_y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}
void spriteRenderRectRota2(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, const DirectX::XMFLOAT2& Magnification, const float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		texSize.x*Magnification.x, texSize.y*Magnification.y,
		texPos.x, texPos.y, texSize.x, texSize.y,
		angle, 1, 1, 1, 0, 0, _tempX, _tempY);
}

void spriteRenderRectRota3(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float centerPos_x, const float centerPos_y, const float Magnification_x, const float Magnification_y, const float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos_x, pos_y,
		texSize_x*Magnification_x, texSize_y*Magnification_y,
		texPos_x, texPos_y, texSize_x, texSize_y,
		angle, 1, 1, 1, centerPos_x, centerPos_y, _tempX, _tempY);
}
void spriteRenderRectRota3(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, const DirectX::XMFLOAT2& centerPos, const DirectX::XMFLOAT2& Magnification, const float angle, bool _tempX, bool _tempY)
{
	GameLib::texture::SpriteRender(
		_sprite,
		pos.x, pos.y,
		texSize.x*Magnification.x, texSize.y*Magnification.y,
		texPos.x, texPos.y, texSize.x, texSize.y,
		angle, 1, 1, 1, centerPos.x, centerPos.y, _tempX, _tempY);
}


//Xinput_pad

int getState()
{
	return GameLib::input::xInput::getState();
}

bool pressedButtons(int _padNum, int _button)
{
	return GameLib::input::xInput::pressedButtons(_padNum, _button);
}

DirectX::XMINT2 getThumbL(int _padNum)
{
	return GameLib::input::xInput::getThumbL(_padNum);
}
DirectX::XMINT2 getThumbR(int _padNum)
{
	return GameLib::input::xInput::getThumbR(_padNum);
}
