#include "EffectManager.h"
#include "common.h"
#include "Camera.h"

#include <cereal\cereal.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\string.hpp>
#include <cereal\types\vector.hpp>

/*
EffectManager
*/
EffectManager::EffectManager()
{
	Init(EFC_ROOT_MAX);
}

EffectManager::~EffectManager()
{
	ClearAllEffects();
}

void EffectManager::AddEmitter(std::shared_ptr<EffectEmitter> _emitter)
{
	auto it_find = std::find_if
	(
		efc_emitters.begin(), efc_emitters.end(),
		[&](std::shared_ptr<EffectEmitter>& _emt)
		{
			return _emt == _emitter;
		}
	);

	if (it_find == efc_emitters.end())
	{
		efc_emitters.push_back(std::move(_emitter));
	}
}

void EffectManager::AddEfcRoot(std::shared_ptr<EffectRoot> _root)
{
	auto it_find = std::find_if
	(
		efc_roots.begin(), efc_roots.end(),
		[&](std::shared_ptr<EffectRoot> & _root)
	{
		return _root == _root;
	}
	);

	if (it_find == efc_roots.end())
	{
		efc_roots.push_back(std::move(_root));
	}
}

#pragma region MakeAndGet
std::shared_ptr<EffectEmitter> EffectManager::MakeAndGetEmitter(const EmitterParams & _emit_params, const DirectX::SimpleMath::Vector2& _pos)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_emitters.emplace_back(std::make_shared<EffectEmitter>(_emit_params, nullptr));

	return efc_emitters.back();
}

//std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRoot(const std::shared_ptr<EMIT_DATASET> _dset, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos, const bool& _is_flip_x)
//{
//	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
//	efc_roots.emplace_back(std::make_shared<EffectRoot>(_dset, _is_static_effect, _pos, _is_flip_x));
//
//	return efc_roots.back();
//}

std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRoot(const std::shared_ptr<EMIT_DATASET> _dset, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos, const bool& _is_flip_x, const DirectX::SimpleMath::Vector2* _p_pos)
{
	if (LOAD_EMIT_DATASET == false)return nullptr;
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_roots.emplace_back(std::make_shared<EffectRoot>(_dset, _is_static_effect, _pos, _is_flip_x, _p_pos));

	return efc_roots.back();
}

std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRoot(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, const Animation & _anim, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_roots.emplace_back(std::make_shared<EffectRoot>(_dset, _anim, _is_static_effect, _pos));

	return efc_roots.back();
}

std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRoot(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, std::shared_ptr<Animation>  _anim, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_roots.emplace_back(std::make_shared<EffectRoot>(_dset, _anim, _is_static_effect, _pos));

	return efc_roots.back();
}
#pragma region ToStack

std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRootToStack(const std::shared_ptr<EMIT_DATASET> _dset, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos, const bool& _is_flip_x, const DirectX::SimpleMath::Vector2* _p_pos)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_roots_stack.emplace_back(std::make_shared<EffectRoot>(_dset, _is_static_effect, _pos, _is_flip_x, _p_pos));

	return efc_roots_stack.back();
}

std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRootToStack(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, const Animation & _anim, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_roots_stack.emplace_back(std::make_shared<EffectRoot>(_dset, _anim, _is_static_effect, _pos));

	return efc_roots_stack.back();
}

std::shared_ptr<EffectRoot> EffectManager::MakeAndGetEfcRootToStack(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, std::shared_ptr<Animation>  _anim, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX) return nullptr;
	efc_roots_stack.emplace_back(std::make_shared<EffectRoot>(_dset, _anim, _is_static_effect, _pos));

	return efc_roots_stack.back();
}

#pragma endregion

std::shared_ptr<EffectRoot> EffectManager::GetEfcRoot(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, const Animation& _anim, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos)
{
	std::shared_ptr<EffectRoot> root = std::make_shared<EffectRoot>(_dset, _anim, _is_static_effect, _pos);
	return						root;
}

#pragma endregion

void EffectManager::SetEmitter(const EmitterParams& _emit_params)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX)return;
	efc_emitters.emplace_back(std::make_shared<EffectEmitter>(_emit_params,nullptr));
}

void EffectManager::SetEmitter
(
	EFC_TYPE type,
	Vector2 pos, Vector2 vel,
	Vector2 efc_vel,
	float duration,
	float set_speed_per_frame,
	float deceleration_rate_mul,
	float gravity,
	bool _is_ui_efc,
	float _size,
	const float* _attract_x_ptr, const float* _attract_y_ptr
)
{
	if (efc_emitters.size() >= EFC_ROOT_MAX)return;

	efc_emitters.emplace_back
	(
		std::make_shared<EffectEmitter>
		(
			type, pos, vel, efc_vel,
			duration, set_speed_per_frame, deceleration_rate_mul, gravity,
			_is_ui_efc, _size,
			_attract_x_ptr, _attract_y_ptr
		)
	);
}

void EffectManager::SetEmitters(const EmitterParams& _params)
{
	switch (_params.type)
	{
	case EFC_DEFAULT:
	{
		{
			int rand_num = 100 + GetRand(0);
			for (int i = 0; i < rand_num; i++)
			{
				SetEmitter
				(
					(EFC_TYPE)_params.type,
					_params.pos + Vector2(GetRandSigned(-2.f, 2.f), GetRandSigned(-2.f, 2.f)),
					_params.vel + Vector2(cosf(GetRand(100.f)), sinf(GetRand(100.f))) * GetRand(8.f),
					/*Vector2(cosf(GetRand(10.f)), sinf(GetRand(10.f)))*/_params.efc.vel * GetRandSigned(0.2f,0.6f),
					20.f + (float)GetRand(100.f), -1.f, 0.9f + GetRandSigned(-0.08f, 0.04f), -1.2f,
					false, 1.f,
					_params.attract_x_ptr, _params.attract_y_ptr
				);
			}
		}
	}
	break;
	default:
		break;
	}
}

void EffectManager::SetEmitDataSet(const EMIT_DATASET & _emit_dataset)
{
	for (auto& it_emit : _emit_dataset.dataset)
	{
		std::shared_ptr<EffectEmitter> emitter;
		emitter = make_shared<EffectEmitter>(it_emit.emit_data,nullptr);
		for (auto& it_efc : it_emit.efc_data)
		{
			emitter->EmitEffect(it_efc);
		}
		efc_emitters.emplace_back(std::move(emitter));
	}
}

void EffectManager::SetAnimEmitDataSet(const ANIM_EMIT_DATASET & _anim_emit_dataset)
{
	for (auto& it_emit : _anim_emit_dataset.dataset)
	{
		std::shared_ptr<AnimationEffectEmitter> anim_emitter;

		anim_emitter = make_shared<AnimationEffectEmitter>(it_emit, nullptr);
		efc_emitters.emplace_back(std::move(anim_emitter));
	}
}

void EffectManager::SetAnimEmitDataSet(const ANIM_EMIT_DATASET & _anim_emit_dataset, const Animation & _anim)
{
	for (auto& it_emit : _anim_emit_dataset.dataset)
	{
		std::shared_ptr<AnimationEffectEmitter> anim_emitter;

		anim_emitter = make_shared<AnimationEffectEmitter>(it_emit, _anim, nullptr);
		efc_emitters.emplace_back(std::move(anim_emitter));
	}
}

std::shared_ptr<EffectRoot> EffectManager::FindStaticEffect()
{
	auto it_find = std::find_if
	(
		efc_roots.begin(),
		efc_roots.end(),
		[](std::shared_ptr<EffectRoot>& _efc_root)
		{
			return _efc_root->IsStaticEffect();
		}
	);
	if (it_find == efc_roots.end())return nullptr;

	return *it_find;
}

void EffectManager::Update()
{
	if (efc_emitters.empty() == false)
	{
		auto remove_it = std::remove_if
		(
			efc_emitters.begin(), efc_emitters.end(),
			[](std::shared_ptr<EffectEmitter>& efc_emt)
		{
			return !efc_emt->Update();
		}
		);
		efc_emitters.erase(remove_it, efc_emitters.end());
	}

	if (efc_roots.empty() == false)
	{
		auto remove_it = std::remove_if
		(
			efc_roots.begin(), efc_roots.end(),
			[](std::shared_ptr<EffectRoot>& _efc_root)
		{
			return !_efc_root->Update();
		}
		);
		efc_roots.erase(remove_it, efc_roots.end());
	}

	// update中に生成するeffectはefc_roots_stackに格納する
	// 生成された分をefc_rootsにmoveする
	while (efc_roots_stack.empty() == false)
	{
		std::shared_ptr<EffectRoot> tmp;
		tmp = std::move(efc_roots_stack.front());
		efc_roots.emplace_back(std::move(tmp));
		efc_roots_stack.pop_front();
	}
}

void EffectManager::Draw(bool _is_ui_efc)
{
	for (auto it : efc_emitters)
	{
		if (!it->is_ui_efc && !_is_ui_efc)
		{
			it->Draw();
		}
		else if (it->is_ui_efc && _is_ui_efc)
		{
			it->Draw();
		}
	}

	{
		for (auto it : efc_roots)
		{
			it->Draw();
		}
	}
	//printfDx("capa:%d\n", (int)efc_emitters.capacity());
	//printfDx("size:%d\n", (int)efc_emitters.size());
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
}

size_t EffectManager::EFC_ROOT_MAX = 131072;
