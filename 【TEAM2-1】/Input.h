#pragma once


void InputKey();
void InputJoypad();
void InputAllKey();
const XINPUT_STATE& GetXInput();

extern int key[256]; // 0:入力されていない 1:入力された瞬間 2:入力されている
extern int keyall;
extern int PAD1;
extern int input;

class Mouse
{
private:
	Mouse() = default;
	Mouse(Mouse&) = default;
	~Mouse() = default;

public:
	struct CLICK
	{
		unsigned int pressing_count	= 0;
		bool is_release				= false;// 前フレームからボタンが離されたかどうかの判定
		int clicked_pos_log_x		= 0;
		int clicked_pos_log_y		= 0;
	};
	void Update();

	const int& pos_x_ref()		const { return pos_x; }
	const int& pos_y_ref()		const { return pos_y; }
	const int& pos_x_old_ref()	const { return pos_x_old; }
	const int& pos_y_old_ref()	const { return pos_y_old; }
	const int& pos_x_delta_ref()const { return pos_x_delta; }
	const int& pos_y_delta_ref()const { return pos_y_delta; }
	const CLICK& left_ref()		const { return left; }
	const CLICK& right_ref()	const { return right; }
	const CLICK& middle_ref()	const { return middle; }

	const int&		rot_ref()	const { return rot; }
	const float&	rotf_ref()	const { return rotf; }

	static Mouse* GetIns() { static Mouse mouse; return &mouse; }
private:
	int pos_x		, pos_y;
	int pos_x_old	, pos_y_old;
	int pos_x_delta	, pos_y_delta;
	CLICK left, right, middle;
	int		rot;
	float	rotf;

	void UpdateButtonsPressingCount();
	void UpdateClickedPosLog();
	void UpdatePosDelta();

};
#define pMOUSE (Mouse::GetIns()) 
