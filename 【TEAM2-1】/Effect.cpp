#include "common.h"
#include "Camera.h"
#include "Effect.h"
#include "EffectEmitter.h"
#include "EffectManager.h"
#include "EffectDataManager.h"
#include "FunctionDxLib.h"
#include "Mo2_BG.h"
#include "ObjectManager.h"

#include <cereal\cereal.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\string.hpp>

using namespace DirectX::SimpleMath;

BaseEffect::BaseEffect()
{
	is_out_of_scr = false;
}

#pragma region EfcDefault

EfcDefault::EfcDefault(Vector2 _pos, Vector2 _vel, float _duration, float _deceleration_rate_mul, float _gravity)
{
	efc_params.pos = _pos;
	efc_params.vel = _vel;
	efc_params.radius = 10.f;
	efc_params.deceleration_rate_mul = _deceleration_rate_mul;
	efc_params.gravity = _gravity;

	efc_params.duration_max = _duration;
	efc_params.duration = _duration;

	efc_params.attract_vel = { 0.f,0.f };

	efc_params.noise_scale = (float)GetRandSigned(1, 10);
	efc_params.curl_speed = 0.1f + GetRand(0.3f);

	if (efc_params.draw.interval < 1)
		efc_params.draw.interval = 1;

	is_reverse_x = (GetRand(1) == 1);
}

EfcDefault::EfcDefault(const EffectParams& _params) : is_reverse_x(false)
{
	efc_params = _params;
}

EfcDefault::EfcDefault(const EffectParams& _params, EffectEmitter* _parent_emitter, const bool& _is_reverse_x) : is_reverse_x(_is_reverse_x)
{
	efc_params		= _params;
	parent_emitter	= _parent_emitter;

	if (efc_params.is_attract_to_parent_emt)
	{
		efc_params.attract_x_ptr = &parent_emitter->GetEmitterParamsRef().pos.x;
		efc_params.attract_y_ptr = &parent_emitter->GetEmitterParamsRef().pos.y;
	}
}

bool EfcDefault::Update()
{
	if (efc_params.duration >= 0.f)
	{
		//カメラ外のエフェクトは寿命の減少を加速させる
		int posx = cam.xi_ext(efc_params.pos.x);
		int posy = cam.yi_ext(efc_params.pos.y);
		if (posx < 0 - scast<int>(efc_params.radius) * 3.f || posx > TEMP_SCREEN_WIDTH  + scast<int>(efc_params.radius) * 3.f ||
			posy < 0 - scast<int>(efc_params.radius) * 3.f || posy > TEMP_SCREEN_HEIGHT + scast<int>(efc_params.radius) * 3.f)
		{
			efc_params.duration -= 3.0f;
			is_out_of_scr = true;
			return true;
		}
		else
			is_out_of_scr = false;

		efc_params.vel.y += efc_params.gravity;
		efc_params.vel *= efc_params.deceleration_rate_mul;

		if(efc_params.is_use_curl_noise)
		{
			Vector2 tmp		= CurlNoise(efc_params.pos, 1, 1.f, 0.f, 24.f*efc_params.noise_scale);
			efc_params.vel += tmp*efc_params.curl_speed;
		}

		if (efc_params.attract_x_ptr != nullptr)
		{
			if (Coll::Circle(Vector2(*efc_params.attract_x_ptr, *efc_params.attract_y_ptr), 50.f, efc_params.pos, 0.5f) == false)
			{
				Vector2 v;
				v.x = *efc_params.attract_x_ptr - efc_params.pos.x;
				v.y = *efc_params.attract_y_ptr - efc_params.pos.y;
				v.Normalize();
				efc_params.attract_vel += v*0.1f;
			}
		}

		efc_params.pos += efc_params.vel * 2.f + efc_params.attract_vel;

		efc_params.col.Update();

		if (efc_params.draw.graph_hadle > 0 || efc_params.primitive_type == EffectParams::ePrimitiveTypes::TRIANGLE)
		{
			switch (efc_params.angle_type)
			{
			case EffectParams::eAngleTypes::ANGLE_NONE:
				break;
			case EffectParams::eAngleTypes::DELAY_MOVE:
				efc_params.angle.Update();
				break;
			case EffectParams::eAngleTypes::FOLLOW_TO_VEL:
				efc_params.angle = GetDegree(atan2f(efc_params.vel.y, efc_params.vel.x));
				efc_params.angle.val = efc_params.angle.target_val;
				break;
			case EffectParams::eAngleTypes::FOLLOW_TO_DURATION:
				efc_params.angle = GetDegree(efc_params.duration);
				efc_params.angle.Update();
				break;
			default:
				efc_params.angle.target_val += GetDegree((efc_params.vel.x + efc_params.vel.y) * 0.5f) * is_reverse_x ? -1.f : 1.f;
				efc_params.angle.Update();
				break;
			}
		}

		efc_params.duration -= 1.0f;
		return true;
	}

	return false;
}

void EfcDefault::Draw(DirectX::SimpleMath::Vector2 _line_prev_pos)
{
	if (is_out_of_scr) return;

	int posx = cam.xi_ext(efc_params.pos.x);
	int posy = cam.yi_ext(efc_params.pos.y);
	float duration_diff = efc_params.duration_max - efc_params.duration;

	// radius
	float radius_duration = 0.f;
	float radius;
	if (efc_params.radius_fade_interval <= duration_diff)
	{
		float radius_decreae_interval = efc_params.duration_max - efc_params.radius_decreae_interval;
		if (efc_params.duration < radius_decreae_interval)
			radius_duration = efc_params.duration_max * (efc_params.duration / radius_decreae_interval);
		else
			radius_duration = efc_params.duration_max;

		radius = Easing::easing_functions[efc_params.easing_algo](radius_duration, efc_params.duration_max, efc_params.radius, 0.f);
	}
	else
	{
		radius_duration = efc_params.duration_max - efc_params.duration;
		radius = Easing::easing_functions[efc_params.easing_algo](radius_duration, efc_params.radius_fade_interval, efc_params.radius, 0.f);
	}

	// blend_param
	float blend_duration = 0.f;
	float blend_param;
	if (efc_params.blend_fade_interval <= duration_diff)
	{
		float blend_decreae_interval = efc_params.duration_max - efc_params.blend_decreae_interval;
		if (efc_params.duration < blend_decreae_interval)
			blend_duration = efc_params.duration_max * (efc_params.duration / blend_decreae_interval);
		else
			blend_duration = efc_params.duration_max;

		blend_param = Easing::easing_functions[efc_params.blend_mode_ease](blend_duration, efc_params.duration_max, 255.f, 0.f);
	}
	else
	{
		blend_duration = efc_params.duration_max - efc_params.duration;
		blend_param = Easing::easing_functions[efc_params.blend_mode_ease](blend_duration, efc_params.radius_fade_interval, efc_params.radius, 0.f);
	}

	// 描画
	float alpha_mul = Fit(efc_params.col.col.w, 255.f, 1.f, 0.f);
	SetDrawBlendMode(efc_params.blend_mode, blend_param * alpha_mul);
	if (efc_params.draw.graph_hadle > -1)
	{
		int anmx; 
		if (efc_params.draw.is_loop)
		{
			anmx = (scasi(efc_params.duration)/efc_params.draw.interval) % efc_params.draw.anmx;
		}
		else
		{
			// anmx から 0 へアニメーションする
			anmx = scasi(Fit(efc_params.duration, efc_params.duration_max, scasf(efc_params.draw.anmx)));
		}
		SetDrawBright(scasi(efc_params.col.col.x), scasi(efc_params.col.col.y), scasi(efc_params.col.col.z));
		DrawRectRotaGraphFast2
		(
			posx, posy,
			efc_params.draw.texx + efc_params.draw.srcx * anmx,
			efc_params.draw.texy + efc_params.draw.srcy * efc_params.draw.anmy,
			efc_params.draw.srcx,
			efc_params.draw.srcy,
			efc_params.draw.cx,
			efc_params.draw.cy,
			radius*cam.chipextrate(),
			GetRadian(efc_params.angle.val),
			efc_params.draw.graph_hadle, 1,
			is_reverse_x
		);
		SetDrawBright(255, 255, 255);
	}
	else // プリミティブ描画
	{
		switch (efc_params.primitive_type)
		{
		case EffectParams::CIRCLE:
			DrawCircle(posx, posy, scasi(radius), efc_params.col.GetTargetCol());
			break;

		case EffectParams::PIXEL:
			DrawPixel(posx, posy, efc_params.col.GetCol());
			break;

		case EffectParams::BOX:
			DrawBox(posx, posy, posx + scasi(radius), posy + scasi(radius), efc_params.col.GetCol(), 1);
			break;

		case EffectParams::TRIANGLE:
		{
			float   angle	 = GetRadian(efc_params.angle.val);
			Vector2	center	 = { efc_params.pos.x				, efc_params.pos.y			};// center = efc_params.pos
			Vector2 point[3];
					point[0] = { efc_params.pos.x				, efc_params.pos.y - radius	};/*		0				 */
					point[1] = { efc_params.pos.x + radius		, efc_params.pos.y + radius	};/*	  /	c \				 */
					point[2] = { efc_params.pos.x - radius		, efc_params.pos.y + radius	};/*	2 _____ 1			 */

			for (int i = 0; i < 3; i++)
			{
				Vector2 point_sub_center;
						point_sub_center = point[i] - center;

				point[i].x = cosf(angle)*point_sub_center.x - sinf(angle)*point_sub_center.y + center.x;
				point[i].y = sinf(angle)*point_sub_center.x + cosf(angle)*point_sub_center.y + center.y;
			}

			DrawTriangle
			(
				cam.xi_ext(point[0].x), cam.yi_ext(point[0].y),
				cam.xi_ext(point[1].x), cam.yi_ext(point[1].y),
				cam.xi_ext(point[2].x), cam.yi_ext(point[2].y),
				efc_params.col.GetCol(), 1
			);
		}
			break;
		case EffectParams::ePrimitiveTypes::PRIMTYPE_NONE:
			break;
		default:
			break;
		}

	}
	if (efc_params.is_draw_line)
	{
		DrawLineCam(_line_prev_pos, efc_params.pos, efc_params.col.GetCol(), scasi(radius));
	}

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

#pragma endregion

#pragma region EfcRandomImageParticle

EfcRandomImageParticle::EfcRandomImageParticle(Vector2 _pos, Vector2 _vel, float _duration, float _deceleration_rate_mul, float _gravity)
{
	efc_params.pos = _pos;
	efc_params.vel = _vel;
	efc_params.radius = 0.f;
	efc_params.deceleration_rate_mul = _deceleration_rate_mul;
	efc_params.gravity = _gravity;

	efc_params.duration_max = _duration;
	efc_params.duration = _duration;

	efc_params.attract_vel = { 0.f,0.f };

	efc_params.noise_scale = (float)GetRandSigned(1, 10);
	efc_params.curl_speed = 0.1f + GetRand(0.3f);

	if (efc_params.draw.interval < 1)
		efc_params.draw.interval = 1;

	is_reverse_x = (GetRand(1) == 1);
}

EfcRandomImageParticle::EfcRandomImageParticle(const EffectParams& _params)
{
	efc_params = _params;

	is_reverse_x = (GetRand(1) == 1);
}

EfcRandomImageParticle::EfcRandomImageParticle(const EffectParams& _params, EffectEmitter* _parent_emitter)
{
	efc_params = _params;
	efc_params.draw.anmx = GetRand(efc_params.draw.anmx);

	parent_emitter = _parent_emitter;

	if (efc_params.is_attract_to_parent_emt)
	{
		efc_params.attract_x_ptr = &parent_emitter->GetEmitterParamsRef().pos.x;
		efc_params.attract_y_ptr = &parent_emitter->GetEmitterParamsRef().pos.y;
	}

	is_reverse_x = (GetRand(1) == 1);
}

bool EfcRandomImageParticle::Update()
{
	if (efc_params.duration >= 0.f)
	{
		// カメラ外のエフェクトは寿命の減少を加速させる
		int posx = cam.xi_ext(efc_params.pos.x);
		int posy = cam.yi_ext(efc_params.pos.y);
		if (posx < 0 - scast<int>(efc_params.radius) * 3.f || posx > TEMP_SCREEN_WIDTH + scast<int>(efc_params.radius) * 3.f ||
			posy < 0 - scast<int>(efc_params.radius) * 3.f || posy > TEMP_SCREEN_HEIGHT + scast<int>(efc_params.radius) * 3.f)
		{
			efc_params.duration -= 8.0f;
			is_out_of_scr = true;
		}
		else
			is_out_of_scr = false;

		if (bounce_limit > 2)
		{
			efc_params.vel.y += efc_params.gravity;
		}
		else
		{
			efc_params.vel = { 0,0 };
		}
		if (!efc_params.is_attract_to_parent_emt || !efc_params.is_use_curl_noise)
			IsHit();

		efc_params.vel *= efc_params.deceleration_rate_mul;

		if (efc_params.is_use_curl_noise)
		{
			Vector2 tmp = CurlNoise(efc_params.pos, 1, 1.f, 0.f, 24.f * efc_params.noise_scale);
			efc_params.vel += tmp * efc_params.curl_speed;
		}

		if (efc_params.attract_x_ptr != nullptr)
		{
			if (Coll::Circle(Vector2(*efc_params.attract_x_ptr, *efc_params.attract_y_ptr), 50.f, efc_params.pos, 0.5f) == false)
			{
				Vector2 v;
				v.x = *efc_params.attract_x_ptr - efc_params.pos.x;
				v.y = *efc_params.attract_y_ptr - efc_params.pos.y;
				v.Normalize();
				efc_params.attract_vel += v * 0.1f;
			}
		}

		efc_params.pos += efc_params.vel * 2.f + efc_params.attract_vel;

		efc_params.col.Update();

		if (efc_params.draw.graph_hadle > 0 || efc_params.primitive_type == EffectParams::ePrimitiveTypes::TRIANGLE)
		{
			switch (efc_params.angle_type)
			{
			case EffectParams::eAngleTypes::ANGLE_NONE:
				break;
			case EffectParams::eAngleTypes::DELAY_MOVE:
				efc_params.angle.Update();
				break;
			case EffectParams::eAngleTypes::FOLLOW_TO_VEL:
				efc_params.angle = GetDegree(atan2f(efc_params.vel.y, efc_params.vel.x));
				efc_params.angle.val = efc_params.angle.target_val;
				break;
			case EffectParams::eAngleTypes::FOLLOW_TO_DURATION:
				efc_params.angle = GetDegree(efc_params.duration);
				efc_params.angle.Update();
				break;
			default:
				efc_params.angle.target_val += GetDegree((efc_params.vel.x + efc_params.vel.y) * 0.5f) * is_reverse_x ? -1.f : 1.f;
				efc_params.angle.Update();
				break;
			}
		}

		efc_params.duration -= 1.0f;
		return true;
	}

	return false;
}

void EfcRandomImageParticle::Draw(DirectX::SimpleMath::Vector2 _line_prev_pos)
{
	if (is_out_of_scr) return;
	int posx = cam.xi_ext(efc_params.pos.x);
	int posy = cam.yi_ext(efc_params.pos.y);
	if (posx < 0 - scast<int>(efc_params.radius)*3.f || posx > TEMP_SCREEN_WIDTH  + scast<int>(efc_params.radius)*3.f) return;
	if (posy < 0 - scast<int>(efc_params.radius)*3.f || posy > TEMP_SCREEN_HEIGHT + scast<int>(efc_params.radius)*3.f) return;

	float duration_diff = efc_params.duration_max - efc_params.duration;

	// radius
	float radius_duration = 0.f;
	float radius;
	if (efc_params.radius_fade_interval <= duration_diff)
	{
		float radius_decreae_interval = efc_params.duration_max - efc_params.radius_decreae_interval;
		if (efc_params.duration < radius_decreae_interval)
			radius_duration = efc_params.duration_max * (efc_params.duration / radius_decreae_interval);
		else
			radius_duration = efc_params.duration_max;

		radius = Easing::easing_functions[efc_params.easing_algo](radius_duration, efc_params.duration_max, efc_params.radius, 0.f);
	}
	else
	{
		radius_duration = duration_diff;
		radius = Easing::easing_functions[efc_params.easing_algo](radius_duration, efc_params.radius_fade_interval, efc_params.radius, 0.f);
	}

	// blend_param
	float blend_duration = 0.f;
	float blend_param;
	if (efc_params.blend_fade_interval <= duration_diff)
	{
		float blend_decreae_interval = efc_params.duration_max - efc_params.blend_decreae_interval;
		if (efc_params.duration < blend_decreae_interval)
			blend_duration = efc_params.duration_max * (efc_params.duration / blend_decreae_interval);
		else
			blend_duration = efc_params.duration_max;

		blend_param = Easing::easing_functions[efc_params.blend_mode_ease](blend_duration, efc_params.duration_max, 255.f, 0.f);
	}
	else
	{
		blend_duration = efc_params.duration_max - efc_params.duration;
		blend_param = Easing::easing_functions[efc_params.blend_mode_ease](blend_duration, efc_params.radius_fade_interval, efc_params.radius, 0.f);
	}

	// 描画
	float alpha_mul = Fit(efc_params.col.col.w, 255.f, 1.f, 0.f);
	SetDrawBlendMode(efc_params.blend_mode, blend_param * alpha_mul);
	if (efc_params.draw.graph_hadle > -1)
	{
		SetDrawBright(scasi(efc_params.col.col.x), scasi(efc_params.col.col.y), scasi(efc_params.col.col.z));
		DrawRectRotaGraph2
		(
			posx, posy,
			efc_params.draw.texx + efc_params.draw.srcx * efc_params.draw.anmx,
			efc_params.draw.texy + efc_params.draw.srcy * efc_params.draw.anmy,
			efc_params.draw.srcx,
			efc_params.draw.srcy,
			efc_params.draw.cx,
			efc_params.draw.cy,
			radius*cam.chipextrate(),
			GetRadian(efc_params.angle.val),
			efc_params.draw.graph_hadle, 1,
			is_reverse_x
		);
		SetDrawBright(255, 255, 255);
	}
	else // プリミティブ描画
	{
		switch (efc_params.primitive_type)
		{
		case EffectParams::CIRCLE:
			DrawCircle(posx, posy, scasi(radius), efc_params.col.GetTargetCol());
			break;

		case EffectParams::PIXEL:
			DrawPixel(posx, posy, efc_params.col.GetCol());
			break;

		case EffectParams::BOX:
			DrawBox(posx, posy, posx + scasi(radius), posy + scasi(radius), efc_params.col.GetCol(), 1);
			break;

		case EffectParams::TRIANGLE:
		{
			float   angle	 = GetRadian(efc_params.angle.val);
			Vector2	center	 = { efc_params.pos.x			, efc_params.pos.y			};// center = efc_params.pos
			Vector2 point[3];
					point[0] = { efc_params.pos.x			, efc_params.pos.y - radius	};/*		0				 */
					point[1] = { efc_params.pos.x + radius	, efc_params.pos.y + radius	};/*	  /	c \				 */
					point[2] = { efc_params.pos.x - radius	, efc_params.pos.y + radius	};/*	2 _____ 1			 */

			for (int i = 0; i < 3; i++)
			{
				Vector2 point_sub_center;
						point_sub_center = point[i] - center;

				point[i].x = cosf(angle)*point_sub_center.x - sinf(angle)*point_sub_center.y + center.x;
				point[i].y = sinf(angle)*point_sub_center.x + cosf(angle)*point_sub_center.y + center.y;
			}

			DrawTriangle
			(
				cam.xi_ext(point[0].x), cam.yi_ext(point[0].y),
				cam.xi_ext(point[1].x), cam.yi_ext(point[1].y),
				cam.xi_ext(point[2].x), cam.yi_ext(point[2].y),
				efc_params.col.GetCol(), 1
			);
		}
			break;
		case EffectParams::ePrimitiveTypes::PRIMTYPE_NONE:
			break;
		default:
			break;
		}

	}
	if (efc_params.is_draw_line)
	{
		DrawLineCam(_line_prev_pos, efc_params.pos, efc_params.col.GetCol(), scasi(radius));
	}

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

bool EfcRandomImageParticle::IsHit()
{
	if (bounce_limit < 0)return false;

	if (isHitAll(efc_params.pos.x, efc_params.pos.y) != -1)
	{
		// prev_posから pos.x - prev_pos.x 分 xを加算した際に埋まっているか否かで反転方向を決定
		// 狭い場所や起伏が多い場所では誤判定する既知の問題あり 
		float bounce_dir_check = efc_params.pos.x - prev_pos.x;
		if (isHitAll(prev_pos.x + bounce_dir_check, prev_pos.y) != -1)// x反転
		{
			efc_params.vel.x *= -bounce_coefficient;
		}
		else// y反転
		{
			bounce_limit--;
			efc_params.vel.y *= -bounce_coefficient;
		}

		prev_pos = efc_params.pos;
		return true;
	}

	prev_pos = efc_params.pos;
	return false;
}

#pragma endregion

#pragma region EfcJewerly

std::string EfcJewerly::jewerly_efc_filename[EfcJewerly::eJewerlyColors::Jew_End] =
{
	"Jewelry_hit_r.bin",
	"Jewelry_hit_g.bin",
	"Jewelry_hit_b.bin",
	"Jewelry_hit_y.bin",
};


//EfcJewerly(DirectX::SimpleMath::Vector2 _pos, DirectX::SimpleMath::Vector2 _vel, float _duration = 40.f, float _deceleration_rate_mul = 0.9f, float _gravity = 0.f);

EfcJewerly::EfcJewerly(DirectX::SimpleMath::Vector2 _pos, DirectX::SimpleMath::Vector2 _vel, float _duration, float _deceleration_rate_mul, float _gravity)
{
	efc_params.pos = _pos;
	efc_params.vel = _vel;
	efc_params.radius = 10.f;
	efc_params.deceleration_rate_mul = _deceleration_rate_mul;
	efc_params.gravity = _gravity;

	efc_params.duration_max = _duration;
	efc_params.duration = _duration;

	efc_params.attract_vel = { 0.f,0.f };

	efc_params.noise_scale = (float)GetRandSigned(1, 10);
	efc_params.curl_speed = 0.1f + GetRand(0.3f);

	if (efc_params.draw.interval < 1)
		efc_params.draw.interval = 1;
	is_near_to_pl = false;

	is_reverse_x = (GetRand(1) == 1);
}

EfcJewerly::EfcJewerly(const EffectParams& _params)
{
	efc_params = _params;
	is_near_to_pl = false;

	is_reverse_x = (GetRand(1) == 1);
}

EfcJewerly::EfcJewerly(const EffectParams& _params, EffectEmitter* _parent_emitter)
{
	efc_params = _params;
	efc_params.draw.anmx = GetRand(efc_params.draw.anmx);

	parent_emitter = _parent_emitter;
	if (efc_params.is_attract_to_parent_emt)
	{
		if (pObjManager->ObjList[Object::PLAYER].empty()) return;
		Vector2* player_pos_ptr = &pObjManager->ObjList[Object::PLAYER].back()->pos;

		efc_params.attract_x_ptr = &parent_emitter->GetEmitterParamsRef().pos.x;
		efc_params.attract_y_ptr = &parent_emitter->GetEmitterParamsRef().pos.y;
		efc_params.attract_x_ptr = &player_pos_ptr->x;
		efc_params.attract_y_ptr = &player_pos_ptr->y;

	}
	is_near_to_pl = false;
	is_reverse_x = (GetRand(1) == 1);

	flare_type = GetRand(1);
	flare_angle = GetRandSigned(-180.f, 180.f);
	attrack_pos_randomize.x = GetRandSigned(-12.f, 12.f);
	attrack_pos_randomize.y = GetRandSigned(-28.f, 28.f);

	attract_timer = 38.f;
}

EfcJewerly::~EfcJewerly()
{
	parent_emitter = nullptr;
}

void EfcJewerly::EmitHitEfc()
{
	EFC_MGR->MakeAndGetEfcRootToStack
	(
		EFCDATA->GetEmitDataset(jewerly_efc_filename[efc_params.draw.anmx]),
		false, efc_params.pos, is_reverse_x
	);
	//TODO::お金
	int money;
	money = 19 + (pBG->getStageNumber()-1) * 3;
	pObjManager->ObjList[Object::PLAYER].back()->addMoney(money);
}

bool EfcJewerly::Update()
{
	//if (efc_params.duration >= 0.f)
	{
		// カメラ外のエフェクトは寿命の減少を加速させる
		int posx = cam.xi_ext(efc_params.pos.x);
		int posy = cam.yi_ext(efc_params.pos.y);
		if (posx < 0 - scast<int>(efc_params.radius) * 3.f || posx > TEMP_SCREEN_WIDTH + scast<int>(efc_params.radius) * 3.f ||
			posy < 0 - scast<int>(efc_params.radius) * 3.f || posy > TEMP_SCREEN_HEIGHT + scast<int>(efc_params.radius) * 3.f)
		{
			//efc_params.duration -= 3.0f;
			is_out_of_scr = true;
		}
		else
			is_out_of_scr = false;

		//if (bounce_limit > 2)
		//{
		//	efc_params.vel.y += efc_params.gravity;
		//}
		//else
		//{
		//	efc_params.vel = { 0,0 };
		//}
		//if (!efc_params.is_attract_to_parent_emt || !efc_params.is_use_curl_noise)
		//	IsHit();

		efc_params.vel *= efc_params.deceleration_rate_mul;

		if (efc_params.is_use_curl_noise)
		{
			Vector2 tmp = CurlNoise(efc_params.pos, 1, 1.f, 0.f, 24.f * efc_params.noise_scale);
			efc_params.vel += tmp * efc_params.curl_speed;
		}

		efc_params.pos += efc_params.vel * 2.f;
		
		attract_timer--;
		if (efc_params.attract_x_ptr != nullptr)
		{
			Vector2 v;
			v.x = (*efc_params.attract_x_ptr + (/*(float)!is_near_to_pl **/ attrack_pos_randomize.x)) - efc_params.pos.x;
			v.y = (*efc_params.attract_y_ptr + (/*(float)!is_near_to_pl **/ attrack_pos_randomize.y)) - efc_params.pos.y - 32.f;
			if (attract_timer < 0.f &&
				Coll::Circle
				(
					Vector2(*efc_params.attract_x_ptr,
							*efc_params.attract_y_ptr - 32.f),
					60.f + GetRand(10.f),
					efc_params.pos,
					50.f + GetRand(10.f)
				) ||
				is_near_to_pl
			)
			{
				is_near_to_pl = true;
				//v.Normalize();
				v *= 0.085f;
				v *= (std::min)(1.f + (fabsf(attract_timer) * 0.1f), 2.8f);

				efc_params.attract_vel = v;
				//efc_params.pos += efc_params.attract_vel;
			}
			else
			{
				v *= 0.01f;
				if (fabsf(efc_params.attract_vel.x) > 10.f)
					efc_params.attract_vel.x = signf(efc_params.attract_vel.x) * 10.f;
				if (fabsf(efc_params.attract_vel.y) > 10.f)
					efc_params.attract_vel.y = signf(efc_params.attract_vel.y) * 10.f;
				efc_params.attract_vel += v * 0.4f;
			}
			efc_params.pos += efc_params.attract_vel;
			if (attract_timer < 0.f && Coll::Circle(
				Vector2(*efc_params.attract_x_ptr + GetRandSigned(-12.f, 12.f) /*+ attrack_pos_randomize.x*/,
						*efc_params.attract_y_ptr - 32.f + GetRandSigned(-32.f, 32.f) /*+ attrack_pos_randomize.y*/),
				20.f,
				efc_params.pos,
				2.f)
				== true)
			{
				EmitHitEfc();
				return false;
			}
		}


		efc_params.col.Update();

		if (efc_params.draw.graph_hadle > 0 || efc_params.primitive_type == EffectParams::ePrimitiveTypes::TRIANGLE)
		{
			switch (efc_params.angle_type)
			{
			case EffectParams::eAngleTypes::ANGLE_NONE:
				break;
			case EffectParams::eAngleTypes::DELAY_MOVE:
				efc_params.angle.Update();
				break;
			case EffectParams::eAngleTypes::FOLLOW_TO_VEL:
				efc_params.angle = GetDegree(atan2f(efc_params.vel.y, efc_params.vel.x));
				efc_params.angle.val = efc_params.angle.target_val;
				break;
			case EffectParams::eAngleTypes::FOLLOW_TO_DURATION:
				efc_params.angle = GetRadian(efc_params.duration);
				efc_params.angle.Update();
				break;
			default:
				efc_params.angle.target_val += GetDegree((efc_params.vel.x + efc_params.vel.y) * 0.5f) * is_reverse_x ? -1.f : 1.f;
				efc_params.angle.Update();
				break;
			}
			flare_angle += 8.f;
		}

		//efc_params.duration -= 1.0f;
		return true;
	}

	return false;

}

void EfcJewerly::Draw(DirectX::SimpleMath::Vector2 _line_prev_pos)
{
	if (is_out_of_scr) return;
	int posx = cam.xi_ext(efc_params.pos.x);
	int posy = cam.yi_ext(efc_params.pos.y);
	if (posx < 0 - scast<int>(efc_params.radius) * 3.f || posx > TEMP_SCREEN_WIDTH + scast<int>(efc_params.radius) * 3.f) return;
	if (posy < 0 - scast<int>(efc_params.radius) * 3.f || posy > TEMP_SCREEN_HEIGHT + scast<int>(efc_params.radius) * 3.f) return;

	float duration_diff = efc_params.duration_max - efc_params.duration;

	// radius
	float radius_duration = 0.f;
	float radius;
	if (efc_params.radius_fade_interval <= duration_diff)
	{
		float radius_decreae_interval = efc_params.duration_max - efc_params.radius_decreae_interval;
		if (efc_params.duration < radius_decreae_interval)
			radius_duration = efc_params.duration_max * (efc_params.duration / radius_decreae_interval);
		else
			radius_duration = efc_params.duration_max;

		radius = Easing::easing_functions[efc_params.easing_algo](radius_duration, efc_params.duration_max, efc_params.radius, 0.f);
	}
	else
	{
		radius_duration = duration_diff;
		radius = Easing::easing_functions[efc_params.easing_algo](radius_duration, efc_params.radius_fade_interval, efc_params.radius, 0.f);
	}

	// blend_param
	float blend_duration = 0.f;
	float blend_param;
	if (efc_params.blend_fade_interval <= duration_diff)
	{
		float blend_decreae_interval = efc_params.duration_max - efc_params.blend_decreae_interval;
		if (efc_params.duration < blend_decreae_interval)
			blend_duration = efc_params.duration_max * (efc_params.duration / blend_decreae_interval);
		else
			blend_duration = efc_params.duration_max;

		blend_param = Easing::easing_functions[efc_params.blend_mode_ease](blend_duration, efc_params.duration_max, 255.f, 0.f);
	}
	else
	{
		blend_duration = efc_params.duration_max - efc_params.duration;
		blend_param = Easing::easing_functions[efc_params.blend_mode_ease](blend_duration, efc_params.radius_fade_interval, efc_params.radius, 0.f);
	}

	// 描画
	float alpha_mul = Fit(efc_params.col.col.w, 255.f, 1.f, 0.f);
	SetDrawBlendMode(efc_params.blend_mode, blend_param * alpha_mul);
	if (efc_params.draw.graph_hadle > -1)
	{
		SetDrawBright(scasi(efc_params.col.col.x), scasi(efc_params.col.col.y), scasi(efc_params.col.col.z));
		DrawRectRotaGraphFast2
		(
			posx, posy,
			efc_params.draw.texx + efc_params.draw.srcx * efc_params.draw.anmx,
			efc_params.draw.texy + efc_params.draw.srcy * efc_params.draw.anmy,
			efc_params.draw.srcx,
			efc_params.draw.srcy,
			efc_params.draw.cx,
			efc_params.draw.cy,
			radius * cam.chipextrate(),
			GetRadian(efc_params.angle.val),
			efc_params.draw.graph_hadle, 1,
			is_reverse_x
		);
		SetDrawBlendMode(DX_BLENDMODE_ADD, 128);
		DrawRectRotaGraphFast2
		(
			posx + 3, posy - 4,
			288 + efc_params.draw.srcx * flare_type,
			528,
			16,
			16,
			8,
			8,
			1.f * cam.chipextrate(),
			-(GetRadian(flare_angle) * (1.f - 2.f * (float)is_reverse_x)),
			efc_params.draw.graph_hadle, 1,
			is_reverse_x
		);
		SetDrawBright(255, 255, 255);
	}
	else // プリミティブ描画
	{
		switch (efc_params.primitive_type)
		{
		case EffectParams::CIRCLE:
			DrawCircle(posx, posy, scasi(radius), efc_params.col.GetTargetCol());
			break;

		case EffectParams::PIXEL:
			DrawPixel(posx, posy, efc_params.col.GetCol());
			break;

		case EffectParams::BOX:
			DrawBox(posx, posy, posx + scasi(radius), posy + scasi(radius), efc_params.col.GetCol(), 1);
			break;

		case EffectParams::TRIANGLE:
		{
			float   angle = GetRadian(efc_params.angle.val);
			Vector2	center = { efc_params.pos.x			, efc_params.pos.y };// center = efc_params.pos
			Vector2 point[3];
			point[0] = { efc_params.pos.x			, efc_params.pos.y - radius };/*		0				 */
			point[1] = { efc_params.pos.x + radius	, efc_params.pos.y + radius };/*	  /	c \				 */
			point[2] = { efc_params.pos.x - radius	, efc_params.pos.y + radius };/*	2 _____ 1			 */

			for (int i = 0; i < 3; i++)
			{
				Vector2 point_sub_center;
				point_sub_center = point[i] - center;

				point[i].x = cosf(angle) * point_sub_center.x - sinf(angle) * point_sub_center.y + center.x;
				point[i].y = sinf(angle) * point_sub_center.x + cosf(angle) * point_sub_center.y + center.y;
			}

			DrawTriangle
			(
				cam.xi_ext(point[0].x), cam.yi_ext(point[0].y),
				cam.xi_ext(point[1].x), cam.yi_ext(point[1].y),
				cam.xi_ext(point[2].x), cam.yi_ext(point[2].y),
				efc_params.col.GetCol(), 1
			);
		}
		break;
		case EffectParams::ePrimitiveTypes::PRIMTYPE_NONE:
			break;
		default:
			break;
		}

	}
	if (efc_params.is_draw_line)
	{
		DrawLineCam(_line_prev_pos, efc_params.pos, efc_params.col.GetCol(), scasi(radius));
	}

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

bool EfcJewerly::IsHit()
{
	if (bounce_limit < 0)return false;

	if (isHitAll(efc_params.pos.x, efc_params.pos.y) != -1)
	{
		// prev_posから pos.x - prev_pos.x 分 xを加算した際に埋まっているか否かで反転方向を決定
		// 狭い場所や起伏が多い場所では誤判定する既知の問題あり 
		float bounce_dir_check = efc_params.pos.x - prev_pos.x;
		if (isHitAll(prev_pos.x + bounce_dir_check, prev_pos.y) != -1)// x反転
		{
			efc_params.vel.x *= -bounce_coefficient;
		}
		else// y反転
		{
			bounce_limit--;
			efc_params.vel.y *= -bounce_coefficient;
		}

		prev_pos = efc_params.pos;
		return true;
	}

	prev_pos = efc_params.pos;
	return false;
}

#pragma endregion
