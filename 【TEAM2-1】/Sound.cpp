#include "Sound.h"
#include <assert.h>
#include <vector>
//TODO インデントをi以外のやつに書き直す

void Sound::DeleteAllSoundHandles()
{
	for (auto&& it_bgm : BGM)
		DeleteSoundMem(it_bgm);

	for (auto&& it_se : SE)
		DeleteSoundMem(it_se);
}

void Sound::loadSounds()
{
	//最大音量の初期化

	maxVolBGM = 255;

	//音のロード

	BGM[BGM_TITLE] = LoadSoundMem("Data/Sounds/BGM/title.mp3");
	BGM[BGM_GAME] = LoadSoundMem("Data/Sounds/BGM/stage.mp3");
	BGM[BGM_REST] = LoadSoundMem("Data/Sounds/BGM/relay.mp3");
	BGM[BGM_BOSS] = LoadSoundMem("Data/Sounds/BGM/boss.mp3");
	BGM[BGM_BOSS_WIND] = LoadSoundMem("Data/Sounds/BGM/wind.mp3");
	BGM[BGM_BOSS_BACK] = LoadSoundMem("Data/Sounds/BGM/boss_back.mp3");
	BGM[BGM_CLEAR] = LoadSoundMem("Data/Sounds/BGM/clear.mp3");
	BGM[BGM_OVER] = LoadSoundMem("Data/Sounds/BGM/over.mp3");
	SE[SE_PL_LSWORD_1ST] = LoadSoundMem("Data/Sounds/SE/player/largesword.wav");
	SE[SE_PL_LSWORD_2ND] = LoadSoundMem("Data/Sounds/SE/player/largesword2.wav");
	SE[SE_PL_LSWORD_3RD] = LoadSoundMem("Data/Sounds/SE/player/largesword3.wav");
	SE[SE_PL_BOW_1ST] = LoadSoundMem("Data/Sounds/SE/player/bow.wav");
	SE[SE_PL_BOW_2ND] = LoadSoundMem("Data/Sounds/SE/player/bow2.wav");
	SE[SE_PL_BOW_3RD] = LoadSoundMem("Data/Sounds/SE/player/bow3.wav");
	SE[SE_PL_HAMMER_1ST] = LoadSoundMem("Data/Sounds/SE/player/hammer.wav");
	SE[SE_PL_SPEAR_1ST] = LoadSoundMem("Data/Sounds/SE/player/spear.wav");
	SE[SE_PL_SPEAR_2ND] = LoadSoundMem("Data/Sounds/SE/player/spear2.wav");
	SE[SE_PL_TOMAHAWK_1ST] = LoadSoundMem("Data/Sounds/SE/player/tomahawk.wav");
	SE[SE_PL_JUMP] = LoadSoundMem("Data/Sounds/SE/player/P_jump.wav");
	SE[SE_PL_GET] = LoadSoundMem("Data/Sounds/SE/player/get.wav");
	//SE[SE_PL_RUN] = LoadSoundMem("Data/Sounds/SE/player/PL_run.mp3");
	//SE[SE_PL_WALK] = LoadSoundMem("Data/Sounds/SE/player/PL_walk.mp3");
	SE[SE_PL_DODGE] = LoadSoundMem("Data/Sounds/SE/player/P_rolling.wav");
	SE[SE_PL_DAMAGE] = LoadSoundMem("Data/Sounds/SE/player/P_damage.wav");
	SE[SE_TITLE_START] = LoadSoundMem("Data/Sounds/SE/title_start.mp3");
	SE[SE_E_Slim] = LoadSoundMem("Data/Sounds/SE/enemy/E_Slime.wav");
	SE[SE_E_B_Skel] = LoadSoundMem("Data/Sounds/SE/enemy/E_archer.wav");
	SE[SE_E_N_Skel] = LoadSoundMem("Data/Sounds/SE/enemy/E_knight.wav");
	SE[SE_E_DEATH] = LoadSoundMem("Data/Sounds/SE/Enemy/E_death.wav");
	SE[SE_E_MISSILE_HIT] = LoadSoundMem("Data/Sounds/SE/player/arrow_hit.wav");
	SE[SE_OBJ_DOOR] = LoadSoundMem("Data/Sounds/SE/object/door.wav");
	SE[SE_OBJ_BREAK] = LoadSoundMem("Data/Sounds/SE/object/door_break.wav");
	SE[SE_OBJ_DROPFLOOR] = LoadSoundMem("Data/Sounds/SE/object/dropfloor.wav");
	SE[SE_OBJ_ELEVATOR] = LoadSoundMem("Data/Sounds/SE/object/elevatorswitch.wav");
	SE[SE_OBJ_TEREPORTER] = LoadSoundMem("Data/Sounds/SE/object/teleport.wav");
	SE[SE_OBJ_TREASUREBOX] = LoadSoundMem("Data/Sounds/SE/object/treasure_open.wav");
	SE[SE_OBJ_TELEPORT_START] = LoadSoundMem("Data/Sounds/SE/object/teleport_light.wav");
	SE[SE_BOSS_DOWN] = LoadSoundMem("Data/Sounds/SE/boss/swingDOWN.wav");
	SE[SE_BOSS_UP] = LoadSoundMem("Data/Sounds/SE/boss/swingUP.wav");
	SE[SE_BOSS_RASH] = LoadSoundMem("Data/Sounds/SE/boss/Rush.wav");
	SE[SE_BOSS_ROCK] = LoadSoundMem("Data/Sounds/SE/boss/rock.wav");
	SE[SE_BOSS_DEATH] = LoadSoundMem("Data/Sounds/SE/boss/boss_death.wav");
	SE[SE_SYSTEM_CANSEL] = LoadSoundMem("Data/Sounds/SE/system/cancel.wav");
	SE[SE_SYSTEM_DECISION] = LoadSoundMem("Data/Sounds/SE/system/decision.wav");
	SE[SE_SYSTEM_GAMESTART] = LoadSoundMem("Data/Sounds/SE/system/gamestart.wav");
	SE[SE_SYSTEM_PAUSE] = LoadSoundMem("Data/Sounds/SE/system/pause.wav");
	SE[SE_SYSTEM_SELECT] = LoadSoundMem("Data/Sounds/SE/system/select.wav");
	SE[SE_SYSTEM_COIN] = LoadSoundMem("Data/Sounds/SE/system/coin.wav");
	SE[SE_SYSTEM_BAD] = LoadSoundMem("Data/Sounds/SE/system/bad.wav");
	for (int i_BGM = 0; i_BGM < BGM_END; i_BGM++)
	{
		if (BGM[i_BGM] == -1)
		{
			printfDx("%d番目のロードがエラー", i_BGM + 1);
		}
	}

	for (int i_SE = 0; i_SE < BGM_END; i_SE++)
	{
		if (SE[i_SE] == -1)
		{
			printfDx("%d番目のロードがエラー", i_SE + 1);
		}
	}


}


//-----------------------------------------------------------//
//							BGM			   					 //
//-----------------------------------------------------------//

//BGMを流す(今流れているBGMは止める)
void Sound::playBGM(int _bgmNum)
{
	for (int i = 0; i < BGM_END; i++)
	{
		if (CheckSoundMem(BGM[i]))
			StopSoundMem(BGM[i]);
	}

	PlaySoundMem(BGM[_bgmNum], DX_PLAYTYPE_LOOP, true);

}

//BGMをフェードインで流す
bool Sound::fadeinBGM(int _bgmNum)
{
	int volume = GetVolumeSoundMem2(BGM[_bgmNum]);

	if (CheckSoundMem(BGM[_bgmNum]) == 0)
	{
		for (int i = 0; i < BGM_END; i++)
		{
			if (CheckSoundMem(BGM[i]))
				StopSoundMem(BGM[i]);
		}

		ChangeVolumeSoundMem(0, BGM[_bgmNum]);
		volume = 0;
		PlaySoundMem(BGM[_bgmNum], DX_PLAYTYPE_LOOP);
	}
	else if (volume < maxVolBGM)
	{
		volume += 5;

		ChangeVolumeSoundMem(volume, BGM[_bgmNum]);
	}

	if (volume < maxVolBGM)
		return false;

	else
		return true;

}

bool Sound::fadeinBGM(int _bgmNum1, bool _fadeOut)
{
	int volume1 = GetVolumeSoundMem2(BGM[_bgmNum1]);
	static std::vector<int> otherVol;
	static std::vector<int> otherIndex;
	static int count;

	if (CheckSoundMem(BGM[_bgmNum1]) == 0)
	{
		count = 0;
		volume1 = 0;
		PlaySoundMem(BGM[_bgmNum1], DX_PLAYTYPE_LOOP);
		if (_fadeOut)
		{
			for (int i = 0; i < BGM_END; i++)
			{
				if (i == _bgmNum1)
					continue;
				if (CheckSoundMem(BGM[i]) == 0)
					continue;
				count++;
				otherIndex.emplace_back();
				otherIndex.back() = i;
				otherVol.emplace_back();
				otherVol.back() = GetVolumeSoundMem2(BGM[i]);
			}
		}
	}
	else if (volume1 < maxVolBGM)
	{
		volume1 += 5;
		ChangeVolumeSoundMem(volume1, BGM[_bgmNum1]);
	}

	if (CheckSoundMem(BGM[_bgmNum1]) != 0 && _fadeOut)
	{
		for (int i = 0; i < count; i++)
		{
			otherVol[i] -= 5;
			ChangeVolumeSoundMem(otherVol[i], BGM[otherIndex[i]]);

			if (otherVol[i] <= 0)
				StopSoundMem(BGM[otherIndex[i]]);
		}
	}

	if (volume1 < maxVolBGM)
		return false;

	else
	{
		if (!otherIndex.empty())
			otherIndex.clear();

		if (!otherVol.empty())
			otherVol.clear();

		return true;
	}
}


//フェードアウトで流れているBGMを止める
bool Sound::fadeoutBGM(int _bgmNum)
{
	int volume = GetVolumeSoundMem2(BGM[_bgmNum]);

	if (CheckSoundMem(_bgmNum) == 0)
		assert(0 && "BGMが流れていません");

	if (volume > 0)
	{
		volume -= 5;

		ChangeVolumeSoundMem(volume, BGM[_bgmNum]);
	}

	if (volume > 0)
		return false;

	else
	{
		StopSoundMem(BGM[_bgmNum]);
		return true;
	}
}


//流れているすべてのBGMを止める
void Sound::stopBGM()
{
	bool flg = false;
	for (int i = 0; i < BGM_END; i++)
	{
		if (CheckSoundMem(BGM[i]) == 1)
			flg = true;
	}

	if (!flg)
		assert(0 && "BGMが流れていません");

	for (int i = 0; i < BGM_END; i++)
	{
		if (CheckSoundMem(BGM[i]))
			StopSoundMem(BGM[i]);
	}
}


//流れている指定のBGMを止める(一時停止に使用)
void Sound::poseBGM(int _bgmNum)
{
	if (CheckSoundMem(_bgmNum) == 0)
		assert(0 && "指定のBGMが流れていません");

	StopSoundMem(BGM[_bgmNum]);
}


//指定のBGMを途中から流す
void Sound::restartBGM(int _bgmNum)
{
	PlaySoundMem(BGM[_bgmNum], DX_PLAYTYPE_LOOP, false);
}


//全BGMの音量を変える(引数は0〜255 それ以外の場合は自動で補正)
void Sound::setVolBGM(int _bgmVol)
{
	if (_bgmVol < 0)
		maxVolBGM = 0;

	else if (_bgmVol > 255)
		maxVolBGM = 255;

	else
		maxVolBGM = _bgmVol;



	for (int i = 0; i < BGM_END; i++)
	{
		ChangeVolumeSoundMem(_bgmVol, BGM[i]);
	}
}



//-----------------------------------------------------------//
//							SE			   					 //
//-----------------------------------------------------------//

//SEを再生する
void Sound::playSE(int seNum, bool _isLoop)
{
	if (_isLoop)
	{
		PlaySoundMem(SE[seNum], DX_PLAYTYPE_LOOP, true);
	}
	else
	{
		PlaySoundMem(SE[seNum], DX_PLAYTYPE_BACK, true);
	}
}

void Sound::stopSE(int _setNum)
{
	StopSoundMem(_setNum);
}

int Sound::checkSE(int _setNum)
{
	return CheckSoundMem(_setNum);
}

//全SEの音量を変える(引数は0〜255 それ以外の場合は自動で補正)
void Sound::setVolSE(int _seVol)
{
	int volume;

	if (_seVol < 0)
		volume = 0;

	else if (_seVol > 255)
		volume = 255;

	else
		volume = _seVol;



	for (int i = 0; i < SE_END; i++)
	{
		ChangeVolumeSoundMem(volume, SE[i]);
	}
}