#pragma once

class Sound
{
public:
	enum BGM_ID
	{
		BGM_TITLE = 0,
		BGM_GAME,
		BGM_REST,
		BGM_BOSS_WIND,
		BGM_BOSS_BACK,
		BGM_BOSS,
		BGM_GAME1,
		BGM_OVER,
		BGM_CLEAR,
		BGM_wwww,
		BGM_END,
	};

	enum SE_ID
	{
		SE_PL_LSWORD_1ST,
		SE_PL_LSWORD_2ND,
		SE_PL_LSWORD_3RD,
		SE_PL_SPEAR_1ST,
		SE_PL_SPEAR_2ND,
		SE_PL_SPEAR_3RD,
		SE_PL_BOW_1ST,
		SE_PL_BOW_2ST,
		SE_PL_BOW_3ST,
		SE_PL_HAMMER_1ST,
		SE_PL_HAMMER_2ST,
		SE_PL_HAMMER_3ST,
		SE_PL_TOMAHAWK_1ST,
		SE_PL_TOMAHAWK_2ST,
		SE_PL_TOMAHAWK_3ST,
		SE_PL_BOW_2ND,
		SE_PL_BOW_3RD,
		SE_PL_JUMP,
		SE_PL_RUN,
		SE_PL_WALK,
		SE_PL_DODGE,
		SE_PL_GET,
		SE_PL_DAMAGE,
		SE_TITLE_START,
		SE_E_B_Skel,
		SE_E_N_Skel,
		SE_E_Slim,
		SE_E_DEATH,
		SE_E_MISSILE_HIT,
		SE_OBJ_DOOR,
		SE_OBJ_BREAK,
		SE_OBJ_DROPFLOOR,
		SE_OBJ_ELEVATOR,
		SE_OBJ_TEREPORTER,
		SE_OBJ_TREASUREBOX,
		SE_OBJ_TELEPORT_START,
		SE_BOSS_DOWN,
		SE_BOSS_UP,
		SE_BOSS_ROCK,
		SE_BOSS_RASH,
		SE_BOSS_DEATH,
		SE_SYSTEM_CANSEL,
		SE_SYSTEM_DECISION,
		SE_SYSTEM_GAMESTART,
		SE_SYSTEM_PAUSE,
		SE_SYSTEM_SELECT,
		SE_SYSTEM_COIN,
		SE_SYSTEM_BAD,
		SE_END,
	};

private:
	Sound() {}
	Sound(Sound& copy) {}
	~Sound(){}
	int BGM[BGM_END];
	int SE[SE_END];
	int maxVolBGM;

public:
	static Sound* getInstance() 
	{
		static Sound instance;
		return &instance;
	}

	void loadSounds();
	//BGM
	void playBGM(int _bgmNum);
	void stopBGM();
	void poseBGM(int _bgmNum);
	void restartBGM(int _bgmNum);
	void setVolBGM(int _bgmVol);
	bool fadeoutBGM(int _bgmNum);//finish = true, else = false
	bool fadeinBGM(int _bgmNum);//finish = true, else = false
	bool fadeinBGM(int _bgmNum1,bool fadeOut);//finish = true, else = false
	//SE
	void playSE(int _seNum, bool _isLoop);
	void stopSE(int _seNum);
	int checkSE(int _setNum);

	void setVolSE(int _seVol);

	void DeleteAllSoundHandles();
};

#define pSound Sound::getInstance()
