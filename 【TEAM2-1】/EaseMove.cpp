#include "EaseMove.h"

#include <d3d11.h>
#include "SimpleMath.h"
using namespace DirectX::SimpleMath;

#include <string>

#include "./src/imgui.h"

void DelayMover::ImGuiTreeNode(const std::string & _tree_node_rabel)
{
	ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(_tree_node_rabel.c_str()))
	{
	//	ImGui::DragFloat	(label														   , &v					, speed			, min				, max		, format, power);
		ImGui::DragFloat	(std::string("pos##"				+ _tree_node_rabel).c_str(), &val				, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("target_pos##"			+ _tree_node_rabel).c_str(), &target_val		, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("delay_move_speed##"	+ _tree_node_rabel).c_str(), &delay_move_speed	, 0.001f		, 0.f				, 1.f		, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("difference_margin##"	+ _tree_node_rabel).c_str(), &difference_margin , FLT_EPSILON	, FLT_EPSILON*2.f	, 0.08f		, "%.10f", 1.f);

		ImGui::TreePop();
	}
}

void EaseMover::SetEasingFunction(const int& _ease_algo)
{
	ease_algo = (Easing::eAlgs)_ease_algo;
}

void EaseMover::StartEasing(const float& _target_val, const float& _ease_time_addition_rate)
{
	target_val = _target_val;
	ease_val_min = val;
	ease_time_now = 0.f;
	ease_time_addition_rate = _ease_time_addition_rate;
}

// イージングの進行度を設定します

void EaseMover::SetEasingProgress(const float& _ease_time)
{
	ease_time_now = _ease_time;
	if (ease_algo != Easing::eAlgs::EASINGEND)
	{
		val = Easing::easing_functions[ease_algo](ease_time_now, ease_totaltime, target_val, ease_val_min);
	}
}

bool EaseMover::Update()
{
	if (is_use_ease)
	{
		if (ease_time_now <= ease_totaltime)
		{
			if (ease_algo != Easing::eAlgs::EASINGEND)
			{
				val = Easing::easing_functions[ease_algo](ease_time_now, ease_totaltime, target_val, ease_val_min);
				ease_time_now += ease_time_addition_rate;
				is_update_completed = false;
				return true;
			}
		}
		else
		{
			ease_time_now = ease_totaltime;
			is_update_completed = true;
			return false;
		}
	}
	else//DelayMove
	{
		return DelayMover::Update();
	}
	return false;
}

void EaseMover::ImGuiTreeNode(const std::string & _tree_node_rabel)
{
	ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(_tree_node_rabel.c_str()))
	{
	//	ImGui::DragFloat	(label																, &v						, speed			, min				, max		, format, power);
		// DelayMover
		ImGui::DragFloat	(std::string("pos##"					 + _tree_node_rabel).c_str(), &val						, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("target_pos##"				 + _tree_node_rabel).c_str(), &target_val				, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("delay_move_speed##"		 + _tree_node_rabel).c_str(), &delay_move_speed			, 0.001f		, 0.f				, 1.f		, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("difference_margin##"		 + _tree_node_rabel).c_str(), &difference_margin		, FLT_EPSILON	, FLT_EPSILON*2.f	, 0.08f		, "%.10f", 1.f);
																																										 
		// EaseMover																																					 
		ImGui::Checkbox		(std::string("is_use_ease##"			 + _tree_node_rabel).c_str(), &is_use_ease);														 
		ImGui::DragFloat	(std::string("ease_min_pos##"			 + _tree_node_rabel).c_str(), &ease_val_min				, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("ease_time_addition_rate##" + _tree_node_rabel).c_str(), &ease_time_addition_rate	, 0.0001f		, 0.0001f			, 1024.f	, "%f"	, 1.f);
		// ease_algo
		{
			if (ImGui::BeginCombo("ease_algo", Easing::algo_names[ease_algo].c_str())) // コンボメニューに表示する文字
			{
				for (int n = 0; n < (int)Easing::algo_names.size(); n++)
				{
					bool is_selected;
						is_selected = (ease_algo == n);
					if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
						ease_algo = n;
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
		}

		if (ImGui::Button(std::string("StartEasing" + _tree_node_rabel).c_str()))
		{
			StartEasing(target_val, ease_time_addition_rate);
		}

		ImGui::TreePop();
	}
}

bool DelayMoverVec2::Update()
{
	if (fabsf(target_x - x) >= difference_margin || fabsf(target_y - y) >= difference_margin)
	{
		DelayMove(x, target_x, delay_move_speed);
		DelayMove(y, target_y, delay_move_speed);
		is_update_completed = false;
		return true;
	}
	else
	{
		//移動完了
		x = target_x;
		y = target_y;
		is_update_completed = true;
		return false;
	}
}

void DelayMoverVec2::ImGuiTreeNode(const std::string & _tree_node_rabel)
{
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(_tree_node_rabel.c_str()))
	{
		float* p_pos[2]			= { &x,&y };
		float* p_target_pos[2]	= { &target_x,&target_y };

	//	ImGui::DragFloat	(label																, &v						, speed			, min				, max		, format, power);
		// DelayMover
		ImGui::DragFloat2	(std::string("pos##"					 + _tree_node_rabel).c_str(), *p_pos					, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat2	(std::string("target_pos##"				 + _tree_node_rabel).c_str(), *p_target_pos				, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("delay_move_speed##"		 + _tree_node_rabel).c_str(), &delay_move_speed			, 0.001f		, 0.f				, 1.f		, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("difference_margin##"		 + _tree_node_rabel).c_str(), &difference_margin		, FLT_EPSILON	, FLT_EPSILON*2.f	, 0.08f		, "%.10f", 1.f);

		ImGui::TreePop();
	}
}

void EaseMoverVec2::SetEasingFunction(const int& _ease_algo)
{
	ease_algo = (Easing::eAlgs)_ease_algo;
}

void EaseMoverVec2::StartEasing
(
	const float& _target_x,
	const float& _target_y,
	const float& _ease_time_addition_rate
)
{
	target_x = _target_x;
	target_y = _target_y;
	ease_min_x = x;
	ease_min_y = y;
	ease_time_now = 0.f;
	ease_time_addition_rate = _ease_time_addition_rate;
}

void EaseMoverVec2::StartEasingAtVector2(const Vector2& _pos, const float& _ease_time_addition_rate)
{
	StartEasing(_pos.x, _pos.y, _ease_time_addition_rate);
}

// イージングの進行度を設定します

void EaseMoverVec2::SetEasingProgress(const float& _ease_time)
{
	ease_time_now = _ease_time;
	if (ease_algo != Easing::eAlgs::EASINGEND)
	{
		x = Easing::easing_functions[ease_algo](ease_time_now, ease_totaltime, target_x, ease_min_x);
		y = Easing::easing_functions[ease_algo](ease_time_now, ease_totaltime, target_y, ease_min_y);
	}
}

////移動完了時にfalseを返します
bool EaseMoverVec2::Update()
{
	if (is_use_ease)
	{
		if (ease_time_now < ease_totaltime)
		{
			if (ease_algo != Easing::eAlgs::EASINGEND)
			{
				x = Easing::easing_functions[ease_algo](ease_time_now, ease_totaltime, target_x, ease_min_x);
				y = Easing::easing_functions[ease_algo](ease_time_now, ease_totaltime, target_y, ease_min_y);
				ease_time_now += ease_time_addition_rate;
				is_update_completed = false;
				return true;
			}
		}
		else
		{
			//移動完了
			ease_time_now = ease_totaltime;
			is_update_completed = true;
			return false;
		}
	}
	else//DelayMove
	{
		return DelayMoverVec2::Update();
	}
	return false;
}

void EaseMoverVec2::ImGuiTreeNode(const std::string& _tree_node_rabel)
{
	ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(_tree_node_rabel.c_str()))
	{
		float* p_pos[2]			= { &x,&y };
		float* p_target_pos[2]	= { &target_x,&target_y };
		float* p_ease_min[2]	= { &ease_min_x,&ease_min_y };

	//	ImGui::DragFloat	(label																, &v						, speed			, min				, max		, format, power);
		// DelayMover
		ImGui::DragFloat2	(std::string("pos##"					 + _tree_node_rabel).c_str(), *p_pos					, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat2	(std::string("target_pos##"				 + _tree_node_rabel).c_str(), *p_target_pos				, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("delay_move_speed##"		 + _tree_node_rabel).c_str(), &delay_move_speed			, 0.001f		, 0.f				, 1.f		, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("difference_margin##"		 + _tree_node_rabel).c_str(), &difference_margin		, FLT_EPSILON	, FLT_EPSILON*2.f	, 0.08f		, "%.10f", 1.f);

		// EaseMover
		ImGui::Checkbox		(std::string("is_use_ease##"			 + _tree_node_rabel).c_str(), &is_use_ease);
		ImGui::DragFloat2	(std::string("ease_min_pos##"			 + _tree_node_rabel).c_str(), *p_ease_min				, 1.f			, -FLT_MAX			, FLT_MAX	, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("ease_time_addition_rate##" + _tree_node_rabel).c_str(), &ease_time_addition_rate	, 0.0001f		, 0.0001f			, 1024.f	, "%f"	, 1.f);
		// ease_algo
		{
			if (ImGui::BeginCombo("ease_algo", Easing::algo_names[ease_algo].c_str())) // コンボメニューに表示する文字
			{
				for (int n = 0; n < (int)Easing::algo_names.size(); n++)
				{
					bool is_selected;
						is_selected = (ease_algo == n);
					if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
						ease_algo = n;
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
		}

		if (ImGui::Button(std::string("StartEasing" + _tree_node_rabel).c_str()))
		{
			StartEasing(target_x, target_y, ease_time_addition_rate);
		}

		ImGui::TreePop();
	}
}

bool DelayColor::Update()
{
	if 
	(
		fabsf(target_col.x - col.x) >= 0.08f ||
		fabsf(target_col.y - col.y) >= 0.08f ||
		fabsf(target_col.z - col.z) >= 0.08f ||
		fabsf(target_col.w - col.w) >= 0.08f
	)
	{
		DelayMove(col.x, target_col.x, delay_move_speed);
		DelayMove(col.y, target_col.y, delay_move_speed);
		DelayMove(col.z, target_col.z, delay_move_speed);
		DelayMove(col.w, target_col.w, delay_move_speed);

		is_update_completed = false;
		return true;
	}
	else
	{
		//移動完了
		col.x = target_col.x;
		col.y = target_col.y;
		col.z = target_col.z;
		col.w = target_col.w;

		is_update_completed = true;
		return false;
	}
}

void DelayColor::ImGuiTreeNode(const std::string & _tree_node_rabel)
{
	ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
	if (ImGui::TreeNode(_tree_node_rabel.c_str()))
	{
		// target_col
		{
			FitTo(target_col.x, 255.f, 1.f, 0.f);
			FitTo(target_col.y, 255.f, 1.f, 0.f);
			FitTo(target_col.z, 255.f, 1.f, 0.f);
			FitTo(target_col.w, 255.f, 1.f, 0.f);
			{
				float* p_col[4] =
				{
					&target_col.x,
					&target_col.y,
					&target_col.z,
					&target_col.w
				};
				ImGui::ColorPicker4(std::string("target_col##" + _tree_node_rabel).c_str(), *p_col, 0);
			}
			FitTo(target_col.x, 1.f, 255.f, 0.f);
			FitTo(target_col.y, 1.f, 255.f, 0.f);
			FitTo(target_col.z, 1.f, 255.f, 0.f);
			FitTo(target_col.w, 1.f, 255.f, 0.f);
		}

		// col
		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode(std::string("col##treenode" + _tree_node_rabel).c_str()))
		{
			if (ImGui::Button("col = target_col;"))col = target_col;

			FitTo(col.x, 255.f, 1.f, 0.f);
			FitTo(col.y, 255.f, 1.f, 0.f);
			FitTo(col.z, 255.f, 1.f, 0.f);
			FitTo(col.w, 255.f, 1.f, 0.f);
			{
				float* p_col[4] =
				{
					&col.x,
					&col.y,
					&col.z,
					&col.w
				};
				ImGui::ColorPicker4(std::string("col##" + _tree_node_rabel).c_str(), *p_col, 0);
			}
			FitTo(col.x, 1.f, 255.f, 0.f);
			FitTo(col.y, 1.f, 255.f, 0.f);
			FitTo(col.z, 1.f, 255.f, 0.f);
			FitTo(col.w, 1.f, 255.f, 0.f);

			ImGui::TreePop();
		}

		ImGui::DragFloat	(std::string("delay_move_speed##"		 + _tree_node_rabel).c_str(), &delay_move_speed			, 0.001f		, 0.f				, 1.f		, "%f"	, 1.f);
		ImGui::DragFloat	(std::string("difference_margin##"		 + _tree_node_rabel).c_str(), &difference_margin		, FLT_EPSILON	, FLT_EPSILON*2.f	, 0.08f		, "%.10f", 1.f);

		ImGui::TreePop();
	}
}
