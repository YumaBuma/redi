#include "Enemy.h"
#include "Mo2_BG.h"
#include "ObjectManager.h"
#include "Texture.h"
#include "common.h"

int E_N_Suicider::personal_counter = 0;


E_N_Suicider::E_N_Suicider()
{
	id.CLASS = E_N_SUICIDER;
	id.PERSONAL = personal_counter;
	personal_counter++;
}

E_N_Suicider::~E_N_Suicider()
{
	personal_counter--;
}

void E_N_Suicider::Init()
{
	{
		//Vector2 vec;
		//vec.x = pObjManager->getData(PLAYER, 0)->get()->pos.x - pos.x;
		//vec.y = pObjManager->getData(PLAYER, 0)->get()->pos.y - pos.y;

		//if (vec.x == 0 && vec.y == 0) vec.x += 0.00001; //atan2エラー防止
		//angle = ToDegrees(atan2f(vec.y, vec.x));
	}


	//pos = { 0,0 };
	speed = { 0,0 };
	MAX_SPEED.x = 10.f;
	MAX_SPEED.y = 15.f;
	INITIAL_SPEED.x = 16.f;
	INITIAL_SPEED.y = 16.f;
	act_interval = 0;
	isActive = false;

	loadAnimFile("E_Slime");
	size = { 100, 100 };


	////tex_size = { 168, 196 };
	//size = { 50, 100 };
}

bool E_N_Suicider::Update()
{

	if (Iwin_flg.objBG || Iwin_flg.enemy) return true;

	//if (isActive)
	//{

	//}
	//else
	//{
	//	walkAlg();
	//}
	//searchAlg();


	//if (!act_interval)
	{
	
		//walkAlg();

		act_interval = 3;
	}


	PosUpdateX();

	PosUpdateY();


	act_interval--;
	if (act_interval <= 0)
	{
		act_interval = 0;
		//if (flipX_flg)
		//{
		//	isFlipX ^= (bool)1;
		//	flipX_flg = false;
		//}
	}

	return 1;
}

void E_N_Suicider::Draw() const
{
#if _DEBUG // 縮小表示への対応
	DrawRectRotaGraph2
	(
		cam.xi_ext(pos.x - tex_size.x * 0.5f),
		cam.yi_ext(pos.y - tex_size.y),
		scasi(tex_pos.x) + tex_size.x * aFrame,
		scasi(tex_pos.y) + tex_size.y * now_anim,
		tex_size.x,
		tex_size.y,
		0, 0,
		cam.chipextrate_d(),
		0.0,
		pTexture->getGraphs(Texture::ENEMY),
		TRUE, isFlipX);
#else
	DrawRectGraph(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
		tex_pos.x + tex_size.x * aFrame,
		tex_pos.y + tex_size.y * now_anim,
		tex_size.x, tex_size.y, pTexture->getGraphs(Texture::ENEMY), TRUE, isFlipX);
#endif

}

void E_N_Suicider::walkAlg(const Object* _pl)
{
	//if (target - angle >= 0 && target - angle< 180) angle += 5;
	//else if (target - angle >= 180 && target - angle < 360) angle -= 5;
	//else if (target - angle <    0 && target - angle > -180) angle -= 5;
	//else if (target - angle <= -180 && target - angle > -360) angle += 5;
	//360度に直す
	if (angle <= 0) angle += 360;
	else if (angle>360) angle -= 360;
	//移動量計算
	speed.x = cosf(ToRadians(angle))*INITIAL_SPEED.x;
	speed.y = sinf(ToRadians(angle))*INITIAL_SPEED.y;
}

void E_N_Suicider::searchAlg(float _dist)
{
	//static char lost_sight = 0;
	//Vector2 vec;
	//vec.x = pObjManager->getData(PLAYER, 0)->get()->pos.x - pos.x;
	//vec.y = pObjManager->getData(PLAYER, 0)->get()->pos.y - pos.y;
	//
	//if (vec.x == 0 && vec.y == 0) vec.x += 0.00001; //atan2エラー防止
	//target = ToDegrees(atan2f(vec.y, vec.x));

	//if (target <= 0) target += 360;
	//else if (target > 360) target -= 360;

}

void E_N_Suicider::Gravity()
{
	onGround = false;

	speed.y += GRAVITY;
	if (speed.y >= MAX_SPEED.y)
	{
		speed.y = MAX_SPEED.y;
	}
}

void E_N_Suicider::checkAreaX()
{
	if (delta.x < 0)
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			mapHoseiRight(this);
			act_interval = 30;
			isFlipX = false;
		}
	}

	if (delta.x > 0)
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			mapHoseiLeft(this);
			act_interval = 30;
			isFlipX = true;
		}
	}

}

void E_N_Suicider::checkAreaY()
{
	if (delta.y > 0)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			mapHoseiUp(this);
		}
	}
	if (delta.y < 0)
	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}
	}
}

void E_N_Suicider::animUpdate()
{

}

void E_N_Suicider::Behavior(float _dist)
{

}

void E_N_Suicider::Attack()
{

}
