#pragma once
#include "EffectEmitter.h"

#include <iostream>
#include <cereal\cereal.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\string.hpp>
#include <cereal\types\unordered_map.hpp>

class EffectRoot
{
public:
#pragma region Constructors

	EffectRoot() {}
	EffectRoot
	(
		std::shared_ptr<EMIT_DATASET> _dataset,
		bool _is_static_effect = false,
		const DirectX::SimpleMath::Vector2 _pos = { 0.f,0.f },
		const bool& _is_flip_x = false,
		const DirectX::SimpleMath::Vector2* _p_attract_pos = nullptr
	)
		:is_anim(false), is_static_effect(_is_static_effect), root_pos(_pos), is_stop_emit(false), is_flip_x(false), p_attract_pos(_p_attract_pos)
	{
		SetDataset(_dataset);
		MakeEmitter(_is_flip_x);
	}

	EffectRoot
	(
		std::shared_ptr<ANIM_EMIT_DATASET> _dataset,
		const Animation& _anim,
		bool _is_static_effect = false,
		const DirectX::SimpleMath::Vector2 _pos = { 0.f,0.f },
		const DirectX::SimpleMath::Vector2* _p_attract_pos = nullptr
	)
		:is_anim(true), is_static_effect(_is_static_effect), root_pos(_pos), is_stop_emit(false), is_flip_x(false), p_attract_pos(_p_attract_pos)
	{
		SetAnimDataset(_dataset);
		anim = std::make_shared<Animation>(_anim);
		MakeAnimEmitter();
	}

	EffectRoot
	(
		std::shared_ptr<ANIM_EMIT_DATASET> _dataset,
		std::shared_ptr<Animation> _anim,
		bool _is_static_effect = false,
		const DirectX::SimpleMath::Vector2 _pos = { 0.f,0.f },
		const DirectX::SimpleMath::Vector2* _p_attract_pos = nullptr
	)
		:is_anim(true), is_static_effect(_is_static_effect), root_pos(_pos), is_stop_emit(false), is_flip_x(false), p_attract_pos(_p_attract_pos)
	{
		SetAnimDataset(_dataset);
		anim = _anim;
		MakeAnimEmitter();
	}

#pragma endregion

	~EffectRoot() { p_attract_pos = nullptr; }

private:
	void SetDataset(std::shared_ptr<EMIT_DATASET> _dataset) 
	{
		anim_emit_dset.reset();
		emit_dset		= _dataset;
		is_anim			= false;
	}
	void SetAnimDataset(std::shared_ptr<ANIM_EMIT_DATASET> _dataset)
	{
		emit_dset.reset();
		anim_emit_dset	= _dataset;
		is_anim			= true;
	}


	void MakeEmitter(const bool& _is_flip_x = false);
	void MakeAnimEmitter();

public:
	void ClearEmitters() { {std::vector<std::shared_ptr<EffectEmitter>>().swap(emitters); } }
	void ClearEffects() { for (auto& it_emt : emitters)it_emt->ClearEffects(); }

	const bool IsStaticEffect()const { return is_static_effect; }
	void SetIsStaticEffect(bool _state) { is_static_effect = _state; }
	void SetAttractPosPtr(const DirectX::SimpleMath::Vector2* _p_attract_pos) {	p_attract_pos = _p_attract_pos; }

	bool Update();
	void Draw();

	void ChangeAnimation(const int& _anim_num) { anim->changeAnimation(_anim_num); }

public:
	DirectX::SimpleMath::Vector2				root_pos;
	std::shared_ptr<Animation>					anim;
	bool										is_stop_emit;
	bool										is_flip_x;
private:
	const DirectX::SimpleMath::Vector2*			p_attract_pos;

	bool										is_static_effect; // このフラグがtrueの場合 Updateで falseを返さない
	std::vector<std::shared_ptr<EffectEmitter>> emitters;

	std::shared_ptr<EMIT_DATASET>				     emit_dset;

	bool										is_anim;
	std::shared_ptr<ANIM_EMIT_DATASET>			anim_emit_dset;
};
