#pragma once
#include "common.h"
#include "Object.h"
#include <string>
#include <iostream>
#include <fstream>

const int CHIP_TEXDIV_X = 14;
const int CHIP_TEXDIV_Y = 13;
const int CHIP_SIZE = 32;
const int GROUND_CHIP = 14 * 7;
enum STAGE_ID
{
	ST_1ST,
	ST_2ND,
	ST_3RD,
	ST_END,
};

class Mo2_BG
{
public:
	~Mo2_BG();
	void Init();
	void Update();
	void Draw();
	void DrawBg();
	void DrawRader();
	void drawFog();
	//void setMAP(STAGE_ID st_id);

	//マップのロード(ステージが変わるタイミングで呼ぶやつ)
	void LoadFile(int st_id);
	
	Vector2 pos;
	STAGE_ID now_stage;
	int stageNumber;
	int groundPos;//地上のマップチップの場所
	std::vector<Vector3> Unevenness;
	INT2 getMapSize() { return mapSize; }
	int** getMap() { return map; }
	void createUnevenness();
	void ImGui();
	int getStageNumber()
	{
		return stageNumber;
	}
	void saveFile();
	void resetData();
	void UnInit()
	{
		Unevenness.erase(Unevenness.begin(), Unevenness.end());

		if (Unevenness.size() != 0)
		{
			Unevenness.erase(Unevenness.begin(), Unevenness.end());
		}

		if (map)
		{
			delete map;
			map = nullptr;
		}
	}


	//シングルトン
	static Mo2_BG* getInstance()
	{
		static Mo2_BG instance;

		return &instance;
	}

private://112

	//チップ格納用の変数
	//合成後
	int** map;
	int  collect;

	INT2 chip;//チップの縦横の数
	INT2 mapSize;//マップの広さ

	Vector2 plPos;
	int bgFirstPos[14];
	int bgScrollFin[14];
	INT2 scrollPos[14];//背景のスクロール用
	INT2 scrollStandard[14];

	INT2 mousePos;
	INT2 renderPos;
	int mouseButton[2];

	int fogTimer[2];
	float fogSpeed[2];

	Mo2_BG();
};

#define pBG  Mo2_BG::getInstance()

enum TR_ATTR
{
	TR_NONE = -1,   // -1:何もなし
	ALL_BLOCK = 0,      //  0:四方ブロック
	UPPER_BLOCK,    //  1:上だけブロック
	UPPER_LADDER,
	TR_LADDER,
	MOVEUP_PT,
	MOVEDOWN_PT,
	ENEMY_TURN
};

const TR_ATTR terrainAttr[CHIP_TEXDIV_Y][CHIP_TEXDIV_X] =
{
	{ TR_NONE, TR_NONE, TR_NONE, TR_NONE, ENEMY_TURN, ENEMY_TURN, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, MOVEUP_PT, MOVEDOWN_PT, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK,	TR_NONE, ALL_BLOCK,	ALL_BLOCK,	ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK,	TR_NONE, ALL_BLOCK,	ALL_BLOCK,	ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK,	TR_NONE, TR_NONE, TR_NONE, ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK, UPPER_BLOCK, UPPER_BLOCK, UPPER_BLOCK, UPPER_BLOCK, UPPER_LADDER, UPPER_LADDER, UPPER_LADDER, UPPER_LADDER, },
	
	{ TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },

	{ TR_NONE, TR_NONE, TR_NONE, TR_NONE, ENEMY_TURN, ENEMY_TURN, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, MOVEUP_PT, MOVEDOWN_PT, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK,	TR_NONE, ALL_BLOCK,	ALL_BLOCK,	ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK,	TR_NONE, ALL_BLOCK,	ALL_BLOCK,	ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK,	TR_NONE, TR_NONE, TR_NONE, ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, ALL_BLOCK, TR_LADDER, TR_NONE, TR_NONE, TR_NONE, TR_NONE, TR_NONE, },
	{ TR_NONE, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK, ALL_BLOCK, UPPER_BLOCK, UPPER_BLOCK, UPPER_BLOCK, UPPER_BLOCK, UPPER_LADDER, UPPER_LADDER, UPPER_LADDER, UPPER_LADDER, },
};

int getData(int** map, float x, float y);
int getTerrainAttr(float x, float y);

int isHitAll(float x, float y);
bool isFloor(float x, float y, float width);
bool isUpperFloor(float x, float y, float width);
bool isCeiling(float x, float y, float width);
bool isWall(float x, float y, float height);
bool isTurn(float x, float y, float height);
bool isNone(float x, float y, float height);
bool isLadderUp(float x, float y, float height);
bool isLadderDown(float x, float y);

void mapHoseiDown(Object* obj);
void mapHoseiUp(Object* obj);
void mapHoseiLeft(Object* obj);
void mapHoseiRight(Object* obj);
