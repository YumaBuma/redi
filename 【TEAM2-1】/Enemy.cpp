#include "Enemy.h"
#include "EffectManager.h"

std::string Enemy::damage_efc_filename[Enemy_ID::E_END] =
{
	"E_N_Slime_damage.bin",
	"E_N_Skeleton_damage.bin",
	"E_B_Skeleton_damage.bin",
};

void Enemy::setknockBack(Vector2 _power)
{
	knockBackPower = _power;
}

void Enemy::setDamage(int _noHitTime, int _damage, Vector2 _patarn)
{
	noDamageTimer = _patarn.y;
	hp -= _damage;
	isDamage = true;
	Knck_SPD = { 2,6 };
	NO_MOVE = 6;
	NO_DAMAGE = 1;
	plAttackNum = _patarn.x;
	knockBackState = 0;

	if (id.CLASS != Enemy_ID::E_BOSS)
	{
		EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(damage_efc_filename[id.CLASS]), false, pos, !damage_direction);
	}
}

void Enemy::knockBackEnm()
{
	switch (knockBackState)
	{
	case 0:
		speed = { knockBackPower.x,-knockBackPower.y };
		if (!damage_direction)
		{
			speed.x *= -1;
		}

		stayTimer = 60;
		changeAnimation(0);
		knockBackState++;
		break;

	case 1:
		if (onGround)
		{
			knockBackState++;
			speed.x = 0;
		}
		break;

	case 2:
		isDamage = false;
		noHitTime = 60;
	}

}
