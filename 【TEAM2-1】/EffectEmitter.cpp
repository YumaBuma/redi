#include "EffectEmitter.h"

#include "EffectManager.h"
#include "common.h"
#include "Camera.h"
#include "FunctionDxLib.h"

#include <cereal\cereal.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\string.hpp>
#include <cereal\types\vector.hpp>

#pragma region EffectEmitter

#pragma region constructors

EffectEmitter::EffectEmitter
(
	EFC_TYPE _type, Vector2 _pos, Vector2 _vel,
	Vector2 _efc_vel, float _duration, float _set_speed_per_frame, float _deceleration_rate_mul,
	float _gravity, bool _is_ui_efc,
	float _size,
	const float* _attract_x_ptr,
	const float* _attract_y_ptr
)
{
	emit_params.type = _type;
	emit_params.pos = _pos;
	emit_params.vel = _vel;
	emit_params.efc.vel = _efc_vel;

	emit_params.deceleration_rate_mul = _deceleration_rate_mul;
	emit_params.gravity = _gravity;

	emit_params.set_speed_per_frame = _set_speed_per_frame;
	emit_params.duration = _duration;
	emit_params.duration_max = _duration;

	is_ui_efc = _is_ui_efc;

	emit_params.attract_x_ptr = _attract_x_ptr;
	emit_params.attract_y_ptr = _attract_y_ptr;

	switch (_type)
	{
	case EFC_DEFAULT:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcDefault>(emit_params.InheritFromEmitterParams(), this));
		break;
	}

	case EFC_RANDOM_IMAGE_PARTICLE:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(emit_params.InheritFromEmitterParams(), this));
		break;
	}

	case EFC_JEWERLY:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcJewerly>(emit_params.InheritFromEmitterParams(), this));
		break;
	}

	default:
		break;
	}
}

EffectEmitter::EffectEmitter(const EmitterParams& _emit_params)
{
	emit_params = _emit_params;
	switch (_emit_params.type)
	{
	case EFC_DEFAULT:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcDefault>(emit_params.InheritFromEmitterParams(), this));
		break;
	}

	case EFC_RANDOM_IMAGE_PARTICLE:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(emit_params.InheritFromEmitterParams(), this));
		break;
	}

	case EFC_JEWERLY:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcJewerly>(emit_params.InheritFromEmitterParams(), this));
		break;
	}

	default:
		break;
	}
}

EffectEmitter::EffectEmitter(const EmitterParams& _emit_params, EffectRoot* _parent_root, const bool& _is_flip_x)
{
	parent_root = _parent_root;
	emit_params.pos = parent_root->root_pos;

	emit_params = _emit_params;
	switch (_emit_params.type)
	{
	case EFC_DEFAULT:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcDefault>(emit_params.InheritFromEmitterParams(parent_root->root_pos, _is_flip_x), this, _is_flip_x));
		break;
	}

	case EFC_RANDOM_IMAGE_PARTICLE:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(emit_params.InheritFromEmitterParams(parent_root->root_pos, _is_flip_x), this));
		break;
	}

	case EFC_JEWERLY:
	{
		int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
		for (int i = 0; i < random; i++)
			efcs.emplace_back(std::make_unique<EfcJewerly>(emit_params.InheritFromEmitterParams(parent_root->root_pos, _is_flip_x), this));
		break;
	}

	default:
		break;
	}
}

#pragma endregion

EffectEmitter::~EffectEmitter()
{
	{ std::vector<std::unique_ptr<BaseEffect>>().swap(efcs); }
	emit_params.attract_x_ptr = nullptr;
	emit_params.attract_y_ptr = nullptr;
	parent_root = nullptr;
}

void EffectEmitter::ClearEffects()
{
	{ std::vector<std::unique_ptr<BaseEffect>>().swap(efcs); }// swap技法
}

bool EffectEmitter::Update()
{
	// duration_subtraction_rateが負数の場合は寿命が増えるのでfalseは返らない
	if (emit_params.duration <= 0.f && efcs.empty())
	{
		return false;
	}

	// エミッター位置更新
	Vector2 pos = {};
	{
		emit_params.vel.x *= emit_params.deceleration_rate_mul;
		emit_params.vel.y *= emit_params.deceleration_rate_mul;

		if (emit_params.vel.y < 8.f)
		{
			emit_params.vel.y += (emit_params.gravity);
		}

		emit_params.pos += (emit_params.vel);

		if (emit_params.is_follow_to_root)
			pos = parent_root->root_pos;
	}

	if (parent_root->is_stop_emit == false)
	{

		if (emit_params.set_speed_per_frame >= 1.f)
		{
			if (emit_params.duration >= 0.f)
			{
				if (((int)emit_params.duration % (int)emit_params.set_speed_per_frame) == 0)
				{
					int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
					for (int i = 0; i < random; i++)
					{
						EmitEffect(emit_params.InheritFromEmitterParams(pos, parent_root->is_flip_x));
					}
				}
			}
		}
	}

	//remove_ifについて : https://qiita.com/naka4/items/1f207b5d6acf102b0db7
	auto remove_it = std::remove_if(
		efcs.begin(), efcs.end(),
		[](std::unique_ptr<BaseEffect>& efc)
	{
		return !efc->Update();
	});
	efcs.erase(remove_it, efcs.end());

	//duration_subtraction_rateが負数の場合は寿命が増えるのでfalseは返らない
	emit_params.duration = fmodf(emit_params.duration - emit_params.duration_subtraction_rate, 65535.0f);
	return true;
}

void EffectEmitter::Draw()
{
#if _DEBUG
	//DrawCircleCam(emit_params.pos + parent_root->root_pos, 2, emit_params.efc.col.GetCol());
	//DrawCircleCam(emit_params.pos + parent_root->root_pos, emit_params.radius, emit_params.efc.col.GetCol(), false, 6);
#endif

	if (efcs.empty())return;

	Vector2 pos_prev = efcs.front()->GetPos();
	for (auto& it : efcs)
	{
		it->Draw(pos_prev);
		pos_prev = it->GetPos();
	}

}

void EffectEmitter::SetAttractPos(const float * _attract_x, const float * _attract_y)
{
	emit_params.attract_x_ptr = _attract_x;
	emit_params.attract_y_ptr = _attract_y;
}

void EffectEmitter::EmitEffect(const EffectParams& _params)
{
	EffectParams params = _params;

	switch (_params.type)
	{
	case EFC_DEFAULT:
		efcs.emplace_back(std::make_unique<EfcDefault>(params, this, parent_root->is_flip_x));
		break;

	case EFC_RANDOM_IMAGE_PARTICLE:
		efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(params, this));
		break;

	case EFC_JEWERLY:
		efcs.emplace_back(std::make_unique<EfcJewerly>(params, this));
		break;

	default:
		break;
	}
}

void EffectEmitter::EmitEffect()
{
	switch (emit_params.type)
	{
	case EFC_DEFAULT:
		efcs.emplace_back(std::make_unique<EfcDefault>(emit_params.pos, emit_params.vel));
		break;

	case EFC_RANDOM_IMAGE_PARTICLE:
		efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(emit_params.pos, emit_params.vel));
		break;

	case EFC_JEWERLY:
		efcs.emplace_back(std::make_unique<EfcJewerly>(emit_params.pos, emit_params.vel));
		break;

	default:
		break;
	}
}

void EffectEmitter::EmitEffects(const EmitterParams & _params)
{
	EmitterParams tmp;
	switch (_params.type)
	{
	case EFC_DEFAULT:
	{
		int rand_num = _params.emit_num + GetRand(_params.emit_num_randomize);
		for (int i = 0; i < rand_num; i++)
		{
			tmp.type = _params.type;
			tmp.pos = _params.pos + Vector2(GetRandSigned(-2.f, 2.f), GetRandSigned(-2.f, 2.f));
			tmp.vel = _params.vel + Vector2(cosf(GetRand(100.f)), sinf(GetRand(100.f))) * GetRand(8.f);
			tmp.efc.vel = _params.efc.vel * GetRandSigned(0.2f, 0.6f)/*Vector2(cosf(GetRand(10.f)), sinf(GetRand(10.f)))*/;
			tmp.duration = tmp.duration_max = 20.f + (float)GetRand(100.f);
			tmp.set_speed_per_frame = -1.f;
			tmp.deceleration_rate_mul = 0.9f + GetRandSigned(-0.08f, 0.04f);
			tmp.gravity = -1.2f;
			tmp.attract_x_ptr = _params.attract_x_ptr;
			tmp.attract_y_ptr = _params.attract_y_ptr;
			efcs.emplace_back(std::make_unique<EfcDefault>(tmp.InheritFromEmitterParams(), this));
		}
	}
	break;
	case EFC_RANDOM_IMAGE_PARTICLE:
	{
		int rand_num = _params.emit_num + GetRand(_params.emit_num_randomize);
		for (int i = 0; i < rand_num; i++)
		{
			tmp.type = _params.type;
			tmp.pos = _params.pos + Vector2(GetRandSigned(-2.f, 2.f), GetRandSigned(-2.f, 2.f));
			tmp.vel = _params.vel + Vector2(cosf(GetRand(100.f)), sinf(GetRand(100.f))) * GetRand(8.f);
			tmp.efc.vel = _params.efc.vel * GetRandSigned(0.2f, 0.6f)/*Vector2(cosf(GetRand(10.f)), sinf(GetRand(10.f)))*/;
			tmp.duration = tmp.duration_max = 20.f + (float)GetRand(100.f);
			tmp.set_speed_per_frame = -1.f;
			tmp.deceleration_rate_mul = 0.9f + GetRandSigned(-0.08f, 0.04f);
			tmp.gravity = -1.2f;
			tmp.attract_x_ptr = _params.attract_x_ptr;
			tmp.attract_y_ptr = _params.attract_y_ptr;
			efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(tmp.InheritFromEmitterParams(), this));
		}
	}
	break;
	case EFC_JEWERLY:
	{
		int rand_num = _params.emit_num + GetRand(_params.emit_num_randomize);
		for (int i = 0; i < rand_num; i++)
		{
			tmp.type = _params.type;
			tmp.pos = _params.pos + Vector2(GetRandSigned(-2.f, 2.f), GetRandSigned(-2.f, 2.f));
			tmp.vel = _params.vel + Vector2(cosf(GetRand(100.f)), sinf(GetRand(100.f))) * GetRand(8.f);
			tmp.efc.vel = _params.efc.vel * GetRandSigned(0.2f, 0.6f)/*Vector2(cosf(GetRand(10.f)), sinf(GetRand(10.f)))*/;
			tmp.duration = tmp.duration_max = 20.f + (float)GetRand(100.f);
			tmp.set_speed_per_frame = -1.f;
			tmp.deceleration_rate_mul = 0.9f + GetRandSigned(-0.08f, 0.04f);
			tmp.gravity = -1.2f;
			tmp.attract_x_ptr = _params.attract_x_ptr;
			tmp.attract_y_ptr = _params.attract_y_ptr;
			efcs.emplace_back(std::make_unique<EfcJewerly>(tmp.InheritFromEmitterParams(), this));
		}
	}
	break;

	default:
		break;
	}
}

#pragma endregion

#pragma region AnimationEffectEmitter

bool AnimationEffectEmitter::Update()
{
	static bool is_safe_update = true;// todo: マスタまでに削除
	if(is_safe_update)
	{
		if (key_frame.empty())return false;
		if (animation == false)return false;
		if (animation->animParam.empty())return false;
	}
	if (parent_root->is_stop_emit && efcs.empty())
		return parent_root->IsStaticEffect();

	//エミッター位置更新
	Vector2 pos = emit_params.is_follow_to_root ? parent_root->root_pos : Vector2(0, 0);
	if (key_frame.empty() == false && animation->now_anim != -1)
	{
		//キーフレーム確認
		char now_anim	= animation->now_anim;
		int  aFrame		= animation->getaFrame();
		if (key_frame.at(now_anim).find(aFrame) != key_frame.at(now_anim).end())
		{
			if (passed_key_num != aFrame) 
				is_emitted = false;
			if (is_emit_once.at(now_anim) && is_emitted)
			{
				// do nothing
			}
			else
			{
				if(parent_root->is_stop_emit == false)
				{
					EffectParams params;
					//格納されていた分のエフェクトを発生
					for (auto& it_efc : key_frame.at(now_anim).find(aFrame)->second)// key_frame.at(now_anim).find(aFrame)->second <-EffectParamsのvector
					{
						params = it_efc;
						params.pos.x *= parent_root->is_flip_x ? -1.f : 1.f;
						params.vel.x *= parent_root->is_flip_x ? -1.f : 1.f;
						EmitEffect(params.GetAddPosParams(emit_params.pos + pos));
					}

					is_emitted = true;
				}
			}
			passed_key_num = (int)aFrame;
		}
	}

	if(parent_root->is_stop_emit == false)
	{
		//Emitterの数値を継承して発生
		if (emit_params.set_speed_per_frame >= 1.f)
		{
			if (emit_params.duration >= 0.f)
			{
				if (((int)emit_params.duration % (int)emit_params.set_speed_per_frame) == 0)
				{
					int random = emit_params.emit_num + GetRand(emit_params.emit_num_randomize);
					for (int i = 0; i < random; i++)
					{
						EmitEffect(emit_params.InheritFromEmitterParams(pos, parent_root->is_flip_x));	
					}
				}
			}
		}
	}

	//remove_ifについて : https://qiita.com/naka4/items/1f207b5d6acf102b0db7
	auto remove_it = std::remove_if
	(
		efcs.begin(), efcs.end(),
		[](std::unique_ptr<BaseEffect>& efc)
	{
		return !efc->Update();
	}
	);
	efcs.erase(remove_it, efcs.end());

	emit_params.efc.col.Update();

	//this->Animation::playAnimation();
	// shared_ptrにしたので個々でplayAnimationする必要なし
	//animation->playAnimation();

	emit_params.duration = fmodf(emit_params.duration - emit_params.duration_subtraction_rate, 65535.0f);

	return true;
}

void AnimationEffectEmitter::Draw()
{
	if (efcs.empty())return;

	Vector2 pos_prev = efcs.front()->GetPos();
	for (auto& it : efcs)
	{
		it->Draw(pos_prev);
		pos_prev = it->GetPos();
	}
}

void AnimationEffectEmitter::SetAttractPos(const float * _attract_x, const float * _attract_y)
{
	emit_params.attract_x_ptr = _attract_x;
	emit_params.attract_y_ptr = _attract_y;
	for (auto& it_anim : key_frame)
	{
		for (auto& it_key_frm : it_anim)
		{
			for (auto& it_efc_prams : it_key_frm.second)
			{
				it_efc_prams.attract_x_ptr = _attract_x;
				it_efc_prams.attract_y_ptr = _attract_y;
			}
		}
	}
}

void AnimationEffectEmitter::EmitEffect(const EffectParams& _params)
{
	EffectParams params_tmp = _params;

	switch (_params.type)
	{
	case EFC_DEFAULT:
		efcs.emplace_back(std::make_unique<EfcDefault>(params_tmp, this, parent_root->is_flip_x));
		break;

	case EFC_RANDOM_IMAGE_PARTICLE:
		efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(params_tmp, this));
		break;

	case EFC_JEWERLY:
		efcs.emplace_back(std::make_unique<EfcJewerly>(params_tmp, this));
		break;

	default:
		break;
	}
}

void AnimationEffectEmitter::EmitEffect()
{
	switch (emit_params.type)
	{
	case EFC_DEFAULT:
		efcs.emplace_back(std::make_unique<EfcDefault>(emit_params.pos, emit_params.vel));
		break;

	case EFC_RANDOM_IMAGE_PARTICLE:
		efcs.emplace_back(std::make_unique<EfcRandomImageParticle>(emit_params.pos, emit_params.vel));
		break;

	case EFC_JEWERLY:
		efcs.emplace_back(std::make_unique<EfcJewerly>(emit_params.pos, emit_params.vel));
		break;

	default:
		break;
	}
}

#pragma endregion
