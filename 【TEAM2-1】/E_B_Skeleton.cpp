#include "Enemy.h"
#include "Mo2_BG.h"
#include "ObjectManager.h"
#include "Texture.h"
#include "Sound.h"
#include "missile.h"
#include "EffectManager.h"


int E_B_Skeleton::personal_counter = 0;
int E_B_Skeleton::attackPoint = 0;
int E_B_Skeleton::max_hp = 10;
int E_B_Skeleton::intarval = 200;
Vector2 E_B_Skeleton::backPower = { 0,0 };
float E_B_Skeleton::range[eRange::end] = { 0 };
float E_B_Skeleton::walkSpeed = 0;
float E_B_Skeleton::jumpPower = 0;

void E_B_Skeleton::loadData()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/bSkeleton_info.txt");

	fopen_s(&file, fileName, "r");

	for (int i = 0; i < eRange::end; i++)
	{

		fscanf_s(file, "%f", &range[i]);
	}



	fscanf_s(file, "%f", &walkSpeed);
	fscanf_s(file, "%f", &backPower.x);
	fscanf_s(file, "%f", &backPower.y);
	fscanf_s(file, "%d", &attackPoint);
	fscanf_s(file, "%d", &max_hp);
	fscanf_s(file, "%d", &intarval);


	fclose(file);
	//TODO::HP倍率
	if (pBG->stageNumber == 3)
	{
		max_hp *= 4;
	}

}

E_B_Skeleton::E_B_Skeleton()
{
	id.CLASS = E_B_SKELETON;
	id.PERSONAL = personal_counter;
	personal_counter++;
}

E_B_Skeleton::~E_B_Skeleton()
{
	personal_counter--;
	EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("E_B_Skeleton_dead.bin"), false, pos, isFlipX);

	if (pObjManager->ObjList.empty() == false)
	{
		Vector2* player_pos_ptr = &pObjManager->ObjList[Object::PLAYER].back()->pos;
		for (int i_jew = 0; i_jew < (std::min)(money / 3, 64)/*(各敵のジュエリー発生量)*/; i_jew++)
		{
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("jewerly.bin"), false, pos)->Update();
		}
	}

	back_step_efc_root->ClearEmitters();
	back_step_efc_root->SetIsStaticEffect(false);
}

void E_B_Skeleton::Init()
{
	noDamageTimer = 0;
	attackState = 0;
	nowState = 0;
	plAttackNum = 0;
	isDamage = false;
	stayTimer = 0;
	speed = { 0,0 };
	MAX_SPEED.x = 10.f;
	MAX_SPEED.y = 15.f;
	INITIAL_SPEED.x = 2.f;
	INITIAL_SPEED.y = 0.f;
	act_interval = 0;
	notFound = 500;
	checkSpan = 10;
	walkCount = 0;
	canBackStep = true;
	isGrip = false;
	isErase = false;
	isAnim = true;
	isStay = false;
	isAttack = false;
	onGround = false;
	backFlg = false;
	size = { 30,70 };
	loadAnimFile("E_SkeletonBowman");
	backStepflg = true;
	hp = max_hp;
	temp_hp = hp;
	maxHP = max_hp;
	back_step_efc_root = EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("E_B_Skeleton_back_step.bin"), true, pos);
	back_step_efc_root->is_stop_emit = true;
}

bool E_B_Skeleton::Update()
{
	if (Iwin_flg.objBG || Iwin_flg.enemy) return true;

	if (cam.IsInCameraFoot(pos, Vector2((float)tex_size.x * 0.5f, (float)tex_size.y)) == false)
	{
		hide();
		return true;
	}
	else
		show();

	if (noDamageTimer)
	{
		noDamageTimer--;
	}
	else
	{
		plAttackNum = 0;
	}

	back_step_efc_root->root_pos = pos;
	back_step_efc_root->is_flip_x = isFlipX;
	if (backStepflg && !onGround)
		back_step_efc_root->is_stop_emit = false;
	else
		back_step_efc_root->is_stop_emit = true;

	searchAlg(pObjManager->distanceToPlayer(*this));

	if (nowState < 0)
	{
		m_isVisible = false;
		return true;
	}
	else
		m_isVisible = true;

	if (temp_hp > hp)
		temp_hp -= 2.0f;
	else
		temp_hp = hp;
	Gravity();
	PosUpdateY();

	checkAreaY();

	Behavior(pObjManager->distanceToPlayer(*this));

	PosUpdateX();


	checkAreaX();

	animUpdate();
	return 1;
}

void E_B_Skeleton::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

#if ENABLE_SMALL_DRAW // 縮小表示への対応
	if (noDamageTimer)
		SetDrawBright(255, 0, 0);
	DrawRectRotaGraphFast2
	(
		cam.xi_ext(pos.x - tex_size.x * 0.5f),
		cam.yi_ext(pos.y - tex_size.y),
		scasi(tex_pos.x) + tex_size.x * aFrame,
		scasi(tex_pos.y) + tex_size.y * now_anim,
		tex_size.x,
		tex_size.y,
		0, 0,
		cam.chipextrate_d(),
		0.0,
		pTexture->getGraphs(Texture::ENEMY),
		TRUE, isFlipX
	);
	SetDrawBright(255, 255, 255);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);

#else
	DrawRectGraph(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
		tex_pos.x + tex_size.x * aFrame,
		tex_pos.y + tex_size.y * now_anim,
		tex_size.x, tex_size.y, pTexture->getGraphs(Texture::ENEMY), TRUE, isFlipX);
#endif

	if (show_debug)
		DrawLineBox
		(
			cam.xi_ext(pos.x - size.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y - size.y)/* - camera_pos.y*/,
			cam.xi_ext(pos.x + size.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y) /*- camera_pos.y*/,
			GetColor(255, 5, 80));
}

void E_B_Skeleton::walkAlg(const Object* _pl)
{
	attackState = 0;
	if (!onGround)return;



	if (_pl == nullptr)
	{
		walkCount = 0;
	}

	else
	{
		walkCount++;

		isFlipX = moveDirection(_pl->pos);
	}

	if (isFlipX)
	{
		speed.x = -walkSpeed;
	}
	else
	{
		speed.x = walkSpeed;
	}
	if (isStay && speed.x == 0)
		changeAnimation(bSkeletonAnim::stay);
	else
		changeAnimation(bSkeletonAnim::walk);

}

void E_B_Skeleton::searchAlg(float _dist)
{
	static Vector2 plPos = { 0,0 };
	plPos = pObjManager->ObjList[PLAYER].back()->pos;	
	
	if (backFlg)return;
	
	if (_dist == -1)
	{
		nowState = -1;
		m_isVisible = false;
		return;
	}

	if (stayTimer && onGround)
	{
		nowState = 0;
		return;
	}

	//if (onGround && backStepflg)
	if (canBackStep && onGround && backStepflg || isDamage)
	{
		backStepflg = false;
	}

	if (isFlipX)
	{
		if (getTerrainAttr(pos.x + size.x + 10, pos.y - 10) == ALL_BLOCK)
			behindWall = true;
		else if (getTerrainAttr(pos.x + size.x + backPower.x * 10, pos.y + 10) == TR_NONE)
			behindWall = true;
		else
			behindWall = false;
	}
	else
	{
		if (getTerrainAttr(pos.x - size.x - 10, pos.y - 10) == ALL_BLOCK)
			behindWall = true;
		else if (getTerrainAttr(pos.x - size.x - backPower.x * 10, pos.y + 10) == TR_NONE)
			behindWall = true;
		else
			behindWall = false;
	}

	if (isFlipX && pos.x + size.x * 2 > plPos.x)
		isFind = true;
	else if (!isFlipX && pos.x - size.x * 2 < plPos.x)
		isFind = true;
	else
		isFind = false;


	if ((pos.y >= plPos.y && pos.y - size.y <= plPos.y) && isFind)
	{
		if (_dist <= range[eRange::backStep] && !behindWall && canBackStep && !backFlg)
			nowState = 4;
		else if (_dist <= range[eRange::attack])
			nowState = 3;
		else if (_dist <= range[eRange::track])
			nowState = 2;
	}
	else if (_dist <= range[eRange::move])
		nowState = 1;
	else
		nowState = -1;
}

void E_B_Skeleton::backStep()
{
	if (onGround && !backStepflg)
	{
		stayTimer = intarval;
		attackState = 0;
		backStepflg = true;
		speed.x = 0;
		speed.y = -backPower.y;
		isFlipX = moveDirection(pObjManager->ObjList[Object::PLAYER].back()->pos);
		speed.x = backPower.x;
		if (!isFlipX)speed.x *= -1;
		canBackStep = false;
		backFlg = false;

	}

}

void E_B_Skeleton::Gravity()
{
	onGround = false;

	speed.y += GRAVITY;
	if (speed.y >= MAX_SPEED.y)
	{
		speed.y = MAX_SPEED.y;
	}
}

void E_B_Skeleton::checkAreaX()
{
	if (delta.x < 0)
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			mapHoseiRight(this);
			if (!walkCount)
			{
				isStay = false;
				isFlipX = true;
			}
			else isStay = true;
		}
	}

	if (delta.x > 0)
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			mapHoseiLeft(this);
			if (!walkCount)
			{
				isStay = false;
				isFlipX = false;
			}
			else isStay = true;
		}

	}

}

void E_B_Skeleton::checkAreaY()
{
	if (delta.y > 0)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			mapHoseiUp(this);
		}
	}
	if (delta.y < 0)
	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}

		if (isUpperFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}
	}
	if (onGround)
	{
		if (isFlipX && speed.x < 0)
		{
			if ((isFloor(pos.x, pos.y + 1, size.x*0.5f) || (isUpperFloor(pos.x, pos.y + 1, size.x*0.5f))) && isNone(pos.x - 1, pos.y + 1, size.x*0.5f))
			{
				isFlipX = false;
			}

		}
		else if (!isFlipX && speed.x > 0)
		{
			if ((isFloor(pos.x, pos.y + 1, size.x*0.5f) || (isUpperFloor(pos.x, pos.y + 1, size.x*0.5f))) && isNone(pos.x + 1, pos.y + 1, size.x*0.5f))
			{
				isFlipX = true;
			}

		}

	}

}

void E_B_Skeleton::animUpdate()
{
	playAnimation();

}



void E_B_Skeleton::Behavior(float _dist)
{
	if (isDamage && isAttack)
		isDamage = false;
	if (noHitTime)
	{
		noHitTime--;
		isDamage = false;
	}

	if (isDamage)
	{
		knockBackEnm();
		changeAnimation(bSkeletonAnim::damage);
		attackState = 0;
		isAttack = false;
		return;
	}
	if (!onGround)
		return;

	switch (nowState)
	{
	case -1:
		break;
	case 0:
		speed = { 0,0 };
		attackState = 0;
		changeAnimation(bSkeletonAnim::stay);
		if (stayTimer > 0)
		{
			stayTimer--;
		}
		break;

	case 1:
		walkAlg(nullptr);
		break;

	case 2:
		walkAlg(pObjManager->ObjList[Object::PLAYER].begin()->get());
		break;

	case 3:
		Attack(_dist);
		break;

	case 4:
		backStep();
	}
}

void E_B_Skeleton::Attack(float _dist)
{

	switch (attackState)
	{
	case 0:
		isAttack = true;
		stop_anim = false;
		changeAnimation(bSkeletonAnim::attack);
		isFlipX = moveDirection(pObjManager->ObjList[Object::PLAYER].back()->pos);
		speed.x = 0;
		attackState++;
		break;
	case 1:
		if (aFrame > 3)backFlg = true;

		if (aFrame == 6 && aCnt == 0)
		{
			attackState++;
			pSound->playSE(Sound::SE_E_B_Skel, false);

			pObjManager->Add(std::make_shared<Missile>(attackPoint), Object::MISSILE, layer + 1, 0, 0);
			pObjManager->ObjList[CLASS_ID::MISSILE].back()->pos = { pos.x,pos.y - size.y / 3 * 2 - 5 };
			if (isFlipX)
			{
				pObjManager->ObjList[CLASS_ID::MISSILE].back()->speed = { -4,0 };
				pObjManager->ObjList[CLASS_ID::MISSILE].back()->isFlipX = true;
			}
			else
				pObjManager->ObjList[CLASS_ID::MISSILE].back()->speed = { 4,0 };

			pObjManager->ObjList[CLASS_ID::MISSILE].back()->angle = 0;

		}
		break;
	case 2:
		if (aFrame == 10)
		{
			isFlipX = moveDirection(pObjManager->ObjList[Object::PLAYER].back()->pos);
			attackState = 0;
			isAttack = false;
			stayTimer = intarval;
			canBackStep = true;
			backFlg = false;

		}
		break;
	}

}



Vector2 E_B_Skeleton::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		return Vector2(0, 0);
		break;

	case 1:
		return Vector2(-1, -1);
		break;

	case 2:
		return Vector2(attackPoint, plAttackNum);
		break;
	}
}


void E_B_Skeleton::eImGui()
{
	if (Iwin_flg.e_info[Enemy_ID::E_B_SKELETON] == false)
		return;
	static const char* rKind[eRange::end] =
	{ "move","track","attack","backStep" };

	ImGui::Begin("bSkeleton", &Iwin_flg.e_info[Enemy_ID::E_B_SKELETON], ImGuiWindowFlags_MenuBar);


	for (int i = 0; i < eRange::end; i++)
	{
		ImGui::DragFloat(rKind[i], &range[i]);
	}
	ImGui::DragFloat("walkSpeed", &walkSpeed);
	ImGui::Text("backStepPower");

	ImGui::DragFloat("x", &backPower.x);
	ImGui::DragFloat("y", &backPower.y);
	ImGui::DragInt("AttackPoint", &attackPoint);
	ImGui::DragInt("maxHP", &max_hp);
	ImGui::DragInt("intarval", &intarval);

	ImGui::NewLine();

	if (ImGui::Button("save"))
	{
		saveFile();
	}

	ImGui::End();

}


void E_B_Skeleton::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/bSkeleton_info.txt");

	fopen_s(&file, fileName, "w");

	for (int i = 0; i < eRange::end; i++)
	{

		fprintf_s(file, "%f ", range[i]);
	}

	fprintf_s(file, "%f ", walkSpeed);
	fprintf_s(file, "%f ", backPower.x);
	fprintf_s(file, "%f ", backPower.y);
	fprintf_s(file, "%d ", attackPoint);
	fprintf_s(file, "%d ", max_hp);
	fprintf_s(file, "%d\n", intarval);
	fclose(file);

}
