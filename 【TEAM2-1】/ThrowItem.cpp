#include "ThrowItem.h"
#include "Mo2_BG.h"
#include "Camera.h"
#include "Texture.h"
#include "EffectManager.h"

std::string ThrowItem::throw_item_efc_filenames[5] =
{
	"missile_pl_arrow.bin"
	,"tomahawk_atk.bin"
	,"pl_arrow_break.bin"
	,"tomahawk_break_x.bin"
	,"tomahawk_break_y.bin"
};

ThrowItem::ThrowItem()
{

}

ThrowItem::ThrowItem(int _POWER, Vector2 _atk, int _weapon)
{
	weapon_id = (FLY_WEAPON)_weapon;
	power = _POWER;
	atk = _atk;
}

ThrowItem::~ThrowItem()
{
	throw_item_efc_root->is_stop_emit = true;
	throw_item_efc_root->SetIsStaticEffect(false);
	if (weapon_id == FLY_WEAPON::ARROW)EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(throw_item_efc_filenames[2]), false, pos, isFlipX);
}
void ThrowItem::Init()
{
	isErase = false;
	MAX_SPEED = { 0.0f, 6.0f };

	throw_item_efc_root = EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(throw_item_efc_filenames[weapon_id]), true, pos, isFlipX);
	throw_item_efc_root->ClearEffects();
	throw_item_efc_root->is_stop_emit = true;
}

bool ThrowItem::Update()
{
	Move();

	PosUpdateX();

	Grivity();
	PosUpdateY();


	throw_item_efc_root->is_stop_emit = false;
	throw_item_efc_root->root_pos = pos;
	throw_item_efc_root->is_flip_x = isFlipX;


	checkAreaX();
	checkAreaY();


	return 1;
}

void ThrowItem::Draw() const
{
		switch (weapon_id)
	{
	case ThrowItem::ARROW:
		DrawRectRotaGraphFast2(cam.xf_ext(
			pos.x), cam.yf_ext(pos.y),
			0, 6, 37, 7, 0, 0, 1.0f, 0.0f,
			pTexture->getGraphs(pTexture->ARROW), TRUE, isFlipX
			);
		break;
	case ThrowItem::TOMAHAWK:
		DrawRectRotaGraphFast2(cam.xf_ext(
			pos.x), cam.yf_ext(pos.y),
			0, 13, 26, 10, size.x*0.5f, size.y*0.5f, 1.0f, ToRadians(angle),
			pTexture->getGraphs(pTexture->ARROW), TRUE, isFlipX
			);
		break;
	default:
		break;
	}
}

void ThrowItem::ImGui()
{

}

void ThrowItem::Grivity()
{
	switch (weapon_id)
	{
	case ThrowItem::ARROW:
		break;
	case ThrowItem::TOMAHAWK:
		speed.y += 0.4f;
		//if (speed.y >= MAX_SPEED.y)
		//{
		//	speed.y = MAX_SPEED.y;
		//}
		break;
	default:
		break;
	}
}

void ThrowItem::Move()
{
	//if (isFlipX)
	//{
	//	if (getTerrainAttr(pos.x - size.x / 2, pos.y) == TR_ATTR::ALL_BLOCK)
	//	{
	//		isErase = true;
	//		return;
	//	}
	//}
	//else
	//{
	//	if (getTerrainAttr(pos.x + size.x / 2, pos.y) == TR_ATTR::ALL_BLOCK)
	//	{
	//		isErase = true;
	//		return;
	//	}
	//}

	switch (weapon_id)
	{
	case ThrowItem::ARROW:
		if (isFlipX)
		{

		}
		else
		{

		}
		break;
	case ThrowItem::TOMAHAWK:
		if (isFlipX)
		{
			angle+= speed.x*5;
		}
		else
		{
			angle+= speed.x*5;
		}
		break;
	default:
		break;
	}
}

void ThrowItem::checkAreaX()
{
	
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			isErase = true;
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(throw_item_efc_filenames[weapon_id + 2]), false, pos, isFlipX);
			return;
		}
	}

	
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			isErase = true;
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(throw_item_efc_filenames[weapon_id + 2]), false, pos, isFlipX);
			return;
		}
	}
}

void ThrowItem::checkAreaY()
{
	
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			isErase = true;
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(throw_item_efc_filenames[weapon_id + 2 + (weapon_id == FLY_WEAPON::TOMAHAWK ? 1 : 0)]), false, pos, isFlipX);
			return;
		}
	}
	

	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			isErase = true;
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset(throw_item_efc_filenames[weapon_id + 2 + (weapon_id == FLY_WEAPON::TOMAHAWK ? 1 : 0)]), false, pos, isFlipX);
			return;
		}
	}
}


void ThrowItem::DrawUpdate()
{

}