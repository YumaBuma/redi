#ifndef GAMELIB_H_
#define GAMELIB_H_

#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <Keyboard.h>
#include <xaudio2.h>
#include <mmsystem.h>
#include <ctime>
#include "util.h"
#include "misc.h"
#include "blend.h"
#include "font.h"
#include "primitive.h"
#include "high_resolution_timer.h"
#include "frame_rate.h"
#include "system.h"
#include "XinputPad.h"

namespace GameLib
{

	HWND  getHandle();
	ID3D11Device*	getDevice();
	ID3D11DeviceContext*  getContext();
	float getDeltaTime();

	//ライブラリ
	//初期化
	void init(LPCTSTR, HINSTANCE, int width = (640), int height = (480),
		int iconNum = -1, bool isFullscreen = (false), double frameRate = (0.0));
	//終了
	void uninit();

	//ゲームループ
	bool gameLoop(bool isShowFrameRate = (false));

	//バックバッファの内容を表示
	HRESULT present(UINT SyncInterval, UINT Flags);

	//画面を指定色にクリア
	void clear(const DirectX::XMFLOAT4&);

	namespace blend
	{
		void setBlendMode(const int _blendNum,const float alpha);

		enum blendNumber
		{
			NONE = 0,
			ALPHA,
			ADD,
			SUBTRACT,
			REPLACE,
			MULTIPLY,
			LIGHTEN,
			DARKEN,
			SCREEN,
			END
		};

	}

	//ウインドウの設定
	namespace window
	{
		//初期化
		HWND init(LPCTSTR, HINSTANCE, int, int, int);
		//解放
		void uninit();

	}

	//DirectX11関連
	namespace DirectX11
	{

		//初期化
		HRESULT init(HWND, int, int, bool);
		//解放
		void uninit();

	}


	DirectX::XMFLOAT2 getWindowSize();

	//-------------------------------------------------------------------//
	//
	//
	//							2D
	//
	//
	//-------------------------------------------------------------------//
	//テクスチャー
	namespace texture
	{
		void SpriteRender(Sprite*,
			float _dx, float _dy, float _dw, float _dh,
			float _sx, float _sy, float _sw, float _sh,
			float _angle, float _r, float _g, float _b,
			float _cx, float _cy, bool _flipX, bool _flipY
		);

		void SpriteRoad(Sprite*, wchar_t* texName);


		void SpriteBatchRoad(SpriteBatch* _batch,wchar_t*);

		void SpriteBatchRender(SpriteBatch*,
			float _dx, float _dy, float _dw, float _dh,
			float _sx, float _sy, float _sw, float _sh,
			float _angle, float _r, float _g, float _b,
			float _cx, float _cy, bool _flipX, bool _flipY
		);

		void SpriteBatchBegin(SpriteBatch* _batch);
		void SpriteBatchEnd(SpriteBatch* _batch);
	}

	//プリミティブ
	namespace primitive
	{
		//------------------------------------------------------
		//  矩形描画
		//------------------------------------------------------
		//  float x         描画位置x     float y        描画位置y
		//  float w         幅            float h        高さ
		//  float centerX   基準点x       float centerY  基準点y
		//  float angle     角度 (radian)
		//  float r         色 r (0.0f ~ 1.0f)
		//  float g         色 g (0.0f ~ 1.0f)
		//  float b         色 b (0.0f ~ 1.0f)
		//------------------------------------------------------
		void rect(float, float, float, float,
			float cx = (0), float cy = (0), float angle = (0),
			float r = (1), float g = (1), float b = (1));

		//------------------------------------------------------
		//  矩形描画
		//------------------------------------------------------
		//  const DirectX::XMFLOAT2& pos      描画位置 (x, y)
		//  const DirectX::XMFLOAT2& size     幅高さ   (w, h)
		//  const DirectX::XMFLOAT2& center   基準点   (x, y)
		//  float angle             角度     (radian)
		//  const DirectX::XMFLOAT4& color    色       (r, g, b, a) (0.0f ~ 1.0f)
		//------------------------------------------------------
		void rect(const DirectX::XMFLOAT2&, const DirectX::XMFLOAT2&,
			const DirectX::XMFLOAT2& center = (DirectX::XMFLOAT2(0, 0)), float angle = (0),
			const DirectX::XMFLOAT3& color = (DirectX::XMFLOAT3(1, 1, 1)));

		//------------------------------------------------------
		//  線描画
		//------------------------------------------------------
		//  float x1        始点 x    float y1    // 始点 y
		//  float x2        終点 x    float y2    // 終点 y
		//  float r         色 r (0.0f ~ 1.0f)
		//  float g         色 g (0.0f ~ 1.0f)
		//  float b         色 b (0.0f ~ 1.0f)
		//  float a         色 a (0.0f ~ 1.0f)
		//  float width     幅
		//------------------------------------------------------
		void line(float, float, float, float,
			float r = (1), float g = (1), float b = (1),
			float width = (1));

		//------------------------------------------------------
		//  線描画
		//------------------------------------------------------
		//  const DirectX::XMFLOAT2& from     始点 (x, y)
		//  const DirectX::XMFLOAT2& to       終点 (x, y)
		//  const DirectX::XMFLOAT4& color    色   (r, g, b, a)
		//  float width             幅
		//------------------------------------------------------
		void line(const DirectX::XMFLOAT2&, const DirectX::XMFLOAT2&,
			const DirectX::XMFLOAT3& color = (DirectX::XMFLOAT3(1, 1, 1)), float width = (1));

		//------------------------------------------------------
		//  円描画
		//------------------------------------------------------
		//  float x         中心位置 x    float y      // 中心位置 y
		//  float radius    半径
		//  float r         色 r (0.0f ~ 1.0f)
		//  float g         色 g (0.0f ~ 1.0f)
		//  float b         色 b (0.0f ~ 1.0f)
		//  float a         色 a (0.0f ~ 1.0f)
		//  int n           何角形か
		//------------------------------------------------------
		void circle(float, float, float,
			float r = (1), float g = (1), float b = (1),
			int n = (32));

		//------------------------------------------------------
		//  円描画
		//------------------------------------------------------
		//  const DirectX::XMFLOAT2& pos      中心位置 (x, y)
		//  float radius            半径
		//  const DirectX::XMFLOAT4& color    色   (r, g, b, a)
		//  int n                   何角形か
		//------------------------------------------------------
		void circle(const DirectX::XMFLOAT2&, float,
			const DirectX::XMFLOAT3& color = (DirectX::XMFLOAT3(1, 1, 1)), int n = (32));

		//------------------------------------------------------
		//  四角ポリゴン描画（頂点指定）
		//------------------------------------------------------
		//  const DirectX::XMFLOAT2(&v)[4]    頂点の配列（大きさ4）
		//  float r     色 r (0.0f ~ 1.0f)
		//  float g     色 g (0.0f ~ 1.0f)
		//  float b     色 b (0.0f ~ 1.0f)
		//  float a     色 a (0.0f ~ 1.0f)
		//------------------------------------------------------
		void quad(const DirectX::XMFLOAT2(&)[4],
			float r = (1), float g = (1), float b = (1));

		//------------------------------------------------------
		//  四角ポリゴン描画（頂点指定）
		//------------------------------------------------------
		//  const DirectX::XMFLOAT2(&v)[4]    頂点の配列（大きさ4）
		//  const DirectX::XMFLOAT4& color    色 (r, g, b, a)
		//------------------------------------------------------
		void quad(const DirectX::XMFLOAT2(&)[4],
			const DirectX::XMFLOAT3& color = (DirectX::XMFLOAT3(1, 1, 1)));
	}

	//--------------------------------------------------------------//
	//
	//
	//音楽類
	//
	//
	//--------------------------------------------------------------//
	//BGM
	namespace music
	{

	}
	//SE
	namespace sound
	{

	}

	//デバック
	namespace debug
	{
		//--------------------------------
		//  デバッグ文字列をセットする(画面に表示する)
		//--------------------------------
		void drawDebug(DirectX::XMINT2 _pos, std::string str);

	}

	//テキスト周り
	namespace font
	{
		float textOut(int, std::wstring,
			float, float, float scaleX = (1), float scaleY = (1),
			float r = (1), float g = (1), float b = (1), float a = (1));

		LPCSTR FormatString(LPCSTR format, ...);
	}

	//インプット周り
	namespace input
	{
		namespace xInput
		{
			int getState();

			bool pressedButtons(int _padNum, int _button);

			DirectX::XMINT2 getThumbL(int _padNum);
			DirectX::XMINT2 getThumbR(int _padNum);

		}
	}
}

#endif // !GAMELIB_H_


