#pragma once
#include "Object.h"
#include <memory>

class EnemyManager
{
private:
	bool selected[4];
	Vector2 praPos;
	static Vector2 pos[2];
	int layer;
	int mousePos[4];
	bool useGrid;
	static int gritSize[2];
	bool comparision;
	bool useMouse;
	int mouseButtons[2];
	int listbox_group_current[2];
	bool gripObj;
	Vector2 knockBackPower;
	std::vector<std::shared_ptr<Object>> CopyList;

	enum select
	{
		put = 0,
		copy,
		move,
		remove,
	};
	enum axis
	{
		x = 0,
		y,
		prevX,
		prevY
	};
	enum mouseState
	{
		previous = 0,
		now,
	};


	EnemyManager() = default;
	EnemyManager(EnemyManager& copy) {}
	~EnemyManager() = default;

public:
	static EnemyManager* getInstance()
	{
		static EnemyManager instance;
		return &instance;
	}

public:
	void setEnemy(int _id, float _posX, float _posY, bool _isflipX);
	void Debug();
	void init();
	void update();
	void draw();
	void loadFile();
	void saveFile();
	void imGui();

	void unInit()
	{
		//{ std::vector<std::shared_ptr<Object>>().swap(CopyList); }
	}

};

#define penmManager (EnemyManager::getInstance())
