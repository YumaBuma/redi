#include "Function.h"
#include <filesystem>
#include <iostream>
#include <sstream>

#pragma region cdefs
const float cdefs::PI_f				= static_cast<float>(3.14159265358979323846);
const float cdefs::TORADIAN			= PI_f / 180.f;	// PI_f  / 180.f
const float cdefs::TODEGREE			= 180.f / PI_f;	// 180.f / PI_f

const float cdefs::e				= 0.0009765625f;
const float cdefs::e2				= e * 2.0f;
const float cdefs::e2_reciprocal	= 1.f / e2;
#pragma endregion


PerlinNoise p_noise;

//----------------------------------------------------------------------
//	ファイル
//----------------------------------------------------------------------

std::vector<std::string> GetAllFileNamesInDir(const std::string& _dir_name, const std::string& _extension, bool _merge_dir_name)
{
	//https://qiita.com/tkymx/items/f9190c16be84d4a48f8a

	HANDLE hFind;
	WIN32_FIND_DATA win32fd;//defined at Windwos.h
	std::vector<std::string> file_names;

	//拡張子の設定
	std::string search_name = _dir_name + "\\*." + _extension;

	hFind = FindFirstFile(search_name.c_str(), &win32fd);

	if (hFind == INVALID_HANDLE_VALUE)
	{
#if 0
		MessageBox
		(
			NULL,
			(
				_dir_name										+
				"\n." + _extension	+ " file is not found"		+
				"\",\n File: "		+ __FILE__					+
				",\n Line: "		+ std::to_string(__LINE__)	+
				",\n In Function: " + __FUNCTION__
				).c_str(),
			"Matching files not found",
			MB_CANCELTRYCONTINUE | MB_ICONSTOP
		);
#endif
		return file_names;
	}

	do
	{
		if (win32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{}
		else
		{
			if (_merge_dir_name)
			{
				file_names.push_back(_dir_name + "\\" + win32fd.cFileName);
			}
			else
			{
				file_names.push_back(win32fd.cFileName);
			}
			//ConvertRelativePath(file_names.back());
		}
	} while (FindNextFile(hFind, &win32fd));

	FindClose(hFind);

	return file_names;
}

std::string GetOpenFileNameWithExplorer(bool _use_relative_path, const std::string& _default_extension)
{
	char full_path[MAX_PATH];
	char file_name[MAX_PATH];
	OPENFILENAME ofn;

	// 0で埋める
	memset(&ofn		, 0, sizeof(OPENFILENAME));
	memset(full_path, 0, sizeof(full_path));
	memset(file_name, 0, sizeof(file_name));

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner	= GetMainWindowHandle();

	// https://docs.microsoft.com/en-us/windows/desktop/api/commdlg/ns-commdlg-tagofna
	// 説明の末尾に \0 を記載して、その後ろに表示するファイルの指定、最後に \0\0 を記述
	ofn.lpstrFilter =
		"															\
			all file(*.*)\0*.*\0									\
			binary file(*.bin)\0*.bin\0								\
			config file(*.ini)\0*.ini\0								\
			image file(*.bmp;*.jpg;*.png)\0*.bmp;*.jpg;*.png\0		\
			json file(*.json)\0*.json\0								\
			text file(*.txt)\0*.txt\0								\
			sound file(*.wav;*.mp3;*.aac)\0*.wav;*.mp3;*.aac\0\0	\
		";

	// lpstrFile に指定する配列にファイルのフルパスが代入されます
	ofn.lpstrFile		= full_path;
	ofn.nMaxFile		= sizeof(full_path);

	// lpstrFileTitle に指定する配列にファイル名( フォルダパスが無い )が代入されます
	ofn.lpstrFileTitle	= file_name;
	ofn.nMaxFileTitle	= sizeof(file_name);

	// http://www-higashi.ist.osaka-u.ac.jp/~k-maeda/vcpp/com5-2ofn.html#flags
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR;

	// ダイアログを開いた際のデフォルトの拡張子を設定します
	ofn.lpstrDefExt	= _default_extension.c_str();

	// lpstrTitle に表示されるダイアログの説明を代入します
	ofn.lpstrTitle	= "GetOpenFileNameWithExplorer()";

	// lpstrInitialDir にカレントディレクトリを代入してエクスプローラーを開いた際の場所を指定します
	//	↑Vista以降動作しない
	// https://social.msdn.microsoft.com/Forums/windowsapps/en-US/7c6614ea-bef9-4421-9778-f8e42b95456a/openfilename-lpstrinitialdir-behavior-change-in-windows-7-how-to-avoid?forum=windowsuidevelopment
	// http://donadona.hatenablog.jp/entry/2017/01/09/005447
	//	IFileDialogという新しいダイアログの使用法がVista以降推奨されているがマルチバイト文字セットでは使えないらしいのでDxライブラリと非互換
	char   c_dir[MAX_PATH];
	memset(c_dir, 0, sizeof(c_dir));
	GetCurrentDirectory(MAX_PATH, c_dir);
	SetCurrentDirectory(c_dir);
	ofn.lpstrInitialDir = c_dir;

	// パスの指定が成功した場合は GetOpenFileName の戻り値は 0 以外になります
	BOOL result;
		 result	= GetOpenFileName(&ofn);
	if ( result != 0)
	{
		if (_use_relative_path)
		{
			std::string current_dir = GetCurrentDir();
			std::string full_path_tmp = full_path;

			//絶対パス部分を削除
			full_path_tmp.erase(0, current_dir.size());

			std::string relative_path("." + full_path_tmp);

			return relative_path;
		}
		else
		{
			return std::string(full_path);
		}
	}

	return std::string();
}

std::vector<std::string> GetOpenMultipleFileNameWithExplorer(bool _use_relative_path, const std::string& _default_extension)
{
	char full_path[MAX_PATH * 256];
	char file_name[MAX_PATH * 256];
	OPENFILENAME ofn;

	// 0で埋める
	memset(&ofn, 0, sizeof(OPENFILENAME));
	memset(full_path, 0, sizeof(full_path));
	memset(file_name, 0, sizeof(file_name));

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = GetMainWindowHandle();

	// https://docs.microsoft.com/en-us/windows/desktop/api/commdlg/ns-commdlg-tagofna
	// 説明の末尾に \0 を記載して、その後ろに表示するファイルの指定、最後に \0\0 を記述
	ofn.lpstrFilter =
		"															\
			all file(*.*)\0*.*\0									\
			binary file(*.bin)\0*.bin\0								\
			config file(*.ini)\0*.ini\0								\
			image file(*.bmp;*.jpg;*.png)\0*.bmp;*.jpg;*.png\0		\
			json file(*.json)\0*.json\0								\
			text file(*.txt)\0*.txt\0								\
			sound file(*.wav;*.mp3;*.aac)\0*.wav;*.mp3;*.aac\0\0	\
		";

	// lpstrFile に指定する配列にファイルのフルパスが代入されます
	ofn.lpstrFile = full_path;
	ofn.nMaxFile = sizeof(full_path);

	// lpstrFileTitle に指定する配列にファイル名( フォルダパスが無い )が代入されます
	ofn.lpstrFileTitle = file_name;
	ofn.nMaxFileTitle = sizeof(file_name);

	// http://www-higashi.ist.osaka-u.ac.jp/~k-maeda/vcpp/com5-2ofn.html#flags
	// OFN_EXPLORER : http://chokuto.ifdef.jp/urawaza/struct/OPENFILENAME.html
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_EXPLORER | OFN_HIDEREADONLY | OFN_NOCHANGEDIR;

	// ダイアログを開いた際のデフォルトの拡張子を設定します
	ofn.lpstrDefExt = _default_extension.c_str();

	// lpstrTitle に表示されるダイアログの説明を代入します
	ofn.lpstrTitle = "GetOpenMultipleFileNameWithExplorer()";

	// lpstrInitialDir にカレントディレクトリを代入してエクスプローラーを開いた際の場所を指定する←できない
	// (105行辺り参照)
	char   c_dir[MAX_PATH];
	memset(c_dir, 0, sizeof(c_dir));
	GetCurrentDirectory(MAX_PATH, c_dir);
	SetCurrentDirectory(c_dir);
	ofn.lpstrInitialDir = c_dir;

	std::string current_dir = GetCurrentDir();

	// パスの指定が成功した場合は GetOpenFileName の戻り値は 0 以外になります
	BOOL result = GetOpenFileName(&ofn);
	if (result != 0)
	{
		std::vector<std::string> paths;
		int counter_old = 0;
		int counter = 0;
		while (true)
		{
			if (full_path[counter] == '\0')
			{
				paths.emplace_back();
				while (counter_old <= counter)
				{
					paths.back().push_back(full_path[counter_old]);
					counter_old++;
				}
			}
			if (full_path[counter] == '\0' && full_path[counter + 1] == '\0')
			{
				break;
			}
			counter++;
		}

		std::vector<std::string> result;
		{
			if (paths.size() == 1)
			{
				if (_use_relative_path)
				{
					//絶対パス部分を削除
					paths.front().erase(0, current_dir.size());

					//相対パス
					std::string dir = "." + paths.front();

					result.push_back(dir);
					return result;
				}
				else
				{
					result.push_back(paths.front());
					return result;
				}
			}

			std::string dir;
			if (_use_relative_path)
			{
				//絶対パス部分を削除
				paths.front().erase(0, current_dir.size());

				//相対パス
				dir = std::string("." + std::string(paths.front().c_str()) + "\\");
			}
			else
			{
				//絶対パス
				dir = paths.front() + "\\";
			}

			for (auto& it_str : paths)
			{
				if (it_str == paths.front())continue;
				result.push_back(std::string(dir + it_str));
			}
		}

		return result;
	}

	return std::vector<std::string>();
}

std::string GetSaveFileNameWithExplorer(bool _use_relative_path, const std::string& _default_extension)
{
	char full_path[MAX_PATH];
	char file_name[MAX_PATH];
	OPENFILENAME ofn;

	// 0で埋める
	memset(&ofn, 0, sizeof(OPENFILENAME));
	memset(full_path, 0, sizeof(full_path));
	memset(file_name, 0, sizeof(file_name));

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = GetMainWindowHandle();

	// https://docs.microsoft.com/en-us/windows/desktop/api/commdlg/ns-commdlg-tagofna
	// 説明の末尾に \0 を記載して、その後ろに表示するファイルの指定、最後に \0\0 を記述
	ofn.lpstrFilter =
		"															\
			all file(*.*)\0*.*\0									\
			binary file(*.bin)\0*.bin\0								\
			config file(*.ini)\0*.ini\0								\
			image file(*.bmp;*.jpg;*.png)\0*.bmp;*.jpg;*.png\0		\
			json file(*.json)\0*.json\0								\
			text file(*.txt)\0*.txt\0								\
			sound file(*.wav;*.mp3;*.aac)\0*.wav;*.mp3;*.aac\0\0	\
		";

	// lpstrFile に指定する配列にファイルのフルパスが代入されます
	ofn.lpstrFile = full_path;
	ofn.nMaxFile = sizeof(full_path);

	// lpstrFileTitle に指定する配列にファイル名( フォルダパスが無い )が代入されます
	ofn.lpstrFileTitle = file_name;
	ofn.nMaxFileTitle = sizeof(file_name);

	// http://www-higashi.ist.osaka-u.ac.jp/~k-maeda/vcpp/com5-2ofn.html#flags
	//
	ofn.Flags = OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY | OFN_NOCHANGEDIR;

	// ダイアログを開いた際のデフォルトの拡張子を設定します
	ofn.lpstrDefExt = _default_extension.c_str();

	// lpstrTitle に表示されるダイアログの説明を代入します
	ofn.lpstrTitle = "GetSaveFileNameWithExplorer()";

	// lpstrInitialDir にカレントディレクトリを代入してエクスプローラーを開いた際の場所を指定する←できない
	// (105行辺り参照)
	char   c_dir[MAX_PATH];
	memset(c_dir, 0, sizeof(c_dir));
	GetCurrentDirectory(MAX_PATH, c_dir);
	SetCurrentDirectory(c_dir);
	ofn.lpstrInitialDir = c_dir;

	// パスの指定が成功した場合は GetOpenFileName の戻り値は 0 以外になります
	BOOL result = GetOpenFileName(&ofn);
	if ( result != 0)
	{
		if (_use_relative_path)
		{
			std::string current_dir = GetCurrentDir();
			std::string full_path_tmp = full_path;

			//絶対パス部分を削除
			full_path_tmp.erase(0, current_dir.size());

			std::string relative_path("." + full_path_tmp);

			return relative_path;
		}
		else
		{
			return std::string(full_path);
		}
	}

	return std::string();
}

std::string GetCurrentDir()
{
	char   c_dir[MAX_PATH];
	memset(c_dir, 0, sizeof(c_dir));

	GetCurrentDirectory(MAX_PATH, c_dir);
	return std::string(c_dir);
}

void ConvertRelativePath(std::string& _full_path)
{
	// カレントディレクトリを取得
	std::string current_dir = GetCurrentDir();

	// 絶対パス部分を削除
	_full_path.erase(0, current_dir.size());

	// 文字列先頭に.を挿入
	_full_path.insert(_full_path.begin(), '.');
}

std::string GetRelativePath(const std::string& _full_path)
{
	std::string relative_path = _full_path;

	// カレントディレクトリを取得
	std::string current_dir = GetCurrentDir();

	// 絶対パス部分を削除
	relative_path.erase(0, current_dir.size());

	// 文字列先頭に.を挿入
	relative_path.insert(_full_path.begin(), '.');

	return relative_path;
}
