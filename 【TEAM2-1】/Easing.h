/*イージング関数を使いたい@C++
//https://qiita.com/hart_edsf/items/962ac03281b871dcc0df
*/
#pragma once

#include <cmath>
#include <functional>
#include <array>

namespace Easing
{
	enum eEasingAlgorithms : int // IN: 入りが緩やか　OUT: 終わりが緩やか
	{
		QUADIN = 0, // イージング掛かりの強さ1
		QUADOUT,	// イージング掛かりの強さ1
		QUADINOUT,	// イージング掛かりの強さ1

		CUBICIN,	// イージング掛かりの強さ2
		CUBICOUT,	// イージング掛かりの強さ2
		CUBICINOUT,	// イージング掛かりの強さ2

		QUARTIN,	// イージング掛かりの強さ3
		QUARTOUT,	// イージング掛かりの強さ3
		QUARTINOUT,	// イージング掛かりの強さ3

		QUINTIN,	// イージング掛かりの強さ4
		QUINTOUT,	// イージング掛かりの強さ4
		QUINTINOUT,	// イージング掛かりの強さ4

		SINEIN,		// イージング掛かりの強さ5
		SINEOUT,	// イージング掛かりの強さ5
		SINEINOUT,	// イージング掛かりの強さ5

		EXPIN,		// イージング掛かりの強さ6
		EXPOUT,		// イージング掛かりの強さ6
		EXPINOUT,	// イージング掛かりの強さ6

		CIRCIN,		// イージング掛かりの強さ7
		CIRCOUT,	// イージング掛かりの強さ7
		CIRCINOUT,	// イージング掛かりの強さ7

		BOUNCEIN,	// ボールが跳ねるような挙動
		BOUNCEOUT,	// ボールが跳ねるような挙動
		BOUNCEINOUT,// ボールが跳ねるような挙動

		BACKIN,		// 開始地点で一度バックする
		BACKOUT,	// 目標地点を通り過ぎた後バックしてくる
		BACKINOUT,	// 2回バックする

		ELASTICIN,	// 振り子のような挙動
		ELASTICOUT,	// 振り子のような挙動
		ELASTICINOUT,// 振り子のような挙動

		LINEAR,		// 線形移動

		EASINGEND
	};
	typedef eEasingAlgorithms eAlgs;

	extern const std::array< std::string, EASINGEND>algo_names;
}


namespace Easing
{
	//extern float(*easing_algorithm[24])(float, float, float, float);
	//extern float(*easing_algorithm_Back[3])(float, float, float, float, float);
	extern const std::array<std::function<float(float, float, float, float)>, EASINGEND> easing_functions;
	//extern const std::array<std::function<float(float, float, float, float, float)>, EASINGBACKEND> easing_algorithm_Back;
}

namespace Easing
{
	float InQuad(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutQuad(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutQuad(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InCubic(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutCubic(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutCubic(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InQuart(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutQuart(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutQuart(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InQuint(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutQuint(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutQuint(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InSine(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutSine(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutSine(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InExp(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutExp(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutExp(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InCirc(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutCirc(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutCirc(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InBounce(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutBounce(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutBounce(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InBack(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutBack(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutBack(float t, float totaltime, float max = 1.f, float min = 0.f);

	float InElastic(float t, float totaltime, float max = 1.f, float min = 0.f);
	float OutElastic(float t, float totaltime, float max = 1.f, float min = 0.f);
	float InOutElastic(float t, float totaltime, float max = 1.f, float min = 0.f);

	float Linear(float t, float totaltime, float max = 1.f, float min = 0.f);
}
