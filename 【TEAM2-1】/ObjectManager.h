#pragma once
#include <vector>
#include <memory>
#include "Object.h"
#include "DrawableList.h"

class ObjectManager
{

private:
	ObjectManager() = default;
	ObjectManager(ObjectManager& copy) {}
	~ObjectManager() = default;

public:
	static ObjectManager* getInstance()
	{
		static ObjectManager instance;
		return &instance;
	}

public:
	std::vector<std::vector<std::shared_ptr<Object>>> ObjList;

	void Add(std::shared_ptr<Object> obj, int _group, int _layer, int _id, int _personalID)
	{
		ObjList[_group].emplace_back(obj);
		ObjList[_group].back()->id = { _id,_personalID };
		ObjList[_group].back()->layer = _layer;;
		ObjList[_group].back()->Init();
		pDrawList->add(ObjList[_group].back(), _layer);
	}

	void Update()
	{
		//オブジェクトのUpdateがfalseだったら消す
		for (int i = 0; i < Object::END; i++)
		{
			auto remove_iter = std::remove_if(ObjList[i].begin(), ObjList[i].end(),
				[](std::shared_ptr<Object>& obj)
			{
				return !obj->Update();
			});

			ObjList[i].erase(remove_iter, ObjList[i].end());

		}

		EraseObject();
	}


	void Draw()
	{
		//全オブジェクトを描画する
		pDrawList->DrawAll();

	}

	void DrawUpdate()
	{
		for (int i = 0; i < Object::END; i++)
		{
			for (auto &it : ObjList[i])
			{
				it->DrawUpdate();
			}

		}

	}

	void EraseObject()
	{
		for (int i = 0; i < Object::END; i++)
		{
			auto remove_iter = std::remove_if(ObjList[i].begin(), ObjList[i].end(),
				[](std::shared_ptr<Object>& obj)
			{
				return obj->isErase;
			});

			ObjList[i].erase(remove_iter, ObjList[i].end());

		}

		for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
		{
			if (p->second->isErase)
				p = pDrawList->drawables.erase(p);
			else
				p++;
		}
	}
	
	void ImGui()
	{
		for (int i = 0; i < Object::END; i++)
		{
			for (auto &it : ObjList[i])
			{
				it->ImGui();
			}

		}
	}

	void Uninit()
	{
		for (int i = 0; i < Object::END; i++)
		{
			if (i == Object::PLAYER)
				continue;
			if (ObjList[i].empty() == false)
			{
				for (auto& p = ObjList[i].begin(); p != ObjList[i].end(); p++)
				{
					p->get()->isErase = true;
				}
			}
		}

		EraseObject();


	}


	bool directToPlayer(const Object& _obj)
	{
		if (ObjList[Object::PLAYER].empty())return -1;

		if (&_obj == ObjList[Object::PLAYER].back().get())return -1;
		float playerPos = ObjList[Object::PLAYER].begin()->get()->pos.x;
		float objectPos = _obj.pos.x;
		float distAxis = objectPos - playerPos;
		if (distAxis < 0)return true;
		else return true;
	}

	float distanceToPlayer(const Object& _obj)
	{
		//エラー時(プレイヤーがいない)は-1を返す
		if (ObjList[Object::PLAYER].empty())return -1;

		if (&_obj == ObjList[Object::PLAYER].back().get())return -1;
		
		//直線距離算出
		Vector2 playerPos = ObjList[Object::PLAYER].begin()->get()->pos;
		Vector2 objectPos = _obj.pos;
		Vector2 distAxis = objectPos - playerPos;

		float distance = sqrtf(distAxis.x*distAxis.x + distAxis.y*distAxis.y);
		
		return distance;
	}

	//std::vector<std::shared_ptr<Object>>::iterator getData(int _CLASS, int _PERSONAL)
	//{
	//	return std::find_if(ObjList.begin(), ObjList.end(), [&](std::shared_ptr<Object> obj) {return (obj->id.CLASS == _CLASS) && (obj->id.PERSONAL == _PERSONAL); });
	//}

	//beginとendを定義しておくとこのクラスに対して
	//range based forが使える(今回は未使用)
	//std::vector< std::shared_ptr<Object> >::const_iterator begin()
	//{
	//	return ObjList.begin();
	//}
	//std::vector< std::shared_ptr<Object> >::const_iterator end()
	//{
	//	return ObjList.end();
	//}
};

#define pObjManager (ObjectManager::getInstance())
