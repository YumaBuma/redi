#include "Input.h"

int key[256]; // 0:入力されていない 1:入力された瞬間 2:入力されている
int keyall;
int input;//0:キーボード　1:ゲームパッド

XINPUT_STATE Xinput;
const XINPUT_STATE& GetXInput() { return Xinput; }

DINPUT_JOYSTATE Dinput;

void InputKey()
{
	//static int input = 1;//0:キーボード　1:ゲームパッド
	static char buf[256];

	switch (input)
	{
	case 0:
		GetHitKeyStateAll(buf);
		for (int i = 0; i < 256; i++)
		{
			if (buf[i])
			{
				if (key[i] == 0) key[i] = 1;
				else if (key[i] >= 1) key[i]++;
			}
			else key[i] = 0;
		}
		break;
	case 1:
		if (GetJoypadXInputState(DX_INPUT_PAD1, &Xinput) == -1)
		{
			if (GetJoypadDirectInputState(DX_INPUT_PAD1, &Dinput) == -1)
			{
				input = 0;
			}
		}


		if (Xinput.ThumbLY > 20000 || Xinput.Buttons[0] == 1 || Dinput.X > 500 || Dinput.POV[0] == 9000)
		{
			key[KEY_INPUT_UP]++;
		}
		else
		{
			key[KEY_INPUT_UP] = 0;
		}

		if (Xinput.ThumbLY < -20000 || Xinput.Buttons[1] == 1 || Dinput.X < -500 || Dinput.POV[0] == 27000)
		{
			if (Xinput.ThumbLX < 20000 && Xinput.ThumbLX >- 20000)
			{
				key[KEY_INPUT_DOWN]++;
			}
			else
			{
				key[KEY_INPUT_DOWN] = 0;
			}
		}
		else
		{
			key[KEY_INPUT_DOWN] = 0;
		}


		if (Xinput.ThumbLX > 25000 || Xinput.Buttons[3] == 1 || Dinput.X > 500 || Dinput.POV[0] == 9000)
		{
			key[KEY_INPUT_RIGHT]++;
		}
		else if (Xinput.ThumbLX > 15000)
		{
			key[KEY_INPUT_LSHIFT]++;
			key[KEY_INPUT_RIGHT]++;
		}
		else
		{
			key[KEY_INPUT_LSHIFT] = 0;
			key[KEY_INPUT_RIGHT] = 0;
		}

		if (Xinput.ThumbLX < -25000 || Xinput.Buttons[2] == 1 || Dinput.X < -500 || Dinput.POV[0] == 27000)
		{
			key[KEY_INPUT_LEFT]++;
		}
		else if (Xinput.ThumbLX < -15000)
		{
			key[KEY_INPUT_LSHIFT]++;
			key[KEY_INPUT_LEFT]++;
		}
		else
		{
			key[KEY_INPUT_LSHIFT] = 0;
			key[KEY_INPUT_LEFT] = 0;
		}

		//if (Xinput.Buttons[4] == 1)
		//{
		//	key[KEY_INPUT_ESCAPE]++;
		//}
		//else
		//{
		//	key[KEY_INPUT_ESCAPE] = 0;
		//}

		if (Xinput.Buttons[5] == 1)
		{
			key[KEY_INPUT_M]++;
		}
		else
		{
			key[KEY_INPUT_M] = 0;
		}

		if (Xinput.Buttons[8] == 1)
		{
			key[KEY_INPUT_Q]++;
		}
		else
		{
			key[KEY_INPUT_Q] = 0;
		}

		if (Xinput.Buttons[9] == 1)
		{
			key[KEY_INPUT_E]++;
		}
		else
		{
			key[KEY_INPUT_E] = 0;
		}


		if (Xinput.Buttons[15] == 1)
		{
			key[KEY_INPUT_S]++;
		}
		else
		{
			key[KEY_INPUT_S] = 0;
		}
		if (Xinput.Buttons[14] == 1 || Dinput.Buttons[0] == 128)
		{
			key[KEY_INPUT_A]++;
		}
		else
		{
			key[KEY_INPUT_A] = 0;
		}

		if (Xinput.Buttons[13] == 1 || Dinput.Buttons[2] == 128)
		{
			key[KEY_INPUT_D]++;
		}
		else
		{
			key[KEY_INPUT_D] = 0;
		}

		if (Xinput.Buttons[12] == 1 || Dinput.Buttons[1] == 128)
		{
			key[KEY_INPUT_SPACE]++;
		}
		else
		{
			key[KEY_INPUT_SPACE] = 0;
		}

		if (Xinput.Buttons[4] == 1 || Dinput.Buttons[9] == 128)
		{
			key[KEY_INPUT_TAB]++;
		}
		else
		{
			key[KEY_INPUT_TAB] = 0;
		}
		break;
	default:
		break;
	}

}

#include "src\\imgui.h"

void Mouse::UpdateButtonsPressingCount()
{
	if ((GetMouseInput() & MOUSE_INPUT_LEFT) != 0)
	{
		left.pressing_count += 1;
		left.is_release = false;
	}
	else
	{
		left.is_release = (left.pressing_count != 0);
		left.pressing_count = 0;
	}

	if ((GetMouseInput() & MOUSE_INPUT_RIGHT) != 0)
	{
		right.pressing_count += 1;
		right.is_release = false;
	}
	else
	{
		right.is_release = (right.pressing_count != 0);
		right.pressing_count = 0;
	}

	if ((GetMouseInput() & MOUSE_INPUT_MIDDLE) != 0)
	{
		middle.pressing_count += 1;
		middle.is_release = false;
	}
	else
	{
		middle.is_release = (middle.pressing_count != 0);
		middle.pressing_count = 0;
	}
}
void Mouse::UpdateClickedPosLog()
{
	if (left.pressing_count == 1)
	{
		left.clicked_pos_log_x = pos_x;
		left.clicked_pos_log_y = pos_y;
	}
	if (right.pressing_count == 1)
	{
		right.clicked_pos_log_x = pos_x;
		right.clicked_pos_log_y = pos_y;
	}
	if (middle.pressing_count == 1)
	{
		middle.clicked_pos_log_x = pos_x;
		middle.clicked_pos_log_y = pos_y;
	}
}

void Mouse::UpdatePosDelta()
{
	pos_x_delta = pos_x - pos_x_old;
	pos_y_delta = pos_y - pos_y_old;
	pos_x_old	= pos_x;
	pos_y_old	= pos_y;
}

void Mouse::Update()
{
	GetMousePoint(&pos_x, &pos_y);
	{
		UpdateButtonsPressingCount();
		UpdateClickedPosLog();
		UpdatePosDelta();
	}
	rot  = (int)-ImGui::GetIO().MouseWheel;
	rotf =      -ImGui::GetIO().MouseWheel;
}
