#pragma once
#include <map>
#include <memory>
#include <algorithm>
#include "Drawable.h"


class DrawableList
{
public:
	std::multimap<int, std::shared_ptr<Drawable>> drawables;

private:
	DrawableList() = default;
	DrawableList(DrawableList& copy){}
	~DrawableList() = default;

public:
	static DrawableList* getInstance()
	{
		static DrawableList instance;
		return &instance;
	}

public:
	void add(std::shared_ptr<Drawable> drawable, int layer)
	{
		drawables.insert(std::make_pair(layer, drawable));

	}

	void DrawAll() const
	{
		std::for_each(drawables.begin(), drawables.end(), [](auto p)
		{
			if (p.second->isVisible() && p.second->layer >= 0 && p.second->layer<20)
			{
				p.second->Draw();
			}
		});
	}

	void DrawBehindChip() const
	{
		std::for_each(drawables.begin(), drawables.end(), [](auto p)
		{
			if (p.second->isVisible() && p.second->layer < 0)
			{
				p.second->Draw();
			}
			else
				return;
		});
	}

	void DrawFront() const
	{
		std::for_each(drawables.begin(), drawables.end(), [](auto p)
		{
			if (p.second->isVisible() && p.second->layer > 20)
			{
				p.second->Draw();
			}
			else
				return;
		});
	}

};

#define pDrawList (DrawableList::getInstance())
