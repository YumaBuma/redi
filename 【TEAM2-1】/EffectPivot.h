#pragma once

#include <d3d11.h>
#include "SimpleMath.h"

class BasePivot
{
public:
	BasePivot()
		:pos(0, 0), pos_log(0, 0), pos_old(0, 0), handle_radius(3.f), axis_length(0, 0),
		is_moving(false), is_over_on_pivot_origin(false)
	{}
	virtual ~BasePivot() {}

	virtual void BeginMove() {}
	virtual void Update() = 0;
	virtual void Draw() = 0;

	void SetPos(const DirectX::SimpleMath::Vector2& _pos) { pos = _pos; }
	void SetHandleRadius(const float& _radius) { handle_radius = _radius; }

	// 前フレームの位置との差分を返します
	const DirectX::SimpleMath::Vector2 GetDelta()const
	{
		DirectX::SimpleMath::Vector2 result;
		result = pos - pos_old;
		return	result;
	}
	// 左ボタンを離すまでの移動量を返します
	const DirectX::SimpleMath::Vector2& GetTotalMoveed()const
	{
		DirectX::SimpleMath::Vector2 result = {};
		if (is_moving)
			return result;
		else
		{
			result = pos - pos_log;
			return	result;
		}
	}

	const DirectX::SimpleMath::Vector2& pos_ref()		const { return pos; }
	const DirectX::SimpleMath::Vector2& pos_old_ref()	const { return pos_old; }
	const bool&							is_moving_ref()	const { return is_moving; }

protected:
	float handle_radius;
	DirectX::SimpleMath::Vector2 axis_length; // ピボット原点からの距離
	DirectX::SimpleMath::Vector2 pos;
	DirectX::SimpleMath::Vector2 pos_log;
	DirectX::SimpleMath::Vector2 pos_old;
	bool is_moving;
	bool is_over_on_pivot_origin;
};

class PositionPivot : public BasePivot
{
public:
	PositionPivot() :BasePivot(), is_over_on_axis_x(false), is_over_on_axis_y(false)
	{
		axis_length = { 32, 32 };
	}
	~PositionPivot() {}

	virtual void BeginMove()override;
	virtual void Update()override;
	virtual void Draw()override;

	void SetAxisLengthXY(const float& _length_xy) { axis_length = { _length_xy }; }
	void SetAxisLengthX(const float& _length_x) { axis_length.x = _length_x; }
	void SetAxisLengthY(const float& _length_y) { axis_length.y = _length_y; }

private:
	bool is_over_on_axis_x;
	bool is_over_on_axis_y;
};

class VelocityPivot : public BasePivot
{
public:
	VelocityPivot() 
		:angle(0), angle_randomize(0), vel_handle_radius(5), is_over_on_vel_handle(false),
		axis_length_log(0, 0), axis_length_pos(0, 0), vel_randomize_coefficient(1)
	{}
	~VelocityPivot() {}

	virtual void BeginMove()override;
	virtual void Update()override;
	virtual void Draw()override;

	void SetAngleRandomize(const float& _random_plus_minus) { angle_randomize = _random_plus_minus; }
	void CalcAngle();

	const DirectX::SimpleMath::Vector2 GetVelocity(const bool& _is_angle_randomize = false, const bool& _is_vel_randomize = false);
	void GetVelocity(const DirectX::SimpleMath::Vector2& _pos, DirectX::SimpleMath::Vector2* _p_vel, const bool& _is_vel_randomize = false);

public:
	float vel_coefficient = 0.2f;

private:
	float vel_handle_radius;
	float angle;
	float angle_randomize;

	float vel_randomize_coefficient;

	bool is_over_on_vel_handle;
	DirectX::SimpleMath::Vector2 axis_length_pos; // ピボット原点からの距離
	DirectX::SimpleMath::Vector2 axis_length_log; // ピボット原点からの距離

};
