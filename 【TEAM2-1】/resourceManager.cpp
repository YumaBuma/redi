#include <fstream>
#include <d3d11.h>
#include <WICTextureLoader.h>
#include "ResourceManager.h"


ResourceManager::ResourceShaderResourceViews	ResourceManager::SRViews[RESOURCE_MAX];
ResourceManager::ResourceVertexShaders			ResourceManager::VertexShaders[RESOURCE_MAX];
ResourceManager::ResourcePixelShaders			ResourceManager::PixelShaders[RESOURCE_MAX];

bool ReadBinaryFile(const char *filename, char **blob, unsigned int &size)
{
	std::ifstream inputFile(filename, std::ifstream::binary);
	inputFile.seekg(0, std::ifstream::end);
	size = static_cast<int>(inputFile.tellg());
	inputFile.seekg(0, std::ifstream::beg);
	*blob = new char[size];
	inputFile.read(*blob, size);
	return true;
}

bool ResourceManager::LoadShaderResourceView(
	ID3D11Device *Device, const wchar_t *filename,
	ID3D11ShaderResourceView **SRView, D3D11_TEXTURE2D_DESC *TexDesc)
{
	int no = -1;
	ResourceShaderResourceViews *find = nullptr;
	ID3D11Resource *Resource = nullptr;

	//	データ探索
	for (int n = 0; n < RESOURCE_MAX; n++) {
		ResourceShaderResourceViews *p = &SRViews[n];//	エイリアス

													 //	データが無いなら無視
													 //	但し、最初に見つけた領域ならセット用に確保
		if (p->iRefNum == 0) {
			if (no == -1) no = n;
			continue;
		}

		//	ファイルパスが違うなら無視
		if (wcscmp(p->path, filename) != 0) continue;

		//	同名ファイルが存在した
		find = p;
		p->SRView->GetResource(&Resource);
		break;
	}

	//	データが見つからなかった→新規読み込み
	if (!find) {
		ResourceShaderResourceViews *p = &SRViews[no];
		if (FAILED(CreateWICTextureFromFileEx
		(Device, filename,0,
			D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, DirectX::WIC_LOADER_DEFAULT,
			&Resource, &(p->SRView))))
			return false;
		find = p;
		wcscpy_s(p->path, 256, filename);
	}

	//	最終処理
	ID3D11Texture2D *tex2D;
	Resource->QueryInterface(&tex2D);
	*SRView = find->SRView;
	tex2D->GetDesc(TexDesc);
	find->iRefNum++;
	tex2D->Release();
	Resource->Release();
	return true;
}

//--------------------------------
//	頂点シェーダのロード
//--------------------------------
bool ResourceManager::LoadVertexShader(
	ID3D11Device *Device, const char *csoFileName,
	D3D11_INPUT_ELEMENT_DESC *InputElementDesc, int numElements,
	ID3D11VertexShader **VertexShader, ID3D11InputLayout **InputLayout)
{
	*VertexShader = nullptr;
	*InputLayout = nullptr;

	ResourceVertexShaders *find = nullptr;
	int no = -1;

	//	const char * -> wchar_t *
	wchar_t	filename[256];
	size_t	stringSize;
	mbstowcs_s(&stringSize, filename, csoFileName, strlen(csoFileName));

	//	データ検索
	for (int n = 0; n < RESOURCE_MAX; n++) {
		ResourceVertexShaders *p = &VertexShaders[n];

		if (p->iRefNum == 0) {
			if (no == -1) no = n;
			continue;
		}

		if (wcscmp(p->path, filename) != 0) continue;

		//	同名ファイルが存在した
		find = p;
		break;
	}

	//	データが見つからなかった
	if (!find) {
		ResourceVertexShaders *p = &VertexShaders[no];
		char *blob;
		unsigned int size;

		//	コンパイル済みピクセルシェーダーオブジェクトの読み込み
		if (!ReadBinaryFile(csoFileName, &blob, size)) return false;

		//	頂点シェーダーオブジェクトの生成
		if (FAILED(Device->CreateVertexShader(blob, size, nullptr, &p->VShader)))
			return false;

		HRESULT hr = Device->CreateInputLayout(InputElementDesc, numElements, blob, size, &p->Layout);
		//	入力レイアウトの作成
		delete[] blob;
		if (FAILED(hr)) return false;

		//	新規データの保存
		find = p;
		wcscpy_s(find->path, 256, filename);
	}

	//	最終処理
	*VertexShader = find->VShader;
	*InputLayout = find->Layout;
	find->iRefNum++;

	return true;
}

//--------------------------------
//	ピクセルシェーダのロード
//--------------------------------
bool ResourceManager::LoadPixelShader(
	ID3D11Device *Device, const char *csoFileName,
	ID3D11PixelShader **PixelShader)
{
	*PixelShader = nullptr;
	ResourcePixelShaders *find = nullptr;
	int no = -1;

	//	const char * -> wchar_t *
	wchar_t	filename[256];
	size_t stringSize = 0;
	mbstowcs_s(&stringSize, filename, csoFileName, strlen(csoFileName));

	//	データ検索
	for (int n = 0; n < RESOURCE_MAX; n++) {
		ResourcePixelShaders *p = &PixelShaders[n];
		if (p->iRefNum == 0) {
			if (no == -1) no = n;
			continue;
		}
		if (wcscmp(p->path, filename) != 0) continue;

		//	同名ファイルが存在した
		find = p;
		break;
	}

	//	新規作成
	if (!find) {
		ResourcePixelShaders *p = &PixelShaders[no];
		char *blob;
		unsigned int size;

		//	コンパイル済みピクセルシェーダーオブジェクトの読み込み
		if (!ReadBinaryFile(csoFileName, &blob, size)) return false;

		HRESULT hr = Device->CreatePixelShader(blob, size, nullptr, &p->PShader);
		delete[] blob;
		if (FAILED(hr)) return false;

		find = p;
		wcscpy_s(find->path, 256, filename);
	}

	//	最終処理（参照渡しでデータを渡す）
	*PixelShader = find->PShader;
	find->iRefNum++;

	return true;
}

//--------------------------------
//	テクスチャ解放処理
//--------------------------------
void ResourceManager::ReleaseShaderResourceView(ID3D11ShaderResourceView *SRView)
{
	if (!SRView) return;
	for (int n = 0; n < RESOURCE_MAX; n++) {
		ResourceShaderResourceViews *p = &SRViews[n];

		//	データが無いなら無視
		if (p->iRefNum == 0) continue;

		//	データが違うなら無視
		if (SRView != p->SRView) continue;

		//	データが存在した
		p->Release();
		break;
	}
}

//--------------------------------
//	頂点シェーダ解放処理
//--------------------------------
void ResourceManager::ReleaseVertexShader(
	ID3D11VertexShader *VertexShader, ID3D11InputLayout *InputLayout)
{
	if (!VertexShader) return;
	if (!InputLayout) return;

	for (int n = 0; n < RESOURCE_MAX; n++) {
		ResourceVertexShaders *p = &VertexShaders[n];
		if (p->iRefNum == 0) continue;
		if (VertexShader != p->VShader) continue;
		if (InputLayout != p->Layout) continue;

		//	データが存在した
		p->Release();
		break;
	}
}

//--------------------------------
//	ピクセルシェーダ解放処理
//--------------------------------
void ResourceManager::ReleasePixelShader(ID3D11PixelShader *PShader)
{
	if (!PShader) return;
	for (int n = 0; n < RESOURCE_MAX; n++) {
		ResourcePixelShaders *p = &PixelShaders[n];

		if (p->iRefNum == 0) continue;

		if (PShader != p->PShader) continue;

		//	データが存在した
		p->Release();
		break;
	}
}

//--------------------------------
//	解放処理
//--------------------------------
void ResourceManager::Release()
{
	for (int i = 0; i<RESOURCE_MAX; i++) {
		SRViews[i].Release(true);
		VertexShaders[i].Release(true);
		PixelShaders[i].Release(true);
	}
}