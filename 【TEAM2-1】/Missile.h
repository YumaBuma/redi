#pragma once
#include "Object.h"
#include "Texture.h"
#include "Mo2_BG.h"
#include "Camera.h"

class EffectRoot;

class Missile : public Object
{
public:
	Missile(int _ATK);
	void Init();
	bool Update();
	void Draw() const;
	Vector2 getATK(int _val);
	~Missile();
private:
	int attackPoint;
	std::shared_ptr<EffectRoot> missile_efc_root;
};

class rocks : public Object
{
public:
	rocks(int _ATK,float _finPos,float _moveSpeed);
	void Init();
	bool Update();
	void Draw() const;
	Vector2 getATK(int _val);
	~rocks();
private:
	int state;
	int attackPoint;
	int alpha;
	float finPos;
	bool isHit;
	bool is_emitted;


};
