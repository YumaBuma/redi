#include "SceneTitle.h"
#include "common.h"
#include "Texture.h"
#include "Sound.h"
#include "Input.h"
#include "fade.h"
#include "Camera.h"
#include "GameMenu.h"
#include "UI.h"
#include ".\\Shader\\PostFx.h"

#include "src\imgui.h"

Title::Title()
{
	//SetDrawValidGraphCreateFlag(false);
	//SetDrawValidFloatTypeGraphCreateFlag(false);
	//SetDrawValidAlphaChannelGraphCreateFlag(true);
	temp_screen = MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);

	fade_alpha = 0;
	scene_change = false;
	next_scene = sceneManager::GAME;
	POSTFX->Init((int)sceneManager::TITLE);
	//POSTFX->option_params.luminance_key.cbuf_params.luminance_multipley = 18.f;
	//POSTFX->option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_intensity = 0.25f;
	fogTimer = 0;
	pFade->Init();
}

Title::~Title()
{
	DeleteGraph(temp_screen);
}

void Title::Init()
{
	//pSound->stopBGM();
	stageNumber = 0;
	pFade->In(60);
	Gmenu.Init();
	ui.Init();
	ui.tmp_MAX_MONEY = 0;
	ui.tmp_money = 0;

	POSTFX->Init(sceneManager::TITLE);
}

void Title::Update()
{
	if (key[KEY_INPUT_LCONTROL] && key[KEY_INPUT_0] == 1)
		show_debug = !show_debug;

	if (fogTimer == 7680)
		fogTimer = 0;
	else
	fogTimer++;
	timer++;
	pSound->fadeinBGM(Sound::BGM_TITLE);
	pFade->Update();

	if (timer >= 60 && key[KEY_INPUT_SPACE] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_GAMESTART,false);
		if(scene_change == false)
		{
			pFade->Out(60.f);
		}
		scene_change = true;
	}
	

	if (scene_change)
	{
		if (pFade->IsUpdateCompleted()/*&& fade_alpha == -1*/)
		{
			sceneManager::isSwitchScene = true;
			sceneManager::next_scene = next_scene;
		}
	}

	POSTFX->Update();
}

void Title::Draw()
{
	SetDrawScreen(temp_screen);
	ClearDrawScreen();
	SetDrawMode(DX_DRAWMODE_NEAREST);
	//SetDrawBlendMode(DX_BLENDMODE_ALPHA, bg_col[3]);
	//DrawFillBox(0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, GetColor(bg_col[0], bg_col[1], bg_col[2]));

	DrawGraph(0, 0, pTexture->getGraphs(pTexture->TITLE_1), TRUE);
	
	DrawGraph(-(fogTimer/4)%1920, 0, pTexture->getGraphs(pTexture->BG7), TRUE);
	DrawGraph(1920-(fogTimer/4)%1920, 0, pTexture->getGraphs(pTexture->BG7), TRUE);

	DrawGraph(0, 0, pTexture->getGraphs(pTexture->TITLE_2), TRUE);
	
	DrawGraph(-(fogTimer/2)%1920, 0, pTexture->getGraphs(pTexture->BG8), TRUE);
	DrawGraph(1920-(fogTimer/2)%1920, 0, pTexture->getGraphs(pTexture->BG8), TRUE);

	DrawGraph(0, 0, pTexture->getGraphs(pTexture->TITLE_3), TRUE);

	POSTFX->Render(&temp_screen);
	SetDrawScreen(DX_SCREEN_BACK);
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		DrawRectRotaGraph2
		(
			SCREEN_CX - cam.zxi(), SCREEN_CY - cam.zyi(),
			0, 0,
			TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
			TEMP_SCREEN_CX, TEMP_SCREEN_CY,
			(double)TEMP_SCREEN_EXT_RATE_MAX + cam.zoom(),
			GetRadian(cam.angle()),
			temp_screen, 1
		);
		POSTFX->Draw();
		if (timer / 40 % 2 == 0)
		{
			DrawRectRotaGraphFast2(0, 0, 0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 0, 0, TEMP_SCREEN_EXT_RATE_MAX, 0.f, pTexture->getGraphs(pTexture->TITLE_BUTTON), TRUE);
		}
		DrawRectRotaGraphFast2(0, 0, 0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 0, 0, TEMP_SCREEN_EXT_RATE_MAX, 0.f, pTexture->getGraphs(pTexture->TITLE_4), TRUE);

	}
	pFade->Draw();
}

void Title::ImGui()
{
	//int temp_scr_tmp = GetDrawScreen();
	//SetDrawScreen(DX_SCREEN_BACK);
	ImGui::Begin("sceneTitle", nullptr, ImGuiWindowFlags_MenuBar);

	ImGui::Text("changeScene");

	if (ImGui::Button("Game"))
	{
		//scene_change = true;
		//next_scene = sceneManager::GAME;
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::GAME;
	}
	if (ImGui::Button("Over"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::OVER;
	}
	if (ImGui::Button("Clear"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::CLEAR;
	}

	if (ImGui::Button("postfx"))Iwin_flg.postfx ^= 1;
	POSTFX->ImGui();

	ImGui::End();
	//SetDrawScreen(temp_scr_tmp);
}
