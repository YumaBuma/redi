#pragma once
#include "Object.h"
#include "common.h"
#include <string>
#include <memory>

class EffectRoot;

class ThrowItem : public Object
{
public:
	enum FLY_WEAPON
	{
		ARROW,
		TOMAHAWK,
	};
	float power;
	Vector2 atk;
	FLY_WEAPON weapon_id;
private:
	std::shared_ptr<EffectRoot> throw_item_efc_root;
	static std::string throw_item_efc_filenames[5];
public:

	ThrowItem();
	ThrowItem(int _POWER, Vector2 _atk, int _weapon);
	~ThrowItem();
	void Init();
	bool Update();
	void Draw() const;
	void ImGui();
	void DrawUpdate();

	Vector2 getATK(int _val)
	{
		switch (_val)
		{
		case 0:return atk;
		case 1:return Vector2(power, 0.0f);
		default:
			break;
		}
	}
	void Grivity();
	void Move();

	void checkAreaX();
	void checkAreaY();
};
