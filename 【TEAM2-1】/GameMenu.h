#pragma once

#include "common.h"
#include "Weapon.h"
#include "MiniMap.h"

class Gamemenu
{
public:
	MiniMap* pmini_map;
	bool isOpen;
	enum MENUBOX_ID
	{
		BACKGROUND = 0,
		BAG,
		MENUBAR,
		MENUBAR_CURSOR,
		END,
	};

	int gain_count;
	int tutorial_num;
	int pl_money;
	Vector2 pos[END];
	int Menubar_C;
	int cursor;
	int tmp_cursor = -1;
	std::vector<WEAPON> bag;
	WEAPON pick_weapon;
	WEAPON equipments[5];
	WEAPON equipXY[2];
	bool equipmode;
	bool setBagFlg;
	bool sendFlg;

public:
	Gamemenu()
	{
		isOpen = false;
		for (int i = 0; i < END; i++)
		{
			pos[i] = { 0,0 };
		}
	}
	~Gamemenu() {};
public:
	void Init();
	void Update();
	void Draw();

	//void setBagsize(std::vector<WEAPON> &_bag)
	//{
	//	tmp_bag.resize(_bag.size());
	//}
	void setBag(std::vector<WEAPON>& _bag)
	{
		bag = _bag;
	}

	//void weapon_Detail_Update();
	void weapon_Detail_Draw(int _cursor);
	void Equipment();
	WEAPON sendEquip(bool _isX);
	void EquipMenu_Update();
	void EquipMenu_Draw();
	void EndMenu_Update();
	void Endmenu_Draw();

	void sortXY();
};
extern Gamemenu Gmenu;
