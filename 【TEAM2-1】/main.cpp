#include <memory>
#include <thread>
#include "GameLibFunctions.h"
#include "common.h"
#include "scene.h"
#include "Input.h"
#include "Texture.h"
#include "Sound.h"
#include "EffectDataManager.h"


LRESULT CALLBACK WndProc(HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) 
{
	startUpLibrary("RedI", hInstance, { TEMP_SCREEN_WIDTH*TEMP_SCREEN_EXT_RATE_MAX,TEMP_SCREEN_HEIGHT*TEMP_SCREEN_EXT_RATE_MAX }, false, 123, 60.0f);
	//SetUseDirectDrawDeviceIndex(0);
	//SetUseDirect3D11(TRUE);
	//SetUseDirect3DVersion(DX_DIRECT3D_11);

	//SetOutApplicationLogValidFlag(FALSE);
	//// ウインドウモードに変更
	//ChangeWindowMode(TRUE);
	////ウインドウのサイズを自由に変更出来るようにする
	////SetWindowSizeChangeEnableFlag(TRUE);
	//// 描画可能サイズをディスプレイのサイズに
	////SetGraphMode(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), 32);    //メインウインドウのクライアント領域のサイズを設定する

	////SetDrawValidGraphCreateFlag(false);
	////SetDrawValidFloatTypeGraphCreateFlag(true);
	////SetDrawValidAlphaChannelGraphCreateFlag(true);
	//SetGraphMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32);
	//SetWindowSize(TEMP_SCREEN_WIDTH*TEMP_SCREEN_EXT_RATE_MAX,TEMP_SCREEN_HEIGHT*TEMP_SCREEN_EXT_RATE_MAX);
	//SetAlwaysRunFlag(TRUE);

	//SetWindowText("RedI");
	//SetMainWindowText("RedI");
	//SetWindowIconID(123);

	//SetWindowStyleMode(10);


//#ifdef _DEBUG
//	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
//
//#endif

#if defined(DEBUG) | defined(_DEBUG)

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//SRand((unsigned int)time(NULL));

	////SetDrawMode(DX_DRAWMODE_NEAREST);
	//if (DxLib_Init() == -1) return -1;    // ＤＸライブラリ初期化処理 エラーが起きたら終了

	//SetWaitVSyncFlag(FALSE);

	//SetDrawScreen(DX_SCREEN_BACK);

	//// メッセージをフックするウインドウプロージャを登録する
	////注意：DxLib_Initを呼んだ後でないと　登録されない。
	//SetHookWinProc(WndProc);
	//ImGui::CreateContext();
	////ＩＭＥを使用する
	//SetUseIMEFlag(TRUE);

	//// Setup ImGui binding
	//ImGui_ImplDxLib_Init();
	//SetOutApplicationLogValidFlag(FALSE);


	//日本語フォント設定
	//************assert **************
	//ImGuiIO& io = ImGui::GetIO();
	//io.Fonts->AddFontFromFileTTF("mplus-1p-regular.ttf", 20.0f, NULL,
	//	io.Fonts->GetGlyphRangesJapanese());

	// 最初に確認したリーク
	//pTexture->loadGrapahs();
	//pSound->loadSounds();
	//pTexture->DeleteAllGraphHandles();
	//pSound->DeleteAllSoundHandles();
	//ImGui_ImplDxLib_Shutdown();
	//DxLib_End();
	//ImGui::DestroyContext();
	//return 0;

	bool show_test_window = false;
	bool show_another_window = false;
	bool loop = true;
	input = 1;

	
	std::unique_ptr<sceneManager> scenemanager = std::make_unique<sceneManager>();

	pTexture->loadGrapahs();
	pSound->loadSounds();

	EFCDATA->Init();
	sceneManager::changeScene(sceneManager::TITLE);

	LPCSTR font_path = "./Data/Font/PixelMplus10-Regular.ttf"; // 読み込むフォントファイルのパス
	if (AddFontResourceEx(font_path, FR_PRIVATE, NULL) > 0) {
	}
	else {
		// フォント読込エラー処理
		MessageBox(NULL, "フォント読込失敗", "", MB_OK);
	}
	while (gameexit && gameLoop(true))
	{
		//SetMouseDispFlag(show_debug);
		clearWindow();// 裏画面のデータを全て削除
		
		InputKey();
		pMOUSE->Update();
		
		/*if (key[KEY_INPUT_ADD] == 1)
		{
			screen_ext_rate += 1.0f;
			screen_ext_rate = clamp(screen_ext_rate, 1.f, TEMP_SCREEN_EXT_RATE_MAX);
			SetWindowSize(TEMP_SCREEN_WIDTH*(int)screen_ext_rate, TEMP_SCREEN_HEIGHT*(int)screen_ext_rate);
		}
		if (key[KEY_INPUT_SUBTRACT] == 1)
		{
			screen_ext_rate -= 1.0f;
			screen_ext_rate = clamp(screen_ext_rate, 1.f, TEMP_SCREEN_EXT_RATE_MAX);
			SetWindowSize(TEMP_SCREEN_WIDTH*(int)screen_ext_rate, TEMP_SCREEN_HEIGHT*(int)screen_ext_rate);
		}*/

		scenemanager->execute();

		ScreenFlip();

		//if (ProcessMessage() == -1) break;                 //エラーが起きたら終了

		//if (CheckHitKey(KEY_INPUT_ESCAPE) == 1)
		//	break;
	}
	loop = false;
	scenemanager->unInit();
	
/*	ImGui_ImplDxLib_Shutdown();

	InitGraph();
	InitSoundMem();

	DxLib_End();*/                                            // ＤＸライブラリ使用の終了処理

	shutDownLibrary();

	ImGui::DestroyContext();

	return 0;                                               // ソフトの終了
}


LRESULT CALLBACK WndProc(HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui_ImplDxLib_WndProcHandler(wnd, msg, wParam, lParam)) {
		SetUseHookWinProcReturnValue(TRUE);
		return 1;
	}

	//IME関連はOSに任せる
	switch (msg)
	{
		//WM_IME_SETCONTEXTを受け取ったときに
		//lParam = 0;とするとIME関連の表示が行われなくなります。
	case WM_IME_SETCONTEXT:
	case WM_IME_STARTCOMPOSITION:
	case WM_IME_ENDCOMPOSITION:
	case WM_IME_COMPOSITION:
	case WM_IME_NOTIFY:
	case WM_IME_REQUEST:
		SetUseHookWinProcReturnValue(TRUE);
		return DefWindowProc(wnd, msg, wParam, lParam);

	case WM_SYSCOMMAND:
		if ((wParam & 0xfff0) == SC_KEYMENU) { // Disable ALT application menu
			SetUseHookWinProcReturnValue(TRUE);
			return 0;
		}
		break;
	}

	return 0;
}


