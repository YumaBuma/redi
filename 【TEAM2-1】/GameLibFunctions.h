#ifndef GAMELIBFUNCTIONS_H_
#define GAMELIBFUNCTIONS_H_

#include "gameLib.h"

//library//
void startUpLibrary(LPCTSTR caption, HINSTANCE instance, int width = 640, int height = 480, bool isFullscreen = false, int iconNum = -1, double frameRate = 0.0);
void startUpLibrary(LPCTSTR caption, HINSTANCE instance, DirectX::XMINT2 screenSize = { 640,480 }, bool isFullscreen = false, int iconNum = -1, double frameRate = 0.0);

void shutDownLibrary();

bool gameLoop(bool isShowFrameRate);

DirectX::XMFLOAT2 getWindowSize();

//ClearWindow//
void clearWindow(const float r = .0f, const float g = .0f, const float b = .0f);


//BLENDMODE//

void setBlendMode_NONE(const float alpha = 1.0f);

void setBlendMode_ALPHA(const float alpha = 1.0f);

void setBlendMode_ADD(const float alpha = 1.0f);

void setBlendMode_SUBTRACT(const float alpha = 1.0f);

void setBlendMode_REPLACE(const float alpha = 1.0f);

void setBlendMode_MULTIPLY(const float alpha = 1.0f);

void setBlendMode_LIGHTEN(const float alpha = 1.0f);

void setBlendMode_DARKEN(const float alpha = 1.0f);

void setBlendMode_SCREEN(const float alpha = 1.0f);


//DEBUG//
template <typename ... Args>
void setString(DirectX::XMINT2 _pos, const std::wstring& fmt, Args ... args)
{
	size_t len = swprintf(nullptr, 0, fmt.c_str(), args ...);
	std::vector<wchar_t> buf(len + 1);
	swprintf(&buf[0], len + 1, fmt.c_str(), args ...);
	std::wstring str(&buf[0], &buf[0] + len);
	GameLib::debug::drawDebug(_pos, str);
}




//PRIMITIVE//
void drawRect(const float x, const float y, const float w, const float h, const float cx = .0f, const float cy = .0f, const float angle = .0f, const float r = 1.0f, const float g = 1.0f, const float b = 1.0f, const float a = 1.0f);
void drawRect(const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& size, const DirectX::XMFLOAT2& center = { .0f,.0f }, const float angle = .0f, const DirectX::XMFLOAT3& color = { 1.0f,1.0f,1.0f});

void drawLine(const float x1, const float y1, const float x2, const float y2, const float width = 1.0f, const float r = .0f, const float g = .0f, const float b = .0f);
void drawLine(const DirectX::XMFLOAT2& begin, const DirectX::XMFLOAT2& end, const float width = 1.0f, const DirectX::XMFLOAT3& color = { 1.0f,1.0f,1.0f});

void drawCircle(const float x, const float y, const float radius = 1.0f, const float r = 1.0f, const float g = 1.0f, const float b = 1.0f, int n = 32);
void drawCircle(const DirectX::XMFLOAT2& pos, const float radius = 1.0f, const DirectX::XMFLOAT3& color = { 1.0f,1.0f,1.0f}, const int n = 32);

void drawQuad(const DirectX::XMFLOAT2(&v)[4], const float r = 1.0f, const float g = 1.0f, const float b = 1.0f);
//void drawQuad(const DirectX::XMFLOAT2(&v)[4], const DirectX::XMFLOAT4& color = { 1.0f,1.0f,1.0f,1.0f });



//SPRITE//

void spriteLoad(Sprite* _sprite, wchar_t* _spriteName);

void spriterender(Sprite* _sprite, const float pos_x, const float pos_y, bool _tempX = false, bool _tempY = false);
void spriteRender(Sprite* _sprite, const DirectX::XMFLOAT2& pos, bool _tempX = false, bool _tempY = false);

void spriteRenderRect(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, bool _tempX = false, bool _tempY = false);
void spriteRenderRect(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, bool _tempX = false, bool _tempY = false);

void spriteRenderExtend(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification = 1.0f, bool _tempX = false, bool _tempY = false);
void spriteRenderExtend(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const float Magnification = 1.0f, bool _tempX = false, bool _tempY = false);

void spriteRenderExtend2(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, bool _tempX = false, bool _tempY = false);
void spriteRenderExtend2(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& Magnification = { 1.0f,1.0f }, bool _tempX = false, bool _tempY = false);

void spriteRenderExtend3(Sprite* _sprite, const float pos_x, const float pos_y, const float centerPos_x, const float centerPos_y, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, bool _tempX = false, bool _tempY = false);
void spriteRenderExtend3(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&centerPos, const DirectX::XMFLOAT2& Magnification = { 1.0f,1.0f }, bool _tempX = false, bool _tempY = false);

void spriteRenderRectExtend(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float Magnification = 1.0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRectExtend(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&texPos, const DirectX::XMFLOAT2&texSize, const float Magnification = 1.0f, bool _tempX = false, bool _tempY = false);

void spriteRenderRectExtend2(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRectExtend2(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&texPos, const DirectX::XMFLOAT2&texSize, const DirectX::XMFLOAT2& Magnification = { 1.0f,1.0f }, bool _tempX = false, bool _tempY = false);

void spriteRenderRectExtend3(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float centerPos_x, const float centerPos_y, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRectExtend3(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2&texPos, const DirectX::XMFLOAT2&texSize, const DirectX::XMFLOAT2& centerPos, const DirectX::XMFLOAT2& Magnification = { 1.0f,1.0f }, bool _tempX = false, bool _tempY = false);

void spriteRenderRota(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification = 1.0f, float angle = .0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRota(Sprite* _sprite, const DirectX::XMFLOAT2 pos, const float Magnification = 1.0f, float angle = .0f, bool _tempX = false, bool _tempY = false);

void spriteRenderRota2(Sprite* _sprite, const float pos_x, const float pos_y, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, float angle = .0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRota2(Sprite* _sprite, const DirectX::XMFLOAT2 pos, const DirectX::XMFLOAT2 Magnification = { 1.0f,1.0f }, float angle = .0f, bool _tempX = false, bool _tempY = false);

void spriteRenderRota3(Sprite* _sprite, const float pos_x, const float pos_y, const float centerPos_x = .0f, const float centerPos_y = .0f, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, float angle = .0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRota3(Sprite* _sprite, const DirectX::XMFLOAT2 pos, const DirectX::XMFLOAT2& centerPos = { .0f,.0f }, const DirectX::XMFLOAT2 Magnification = { 1.0f,1.0f }, float angle = .0f, bool _tempX = false, bool _tempY = false);

void spriteRenderRectRota(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float angle = 0, bool _tempX = false, bool _tempY = false);
void spriteRenderRectRota(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, const float angle = 0, bool _tempX = false, bool _tempY = false);

void spriteRenderRectRota2(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, const float angle = .0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRectRota2(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, const DirectX::XMFLOAT2& Magnification = { 1.0f,1.0f }, const float angle = .0f, bool _tempX = false, bool _tempY = false);

void spriteRenderRectRota3(Sprite* _sprite, const float pos_x, const float pos_y, const float texPos_x, const float texPos_y, const float texSize_x, const float texSize_y, const float centerPos_x = .0f, const float centerPos_y = .0f, const float Magnification_x = 1.0f, const float Magnification_y = 1.0f, const float angle = .0f, bool _tempX = false, bool _tempY = false);
void spriteRenderRectRota3(Sprite* _sprite, const DirectX::XMFLOAT2& pos, const DirectX::XMFLOAT2& texPos, const DirectX::XMFLOAT2& texSize, const DirectX::XMFLOAT2& centerPos = { .0f,.0f }, const DirectX::XMFLOAT2& Magnification = { 1.0f,1.0f }, const float angle = .0f, bool _tempX = false, bool _tempY = false);


//Xinput_Pad
int getState();

bool pressedButtons(int _padNum, int _button);

DirectX::XMINT2 getThumbL(int _padNum);
DirectX::XMINT2 getThumbR(int _padNum);


#endif // !GAMELIBFUNCTIONS_H_
