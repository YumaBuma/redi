#include "Easing.h"

static const float PI		= static_cast<float>(3.14159265358979323846);
static const float PI_mul2 = static_cast<float>(PI * 2.f);
static const float PI_div2	= static_cast<float>(1.57079632679489661923);

static const float S = 1.70158f;	//In,OutBack用
static const float S2 = S * 1.525f; //InOutBack用

//イージング関数配列
namespace Easing
{
	//float(*easing_algorithm[24])(float, float, float, float) =
	const std::array<std::function<float(float, float, float, float)>, EASINGEND>
		easing_functions =
	{
		InQuad,
		OutQuad,
		InOutQuad,

		InCubic,
		OutCubic,
		InOutCubic,

		InQuart,
		OutQuart,
		InOutQuart,

		InQuint,
		OutQuint,
		InOutQuint,

		InSine,
		OutSine,
		InOutSine,

		InExp,
		OutExp,
		InOutExp,

		InCirc,
		OutCirc,
		InOutCirc,

		InBounce,
		OutBounce,
		InOutBounce,

		InBack,
		OutBack,
		InOutBack,

		InElastic,
		OutElastic,
		InOutElastic,

		Linear,
	};

	//float(*easing_algorithm_Back[3])(float, float, float, float, float) =
	//書式すらわからず、そもそもスマポを使う必要があったのか?
	/*std::shared_ptr<std::function<float(float, float, float, float, float)>[EasingBackEnd]>
	easing_algorithm_Back =
	std::make_shared<std::function<float(float, float, float, float, float)>[EasingBackEnd]>
	(std::initializer_list<std::function<float(float, float, float, float, float)>>*/

	const std::array< std::string, EASINGEND > algo_names =
	{
		"Easing::QUADIN",
		"Easing::QUADOUT",
		"Easing::QUADINOUT",

		"Easing::CUBICIN",
		"Easing::CUBICOUT",
		"Easing::CUBICINOUT",

		"Easing::QUARTIN",
		"Easing::QUARTOUT",
		"Easing::QUARTINOUT",

		"Easing::QUINTIN",
		"Easing::QUINTOUT",
		"Easing::QUINTINOUT",

		"Easing::SINEIN",
		"Easing::SINEOUT",
		"Easing::SINEINOUT",

		"Easing::EXPIN",
		"Easing::EXPOUT",
		"Easing::EXPINOUT",

		"Easing::CIRCIN",
		"Easing::CIRCOUT",
		"Easing::CIRCINOUT",

		"Easing::BOUNCEIN",
		"Easing::BOUNCEOUT",
		"Easing::BOUNCEINOUT",

		"Easing::BACKIN",
		"Easing::BACKOUT",
		"Easing::BACKINOUT",

		"Easing::ELASTICIN",
		"Easing::ELASTICOUT",
		"Easing::ELASTICINOUT",

		"Easing::LINEAR"
	};
}

//イージング関数
namespace Easing
{
	float InQuad(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return max*t*t + min;
	}
	float OutQuad(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return -max*t*(t - 2.f) + min;
	}
	float InOutQuad(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= (totaltime / 2.f);
		if (t < 1.f)//sourceではif文条件式かっこ内で/2.fの除算を行っていたため結果がおかしかったがなぜそうなったのか分からない
		{
			return max / 2.f * t * t + min;
		}
		--t;
		return -max / 2.f * (t * (t - 2.f) - 1.f) + min;
	}

	float InCubic(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return max * t*t*t + min;
	}
	float OutCubic(float t, float totaltime, float max, float min)
	{
		max -= min;
		t = t / totaltime - 1.f;
		return max * (t*t*t + 1.f) + min;
	}
	float InOutCubic(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime / 2.f;
		if (t < 1.f)//sourceではif文条件式かっこ内で/2.fの除算を行っていたため結果がおかしかったがなぜそうなったのか分からない<-多分jQueryの言語仕様
			return max / 2.f * t*t*t + min;
		t -= 2.f;
		return max / 2.f * (t*t*t + 2.f) + min;
	}

	float InQuart(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return max * t*t*t*t + min;
	}
	float OutQuart(float t, float totaltime, float max, float min)
	{
		max -= min;
		t = t / totaltime - 1.f;
		return -max*(t*t*t*t - 1.f) + min;
	}
	float InOutQuart(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime / 2.f;
		if (t < 1.f)//sourceではif文条件式かっこ内で/2.fの除算を行っていたため結果がおかしかったがなぜそうなったのか分からない
		{
			return max / 2.f * t*t*t*t + min;
		}
		t -= 2.f;
		return -max / 2.f * (t*t*t*t - 2.f) + min;
	}

	float InQuint(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return max*t*t*t*t*t + min;
	}
	float OutQuint(float t, float totaltime, float max, float min)
	{
		max -= min;
		t = t / totaltime - 1.f;
		return max*(t*t*t*t*t + 1.f) + min;
	}
	float InOutQuint(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime / 2.f;
		if (t < 1.f)//sourceではif文条件式かっこ内で/2.fの除算を行っていたため結果がおかしかったがなぜそうなったのか分からない
			return max / 2.f * t*t*t*t*t + min;
		t -= 2.f;
		return max / 2.f * (t*t*t*t*t + 2.f) + min;
	}

	float InSine(float t, float totaltime, float max, float min)
	{
		max -= min;
		return -max*cosf((t / totaltime) * PI_div2) + max + min;
	}
	float OutSine(float t, float totaltime, float max, float min)
	{
		max -= min;
		return max * sin((t / totaltime) * PI_div2) + min;
	}
	float InOutSine(float t, float totaltime, float max, float min)
	{
		max -= min;
		return -max / 2.f * (cosf(PI*(t / totaltime)) - 1.f) + min;
	}

	float InExp(float t, float totaltime, float max, float min)
	{
		max -= min;
		return t == 0.0f ? min : max*powf(2.f, 10.f * (t / totaltime - 1.f)) + min;
	}
	float OutExp(float t, float totaltime, float max, float min)
	{
		max -= min;
		return t == totaltime ? max + min : max*(-powf(2.f, -10.f * t / totaltime) + 1.f) + min;
	}
	float InOutExp(float t, float totaltime, float max, float min)
	{
		if (t == 0.0f)
			return min;
		if (t == totaltime)
			return max;
		max -= min;
		t /= (totaltime / 2.f);

		if (t < 1.f)
			return max / 2.f * powf(2.f, 10.f * (t - 1.f)) + min;
		--t;
		return max / 2.f * (-powf(2.f, -10.f * t) + 2.f) + min;

	}

	float InCirc(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return -max*(sqrtf(1.f - t*t) - 1.f) + min;
	}
	float OutCirc(float t, float totaltime, float max, float min)
	{
		max -= min;
		t = t / totaltime - 1.f;
		return max*sqrtf(1.f - t*t) + min;
	}
	float InOutCirc(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime / 2.f;
		if (t < 1.f)//sourceではif文条件式かっこ内で/2.fの除算を行っていたため結果がおかしかったがなぜそうなったのか分からない
			return -(max / 2.f) * (sqrtf(1.f - t*t) - 1.f) + min;
		t -= 2.f;
		return (max / 2.f) * (sqrtf(1.f - t*t) + 1.f) + min;
	}

	float OutBounce(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;

		float t_ifexpression_tmp;
		t_ifexpression_tmp = 1.f / 2.75f;
		if (t < t_ifexpression_tmp)
		{
			return max*(7.5625f*t*t) + min;
		}
		else
		{
			t_ifexpression_tmp = 2.f / 2.75f;
			if (t < t_ifexpression_tmp)
			{
				t -= 1.5f / 2.75f;
				return max*(7.5625f*t*t + 0.75f) + min;
			}
			else
			{
				t_ifexpression_tmp = 2.5f / 2.75f;
				if (t < t_ifexpression_tmp)
				{
					t -= 2.25f / 2.75f;
					return max*(7.5625f*t*t + 0.9375f) + min;
				}
				else
				{
					t -= 2.625f / 2.75f;
					return max*(7.5625f*t*t + 0.984375f) + min;
				}
			}
		}
	}
	float InBounce(float t, float totaltime, float max, float min)
	{
		float result = max - OutBounce(totaltime - t, totaltime, max, min) + min;
		return result;
	}
	float InOutBounce(float t, float totaltime, float max, float min)
	{
		float result;
		float totaltime_div2 = totaltime / 2.f;
		if (t < totaltime_div2)
		{
			result = InBounce(t * 2.f, totaltime, max - min, 0.f)*0.5f + min;
			return result;
		}
		else
		{
			result = OutBounce(t * 2.f - totaltime, totaltime, max - min, 0.f)*0.5f + min + (max - min)*0.5f;
			return result;
		}
	}

	float InBack(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime;
		return max*t*t*((S + 1.f)*t - S) + min;
	}
	float OutBack(float t, float totaltime, float max, float min)
	{
		max -= min;
		t = t / totaltime;
		t--;
		return max*(t*t*((S + 1.f)*t + S) + 1.f) + min;
	}
	float InOutBack(float t, float totaltime, float max, float min)
	{
		max -= min;
		t /= totaltime / 2.f;
		if (t < 1.f)
		{
			return (max / 2.f) * (t*t*((S2 + 1.f)*t - S2)) + min;
		}
		t -= 2.f;
		return (max / 2.f) * (t*t*((S2 + 1.f)*t + S2) + 2.f) + min;
	}

	// https://tr.you84815.space/animejs/easing.html
	// https://github.com/jesusgollonet/ofpennereasing/blob/master/PennerEasing/Elastic.cpp
	// t: current time, b: begInnIng value初期値, c: change In value目標値, d: duration

	float InElastic(float t, float totaltime, float max, float min)
	{
		if (fabsf(max - min) <= FLT_EPSILON)return max;
		if (t == 0.f)return min;

		float s = S;
		float period = 0.5f;//Period(往復数)
		float amplitude = 1.f;//Amplitude(振幅)

		max -= min;
		t   /= totaltime;
		if (t == 1.f)return min + max;

		if (period == 0.f) period = totaltime * 0.3f;

		if (amplitude < fabsf(max)) 
		{
			amplitude = max;
			s = period / 4.f;
		}
		else
		{
			s = period / PI_mul2 * asinf(max / amplitude);
		}

		t -= 1.f;
		return -(amplitude * powf(2.f, 10.f * t) * sinf((t * totaltime - s) * PI_mul2 / period)) + min;
	}
	float OutElastic(float t, float totaltime, float max, float min)
	{
		if (fabsf(max - min) <= FLT_EPSILON)return max;
		if (t == 0.f) return min;

		float s = S;
		float period	= 0.5f;//Period(往復数)
		float amplitude = 1.f;//Amplitude(振幅)

		max -= min;
		t /= totaltime;
		if (t == 1.f) return min + max;
			
		if (period == 0.f) period = totaltime * 0.3f;

		if (amplitude < fabsf(max))
		{
			amplitude = max;
			s = period / 4.f;
		}
		else
		{
			s = period / PI_mul2 * asinf(max / amplitude);
		}

		return amplitude * powf(2.f, -10.f * t) * sinf((t * totaltime - s) * PI_mul2 / period) + max + min;
	}
	float InOutElastic(float t, float totaltime, float max, float min)
	{
		if (fabsf(max - min) <= FLT_EPSILON)return max;
		if (t == 0.f)	return min;

		float s = S;
		float period = 0.5f;//Period(往復数)
		float amplitude = 1.f;//Amplitude(振幅)

		max -= min;
		t /= (totaltime / 2.f);
		if (t == 2.f) return min + max;

		if (period == 0.f) period = totaltime * (0.3f * 1.5f);

		if (amplitude < fabsf(max))
		{
			amplitude = max;
			s = period / 4.f;
		}
		else
		{
			s = period / PI_mul2 * asinf(max / amplitude);
		}

		if (t < 1.f)
		{
			t -= 1.f;
			return -0.5f * (amplitude * powf(2.f, 10.f * t) * sinf((t * totaltime - s)* PI_mul2 / period)) + min;
		}

		t -= 1.f;
		return amplitude*powf(2.f, -10.f * t) * sinf((t * totaltime - s) * PI_mul2 / period)*0.5f + max + min;
	}

	float Linear(float t, float totaltime, float max, float min)
	{
		return (max - min)*t / totaltime + min;
	}
}
