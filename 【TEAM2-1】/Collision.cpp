#include "Collision.h"
#include "Camera.h"

bool Rect_Rect(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2);
bool Rect_RotaRect(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2);
bool Circle_Rect(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2);
bool Circle_Circle(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2);

bool(*Collision[])(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2) =
{
	Rect_Rect/*, Circle_Rect, Circle_Circle*/,
	Rect_RotaRect
};

bool Rect_Rect(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2)
{
	Rect r1 = { obj1->pos.x - size1.x*0.5f + margin1.x, obj1->pos.y - size1.y - margin1.y, obj1->pos.x + size1.x*0.5f + margin1.x, obj1->pos.y - margin1.y };
	Rect r2 = { obj2->pos.x - size2.x*0.5f + margin2.x, obj2->pos.y - size2.y - margin2.y, obj2->pos.x + size2.x*0.5f + margin2.x, obj2->pos.y - margin2.y };

	if (r1.l < r2.r && r2.l < r1.r && r1.t < r2.b && r2.t < r1.b)
	{
		return true;
	}
	return false;
}

bool Rect_RotaRect(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2)
{
	Rect r1 = { obj1->pos.x - size1.x*0.5f + margin1.x, obj1->pos.y - size1.y - margin1.y, obj1->pos.x + size1.x*0.5f + margin1.x, obj1->pos.y - margin1.y };
	Rect r2 = {
		obj2->pos.x - size2.x*0.5f + margin2.x,
		obj2->pos.y - size2.y *0.5f - margin2.y,
		obj2->pos.x + size2.x*0.5f + margin2.x,
		obj2->pos.y + size2.y *0.5f - margin2.y
	};




	if (r1.l < r2.r && r2.l < r1.r && r1.t < r2.b && r2.t < r1.b)
	{
		return true;
	}
	return false;
}


//bool Circle_Rect(Object* obj1, Object* obj2)
//{
//
//}
//
//bool Circle_Circle(Object* obj1, Object* obj2)
//{
//
//}
//
//


