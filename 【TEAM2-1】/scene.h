#pragma once

class BaseScene
{
protected:
	int timer;
	int state;
public:
	static int stageNumber;
	BaseScene()
	{
		timer = 0;
		state = 0;
	}
	virtual ~BaseScene() { unInit(); }
	virtual void Init()
	{
		timer = 0;
		state = 0;
	}
	virtual void Update() {};
	virtual void Draw() {};
	virtual void unInit() {};
	virtual void ImGui() {};
};

class sceneManager
{
public:
	enum SCENE
	{
		TITLE = 0,//タイトルシーン
		GAME,		//ゲームシーン
		OVER,
		CLEAR
	};
	static bool isSwitchScene;
	static SCENE next_scene;

	sceneManager()
	{
		start = true;
	}

	~sceneManager()
	{
		if (m_pScene)delete m_pScene;
		m_pScene = nullptr;
	}

	static void changeScene(SCENE scene);
	static void Init();
	static void unInit();
	static void Update();
	static void Draw();
	static void execute();

private:
	static BaseScene* m_pScene;
	static bool start;

};

