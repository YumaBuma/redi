#pragma once
#include "Object.h"
#include <memory>

class objBGManager
{
private:
	objBGManager() = default;
	objBGManager(objBGManager& copy) {}
	~objBGManager() 
	{
		if (CopyList.empty() == false)
			CopyList.erase(CopyList.begin(), CopyList.end());
		if (multianimData.empty() == false)
			multianimData.erase(multianimData.begin(), multianimData.end());
	}
	bool selected[4];
	Vector2 praPos;
	static Vector2 pos[2];
	int layer;
	int mousePos[4];
	bool useGrid;
	static int gritSize[2];
	bool comparision;
	bool useMouse;
	int mouseButtons[2];
	int listbox_group_current[2];
	int listbox_item_current[2];
	bool gripObj;
	bool noTouchWall;
	bool textureEditFlg;
	std::vector<std::shared_ptr<Object>> CopyList;
	int textureNumber;

	std::vector<Animation> multianimData;


	enum select
	{
		put = 0,
		copy,
		move,
		remove,
		end,
	};
	enum axis
	{
		x = 0,
		y,
		prevX,
		prevY
	};
	enum mouseState
	{
		previous = 0,
		now,
	};
public:
	static objBGManager* getInstance()
	{
		static objBGManager instance;
		return &instance;
	}

public:
	void init();
	void update();
	void setObj(int _id, int _personalID, Vector2 pos, bool move);
	void Debug();
	void ImGui();
	void draw();
	void loadFile();
	void saveFile();
	
	void MultianimImGui(string _tag);
	void MultiInputFile(string txt);
	void MultiOutputFile(string txt);

	void unInit()
	{
		//{ std::vector<std::shared_ptr<Object>>().swap(CopyList); }
	}

};

#define pobjBGManager (objBGManager::getInstance())
