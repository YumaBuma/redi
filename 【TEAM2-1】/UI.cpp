#include "UI.h"
#include "Camera.h"
#include "ObjectManager.h"
#include "Object.h"
#include "Player.h"
#include "Texture.h"
#include "scene.h"
#include "GameMenu.h"
UI ui;

int PL_HEAL_SPEED = 10;
int PL_DAMAGE_SPEED = 15;
int DPS_INTERVAL = 15;

UI::UI()
{
	bag_tutorial = true;
	bag_tutorial_count = 0;
	dps = 0;
	tmp_money = 0;
	ui_equipX = { -1,0 };
	ui_equipY = { -1,0 };

}

UI::~UI()
{

}

void UI::Init()
{
	apeal_pos = { 0.f,0.f };
	apeal_cnt = 0;
	isGain = false;

	info[PL_HP].tex_size = { 226,14 };
	info[PL_HP].pos = { 10,332 };
	info[EQUIP_X].tex_size = { 32,32 };
	info[EQUIP_X].pos = { 10,296 };
	info[EQUIP_Y].tex_size = { 32,32 };
	info[EQUIP_Y].pos = { 47,296 };
	info[MONEY].tex_size = { 64,20 };
	info[MONEY].pos = { 84,308 };
	info[UI_BAG].tex_size = { 38,18 };
	info[UI_BAG].pos = { 460,300 };
	info[UI_ARROW].tex_size = { 32,32 };
	info[UI_ARROW].pos = { 428,312 };

	ui_equipX = { -1,0 };
	ui_equipY = { -1,0 };

}

void UI::Update()
{
	tmp_MAX_MONEY = pObjManager->ObjList[Object::PLAYER].back()->money;



	if (pObjManager->ObjList[Object::PLAYER].empty())
	{
		return;
	}

	info[PL_HP].scale.x = (float)pObjManager->ObjList[Object::PLAYER].back()->getHP(1) / (float)INIT_MAX_PL_HP;
	if (info[PL_HP].scale.x > 6.f)
	{
		info[PL_HP].scale.x = 6.f;
	}

	tmp_MAX_PL_HP += PL_HEAL_SPEED;
	if (tmp_MAX_PL_HP >= pObjManager->ObjList[Object::PLAYER].back()->getHP(1))
	{
		tmp_MAX_PL_HP = pObjManager->ObjList[Object::PLAYER].back()->getHP(1);
	}

	dps++;
	if (DPS_INTERVAL < dps)
	{
		tmp_PL_HP -= PL_DAMAGE_SPEED;
	}

	if (tmp_PL_HP <= pObjManager->ObjList[Object::PLAYER].back()->getHP(0))
	{
		tmp_PL_HP = pObjManager->ObjList[Object::PLAYER].back()->getHP(0);
		dps = 0;
	}

	ui_money_Update();

	if (isGain)
	{
		apeal_cnt++;
		if (apeal_cnt > 60)
		{
			apeal_pos.y -= 40.0f / apeal_cnt;
		}
		if (apeal_cnt > 210)
		{
			apeal_weapon = { -1,-1 };
			apeal_cnt = 0;
			isGain = false;
		}
	}

	if (Gmenu.gain_count == 1)
	{
		if (bag_tutorial)
		{
			bag_tutorial_count++;
			if (bag_tutorial_count % 3 == 0)
			{
				if (bag_tutorial_count / 10 % 2)
				{
					info[UI_ARROW].pos.x += 0.6f;
				}
				else
				{
					info[UI_ARROW].pos.x -= 0.6f;
				}
			}
		}
	}
}

void UI::Draw()
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);

	pl_ui_Draw();
	enm_ui_Draw();


	//Tutorial

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

	if (BaseScene::stageNumber == 1)
	{
		//jump
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 1566.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 1780.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 1790.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 1890.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 0, 84, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		}

		//down jump
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2106.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 2304.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 1940.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 2000.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 24, 123, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		}

		//ladder down
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2026.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 2293.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 2370.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 2550.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 48, 123, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);

		}

		//mode_ladder OFF
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2077.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 2116.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 2585.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 2785.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 72, 123, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		}

		//ladder up
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2059.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 2135.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 2911.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 2950.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 96, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		}

		//dodge roll
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2255.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 2390.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 2911.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 2955.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 120, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		}

		//three attack
		if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2950.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 3450.0f &&
			pObjManager->ObjList[Object::PLAYER].back()->pos.y > 2943.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 3136.0f)
		{
			DrawRectGraph(
				cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
				cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150,
				0, 144, 106, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);

		}
	}

	DrawRectGraph(
		info[UI_BAG].pos.x,
		info[UI_BAG].pos.y,
		0,
		544,
		info[UI_BAG].tex_size.x,
		info[UI_BAG].tex_size.y,
		pTexture->getGraphs(pTexture->UI), TRUE
		);
	if (Gmenu.gain_count == 1 && bag_tutorial)
	{
		DrawRectGraph(
			info[UI_ARROW].pos.x,
			info[UI_ARROW].pos.y,
			32,
			608,
			info[UI_ARROW].tex_size.x,
			info[UI_ARROW].tex_size.y,
			pTexture->getGraphs(pTexture->UI), TRUE
			);
	}
	DrawRectGraph(
		info[UI_BAG].pos.x,
		info[UI_BAG].pos.y + 20,
		0,
		562,
		info[UI_BAG].tex_size.x,
		info[UI_BAG].tex_size.y,
		pTexture->getGraphs(pTexture->UI), TRUE
		);

	Interactive();

	if (isGain)
	{
		if (apeal_cnt > 60)
		{
			if (apeal_weapon.id != -1)
			{
				DrawRectGraph(cam.xi_ext(apeal_pos.x - 16), cam.yi_ext(apeal_pos.y - 32), 32 * apeal_weapon.id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
				DrawRectGraph(cam.xi_ext(apeal_pos.x - 16), cam.yi_ext(apeal_pos.y - 32), 32 * (apeal_weapon.rarity - 1), 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
			}
		}
	}

	//air jump
	//if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2518.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.x < 2695.0f &&
	//	pObjManager->ObjList[Object::PLAYER].back()->pos.y > 2846.0f && pObjManager->ObjList[Object::PLAYER].back()->pos.y < 2976.0f)
	//{
	//	DrawRectExtendGraph(
	//		cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) - 35, cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 153, cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 80,
	//		cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 127, 0, 20, 480, 26, pTexture->getGraphs(pTexture->UI_BUTTON), TRUE);
	//	DrawRectGraph(cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) - 35, cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 153, 0, 20, 3, 26, pTexture->getGraphs(pTexture->UI_BUTTON), TRUE);
	//	DrawRectGraph(cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 80, cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 153, 477, 20, 3, 26, pTexture->getGraphs(pTexture->UI_BUTTON), TRUE);
	//	DrawRectGraph(cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) - 30, cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 150, 20, 0, 20, 20, pTexture->getGraphs(pTexture->UI_BUTTON), TRUE);
	//	DrawString(cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x), cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 148, "��?�h?", GetColor(255, 255, 255));
	//}


}

void UI::Interactive()
{
	if (interact == -1)return;

	switch (interact)
	{
	case 0:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 192, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	case 1:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 216, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	case 2:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 240, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	case 3:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 264, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	case 4:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 288, 72, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	case 5:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 312, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	case 6:
		DrawRectGraph(
			cam.xi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.x) + 35,
			cam.yi_ext(pObjManager->ObjList[Object::PLAYER].back()->pos.y) - 100,
			0, 336, 62, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);
		break;
	default:
		break;
	}
}

void UI::ui_money_Update()
{
	if (tmp_money != tmp_MAX_MONEY)
	{
		if (tmp_money < tmp_MAX_MONEY)
		{
			if (tmp_MAX_MONEY - tmp_money > 500)
			{
				tmp_money += 100;
			}
			else if (tmp_MAX_MONEY - tmp_money > 250)
			{
				tmp_money += 30;
			}
			else if (tmp_MAX_MONEY - tmp_money > 100)
			{
				tmp_money += 10;
			}
			else
			{
				tmp_money += 3;
			}
			if (tmp_money > tmp_MAX_MONEY)
			{
				tmp_money = tmp_MAX_MONEY;
			}
		}
		if (tmp_money > tmp_MAX_MONEY)
		{
			if (tmp_money - tmp_MAX_MONEY > 250)
			{
				tmp_money -= 100;
			}
			else if (tmp_money - tmp_MAX_MONEY > 250)
			{
				tmp_money -= 30;
			}
			else if (tmp_money - tmp_MAX_MONEY > 100)
			{
				tmp_money -= 10;
			}
			else
			{
				tmp_money -= 3;
			}
			if (tmp_money < tmp_MAX_MONEY)
			{
				tmp_money = tmp_MAX_MONEY;
			}

		}
	}
}

void UI::pl_ui_Draw()
{
	DrawRectExtendGraph(
		info[PL_HP].pos.x,
		info[PL_HP].pos.y,
		info[PL_HP].pos.x + info[PL_HP].tex_size.x * info[PL_HP].scale.x,
		info[PL_HP].pos.y + info[PL_HP].tex_size.y, 0, 28,
		info[PL_HP].tex_size.x, info[PL_HP].tex_size.y,
		pTexture->getGraphs(pTexture->UI), TRUE
		);
	DrawRectExtendGraph(
		info[PL_HP].pos.x,
		info[PL_HP].pos.y,
		info[PL_HP].pos.x + info[PL_HP].tex_size.x* tmp_PL_HP / pObjManager->ObjList[Object::PLAYER].back()->getHP(1)* info[PL_HP].scale.x,
		info[PL_HP].pos.y + info[PL_HP].tex_size.y, 0, 14,
		info[PL_HP].tex_size.x* tmp_PL_HP / pObjManager->ObjList[Object::PLAYER].back()->getHP(1), info[PL_HP].tex_size.y,
		pTexture->getGraphs(pTexture->UI), TRUE
		);

	DrawRectExtendGraph(
		info[PL_HP].pos.x,
		info[PL_HP].pos.y,
		info[PL_HP].pos.x + info[PL_HP].tex_size.x * pObjManager->ObjList[Object::PLAYER].back()->getHP(0) / pObjManager->ObjList[Object::PLAYER].back()->getHP(1) * info[PL_HP].scale.x,
		info[PL_HP].pos.y + info[PL_HP].tex_size.y, 0, 0,
		info[PL_HP].tex_size.x * pObjManager->ObjList[Object::PLAYER].back()->getHP(0) / pObjManager->ObjList[Object::PLAYER].back()->getHP(1), info[PL_HP].tex_size.y,
		pTexture->getGraphs(pTexture->UI), TRUE
		);

	if (ui_equipX.id != -1)
	{
		DrawRectGraph(
			info[EQUIP_X].pos.x,
			info[EQUIP_X].pos.y,
			info[EQUIP_X].tex_size.x*ui_equipX.id,
			64,
			info[EQUIP_X].tex_size.x,
			info[EQUIP_X].tex_size.y,
			pTexture->getGraphs(pTexture->UI), TRUE
			);

		DrawRectGraph(
			info[EQUIP_X].pos.x,
			info[EQUIP_X].pos.y,
			info[EQUIP_X].tex_size.x*(ui_equipX.rarity - 1),
			128,
			info[EQUIP_X].tex_size.x,
			info[EQUIP_X].tex_size.y,
			pTexture->getGraphs(pTexture->UI), TRUE
			);

		DrawRectGraph(
			info[EQUIP_X].pos.x + 8,
			info[EQUIP_X].pos.y - 18,
			0,
			432,
			15,
			16,
			pTexture->getGraphs(pTexture->UI), TRUE
			);
	}
	if (ui_equipY.id != -1)
	{
		DrawRectGraph(
			info[EQUIP_Y].pos.x,
			info[EQUIP_Y].pos.y,
			info[EQUIP_Y].tex_size.x*ui_equipY.id,
			64,
			info[EQUIP_Y].tex_size.x,
			info[EQUIP_Y].tex_size.y,
			pTexture->getGraphs(pTexture->UI), TRUE
			);
		DrawRectGraph(
			info[EQUIP_Y].pos.x,
			info[EQUIP_Y].pos.y,
			info[EQUIP_Y].tex_size.x*(ui_equipY.rarity - 1),
			128,
			info[EQUIP_Y].tex_size.x,
			info[EQUIP_Y].tex_size.y,
			pTexture->getGraphs(pTexture->UI), TRUE
			);
		DrawRectGraph(
			info[EQUIP_Y].pos.x + 8,
			info[EQUIP_Y].pos.y - 18,
			15,
			432,
			15,
			16,
			pTexture->getGraphs(pTexture->UI), TRUE
			);


	}

	DrawRectGraph(
		info[MONEY].pos.x,
		info[MONEY].pos.y,
		0,
		44,
		64,
		20,
		pTexture->getGraphs(pTexture->UI), TRUE
		);

	{
		DrawRectGraph(
			info[MONEY].pos.x + 54,
			info[MONEY].pos.y + 4,
			8 * (tmp_money % 10),
			96,
			8,
			12,
			pTexture->getGraphs(pTexture->UI), TRUE
			);
		if (tmp_money >= 10)
		{
			DrawRectGraph(
				info[MONEY].pos.x + 44,
				info[MONEY].pos.y + 4,
				8 * (tmp_money / 10 % 10),
				96,
				8,
				12,
				pTexture->getGraphs(pTexture->UI), TRUE
				);
		}
		if (tmp_money >= 100)
		{
			DrawRectGraph(
				info[MONEY].pos.x + 34,
				info[MONEY].pos.y + 4,
				8 * (tmp_money / 100 % 10),
				96,
				8,
				12,
				pTexture->getGraphs(pTexture->UI), TRUE
				);
		}
		if (tmp_money >= 1000)
		{
			DrawRectGraph(
				info[MONEY].pos.x + 24,
				info[MONEY].pos.y + 4,
				8 * (tmp_money / 1000 % 10),
				96,
				8,
				12,
				pTexture->getGraphs(pTexture->UI), TRUE
				);
		}
		if (tmp_money >= 10000)
		{
			DrawRectGraph(
				info[MONEY].pos.x + 14,
				info[MONEY].pos.y + 4,
				8 * (tmp_money / 10000 % 10),
				96,
				8,
				12,
				pTexture->getGraphs(pTexture->UI), TRUE
				);
		}
	}
}

void UI::enm_ui_Draw()
{
	for (auto& it : pObjManager->ObjList[Object::ENEMY])
	{
		if (!it->m_isVisible)
			continue;
		if (BaseScene::stageNumber != 5)
		{
			if (it->getHP(0) != it->getHP(2))
			{
				DrawRectExtendGraph(
					cam.xi_ext(it->pos.x - 9),
					cam.yi_ext(it->pos.y - it->size.y - 10),
					cam.xi_ext(it->pos.x + 9),
					cam.yi_ext(it->pos.y - it->size.y - 4),
					0, 120, 18, 6,
					pTexture->getGraphs(pTexture->UI), TRUE
					);
				DrawRectExtendGraph(
					cam.xi_ext(it->pos.x - 9),
					cam.yi_ext(it->pos.y - it->size.y - 10),
					cam.xi_ext(it->pos.x - 9 + (18.0f / it->getHP(2))*it->getHP(1)),
					cam.yi_ext(it->pos.y - it->size.y - 4),
					0, 114, 18, 6,
					pTexture->getGraphs(pTexture->UI), TRUE
					);

				DrawRectExtendGraph(
					cam.xi_ext(it->pos.x - 9),
					cam.yi_ext(it->pos.y - it->size.y - 10),
					cam.xi_ext(it->pos.x - 9 + (18.0f / it->getHP(2))*it->getHP(0)),
					cam.yi_ext(it->pos.y - it->size.y - 4),
					0, 108, 18, 6,
					pTexture->getGraphs(pTexture->UI), TRUE
					);


			}
		}
		else if (it->getATK(7).x >= 3)
		{
			int alpha = (255.0f / it->getHP(0))*it->getHP(3);
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);

			DrawRectExtendGraph(
				50,
				0,
				590,
				24,
				0, 208, 540, 24,
				pTexture->getGraphs(pTexture->UI), TRUE
				);
			DrawRectExtendGraph(
				50,
				0,
				50 + (540.0f / it->getHP(2))*it->getHP(1),
				24,
				0, 184, (540.0f / it->getHP(2))*it->getHP(1), 24,
				pTexture->getGraphs(pTexture->UI), TRUE
				);

			DrawRectExtendGraph(
				50,
				0,
				50 + (540.0f / it->getHP(2))*it->getHP(3),
				24,
				0, 160, (540.0f / it->getHP(2))*it->getHP(3), 24,
				pTexture->getGraphs(pTexture->UI), TRUE
				);

			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

		}
	}

}

void UI::ImGui()
{


	ImGui::DragInt("HEAL_SPEED##Player", &PL_HEAL_SPEED);

	ImGui::DragInt("DAMAGE_SPEED##Player", &PL_DAMAGE_SPEED);


	ImGui::DragInt("DPS_INTERVAL##Player", &DPS_INTERVAL);
}
