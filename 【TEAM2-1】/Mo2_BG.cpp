#include "Mo2_BG.h"
#include "Texture.h"
#include "common.h"
#include "Input.h"
#include "Camera.h"
#include "ObjectManager.h"

Vector2 camera_dist;

Mo2_BG::Mo2_BG()
{
	Init();
}

Mo2_BG::~Mo2_BG()
{
	delete map;
}

void Mo2_BG::Init()
{
	stageNumber = 0;
	map = nullptr;
	//setMAP(STAGE_ID::ST_NONE);
	camera_pos = { 0,0 };
	camera_move_rate = 0.02f;
	camera_max_speed = 15.0f;
	camera_dist = { 0,0 };
	for (int i = 0; i < 14; i++)
	{
		scrollPos[i] = { 0,0 };
	}
	plPos = { 0,0 };
	groundPos = 0;
	collect = 0;
	for (int i = 0; i < 14; i++)
	{
		scrollStandard[i] = { 0,0 };
		bgFirstPos[i] = 0;
		bgScrollFin[i] = 360;
	}
	stageNumber = 0;

	mousePos = { 0,0 };
	mouseButton[0] = 0;
	mouseButton[1] = 0;
	renderPos = { 0,0 };
}

void Mo2_BG::Update()
{
	if (collect == 0 && Iwin_flg.bg)
	{
		mouseButton[0] = GetMouseInput();
		mousePos = { (float)((int)cam.GetWorldPosFromCursorPos().x / CHIP_SIZE)*CHIP_SIZE,(float)((int)cam.GetWorldPosFromCursorPos().y / CHIP_SIZE)*CHIP_SIZE };
		renderPos = { cam.xf_ext(mousePos.x),cam.yf_ext(mousePos.y) };
		if ((mouseButton[0] & MOUSE_INPUT_RIGHT) && !(mouseButton[1] & MOUSE_INPUT_RIGHT))
		{
			groundPos = mousePos.y / CHIP_SIZE;
		}

		//マウスの情報を保存
		mouseButton[1] = mouseButton[0];
	}


	//if (pObjManager->ObjList[Object::PLAYER].empty() == false)
	//	plPos = pObjManager->ObjList[Object::PLAYER].back()->pos;
	plPos = { (cam.GetCamXF() + 160.f) / CHIP_SIZE,(cam.GetCamYF() + 120.f) / CHIP_SIZE };

	for (int i = 0; i < 14; i++)
	{
		scrollPos[i].x = (plPos.x / mapSize.x) * (scrollStandard[i].x);
	}

	if (groundPos >= plPos.y)
	{
		for (int i = 0; i < 7; i++)
		{
			scrollPos[i].y = bgFirstPos[i] - ((groundPos - plPos.y) / groundPos)*scrollStandard[i].y;

			if (scrollPos[i].y < bgScrollFin[i])
				scrollPos[i].y = bgScrollFin[i];
		}
	}
	else
	{
		for (int i = 7; i < 14; i++)
		{
			scrollPos[i].y = bgFirstPos[i] - ((mapSize.y - plPos.y) / mapSize.y)*scrollStandard[i].y;

			if (scrollPos[i].y < bgScrollFin[i])
				scrollPos[i].y = bgScrollFin[i];
		}

	}


}

void Mo2_BG::Draw()
{
	//DrawGraph(0, 0, pTexture->getGraphs(pTexture->BACK), TRUE);


	float ext				= cam.chipextrate();
	float ext_target		= cam.chipextrate_target();
	float chipsize			= (float)CHIP_SIZE*ext;
	float chipsize_target	= (float)CHIP_SIZE*ext_target;


	float	draw_start_chip_x = cam.GetCamXF() / chipsize;
	float	draw_start_chip_y = cam.GetCamYF() / chipsize;
		if (draw_start_chip_x < 0.f)draw_start_chip_x = 0.f;
		if (draw_start_chip_y < 0.f)draw_start_chip_y = 0.f;

	float draw_iterate_num_x = ((float)TEMP_SCREEN_WIDTH / chipsize_target);
	float draw_iterate_num_y = ((float)TEMP_SCREEN_HEIGHT / chipsize_target);
		draw_iterate_num_x *= (1.f / ext);
		draw_iterate_num_y *= (1.f / ext);
		draw_iterate_num_x += (draw_start_chip_x) + 1.f;
		draw_iterate_num_y += (draw_start_chip_y) + 1.f;
	if (draw_iterate_num_x > (float)chip.x)draw_iterate_num_x = (float)chip.x;
	if (draw_iterate_num_y > (float)chip.y)draw_iterate_num_y = (float)chip.y;

	static int grass = 0;
	for (int y = static_cast<int>(draw_start_chip_y); y < static_cast<int>(draw_iterate_num_y); y++)
	{
		for (int x = static_cast<int>(draw_start_chip_x); x < static_cast<int>(draw_iterate_num_x); x++)
		{
			int cx = cam.xi(chipsize * (float)x);
			int cy = cam.yi(chipsize * (float)y);

			int index = map[y][x];

			if (index >= 98)
			{
				grass = 98;
				index -= 98;
			}
			else {
				grass = 0;
			}

			if (index == -1)continue;


			if ((index >= 76 && index <= 83) || index == 22 || index == 36 || index == 50 || index == 64)
				continue;

			int row = 0;
			int col = 0;
			if (index != 0)
			{
				row = (index + grass) / CHIP_TEXDIV_X;
				col = (index + grass) % CHIP_TEXDIV_X;
			}
			DrawRectExtendGraph
			(
				cx,
				cy,
				cx + (int)chipsize,
				cy + (int)chipsize,
				CHIP_SIZE * col	, CHIP_SIZE * row,
				CHIP_SIZE		, CHIP_SIZE,
				pTexture->getGraphs(pTexture->BGCHIP), TRUE
			);
		}
	}

	float chip_size_reciprocal = 1.f / (float)CHIP_SIZE;
	for (auto& p = Unevenness.begin(); p != Unevenness.end(); p++)
	{
		if (p->x * chip_size_reciprocal < -3 + draw_start_chip_x)continue;
		if (p->x * chip_size_reciprocal > 3 + draw_iterate_num_x)continue;
		if (p->y * chip_size_reciprocal < -3 + draw_start_chip_y)continue;
		if (p->y * chip_size_reciprocal > 3 + draw_iterate_num_y)continue;

		DrawRectExtendGraph
		(
			cam.xi_ext(p->x),
			cam.yi_ext(p->y),
			cam.xi_ext(p->x)+(int)chipsize, cam.yi_ext(p->y) + (int)chipsize,
			((int)p->z % 14)*CHIP_SIZE, ((int)p->z / 14)*CHIP_SIZE,
			CHIP_SIZE, CHIP_SIZE,
			pTexture->getGraphs(pTexture->BGCHIP), TRUE
		);

	}

	if (collect == 0 && Iwin_flg.bg)
	{
		DrawLineBox(renderPos.x, renderPos.y, renderPos.x + 32, renderPos.y + 32, GetColor(255, 0, 0));
	}

}

void Mo2_BG::DrawBg()
{
	for (int i = pTexture->BG0; i != pTexture->BG7; i++)
	{
		DrawGraph(-scrollPos[i - pTexture->BG0].x, scrollPos[i - pTexture->BG0].y, pTexture->getGraphs(i), TRUE);
	}
	if (fogTimer[0] * fogSpeed[0] >= 1920)
		fogTimer[0] = 0;
	fogTimer[0]++;

	if (plPos.y - 11 > groundPos)
	{
		for (int i = pTexture->U_BG0; i != pTexture->U_BG6+1; i++)
		{
			DrawGraph(-scrollPos[i - pTexture->U_BG0 + 7].x, scrollPos[i - pTexture->U_BG0 + 7].y, pTexture->getGraphs(i), TRUE);
		}
	}

	DrawGraph(-(int)(fogTimer[0] * fogSpeed[0]) % 1920, 0, pTexture->getGraphs(pTexture->BG7), TRUE);
	DrawGraph(-(int)(fogTimer[0] * fogSpeed[0]) % 1920 + 1920, 0, pTexture->getGraphs(pTexture->BG7), TRUE);


}


void Mo2_BG::DrawRader()
{
	float ext 				= cam.chipextrate();
	float ext_target 		= cam.chipextrate_target();
	float chipsize 			= (float)CHIP_SIZE*ext;
	float chipsize_target 	= (float)CHIP_SIZE*ext_target;


	float draw_start_chip_x = cam.GetCamXF() / chipsize;
	float draw_start_chip_y = cam.GetCamYF() / chipsize;
	if (draw_start_chip_x < 0.f)draw_start_chip_x = 0.f;
	if (draw_start_chip_y < 0.f)draw_start_chip_y = 0.f;

	float draw_iterate_num_x = ((float)TEMP_SCREEN_WIDTH / chipsize_target);
	float draw_iterate_num_y = ((float)TEMP_SCREEN_HEIGHT / chipsize_target);
	draw_iterate_num_x *= (1.f / ext);
	draw_iterate_num_y *= (1.f / ext);
	draw_iterate_num_x += (draw_start_chip_x)+1.f;
	draw_iterate_num_y += (draw_start_chip_y)+1.f;
	if (draw_iterate_num_x > (float)chip.x)draw_iterate_num_x = (float)chip.x;
	if (draw_iterate_num_y > (float)chip.y)draw_iterate_num_y = (float)chip.y;

	static int grass = 0;
	for (int y = static_cast<int>(draw_start_chip_y); y < static_cast<int>(draw_iterate_num_y); y++)
	{
		for (int x = static_cast<int>(draw_start_chip_x); x < static_cast<int>(draw_iterate_num_x); x++)
		{
			int cx = cam.xi(chipsize * (float)x);
			int cy = cam.yi(chipsize * (float)y);

			int index = map[y][x];

			if (index >= 98)
			{
				grass = 98;
				index -= 98;
			}
			else {
				grass = 0;
			}


			if ((index >= 76 && index <= 83) || index == 22 || index == 36 || index == 50 || index == 64)
			{
				int row = 0;
				int col = 0;
				if (index != 0)
				{
					row = (index + grass) / CHIP_TEXDIV_X;
					col = (index + grass) % CHIP_TEXDIV_X;
				}
				DrawRectExtendGraph
				(
					cx,
					cy,
					cx + (int)chipsize,
					cy + (int)chipsize,
					CHIP_SIZE * col, CHIP_SIZE * row,
					CHIP_SIZE, CHIP_SIZE,
					pTexture->getGraphs(pTexture->BGCHIP), TRUE
				);
			}
		}
	}
}
void Mo2_BG::drawFog()
{
	if (fogTimer[1] * fogSpeed[1] >= 1920)
		fogTimer[1] = 0;
	fogTimer[1]++;

	DrawGraph(-(int)(fogTimer[1] * fogSpeed[1]) % 1920, 0, pTexture->getGraphs(pTexture->BG8), TRUE);
	DrawGraph(-(int)(fogTimer[1] * fogSpeed[1]) % 1920 + 1920, 0, pTexture->getGraphs(pTexture->BG8), TRUE);

}
//void Mo2_BG::setMAP(STAGE_ID st_id)
//{
//	switch (st_id)
//	{
//
//	case ST_1ST:
//		break;
//	case ST_2ND:
//		break;
//	case ST_3RD:
//		break;
//	default:
//		break;
//	}
//}


void Mo2_BG::LoadFile(int st_id)
{
	stageNumber = st_id;
	//地上の高さやスクロール速度のデータをロード
	{
		char fileName[30] = { 0 };
		sprintf_s(fileName, "Data/BGinfo/%d.txt", st_id);

		FILE* fp = nullptr;

		if (fopen_s(&fp, fileName, "r") == 0)
		{

			fscanf_s(fp, "%d", &groundPos);

			for (int i = 0; i < 14; i++)
			{
				fscanf_s(fp, "%d", &bgScrollFin[i]);

				fscanf_s(fp, "%d", &bgFirstPos[i]);

				fscanf_s(fp, "%d", &scrollStandard[i].x);
				fscanf_s(fp, "%d", &scrollStandard[i].y);

			}

			for (int i = 0; i < 2; i++)
			{
				fscanf_s(fp, "%f", &fogSpeed[i]);

			}
			fclose(fp);
		}
	}



	//前のステージのマップを消す
	if (map != nullptr)
	{
		delete map;
	}
	using namespace std;
	char* str = nullptr;
	char mapFile[40] = { 0 };
	bool check = false;
	const string delim = ",";
	string line;
	int mapX = 0;
	int mapY = 0;
	//stageNumber = 0;
	//IDに合わせて読み込みファイルを切り替え
	sprintf_s(mapFile, "Data/MapChip/Map1/map1-%d.csv", st_id);

	str = fileProcessing::readTextFile(mapFile);

	chip.x = 0;
	//項目数(横)を計測
	for (int i = 0; i < strlen(str); i++) {
		if (check)
			break;
		switch (str[i]) {
		case ',':
			chip.x++;
			break;
		case '\n':
			chip.x++;
			check = true;
			
			break;
		}
	}

	//項目数(縦)を計測
	ifstream ifs(mapFile);
	chip.y = 0;
	if (ifs)
	{
		string line;

		// 何行あるかわからないので、while文を使って繰り返す
		while (true)
		{
			// 1行読み取る。
			getline(ifs, line);

			// 1行読み取ったので、インクリメントする。
			chip.y++;

			// ファイルが終端まで来たら、break 文を実行して while 文を抜ける。
			if (ifs.eof())
				break;
		}
	}
	chip.y--;

	//計測された要素数の二次元配列の作成
		map = new int*[chip.y];

		for (int i = 0; i < chip.y; i++)
		{
			map[i] = new int[chip.x];
		}
	

	//作成した二次元配列にマップ情報をパーツごとに格納
		ifstream stream(mapFile);

		mapY = 0;
		while (getline(stream, line)) {
			mapX = 0;
			for (string::size_type spos, epos = 0; (spos = line.find_first_not_of(delim, epos)) != string::npos;) {
				string token = line.substr(spos, (epos = line.find_first_of(delim, spos)) - spos);
				map[mapY][mapX++] = stoi(token);
			}
			++mapY;
		}



	//マップサイズを算出
	mapSize.x = chip.x;
	mapSize.y = chip.y;
	createUnevenness();
}

void Mo2_BG::createUnevenness()
{
	static int grass = 0;
	for (int y = 0; y < mapSize.y; y++)
	{
		for (int x = 0; x < mapSize.x; x++)
		{
			float index = (float)map[y][x];

			if (index == -1)
				continue;

			if (index >= 98)
			{
				grass = 98;
				index -= 98;
			}
			else {
				grass = 0;
			}
			if (index == 30)
				continue;

			if (index == 15 || index == 16 || index == 17 || index == 21 || index == 71 || index == 72 || index == 73 || index == 74)
			{
				Vector3 info = { x * (float)CHIP_SIZE, (y - 1)*(float)CHIP_SIZE , (float)(rand() % 3 + 1 + grass) };

				Unevenness.emplace_back(info);

			}

			if (index == 43 || index == 44 || index == 45 || index == 63 || index == 71 || index == 72 || index == 73 || index == 74)
			{
				Vector3 info = { x * (float)CHIP_SIZE, (y + 1)*(float)CHIP_SIZE ,(float)(rand() % 3 + 57 + grass) };

				Unevenness.emplace_back(info);

			}

			if (index == 15 || index == 29 || index == 43 || index == 21 || index == 35 || index == 49 || index == 63 || index == 71)
			{
				Vector3 info = { (x - 1) * (float)CHIP_SIZE, y *(float)CHIP_SIZE ,(float)((rand() % 3 + 1) * 14 + grass) };

				Unevenness.emplace_back(info);

			}

			if (index == 17 || index == 31 || index == 45 || index == 21 || index == 35 || index == 49 || index == 63 || index == 74)
			{
				Vector3 info = { (x + 1)* (float)CHIP_SIZE, y *(float)CHIP_SIZE ,(float)((rand() % 3 + 1) * 14 + 4 + grass) };

				Unevenness.emplace_back(info);

			}

		}
	}


}

void Mo2_BG::ImGui()
{
	const char* listbox_group =
	{ "groundPos\0groundBG\0underBG\0\0" };
	static char text[15] = { 0 };

	if (!Iwin_flg.bg)
	{
		collect = 0;
		return;
	}
	ImGui::Begin("BG", &Iwin_flg.bg, ImGuiWindowFlags_MenuBar);

	ImGui::Text("mapSize\nx : %d\ny : %d\ncollect : %d", chip.x, chip.y, collect);

	if (ImGui::Button("save"))
	{
		saveFile();
	}

	if (ImGui::Button("reset"))
	{
		resetData();
	}
	ImGui::Combo("select", &collect, listbox_group);

	if (collect == 0)
	{
		ImGui::Text("mousePos");
		ImGui::Text("x : %d", mousePos.x / CHIP_SIZE);
		ImGui::Text("y : %d", mousePos.y / CHIP_SIZE);

		ImGui::NewLine();
		ImGui::Text("groundPos : %d", groundPos);
	}
	else if (collect == 1)
	{
		for (int i = 0; i < 7; i++)
		{

			ImGui::Text("BG%d", i);

			ImGui::Text("pos");
			sprintf_s(text, "y##%dfirst", i);
			ImGui::DragInt(text, &bgFirstPos[i]);

			sprintf_s(text, "##%dsize", i);
			ImGui::Text("ScrollFin");
			ImGui::DragInt(text, &bgScrollFin[i]);


			ImGui::Text("scroll");
			sprintf_s(text, "x##%d", i);
			ImGui::DragInt(text, &scrollStandard[i].x);
			sprintf_s(text, "y##%d", i);
			ImGui::DragInt(text, &scrollStandard[i].y);
		}

		for (int i = 0; i < 2; i++)
		{
			ImGui::Text("fog%d", i);
			sprintf_s(text, "##%d", i);

			ImGui::DragFloat(text, &fogSpeed[i]);

		}


	}
	else
	{
		for (int i = 7; i < 14; i++)
		{

			ImGui::Text("BG%d", i);

			ImGui::Text("pos");
			sprintf_s(text, "y##%dfirst", i);
			ImGui::DragInt(text, &bgFirstPos[i]);

			sprintf_s(text, "##%dsize", i);
			ImGui::Text("ScrollFin");
			ImGui::DragInt(text, &bgScrollFin[i]);


			ImGui::Text("scroll");
			sprintf_s(text, "x##%d", i);
			ImGui::DragInt(text, &scrollStandard[i].x);
			sprintf_s(text, "y##%d", i);
			ImGui::DragInt(text, &scrollStandard[i].y);
		}

	}
	ImGui::End();
}

void Mo2_BG::saveFile()
{
	FILE* fp = nullptr;
	char fileName[30];
	sprintf_s(fileName, "Data/BGinfo/%d.txt", stageNumber);


	fopen_s(&fp, fileName, "w");

	fprintf_s(fp, "%d\n", groundPos);

	for (int i = 0; i < 14; i++)
	{
		fprintf_s(fp, "%d\n", bgScrollFin[i]);

		fprintf_s(fp, "%d\n", bgFirstPos[i]);

		fprintf_s(fp, "%d ", scrollStandard[i].x);
		fprintf_s(fp, "%d\n", scrollStandard[i].y);

	}

	for (int i = 0; i < 2; i++)
	{
		fprintf_s(fp, "%f\n", fogSpeed[i]);
	}
	fclose(fp);



}


void Mo2_BG::resetData()
{
	LoadFile(stageNumber);
}


//-----------------------------------------------
//        search index
//-----------------------------------------------

int getData(int** map, float x, float y)
{
	int divX = static_cast<int>(x) >> 5;      // x方向のインデックス
	if (divX < 0) return 0;
	int divY = static_cast<int>(y) >> 5;      // y方向のインデックス
	if (divY < 0) return 0;

	return map[divY][divX];
}

int getTerrainAttr(float x, float y)
{
	int index = getData(pBG->getMap(), x, y);

	// インデックスが-1であればTR_NONEを返す
	if (index < 0 || index>CHIP_TEXDIV_X*CHIP_TEXDIV_Y) return -1;

	// x方向のインデックス
	int remX = index % CHIP_TEXDIV_X;

	// y方向のインデックス
	int divY = index / CHIP_TEXDIV_X;

	// 添字の範囲チェック
	//assert(remX >= 0 && remX < CHIP_DIV_X);
	//assert(divY >= 0 && divY < CHIP_DIV_Y);

	// リターン
	return terrainAttr[divY][remX];
}


//
//
//


int isHitDown(float x, float y)
{
	switch (getTerrainAttr(x, y))
	{
	case TR_ATTR::ALL_BLOCK:return TR_ATTR::ALL_BLOCK; //break;
	case TR_ATTR::UPPER_BLOCK:return TR_ATTR::UPPER_BLOCK;
	case TR_ATTR::UPPER_LADDER:return TR_ATTR::UPPER_LADDER;
	case TR_ATTR::TR_LADDER:return TR_ATTR::TR_LADDER;
	case TR_ATTR::MOVEUP_PT:return TR_ATTR::MOVEUP_PT;
	case TR_ATTR::MOVEDOWN_PT:return TR_ATTR::MOVEDOWN_PT;
	}
	return TR_ATTR::TR_NONE;
}

int isHitAll(float x, float y)
{
	switch (getTerrainAttr(x, y))
	{
	case TR_ATTR::ALL_BLOCK:return TR_ATTR::ALL_BLOCK; //break;
	case TR_ATTR::UPPER_LADDER:return TR_ATTR::UPPER_LADDER;
	case TR_ATTR::TR_LADDER:return TR_ATTR::TR_LADDER;
	}
	return TR_ATTR::TR_NONE;
}

bool isFloor(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitDown(x - width, y) == TR_ATTR::ALL_BLOCK) return true;       // 左端から
		if (isHitDown(x + width, y) == TR_ATTR::ALL_BLOCK) return true;       // 右端から
	}
	return (isHitDown(x, y) == TR_ATTR::ALL_BLOCK) ? true : false;                             // 最後に真ん中で判定
}

bool isUpperFloor(float x, float y, float width)
{
	if (wrap(static_cast<int>(y), 0, CHIP_SIZE) > 16) return false;

	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitDown(x - width, y) == TR_ATTR::UPPER_BLOCK || isHitDown(x - width, y) == TR_ATTR::UPPER_LADDER) return true;       // 左端から
		if (isHitDown(x + width, y) == TR_ATTR::UPPER_BLOCK || isHitDown(x + width, y) == TR_ATTR::UPPER_LADDER) return true;       // 右端から
	}
	return (isHitDown(x, y) == TR_ATTR::UPPER_BLOCK || isHitDown(x, y) == TR_ATTR::UPPER_LADDER) ? true : false;                    // 最後に真ん中で判定
}

bool isNone(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitDown(x - width, y) == TR_ATTR::TR_NONE) return true;       // 左端から
		if (isHitDown(x + width, y) == TR_ATTR::TR_NONE) return true;       // 右端から
	}
	return (isHitDown(x, y) == TR_ATTR::TR_NONE) ? true : false;                             // 最後に真ん中で判定
}

//天井
bool isCeiling(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)
	{
		if (isHitAll(x - width, y) == TR_ATTR::ALL_BLOCK)return true;
		if (isHitAll(x + width, y) == TR_ATTR::ALL_BLOCK)return true;
	}
	return (isHitAll(x, y) == TR_ATTR::ALL_BLOCK) ? true : false;
}


bool isWall(float x, float y, float height)
{
	height -= 20;
	for (; height > 0; height -= CHIP_SIZE)
	{
		if (isHitAll(x, y - height) == TR_ATTR::ALL_BLOCK)return true;
	}
	return false;
}

bool isTurn(float x, float y, float height)
{
	height -= 20;
	for (; height > 0; height -= CHIP_SIZE)
	{
		if (isHitAll(x, y - height) == TR_ATTR::ENEMY_TURN)return true;
	}
	return false;
}

bool isLadderUp(float x, float y, float height)
{
	for (; height > 0; height -= CHIP_SIZE)
	{
		if (isHitAll(x, y - height) == TR_ATTR::TR_LADDER || isHitAll(x, y - height) == TR_ATTR::UPPER_LADDER)return true;
	}
	return false;
}

bool isLadderDown(float x, float y)
{
	//for (; height > 0; height -= CHIP_SIZE)
	{
		if (isHitDown(x, y + 1) == TR_ATTR::TR_LADDER || isHitDown(x, y + 1) == TR_ATTR::UPPER_LADDER)return true;
	}
	return false;
}


void mapHoseiDown(Object* obj)
{
	static constexpr float ADJUST_Y = 0.125f;
	float y = obj->pos.y;                          // わかりやすく書くためいったんyに代入
	y -= wrap(y, 0.0f, static_cast<float>(CHIP_SIZE));  // 0.0fからCHIP_SIZEまでの間をラップアラウンドさせる
	obj->pos.y = (y - ADJUST_Y);                     // 少し浮かせる
	obj->speed.y = (std::min)(obj->speed.y, 0.0f);      // 地面にあたったので速度が止まる
}


void mapHoseiUp(Object* obj)
{
	static constexpr float ADJUST_Y = 0.125f;
	float y = obj->pos.y - obj->size.y;
	y -= wrap(static_cast<int>(y), -static_cast<float>(CHIP_SIZE), 0);
	obj->pos.y = (y + obj->size.y + ADJUST_Y);
	if (obj->speed.y < 0)obj->speed.y = (0.0f);
}


void mapHoseiRight(Object* obj)
{
	static constexpr float ADJUST_X = 0.125f;
	float x = obj->pos.x + obj->size.x*0.5f;
	x -= wrap(x, 0.0f, static_cast<float>(CHIP_SIZE));
	obj->pos.x = x - obj->size.x*0.5f - ADJUST_X;
	obj->speed.x = 0.0f;
}

void mapHoseiLeft(Object* obj)
{
	static constexpr float ADJUST_X = 0.125f;
	float x = obj->pos.x - obj->size.x*0.5f;
	x -= wrap(x, -(static_cast<float>(CHIP_SIZE)), 0.0f);
	obj->pos.x = x + obj->size.x*0.5f + ADJUST_X;
	obj->speed.x = 0.0f;
}
