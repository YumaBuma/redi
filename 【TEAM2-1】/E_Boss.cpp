#include "Enemy.h"
#include "Mo2_BG.h"
#include "ObjectManager.h"
#include "Missile.h"
#include "Texture.h"
#include "Sound.h"
#include "EffectManager.h"

int E_Boss::personal_counter = 0;
int E_Boss::attackPoint[3] = { 0 };
int E_Boss::rate[4][3] = { 0 };
int E_Boss::max_hp = 10;
int E_Boss::intarval = 200;
int E_Boss::random = 0;
int E_Boss::chargeTime[5] = { 0 };
float E_Boss::range[bossRange::end_range] = { 0 };
float E_Boss::walkSpeed = 0;
float E_Boss::jumpPower = 0;
float E_Boss::dashSpeed = 0;
Vector2 E_Boss::atkSize[2] = { { 0.f,0.f } };
Vector2 E_Boss::distPos[2] = { { 0.f,0.f } };

E_Boss::E_Boss()
{
	id.CLASS = E_BOSS;
	id.PERSONAL = personal_counter;
	personal_counter++;
}

E_Boss::~E_Boss()
{
	pSound->playSE(pSound->SE_BOSS_DEATH,false);
	personal_counter--;
	anmefc_head_root->is_stop_emit = true;
	anmefc_head_root->SetIsStaticEffect(false);
	anmefc_atk_root->is_stop_emit = true;
	anmefc_atk_root->SetIsStaticEffect(false);
	EFC_MGR->AddEfcRoot(std::move(anmefc_head_root));
	EFC_MGR->AddEfcRoot(std::move(anmefc_atk_root));
	EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("boss_dead.bin"), false, pos);
	for (size_t i = 0; i < 2; i++)
	{
		EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("jewerly.bin"), false, pos);
	}
}

void E_Boss::Init()
{
	noDamageTimer = 0;
	isAttack = false;
	attackState = 0;
	nowState = 0;
	plAttackNum = 0;
	isDamage = false;
	stayTimer = 0;
	speed = { 0,0 };
	MAX_SPEED.x = 10.f;
	MAX_SPEED.y = 15.f;
	INITIAL_SPEED.x = 2.f;
	INITIAL_SPEED.y = 0.f;
	act_interval = 0;
	notFound = 500;
	checkSpan = 10;
	walkCount = 0;
	canBackStep = true;
	isGrip = false;
	isErase = false;
	isAnim = true;
	isStay = false;
	isAttack = false;
	onGround = false;
	size = { 30,70 };
	loadAnimFile("boss");
	hp = max_hp;
	maxHP = max_hp;
	upperHP = 0;
	temp_hp = hp;
	attackPos = { 0,0 };
	attackSize = { 0,0 };
	isFin = true;
	state = 0;
	timer = 0;
	// effect
	{
		Animation anim;
		anim.loadAnimFile("boss");
		anmefc_head_root = EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("boss_head.bin"), anim, /*is_static_effect =*/ true, pos);

		//anim = Animation();
		anmefc_atk_root = EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("boss_atk.bin"), anim, /*is_static_effect =*/ true, pos);
	}
}

bool E_Boss::Update()
{
	if (Iwin_flg.objBG || Iwin_flg.enemy) return true;

	switch (state)
	{
	case 0:
		nowState = 0;
		if (pObjManager->distanceToPlayer(*this) <450)
			state++;
		break;

	case 1:
		if (timer++ > 60)
		{
			timer = 160;
			state++;
		}
		isFlipX = true;
		nowState = 4;
		break;

	case 2:
		if (timer-- <= 0)
		{
			state++;
			if (nowState != 1)
			{
				nowState = 1;
				attackState = 0;
				isAttack = false;
			}
		}
		if (pObjManager->distanceToPlayer(*this) < 200)
			if (nowState != 1)
			{
				nowState = 1;
				attackState = 0;
				isAttack = false;
			}
		break;
	case 3:
	if (noDamageTimer)
	{
		noDamageTimer--;
	}
	else
	{

		plAttackNum = 0;
	}
		if (upperHP < hp)
		{
			upperHP += 240.0f;
			temp_hp = upperHP;
		}
		else
			upperHP = hp;

		if (hp <= temp_hp)
			temp_hp -= 10.0f;

	searchAlg(pObjManager->distanceToPlayer(*this));



	}
	Gravity();

	PosUpdateY();

	checkAreaY();

	Behavior(pObjManager->ObjList[Object::PLAYER].begin()->get());

	PosUpdateX();


	checkAreaX();

	animUpdate();

	return true;
}

void E_Boss::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	if (cam.IsInCameraFoot(pos, Vector2((float)tex_size.x * 0.5f, (float)tex_size.y)) == false) return;
#if ENABLE_SMALL_DRAW // 縮小表示への対応
	if (noDamageTimer)
		SetDrawBright(255, 0, 0);
	DrawRectRotaGraphFast2
	(
		cam.xi_ext(pos.x - tex_size.x * 0.5f),
		cam.yi_ext(pos.y - tex_size.y),
		scasi(tex_pos.x) + tex_size.x * aFrame,
		scasi(tex_pos.y) + tex_size.y * now_anim,
		tex_size.x,
		tex_size.y,
		0, 0,
		cam.chipextrate_d(),
		0.0,
		pTexture->getGraphs(Texture::BOSS),
		TRUE, isFlipX
	);
	SetDrawBright(255, 255, 255);


#else
	DrawRectGraph(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
		tex_pos.x + tex_size.x * aFrame,
		tex_pos.y + tex_size.y * now_anim,
		tex_size.x, tex_size.y, pTexture->getGraphs(Texture::ENEMY), TRUE, isFlipX);
#endif
	anmefc_head_root->Draw();
	anmefc_atk_root->Draw();

	if (show_debug)
		DrawLineBox
		(
			cam.xi_ext(pos.x + attackPos.x - attackSize.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y + attackPos.y - attackSize.y)/* - camera_pos.y*/,
			cam.xi_ext(pos.x + attackPos.x + attackSize.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y + attackPos.y) /*- camera_pos.y*/,
			GetColor(255, 5, 80));

}


void E_Boss::Gravity()
{
	onGround = false;

	speed.y += GRAVITY;
	if (speed.y >= MAX_SPEED.y)
	{
		speed.y = MAX_SPEED.y;
	}
}

void E_Boss::checkAreaX()
{
	if (delta.x < 0)
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			mapHoseiRight(this);

			if (isAttack)
				speed.x = 0;
			else if (!walkCount)
			{
				isStay = false;
				isFlipX = true;
			}
			else isStay = true;
		}
	}

	if (delta.x > 0)
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			mapHoseiLeft(this);
			if (isAttack)
				speed.x = 0;

			else if (!walkCount)
			{
				isStay = false;
				isFlipX = false;
			}
			else isStay = true;
		}

	}

}

void E_Boss::checkAreaY()
{
	if (delta.y > 0)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			mapHoseiUp(this);
		}
	}
	if (delta.y < 0)
	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}

		if (isUpperFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}
	}
	if (onGround)
	{
		if (isFlipX && speed.x < 0)
		{
			if (isNone(pos.x - size.x*0.25, pos.y + 1, size.x*0.25f))
			{
				isFlipX = false;
			}

		}
		else if (!isFlipX && speed.x > 0)
		{
			if (isNone(pos.x + size.x*0.25, pos.y + 1, size.x*0.25f))
			{
				isFlipX = true;
			}

		}
	}

}

void E_Boss::Behavior(const Object* _pl)
{
	switch (nowState)
	{
	case -1:
		break;
	case 0:
		speed = { 0,0 };
		changeAnimation(bossAnim::stay);
		anmefc_head_root->ChangeAnimation(bossAnim::stay);
		anmefc_atk_root->ChangeAnimation(bossAnim::stay);
		if (stayTimer > 0)
		{
			stayTimer--;
		}
		isAttack = false;
		attackState = 0;
		break;
	case 1:
		Attack_down(_pl);
		break;
	case 2:
		Attack_up(_pl);
		break;
	case 3:
		Attack_dash(_pl);
		break;
	case 4:
		walkAlg(_pl);
		break;
	}

}

void E_Boss::Attack_up(const Object* _pl)
{
	ATK = 0;
	switch (attackState)
	{
	case 0:
		changeAnimation(bossAnim::Attack_Up);
		anmefc_head_root->ChangeAnimation(bossAnim::Attack_Up);
		anmefc_atk_root->ChangeAnimation(bossAnim::Attack_Up);
		isAttack = true;
		attackState++;
		speed.x = 0;
		if (_pl->pos.x > pos.x)
			isFlipX = false;
		else
			isFlipX = true;
		isFin = false;
	case 1:

		if (aFrame == 2)
		{
			attackState++;
			stopTimer = chargeTime[0];
		}
		break;
	case 2:
		stop_anim = true;
		if (stopTimer)
			stopTimer--;
		else
		{
			attackState++;
		}
		break;
	case 3:
		stop_anim = false;
		if (aFrame == 7)
		{
			for (int i = 0; i < 20; i++)
			{
				pObjManager->Add(std::make_shared<rocks>(attackPoint[1], pos.y, 5), Object::MISSILE, layer + 1, 1, 0);
				if (isFlipX)
				{
					pObjManager->ObjList[CLASS_ID::MISSILE].back()->pos = { pos.x - size.x - i * 20,pos.y + 64 + 20 * i };
				}
				else
					pObjManager->ObjList[CLASS_ID::MISSILE].back()->pos = { pos.x + size.x + i * 20,pos.y + 64 + 20 * i };
				pSound->playSE(pSound->SE_BOSS_ROCK, false);

			}
			attackState++;
			stopTimer = chargeTime[1];
		}
		break;
	case 4:
		stop_anim = true;
		if (stopTimer)
			stopTimer--;
		else
			attackState++;
		break;

	case 5:
		stop_anim = false;
		if (aFrame == 10)
		{
			isFin = true;
			isAttack = false;
			stayTimer = intarval;
		}
		break;
	}


}

void E_Boss::Attack_down(const Object* _pl)
{
	switch (attackState)
	{
	case 0:
		changeAnimation(bossAnim::Attack_Down);
		isFin = false;
		anmefc_head_root->ChangeAnimation(bossAnim::Attack_Down);
		anmefc_atk_root->ChangeAnimation(bossAnim::Attack_Down);
		isAttack = true;
		speed.x = 0;
		attackState++;
		if (_pl->pos.x > pos.x)
			isFlipX = false;
		else
			isFlipX = true;
	case 1:
		if (aFrame == 2)
		{
			attackState++;
			stopTimer = chargeTime[2];
		}
		break;
	case 2:
		stop_anim = true;
		if (stopTimer)
			stopTimer--;
		else
		{
			pSound->playSE(pSound->SE_BOSS_DOWN, false);
			ATK = attackPoint[0];
			attackState++;
		}
		break;
	case 3:
		stop_anim = false;
		if (aFrame == 4)
		{
			if (isFlipX)
				attackPos.x = -distPos[0].x;
			else
				attackPos.x = distPos[0].x;
			attackSize = atkSize[0];

		}
		if (aFrame == 10)
		{
			ATK = 0;
			attackPos = { 0,0 };
			attackSize = { 0,0 };
		}
		if (aFrame == 14)
		{
			isFin = true;
			isAttack = false;
			stayTimer = intarval;
		}
	}

}

void E_Boss::Attack_dash(const Object* _pl)
{
	switch (attackState)
	{
	case 0:
		changeAnimation(bossAnim::Attack_Dash);
		anmefc_head_root->ChangeAnimation(bossAnim::Attack_Dash);
		anmefc_atk_root->ChangeAnimation(bossAnim::Attack_Dash);
		speed.x = 0;
		isAttack = true;
		attackState++;
		if (_pl->pos.x > pos.x)
			isFlipX = false;
		else
			isFlipX = true;
		isFin = false;
	case 1:
		if (aFrame == 1)
		{
			attackState++;
			stopTimer = chargeTime[3];
		}
		break;
	case 2:
		stop_anim = true;
		if (stopTimer)
			stopTimer--;
		else
		{
			pSound->playSE(pSound->SE_BOSS_RASH, false);
			ATK = attackPoint[2];
			attackState++;
			if (isFlipX)
				attackPos.x = -distPos[1].x;
			else
				attackPos.x = distPos[1].x;

			/*attackSize = { tex_size.x / 1.0f,tex_size.y / 1.0f };
			attackSize.x / 2;*/
			attackSize = atkSize[1];
		}
		break;
	case 3:
		stop_anim = false;
		speed.x = dashSpeed;
		if (isFlipX)
			speed.x *= -1.0f;
		if (aFrame == 7 ||
			(isFlipX && getTerrainAttr(pos.x - size.x, pos.y) == ALL_BLOCK) ||
			(!isFlipX && getTerrainAttr(pos.x + size.x, pos.y) == ALL_BLOCK))
			attackState++;
		break;
	case 4:
		ATK = 0;
		speed.x = 0;
		attackPos = { 0,0 };
		attackSize = { 0,0 };
		if (aFrame == 9)
		{
			isFin = true;
			stayTimer = intarval;
			isAttack = false;
		}
		break;
	}

}

Vector2 E_Boss::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		return attackPos;
		break;

	case 1:
		return attackSize;
		break;
	case 2://xは攻撃力を返す(仮)
		return Vector2(ATK, plAttackNum);
		break;

	case 7:
		return Vector2(state, 0);
		break;

	}
}


void E_Boss::walkAlg(const Object * pl)
{
	if (pl->pos.x > pos.x)
		isFlipX = false;
	else
		isFlipX = true;

	switch (attackState)
	{
	case 0:
		changeAnimation(bossAnim::walk);
		anmefc_head_root->ChangeAnimation(bossAnim::walk);
		anmefc_atk_root->ChangeAnimation(bossAnim::walk);
		isFin = false;
		attackState++;
	case 1:
		speed.x = walkSpeed;
		if (isFlipX)
			speed.x *= -1.0f;

		if (sqrtf((pos.x - pl->pos.x)*(pos.x - pl->pos.x)) < 150)
			attackState++;
		break;
	case 2:
		isFin = true;
		speed.x = 0;
		attackState = 0;
		walkCount = 0;
		break;
	}
}

void E_Boss::searchAlg(float _dist)
{
	static int number = 0;
	static bool isUse[4] = { false };

	if (isAttack)return;

	if (!isFin)
		return;
	//if (_dist == -1)
	//{
	//	nowState = -1;
	//	m_isVisible = false;
	//	return;
	//}

	if (stayTimer)
	{
		nowState = 0;
		return;
	}
	number = 0;
	for (int i = 0; i < 4; i++)
	{
		isUse[i] = false;
	}
	int index = (int)_dist / 400;
	if (index > 2)index = 2;
	for (int i = 0; i < bossRange::end_range; i++)
	{
		if (_dist <= range[i])
		{
			number += rate[i][index];
			isUse[i] = true;
		}
	}
	if (number)
	{

		random = GetRand(number);
		for (int i = 0; i < bossRange::end_range; i++)
		{
			if (!isUse[i])
				continue;

			if (random < rate[i][index])
			{
				nowState = i + 1;
				break;
			}
			else
				random - rate[i][index];
		}
	}
	else
		nowState = 0;
}


void E_Boss::animUpdate()
{
	playAnimation();

	//anmefc_head_root->root_pos = { pos.x + (isFlipX * -1.0f), (pos.y + 1.f) };
	anmefc_head_root->anim->stop_anim = stop_anim;
	anmefc_head_root->root_pos = pos + (stop_anim ? Vector2(GetRandSigned(4.5f), GetRandSigned(4.5f)) : Vector2(0, 0));
	anmefc_head_root->is_flip_x = isFlipX;
	anmefc_head_root->Update();

	anmefc_atk_root->anim->stop_anim = stop_anim;
	anmefc_atk_root->root_pos = pos + (stop_anim ? Vector2(GetRandSigned(4.5f), GetRandSigned(4.5f)) : Vector2(0, 0));
	anmefc_atk_root->is_flip_x = isFlipX;
	anmefc_atk_root->Update();
}

void E_Boss::eImGui()
{
	if (Iwin_flg.e_info[Enemy_ID::E_BOSS] == false)
		return;
	static const char* actKind =
	{ "attack_down\0attack_up\0attack_dash\0track\0\0" };
	static const char* stopKind[5] =
	{ "up1","up2","down","dash" };
	static int selectAct = 0;
	static const string rKind[bossRange::end_range] =
	{ "##attack_down","##attack_up","##attack_dash","##track" };

	ImGui::Begin("boss", &Iwin_flg.e_info[Enemy_ID::E_BOSS], ImGuiWindowFlags_MenuBar);

	ImGui::Text("%d", random);

	ImGui::Combo("selectAct", &selectAct, actKind);

	string name = "range" + rKind[selectAct];
	string attackName = "AttackPoint" + rKind[selectAct];
	string attack_sName = "AttackSize" + rKind[selectAct];
	string attack_pName = "AttackPos" + rKind[selectAct];
	string rateName = "rate";
	ImGui::DragFloat(name.c_str(), &range[selectAct]);

	switch (selectAct)
	{
	case 0:
		ImGui::DragInt(attackName.c_str(), &attackPoint[0]);
		ImGui::Text("stopTime");
		ImGui::DragInt(stopKind[2], &chargeTime[2]);
		ImGui::Text(attack_pName.c_str());
		ImGui::DragFloat("x##atk_1", &distPos[0].x);
		ImGui::DragFloat("y##atk_1", &distPos[0].y);
		ImGui::Text(attack_sName.c_str());
		ImGui::DragFloat("x##size_1", &atkSize[0].x);
		ImGui::DragFloat("y##size_1", &atkSize[0].y);
		for (int i = 0; i < 3; i++)
		{
			rateName += i + "##down";
			ImGui::DragInt(rateName.c_str(), &rate[0][i]);
		}
		break;

	case 1:
		ImGui::DragInt(attackName.c_str(), &attackPoint[1]);
		ImGui::Text("stopTime");
		ImGui::DragInt(stopKind[0], &chargeTime[0]);
		ImGui::DragInt(stopKind[1], &chargeTime[1]);
		for (int i = 0; i < 3; i++)
		{
			rateName += i + "##up";
			ImGui::DragInt(rateName.c_str(), &rate[1][i]);
		}

		break;

	case 2:
		ImGui::DragFloat("dashSpeed", &dashSpeed);
		ImGui::DragInt(attackName.c_str(), &attackPoint[2]);
		ImGui::Text("stopTime");
		ImGui::DragInt(stopKind[3], &chargeTime[3]);
		ImGui::Text(attack_pName.c_str());
		ImGui::DragFloat("x##atk_2", &distPos[1].x);
		ImGui::DragFloat("y##atk_2", &distPos[1].y);
		ImGui::Text(attack_sName.c_str());
		ImGui::DragFloat("x##size_2", &atkSize[1].x);
		ImGui::DragFloat("y##size_2", &atkSize[1].y);
		for (int i = 0; i < 3; i++)
		{
			rateName += i + "##dash";
			ImGui::DragInt(rateName.c_str(), &rate[2][i]);
		}

		break;

	case 3:
		ImGui::DragFloat("walkSpeed", &walkSpeed);
		for (int i = 0; i < 3; i++)
		{
			rateName += i + "##walk";
			ImGui::DragInt(rateName.c_str(), &rate[3][i]);
		}

		break;
	}

	ImGui::Text("info");
	ImGui::DragInt("maxHP", &max_hp);
	ImGui::DragInt("intarval", &intarval);
	ImGui::NewLine();

	if (ImGui::Button("save"))
	{
		saveFile();
	}

	ImGui::End();

}

void E_Boss::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/boss_info.txt");

	fopen_s(&file, fileName, "w");

	for (int i = 0; i < bossRange::end_range; i++)
	{

		fprintf_s(file, "%f\n", range[i]);
	}
	for (int i = 0; i < 2; i++)
	{
		fprintf_s(file, "%f\n", distPos[i].x);
		fprintf_s(file, "%f\n", distPos[i].y);

		fprintf_s(file, "%f\n", atkSize[i].x);
		fprintf_s(file, "%f\n", atkSize[i].y);
	}

	fprintf_s(file, "%f\n", walkSpeed);
	fprintf_s(file, "%f\n", dashSpeed);
	for (int i = 0; i < 3; i++)
	{
		fprintf_s(file, "%d\n", attackPoint[i]);
	}
	fprintf_s(file, "%d\n", max_hp);
	fprintf_s(file, "%d\n", intarval);

	for (int i = 0; i < 5; i++)
	{
		fprintf_s(file, "%d\n", chargeTime[i]);
	}

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			fprintf_s(file, "%d\n", rate[i][j]);
		}
	}

	fclose(file);
}

void E_Boss::loadData()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/boss_info.txt");

	fopen_s(&file, fileName, "r");

	for (int i = 0; i < bossRange::end_range; i++)
	{

		fscanf_s(file, "%f", &range[i]);
	}
	for (int i = 0; i < 2; i++)
	{
		fscanf_s(file, "%f", &distPos[i].x);
		fscanf_s(file, "%f", &distPos[i].y);

		fscanf_s(file, "%f", &atkSize[i].x);
		fscanf_s(file, "%f", &atkSize[i].y);
	}


	fscanf_s(file, "%f", &walkSpeed);
	fscanf_s(file, "%f", &dashSpeed);
	for (int i = 0; i < 3; i++)
	{
		fscanf_s(file, "%d", &attackPoint[i]);
	}
	fscanf_s(file, "%d", &max_hp);
	fscanf_s(file, "%d", &intarval);

	for (int i = 0; i < 5; i++)
	{

		fscanf_s(file, "%d", &chargeTime[i]);
	}

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			fscanf_s(file, "%d", &rate[i][j]);
		}
	}

	fclose(file);
}
