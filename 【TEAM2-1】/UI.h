#pragma once
#include "common.h"
#include "Weapon.h"
struct ui_info
{
	Vector2 pos;
	int num;
	Vector2 tex_size;
	Vector2 scale;
};


class UI
{
public:
	enum UI_ID
	{
		NONE = 0,
		PL_HP,
		EQUIP_X,
		EQUIP_Y,
		UI_BAG,
		UI_ARROW,
		MONEY,
		END,
	};

	ui_info info[UI_ID::END];

public:

	bool bag_tutorial;
	int bag_tutorial_count;
	int interact;
	int tmp_MAX_MONEY;
	int tmp_money;
	int tmp_MAX_PL_HP;
	int tmp_PL_HP;
	int dps;
	WEAPON ui_equipX;
	WEAPON ui_equipY;

	WEAPON apeal_weapon;
	Vector2 apeal_pos;
	int apeal_cnt;
	bool isGain;

public:
	UI::UI();
	UI::~UI();
	void Init();
	void Update();
	void Draw();
	void ImGui();

	//void DrawFloating
	void Interactive();
	void ui_money_Update();
	void pl_ui_Draw();
	void enm_ui_Draw();
};

extern UI ui;
