#include "Enemy.h"
#include "Mo2_BG.h"
#include "ObjectManager.h"
#include "Camera.h"
#include "Texture.h"
#include "Sound.h"
#include "EffectManager.h"

int E_N_Slime::personal_counter = 0;
int E_N_Slime::attackPoint = 0;
int E_N_Slime::max_hp = 10;
int E_N_Slime::intarval = 100;
float E_N_Slime::range[eRange::end] = { 0 };
float E_N_Slime::walkSpeed = 0;
Vector2 E_N_Slime::jumpPower = { 0,0 };
int E_N_Slime::maxChargeTime = 0;
void E_N_Slime::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/nSlime_info.txt");

	fopen_s(&file, fileName, "w");

	for (int i = 0; i < 3; i++)
	{

		fprintf_s(file, "%f ", range[i]);
	}

	fprintf_s(file, "%f\n", walkSpeed);
	fprintf_s(file, "%d\n", attackPoint);
	fprintf_s(file, "%d\n", max_hp);
	fprintf_s(file, "%f\n", jumpPower.x);
	fprintf_s(file, "%f\n", jumpPower.y);
	fprintf_s(file, "%d\n", intarval);
	fprintf_s(file, "%d\n", maxChargeTime);
	fclose(file);


}


void E_N_Slime::loadData()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/nSlime_info.txt");

	fopen_s(&file, fileName, "r");

	for (int i = 0; i < 3; i++)
	{

		fscanf_s(file, "%f", &range[i]);
	}



	fscanf_s(file, "%f", &walkSpeed);
	fscanf_s(file, "%d", &attackPoint);
	fscanf_s(file, "%d", &max_hp);
	fscanf_s(file, "%f", &jumpPower.x);
	fscanf_s(file, "%f", &jumpPower.y);
	fscanf_s(file, "%d", &intarval);
	fscanf_s(file, "%d", &maxChargeTime);


	fclose(file);

	//TODO::HP倍率
	if (pBG->stageNumber == 3)
		max_hp *= 4;

}

E_N_Slime::E_N_Slime()
{
	id.CLASS = E_N_SLIME;
	id.PERSONAL = personal_counter;
	personal_counter++;
	
}

E_N_Slime::~E_N_Slime()
{
	personal_counter--;
	EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("E_N_Slime_dead.bin"), false, pos);
	if (pObjManager->ObjList.empty() == false)
	{
		Vector2 * player_pos_ptr = &pObjManager->ObjList[Object::PLAYER].back()->pos;
		for (int i_jew = 0; i_jew < money/3/*(各敵のジュエリー発生量)*/; i_jew++)
		{
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("jewerly.bin"), false, pos)->Update();
		}
	}
}

void E_N_Slime::Init()
{
	nowState = 0;
	isDamage = false;
	noDamageTimer = 0;
	plAttackNum = 0;
	chargeTime = 0;
	speed = { 0,0 };
	MAX_SPEED.x = 10.f;
	MAX_SPEED.y = 15.f;
	INITIAL_SPEED.x = 2.f;
	INITIAL_SPEED.y = 0.f;
	hp = max_hp;
	maxHP = max_hp;
	temp_hp = hp;
	act_interval = 0;
	notFound = 500;
	checkSpan = 10;
	walkCount = 0;
	attackState = 0;
	isGrip = false;
	isErase = false;
	isAnim = true;
	isStay = false;
	isAttack = false;
	onGround = false;
	size = { 30,30 };
	loadAnimFile("E_Slime");
}

bool E_N_Slime::Update()
{
	if (Iwin_flg.objBG || Iwin_flg.enemy) return true;

	if (cam.IsInCameraFoot(pos, Vector2((float)tex_size.x * 0.5f, (float)tex_size.y)) == false)
	{
		hide();
		return true;
	}
	else
		show();

	if (noDamageTimer)
	{
		noDamageTimer--;
	}
	else
	{

		plAttackNum = 0;
	}

	searchAlg(pObjManager->distanceToPlayer(*this));

	if (nowState == -1)
	{
		m_isVisible = false;
		return true;
	}
	else
		m_isVisible = true;

	if (temp_hp != hp)
		temp_hp -= 2.0f;

	if(isAttack && !onGround)
		EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("E_N_Slime_atk.bin"), false, pos, isFlipX);

	Gravity();

	PosUpdateY();

	checkAreaY();

	Behavior(pObjManager->distanceToPlayer(*this));

	PosUpdateX();

	checkAreaX();

	animUpdate();

	return 1;
}

void E_N_Slime::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

#if ENABLE_SMALL_DRAW // 縮小表示への対応
	DrawRectRotaGraphFast2
	(
		cam.xi_ext(pos.x - tex_size.x * 0.5f),
		cam.yi_ext(pos.y - tex_size.y),
		scasi(tex_pos.x) + tex_size.x * aFrame,
		scasi(tex_pos.y) + tex_size.y * now_anim,
		tex_size.x,
		tex_size.y,
		0, 0,
		cam.chipextrate_d(),
		0.0,
		pTexture->getGraphs(Texture::ENEMY),
		TRUE, isFlipX
	);

		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);

	//HP関連 (UIに移行済み)
	/*	if (hp != max_hp)
		{
			DrawRectExtendGraphF(
				cam.xi_ext(pos.x - 9),
				cam.yi_ext(pos.y - size.y - 10),
				cam.xi_ext(pos.x + 9),
				cam.yi_ext(pos.y - size.y - 4),
				0, 120, 18, 6,
				pTexture->getGraphs(pTexture->UI), TRUE
			);
			DrawRectExtendGraphF(
				cam.xi_ext(pos.x - 9),
				cam.yi_ext(pos.y - size.y - 10),
				cam.xi_ext(pos.x - 9 + (18.0f / max_hp)*temp_hp),
				cam.yi_ext(pos.y - size.y - 4),
				0, 114, 18, 6,
				pTexture->getGraphs(pTexture->UI), TRUE
			);

			DrawRectExtendGraphF(
				cam.xi_ext(pos.x - 9),
				cam.yi_ext(pos.y - size.y - 10),
				cam.xi_ext(pos.x - 9 + (18.0f / max_hp)*hp),
				cam.yi_ext(pos.y - size.y - 4),
				0, 108, 18, 6,
				pTexture->getGraphs(pTexture->UI), TRUE
			);
	}*/

#else
	DrawRectGraph(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
		tex_pos.x + tex_size.x * aFrame,
		tex_pos.y + tex_size.y * now_anim,
		tex_size.x, tex_size.y, pTexture->getGraphs(Texture::ENEMY), TRUE, isFlipX);
#endif
	if (show_debug)
		DrawLineBox
		(
			cam.xi_ext(pos.x - size.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y - size.y)/* - camera_pos.y*/,
			cam.xi_ext(pos.x + size.x * 0.5f)/* - camera_pos.x*/,
			cam.yi_ext(pos.y) /*- camera_pos.y*/,
			GetColor(255, 5, 80));

}

void E_N_Slime::walkAlg(const Object* _pl)
{

	if (!onGround)return;

	if (isStay)
		changeAnimation(nSlimeAnim::stay);
	else
		changeAnimation(nSlimeAnim::walk);

	if (_pl == nullptr)
	{
		walkCount = 0;
	}

	else
	{
		walkCount++;

		isFlipX = moveDirection(_pl->pos);
	}

	if (isFlipX)
	{
		speed.x = -walkSpeed;
	}
	else
	{
		speed.x = walkSpeed;
	}

}

void E_N_Slime::searchAlg(float _dist)
{
	static Vector2 plPos = { 0,0 };
	plPos = pObjManager->ObjList[PLAYER].back()->pos;

	if (isAttack)return;

	if (_dist == -1)
	{
		nowState = -1;
		m_isVisible = false;
		return;
	}

	if (stayTimer)
	{
		nowState = 0;
		return;
	}

	if (isFlipX && pos.x + size.x * 2 > plPos.x)
		isFind = true;
	else if (!isFlipX && pos.x - size.x * 2 < plPos.x)
		isFind = true;
	else
		isFind = false;

	if ((pos.y >= plPos.y && pos.y - size.y <= plPos.y) && isFind)
	{
		if (_dist <= range[eRange::attack])
			nowState = 3;
		else if (_dist <= range[eRange::track])
			nowState = 2;
	}
	else if (_dist <= range[eRange::move])
		nowState = 1;
	else
		nowState = -1;
}

void E_N_Slime::Gravity()
{
	onGround = false;

	speed.y += GRAVITY;
	if (speed.y >= MAX_SPEED.y)
	{
		speed.y = MAX_SPEED.y;
	}
}

void E_N_Slime::checkAreaX()
{
	if (delta.x < 0)
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			mapHoseiRight(this);
			if (!isAttack)
			{
				if (!walkCount)
				{
					isStay = false;
					isFlipX = true;
				}
				else isStay = true;
			}
			else
				speed.x = 0;
		}
	}

	if (delta.x > 0)
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			mapHoseiLeft(this);
			if (!isAttack)
			{
				if (!walkCount)
				{
					isStay = false;
					isFlipX = false;
				}
				else isStay = true;
			}
			else
				speed.x = 0;
		}

	}



}

void E_N_Slime::checkAreaY()
{
	if (delta.y > 0)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			mapHoseiUp(this);
		}
	}
	if (delta.y < 0)
	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}

		if (isUpperFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
		}

		if (onGround)
		{
			if (isFlipX && speed.x < 0)
			{
				if ((isFloor(pos.x, pos.y + 1, size.x*0.5f) || (isUpperFloor(pos.x, pos.y + 1, size.x*0.5f))) && isNone(pos.x - 1, pos.y + 1, size.x*0.5f))
				{
					isFlipX = false;
				}

			}
			else if (!isFlipX && speed.x > 0)
			{
				if ((isFloor(pos.x, pos.y + 1, size.x*0.5f) || (isUpperFloor(pos.x, pos.y + 1, size.x*0.5f))) && isNone(pos.x + 1, pos.y + 1, size.x*0.5f))
				{
					isFlipX = true;
				}

			}

		}
	
	}

}

void E_N_Slime::animUpdate()
{
	playAnimation();
}

Vector2 E_N_Slime::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		if (now_anim == nSlimeAnim::jump)
		{
			if (!isFlipX)
				return Vector2(size.x / 2, 0);
			else
				return Vector2(-size.x / 2, 0);

		}
		else
			return Vector2(0, 0);
		break;

	case 1:
		if (now_anim == nSlimeAnim::jump)
		{
			return Vector2(size.x / 2, size.y);
		}
		else
			return Vector2(-1, -1);

		break;

	case 2://xは攻撃力を返す(仮)
		return Vector2(attackPoint, plAttackNum);

	}
}


void E_N_Slime::Behavior(float _dist)
{
	if (isDamage && isAttack)
		isDamage = false;
	if (noHitTime)
	{
		noHitTime--;
		isDamage = false;
	}

	if (isDamage)
	{
		knockBackEnm();
		changeAnimation(nSlimeAnim::damage);
		isAttack = false;
		attackState = 0;
		return;
	}
	if (!onGround)
	{
		return;
	}

	if (!onGround && now_anim == nSlimeAnim::stay)
	{
		return;
	}

	switch (nowState)
	{
	case -1:
		break;
	case 0:
		speed = { 0,0 };
		changeAnimation(nSlimeAnim::stay);
		if (stayTimer > 0)
		{
			stayTimer--;
		}
		break;

	case 1:
		walkAlg(nullptr);
		break;

	case 2:
		walkAlg(pObjManager->ObjList[Object::PLAYER].begin()->get());
		break;

	case 3:
		Attack(_dist);
		break;

	}
}

void E_N_Slime::Attack(const float _dist)
{
	switch (attackState)
	{
	case 0:
		speed = { 0,0 };
		isAttack = true;
		attackState++;
		changeAnimation(nSlimeAnim::attack);
		chargeTime = 60;
		break;
	case 1:
		isFlipX = moveDirection(pObjManager->ObjList[Object::PLAYER].back()->pos);
		if (aFrame == 3)
		{
			if (chargeTime)

				chargeTime--;

			else
			{
				attackState++;
				pSound->playSE(Sound::SE_E_Slim, false);
			}
		}

		break;
	case 2:
	
		speed.y = -jumpPower.y;
		changeAnimation(nSlimeAnim::jump);
		attackState++;
	

	case 3:
		if (isFlipX)
		{
			speed.x = -jumpPower.x;
		}

		else
		{
			speed.x = jumpPower.x;
		}

		EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("E_N_Slime_atk.bin"), false, pos, isFlipX);

		if (onGround)
			attackState++;
	break;
	case 4:
		changeAnimation(nSlimeAnim::landing);
		speed.x = 0;
		if (aFrame == 5)
			attackState++;
		break;
	case 5:
		stayTimer = intarval;
		attackState = 0;
		isAttack = false;
		break;
	}

}

void E_N_Slime::eImGui()
{
	if (Iwin_flg.e_info[Enemy_ID::E_N_SLIME] == false)
		return;

	static const char* rKind[3] =
	{ "move","track","attack" };

	ImGui::Begin("nSlime", &Iwin_flg.e_info[Enemy_ID::E_N_SLIME], ImGuiWindowFlags_MenuBar);


	for (int i = 0; i < 3; i++)
	{
		ImGui::DragFloat(rKind[i], &range[i]);
	}
	ImGui::DragFloat("walkSpeed", &walkSpeed);
	ImGui::Text("jumpPower");
	ImGui::DragFloat("x", &jumpPower.x);
	ImGui::DragFloat("y", &jumpPower.y);
	ImGui::DragInt("AttackPoint", &attackPoint);
	ImGui::DragInt("maxHP", &max_hp);
	ImGui::DragInt("intarval", &intarval);
	ImGui::DragInt("chargeTime", &maxChargeTime);

	ImGui::NewLine();

	if (ImGui::Button("save"))
	{
		saveFile();
	}
	ImGui::End();


}
