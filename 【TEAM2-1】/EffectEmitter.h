#pragma once
#include "Effect.h" 

#include "Singleton.h"
#include "EffectDataManager.h"

#include <memory>
#include <vector>
#include <deque>
#include <unordered_map>
#include <fstream>
#include <iostream>

#include <cereal\cereal.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\deque.hpp>
#include <cereal\types\string.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\types\memory.hpp>

struct EmitterParams
{
	EmitterParams() {}
	//エミッター
#pragma region variables
	DirectX::SimpleMath::Vector2 pos						= { 0.f,0.f };
	bool						 is_follow_to_root			= true;

	DirectX::SimpleMath::Vector2 vel						= { 0.f,0.f };
	float						 deceleration_rate_mul		= 1.f;
	float						 gravity					= 0.f;

	float						 set_speed_per_frame		= -1.f;
	float						 duration					= 10.f;
	float						 duration_max				= 10.f;
	float						 duration_subtraction_rate	= 1.0f;

	const float*				 attract_x_ptr				= nullptr;
	const float*				 attract_y_ptr				= nullptr;

	int							 emit_num					= 0;		// 1度にエミットするエフェクトの数
	int							 emit_num_randomize			= 0;		// 1度にエミットするエフェクトのランダム化

	float						 radius						= 1.f;		// 拡大率にも代替して使用する
	float						 radius_decreae_interval	= 1.f;		// radiusが縮小し始めるまでのしきい値

	int							 type						= EFC_DEFAULT;

	//テクスチャ
	std::string					 graph_filename				= "";

	//エフェクト
	DirectX::SimpleMath::Vector4 efc_pos_randomize			= { -0.f,-0.f,+0.f,+0.f };	// { 最小値xy(x,y), 最大値xy(z,w) }
	DirectX::SimpleMath::Vector4 efc_vel_randomize			= { -0.f,-0.f,+0.f,+0.f };	// { 最小値xy(x,y), 最大値xy(z,w) }
	DirectX::SimpleMath::Vector4 efc_color_randomize		= {  0.f, 0.f, 0.f, 0.f };	// +rgba
	float						 efc_angle_randomize		= 0.f;						// +-0.f~720目安 
	float						 efc_radius_randomize		= 0.f;						// +
	float						 efc_duration_randomize		= 0.f;
	float						 efc_deceleration_rate_mul_randomize = 0.f;				// 0.f~1.f 加算
	float						 efc_noise_scale_randomize	= 1.0f;						// 1~10目安 
	float						 efc_curl_speed_randomize	= 0.f;						// 0.f~0.3f目安 加算
	EffectParams				 efc;
#pragma endregion

	EffectParams InheritFromEmitterParams(const DirectX::SimpleMath::Vector2& _add_pos = { 0.f,0.f }, const bool& _is_flip_x = false) const
	{
		DirectX::SimpleMath::Vector2	randoms			= { 0.f,0.f };
		EffectParams					inherit_params	= efc;

		// 位置
		randoms									= GetRandSignedVec2(efc_pos_randomize);
		inherit_params.pos						= pos + randoms;
		inherit_params.pos.x					*= _is_flip_x ? -1.f : 1.f;
		inherit_params.pos						+= _add_pos;

		// 速度
		randoms									= GetRandSignedVec2(efc_vel_randomize);
		inherit_params.vel						= vel + randoms;
		inherit_params.vel.x					*= _is_flip_x ? -1.f : 1.f;

		// 減速乗数
		randoms.x								= GetRand(efc_deceleration_rate_mul_randomize);
		inherit_params.deceleration_rate_mul	= deceleration_rate_mul + randoms.x;

		// 寿命
		randoms.x								= GetRand(efc_duration_randomize);
		inherit_params.radius_decreae_interval	+= randoms.x;
		inherit_params.blend_decreae_interval	+= randoms.x;
		inherit_params.duration_max				+= randoms.x;
		inherit_params.duration					= inherit_params.duration_max;

		// 追跡位置
		inherit_params.attract_x_ptr			= attract_x_ptr;
		inherit_params.attract_y_ptr			= attract_y_ptr;

		// 重力
		inherit_params.gravity					= efc.gravity;

		// type
		inherit_params.type						= type;

		// 半径
		randoms.x								= GetRand(efc_radius_randomize);
		inherit_params.radius					= radius + randoms.x;
		//inherit_params.radius_decreae_interval	= radius_decreae_interval;
		inherit_params.radius_fade_interval		+= inherit_params.radius_fade_interval > 0.f ? 1.f : 0.f * randoms.x;

		// 角度
		inherit_params.angle.val				+= GetRandSigned(-efc_angle_randomize,efc_angle_randomize);
		inherit_params.angle.target_val			+= GetRandSigned(-efc_angle_randomize,efc_angle_randomize);

		// curl speed
		inherit_params.noise_scale				+= (float)GetRand((int)efc_noise_scale_randomize);
		inherit_params.curl_speed				+= GetRand(efc_curl_speed_randomize);

		// col
		randoms.x								= GetRandSigned(efc_color_randomize.x);
		inherit_params.col.col.x				+= randoms.x;
		inherit_params.col.target_col.x			+= randoms.x;
		randoms.x								= GetRandSigned(efc_color_randomize.y);
		inherit_params.col.col.y				+= randoms.x;
		inherit_params.col.target_col.y			+= randoms.x;
		randoms.x								= GetRandSigned(efc_color_randomize.z);
		inherit_params.col.col.z				+= randoms.x;
		inherit_params.col.target_col.z			+= randoms.x;
		randoms.x								= GetRandSigned(efc_color_randomize.w);
		inherit_params.col.col.w				+= randoms.x;
		inherit_params.col.target_col.w			+= randoms.x;

		return inherit_params;
	}

	EffectParams InheritFromEffectParams(const DirectX::SimpleMath::Vector2& _add_pos = { 0.f,0.f }) const
	{
		Vector2			randoms = { 0.f,0.f };
		EffectParams	inherit_params = efc;

		// 位置
		randoms							= GetRandSignedVec2(efc_pos_randomize);
		inherit_params.pos				= efc.pos + pos + randoms + _add_pos;

		// 速度
		randoms							= GetRandSignedVec2(efc_vel_randomize);
		inherit_params.vel				= efc.vel + randoms;

		// 減速乗数
		randoms.x						= GetRand(efc_deceleration_rate_mul_randomize);
		inherit_params.deceleration_rate_mul = efc.deceleration_rate_mul + randoms.x;

		// 寿命
		randoms.x						= GetRand(efc_duration_randomize);
		inherit_params.duration_max		= efc.duration_max + randoms.x;
		inherit_params.duration			= inherit_params.duration_max;

		// 追跡位置
		inherit_params.attract_x_ptr	= efc.attract_x_ptr;
		inherit_params.attract_y_ptr	= efc.attract_y_ptr;

		// 重力
		inherit_params.gravity			= efc.gravity;

		// culr_speed
		inherit_params.curl_speed		= GetRand(efc_curl_speed_randomize);

		// type
		inherit_params.type				= efc.type;

		if (!graph_filename.empty())
			inherit_params.draw.graph_hadle = EFCDATA->GetEfcRef(graph_filename);

		return inherit_params;
	}

	template <class Archive>
	void serialize(Archive& _ar, std::uint32_t const version)
	{
		_ar
		(
			CEREAL_NVP(pos.x), CEREAL_NVP(pos.y),
			CEREAL_NVP(is_follow_to_root),
			CEREAL_NVP(vel.x), CEREAL_NVP(vel.y),
			CEREAL_NVP(deceleration_rate_mul),
			CEREAL_NVP(gravity),

			CEREAL_NVP(set_speed_per_frame),
			CEREAL_NVP(duration),
			CEREAL_NVP(duration_max),
			CEREAL_NVP(duration_subtraction_rate),

			CEREAL_NVP(emit_num),
			CEREAL_NVP(emit_num_randomize),

			CEREAL_NVP(radius),
			CEREAL_NVP(radius_decreae_interval),

			CEREAL_NVP(type),

			CEREAL_NVP(graph_filename),

			CEREAL_NVP(efc_pos_randomize.x),
			CEREAL_NVP(efc_pos_randomize.y),
			CEREAL_NVP(efc_pos_randomize.z),
			CEREAL_NVP(efc_pos_randomize.w),
			CEREAL_NVP(efc_vel_randomize.x),
			CEREAL_NVP(efc_vel_randomize.y),
			CEREAL_NVP(efc_vel_randomize.z),
			CEREAL_NVP(efc_vel_randomize.w),
			CEREAL_NVP(efc_angle_randomize),
			CEREAL_NVP(efc_radius_randomize),
			CEREAL_NVP(efc_duration_randomize),
			CEREAL_NVP(efc_deceleration_rate_mul_randomize),
			CEREAL_NVP(efc_noise_scale_randomize),
			CEREAL_NVP(efc_curl_speed_randomize),

			CEREAL_NVP(efc)
		);

		if (version >= 1)
		{
			// archive(...);
		}
	}
};
CEREAL_CLASS_VERSION(EmitterParams, 1);

struct EMIT_DATA
{
	EmitterParams			  emit_data;
	std::vector<EffectParams> efc_data;

	template <class Archive>
	void serialize(Archive& _ar, std::uint32_t const version)
	{
		_ar
		(
			CEREAL_NVP(emit_data),
			CEREAL_NVP(efc_data)
		);
	}
};
CEREAL_CLASS_VERSION(EMIT_DATA, 1);

struct EMIT_DATASET
{
	std::vector<EMIT_DATA> dataset;

	template <class Archive>
	void serialize(Archive& _ar, std::uint32_t const version)
	{
		_ar(CEREAL_NVP(dataset));
	}
};
CEREAL_CLASS_VERSION(EMIT_DATASET, 1);

struct ANIM_EMIT_DATA
{
	EmitterParams anim_emit_data;

	std::deque												//アニメーションの種類
		<
			bool											// 対応するキーフレームでエフェクトのエミットを1回限りにするキーフレーム
		>	is_emit_once;									

	std::vector												//アニメーションの種類
		<
		std::unordered_map< int, std::vector<EffectParams> >//std::unordered_map< キーフレーム, emitするデータのvector >
		>	anim_efc_data;

	template <class Archive>
	void serialize(Archive& _ar, std::uint32_t const version)
	{
		_ar
		(
			CEREAL_NVP(anim_emit_data),
			CEREAL_NVP(anim_efc_data)
		);

		if (version >= 2)
		{
			_ar(is_emit_once);
		}
	}
};
CEREAL_CLASS_VERSION(ANIM_EMIT_DATA, 2);


struct ANIM_EMIT_DATASET
{
	std::vector<ANIM_EMIT_DATA> dataset;

	template <class Archive>
	void serialize(Archive& _ar, std::uint32_t const version) // 実装もヘッダで書かないと外部でこの(これらの)構造体を使用した際にリンクエラーが発生する
	{
		_ar(CEREAL_NVP(dataset));
	}
};
// クラスに変数を追加する場合前のcerealのデータと互換性が無くなるので、取り敢えず CEREAL_CLASS_VERSIONでバージョン登録したほうが良いかもしれない
// https://uscilab.github.io/cereal/assets/doxygen/group__Utility.html#gabea833652252591a51f75cb9ce31f44a
CEREAL_CLASS_VERSION(ANIM_EMIT_DATASET, 1);

class EffectRoot;

//エフェクト生成地点
class EffectEmitter
{
public:
	EffectEmitter
	(
		EFC_TYPE _type,
		DirectX::SimpleMath::Vector2 _pos,
		DirectX::SimpleMath::Vector2 _vel,
		DirectX::SimpleMath::Vector2 _efc_vel,
		float _duration,
		float _set_speed_per_frame,
		float _deceleration_rate_mul = 0.f,
		float _gravity = 0.f,
		bool _is_ui_efc = false,
		float _size = 1.f,
		const float* _attract_x_ptr = nullptr,
		const float* _attract_y_ptr = nullptr
	);
	EffectEmitter(const EmitterParams& _emit_params);
	EffectEmitter(const EmitterParams& _emit_params, EffectRoot* _parent_root = nullptr, const bool& _is_flip_x = false);
	EffectEmitter() { emit_params = EmitterParams(); }
	virtual ~EffectEmitter();

	void ClearEffects();

	virtual bool Update();
	virtual void Draw();

	virtual void SetAttractPos(const float* _attract_x = nullptr, const  float* _attract_y = nullptr);
	void SetPos(const DirectX::SimpleMath::Vector2& _root_pos) { emit_params.pos = _root_pos; }


	EFC_TYPE GetType() { return (EFC_TYPE)emit_params.type; }
	const EmitterParams& GetEmitterParamsRef()const { return emit_params; }
	virtual void EmitEffect(const EffectParams& _params);
	virtual void EmitEffect();
	virtual void EmitEffects(const EmitterParams& _params);

public:
	bool is_ui_efc = false;
protected:

	EmitterParams								emit_params;
	std::vector<std::unique_ptr<BaseEffect>>	efcs;
	EffectRoot*									parent_root;

};

class AnimationEffectEmitter :public EffectEmitter
{
public:
#pragma region constructors
	//AnimationEffectEmitter(const EmitterParams& _emit_params, const int& _key_num = 0)	:EffectEmitter(_emit_params), passed_key_num(_key_num), is_emitted(false) {}
	AnimationEffectEmitter()																	:EffectEmitter()			, passed_key_num(0), is_emitted(false) {}
	AnimationEffectEmitter(const ANIM_EMIT_DATA& _emit_data, EffectRoot* _parent_root = nullptr):EffectEmitter()			, passed_key_num(0), is_emitted(false)
	{
		emit_params		= _emit_data.anim_emit_data;
		key_frame		= _emit_data.anim_efc_data;
		is_emit_once	= _emit_data.is_emit_once;
		parent_root		= _parent_root;
	}
	AnimationEffectEmitter(const ANIM_EMIT_DATA& _emit_data, const Animation& _animation, EffectRoot* _parent_root = nullptr) :EffectEmitter(), passed_key_num(0), is_emitted(false)
	{
		emit_params		= _emit_data.anim_emit_data;
		key_frame		= _emit_data.anim_efc_data;
		is_emit_once	= _emit_data.is_emit_once;
		parent_root		= _parent_root;
	}
	AnimationEffectEmitter(const ANIM_EMIT_DATA& _emit_data, std::shared_ptr<Animation>& _animation, EffectRoot* _parent_root = nullptr)
		:EffectEmitter(), passed_key_num(0), is_emitted(false)
	{
		emit_params		= _emit_data.anim_emit_data;
		key_frame		= _emit_data.anim_efc_data;
		is_emit_once	= _emit_data.is_emit_once;
		animation		= _animation;
		parent_root		= _parent_root; 
	}
#pragma endregion

	~AnimationEffectEmitter()
	{
		//for (auto& it : key_frame)
		//{
		//	for (auto& it_anim : it)
		//	{
		//		for (auto& it_param : it_anim.second)
		//		{
		//			it_param.attract_x_ptr = nullptr;
		//			it_param.attract_y_ptr = nullptr;
		//		}
		//	}
		//}
	}

	virtual bool Update()override;
	virtual void Draw()override;

	virtual void SetAttractPos(const float* _attract_x = nullptr, const  float* _attract_y = nullptr)override;

	virtual void EmitEffect(const EffectParams& _params)override;
	virtual void EmitEffect()override;

	EmitterParams* GetEffectEmitter() { return &emit_params; }

public:
	std::deque													//アニメーションの種類
	<
		bool													// 対応するキーフレームでエフェクトのエミットを1回限りにするキーフレーム
	>	is_emit_once;

	std::vector													//アニメーションの種類
	<
		std::unordered_map< int, std::vector<EffectParams> >	//std::unordered_map< キーフレーム, emitするデータのvector >
	>	key_frame;

	template<class Archive>
	void serialize(Archive & _ar)
	{

	}

private:
	int passed_key_num;//通過したキーの数
	bool is_emitted;
	std::shared_ptr<Animation> animation;
};