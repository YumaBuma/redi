#include "Mo2_BG.h"
#include "Input.h"
#include "Camera.h"
#include "EnemyManager.h"
#include "Enemy.h"
#include "ObjectManager.h"
#include "src\imgui.h"


Vector2 EnemyManager::pos[2] = { { 0,0 },{ 0,0 } };
int EnemyManager::gritSize[2] = { 32,32 };

void EnemyManager::setEnemy(int _id, float _posX, float _posY, bool _isflipX)
{
	switch (_id)
	{
	case E_N_SLIME:
		pObjManager->Add(std::make_shared<E_N_Slime>(), Object::ENEMY, 8, _id, 0);
		break;
	case E_N_SKELETON:
		pObjManager->Add(std::make_shared<E_N_Skeleton>(), Object::ENEMY, 8, _id, 0);
		break;
	case E_B_SKELETON:
		pObjManager->Add(std::make_shared<E_B_Skeleton>(), Object::ENEMY, 8, _id, 0);
		break;
	case E_BOSS:
		pObjManager->Add(std::make_shared<E_Boss>(), Object::ENEMY, 8, _id, 0);

		break;
	case 5:
		break;
	case 6:
		break;
	default:
		break;
	}
	pObjManager->ObjList[Object::ENEMY].back()->pos = { _posX, _posY };
	pObjManager->ObjList[Object::ENEMY].back()->isFlipX = _isflipX;
	pObjManager->ObjList[Object::ENEMY].back()->setknockBack(knockBackPower);
	//TODO::お金個数
	pObjManager->ObjList[Object::ENEMY].back()->money = 30 + (GetRand(10) - 5);

}


void EnemyManager::Debug()
{
	//DrawFormatString(200, 200, GetColor(255, 255, 255), "%d 体", pObjManager->ObjList[Object::ENEMY].size());
	setString({ 200, 200 }, L"%d 体", pObjManager->ObjList[Object::ENEMY].size());
}

void EnemyManager::init()
{
	for (int i = 0; i < 4; i++)
	{
		selected[i] = false;
	}
	pos[now] = { 0,0 };
	pos[previous] = { 0,0 };
	layer = 0;
	useGrid = false;
	gritSize[x] = 32;
	gritSize[y] = 32;
	comparision = false;
	useMouse = false;
	gripObj = false;
	mouseButtons[2] = { 0 };
	listbox_group_current[2] = { 0 };
	knockBackPower = { 0,0 };
}

void EnemyManager::update()
{
	mouseButtons[now] = GetMouseInput();
	cam.SwitchIsUsePtr(false);

	cam.FollowMouse();

	if (!useMouse)
		praPos = { cam.xf(pos[now].x),cam.yf(pos[now].y) };

	mousePos[prevX] = mousePos[x];
	mousePos[prevY] = mousePos[y];

	if (!selected[put] && !selected[move] && !selected[remove] && !selected[copy])
	{
		if (CopyList.empty() == false)
		{
			for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
			{
				p->get()->isErase = true;
			}

			CopyList.erase(CopyList.begin(), CopyList.end());

			for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
			{
				if (p->second->isErase)
					p = pDrawList->drawables.erase(p);
				else
					p++;
			}
		}
	}

	//カーソル移動
	{

		if (key[KEY_INPUT_W] == 1 || key[KEY_INPUT_UP] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				if (pos[now].y >= 32)
				{
					pos[now].y -= 32;
					if ((int)pos[now].y % 32 != 0)
						pos[now].y = ((int)pos[now].y / 32 + 1) * 32;
				}
			}
			else if (pos[now].y > 0)
			{
				pos[now].y--;
			}
		}
		else if (key[KEY_INPUT_S] == 1 || key[KEY_INPUT_DOWN] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				pos[now].y += 32;
				pos[now].y = (int)pos[now].y / 32 * 32;
			}
			else
				pos[now].y++;
		}
		else if (key[KEY_INPUT_D] == 1 || key[KEY_INPUT_RIGHT] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				pos[now].x += 32;
				pos[now].x = ((int)pos[now].x) / 32 * 32;
			}
			else
				pos[now].x++;
		}
		else if (key[KEY_INPUT_A] == 1 || key[KEY_INPUT_LEFT] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				if (pos[now].x >= 32)
				{
					pos[now].x -= 32;
					if ((int)pos[now].x % 32 != 0)
						pos[now].x = ((int)pos[now].x / 32 + 1) * 32;
				}
			}
			else if (pos[now].x > 0)
			{
				pos[now].x--;
			}
		}

		if (useMouse)
		{
			mousePos[x] = (int)cam.GetWorldPosFromCursorPos().x;
			mousePos[y] = (int)cam.GetWorldPosFromCursorPos().y;

			if (useGrid)
			{
				float	ext = cam.chipextrate_target();
				//int		gridSizeTmpX	= gritSize[x] / (int)(1.f / ext);
				//int		gridSizeTmpY	= gritSize[y] / (int)(1.f / ext);

				mousePos[x] = (mousePos[x] / 32) * 32;
				mousePos[y] = (mousePos[y] / 32) * 32;
			}

			pos[now] = { (float)mousePos[x],(float)mousePos[y] };
			praPos = { cam.xf_ext(pos[now].x),cam.yf_ext(pos[now].y) };
		}

	}

	if (!selected[put] && !selected[move] && !selected[remove] && !selected[copy])return;

	//設置
	{

		if (selected[put])
		{
			if (CopyList.empty() == false)
			{
				CopyList.back()->pos = { pos[now].x ,pos[now].y + gritSize[y] / 2 };
			}

			if (listbox_group_current[now] != listbox_group_current[previous])
			{
				if (CopyList.empty() == false)
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						p->get()->isErase = true;
					}

					CopyList.erase(CopyList.begin(), CopyList.end());

					for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
					{
						if (p->second->isErase)
							p = pDrawList->drawables.erase(p);
						else
							p++;
					}

				}

				switch (listbox_group_current[now])
				{
				case E_N_SLIME:
					CopyList.emplace_back(std::make_shared<E_N_Slime>());
					break;
				case E_N_SKELETON:
					CopyList.emplace_back(std::make_shared<E_N_Skeleton>());
					break;
				case E_B_SKELETON:
					CopyList.emplace_back(std::make_shared<E_B_Skeleton>());
					break;
				case E_BOSS:
					CopyList.emplace_back(std::make_shared<E_Boss>());
					break;
				case 5:
					break;
				case 6:
					break;
				default:
					break;
				}
				{
					CopyList.back()->id = { listbox_group_current[now],0 };
					CopyList.back()->layer = 4;
					CopyList.back()->Init();
					gritSize[x] = CopyList.back()->size.x;
					gritSize[y] = CopyList.back()->size.y;
					CopyList.back()->pos = { pos[now].x,pos[now].y + gritSize[y] / 2 };
					CopyList.back()->isGrip = true;
					CopyList.back()->isErase = false;
					pDrawList->add(CopyList.back(), 0);
				}
			}

			if (useMouse)
			{
				if ((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				{

					setEnemy(listbox_group_current[now], pos[now].x, pos[now].y + gritSize[y] / 2, false);
				}
			}
			else if (key[KEY_INPUT_RETURN] == 1)
			{
				setEnemy(listbox_group_current[now], pos[now].x, pos[now].y + gritSize[y] / 2, false);
			}
		}
	}

	//コピー
	{

		if (selected[copy])
		{
			if (gripObj)
			{
				if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
					|| key[KEY_INPUT_RETURN] == 1)
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						setEnemy(p->get()->id.CLASS, p->get()->pos.x, p->get()->pos.y, false);
					}
				}

				else
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						if (p->get()->isGrip)
							p->get()->pos += { pos[now].x - pos[previous].x, pos[now].y - pos[previous].y };
					}

				}

				if (key[KEY_INPUT_DELETE] == 1)
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						p->get()->isErase = true;
					}

					CopyList.erase(CopyList.begin(), CopyList.end());

					for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
					{
						if (p->second->isErase)
							p = pDrawList->drawables.erase(p);
						else
							p++;
					}

					gripObj = false;
				}
			}

			else if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				|| key[KEY_INPUT_RETURN] == 1)
			{
				if (pObjManager->ObjList[Object::ENEMY].empty() == false)
				{
					for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(), end = pObjManager->ObjList[Object::ENEMY].end(); p != end; p++)
					{
						if ((pos[now].x <= p->get()->pos.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x) && (pos[now].y <= p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y))
						{
							gripObj = true;
							//p->get()->isGrip = true;
							switch (p->get()->id.CLASS)
							{
							case E_N_SLIME:
								CopyList.emplace_back(std::make_shared<E_N_Slime>());
								break;
							case E_N_SKELETON:
								CopyList.emplace_back(std::make_shared<E_N_Skeleton>());
								break;
							case E_B_SKELETON:
								CopyList.emplace_back(std::make_shared<E_B_Skeleton>());
								break;
							case E_BOSS:
								CopyList.emplace_back(std::make_shared<E_Boss>());
								break;
							case 5:
								break;
							case 6:
								break;
							default:
								break;
							}
							CopyList.back()->id = { p->get()->id.CLASS,p->get()->id.PERSONAL };
							CopyList.back()->layer = p->get()->layer;
							CopyList.back()->Init();
							CopyList.back()->pos = p->get()->pos;
							CopyList.back()->isGrip = true;
							pDrawList->add(CopyList.back(), 0);

						}
					}

				}

			}


		}

	}

	//移動
	{

		if (selected[move])
		{
			if (gripObj)
			{
				if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
					|| key[KEY_INPUT_RETURN] == 1)
				{
					gripObj = false;
					for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
					{
						p->get()->isGrip = false;
					}
				}

				else
				{
					for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
					{
						if (p->get()->isGrip)
							p->get()->pos += { pos[now].x - pos[previous].x, pos[now].y - pos[previous].y };
					}

				}
			}
			else if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				|| key[KEY_INPUT_RETURN] == 1)

			{
				for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
				{
					if ((pos[now].x <= p->get()->pos.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x) && (pos[now].y <= p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y))
					{
						p->get()->isGrip = true;
						gripObj = true;
					}
				}

			}
		}

	}

	//削除
	{

		if (selected[remove])
		{
			if (pObjManager->ObjList[Object::ENEMY].empty() == false
				&& ((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				|| key[KEY_INPUT_RETURN] == 1
				)
			{
				for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
				{
					if ((pos[now].x <= p->get()->pos.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x) && (pos[now].y <= p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y))
						p->get()->isErase = true;
				}


				auto removeIt = std::remove_if(pObjManager->ObjList[Object::ENEMY].begin(), pObjManager->ObjList[Object::ENEMY].end(),
					[](std::shared_ptr<Object>& obj)
				{
					return obj->isErase;
				}
				);


				pObjManager->ObjList[Object::ENEMY].erase(removeIt, pObjManager->ObjList[Object::ENEMY].end());
				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}


		}
	}

	//情報保存
	{
		mouseButtons[previous] = mouseButtons[now];
		pos[previous] = pos[now];
		listbox_group_current[previous] = listbox_group_current[now];
	}
}


void EnemyManager::draw()
{
	DrawLineBox(praPos.x - gritSize[x] / 2, praPos.y - gritSize[y] / 2, praPos.x + gritSize[x] / 2, praPos.y + gritSize[y] / 2, GetColor(255, 0, 0));

}


void EnemyManager::imGui()
{
	static const char* ekind[Enemy_ID::E_END] =
	{
		"slime","nSkeleton","bSkeleton","boss",
	};

	if (!Iwin_flg.enemy)
	{
		if (CopyList.empty() == false)
		{
			for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
			{
				p->get()->isErase = true;
			}

			CopyList.erase(CopyList.begin(), CopyList.end());

			for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
			{
				if (p->second->isErase)
					p = pDrawList->drawables.erase(p);
				else
					p++;
			}
		}

		return;
	}
	const char* listbox_enemy[] = { "slime","knight","bowman","boss" };

	ImGui::Begin("enemy", &Iwin_flg.enemy, ImGuiWindowFlags_MenuBar);

	//メニューバー
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("Menu"))
			{
				if (ImGui::MenuItem("Save"))
				{
					saveFile();
				}
				if (ImGui::MenuItem("Exit"))
				{
					Iwin_flg.enemy ^= 1;
				}

				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("enemyInfo"))
			{
				for (int i = 0; i < Enemy_ID::E_END; i++)
				{
					if (ImGui::MenuItem(ekind[i]))
					{
						Iwin_flg.e_info[i] ^= 1;
					}
				}

				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
		}
	}

	ImGui::Checkbox("use Mouse", &useMouse);
	ImGui::Checkbox("use Grit", &useGrid);
	ImGui::Checkbox("equal rate", &comparision);


	ImGui::Selectable("put", &selected[put]);
	if (selected[put])
	{
		if (selected[copy] || selected[remove] || selected[move])
		{
			if (gripObj)
			{
				gripObj = false;
				for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
				{
					p->get()->isGrip = false;
				}
			}

			if (CopyList.empty() == false)
			{

				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}

			selected[move] = false;
			selected[copy] = false;
			selected[remove] = false;
		}

		if (CopyList.empty() == true)
		{
			switch (listbox_group_current[now])
			{
			case E_N_SLIME:
				CopyList.emplace_back(std::make_shared<E_N_Slime>());
				break;
			case E_N_SKELETON:
				CopyList.emplace_back(std::make_shared<E_N_Skeleton>());
				break;
			case E_B_SKELETON:
				CopyList.emplace_back(std::make_shared<E_B_Skeleton>());
				break;
			case E_BOSS:
				CopyList.emplace_back(std::make_shared<E_Boss>());
				break;
			case 5:
				break;
			case 6:
				break;
			default:
				break;
			}
			{
				CopyList.back()->id = { listbox_group_current[now],0 };
				CopyList.back()->layer = 4;;
				CopyList.back()->Init();
				gritSize[x] = CopyList.back()->size.x;
				gritSize[y] = CopyList.back()->size.y;
				CopyList.back()->pos = { pos[now].x,pos[now].y + gritSize[y] };
				CopyList.back()->isGrip = true;
				CopyList.back()->isErase = false;
				pDrawList->add(CopyList.back(), 0);
			}
		}




		ImGui::ListBox("select item", &listbox_group_current[now], listbox_enemy, IM_ARRAYSIZE(listbox_enemy), 4);
		ImGui::DragInt("layerNumber", &layer);


	}

	ImGui::Selectable("copy", &selected[copy]);
	if (selected[copy])
	{
		{
			ImGui::Text("gritSize");
			ImGui::DragInt2(" ", gritSize, 1, 32, 600);
			if (comparision)
				gritSize[y] = gritSize[x];
		}
		if (selected[remove] || selected[put] || selected[move])
		{
			if (gripObj)
			{
				gripObj = false;
				for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
				{
					p->get()->isGrip = false;
				}
			}

			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			selected[put] = false;
			selected[move] = false;
			selected[remove] = false;
		}
	}

	ImGui::Selectable("move", &selected[move]);
	if (selected[move])
	{
		{
			ImGui::Text("gritSize");
			ImGui::DragInt2(" ", gritSize, 1, 32, 600);
			if (comparision)
				gritSize[y] = gritSize[x];
		}
		if (selected[copy] || selected[put] || selected[remove])
		{
			if (gripObj)
			{
				gripObj = false;
				for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
				{
					p->get()->isGrip = false;
				}
			}

			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			selected[copy] = false;
			selected[put] = false;
			selected[remove] = false;
		}
	}

	ImGui::Selectable("remove", &selected[remove]);
	if (selected[remove])
	{

		{
			ImGui::Text("gritSize");
			ImGui::DragInt2(" ", gritSize, 1, 32, 600);
			if (comparision)
				gritSize[y] = gritSize[x];
		}
		if (selected[copy] || selected[put] || selected[move])
		{
			if (gripObj)
			{
				gripObj = false;
				for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
				{
					p->get()->isGrip = false;
				}
			}

			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			selected[copy] = false;
			selected[put] = false;
			selected[move] = false;
		}
	}
	ImGui::NewLine();

	//リセットボタン
	{

		if (ImGui::Button("reset(all reset)"))
		{
			for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
			{
				p->get()->isErase = true;
			}

			auto removeIt = std::remove_if(pObjManager->ObjList[Object::ENEMY].begin(), pObjManager->ObjList[Object::ENEMY].end(),
				[](std::shared_ptr<Object>& obj)
			{
				return obj->isErase == true;
			}
			);


			pObjManager->ObjList[Object::ENEMY].erase(removeIt, pObjManager->ObjList[Object::ENEMY].end());

			for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
			{
				if (p->second->isErase)
					p = pDrawList->drawables.erase(p);
				else
					p++;
			}

			for (int i = 0; i < 4; i++)
			{
				selected[i] = false;
			}
		}
	}

	//ノックバック周りの設定
	{
		ImGui::Text("knockBackPower");
		ImGui::DragFloat("x", &knockBackPower.x);
		ImGui::DragFloat("y", &knockBackPower.y);
	}

	ImGui::End();


	pObjManager->ImGui();

}

void EnemyManager::loadFile()
{


	E_N_Slime::loadData();
	E_N_Skeleton::loadData();
	E_B_Skeleton::loadData();
	E_Boss::loadData();


	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/knockBack.txt");

	fopen_s(&file, fileName, "r");

	fscanf_s(file, "%f", &knockBackPower.x);
	fscanf_s(file, "%f", &knockBackPower.y);


	fclose(file);

	file = nullptr;
	
	int objectNum = 0;
	sprintf(fileName, "Data/Enemy/Stage%d-%d.txt", pBG->now_stage, pBG->stageNumber);
	char* str = fileProcessing::readTextFile(fileName);
	if (str == nullptr)
		return;
	//項目数(縦、横)を計測
	for (int i = 0; i < strlen(str); i++) {
		switch (str[i]) {
		case '\n':
			objectNum++;
			break;
		}
	}

	fopen_s(&file, fileName, "r");

	for (int i = 0; i < objectNum; i++)
	{
		int pID;
		int ID;
		int lay;
		Vector2 p;
		fscanf_s(file, "%d", &ID);
		fscanf_s(file, "%d", &pID);
		fscanf_s(file, "%f", &p.x);
		fscanf_s(file, "%f", &p.y);
		fscanf_s(file, "%d", &lay);
		setEnemy(ID, p.x, p.y, false);
	}

	fclose(file);



}

void EnemyManager::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Enemy/Stage%d-%d.txt", pBG->now_stage, pBG->stageNumber);

	fopen_s(&file, fileName, "w");

	if (pObjManager->ObjList[Object::ENEMY].empty() == false)
	{
		for (auto& p = pObjManager->ObjList[Object::ENEMY].begin(); p != pObjManager->ObjList[Object::ENEMY].end(); p++)
		{

			fprintf_s(file, "%d ", p->get()->id.CLASS);
			fprintf_s(file, "%d ", p->get()->id.PERSONAL);
			fprintf_s(file, "%f ", p->get()->pos.x);
			fprintf_s(file, "%f ", p->get()->pos.y);
			fprintf_s(file, "%d\n", p->get()->layer);

		}
	}

	fclose(file);

	file = nullptr;
	sprintf(fileName, "Data/Enemy/knockBack.txt");

	fopen_s(&file, fileName, "w");

	fprintf_s(file, "%f\n", knockBackPower.x);
	fprintf_s(file, "%f", knockBackPower.y);


	fclose(file);
}
