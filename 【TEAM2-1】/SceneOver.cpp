#include "SceneOver.h"
#include "Sound.h"
#include "common.h"
#include "Input.h"
#include "Fade.h"
#include "Texture.h"
#include ".\\Shader\\PostFx.h"

#include "src\imgui.h"

Over::Over()
{
	select = 1;
	fontsize = 128;
	temp_screen = MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
}

Over::~Over()
{

}

void Over::Init()
{
	stageNumber--;
	fade_alpha = 255;
	for (auto& it : game_over)
	{
		it = DelayMover();
		it.SetMoveSpeed(0.04f);
		it.val = -40.f;
		it.target_val = 128.f;
	}
	pl_death_counter = 160;
	continue_alpha.SetMoveSpeed(0.07f);
	continue_alpha.difference_margin = 0.1f;
	continue_alpha.val = 0.f;
	continue_alpha.target_val = 255.f;
	continue_alpha.Update();
	pFade->In(80.f);
	scene_change = false;
	isSelect = true;
	POSTFX->Init((int)sceneManager::OVER);
}

void Over::Update()
{
	pFade->Update();
	POSTFX->Update();
	pSound->fadeinBGM(pSound->BGM_OVER);
	if (continue_alpha.IsUpdateCompleted())
	{
		{
			if (isSelect)
			{
				if (key[KEY_INPUT_LEFT] == 1)
				{
					pSound->playSE(pSound->SE_SYSTEM_SELECT,false);
					select = 1;
				}
				if (key[KEY_INPUT_RIGHT] == 1)
				{
					pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
					select = 2;
				}
				if (key[KEY_INPUT_SPACE] == 1)
				{
					pSound->playSE(pSound->SE_SYSTEM_DECISION, false);
					if (scene_change == false)pFade->Out(60.f);
					scene_change = true;
					isSelect = false;
				}
			}
		}
	}
	if (scene_change && pFade->IsUpdateCompleted())
	{
		if (select == 1)
		{
			sceneManager::isSwitchScene = true;
			sceneManager::next_scene = sceneManager::GAME;
		}
		else if (select == 2)
		{
			sceneManager::isSwitchScene = true;
			sceneManager::next_scene = sceneManager::TITLE;
		}
	}

	int counter = 0;
	for (auto& it : game_over)
	{
		if (pl_death_counter < (160 / 8) * counter)
		{
			game_over[7 - counter].Update();
		}
		counter++;
	}
	pl_death_counter--;
	if (pl_death_counter < 0)
	{
		continue_alpha.Update();
	}
}

void Over::Draw()
{
	if (key[KEY_INPUT_LCONTROL] && key[KEY_INPUT_0] == 1)
		show_debug = !show_debug;

	SetDrawScreen(temp_screen);
	ClearDrawScreen();

	int counter = 0;
	for (auto& it : game_over)
	{
		DrawRectGraph(256 + (16 * counter), SCASI(it.val), (16 * counter), 512, 16, 32, pTexture->getGraphs(Texture::UI_FONT), TRUE);
		counter++;
	}

	SetFontSize(fontsize);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, SCASI(continue_alpha.val));
	DrawRectGraph(265, 200, 0, 544, 120, 32, pTexture->getGraphs(Texture::UI_FONT), TRUE);
	if (select == 1)
	{
		DrawRectGraph(224, 240, 0, 576, 192, 64, pTexture->getGraphs(Texture::UI_FONT), TRUE);
	}
	if (select == 2)
	{
		DrawRectGraph(224, 240, 192, 576, 192, 64, pTexture->getGraphs(Texture::UI_FONT), TRUE);
	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);


	//SetFontSize(16);

	POSTFX->Render(&temp_screen);
	pFade->Draw();

	SetDrawScreen(DX_SCREEN_BACK);
	ClearDrawScreen();
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
	DrawRectRotaGraph2
	(
		SCREEN_CX, SCREEN_CY,
		0, 0,
		TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
		TEMP_SCREEN_CX, TEMP_SCREEN_CY,
		(double)TEMP_SCREEN_EXT_RATE_MAX,
		0.0,
		temp_screen, 1
	);
	POSTFX->Draw();

}

void Over::ImGui()
{
	ImGui::Begin("sceneOver", nullptr, ImGuiWindowFlags_MenuBar);

	ImGui::Text("changeScene");

	if (ImGui::Button("Title"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::TITLE;
	}
	if (ImGui::Button("Game"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::GAME;
	}
	if (ImGui::Button("Clear"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::CLEAR;
	}

	if (ImGui::Button("postfx"))Iwin_flg.postfx ^= 1;
	POSTFX->ImGui();

	ImGui::End();

}
