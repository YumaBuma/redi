#pragma once
#include "scene.h"
#include "Camera.h"
#include "EffectManager.h"
//#include "./edt_import/oFILE.h"
//#include "./edt_import/inDATA.h"
#include <memory>
#include <fstream>
#include <utility>
#include <array>
#include <map>
#include <string>
#include <unordered_map>
#include <initializer_list>

#include <iostream>
#include <cereal\cereal.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\string.hpp>
#include <cereal\types\unordered_map.hpp>
#include "CerealLoadAndSave.h"

#include "EffectPivot.h"

// todo: DONE!! double jump effect
// todo: DONE!! score, item, gem? and attract to player
// todo: DONE!! camera minimap
// todo: DONE!! boss's eyelight like blue
// todo: DONE!! post processing

// todo: emitter place template
// todo: emitter layer change

// todo: DONE!! camera aiming
// todo: DONE!! moving pivot velocity pivot
// todo: DONE!! effect size fade in out
// todo: DONE!! effect separate fade alpha radius
// todo: DONE!! effect collision

class EffectEditor : public BaseScene
{
public:
	EffectEditor();
	~EffectEditor();
	void Init();
	void Update();
	void Draw();

	void ImGui(const int& _ghnd);
	void ImGuiImageViewer();
	bool ImGuiImagesList(const std::string& _hidden_label, int* _p_ghnd = nullptr, std::string* _p_ghnd_path = nullptr);
	void ImGuiEmitterNum();
	void ImGuiEffectList();

	void ImGuiEffectEmitter();
	void ImGuiEffect();

	void ImGuiMultipleSelectEffects();
	void GetMultipleSelectEffects();
	void MoveMultipleSelectEffects();
	void CalcMinPos();
	void ResetEffects();

	void ImGuiTreeNodeEffectParams(const std::string& _hidden_label, EffectParams& _params);

	void ImGuiGetPixelColor(const int& _ghnd);

	template <class Archive>
	void serialize(Archive& _ar)
	{
		_ar(CEREAL_NVP(emit_dset));
	}

	void SaveEmitDataset(const std::string& _filename, const int& _archive_type);
	void LoadEmitDataset(const std::string& _filename, const int& _archive_type);
public:
	std::vector<int>			ghnds;
	std::vector<std::string>	ghnds_path;// relative

	int emitter_size = 0;
	int now_edit_emitparam_num = 0;
	int now_edit_efcparam_num = 0;

	int											ghnd;
	std::string									ghnd_path;

	std::array<std::string, EFC_END + 1>		efc_types;
	std::array<std::string, EffectParams::eAngleTypes::ANGLE_END + 1>		efc_angle_types;
	std::array<std::string, EffectParams::ePrimitiveTypes::PRIMTYPE_END>	efc_prim_types;
	std::array<std::pair<int, std::string >, 5> blend_modes;

	int											efc_states;
	bool										is_now_playing;
	bool										is_able_to_use_tools;
	bool										can_set_effect;
	bool										can_select_multiple_effect;
	bool										can_move_multiple_effect;
	bool										is_inherit_emt;
	bool										is_pivot_base_vel;


	EffectParams::DRAW							play_param;

	std::shared_ptr<EMIT_DATASET>				emit_dset;

	EMIT_DATA*									now_edt_emit_data;
	std::vector<EffectParams>*					now_edt_efcparam;

				EffectParams*					now_edt_efc_tmp;
	std::vector<size_t>							now_selecting_efcs_tmp;

	std::shared_ptr<EffectRoot>					efc_root;

	DirectX::SimpleMath::Vector2				pos = { 100.f, 50.f };

	PositionPivot								pos_pivot;
	VelocityPivot								vel_pivot;
};
