#include "SceneClear.h"
#include "common.h"
#include "fade.h"
#include "Texture.h"
#include "Input.h"
#include "Camera.h"
#include "Sound.h"
#include "src\imgui.h"
#include ".\\Shader\\PostFx.h"
#include "ObjectManager.h"
#include "GameMenu.h"
#include "UI.h"

int Clear::fogTimer = 0;

Clear::Clear()
{
	loading_alpha = 255;
	isincliment = true;
	timer = 0;
	pFade->Init();
	pFade->In(60, GetColor(0,0,0));

	start = true;
	//SetDrawValidGraphCreateFlag(false);
	//SetDrawValidFloatTypeGraphCreateFlag(false);
	//SetDrawValidAlphaChannelGraphCreateFlag(true);
	temp_screen = MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);

	fade_alpha = 255;
}

Clear::~Clear()
{
	DeleteGraph(temp_screen);
}

void Clear::Init()
{
	stageNumber = 0;

	clear_Gr.loadAnimFile("clear");
	clear_efc = EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("clear.bin"), clear_Gr, true);

	POSTFX->Init((int)sceneManager::CLEAR);

	if (!pObjManager->ObjList[Object::PLAYER].empty())
	{
		pObjManager->ObjList[Object::PLAYER].back()->money = 0;
		pObjManager->ObjList[Object::PLAYER].back()->addHP(1000);
	}
	Gmenu.pl_money = 0;
	ui.tmp_MAX_MONEY = 0;
	ui.tmp_money = 0;
	
	//pObjManager->ObjList[Object::PLAYER].back()->pos.x, pObjManager->ObjList[Object::PLAYER].back()->Init();

}

void Clear::Update()
{
	if (key[KEY_INPUT_LCONTROL] && key[KEY_INPUT_0] == 1)
		show_debug = !show_debug;
	pSound->fadeinBGM(pSound->BGM_CLEAR);
	clear_efc->Update();
	pFade->Update();
	if (start)
	{
		//pFade->In(30.0f);

		if (pFade->IsUpdateCompleted())
		{
			start = false;
		}
	}
	else
	{
		timer++;
	}

	if (isincliment)
	{
		loading_alpha += 3;
		if (loading_alpha >= 255)
		{
			isincliment = false;
		}
	}
	else
	{
		loading_alpha-= 3;
		if (loading_alpha <= 0)
		{
			isincliment = true;
		}
	}

	fogTimer++;

	if (fogTimer > 7680)
	{
		fogTimer = 0;
	}

	if (key[KEY_INPUT_SPACE] == 1 && timer < 600)
	{
		timer = 600;
		pFade->Out(30, GetColor(255, 255, 255));
		pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
	}

	if (timer > 600)
	{
		if (pFade->IsUpdateCompleted())
		{
			sceneManager::isSwitchScene = true;
			sceneManager::next_scene = sceneManager::TITLE;
		}
	}

	POSTFX->Update();
}

void Clear::Draw()
{
	clear_Gr.stop_anim = false;
	clear_Gr.playAnimation();
	if (timer == INT_MAX)
		timer = 0;
	SetDrawScreen(temp_screen);
	ClearDrawScreen();
	SetDrawMode(DX_DRAWMODE_NEAREST);
	//SetDrawBlendMode(DX_BLENDMODE_ALPHA, bg_col[3]);
	//DrawFillBox(0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, GetColor(bg_col[0], bg_col[1], bg_col[2]));

	//DrawRectGraph(0, 0, clear_Gr.tex_size.x*clear_Gr.aFrame, 0, clear_Gr.tex_size.x, clear_Gr.tex_size.y, pTexture->getGraphs(pTexture->ENDROLL), TRUE);
	
	DrawGraph(0, 0, pTexture->getGraphs(pTexture->END_1), TRUE);

	DrawGraph(-(fogTimer / 4) % 1920, 0, pTexture->getGraphs(pTexture->BG7), TRUE);
	DrawGraph(1920 - (fogTimer / 4) % 1920, 0, pTexture->getGraphs(pTexture->BG7), TRUE);

	DrawGraph(-640*((timer/5)%12), 0, pTexture->getGraphs(pTexture->END_2), TRUE);

	DrawGraph(-(fogTimer / 2) % 1920, 0, pTexture->getGraphs(pTexture->BG8), TRUE);
	DrawGraph(1920 - (fogTimer / 2) % 1920, 0, pTexture->getGraphs(pTexture->BG8), TRUE);

	DrawGraph(-640*((timer/10)%12), 0, pTexture->getGraphs(pTexture->END_3), TRUE);

	
	clear_efc->Draw();
	//SetDrawBlendMode(DX_BLENDMODE_ALPHA, loading_alpha);
	//DrawGraph(0, 0, pTexture->getGraphs(pTexture->LOADING), TRUE);
	//SetDrawBlendMode(DX_BLENDMODE_NOBLEND, loading_alpha);
	//DrawExtendFormatStringToHandle(SCREEN_WIDTH*0.36f, SCREEN_HEIGHT*0.7f, 4.0, 3.0, GetColor(255, 255, 150), font_c, "PUSH_START_BUTTON");
	POSTFX->Render(&temp_screen);
	SetDrawScreen(DX_SCREEN_BACK);
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		DrawRectRotaGraph2
		(
			SCREEN_CX - cam.zxi(), SCREEN_CY - cam.zyi(),
			0, 0,
			TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
			TEMP_SCREEN_CX, TEMP_SCREEN_CY,
			(double)TEMP_SCREEN_EXT_RATE_MAX + cam.zoom(),
			GetRadian(cam.angle()),
			temp_screen, 1
		);

		POSTFX->Draw();


	}
	DrawRectRotaGraphFast2(0, 0, 0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 0, 0, TEMP_SCREEN_EXT_RATE_MAX, 0.f, pTexture->getGraphs(pTexture->END_4), TRUE);

	pFade->Draw();

}

void Clear::ImGui()
{
	ImGui::Begin("sceneClear", nullptr, ImGuiWindowFlags_MenuBar);

	ImGui::Text("changeScene");

	if (ImGui::Button("Title"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::TITLE;
	}
	if (ImGui::Button("Game"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::GAME;
	}
	if (ImGui::Button("Over"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::OVER;
	}

	if (ImGui::Button("postfx"))Iwin_flg.postfx ^= 1;
	POSTFX->ImGui();

	ImGui::End();

}
