#ifndef _FUNCTION_H_
#define _FUNCTION_H_

#include <d3d11.h>
#include "SimpleMath.h"

#include "PerlinNoise.h"

#include <vector>//469行目から
#include <string>//469行目から

// 共用体
union Color255 // RGBA 0~255
{
	enum eColor255 { r, g, b, a, end };
	unsigned int	colors;		// 1バイト毎に0~255の色情報を格納する
	unsigned char	at[end];	// colorsの共用体　各色を抽出できる
};
typedef Color255::eColor255 eCol;// Color255::eColor255

struct cdefs
{
	static const float PI_f;
	static const float TORADIAN;
	static const float TODEGREE;
	
	static const float e;
	static const float e2;
	static const float e2_reciprocal;
};

extern PerlinNoise p_noise;

#pragma region キャスト

// static_cast<out_type>(_value)
template<typename out_type, typename in_type>
__forceinline out_type scast(const in_type& _value)
{
	return static_cast<out_type>(_value);
}

// static_cast<int>(_value)
template<typename in_type>
__forceinline int scasi(const in_type& _value)// int
{
	return static_cast<int>(_value);
}

// static_cast<float>(_value)
template<typename in_type>
__forceinline float scasf(const in_type& _value)// float
{
	return static_cast<float>(_value);
}

// static_cast<double>(_value)
template<typename in_type>
__forceinline double scasd(const in_type& _value)
{
	return static_cast<double>(_value);
}

#pragma endregion

#pragma region マクロ

// 変数名を文字列にする
#define VAR_TO_STR(_variable_name) #_variable_name

// static_cast<int>(_value)
#define SCASI(_value) static_cast<int>((_value))
// static_cast<float>(_value)
#define SCASF(_value) static_cast<float>((_value))
// static_cast<double>(_value)
#define SCASD(_value) static_cast<double>((_value))

#define XY_F2I(_vec2) SCASI(_vec2.x), SCASI(_vec2.y)
#define XY_I2F(_vec2) SCASF(_vec2.x), SCASF(_vec2.y)
#define XY_F(_vec2) _vec2.x, _vec2.y
#define XYZ_F2I(_vec3) SCASI(_vec3.x), SCASI(_vec3.y), SCASI(_vec3.z)
#define XYZ_I2F(_vec3) SCASF(_vec3.x), SCASF(_vec3.y), SCASF(_vec3.z)
#define XYZ_F(_vec3) _vec3.x, _vec3.y, _vec3.z
#define XYZW_F(_vec4) _vec4.x, _vec4.y, _vec4.z, _vec4.w

#pragma endregion

#pragma region 符号関数

inline int signi(const int& _value)
{
	return (_value >> 31) + (_value > 0);
}
inline float signf(const float& _value)
{
	// https://www.javadrive.jp/cstart/ope/index6.html
	// http://www.c-lang.org/operator.html
	// https://www.google.com/search?biw=1920&bih=975&tbm=isch&sa=1&ei=ZNHXXIzzC4CRr7wPnYOJsAk&q=%E3%83%93%E3%83%83%E3%83%88%E3%82%B7%E3%83%95%E3%83%88%E3%80%80%E7%AC%A6%E5%8F%B7&oq=%E3%83%93%E3%83%83%E3%83%88%E3%82%B7%E3%83%95%E3%83%88%E3%80%80%E7%AC%A6%E5%8F%B7&gs_l=img.3..0i24.635043.638311..638623...0.0..0.69.447.7......0....1..gws-wiz-img.......0i4i24.QKwVP36L4KU
	// https://www.cc.kyoto-su.ac.jp/~yamada/programming/bit.html

	// 符号部分は32bit目なので符号ビット前までシフトして符号を抽出
	// 右シフト時に埋められる値は符号bitの値を継承する
	//	例えば符号bitが1(負)の場合
	//		に右へシフトを行うと1で埋まるので
	//		(value_i >> 31)の場合-1(0xfffffff)となる
	int value_i = int(_value);
	float sign_val = float
	(
		(value_i >> 31)	// result: -1   0   0
		+ (value_i > 0)	// value is bigger than zero?
	);

	return sign_val;
}
//inline double signd(const double& _value)
//{
//	long long int value_lli = long long int(_value);
//	double sign_val = double
//	(
//		(value_lli >> 63)	// result: -1   0   0
//		+ (value_lli > 0)	// value is bigger than zero?
//	);
//
//	return sign_val;
//}

#pragma endregion

#pragma region 距離取得

inline const float& GetLength(const float& _vx, const float& _vy)
{
	return sqrtf((_vx * _vx) + (_vy * _vy));
}
inline const float& GetLengthAtoB(const float& _ax, const float& _ay, const float& _bx, const float& _by)
{
	DirectX::SimpleMath::Vector2 b_sub_a = { _bx - _ax, _by - _ay };
	return sqrtf((b_sub_a.x * b_sub_a.x) + (b_sub_a.y * b_sub_a.y));
}

inline const float& GetLength(const DirectX::SimpleMath::Vector2& _v)
{
	// result = sqrt(x^2 + y^2);
	return sqrtf
	(
		(_v.x * _v.x) +
		(_v.y * _v.y)
	);
}
inline const float& GetLengthVec3(const DirectX::SimpleMath::Vector3& _v)
{
	float result = sqrtf
	(
		_v.x * _v.x +
		_v.y * _v.y +
		_v.z * _v.z
	);
	return result;
}

inline const float& GetDistanceAtoB(const DirectX::SimpleMath::Vector2 & _a, const DirectX::SimpleMath::Vector2 & _b)
{
	DirectX::SimpleMath::Vector2 b_sub_a = _b - _a;
	return sqrtf((b_sub_a.x * b_sub_a.x) + (b_sub_a.y * b_sub_a.y));
}
inline const float& GetDistanceVec3AToB(const DirectX::SimpleMath::Vector3 & _a, const DirectX::SimpleMath::Vector3 & _b)
{
	DirectX::SimpleMath::Vector3 b_sub_a = { _b.x - _a.x, _b.y - _a.y, _b.z - _a.z };
	float result = sqrtf
	(
		b_sub_a.x * b_sub_a.x +
		b_sub_a.y * b_sub_a.y +
		b_sub_a.z * b_sub_a.z
	);
	return result;
}

#pragma endregion

#pragma region Vector

#pragma region 乗算除算
inline const DirectX::SimpleMath::Vector3&	Vec3Mul			(										const DirectX::SimpleMath::Vector3& _v1, const DirectX::SimpleMath::Vector3&	_v2		)
{
	DirectX::SimpleMath::Vector3 result =
	{
		_v1.x * _v2.x,
		_v1.y * _v2.y,
		_v1.z * _v2.z
	};
	return result;
}
inline void									Vec3MulOut		(DirectX::SimpleMath::Vector3& _vout,	const DirectX::SimpleMath::Vector3& _v1, const DirectX::SimpleMath::Vector3&	_v2		)
{
	_vout.x = _v1.x * _v2.x;
	_vout.y = _v1.y * _v2.y;
	_vout.z = _v1.z * _v2.z;
}
inline const DirectX::SimpleMath::Vector3&	Vec3MulScale	(										const DirectX::SimpleMath::Vector3& _v1, const float&							_scale	)
{
	DirectX::SimpleMath::Vector3 result =
	{
		_v1.x * _scale,
		_v1.y * _scale,
		_v1.z * _scale
	};
	return result;
}
inline void									Vec3MulScaleOut	(DirectX::SimpleMath::Vector3& _vout,	const DirectX::SimpleMath::Vector3& _v1, const float&							_scale	)
{
	_vout.x = _v1.x * _scale;
	_vout.y = _v1.y * _scale;
	_vout.z = _v1.z * _scale;
}

inline const DirectX::SimpleMath::Vector3&	Vec3Div			(										const DirectX::SimpleMath::Vector3& _v1, const DirectX::SimpleMath::Vector3&	_v2		)
{
	DirectX::SimpleMath::Vector3 result =
	{
		_v1.x / _v2.x,
		_v1.y / _v2.y,
		_v1.z / _v2.z
	};
	return result;
}
inline void									Vec3DivOut		(DirectX::SimpleMath::Vector3& _vout,	const DirectX::SimpleMath::Vector3& _v1, const DirectX::SimpleMath::Vector3&	_v2		)
{
	_vout.x = _v1.x / _v2.x;
	_vout.y = _v1.y / _v2.y;
	_vout.z = _v1.z / _v2.z;
}
inline const DirectX::SimpleMath::Vector3&	Vec3DivScale	(										const DirectX::SimpleMath::Vector3& _v1, const float&							_scale	)
{
	DirectX::SimpleMath::Vector3 result =
	{
		_v1.x / _scale,
		_v1.y / _scale,
		_v1.z / _scale
	};
	return result;
}
inline void									Vec3DivScaleOut	(DirectX::SimpleMath::Vector3& _vout,	const DirectX::SimpleMath::Vector3& _v1, const float&							_scale	)
{
	_vout.x = _v1.x / _scale;
	_vout.y = _v1.y / _scale;
	_vout.z = _v1.z / _scale;
}
#pragma endregion

#pragma region 内積

// 内積を取得します
inline const float& GetDotVec2(const DirectX::SimpleMath::Vector2& _a, const DirectX::SimpleMath::Vector2& _b)
{
	float result =
		(_a.x * _b.x) +
		(_a.y * _b.y);
	return result;
}
// 内積を取得します
inline const float& GetDotVec3(const DirectX::SimpleMath::Vector3& _a, const DirectX::SimpleMath::Vector3& _b)
{
	float result = 
		(_a.x * _b.x) +
		(_a.y * _b.y) +
		(_a.z * _b.z);
	return result;
}

#pragma endregion

#pragma region 外積

inline const float GetCrossVec2
(
	const DirectX::SimpleMath::Vector2& _a,
	const DirectX::SimpleMath::Vector2& _b
)
{
	// 外積は基本的に正規化しておいたほうが良い
	float result = 
		(_a.x * _b.y) -
		(_a.y * _b.x);
	return result;
}

inline DirectX::SimpleMath::Vector3 GetCrossVec3
(
	const DirectX::SimpleMath::Vector3& _a,
	const DirectX::SimpleMath::Vector3& _b
)
{
	// 外積は基本的に正規化しておいたほうが良い
	// directxは左手座標系(自分の方向へのzが正)
	// https://www.google.com/search?q=%E5%B7%A6%E6%89%8B%E7%B3%BB&source=lnms&tbm=isch&sa=X&ved=0ahUKEwigraDX35XiAhWHwJQKHfNOC6oQ_AUIDigB&biw=1920&bih=975#imgrc=Inz6bikp28cr_M:
	// https://www.google.com/search?q=%E5%B7%A6%E6%89%8B%E7%B3%BB&source=lnms&tbm=isch&sa=X&ved=0ahUKEwigraDX35XiAhWHwJQKHfNOC6oQ_AUIDigB&biw=1920&bih=975#imgrc=lNcBZwilmDoELM:
	// https://www.google.com/search?q=%E3%83%99%E3%82%AF%E3%83%88%E3%83%AB+%E5%A4%96%E7%A9%8D&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiw4caj1ZXiAhUmxIsBHZY7Ab4Q_AUIDigB&biw=1920&bih=975#imgrc=_a9bNyubZOLo4M:
	DirectX::SimpleMath::Vector3 result;
	// cross.x
	// "ax" ay az
	//        \
	// "bx" by bz
	//	      -
	// "ax" ay az
	//        /
	// "bx" by bz
	result.x = (_a.y * _b.z) - (_a.z * _b.y);

	// cross.y
	// ax "ay" az	<<	"ay" az ax
	//  \------\		       \     
	// bx "by" bz	<<	"by" bz bx
	//	       -		       -     
	// ax "ay" az	<<	"ay" az ax
	//  /------/		       /     
	// bx "by" bz	<<	"by" bz bx
	result.y = (_a.z * _b.x) - (_a.x * _b.z);

	// cross.y
	// ax ay "az"	<<	"az" ax ay
	//   \				     \
	// bx by "bz"	<<	"bz" bx by
	//	 -				     -
	// ax ay "az"	<<	"az" ax ay
	//   /				     /
	// bx by "bz"	<<	"bz" bx by
	result.z = (_a.x * _b.y) - (_a.y * _b.x);

}

#pragma endregion

#pragma region 射影ベクトル

// 射影ベクトルを取得します
inline const DirectX::SimpleMath::Vector3& GetOrthographicVector
(
	const DirectX::SimpleMath::Vector3& _a,
	const DirectX::SimpleMath::Vector3& _screen // 投影ベクトル
)
{
	// result = ( dot(a,screen) / (len(screen)^2) ) * screen
	float	dot				= GetDotVec3(_a, _screen);
	float	screen_len_pow2 = GetLengthVec3(_screen);
			screen_len_pow2 = screen_len_pow2 * screen_len_pow2;

	float	dot_div_len = dot / screen_len_pow2;

	DirectX::SimpleMath::Vector3 // OrthographicVector
			result;
			result = Vec3Mul(_screen, DirectX::SimpleMath::Vector3(dot_div_len, dot_div_len, dot_div_len));
	return	result;
};

// 射影ベクトルを取得します
inline const DirectX::SimpleMath::Vector3& GetOrthographicVectorNormalized
(
	const DirectX::SimpleMath::Vector3& _a,
	const DirectX::SimpleMath::Vector3& _screen // 投影ベクトル
)
{
	// _screenが単位ベクトルだった場合は式を簡略化できる
	// result = dot(a,screen) * screen
	float dot = GetDotVec3(_a, _screen);

	DirectX::SimpleMath::Vector3 // OrthographicVector
			result; 
			result = Vec3Mul(_screen, DirectX::SimpleMath::Vector3(dot, dot, dot));
	return	result;
};

#pragma endregion

#pragma endregion

#pragma region 角度取得

//	ラジアン取得
inline float GetRadian(const float& _degree)
{
	float radian = _degree * cdefs::TORADIAN;
	return radian;
}

// ベクトルから角度(ラジアン)を返します
inline float GetAngleFromVec2(const DirectX::SimpleMath::Vector2& _v)
{
	float	result = atan2f(_v.y, _v.x);
	return	result;
}

// 2つのベクトルのなす角度を取得します
inline const float& GetCornerVec2
(
	const DirectX::SimpleMath::Vector2& _a,
	const DirectX::SimpleMath::Vector2& _b
)
{
	// aとbの内積 / aの長さ*bの長さ
	float	dot			= GetDotVec2(_a, _b);
	float	len_a_mul_b = GetLength(_a) * GetLength(_b);

	float	result;						// radian
			result = dot / len_a_mul_b;	// ここで求められる値は cosθ なのでacosfでラジアンを算出する
			result = acosf(result);
	return	result;
}
// 2つのベクトルのなす角度を取得します
inline const float& GetCornerVec3
(
	const DirectX::SimpleMath::Vector3& _a,
	const DirectX::SimpleMath::Vector3& _b
)
{
	// aとbの内積 / aの長さ*bの長さ
	float	dot			= GetDotVec3(_a, _b);
	float	len_a_mul_b = GetLengthVec3(_a) * GetLengthVec3(_b);

	float	result;						// radian
			result = dot / len_a_mul_b;	// ここで求められる値は cosθ なのでacosfでラジアンを算出する
			result = acosf(result);
	return	result;
}


//	ディグリー取得
inline float GetDegree(const float& _radian)
{
	float degree = _radian * cdefs::TODEGREE;
	return degree;
}

#pragma endregion

#pragma region ベクトル正規化

inline const DirectX::SimpleMath::Vector2 GetNormalize(const DirectX::SimpleMath::Vector2& _vector2)
{
	DirectX::SimpleMath::Vector2 result;
								 result = _vector2;
								 result.Normalize();
	return						 result;
}
inline const DirectX::SimpleMath::Vector2 GetNormalizeVec2(DirectX::SimpleMath::Vector2 _vector2)
{
	// 正規化ベクトル = 各ベクトル成分.x, y / 長さ(l);
	float l = GetLength(_vector2);
	DirectX::SimpleMath::Vector2 result;
								 result.x = _vector2.x / l;
								 result.y = _vector2.y / l;
	return						 result;
};
inline const DirectX::SimpleMath::Vector3& GetNormalizedVec3(const DirectX::SimpleMath::Vector3 _v)
{
	float l = GetLengthVec3(_v);
	DirectX::SimpleMath::Vector3 result;
	Vec3DivScaleOut(result, _v, l);
	return result;
}

// 角度(ラジアン)からベクトルを返します
inline DirectX::SimpleMath::Vector2 GetVec2FromAngle(const float& _angle_rad)
{
	DirectX::SimpleMath::Vector2 result;
			result.x = cosf(_angle_rad);
			result.y = sinf(_angle_rad);
	return	result;
}

#pragma endregion

#pragma region 合成波の揺れ

// ベース波の周期, ベース波の振幅, 時間, サブ波周期のオクターブ(ベース波の周期に乗算), サブ波の振幅(ベース波の振幅に乗算)
inline float WiggleX
(
	const float& _frequency,
	const float& _amplitude,
	const float& _time,
	const float& _octaves = 1.0f,
	const float& _amplitude_mul = 0.5f
)
{
	float	result;
			result = cosf( _frequency*			  GetRadian(_time)) *  _amplitude
					 +
					 cosf((_frequency*_octaves) * GetRadian(_time)) * (_amplitude*_amplitude_mul);
	return	result;
}

// ベース波の周期, ベース波の振幅, 時間, サブ波周期のオクターブ(ベース波の周期に乗算), サブ波の振幅(ベース波の振幅に乗算)
inline float WiggleY
(
	const float& _frequency,
	const float& _amplitude,
	const float& _time,
	const float& _octaves = 1.0f,
	const float& _amplitude_mul = 0.5f
)
{
	float	result;
			result = sinf(_frequency*			  GetRadian(_time)) *  _amplitude
					 +
					 sinf((_frequency*_octaves) * GetRadian(_time)) * (_amplitude*_amplitude_mul);
	return	result;
}

#pragma endregion

#pragma region 遅延移動

inline void DelayMove(float& _now_value, const float& _target_value, const float& _delay_move_speed)
{
	_now_value += (_target_value - _now_value) * _delay_move_speed;
}

inline void DelayMoveVec2(DirectX::SimpleMath::Vector2& _now_pos, const DirectX::SimpleMath::Vector2& _target_pos, const float& _delay_move_speed)
{
	DelayMove(_now_pos.x, _target_pos.x, _delay_move_speed);
	DelayMove(_now_pos.y, _target_pos.y, _delay_move_speed);
}

#pragma endregion

#pragma region 正規化(数値調節)

/*==================================================*/
//  正規化(数値調節)//https://mathwords.net/dataseikika
/*==================================================*/
//_valueが_value_max時に_fit_maxを返し、_value_minの時に_fit_minを返す
inline float Fit
(
	const float& _value,
	const float& _value_max,
	const float& _fit_max = 1.f,
	const float& _fit_min = 0.f,
	const float& _value_min = 0.f//大抵の場合_value_minは0で使うと思うので後ろに置いています
)
{
	float	div_num = _value_max - _value_min;
	if	   (div_num == 0.f) return _fit_min;

	float	result ;//公式を基に計算します
			result = (_value - _value_min) / div_num;
			result = result * (_fit_max - _fit_min);
			result = result + _fit_min;
	return	result;
}
//_value_refを_value_max時に_fit_maxに変換し、_value_minの時に_fit_minに変換します
inline void FitTo
(
	float&		 _value_ref,
	const float& _value_max,
	const float& _fit_max = 1.f,
	const float& _fit_min = 0.f,
	const float& _value_min = 0.f//大抵の場合_value_minは0で使うと思うので後ろに置いています
)
{
	float div_num = _value_max - _value_min;
	if	 (div_num == 0.f)
	{
		_value_ref = _fit_min;
		return;
	}

	float	result;//公式を基に計算します
			result = (_value_ref - _value_min) / div_num;
			result = result * (_fit_max - _fit_min);
			result = result + _fit_min;

	_value_ref	= result;
}

#pragma endregion

#pragma region 比率計算

// _valの標準数 _preferred_numberに対する _compare_valの比率を返します
inline float GetRatio(const float& _val, const float& _compare_val, const float& _preferred_number = 1.f)
{
	// _before_val : _after_val = _composition_ratio : result;
	// _before_val * result		= _after_val * _composition_ratio;
	//				 result		= (_after_val * _composition_ratio) / _before_val;

	if (_val == 0.f)return 0.f;

	float	result;
	float	mid_expr;// 途中式

			mid_expr = _compare_val * _preferred_number;
			result   = mid_expr / _val;

	return	result;
}

// _valに対する _compare_valの構成比率を返します
inline float GetCompoRatio(const float& _val, const float& _compare_val, const float& _normal_percentage = 100.f)
{
	float	result;
	float	component_ratio;
	float	mid_expr;// 途中式

			mid_expr		= _val + _compare_val;
			component_ratio = _compare_val / mid_expr;
			result			= component_ratio * _normal_percentage;

	return	result;
}

#pragma endregion

#pragma region 乱数

/*==================================================*/
//  float乱数
/*==================================================*/
inline float GetRand(const float& RandMax)
{
	float random_fit1 = scasf(scasf(GetRand(RAND_MAX)) / scasf(RAND_MAX));

	float result = (RandMax*random_fit1);
	return result;
}

/*==================================================*/
//  符号付き乱数
/*==================================================*/
inline int GetRandSigned(const int& _min, const int& _max)
{
	int		result = scasi(Fit(GetRand(scasf(_max)), scasf(_max), scasf(_max), scasf(_min)));
	return	result;
}

inline int GetRandSigned(const int& _min_max)
{
	int		result = scasi(Fit(scasf(GetRand(_min_max)), scasf(_min_max), scasf(_min_max), scasf(-_min_max)));
	return	result;
}

inline float GetRandSigned(const float& _min, const float& _max)
{
	float	result = Fit(GetRand(_max), _max, _max, _min);
	return	result;
}

inline float GetRandSigned(const float& _min_max)
{
	float	result = Fit(GetRand(_min_max), _min_max, _min_max, -_min_max);
	return	result;
}

//_is_normalize = trueの場合 ｢_random_rangeの乱数によって生成されたベクトルを正規化したベクトル｣ * ｢_random_rangeの乱数によって生成されたベクトル｣ を返します
inline DirectX::SimpleMath::Vector2 GetRandVec2
(
	const DirectX::SimpleMath::Vector2& _random_range = { 1.f,1.f },
	const bool&							_is_normalize = true
)
{
	DirectX::SimpleMath::Vector2 result = { GetRand(_random_range.x), GetRand(_random_range.y) };
	if (_is_normalize)			 result = GetNormalize(result)*result;
	return						 result;
}

//_is_normalize = trueの場合 ｢_random_rangeの乱数によって生成されたベクトルを正規化したベクトル｣ * ｢_random_rangeの乱数によって生成されたベクトル｣ を返します
inline DirectX::SimpleMath::Vector2 GetRandSignedVec2
(
	const DirectX::SimpleMath::Vector4& _random_range = { -1.f,-1.f,+1.f,+1.f },
	const bool&							_is_normalize = true
)
{
	DirectX::SimpleMath::Vector2 result = { GetRandSigned(_random_range.x, _random_range.z), GetRandSigned(_random_range.y, _random_range.w) };
	if (_is_normalize)
	{
		DirectX::SimpleMath::Vector2 norm;
		norm = GetNormalize(result);
		result.x *= fabsf(norm.x);
		result.y *= fabsf(norm.y);
	}
	return result;
}

//_is_normalize = trueの場合 ｢_random_range_x,yの乱数によって生成されたベクトルを正規化したベクトル｣ * ｢_random_range_x,yの乱数によって生成されたベクトル｣ を返します
inline DirectX::SimpleMath::Vector2 GetRandSignedVec2
(
	const DirectX::SimpleMath::Vector2& _random_range_x = { -1.f,+1.f },
	const DirectX::SimpleMath::Vector2& _random_range_y = { -1.f,+1.f },
	const bool&							_is_normalize	= true
)
{
	DirectX::SimpleMath::Vector2 result = { GetRandSigned(_random_range_x.x, _random_range_x.y), GetRandSigned(_random_range_y.x, _random_range_y.y) };
	if (_is_normalize)			 result = GetNormalize(result)*result;
	return						 result;
}

#pragma endregion

#pragma region ランダムなベクトル

inline DirectX::SimpleMath::Vector3 PerlinNoiseVec3
(
	DirectX::SimpleMath::Vector3 _x,
	int				_noise_octaves		= 1,
	const float&	_noise_persistence	= 1.f,
	const float&	_time				= 0.f,
	const float&	_noise_scale		= 24.f
)
{
	/*
		Note: もし、ノイズ関数が整数格子にもとづいており、
		[-1, 1]の範囲で滑らかに変化するのであれば、
		N(v/N)にスケーリングされた偏微分は、おおよそO([-1/L, 1/L])
		の範囲の値でスケールLに渡って変化する。
	*/
	float fx = _x.x / _noise_scale;
	float fy = _x.y / _noise_scale;
	float fz = _x.z / _noise_scale;

	//0をOctavePerlin関数の引数_Octavesに入れると無限ループするため
	//if (_noise_octaves == 0)_noise_octaves = 1;
	_noise_octaves += (int)(!(bool)signi(_noise_octaves));

	float s = p_noise.OctavePerlin
	(
		fx,
		fy,
		fz,
		_noise_octaves, _noise_persistence
	);

	// (ページ中真ん中あたり) http://edom18.hateblo.jp/entry/2018/01/18/081750
	/*
		3Dの場合はポテンシャルのために3つの要素が必要となる。
		3つの、明らかに無関係なノイズ関数（vN(v)を使用する。それらは同じノイズ関数を、大きなオフセットを持たせたもの。
		下記のマジックナンバーはsから無関係な値分移動した場合のノイズ関数
	*/
	float s1 = p_noise.OctavePerlin
	(
		fy + 31.416f + _time,
		fz - 47.853f + _time,
		fx + 12.793f + _time,
		_noise_octaves, _noise_persistence
	);

	float s2 = p_noise.OctavePerlin
	(
		fz - 233.145f + _time,
		fx - 113.408f + _time,
		fy - 185.31f + _time,
		_noise_octaves, _noise_persistence
	);

	return DirectX::SimpleMath::Vector3(s, s1, s2);
}

inline DirectX::SimpleMath::Vector3 CurlNoise
(
	const DirectX::SimpleMath::Vector3& _pos,
	int _noise_octaves = 1,
	const float& _noise_persistence = 1.0f,
	const float& _time = 0.f,
	const float& _noise_scale = 24.f
)
{
	DirectX::SimpleMath::Vector3 _pos_tmp = { _pos.x, _pos.y, 0.f };

	// e = 現在位置から少しだけ移動した際の位置と現在位置との移動量の仮の値(ニセの移動量delta)
	DirectX::SimpleMath::Vector3 dx = DirectX::SimpleMath::Vector3(cdefs::e, 0.f, 0.f);
	DirectX::SimpleMath::Vector3 dy = DirectX::SimpleMath::Vector3(0.f, cdefs::e, 0.f);
	DirectX::SimpleMath::Vector3 dz = DirectX::SimpleMath::Vector3(0.f, 0.f, cdefs::e);

	DirectX::SimpleMath::Vector3 p_x0 = PerlinNoiseVec3(_pos_tmp - dx, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_x1 = PerlinNoiseVec3(_pos_tmp + dx, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_y0 = PerlinNoiseVec3(_pos_tmp - dy, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_y1 = PerlinNoiseVec3(_pos_tmp + dy, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_z0 = PerlinNoiseVec3(_pos_tmp - dz, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_z1 = PerlinNoiseVec3(_pos_tmp + dz, _noise_octaves, _noise_persistence, _time, _noise_scale);

	DirectX::SimpleMath::Vector3 result =
	{
		(p_y1.z - p_y0.z) - (p_z1.y - p_z0.y),
		(p_z1.x - p_z0.x) - (p_x1.z - p_x0.z),
		(p_x1.y - p_x0.y) - (p_y1.x - p_y0.x)
	};

	// e2 = 
	//		DirectX::SimpleMath::Vector3 p_x0 = PerlinNoiseVec3(_pos_tmp [- dx]<-この部分 , _noise_octaves, _noise_persistence, _time, _noise_scale);
	//		DirectX::SimpleMath::Vector3 p_x1 = PerlinNoiseVec3(_pos_tmp [+ dx]<-この部分 , _noise_octaves, _noise_persistence, _time, _noise_scale);
	// 絶対的な移動量はe + eとなるのでe2

	// e2_reciprocal = 求めたいのは偏微分なので分母となるe2の逆数
	//	result *= e2_reciprocal == result /= e2
			result.x *= cdefs::e2_reciprocal;
			result.y *= cdefs::e2_reciprocal;
			result.z *= cdefs::e2_reciprocal;
			result.Normalize();
	return	result;
}

inline DirectX::SimpleMath::Vector2 CurlNoise
(
	const DirectX::SimpleMath::Vector2& _pos,
	int _noise_octaves = 1,
	const float& _noise_persistence = 1.0f, 
	const float& _time = 0.f,
	const float& _noise_scale = 24.f
)
{
	DirectX::SimpleMath::Vector3 _pos_tmp = { _pos.x, _pos.y, 0.f };

	// e = 現在位置から少しだけ移動した際の位置と現在位置との移動量の仮の値(ニセの移動量delta)
	DirectX::SimpleMath::Vector3 dx = DirectX::SimpleMath::Vector3(cdefs::e, 0.f, 0.f);
	DirectX::SimpleMath::Vector3 dy = DirectX::SimpleMath::Vector3(0.f, cdefs::e, 0.f);
	DirectX::SimpleMath::Vector3 dz = DirectX::SimpleMath::Vector3(0.f, 0.f, cdefs::e);

	DirectX::SimpleMath::Vector3 p_x0 = PerlinNoiseVec3(_pos_tmp - dx, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_x1 = PerlinNoiseVec3(_pos_tmp + dx, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_y0 = PerlinNoiseVec3(_pos_tmp - dy, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_y1 = PerlinNoiseVec3(_pos_tmp + dy, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_z0 = PerlinNoiseVec3(_pos_tmp - dz, _noise_octaves, _noise_persistence, _time, _noise_scale);
	DirectX::SimpleMath::Vector3 p_z1 = PerlinNoiseVec3(_pos_tmp + dz, _noise_octaves, _noise_persistence, _time, _noise_scale);

	// 微分における分子の部分
	DirectX::SimpleMath::Vector3 result = 
	{
		(p_y1.z - p_y0.z) - (p_z1.y - p_z0.y),
		(p_z1.x - p_z0.x) - (p_x1.z - p_x0.z),
		(p_x1.y - p_x0.y) - (p_y1.x - p_y0.x)
	};

	// e2 = 
	//		DirectX::SimpleMath::Vector3 p_x0 = PerlinNoiseVec3(_pos_tmp [- dx]<-この部分 , _noise_octaves, _noise_persistence, _time, _noise_scale);
	//		DirectX::SimpleMath::Vector3 p_x1 = PerlinNoiseVec3(_pos_tmp [+ dx]<-この部分 , _noise_octaves, _noise_persistence, _time, _noise_scale);
	// 絶対的な移動量はe + eとなるのでe2

	// e2_reciprocal = 求めたいのは偏微分なので分母となるe2の逆数
	// result *= e2_reciprocal == result /= e2
	result.x *= cdefs::e2_reciprocal;
	result.y *= cdefs::e2_reciprocal;
	result.z *= cdefs::e2_reciprocal;
	result.Normalize();
	return DirectX::SimpleMath::Vector2(result.x, result.y);
}

#pragma endregion

namespace Coll
{
	//----------------------------------------------------------------------
	//	AABB
	//----------------------------------------------------------------------
	inline bool AABB
	(
		const DirectX::SimpleMath::Vector2& _a_min_pos,
		const DirectX::SimpleMath::Vector2& _a_max_pos,
		const DirectX::SimpleMath::Vector2& _b_min_pos,
		const DirectX::SimpleMath::Vector2& _b_max_pos
	)
	{
		if (_a_min_pos.x > _b_max_pos.x)return false;
		if (_a_max_pos.x < _b_min_pos.x)return false;
		if (_a_min_pos.y > _b_max_pos.y)return false;
		if (_a_max_pos.y < _b_min_pos.y)return false;

		return true;
	}
	inline bool AABBf
	(
		const float& _a_min_pos_x,
		const float& _a_min_pos_y,
		const float& _a_max_pos_x,
		const float& _a_max_pos_y,
		const float& _b_min_pos_x,
		const float& _b_min_pos_y,
		const float& _b_max_pos_x,
		const float& _b_max_pos_y
	)
	{
		if (_a_min_pos_x > _b_max_pos_x)return false;
		if (_a_max_pos_x < _b_min_pos_x)return false;
		if (_a_min_pos_y > _b_max_pos_y)return false;
		if (_a_max_pos_y < _b_min_pos_y)return false;

		return true;
	}
	inline bool AABBi
	(
		const int& _a_min_pos_x,
		const int& _a_min_pos_y,
		const int& _a_max_pos_x,
		const int& _a_max_pos_y,
		const int& _b_min_pos_x,
		const int& _b_min_pos_y,
		const int& _b_max_pos_x,
		const int& _b_max_pos_y
	)
	{
		if (_a_min_pos_x > _b_max_pos_x)return false;
		if (_a_max_pos_x < _b_min_pos_x)return false;
		if (_a_min_pos_y > _b_max_pos_y)return false;
		if (_a_max_pos_y < _b_min_pos_y)return false;

		return true;
	}
	//----------------------------------------------------------------------
	//	AABBCenter
	//----------------------------------------------------------------------
	inline bool AABBCenter
	(
		const DirectX::SimpleMath::Vector2& _a_center_pos,
		const DirectX::SimpleMath::Vector2& _a_radius,
		const DirectX::SimpleMath::Vector2& _b_center_pos,
		const DirectX::SimpleMath::Vector2& _b_radius
	)
	{
		if (_a_center_pos.x + _a_radius.x < _b_center_pos.x - _b_radius.x)return false;
		if (_a_center_pos.x - _a_radius.x > _b_center_pos.x + _b_radius.x)return false;
		if (_a_center_pos.y + _a_radius.y < _b_center_pos.y - _b_radius.y)return false;
		if (_a_center_pos.y - _a_radius.y > _b_center_pos.y + _b_radius.y)return false;

		return true;
	}
	inline bool AABBCenteri
	(
		const int& _a_center_pos_x,
		const int& _a_center_pos_y,
		const int& _a_radius_x,
		const int& _a_radius_y,
		const int& _b_center_pos_x,
		const int& _b_center_pos_y,
		const int& _b_radius_x,
		const int& _b_radius_y
	)
	{
		if (_a_center_pos_x + _a_radius_x < _b_center_pos_x - _b_radius_x)return false;
		if (_a_center_pos_x - _a_radius_x > _b_center_pos_x + _b_radius_x)return false;
		if (_a_center_pos_y + _a_radius_y < _b_center_pos_y - _b_radius_y)return false;
		if (_a_center_pos_y - _a_radius_y > _b_center_pos_y + _b_radius_y)return false;

		return true;
	}

	//----------------------------------------------------------------------
	//	Circle
	//----------------------------------------------------------------------
	inline bool Circle
	(
		const DirectX::SimpleMath::Vector2& _a_center_pos,
		const float&						_a_radius,
		const DirectX::SimpleMath::Vector2& _b_center_pos,
		const float&						_b_radius
	)
	{
		float len = ::GetDistanceAtoB(_a_center_pos, _b_center_pos);
		float rad = _a_radius + _b_radius;

		if (len <= rad)
			return true;
		else
			return false;
	}
}

#pragma region ボタン長押し時の挙動

// https://qiita.com/sumomoneko/items/58358eb8bcc8b70a481b

// ボタン(_pressing_count)が長押しされた時の挙動を返します
//	_pressing_countが 1の場合 1が返ります
//	_long_pressing_threshold 　: 長押しされたと見なす最小値
//	_chain_interval　　　　　　: 長押し時に0以外を返す間隔
inline const int& ButtonPressing(const int& _pressing_count, const int& _long_pressing_threshold = 20, const int& _chain_interval = 4)
{
	if (_pressing_count == 1) 
		return 1;
	if (_pressing_count < _long_pressing_threshold)
		return 0;

	if (_pressing_count % _chain_interval == 0)
		return _pressing_count;
	else
		return 0;
}

#pragma endregion

#pragma region ファイル

// _dir_nameディレクトリに存在するすべての_extension拡張子のついたファイルを返します(_extensionの文字列先頭に｢.｣を付ける必要なし)
// _merge_dir_name がtrueの場合第一引数_dir_nameの文字列とファイル名を結合した文字列を返します
std::vector<std::string> GetAllFileNamesInDir(const std::string& _dir_name, const std::string& _extension, bool _merge_dir_name = false);

// 開くファイル名をエクスプローラーを介して取得します
// _use_relative_path == true の場合相対パスを返し、falseの場合絶対パスを返します
std::string GetOpenFileNameWithExplorer(bool _use_relative_path = false, const std::string& _default_extension = "");

// 開くファイル名を複数選択してエクスプローラーを介して取得します
// _use_relative_path == true の場合相対パスを返し、falseの場合絶対パスを返します
std::vector<std::string> GetOpenMultipleFileNameWithExplorer(bool _use_relative_path = false, const std::string& _default_extension = "");

// 保存するファイル名をエクスプローラーを介して取得します
// _use_relative_path == true の場合相対パスを返し、falseの場合絶対パスを返します
std::string GetSaveFileNameWithExplorer(bool _use_relative_path = false , const std::string& _default_extension = "");

//カレントディレクトリを返します
std::string GetCurrentDir();

// 引数の絶対パスを相対パスに変換します
void ConvertRelativePath(std::string& _full_path);

// 引数の絶対パスを相対パスに変換した文字列を返します
std::string GetRelativePath(const std::string& _full_path);

// 引数のパスからファイル名のみを抽出して返します
// _is_hold_extension == true の場合ファイル名の拡張子を保持します
inline std::string GetExtractedFileNameFromPath(const std::string& _path, const bool& _is_hold_extension = true)
{
	std::string	result;
				result = _path;
				result.erase(0, result.find_last_of('\\') + 1); // パス部分を削除 (0から find_last_of('\\')だと find_last_of('\\')未満の要素を削除する事になるので+1する)
	if (_is_hold_extension)
	{
		return	result;
	}
	else // 拡張子(.txt等) 部分を削除
	{
				result.erase(result.find_last_of('.'), result.size());
		return	result;
	}
}

#pragma endregion

#endif // !_FUNCTION_H_
