#pragma once
#include "Singleton.h"
//#include "EffectManager.h"
#include <memory>
#include <string>
#include <vector>
#include <map>

#include <cereal\cereal.hpp>
#include <cereal\archives\binary.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\archives\xml.hpp>
#include <cereal\types\deque.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\memory.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\types\string.hpp>

extern const int LOAD_EMIT_DATASET;

//struct EffectParams;
//struct EmitterParams;
//struct EMIT_DATA;
struct EMIT_DATASET;
//struct ANIM_EMIT_DATA;
struct ANIM_EMIT_DATASET;

class Data final : public Singleton<Data>
{
public:
	enum eImagesEfc : int
	{
		EFC_TEST = 0,
		EFC_END,
	};
	enum eImagesUI : int
	{
		UI_END,
	};
	enum eImagesOther : int
	{
		OTHER_END,
	};
	enum eFont : int
	{
		FONT_END,
	};
public:
	Data();
	~Data() 
	{
		ReleaseDataAll();
	}
	void Init();

	void LoadGraphEfc(const std::string& _dir, const std::string& _filename);
	void LoadGraphUI(const std::string& _dir, const std::string& _filename);
	void LoadGraphOther(const std::string& _dir, const std::string& _filename);
	void LoadFont(const std::string& _dir, const std::string& _filename);

	void LoadEmitDataSet(const std::string& _dir, const std::string& _filename);
	void LoadAnimEmitDataSet(const std::string& _dir, const std::string& _filename);

	void ReleaseImageDataAll();
	void ReleaseFontDataAll();
	void ReleaseEmitDatasetsAll();
	void ReleaseDataAll();

	const int& GetEfcRef(const std::string& _path)const
	{
		return images_efc.at(_path);
	}
	const int& GetEfcRefSafe(const std::string& _path)const
	{
		auto it_find = images_efc.find(_path);
		if (it_find == images_efc.end()) 
			return -1;
		return images_efc.at(_path);
	}

	const int& GetUIRef		(const std::string& _path)const { return images_ui.at(_path); }

	const int& GetOtherRef	(const std::string& _path)const { return images_other.at(_path); }

	const int& GetFontRef	(const std::string& _path)const { return fonts.at(_path); }

	const std::shared_ptr<EMIT_DATASET>&		GetEmitDataset		(const std::string& _filename)const { return emit_datasets.at(_filename); }
	const std::shared_ptr<ANIM_EMIT_DATASET>&	GetAnimEmitDataset	(const std::string& _filename)const { return anim_emit_datasets.at(_filename); }

	const std::map<std::string, int>& images_other_ref()const { return images_other; }
	const std::map<std::string, int>& images_ui_ref()	const { return images_ui; }
	const std::map<std::string, int>& images_efc_ref()	const { return images_efc; }
	const std::map<std::string, int>& fonts_ref()		const { return fonts; }

private:
	std::map< std::string, int > images_other;
	std::map< std::string, int > images_ui;
	std::map< std::string, int > images_efc;
	std::map< std::string, int > fonts;

	std::map< std::string, std::shared_ptr<EMIT_DATASET>>		emit_datasets;
	std::map< std::string, std::shared_ptr<ANIM_EMIT_DATASET>>	anim_emit_datasets;
};

#define EFCDATA (Data::GetIns())
