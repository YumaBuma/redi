#include "Elevator.h"
#include "Input.h"
#include "Camera.h"
#include "Mo2_BG.h"
#include "Texture.h"

Elevator::Elevator()
{

}

Elevator::Elevator(float x, float y)
{
	pos.x = x;
	pos.y = y;
}

Elevator::~Elevator()
{

}


//TODO mapchipによる出現位置の設定

void Elevator::Init()
{
	pos.x = 1020;
	pos.y = 1920;
	delta = { 0,0 };
	MAX_SPEED.x = 10.0f;
	MAX_SPEED.y = 10.0f;
	INITIAL_SPEED.x = 1.f;
	INITIAL_SPEED.y = 1.5f;

	isErase = false;
	state = 1;
	isActive = false;
	onPlayer[0] = false;
	onPlayer[1] = false;
	isGrip = false;
}

bool Elevator::Update()
{
	size = { tex_size.x*1.0f,tex_size.y*0.3f };

	onPlayer[0] = get_PASS_DATA();

	Switching();

	Move();

	PosUpdateY();

	if (state == 1)
	{
		if (isFloor(pos.x, pos.y, size.x*0.5f))
		{
			mapHoseiDown(this);
			speed.y = 0;

		}

	}



	onPlayer[1] = onPlayer[0];
	return 1;
}

void Elevator::Switching()
{

	if (isActive)
	{
		isActive = false;
		state *= -1;
	}
	if (onPlayer[0])
	{
		if (key[KEY_INPUT_E] == 1)
			state *= -1;
	}
	//if (isFloor(pos.x, pos.y, size.x*0.5f))
	//{
	//	state = 1;
	//	isActive = false;
	//}
}

void Elevator::Move()
{
	switch (state)
	{
	case 1:
		speed.y += INITIAL_SPEED.y;
		break;
	case -1:
		speed.y -= INITIAL_SPEED.y;
		break;
	case 0:
		speed.y = 0;
		break;
	}

	clamp(speed.y, -MAX_SPEED.y, MAX_SPEED.y);
}

void Elevator::Draw() const
{
	if (isGrip)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	for (int i = Texture::OBJECT; i < Texture::OBJECT3; i++)
	{
		if (textureNum != i - Texture::OBJECT)
			continue;
		if (animParam.size())
		{
			DrawRectRotaGraphFast2
			(
				cam.xi_ext(pos.x - tex_size.x * 0.5f)/* - camera_pos.x*/,
				cam.yi_ext(pos.y - tex_size.y)/* - camera_pos.y*/,
				tex_pos.x + tex_size.x * aFrame,
				tex_pos.y + tex_size.y * now_anim,
				tex_size.x, tex_size.y,
				0, 0,
				cam.chipextrate(),
				0.f,
				pTexture->getGraphs(i),
				TRUE, isFlipX
			);
		}
		else
			DrawRectRotaGraphFast2(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y),
				tex_pos.x, tex_pos.y, tex_size.x, tex_size.y,
				0, 0, cam.chipextrate(), 0.f, pTexture->getGraphs(i), TRUE, isFlipX);

	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

void Elevator::ImGui()
{

}
