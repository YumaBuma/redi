#pragma once
#include "scene.h"
#include "EaseMove.h"

class Over : public BaseScene
{
public:
	bool isSelect;
	int select;
	int fontsize;
	bool isincliment;
	int temp_screen;
	int fade_alpha;
	bool scene_change;

	int pl_death_state = 0;
	int pl_death_counter;

	DelayMover game_over[8];
	DelayMover continue_alpha;

	Over();
	~Over();
	void Init();
	void Update();
	void Draw();
	void ImGui();
};
