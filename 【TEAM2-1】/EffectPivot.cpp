#include "EffectPivot.h"
#include "Input.h"
#include "Camera.h"

#include "Function.h"
#include "FunctionDxLib.h"

using namespace DirectX::SimpleMath;

void PositionPivot::BeginMove()
{
	if ((is_over_on_axis_x | is_over_on_axis_y | is_over_on_pivot_origin) == false) return;
	if (pMOUSE->left_ref().pressing_count == 1)
	{
		pos_log = pos;
		is_moving = true;
	}
}

void PositionPivot::Update()
{
	BeginMove();
	Vector2 cursor_pos = cam.GetWorldPosFromCursorPos();
	pos_old = pos;

	if (is_moving)
	{
		if (is_over_on_pivot_origin)pos		= cursor_pos;
		if (is_over_on_axis_x)		pos.x	= cursor_pos.x - axis_length.x;
		if (is_over_on_axis_y)		pos.y	= cursor_pos.y - axis_length.y;

		if (pMOUSE->left_ref().is_release)
		{
			is_moving = false;
		}
	}
	else
	{
		is_over_on_pivot_origin = Coll::Circle(pos								, handle_radius, cursor_pos, 1.f);
		is_over_on_axis_x		= Coll::Circle(pos + Vector2(axis_length.x, 0.f), handle_radius, cursor_pos, 1.f);
		is_over_on_axis_y		= Coll::Circle(pos + Vector2(0.f, axis_length.y), handle_radius, cursor_pos, 1.f);
	}
}

void PositionPivot::Draw()
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 164);
	{
		DrawLineCam(pos, pos + Vector2(axis_length.x - handle_radius, 0.f), GetColor(247, 64, 64), 3);
		DrawLineCam(pos, pos + Vector2(0.f, axis_length.y - handle_radius), GetColor(64, 247, 64), 3);

		DrawCircleCam(pos								, handle_radius, GetColor(232, 232, 90), is_over_on_pivot_origin);
		DrawCircleCam(pos + Vector2(axis_length.x, 0.f)	, handle_radius, GetColor(247, 64, 64), is_over_on_axis_x);
		DrawCircleCam(pos + Vector2(0.f, axis_length.y)	, handle_radius, GetColor(64, 247, 64), is_over_on_axis_y);

		if (is_moving)
			DrawCircleCam(pos_log, 2, GetColor(147, 147, 147), 0, 2);
	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

void VelocityPivot::BeginMove()
{
	//if (is_over_on_pivot_origin == false) return;
	if (pMOUSE->left_ref().pressing_count == 1)
	{
		if(is_over_on_pivot_origin)
		{
			pos_log = pos;
			is_moving = true;
		}
		else if (is_over_on_vel_handle)
		{
			pos_log = pos;
			axis_length_log = axis_length_pos;
			is_moving = true;
		}
	}
}

void VelocityPivot::Update()
{
	BeginMove();
	Vector2 cursor_pos = cam.GetWorldPosFromCursorPos();

	if (is_moving)
	{
		if (is_over_on_pivot_origin)pos				= cursor_pos;
		if (is_over_on_vel_handle)	axis_length_pos = cursor_pos;

		if (pMOUSE->left_ref().is_release)
		{
			is_moving = false;
		}
	}
	else
	{
		is_over_on_pivot_origin = Coll::Circle(pos				, handle_radius, cursor_pos, 1.f);
		is_over_on_vel_handle	= Coll::Circle(pos + axis_length, vel_handle_radius, cursor_pos, 1.f);
	}

	if (pMOUSE->right_ref().pressing_count)
		vel_randomize_coefficient	+= 0.001f * scasf(pMOUSE->pos_x_delta_ref());

	if (pMOUSE->middle_ref().pressing_count)
		angle_randomize				+= 0.5f * scasf(pMOUSE->pos_x_delta_ref());

	Vector2 delta = GetDelta();
	axis_length_pos += delta;
	//printfDx("%f, %f", delta.x, delta.y);
	//printfDx("%f, %f", axis_length_pos.x , axis_length_pos.y);
	axis_length = axis_length_pos - pos;
	CalcAngle();

	pos_old = pos;
}

void VelocityPivot::CalcAngle()
{
	angle = GetDegree(atan2f(axis_length.y, axis_length.x));
}

const DirectX::SimpleMath::Vector2 VelocityPivot::GetVelocity(const bool& _is_angle_randomize, const bool& _is_vel_randomize)
{
	float	random_angle = scasf(_is_angle_randomize) * GetRandSigned(angle_randomize);

	float	random_vel	= scasf(_is_vel_randomize) * GetRand(vel_randomize_coefficient);
	float	len			= GetLength(axis_length);
			len			-= len * random_vel;

	Vector2 result = {};
			result = GetVec2FromAngle(GetRadian(angle + random_angle)) * len;
			result.x *= vel_coefficient;
			result.y *= vel_coefficient;
	return	result;
}

void VelocityPivot::GetVelocity(const DirectX::SimpleMath::Vector2& _pos, DirectX::SimpleMath::Vector2* _p_vel, const bool& _is_vel_randomize)
{
	float	random_vel = scasf(_is_vel_randomize) * GetRand(vel_randomize_coefficient);
	float	len = GetDistanceAtoB(pos, _pos);
			len -= len * random_vel;
			Vector2 b_sub_a = (_pos /*- cam.GetCamPos()*/) - (pos);
	float angle_tmp = atan2f(b_sub_a.y, b_sub_a.x);

	_p_vel->x = cosf(angle_tmp) * len;
	_p_vel->y = sinf(angle_tmp) * len;
	_p_vel->x *= vel_coefficient * (angle_randomize * 0.02f);
	_p_vel->y *= vel_coefficient * (angle_randomize * 0.02f);
}

void VelocityPivot::Draw()
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 164);
	{
		DrawLineCam(pos, pos + axis_length, GetColor(64, 64, 247), 2);

		DrawCircleCam(pos				, handle_radius, GetColor(232, 232, 90)	, is_over_on_pivot_origin);
		DrawCircleCam(pos + axis_length	, vel_handle_radius, GetColor(64, 64, 247)	, is_over_on_vel_handle);

		if (is_moving)
		{
			DrawCircleCam(pos_log, 2, GetColor(147, 147, 147), 0, 2);
			DrawCircleCam(axis_length_log, 2, GetColor(147, 147, 147), 0, 2);
		}
	}

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 74);
	{
		float	len = GetLength(axis_length);
		Vector2 random_axis = {};

		random_axis.x = cosf(GetRadian(angle + angle_randomize)) * len;
		random_axis.y = sinf(GetRadian(angle + angle_randomize)) * len;
		DrawLineCam(pos, pos + random_axis, GetColor(64, 64, 247), 2);
		/*DrawExtendFormatString*/(CAM_ARGS((pos + random_axis)), 1., 1., GetColor(164, 164, 164), "angle: %.2f", angle_randomize);

		random_axis.x = cosf(GetRadian(angle - angle_randomize)) * len;
		random_axis.y = sinf(GetRadian(angle - angle_randomize)) * len;
		DrawLineCam(pos, pos + random_axis, GetColor(64, 64, 247), 2);
		//DrawExtendFormatString(CAM_ARGS((pos + random_axis)), 1., 1., GetColor(164, 164, 164), "angle: %.2f", -angle_randomize);

		len *= vel_randomize_coefficient;
		random_axis.x = cosf(GetRadian(angle)) * len;
		random_axis.y = sinf(GetRadian(angle)) * len;
		DrawCircleCam(pos + random_axis, handle_radius, GetColor(64, 64, 247), 0);
		//DrawExtendFormatString(CAM_ARGS((pos + random_axis)), 1., 1., GetColor(164, 164, 164), "%.4f, vel: %.2f, %.2f", vel_randomize_coefficient, random_axis.x, random_axis.y);

	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}


