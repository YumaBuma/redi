#include "Missile.h"
#include "EffectManager.h"
#include "Sound.h"
Missile::Missile(int _ATK)
{
	attackPoint = _ATK;
}

void Missile::Init()
{
	size = { 17,2.5 };
	isErase = false;

	missile_efc_root = EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("missile_enm_arrow.bin"), true, pos, isFlipX);
	missile_efc_root->ClearEffects();
	missile_efc_root->is_stop_emit = true;
}

bool Missile::Update()
{
	pos += speed;

	if (isFlipX)
	{
		if (getTerrainAttr(pos.x - size.x / 2, pos.y) == TR_ATTR::ALL_BLOCK)
			isErase = true;
	}
	else
	{
		if (getTerrainAttr(pos.x + size.x / 2, pos.y) == TR_ATTR::ALL_BLOCK)
			isErase = true;
	}

	missile_efc_root->is_stop_emit = false;
	missile_efc_root->root_pos = pos;
	missile_efc_root->is_flip_x = isFlipX;

	return true;
}

void Missile::Draw() const
{
	DrawRectRotaGraphFast2(cam.xi_ext(pos.x), cam.yi_ext(pos.y), 0, 0, 34, 5, 17, 2.5, cam.chipextrate(), angle, pTexture->getGraphs(pTexture->ARROW), true, isFlipX);
}

Vector2 Missile::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		return Vector2(0, 2.5);
		break;

	case 1:
			return size;

		break;

	case 2://xは攻撃力を返す(仮)
		return Vector2(attackPoint, 0);

	}
}


Missile::~Missile()
{
	missile_efc_root->is_stop_emit = true;
	missile_efc_root->SetIsStaticEffect(false);
	EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("missile_enm_arrow_break.bin"), false, pos, isFlipX);
}

rocks::rocks(int _ATK, float _finPos, float _moveSpeed)
{
	speed.y = _moveSpeed;
	attackPoint = _ATK;
	finPos = _finPos;
}

void rocks::Init()
{
	size = { 64,40 };
	tex_size = { 64,64 };
	tex_pos = { (rand() % 6)*64.0f,0 };
	isErase = false;
	alpha = 255;
	state = 0;
	isHit = false;
}

bool rocks::Update()
{

	switch (state)
	{
	case 0:
		pos.y -= speed.y;
		if (pos.y < finPos + 20)
		{
			isHit = true;
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("rock.bin"), false, Vector2(pos.x, pos.y-2.f));
		}
		if (finPos > pos.y)
		{
			pSound->playSE(pSound->SE_BOSS_UP, false);
			pos.y = finPos;
			state++;
		}
		break;

	case 1:
		alpha -= 5;
		pos.y += speed.y/2;
		if (alpha < 240)
			isHit = false;

		if (alpha <= 0)
			state++;
		break;

	case 2:
		isErase = true;
	}

	return true;
}

void rocks::Draw() const
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);

	DrawRectRotaGraphFast2
	(cam.xi_ext(pos.x - tex_size.x * 0.5f), cam.yi_ext(pos.y - tex_size.y), tex_pos.x, tex_pos.y,
		tex_size.x, tex_size.y, 0, 0, cam.chipextrate(),
		0, pTexture->getGraphs(pTexture->ROCKS), true, isFlipX);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

}

Vector2 rocks::getATK(int _val)
{
	switch (_val)
	{
	case 0:
		return Vector2(0, 0);
		break;
	case 3:
		return Vector2(isHit,0);
		break;
	case 2: 
		return Vector2(attackPoint, 0);
	}

	return Vector2();
}

rocks::~rocks()
{
}
