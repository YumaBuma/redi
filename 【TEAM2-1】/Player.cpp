#include "common.h"
#include "Player.h"
#include "Input.h"
#include "Mo2_BG.h"
#include "Texture.h"
#include "Sound.h"
#include "Camera.h"
#include "EffectManager.h"
#include "UI.h"
#include "ObjectManager.h"
#include "GameMenu.h"
#include "Shop.h"
#include "ThrowItem.h"

#include "src\imgui.h"

int Player::personal_counter = 0;

wstring mamamamaamamaamama = L"";
static int DATA;
static int INDEX;

const int WEAPON_TEX_BEGIN = pTexture->PL_LARGESWORD;

//struct ATK_INFO
//{
//	ATK first, second, third;
//};
//


//Weapon Information
ATK atk_info[] =
{
	//LargeSword
	{ 80,1,32,40,{ 0,0 },{ 5,12 },{ 5.0f, 0.0f }, 5,15,{ 80.0f, 35.0f },{ 45.0f,20.0f } },
	{ 120,2,36,40,{ 0,0 },{ 3, 8 },{ 9.0f, 0.0f }, 5,15,{ 80.0f,120.0f },{ 45.0f, 0.0f } },
	{ 150,3,40,10,{ 0,0 },{ 10,0 },{ 5.0f, 0.0f }, 10,20,{ 78.0f,116.0f },{ 45.0f, 0.0f } },
	//
	{ 70,1,25,40,{ 10,0 },{ 12,12 },{ 6.5f, 0.0f }, 5,20,{ 85.0f, 25.0f },{ 45.0f,20.0f } },
	{ 90,2,35,0,{ 10,0 },{ 13, 8 },{ 8.0f, 0.0f }, 10,30,{ 90.0f,25.0f },{ 50.0f, 20.0f } },
	{ 0,3,0,0,{ 0,0 },{ 0,0 },{ 0.0f, 0.0f }, 0,0,{ 0.0f,0.0f },{ 0.0f, 0.0f } },//2ゲキよう
	//
	{ 50,1,22,40,{ 20,0 },{ 22,12 },{ -5.0f, 0.0f }, 10,10,{ 0.0f, 0.0f },{ 0.0f,0.0f } },
	{ 60,2,25,40,{ 23,0 },{ 25, 8 },{ -7.0f, 0.0f }, 10,10,{ 0.0f, 0.0f },{ 0.0f, 0.0f } },
	{ 70,3,40,10,{ 35,0 },{ 37,0 },{ -15.0f, 0.0f }, 30,30,{ 0.0f, 0.0f },{ 0.0f, 0.0f } },
	//
	{ 400,1,100,40,{ 40,0 },{ 45,12 },{ 5.0f, 0.0f },40,100,{ 65.0f, 70.0f },{ 40.0f,0.0f } },
	{ 0,2,0,0,{ 0,0 },{ 0,0 },{ 0.0f, 0.0f }, 0,0,{ 0.0f,0.0f },{ 0.0f, 0.0f } },//1ゲキよう
	{ 0,3,0,0,{ 0,0 },{ 0,0 },{ 0.0f, 0.0f }, 0,0,{ 0.0f,0.0f },{ 0.0f, 0.0f } },//1ゲキよう
	//
	{ 40,1,30,40,{ 18,0 },{ 20,20 },{ 4.0f, 0.0f }, 15,15,{ 0.0f, 0.0f },{ 0.0f,0.0f } },
	{ 0,2,0,0,{ 0,0 },{ 0,0 },{ 0.0f, 0.0f }, 0,0,{ 0.0f,0.0f },{ 0.0f, 0.0f } },//1ゲキよう
	{ 0,3,0,0,{ 0,0 },{ 0,0 },{ 0.0f, 0.0f }, 0,0,{ 0.0f,0.0f },{ 0.0f, 0.0f } },//1ゲキよう

};//Weapon Information


Player::Player()
{
	hp = max_hp;
	id.CLASS = PLAYER;
	id.PERSONAL = personal_counter;
	personal_counter++;
	isErase = false;
	//Init();
}

void Player::Init()
{
	//ZeroMemory(this, sizeof(Player));

	max_hp = INIT_MAX_PL_HP;
	hp = max_hp;

	pos.x = 140;
	pos.y = 1800;
	delta = { 0,0 };
	MAX_SPEED.x = 6.0f;
	MAX_SPEED.y = 10.0f;
	INITIAL_SPEED.x = 1.f;
	INITIAL_SPEED.y = 1.5f;
	MAX_JTIMER = 9;
	JUMP_SPEED_Y = 9.0f;
	down_jump = 0;
	MAX_DOWNJTIMER = 5;
	GRAVITY = 0.9f;

	jumpCnt = 0;
	MAX_JCOUNT = 1;
	MAX_AIRJTIMER = 5;

	//bag.resize(40);

	// weapon = 0;
	// atk.cnt = 0;
	// atk.time = 0;
	// atk.next = 0;
	// atk.size = { 0,0 };

	isDamage = false;
	noHitTime = 0;
	NO_MOVE = NODAMAGE - NOMOVE;
	NO_DAMAGE = NODAMAGE - NOMOVE * 2;
	Knck_SPD = { 6.0f, 4.0f };
	
	step = 0;
	MAX_STEP = 20;
	STEP_MARGIN = 10;
	STEP_SPEED_X = 8.f;

	mode_ladder = false;
	isGrip = false;

	size.x = 20;
	size.y = 60;

	system_saved = 0;

	isFlipX = false;
	onGround = false;

	debug_box = true;

	weaponX = Gmenu.sendEquip(true);
	weaponY = Gmenu.sendEquip(false);
	ui.ui_equipX = weaponX;
	ui.ui_equipY = weaponY;

	money = Gmenu.pl_money;
	ui.tmp_MAX_MONEY = money;

	//ui.tmp_MAX_MONEY = money;

	//loadAnimFile("LargeSword");
	loadAnimFile("player");


	//for (int i = 0; i < ARRAYSIZE(atk_info); i++)
	{
		pl_atk_anim.emplace_back(Animation());
		pl_atk_anim.back().loadAnimFile("P_LargeSword");
		pl_atk_anim.emplace_back(Animation());
		pl_atk_anim.back().loadAnimFile("P_Spear");
		pl_atk_anim.emplace_back(Animation());
		pl_atk_anim.back().loadAnimFile("P_Bow");
		pl_atk_anim.emplace_back(Animation());
		pl_atk_anim.back().loadAnimFile("P_Hammer");
		pl_atk_anim.emplace_back(Animation());
		pl_atk_anim.back().loadAnimFile("P_Tomahawk");
	}
	//LoadSystemData();


	// effect
	{
		Animation anim;
		switch (/*eAnmEfcsEye*/0)// eye
		{
		case eAnmEfcsEye::EYEANM_P_LargeSword:
		{
			anim = Animation();
			anim.loadAnimFile("P_LargeSword");
			anmefc_eye_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_LargeSword_eye.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_P_Spear:
		{
			anim = Animation();
			anim.loadAnimFile("P_Spear");
			anmefc_eye_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Spear_eye.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_P_Bow:
		{
			anim = Animation();
			anim.loadAnimFile("P_Bow");
			anmefc_eye_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Bow_eye.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_P_Hummer:
		{
			anim = Animation();
			anim.loadAnimFile("P_Hammer");
			anmefc_eye_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Hummer_eye.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_P_Tomahawk:
		{
			anim = Animation();
			anim.loadAnimFile("P_Tomahawk");
			anmefc_eye_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Tomahawk_eye.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_player:
		{
			anim = Animation();
			anim.loadAnimFile("player");
			anmefc_eye_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("player.bin"), anim, /*is_static_effect =*/ true));
		}

		default:
			break;
		}
		switch (/*eAnmEfcsAtk*/0)// atk
		{
		case eAnmEfcsAtk::ATKANM_P_LargeSword:
		{
			anim = Animation();
			anim.loadAnimFile("P_LargeSword");
			anmefc_atk_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_LargeSword_atk.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsAtk::ATKANM_P_Spear:
		{
			anim = Animation();
			anim.loadAnimFile("P_Spear");
			anmefc_atk_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Spear_atk.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsAtk::ATKANM_P_Bow:
		{
			anim = Animation();
			anim.loadAnimFile("P_Bow");
			anmefc_atk_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Bow_atk.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_P_Hummer:
		{
			anim = Animation();
			anim.loadAnimFile("P_Hammer");
			anmefc_atk_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Hummer_atk.bin"), anim, /*is_static_effect =*/ true));
		}
		case eAnmEfcsEye::EYEANM_P_Tomahawk:
		{
			anim = Animation();
			anim.loadAnimFile("P_Tomahawk");
			anmefc_atk_roots.emplace_back(EFC_MGR->GetEfcRoot(EFCDATA->GetAnimEmitDataset("P_Tomahawk_atk.bin"), anim, /*is_static_effect =*/ true));
		}

		default:
			break;
		}
	}
}

bool Player::Update()
{
	if (bossProd && step == 0)
	{
		Gravity();
		PosUpdateY();

		INDEX = getData(pBG->getMap(), pos.x, pos.y);
		DATA = getTerrainAttr(pos.x, pos.y);
		checkAreaY();
		step = 0;
		speed.x = 0;
		return true;
	}

	if (!m_isVisible)return 1;

	if (Iwin_flg.objBG || Iwin_flg.enemy) return true;

	if (Gmenu.sendFlg)
	{
		weaponX = Gmenu.sendEquip(true);
		weaponY = Gmenu.sendEquip(false);
		ui.ui_equipX = weaponX;
		ui.ui_equipY = weaponY;
		Gmenu.sendFlg = false;
	}


	if (key[KEY_INPUT_P] == 1)mode_ladder = false;
	//if (key[KEY_INPUT_L] == 1)return 0;

	Ladder();

	//ジャンプ処理
	Jump();

	Gravity();

	knockBack();
	{
		static int tmpcnt = 0;
		if (noHitTime > 0)
		{
			tmpcnt++;
			if (tmpcnt > 10)
			{
				tmpcnt = 0;
				if (alpha == 255)
				{
					alpha = 128;
				}
				else
				{
					alpha = 255;
				}
			}
		}
		else
		{
			tmpcnt = 0;
			alpha = 255;
		}
	}

	PosUpdateY();

	INDEX = getData(pBG->getMap(), pos.x, pos.y);
	DATA = getTerrainAttr(pos.x, pos.y);
	checkAreaY();

	//移動処理
	Move();

	//ステップ処理
	Step();

	PosUpdateX();

	checkAreaX();

	//攻撃処理
	Attack();

	RectUpdate();
	//DrawUpdate();

	return 1;
}

void Player::Draw() const
{
	float xf = 0.f;
	float yf = 0.f;

	// player
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
		if (!atk.time)
		{
			xf = cam.xf_ext(pos.x - tex_size.x * 0.5f);
			yf = cam.yf_ext(pos.y - tex_size.y);

			DrawRectRotaGraph2
				(
					scasi(xf),
					scasi(yf),
					scasi(tex_pos.x) + tex_size.x*aFrame,
					scasi(tex_pos.y) + tex_size.y*now_anim,
					tex_size.x,
					tex_size.y,
					0, 0,
					cam.chipextrate_d(),
					0.0,
					pTexture->getGraphs(Texture::PLAYER),
					TRUE, isFlipX
					);
		}
		else
		{
			{
				xf = cam.xf_ext(pos.x - pl_atk_anim[weapon.id].tex_size.x * 0.5f);
				yf = cam.yf_ext(pos.y - pl_atk_anim[weapon.id].tex_size.y);
				DrawRectRotaGraph2
					(
						scasi(xf),
						scasi(yf),
						scasi(pl_atk_anim[weapon.id].tex_pos.x) + pl_atk_anim[weapon.id].tex_size.x*pl_atk_anim[weapon.id].aFrame,
						scasi(pl_atk_anim[weapon.id].tex_pos.y) + pl_atk_anim[weapon.id].tex_size.y*pl_atk_anim[weapon.id].now_anim,
						pl_atk_anim[weapon.id].tex_size.x,
						pl_atk_anim[weapon.id].tex_size.y,
						0, 0,
						cam.chipextrate_d(),
						0.0,
						pTexture->getGraphs(WEAPON_TEX_BEGIN + weapon.id),
						TRUE, isFlipX
						);

			}

		}


		{
			for (auto& it : anmefc_eye_roots)
			{
				it->Draw();
			}
			for (auto& it : anmefc_atk_roots)
			{
				it->Draw();
			}
		}
	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, alpha);

	if (!show_debug || !debug_box)return;
	// box
	{
		float dest_xf = cam.xf_ext(pos.x + tex_size.x * 0.5f);
		float dest_yf = cam.yf_ext(pos.y);

		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 160);
		DrawBox// tex_size
		(
			scasi(xf),
			scasi(yf),
			scasi(dest_xf),
			scasi(dest_yf),
			GetColor(60, 255, 60), FALSE
		);

		DrawBox//atk_box_size
			(
				cam.xi_ext(pos.x - atk.size.x*0.5f + atk.margin.x)/* - camera_pos.x*/,
				cam.yi_ext(pos.y - atk.size.y - atk.margin.y)/* - camera_pos.y*/,
				cam.xi_ext(pos.x + atk.size.x*0.5f + atk.margin.x)/* - camera_pos.x*/,
				cam.yi_ext(pos.y - atk.margin.y) /*- camera_pos.y*/,
				GetColor(255, 0, 0), _PASS_DATA);

		//DrawBox//atk_box_size
		//	(
		//		cam.xi_ext(pos.x - atkY.size.x*0.5f + atkY.margin.x)/* - camera_pos.x*/,
		//		cam.yi_ext(pos.y - atkY.size.y - atkY.margin.y)/* - camera_pos.y*/,
		//		cam.xi_ext(pos.x + atkY.size.x*0.5f + atkY.margin.x)/* - camera_pos.x*/,
		//		cam.yi_ext(pos.y - atkY.margin.y) /*- camera_pos.y*/,
		//		GetColor(255, 0, 0), _PASS_DATA);

		DrawBox//chara_size
			(
				cam.xi_ext(pos.x - size.x*0.5f)/* - camera_pos.x*/,
				cam.yi_ext(pos.y - size.y)/* - camera_pos.y*/,
				cam.xi_ext(pos.x + size.x*0.5f)/* - camera_pos.x*/,
				cam.yi_ext(pos.y) /*- camera_pos.y*/,
				GetColor(255, 255, 0), isDamage);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 160);

		setString({ cam.xi_ext(pos.x - size.x * 0.5f), cam.yi_ext(pos.y - size.y) }, mamamamaamamaamama.c_str());
		//DrawString(cam.xi_ext(pos.x - size.x*0.5f), cam.yi_ext(pos.y - size.y), mamamamaamamaamama.c_str(), GetColor(fontcolor[0], fontcolor[1], fontcolor[2]));
		//SetFontSize(10);
		setString({ cam.xi_ext(pos.x - size.x * 0.5f), cam.yi_ext(pos.y - size.y) }, mamamamaamamaamama.c_str());
		//DrawString(cam.xi_ext(pos.x - size.x*0.5f), 20 + cam.yi_ext(pos.y - size.y), mamamamaamamaamama.c_str(), GetColor(fontcolor[0], fontcolor[1], fontcolor[2]), GetColor(fontedgecolor[0], fontedgecolor[1], fontedgecolor[2]));

		//DrawFormatStringF(0, 0, GetColor(255, 255, 255), "index:%d data:%d", INDEX, DATA);

		//if (system_saved)
		//{
		//	DrawFormatString(pos.x, pos.y - size.y, GetColor(system_saved, system_saved, system_saved), "System_data_was_saved");
		//}
	}
}


void Player::Move()
{
	if (step > STEP_MARGIN)return;//
	if (noHitTime > NO_DAMAGE)return;
	if (atk.time)return;

	isCrouch = false;
	if (key[KEY_INPUT_DOWN])
	{
		isCrouch = true;
		speed.x = 0.f;
		return;
	}
	if (mode_ladder)return;

	//移動処理
	if (key[KEY_INPUT_LEFT])
	{
		if (key[KEY_INPUT_LSHIFT])
		{
			if (pSound->checkSE(pSound->SE_PL_WALK) == -1)
			{
				pSound->playSE(pSound->SE_PL_WALK, true);
			}

		}
		else
		{
			if (pSound->checkSE(pSound->SE_PL_RUN) == -1)
			{
				pSound->playSE(pSound->SE_PL_RUN, true);
			}
		}

		if (speed.x > 0)speed.x = 0.f;
		speed.x -= INITIAL_SPEED.x;
		isFlipX = true;
	}
	else if (key[KEY_INPUT_RIGHT])
	{
		if (key[KEY_INPUT_LSHIFT])
		{
			if (pSound->checkSE(pSound->SE_PL_WALK) != 1)
			{
				//pSound->playSE(pSound->SE_PL_WALK, true);
			}
		}
		else
		{
			if (pSound->checkSE(pSound->SE_PL_RUN) != 1)
			{
				//pSound->playSE(pSound->SE_PL_RUN, true);
			}
		}


		if (speed.x < 0)speed.x = 0.f;
		speed.x += INITIAL_SPEED.x;
		isFlipX = false;
	}
	else
	{
		{
			pSound->stopSE(pSound->SE_PL_RUN);
		}

		{
			pSound->stopSE(pSound->SE_PL_WALK);
		}

		speed.x = 0.f;
	}
	if (key[KEY_INPUT_LSHIFT])
	{
		clamp(speed.x, -(MAX_SPEED.x) *0.3f, MAX_SPEED.x*0.3f);
	}
	else
	{
		clamp(speed.x, -MAX_SPEED.x, MAX_SPEED.x);
	}

	// effect
	{
		//if (!atkX.time)
		{
			//for (auto& it : anmefc_eye_roots)
			//	it->anim->isFlipX = isFlipX;
			//for (auto& it : anmefc_atk_roots)
			//	it->anim->isFlipX = isFlipX;
		}
		//else
		//	anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->isFlipX = isFlipX;
	}
}

void Player::Gravity()
{
	if (mode_ladder)return;//be mode_ladder while not Gravity

	//if (atkX.time < atkX.time - 10)return;//a little floating while attackung

	onGround = false;
	if (step > 25)return;
	speed.y += GRAVITY;
	if (speed.y >= MAX_SPEED.y)
	{
		speed.y = MAX_SPEED.y;
	}
}

void Player::Jump()
{
	if (noHitTime > NO_DAMAGE)return;
	if (step > STEP_MARGIN || atk.time)return;
	if (!onGround && !mode_ladder)
	{
		if (jumpCnt < MAX_JCOUNT)
		{
			if (key[KEY_INPUT_SPACE] == 1)
			{
				if (key[KEY_INPUT_DOWN] == 0)
				{
					pSound->playSE(pSound->SE_PL_JUMP, false);
				}
				EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("double_jump.bin"), false, pos, isFlipX);

				jumpTimer = MAX_AIRJTIMER;
				jumpCnt++;
			}
		}
	}
	else if (onGround || mode_ladder)
	{
		if (key[KEY_INPUT_SPACE] == 1)
		{
			if (key[KEY_INPUT_DOWN] == 0)
			{
				pSound->playSE(pSound->SE_PL_JUMP, false);
			}

			if (isCrouch)
			{
				down_jump = MAX_DOWNJTIMER;
				mode_ladder = false;
			}
			else
			{
				jumpTimer = MAX_JTIMER;
				mode_ladder = false;
			}
		}
	}

	if (jumpTimer > 0)
	{
		if (key[KEY_INPUT_SPACE])
		{
			speed.y = -JUMP_SPEED_Y;
			jumpTimer--;
		}
		else
		{
			jumpTimer = 0;
		}
		if (jumpTimer <= 0)
		{
			jumpTimer = 0;
		}
		cam.SetIsLockRStickMove(true);
	}
	else if (onGround || mode_ladder || air_Jump)
		cam.SetIsLockRStickMove(false);

}

void Player::Ladder()
{
	if (!mode_ladder || noHitTime > NO_DAMAGE)return;

	if (noHitTime > NO_DAMAGE)
	{
		mode_ladder = false;
	}
	speed.x = 0.f;
	speed.y = 0.f;
	if (key[KEY_INPUT_UP])
	{
		speed.y = -2.f;
	}
	if (key[KEY_INPUT_DOWN])
	{
		speed.y = 2.f;
	}
}

void Player::Attack()
{
	if (step > STEP_MARGIN)return;
	if (noHitTime > NO_DAMAGE || mode_ladder)
	{
		atk.time = 0;
		atkX.time = 0;
		atkY.time = 0;
		atk.next = 0;
		atkX.next = 0;
		atkY.next = 0;
		atk.cnt = 0;
		atkX.cnt = 0;
		atkY.cnt = 0;
		atk.size = { 0,0 };
		return;
	}

	atk.time--;
	if (atk.time < 0)//X
	{
		atk.size = { 0.0f, 0.0f };
		atk.time = 0;
		atkX.time = 0;
		atkY.time = 0;
		atk.next--;
		if (atk.next < 0)
		{
			atk.next = 0;
			atkX.next = 0;
			atkY.next = 0;
			atk.cnt = 0;
			atkX.cnt = 0;
			atkY.cnt = 0;
			atk.size = { 0,0 };
		}
	}
	if (atk.time)//X
	{
		if (atk.move_begin.x > atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time || atk.move_end.x < atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time)
		{
			speed.x = 0;
		}
		else
		{
			if (isFlipX)
			{
				speed.x = -atk_info[weapon.id * 3 + atk.cnt - 1].SPEED.x;
			}
			else
			{
				speed.x = atk_info[weapon.id * 3 + atk.cnt - 1].SPEED.x;
			}
		}
		if (atk.move_begin.y > atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time || atk.move_end.y < atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time)
		{
			//speed.y = 0;
		}
		else
		{
			if (isFlipX)
			{
				speed.y = -atk_info[weapon.id * 3 + atk.cnt - 1].SPEED.y;
			}
			else
			{
				speed.y = -atk_info[weapon.id * 3 + atk.cnt - 1].SPEED.y;
			}
		}

		if (atk.hitTime_begin > atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time || atk.hitTime_end < atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time)
		{
			atk.size = { 0.f,0.f };
		}
		else
		{
			if (weapon.id == 2 || weapon.id == 4)
			{
				if (weapon.id == 2)
				{
					pObjManager->Add(std::make_shared<ThrowItem>(atk_info[weapon.id * 3 + atk.cnt - 1].power, Vector2(atk.cnt, atk.time), weapon.id / 2 - 1), Object::PL_WEAPON, 10, Object::PL_WEAPON, 0);
					if (isFlipX)
					{
						pObjManager->ObjList[Object::PL_WEAPON].back()->pos = { pos.x, pos.y - 50 };
						pObjManager->ObjList[Object::PL_WEAPON].back()->speed = { -9.0f, 0 };
					}
					else
					{
						pObjManager->ObjList[Object::PL_WEAPON].back()->pos = { pos.x, pos.y - 50 };
						pObjManager->ObjList[Object::PL_WEAPON].back()->speed = { 9.0f, 0 };
					}
					pObjManager->ObjList[Object::PL_WEAPON].back()->size = { 37.0f, 7.0f };
					pObjManager->ObjList[Object::PL_WEAPON].back()->isFlipX = isFlipX;
				}
				else if (weapon.id == 4)
				{
					for (int i = 0; i < 3; i++)
					{
						pObjManager->Add(std::make_shared<ThrowItem>(atk_info[weapon.id * 3 + atk.cnt - 1].power, Vector2(atk.cnt + i, atk.time), weapon.id / 2 - 1), Object::PL_WEAPON, 10, Object::PL_WEAPON, 0);
						if (isFlipX)
						{
							pObjManager->ObjList[Object::PL_WEAPON].back()->pos = { pos.x, pos.y - 50 };
							pObjManager->ObjList[Object::PL_WEAPON].back()->speed = { -(3.0f + 2.0f*i), -5.0f };
							pObjManager->ObjList[Object::PL_WEAPON].back()->angle = 270.0f;
						}
						else
						{
							pObjManager->ObjList[Object::PL_WEAPON].back()->pos = { pos.x, pos.y - 50 };
							pObjManager->ObjList[Object::PL_WEAPON].back()->speed = { (3.0f + 2.0f*i), -5.0f };
							pObjManager->ObjList[Object::PL_WEAPON].back()->angle = 180.0f;
						}
						pObjManager->ObjList[Object::PL_WEAPON].back()->size = { 26.0f, 10 };
						pObjManager->ObjList[Object::PL_WEAPON].back()->isFlipX = isFlipX;
					}
				}
				pSound->playSE(pSound->SE_PL_LSWORD_1ST + weapon.id * 3 /*+ (atk.cnt-1)*/, false);

			}
			else
			{
				atk.size = atk_info[weapon.id * 3 + atk.cnt - 1].size;
				if (atk.hitTime_begin == atk_info[weapon.id * 3 + atk.cnt - 1].time - atk.time)
				{
					pSound->playSE(pSound->SE_PL_LSWORD_1ST + weapon.id * 3/*+ (atk.cnt-1)*/, false);
				}
				
			}
		}

		return;
	}

	if (!atkX.time && atkX.cnt < 3)
	{
		if (key[KEY_INPUT_A] == 1)
		{
			if (weaponX.id == -1)return;
			atkY.cnt = 0;
			atkY.time = 0;
			atkY.next = 0;
			atkX.time = atk_info[weaponX.id * 3 + atkX.cnt].time;
			atkX.next = atk_info[weaponX.id * 3 + atkX.cnt].next;
			atkX.move_begin = atk_info[weaponX.id * 3 + atkX.cnt].move_begin;
			atkX.move_end = atk_info[weaponX.id * 3 + atkX.cnt].move_end;
			atkX.hitTime_begin = atk_info[weaponX.id * 3 + atkX.cnt].hitTime_begin;
			atkX.hitTime_end = atk_info[weaponX.id * 3 + atkX.cnt].hitTime_end;
			atkX.power = atk_info[weaponX.id * 3 + atkX.cnt].power + atk_info[weaponX.id * 3 + atkX.cnt].power *((weaponX.rarity - 1) *(weaponX.rarity - 1))*0.1f;
			if (isFlipX)
			{
				//atkX.size.x = atk_info[weaponX.id * 3 + atkX.cnt].size.x;
				//atkX.size.y = atk_info[weaponX.id * 3 + atkX.cnt].size.y;
				atkX.margin.x = -atk_info[weaponX.id * 3 + atkX.cnt].margin.x;
				atkX.margin.y = atk_info[weaponX.id * 3 + atkX.cnt].margin.y;
			}
			else
			{
				//atkX.size.x = atk_info[weaponX.id * 3 + atkX.cnt].size.x;
				//atkX.size.y = atk_info[weaponX.id * 3 + atkX.cnt].size.y;
				atkX.margin.x = atk_info[weaponX.id * 3 + atkX.cnt].margin.x;
				atkX.margin.y = atk_info[weaponX.id * 3 + atkX.cnt].margin.y;
			}

			atkX.cnt = atk_info[weaponX.id * 3 + atkX.cnt].cnt;

			atk = atkX;
			weapon = weaponX;
			pl_atk_anim[weaponX.id].aCnt = 0;
			pl_atk_anim[weaponX.id].aFrame = 0;
		}
	}	
	if (!atkY.time && atkY.cnt < 3)
	{
		if (key[KEY_INPUT_S] == 1)
		{
			if (weaponY.id == -1)return;
			atkX.cnt = 0;
			atkX.time = 0;
			atkX.next = 0;
			atkY.time = atk_info[weaponY.id * 3 + atkY.cnt].time;
			atkY.next = atk_info[weaponY.id * 3 + atkY.cnt].next;
			atkY.move_begin = atk_info[weaponY.id * 3 + atkY.cnt].move_begin;
			atkY.move_end = atk_info[weaponY.id * 3 + atkY.cnt].move_end;
			atkY.hitTime_begin = atk_info[weaponY.id * 3 + atkY.cnt].hitTime_begin;
			atkY.hitTime_end = atk_info[weaponY.id * 3 + atkY.cnt].hitTime_end;
			atkY.power = atk_info[weaponY.id * 3 + atkY.cnt].power + atk_info[weaponY.id * 3 + atkY.cnt].power*((weaponY.rarity - 1)* (weaponY.rarity - 1))*0.1f;
			if (isFlipX)
			{
				//atkY.size.x = atk_info[weaponY.id * 3 + atkY.cnt].size.x;
				//atkY.size.y = atk_info[weaponY.id * 3 + atkY.cnt].size.y;
				atkY.margin.x = -atk_info[weaponY.id * 3 + atkY.cnt].margin.x;
				atkY.margin.y = atk_info[weaponY.id * 3 + atkY.cnt].margin.y;
			}
			else
			{
				//atkY.size.x = atk_info[weaponY.id * 3 + atkY.cnt].size.x;
				//atkY.size.y = atk_info[weaponY.id * 3 + atkY.cnt].size.y;
				atkY.margin.x = atk_info[weaponY.id * 3 + atkY.cnt].margin.x;
				atkY.margin.y = atk_info[weaponY.id * 3 + atkY.cnt].margin.y;
			}

			atkY.cnt = atk_info[weaponY.id * 3 + atkY.cnt].cnt;

			atk = atkY;
			weapon = weaponY;
			pl_atk_anim[weaponY.id].aCnt = 0;
			pl_atk_anim[weaponY.id].aFrame = 0;
		}
	}
}

void Player::AttackUpdate()
{

}


void Player::Step()
{
	if (noHitTime > NO_DAMAGE)return;
	if (atk.cnt) { if (atk.time > atk_info[weaponX.id * 3 + (atk.cnt - 1)].time - 10)return; }
	if (key[KEY_INPUT_D] == 1 && !step)
	{

		pSound->playSE(pSound->SE_PL_DODGE, false);
		if (key[KEY_INPUT_LEFT])
		{
			isFlipX = true;
		}
		if (key[KEY_INPUT_RIGHT])
		{
			isFlipX = false;
		}
		step = MAX_STEP + 10;
		speed = { 0,0 };
		jumpTimer = 0;
		atk.time = 0;
		atk.size = { 0.0f, 0.0f };
	}

	if (step > 10)
	{

		if (isFlipX)
		{
			speed.x = -STEP_SPEED_X;
		}
		else
		{
			speed.x = STEP_SPEED_X;
		}
	}
	step--;
	if (step < 0)
	{
		step = 0;
	}

}//Step()


void Player::ImGui()
{
	if (!Iwin_flg.pl)return;


	ImGui::SetNextWindowSize(ImVec2(500, _SCREEN_HEIGHT), ImGuiSetCond_Once);
	ImGui::SetNextWindowPos(ImVec2(_SCREEN_WIDTH - 500, 0), ImGuiSetCond_Once);
	ImGui::Begin("Player", &Iwin_flg.pl);

	ImGui::Text("pl_pos.x:%.2f pl_pos.y:%.2f", pos.x, pos.y);
	ImGui::DragFloat("pos_x##Player", &(pos.x), 1.f, 0.f, 0.f, "%.2f");
	ImGui::DragFloat("pos_y##Player", &(pos.y), 1.f, 0.f, 0.f, "%.2f");
	ImGui::NewLine();

	ImGui::Text("Animation##Player");
	ImGui::Text("nowanim:%d aFrame:%d ", now_anim, aFrame);

	ImGui::NewLine();

	ImGui::Text("speed.x:%.2f##Player", speed.x);
	ImGui::DragFloat("Initial_speed_x##Player", &(INITIAL_SPEED.x), 1.f, 0.f, 0.f, "%.2f");
	ImGui::DragFloat("Max_speed_x##Player", &(MAX_SPEED.x), 1.f, 0.f, 0.f, "%.2f");

	ImGui::NewLine();

	ImGui::Text("speed.y:%.2f##Player", speed.y);
	ImGui::DragFloat("Initial_speed_y##Player", &(INITIAL_SPEED.y), 1.f, 0.f, 0.f, "%.2f");
	ImGui::DragFloat("Max_speed_y##Player", &(MAX_SPEED.y), 1.f, 0.f, 0.f, "%.2f");

	ImGui::NewLine();

	ImGui::Text("JumpTimer : %d", jumpTimer);
	ImGui::DragFloat("GRAVITY##player", &GRAVITY);
	ImGui::DragFloat("JUMP_SPEED##Player", &JUMP_SPEED_Y);
	ImGui::DragInt("MAX_JTIMER", &MAX_JTIMER);
	ImGui::InputInt("MAX_DOWN_JUMPTIMER##Player", &MAX_DOWNJTIMER);
	ImGui::Text("JUMP_COUNT : %d", jumpCnt);
	ImGui::InputInt("MAX_JUMP_COUNT##Player", &MAX_JCOUNT);
	ImGui::InputInt("MAX_AIR_JUMPTIMER##Player", &MAX_AIRJTIMER);

	ImGui::NewLine();

	ImGui::Text("Step : %d", step);
	ImGui::DragInt("MAX_STEP##Player", &MAX_STEP);
	ImGui::DragFloat("STEP_SPEED##Player", &STEP_SPEED_X);

	ImGui::NewLine();

	ImGui::Text("chara_SIZE");
	ImGui::DragFloat("size_x##Player", &(size.x), 1.f, 0.f, 0.f, "%.2f");
	ImGui::DragFloat("size_y##Player", &(size.y), 1.f, 0.f, 0.f, "%.2f");

	ImGui::NewLine();

	ImGui::Text("Status##Player");
	ImGui::DragInt("MAX_HP##Player", &max_hp);
	ImGui::DragInt("HP##Player", &hp);

	if (ImGui::Button("MAX_HP >UP<"))
	{
		max_hp *= 1.7f;
		hp *= 1.7f;
	}
	if (ImGui::Button("Damage!!!!!!"))
	{
		if (!isDamage)
		{
			hp -= 500;
			isDamage = true;
			noHitTime = NODAMAGE;
		}
	}
	if (ImGui::Button("Healing?"))
	{
		hp += 1000;
	}

	ImGui::NewLine();

	ui.ImGui();

	ImGui::Text("ATK_POWER : %d", atk.power);

	static bool isATK_DETAIL = false;
	if (ImGui::Button("WEAPON_SET_DETAIL"))
	{
		isATK_DETAIL ^= 1;
	}

	ImGui::NewLine();

	if (ImGui::Button("DEBUG_BOX##Player"))debug_box ^= 1;

	//ImGui::Text("string width:%d height:%d", GetDrawStringWidth(mamamamaamamaamama.c_str(), 10));
	//ImGui::Text("string size:%d", GetFontSize());

	ImGui::Text("BackGround_Color");

	ImGui::ColorPicker4("color##文字フォント", prev_col);
	if (ImGui::Button("moji"))
	{
		fontcolor[0] = scasi(prev_col[0] * 255.f);
		fontcolor[1] = scasi(prev_col[1] * 255.f);
		fontcolor[2] = scasi(prev_col[2] * 255.f);
		fontcolor[3] = scasi(prev_col[3] * 255.f);
	}
	if (ImGui::Button("edge"))
	{
		fontedgecolor[0] = scasi(prev_col[0] * 255.f);
		fontedgecolor[1] = scasi(prev_col[1] * 255.f);
		fontedgecolor[2] = scasi(prev_col[2] * 255.f);
		fontedgecolor[3] = scasi(prev_col[3] * 255.f);
	}

	animImGui("Player");

	//MultianimImGui("Playerjanaiyo");

	if (isATK_DETAIL)
	{
		static int ATK_NUMBER = 0;
		static bool LR_ATK = true;
		static enum
		{
			ATTACK_1ST = 0,
			ATTACK_2ND,
			ATTACK_3RD,
		};

		ImGui::SetNextWindowSize(ImVec2(350, _SCREEN_HEIGHT* 0.65f), ImGuiSetCond_Once);
		ImGui::SetNextWindowPos(ImVec2(_SCREEN_WIDTH - 500 - 350, 0), ImGuiSetCond_Once);
		ImGui::Begin("WEAPON_SET_DETAIL##Player", 0, ImGuiWindowFlags_MenuBar);
		// ImGui::Text("ATK_CNT : %d", atk.cnt);
		// ImGui::Text("ATK_TIME : %d", atk.time);
		// ImGui::Text("NEXT_ATK_TIME : %d", atk.next);

		{
			ImGui::Text("ATK_CNT : %d  ", atk.cnt);
			//ImGui::SameLine();
			//ImGui::Text("ATKY_CNT : %d", atkY.cnt);
			ImGui::Text("ATK_TIME : %d  ", atk.time);
			//ImGui::SameLine();
			//ImGui::Text("ATKY_TIME : %d", atkY.time);
			ImGui::Text("NEXT_ATK_TIME : %d  ", atk.next);
			//ImGui::SameLine();
			//ImGui::Text("NEXT_ATKY_TIME : %d", atkY.next);
		}

		ImGui::NewLine();
		static WEAPON w_tmp;
		ImGui::InputInt("EQUIPX_ID##Player", &weaponX.id);
		ImGui::InputInt("EQUIPY_ID##Player", &weaponY.id);
		ImGui::NewLine();
		ImGui::InputInt("WEAPON_ID##Player", &w_tmp.id);
		ImGui::RadioButton("1st##Player", &ATK_NUMBER, ATTACK_1ST); ImGui::SameLine(); ImGui::RadioButton("2nd##Player", &ATK_NUMBER, ATTACK_2ND); ImGui::SameLine(); ImGui::RadioButton("3rd##Player", &ATK_NUMBER, ATTACK_3RD);

		if (ATK_NUMBER == ATTACK_1ST)
		{

			ImGui::DragInt("1ST_ATK_POWER##Player", &atk_info[w_tmp.id * 3 + 0].power);
			ImGui::DragInt("1ST_ATK_TIME##Player", &atk_info[w_tmp.id * 3 + 0].time);
			ImGui::DragInt("1ST_ATK_CONTINUE_TIME##Player", &atk_info[w_tmp.id * 3 + 0].next);
			ImGui::DragInt("1ST_ATK_MOVE_BEGIN_X##Player", &atk_info[w_tmp.id * 3 + 0].move_begin.x);
			ImGui::DragInt("1ST_ATK_MOVE_BEGIN_Y##Player", &atk_info[w_tmp.id * 3 + 0].move_begin.y);
			ImGui::DragInt("1ST_ATK_MOVE_END_X##Player", &atk_info[w_tmp.id * 3 + 0].move_end.x);
			ImGui::DragInt("1ST_ATK_MOVE_END_Y##Player", &atk_info[w_tmp.id * 3 + 0].move_end.y);
			ImGui::DragFloat("1ST_ATK_SPEED_X##Player", &atk_info[w_tmp.id * 3 + 0].SPEED.x);
			ImGui::DragFloat("1ST_ATK_SPEED_Y##PlayerX", &atk_info[w_tmp.id * 3 + 0].SPEED.y);
			ImGui::DragInt("1ST_HIT_TIME_BEGIN##Player", &atk_info[w_tmp.id * 3 + 0].hitTime_begin);
			ImGui::DragInt("1ST_HIT_TIME_END##Player", &atk_info[w_tmp.id * 3 + 0].hitTime_end);
			ImGui::DragFloat("1ST_ATK_SIZE_X##Player", &atk_info[w_tmp.id * 3 + 0].size.x);
			ImGui::DragFloat("1ST_ATK_SIZE_Y##Player", &atk_info[w_tmp.id * 3 + 0].size.y);
			ImGui::DragFloat("1St_ATK_MARGIN_X##Player", &atk_info[w_tmp.id * 3 + 0].margin.x);
			ImGui::DragFloat("1St_ATK_MARGIN_Y##Player", &atk_info[w_tmp.id * 3 + 0].margin.y);
		}

		if (ATK_NUMBER == ATTACK_2ND)
		{
			ImGui::DragInt("2ND_ATK_POWER##Player", &atk_info[w_tmp.id * 3 + 1].power);
			ImGui::DragInt("2ND_ATK_TIME##Player", &atk_info[w_tmp.id * 3 + 1].time);
			ImGui::DragInt("2ND_ATK_CONTINUE_TIME##Player", &atk_info[w_tmp.id * 3 + 1].next);
			ImGui::DragInt("2ND_ATK_MOVE_BEGIN_X##Player", &atk_info[w_tmp.id * 3 + 1].move_begin.x);
			ImGui::DragInt("2ND_ATK_MOVE_BEGIN_Y##Player", &atk_info[w_tmp.id * 3 + 1].move_begin.y);
			ImGui::DragInt("2ND_ATK_MOVE_END_X##Player", &atk_info[w_tmp.id * 3 + 1].move_end.x);
			ImGui::DragInt("2ND_ATK_MOVE_END_Y##Player", &atk_info[w_tmp.id * 3 + 1].move_end.y);
			ImGui::DragFloat("2ND_ATK_SPEED_X##Player", &atk_info[w_tmp.id * 3 + 1].SPEED.x);
			ImGui::DragFloat("2ND_ATK_SPEED_Y##Player", &atk_info[w_tmp.id * 3 + 1].SPEED.y);
			ImGui::DragInt("2ND_HIT_TIME_BEGIN##Player", &atk_info[w_tmp.id * 3 + 1].hitTime_begin);
			ImGui::DragInt("2ND_HIT_TIME_END##Player", &atk_info[w_tmp.id * 3 + 1].hitTime_end);
			ImGui::DragFloat("2ND_ATK_SIZE_X##Player", &atk_info[w_tmp.id * 3 + 1].size.x);
			ImGui::DragFloat("2ND_ATK_SIZE_Y##Player", &atk_info[w_tmp.id * 3 + 1].size.y);
			ImGui::DragFloat("2ND_ATK_MARGIN_X##Player", &atk_info[w_tmp.id * 3 + 1].margin.x);
			ImGui::DragFloat("2ND_ATK_MARGIN_Y##Player", &atk_info[w_tmp.id * 3 + 1].margin.y);
		}

		if (ATK_NUMBER == ATTACK_3RD)
		{
			ImGui::DragInt("3RD_ATK_POWER##Player", &atk_info[w_tmp.id * 3 + 2].power);
			ImGui::DragInt("3RD_ATK_TIME##Player", &atk_info[w_tmp.id * 3 + 2].time);
			ImGui::DragInt("3RD_ATK_CONTINUE_TIME##Player", &atk_info[w_tmp.id * 3 + 2].next);
			ImGui::DragInt("3RD_ATK_MOVE_BEGIN_X##Player", &atk_info[w_tmp.id * 3 + 2].move_begin.x);
			ImGui::DragInt("3RD_ATK_MOVE_BEGIN_Y##Player", &atk_info[w_tmp.id * 3 + 2].move_begin.y);
			ImGui::DragInt("3RD_ATK_MOVE_END_X##Player", &atk_info[w_tmp.id * 3 + 2].move_end.x);
			ImGui::DragInt("3RD_ATK_MOVE_END_Y##Player", &atk_info[w_tmp.id * 3 + 2].move_end.y);
			ImGui::DragFloat("3RD_ATK_SPEED_X##Player", &atk_info[w_tmp.id * 3 + 2].SPEED.x);
			ImGui::DragFloat("3RD_ATK_SPEED_Y##Player", &atk_info[w_tmp.id * 3 + 2].SPEED.y);
			ImGui::DragInt("3RD_HIT_TIME_BEGIN##Player", &atk_info[w_tmp.id * 3 + 2].hitTime_begin);
			ImGui::DragInt("3RD_HIT_TIME_END##Player", &atk_info[w_tmp.id * 3 + 2].hitTime_end);
			ImGui::DragFloat("3RD_ATK_SIZE_X##Player", &atk_info[w_tmp.id * 3 + 2].size.x);
			ImGui::DragFloat("3RD_ATK_SIZE_Y##Player", &atk_info[w_tmp.id * 3 + 2].size.y);
			ImGui::DragFloat("3RD_ATK_MARGIN_X##Player", &atk_info[w_tmp.id * 3 + 2].margin.x);
			ImGui::DragFloat("3RD_ATK_MARGIN_Y##Player", &atk_info[w_tmp.id * 3 + 2].margin.y);
		}





		ImGui::NewLine();

		{
			string buff = "PL_WEAPON" + w_tmp.id;
			std::vector<Animation>::iterator it = pl_atk_anim.begin() + w_tmp.id;
			it->animImGui(buff);

		}
		ImGui::End();
	}



	//if (ImGui::Button("saveSystemData##Player"))
	//{
	//	SaveSystemData();
	//	system_saved = 200;
	//}
	//system_saved--;
	//if (system_saved < 0)
	//{
	//	system_saved = 0;
	//}

	ImGui::End();
}

void Player::checkAreaX()
{
	//if (delta.x < 0)
	{
		if (isWall(pos.x + size.x*0.5f, pos.y, size.y))
		{
			mapHoseiRight(this);
		}
	}

	//if (delta.x > 0)
	{
		if (isWall(pos.x - size.x*0.5f, pos.y, size.y))
		{
			mapHoseiLeft(this);
		}
	}
}

void Player::checkAreaY()
{
	static bool mode_prev;
	mode_prev = mode_ladder;
	
	if (jumpTimer < MAX_JTIMER - 5)
	{
		if (isLadderUp(pos.x + size.x*0.3f, pos.y, size.y) || isLadderUp(pos.x - size.x*0.3f, pos.y, size.y))
		{
			if (key[KEY_INPUT_UP])
			{
				mode_ladder = true;
			}
		}
		else if (isLadderDown(pos.x + size.x*0.3f, pos.y) || isLadderDown(pos.x - size.x*0.3f, pos.y))
		{
			if (key[KEY_INPUT_DOWN])
			{
				mode_ladder = true;
				pos.y += 20.f;
			}
		}
		else
		{
			mode_ladder = false;
		}
	}


	if (mode_prev != mode_ladder)return;

	if (step != 0 && step <= 11)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x*0.5f))
		{
			step++;
		}
	}

	if (delta.y > 0)
	{
		if (isCeiling(pos.x, pos.y - size.y, size.x * 0.5f))
		{
			mapHoseiUp(this);
			//jumpTimer = 0;
		}

	}

	if (delta.y < 0)
	{
		if (isFloor(pos.x, pos.y, size.x * 0.5f))
		{
			mapHoseiDown(this);
			onGround = true;
			jumpCnt = 0;
			mode_ladder = false;
		}

		if (down_jump < 0)down_jump = 0;
		if (!down_jump)
		{
			if (isUpperFloor(pos.x, pos.y, size.x * 0.5f))
			{
				mapHoseiDown(this);
				onGround = true;
				jumpCnt = 0;
				mode_ladder = false;
			}
		}
		down_jump--;

	}
}

void Player::DrawUpdate()
{
	static int prev;//Inputで作るべき


	if (!atkX.time && !atkY.time)
	{

		for (int i_root = 0; i_root < eAnmEfcsEye::EYEANM_player; i_root++)
		{
			anmefc_eye_roots[i_root]->anim->aCnt	= 0;
			anmefc_eye_roots[i_root]->anim->aFrame	= 0;
			anmefc_eye_roots[i_root]->is_stop_emit	= true;
			anmefc_atk_roots[i_root]->anim->aCnt	= 0;
			anmefc_atk_roots[i_root]->anim->aFrame	= 0;
			anmefc_atk_roots[i_root]->is_stop_emit	= true;
		}
	}
	anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->is_stop_emit = false;

	stop_anim = true;
	anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->stop_anim = true;
	if (bossProd && step == 0)
	{
		changeAnimation(0);
		anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(0);
	}
	else if (noHitTime > NO_MOVE)
	{
		changeAnimation(4);
		anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(4);
	}
	else if (atk.time)
	{
		anmefc_eye_roots[weapon.id]->is_stop_emit = false;
		anmefc_atk_roots[weapon.id]->is_stop_emit = false;
		anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->is_stop_emit = true;

		if (atk.cnt == 1)
		{
			pl_atk_anim[weapon.id].changeAnimation(0);
			anmefc_atk_roots[weapon.id]->anim->changeAnimation(0);
			anmefc_eye_roots[weapon.id]->anim->changeAnimation(0);
		}
		else if (atk.cnt == 2)
		{
			pl_atk_anim[weapon.id].changeAnimation(1);
			anmefc_atk_roots[weapon.id]->anim->changeAnimation(1);
			anmefc_eye_roots[weapon.id]->anim->changeAnimation(1);
		}
		else if (atk.cnt == 3)
		{
			pl_atk_anim[weapon.id].changeAnimation(2);
			anmefc_atk_roots[weapon.id]->anim->changeAnimation(2);
			anmefc_eye_roots[weapon.id]->anim->changeAnimation(2);
		}
	}
	else if (step > STEP_MARGIN)
	{
		changeAnimation(7);
		 anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(7);
	}
	else if (mode_ladder)
	{
		if (key[KEY_INPUT_UP])
		{
			changeAnimation(9);
			 anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(9);
		}
		else if (key[KEY_INPUT_DOWN])
		{
			changeAnimation(8);
			 anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(8);
		}

	}
	else if (!onGround)
	{
		if (delta.y > 0)
		{
			changeAnimation(3);
			anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(3);
		}
		else
		{
			changeAnimation(4);
			anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(4);
		}
	}
	else if (isCrouch)
	{
		changeAnimation(5);
		anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(5);
		prev = 5;
	}
	else if (speed.x)
	{
		if (key[KEY_INPUT_LSHIFT])
		{
			if (pSound->checkSE(pSound->SE_PL_WALK) == -1)
			{
				pSound->playSE(pSound->SE_PL_WALK, true);
			}
			changeAnimation(1);
			anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(1);
		}
		else
		{

			changeAnimation(2);
			anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(2);
		}
	}
	else
	{
		if (prev >= 0)
		{
			changeAnimation(6);
			anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(6);
			prev--;
		}
		else
		{
			changeAnimation(0);
			anmefc_eye_roots[eAnmEfcsEye::EYEANM_player]->anim->changeAnimation(0);
		}
	}



	if (weaponX.id != -1)pl_atk_anim[weaponX.id].playAnimation();
	if (weaponY.id != -1)pl_atk_anim[weaponY.id].playAnimation();
	if (!atk.time)playAnimation();

	//眼光
	{
		float counter = 0.f;
		for (auto& it_eye : anmefc_eye_roots)
		{
			it_eye->is_flip_x = isFlipX;
			it_eye->root_pos = { pos.x + (isFlipX * -1.0f), (pos.y + 1.f) };
			it_eye->Update();
			counter += 1.f;
		}

		for (auto& it_atk : anmefc_atk_roots)
		{
			it_atk->is_flip_x = isFlipX;
			it_atk->root_pos = { pos.x , pos.y };
			it_atk->Update();
		}
	}

	if (weaponX.id == 3 || weaponY.id == 3)// hammer
		if (pl_atk_anim[weapon.id].getaFrame() == 10)
			cam.ShakeCamera(Camera::eShake::Shake_Pl_Hammer);
}

void Player::RectUpdate()
{
	static int prev;//Inputで作るべき

	stop_anim = true;
	if (step > STEP_MARGIN)
	{
		size.x = 20;
		size.y = 35;
	}
	else if (mode_ladder)
	{
		size.x = 20;
		size.y = 60;
	}
	else if (!onGround)
	{
		if (delta.y > 0)//上
		{
			size.x = 20;
			size.y = 60;
		}
		else
		{
			size.x = 20;
			size.y = 60;
		}
	}
	else if (speed.x)
	{
		if (key[KEY_INPUT_LSHIFT])
		{
			size.x = 20;
			size.y = 60;
		}
		else
		{
			size.x = 20;
			size.y = 60;
		}
		prev = -1;
	}
	else if (isCrouch)
	{
		size.x = 20;
		size.y = 35;
		prev = 5;
	}
	else
	{
		if (prev >= 0)
		{
			size.x = 20;
			size.y = 35;
			prev--;
		}
		else
		{
			size.x = 20;
			size.y = 60;
		}
	}

}

//bool Player::SaveSystemData()
//{
//	std::stringstream ss;
//	{
//		//出力時の受け皿
//		std::shared_ptr<Player> pl;
//		pl = std::make_shared<Player>(*this);
//
//		std::stringstream stream;
//		//出力用の型に文字列streamを登録
//		cereal::JSONOutputArchive binaryOutArchive(stream);
//		//文字列ストリームにjsonを投げつけてもらう
//		binaryOutArchive(cereal::make_nvp("PlayerSystemTag", pl));
//		//ファイル出力用ストリーム作成
//		std::ofstream outputFile("./Data/Player/PlayerSystemData.txt", std::ios::binary | std::ios::out);
//		//書き出す
//		outputFile << stream.str();
//		//閉じる
//		outputFile.close();
//		stream.clear();	
//	}
//	return true;
//}
//
//bool Player::LoadSystemData()
//{
//	std::shared_ptr<Player> pl;
//	pl = std::make_shared<Player>(*this);
//
//	//入力される文字列受け皿
//	std::stringstream stream;
//	//ファイル入力ストリーム作成
//	std::ifstream inputFile("./Data/Player/PlayerSystemData.txt", std::ios::binary | std::ios::in);
//	if (inputFile.fail())return false;
//	//入力データを全部文字列streamに投げる
//	stream << inputFile.rdbuf();
//	//jsonをロード
//	cereal::JSONInputArchive binaryInputArchive(stream);
//	//デコードしたデータをvにセット
//	binaryInputArchive(cereal::make_nvp("PlayerSystemTag", pl));
//
//	return true;
//}
