#pragma once
#include "Object.h"

struct Rect
{
	float l, t, r, b;

	Rect() noexcept : Rect(0.f, 0.f, 0.f, 0.f) {}
	 explicit Rect(float x) : Rect(x, x, x, x) {}
	 Rect(float _l, float _t, float _r, float _b) : l(_l), t(_t), r(_r), b(_b){}

};


enum COLLISION_TYPE
{
	RECT_RECT = 0,
	RECT_ROTARECT,
	RECT_CIRCLE,
	CIRCLE_CIRCLE,
};

bool(*Collision[])(shared_ptr<Object>& obj1, Vector2& size1, Vector2& margin1, shared_ptr<Object>& obj2, Vector2& size2, Vector2& margin2);

