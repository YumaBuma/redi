#pragma once

#include "./src/imgui.h"
#include <string>
#include <array>

namespace ImGui
{
	namespace MyImGui
	{
		bool ComboMenu(const std::string& _label, int* _p_selecting_num, const std::string _combo_menu_elems[], const int _array_size);

		template<int _array_size>
		bool ComboMenu(const std::string& _label, int* _p_selecting_num, const std::array<std::string, (size_t)_array_size>& _combo_menu_elems)
		{
			bool is_value_changed = false;
			int current_num = *_p_selecting_num;
			if (ImGui::BeginCombo((_label).c_str(), _combo_menu_elems[current_num].c_str())) // コンボメニューに表示する文字
			{
				for (int num_elem = 0; num_elem < _array_size; num_elem++)
				{
					bool is_selected = (current_num == num_elem);
					if (ImGui::Selectable(_combo_menu_elems[num_elem].c_str(), is_selected))
					{
						*_p_selecting_num = num_elem;
						is_value_changed = true;
					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}

			return is_value_changed;
		}

	}
}
