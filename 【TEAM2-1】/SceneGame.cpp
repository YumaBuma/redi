#include "SceneGame.h"
#include "common.h"
#include "Input.h"
#include "Collision.h"
#include "Object.h"
#include <vector>

#include "Sound.h"
#include "Mo2_BG.h"
#include "Player.h"
#include "Enemy.h"
#include "fade.h"

#include "Elevator.h"
#include "UI.h"
#include "GameMenu.h"
#include "bgObject.h"
#include "Shop.h"

#include "EnemyManager.h"
#include "ObjectManager.h"
#include "bgObjectManager.h"
#include "DrawableList.h"
#include "EffectManager.h"

#include "src\imgui.h"

#include "Camera.h"
#include ".\\Shader\\PostFx.h"

#include "FunctionDxLib.h"

extern bool is_mid_event;
#define PL (pObjManager->ObjList[Object::PLAYER].back())

Game::Game()
{
	//SetDrawValidGraphCreateFlag(false);
	//SetDrawValidFloatTypeGraphCreateFlag(false);
	//SetDrawValidAlphaChannelGraphCreateFlag(true);
	temp_screen		= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
	temp_screen_ui	= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
	fade_alpha = 255;
	loadFin[0] = false;
	loadFin[1] = false;
	bossProd = false;
	finLinear = false;
	finButtle = false;
}

Game::~Game()
{
	DeleteGraph(temp_screen);
	DeleteGraph(temp_screen_ui);
	pObjManager->Uninit();
	//pobjBGManager->unInit();
	EFC_MGR->ClearAllEffects();
	//penmManager->unInit();
	pBG->UnInit();
	POSTFX->UnInit();
}

void Game::Init()
{
	stageNumber++;
	ChangeFont("pixelMpuls10");
	ChangeFontType(DX_FONTTYPE_EDGE);

	pFade->Init();
	pFade->In(60.f);
	penmManager->init();
	pObjManager->ObjList.resize(Object::END);

	// thread
	{
		load[0] = new std::thread(&Game::loadThread1, this, std::ref(loadFin[0]));
		load[1] = new std::thread(&Game::loadThread2, this, std::ref(loadFin[1]));

		load[0]->detach();
		load[1]->detach();
	}

	// カメラとプレイヤー
	{
		cam.Init(stageNumber, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT);
		std::shared_ptr<Player> tmp;

		if (pObjManager->ObjList[Object::PLAYER].empty())
		{
			tmp = make_shared<Player>();
			cam.SetTargetPosPtr(&tmp.get()->pos.x, &tmp.get()->pos.y);
		}
		else
		{

			cam.SetTargetPosPtr(&pObjManager->ObjList[Object::PLAYER].back()->pos.x, &pObjManager->ObjList[Object::PLAYER].back()->pos.y);
		}

		cam.SwitchIsUsePtr(true);
		cam.AlwaysSwingCamera(Camera::eAlSwing::AlSwing_Pl_Life_Low);
		cam.StartAlwaysSwing();
		cam.StopAlwaysSwing();

		if (pObjManager->ObjList[Object::PLAYER].empty())
			pObjManager->Add(std::move(tmp), Object::PLAYER, 10, Object::PLAYER, 1);

		pObjManager->ObjList[Object::PLAYER].back()->hide();

		setStartPos();
		cam.SetPosAtVector2(pObjManager->ObjList[Object::PLAYER].back()->pos);

	}


	if (stageNumber == 1)
	{
		ui.Init();
		Gmenu.Init();
	}
	shop.Init();

	// editor
	{
		a_efc_editor.Init();
		efc_editor.Init();

		prev_col[0] = 53.f / 255.f;
		prev_col[1] = 32.f / 255.f;
		prev_col[2] = 29.f / 255.f;
		prev_col[3] = 1.f;
		bg_col[0] = 0;
		bg_col[1] = 0;
		bg_col[2] = 0;
		bg_col[3] = 255;

		POSTFX->Init((int)sceneManager::GAME);
		pl_dead_state 	 		   = 0;
		pl_dead_timer 	 		   = 310;
		pl_low_hp_offset 		   = 520.f;
		pl_low_hp 		 		   = DelayMover();
		pl_low_hp.val 	 		   = PL->getHP(0);
		pl_low_hp.target_val 	   = PL->getHP(0);
		pl_low_hp.SetMoveSpeed(0.075f);

		pl_low_hp_alpha 		   = DelayMover();
		pl_low_hp_alpha.val		   = 0.f;
		pl_low_hp_alpha.target_val = 156.f;
		pl_low_hp.SetMoveSpeed(0.068f);
	}

	timer = 0;
}

void Game::Update()
{
	timer++;
	switch (stageNumber)
	{
	case 1:
	case 3:
		pSound->fadeinBGM(pSound->BGM_GAME);
		break;
	case 2:
	case 4:
		pSound->fadeinBGM(pSound->BGM_REST);
		break;
	case 5:
		pSound->fadeoutBGM(pSound->BGM_REST);
		if (!finButtle)
		{
			if (!finLinear)
			{

				pSound->fadeinBGM(pSound->BGM_BOSS_WIND, false);
				pSound->fadeinBGM(pSound->BGM_BOSS_BACK, false);
			}
			else
			{
				pSound->fadeinBGM(pSound->BGM_BOSS, true);
			}
		}
		else
		{
			pSound->fadeinBGM(pSound->BGM_BOSS_WIND, true);

		}
		break;
	}

	if (pObjManager->ObjList[Object::PLAYER].empty())
	{
		return;
	}
	if (!loadFin[0] || !loadFin[1])
	{
		return;
	}
	else
	{
		mini_map.Init();
		Gmenu.pmini_map = &mini_map;
	}
	pFade->Update();
	pl_low_hp.Update();
	pl_low_hp_alpha.Update();

	// ゲームオーバー時の処理
	{
		if (pObjManager->ObjList[Object::PLAYER].back()->getHP(0) <= 0 && pObjManager->ObjList[Object::PLAYER].back()->m_isVisible == true)
		{
			pObjManager->ObjList[Object::PLAYER].back()->m_isVisible = false;
			EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("pl_dead.bin"), false, pObjManager->ObjList[Object::PLAYER].back()->pos);
			fade_alpha = 0;
			pl_dead_state++;
		}
		if (pl_dead_state && pl_dead_timer <= 0)
		{
			// todo:
			pObjManager->ObjList[Object::PLAYER].back()->isErase = true;
			sceneManager::isSwitchScene = true;
			sceneManager::next_scene = sceneManager::OVER;
		}
		if (pl_dead_state)
		{
			pl_dead_timer--;
			if (pl_dead_timer == 30)
				pFade->Out(28.f);
		}
	}

	//TODO 0606 GAMECLEAR
	{
		if (BaseScene::stageNumber == 5)
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 3150.0f)
			{
				sceneManager::isSwitchScene = true;
				sceneManager::next_scene = sceneManager::CLEAR;
				pFade->Out(28.f);
			}
		}
	}


	// ミニマップテレポート装置選択画面
	if (mini_map.IsSelectedTelepoDestination())
	{
		// todo: ここでテレポート先の座標を受け取ってください
		pObjManager->ObjList[Object::PLAYER].back()->pos = mini_map.telepo_destination_pos.GetTargetPosAtVector2();
		cam.SetTargetPosAtVector2(mini_map.telepo_destination_pos.GetTargetPosAtVector2());
		cam.SetPosAtVector2(cam.GetTargetPosAtVector2());
		pSound->playSE(pSound->SE_OBJ_TEREPORTER, false);
		pFade->In(120.f, GetColor(238, 236, 210));
	}

	mini_map.Update();
	if (mini_map.IsSelectingTelepoDestination()) return;

	shop.Update();

	if (!shop.isOpen)Gmenu.Update();
	ui.Update();

	if (Gmenu.isOpen || shop.isOpen)return;

	if (pObjManager->ObjList[Object::PLAYER].back()->getHP(0) < SCASI(pl_low_hp_offset * 0.5f))
	{
		cam.AlwaysSwingCamera(Camera::eAlSwing::AlSwing_Pl_Life_Low);
		cam.StartAlwaysSwing();
	}
	else
	{
		//cam.AlwaysSwingCamera(Camera::eAlSwing::AlSwing_Pl_Life_Low);
		cam.StopAlwaysSwing();
	}

	cam.Update();

	//Debug
	if (key[KEY_INPUT_LCONTROL] && key[KEY_INPUT_0] == 1)
	{
		show_debug ^= 1;
	}

	if (Iwin_flg.efc || Iwin_flg.anmefc)
	{
		if (Iwin_flg.efc)
		{
			efc_editor.Update();
		}
		else if (Iwin_flg.anmefc)
		{
			a_efc_editor.Update();
		}
	}
	else
	{
		pBG->Update();

		pObjManager->Update();

		if (pObjManager->ObjList[Object::PLAYER].empty() == true)
			return;

		pObjManager->ObjList[Object::PLAYER].back()->_PASS_DATA = false;

		if (pObjManager->ObjList[Object::PLAYER].back()->getHP(0) <= 0)
		{
			//pFade->Out(100.f);
			if (pFade->IsUpdateCompleted() && pl_dead_timer < 0)
			{
				pObjManager->ObjList[Object::PLAYER].back()->isErase = true;
				sceneManager::isSwitchScene = true;
				sceneManager::next_scene = sceneManager::OVER;
				return;
			}
		}
		else
		{
			//pFade->In(120.f);
		}

		colUpdate();
	}


	EFC_MGR->Update();

	if (Iwin_flg.objBG)
		pobjBGManager->update();
	else if (Iwin_flg.enemy)
		penmManager->update();
	else
		cam.SwitchIsUsePtr(true);

	pObjManager->DrawUpdate();

	POSTFX->Update();

	if (key[KEY_INPUT_O] == 1)
	{
		pSound->playSE(pSound->SE_E_DEATH, false);

		sceneManager::isSwitchScene = true;
		if (stageNumber != 5)
		{
			sceneManager::next_scene = sceneManager::GAME;
		}
		else
		{
			sceneManager::next_scene = sceneManager::CLEAR;
		}
	}
}

void Game::Draw()
{
	if (pObjManager->ObjList[Object::PLAYER].empty())return;
	//仮画面に描画
	SetDrawScreen(temp_screen);
	ClearDrawScreen();
	SetDrawMode(DX_DRAWMODE_NEAREST);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, bg_col[3]);
	DrawBox(0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, GetColor(bg_col[0], bg_col[1], bg_col[2]), 1);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);

	if (!loadFin[0] || !loadFin[1])
	{
	}
	else
	{
		if (Iwin_flg.efc || Iwin_flg.anmefc)
		{
			if (Iwin_flg.efc)
			{
				pBG->Draw();
				efc_editor.Draw();
			}
			else if (Iwin_flg.anmefc)
			{
				pBG->Draw();
				a_efc_editor.Draw();
			}
		}
		else
		{
			pBG->DrawBg();

			pDrawList->DrawBehindChip();

			pBG->DrawRader();

			pObjManager->Draw();

			pBG->Draw();

			pDrawList->DrawFront();

			EFC_MGR->Draw();

			pBG->drawFog();

			POSTFX->Render(&temp_screen);

			if (Iwin_flg.enemy)
			{
				penmManager->draw();
				penmManager->Debug();
			}

			if (Iwin_flg.objBG)
			{
				pobjBGManager->draw();
				pobjBGManager->Debug();
			}
			//printfDx("ステージ番号 : %d\n", stageNumber);

			cam.Draw();

			mini_map.DrawComposite();

		}
	}

	//仮画面を裏画面に描画
	SetDrawScreen(DX_SCREEN_BACK);
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		DrawRectRotaGraph2
		(
			SCREEN_CX - cam.zxi(), SCREEN_CY - cam.zyi(),
			0, 0,
			TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
			TEMP_SCREEN_CX, TEMP_SCREEN_CY,
			(double)TEMP_SCREEN_EXT_RATE_MAX + cam.zoom(),
			GetRadian(cam.angle()),
			temp_screen, 1
		);

		POSTFX->Draw();

		
		// hp_low
		{
			SetDrawScreen(temp_screen_ui);
			ClearDrawScreen();
			{
				pl_low_hp = 255.f;
				float pl_hp = SCASF(pObjManager->ObjList[Object::PLAYER].back()->getHP(0));
				if (pl_hp <= pl_low_hp_offset)
				{
					pl_low_hp = (std::max)(Fit(pl_hp, pl_low_hp_offset, 255.f, 0.f), 42.f);
				}
				GraphFilterBlt(pTexture->getGraphs(Texture::UI_PL_HP_LOW), temp_screen_ui, DX_GRAPH_FILTER_BRIGHT_CLIP, DX_CMP_LESS, SCASI(pl_low_hp.val), TRUE, GetColor(153, 16, 0), 0);
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, (pl_low_hp_offset > (float)PL->getHP(0) ? SCASI(pl_low_hp_alpha.val) : (int)Fit(pl_low_hp_alpha.val, 255.f, 255.f, 0.f, 156.f)));
				DrawGraph(0, 0, pTexture->getGraphs(Texture::UI_BLOOD), TRUE);
				SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
			}
			SetDrawScreen(DX_SCREEN_BACK);
			{
				SetDrawBright(152, 16, 0);
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, SCASI(pl_low_hp_alpha.val));
				DrawRectRotaGraph2
				(
					SCREEN_CX - cam.zxi(), SCREEN_CY - cam.zyi(),
					0, 0,
					TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
					TEMP_SCREEN_CX, TEMP_SCREEN_CY,
					(double)TEMP_SCREEN_EXT_RATE_MAX + cam.zoom(),
					GetRadian(cam.angle()),
					temp_screen_ui, 1
				);
				SetDrawBright(255, 255, 255);
			}
		}

		SetDrawScreen(temp_screen_ui);
		ClearDrawScreen();
		{
			ui.Draw();
			mini_map.Draw();
			Gmenu.Draw();
			shop.Draw();
			if (loadFin[0] && loadFin[1])
				if (stageNumber == 5)
				bossProduction();
		}

		SetDrawScreen(DX_SCREEN_BACK);
		// ui
		DrawRectRotaGraph2
		(
			SCREEN_CX - cam.zxi(), SCREEN_CY - cam.zyi(),
			0, 0,
			TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
			TEMP_SCREEN_CX, TEMP_SCREEN_CY,
			(double)TEMP_SCREEN_EXT_RATE_MAX + cam.zoom(),
			GetRadian(cam.angle()),
			temp_screen_ui, 1
		);

	}

	pFade->Draw();
}

void Game::ImGui()
{
	if (!loadFin[0] || !loadFin[1])
		return;

	ImGui::SetNextWindowSize(ImVec2(200, 500), ImGuiSetCond_Once);
	ImGui::SetNextWindowPos(ImVec2(0, SCREEN_HEIGHT - 500), ImGuiSetCond_Once);

	ImGui::Begin("sceneGame", nullptr, ImGuiWindowFlags_MenuBar);

	ImGui::Text("changeScene");

	if (ImGui::Button("Title"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::TITLE;
	}
	if (ImGui::Button("Over"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::OVER;
	}
	if (ImGui::Button("Clear"))
	{
		sceneManager::isSwitchScene = true;
		sceneManager::next_scene = sceneManager::CLEAR;
	}
	ImGui::NewLine();
	ImGui::Text("BackGround_Color");

	ImGui::ColorPicker4("color##ホンマだるい", prev_col);
	if (ImGui::Button("Just do it!!!!!"))
	{
		bg_col[0] = scasi(prev_col[0] * 255.f);
		bg_col[1] = scasi(prev_col[1] * 255.f);
		bg_col[2] = scasi(prev_col[2] * 255.f);
		bg_col[3] = scasi(prev_col[3] * 255.f);
	}

	if (ImGui::Button("-##WindowSize", ImVec2(16.f * (TEMP_SCREEN_EXT_RATE_MAX - screen_ext_rate), 16.f * (TEMP_SCREEN_EXT_RATE_MAX - screen_ext_rate))))
	{
		screen_ext_rate -= screen_ext_rate > 1.f ? 1.f : 0.f;
		SetWindowSize(TEMP_SCREEN_WIDTH * scasi(screen_ext_rate), TEMP_SCREEN_HEIGHT * scasi(screen_ext_rate));
	}
	ImGui::SameLine();
	if (ImGui::Button("+##WindowSize", ImVec2(16.f * (TEMP_SCREEN_EXT_RATE_MAX - screen_ext_rate), 16.f * (TEMP_SCREEN_EXT_RATE_MAX - screen_ext_rate))))
	{
		screen_ext_rate += screen_ext_rate < TEMP_SCREEN_EXT_RATE_MAX ? 1.f : 0.f;
		SetWindowSize(TEMP_SCREEN_WIDTH * scasi(screen_ext_rate), TEMP_SCREEN_HEIGHT * scasi(screen_ext_rate));
	}
	ImGui::SameLine();
	ImGui::Text("Window Size: %d  ", scasi(screen_ext_rate));

	ImGui::NewLine();

	if (ImGui::Button("setStartPos"))
	{
		saveStartPos();
	}

	ImGui::NewLine();
	ImGui::NewLine();
	if (ImGui::Button("Player"))Iwin_flg.pl ^= 1;
	if (ImGui::Button("BG"))Iwin_flg.bg ^= 1;
	if (ImGui::Button("Obstacles"))Iwin_flg.obstacle ^= 1;
	if (ImGui::Button("Enemy"))Iwin_flg.enemy ^= 1;
	if (ImGui::Button("objBG"))Iwin_flg.objBG ^= 1;
	if (ImGui::Button("Camera"))Iwin_flg.cam ^= 1;
	if (ImGui::Button("EfcEditor"))
	{
		cam.SwitchIsUsePtr();
		cam.SetTargetPosAtVector2(Vector2(200, 130));
		cam.SetAreaMaxPos((-1.f * !Iwin_flg.efc) + (640000.f * Iwin_flg.efc), (-1.f * !Iwin_flg.efc) + (640000.f * Iwin_flg.efc));
		Iwin_flg.efc ^= 1;
	}
	if (ImGui::Button("AnmEfcEditor"))
	{
		cam.SwitchIsUsePtr();
		cam.SetTargetPosAtVector2(Vector2(200, 130));
		cam.SetAreaMaxPos((-1.f * !Iwin_flg.anmefc) + (640000.f * Iwin_flg.anmefc), (-1.f * !Iwin_flg.anmefc) + (640000.f * Iwin_flg.anmefc));
		Iwin_flg.anmefc ^= 1;
	}
	if (ImGui::Button("minimap"))Iwin_flg.minimap ^= 1;
	if (ImGui::Button("postfx"))Iwin_flg.postfx ^= 1;
	ImGui::End();


	pBG->ImGui();

	pObjManager->ImGui();

	pobjBGManager->ImGui();
	penmManager->imGui();
	E_N_Slime::eImGui();
	E_N_Skeleton::eImGui();
	E_B_Skeleton::eImGui();
	E_Boss::eImGui();
	Obstacles::obImGui();
	cam.ImGui();

	a_efc_editor.ImGui(temp_screen);
	efc_editor.ImGui(temp_screen);
	mini_map.ImGui();
	POSTFX->ImGui();

	ImGui::ShowDemoWindow();

}

void Game::setStartPos()
{
	char fileName[30] = { 0 };
	sprintf_s(fileName, "Data/startPos/%d.txt", stageNumber);

	FILE* fp = nullptr;

	if (fopen_s(&fp, fileName, "r") == 0)
	{

		fscanf_s(fp, "%f", &pl_StartPos.x);

		fscanf_s(fp, "%f", &pl_StartPos.y);

		fclose(fp);
	}
	else
	{
		pl_StartPos = { 0,0 };
	}

	pObjManager->ObjList[Object::PLAYER].back()->pos = pl_StartPos;

}

void Game::saveStartPos()
{
	char fileName[30] = { 0 };
	sprintf_s(fileName, "Data/startPos/%d.txt", stageNumber);

	FILE* fp = nullptr;

	if (fopen_s(&fp, fileName, "w") == 0)
	{

		fprintf_s(fp, "%f\n", pObjManager->ObjList[Object::PLAYER].back()->pos.x);

		fprintf_s(fp, "%f", pObjManager->ObjList[Object::PLAYER].back()->pos.y);

		fclose(fp);
	}

}

void Game::colUpdate()
{
	static float pushBack = 1.0f;

	for (auto& it : pObjManager->ObjList[Object::ENEMY])
	{
		if (pObjManager->ObjList[Object::PLAYER].back()->getNoHitTime() > NODAMAGE - NOMOVE)break;

		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->size, Vector2(0, 0), it, it->size, Vector2(0, 0)))
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getStep(0) > 0)break;
			if (pObjManager->ObjList[Object::PLAYER].back()->pos.x < it->pos.x)
			{
				pObjManager->ObjList[Object::PLAYER].back()->pos.x -= 4.0f * pushBack;
				pushBack *= 0.9f;
				break;
			}
			else
			{
				pObjManager->ObjList[Object::PLAYER].back()->pos.x += 4.0f * pushBack;
				pushBack *= 0.9f;
				break;
			}
		}
		if (pushBack < 0.9f)
		{
			pushBack = 1.0f;
		}

	}
	//エネミー同士の押し返し＆反転
	for (auto& it : pObjManager->ObjList[Object::ENEMY])
	{
		for (auto& it_2 : pObjManager->ObjList[Object::ENEMY])
		{
			if (it == it_2)continue;

			if (Collision[RECT_RECT](it_2, it_2->size, Vector2(0, 0), it, it->size, Vector2(0, 0)))
			{
				if (it_2->pos.x < it->pos.x)
				{
					it_2->pos.x -= 4.0f * pushBack;
					pushBack *= 0.9f;
					//break;
				}
				else
				{
					it_2->pos.x += 4.0f * pushBack;
					pushBack *= 0.9f;
					//break;
				}

				if (it->isFlipX && it->pos.x > it_2->pos.x && it->speed.x < 0)
					it->isFlipX = false;
				else if (!it->isFlipX && it->pos.x < it_2->pos.x && it->speed.x > 0)
					it->isFlipX = true;

				if (it_2->isFlipX && it_2->pos.x > it->pos.x && it_2->speed.x < 0)
					it_2->isFlipX = false;
				else if (!it_2->isFlipX && it_2->pos.x < it->pos.x && it_2->speed.x > 0)
					it_2->isFlipX = true;

			}
			if (pushBack < 0.9f)
			{
				pushBack = 1.0f;
			}
		}

	}



	//Player attack to Enemies

	if (!pObjManager->ObjList[Object::ENEMY].empty())
	{
		for (auto& it : pObjManager->ObjList[Object::ENEMY])
		{
			if (!it->m_isVisible)
				continue;
				
			if (it->getATK(2).y == pObjManager->ObjList[Object::PLAYER].back()->getATK(3).x)
				continue;

			if (pObjManager->ObjList[Object::PLAYER].back()->getATK(0).x <= 0)break;

			if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->getATK(0), pObjManager->ObjList[Object::PLAYER].back()->getATK(1), it, it->size, Vector2(0, 0)))
			{
				if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > it->pos.x)
				{
					it->damage_direction = false;
				}
				else
				{
					it->damage_direction = true;
				}

				pObjManager->ObjList[Object::PLAYER].back()->_PASS_DATA = true;
				it->setDamage(26, pObjManager->ObjList[Object::PLAYER].back()->getATK(2).x, pObjManager->ObjList[Object::PLAYER].back()->getATK(3));
				int tempHp = it->getHP(0);
				if (tempHp <= 0)
				{
					pSound->playSE(pSound->SE_E_DEATH, false);
					it->isErase = true;
					//pObjManager->ObjList[Object::PLAYER].back()->addMoney(it->money); 
					cam.ShakeCamera(Camera::eShake::Shake_Enm_Dead);
				}
				continue;
			}
		}


		for (auto& it : pObjManager->ObjList[Object::PL_WEAPON])
		{
			for (auto& iter : pObjManager->ObjList[Object::ENEMY])
			{
				if (!iter->m_isVisible)
					continue;

				if (iter->getATK(2).y == it->getATK(0).x)
					continue;

				if (Collision[RECT_RECT](it, it->size, Vector2(0, 0), iter, iter->size, Vector2(0, 0)))
				{
					if (it->pos.x > iter->pos.x)
					{
						iter->damage_direction = false;
					}
					else
					{
						iter->damage_direction = true;
					}
					iter->setDamage(26, it->getATK(1).x, it->getATK(0));
					it->isErase = true;
					pSound->playSE(pSound->SE_E_MISSILE_HIT, false);
					if (iter->getHP(0) <= 0)
					{
						pSound->playSE(pSound->SE_E_DEATH, false);
						iter->isErase = true;
						cam.ShakeCamera(Camera::eShake::Shake_Enm_Dead);
					}
				}

			}
		}
	}


	//Enemy attack to Player
	for (auto& it : pObjManager->ObjList[Object::ENEMY])
	{
		if (!it->m_isVisible)
			continue;

		if (it->getATK(1).x < 0)continue;
		if (pObjManager->ObjList[Object::PLAYER].back()->noHitTime > 0)break;

		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->size, Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getStep(0) > 0)break;

			pObjManager->ObjList[Object::PLAYER].back()->setDamage(NODAMAGE, it->getATK(2).x, { 0,0 });
			cam.ShakeCamera(Camera::eShake::Shake_Pl_Damage);

			pl_low_hp_alpha.val = 255.f;

			break;
		}
	}

	//enemyArrow to Player
	if (pObjManager->ObjList[Object::MISSILE].empty() == false)
	{
		for (auto& it : pObjManager->ObjList[Object::MISSILE])
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getStep(0) > 0)break;

			if (pObjManager->ObjList[Object::PLAYER].back()->getNoHitTime() > 0)break;

			if (!it->getATK(2).x)continue;


			if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->size, Vector2(0, 0), it, it->size, it->getATK(0)))
			{
				if (pObjManager->ObjList[Object::PLAYER].back()->pos.x < it->pos.x)
				{
					pObjManager->ObjList[Object::PLAYER].back()->damage_direction = false;
				}
				else
				{
					pObjManager->ObjList[Object::PLAYER].back()->damage_direction = true;
				}
				pObjManager->ObjList[Object::PLAYER].back()->setDamage(NODAMAGE, it->getATK(2).x, { 0,0 });
				cam.ShakeCamera(Camera::eShake::Shake_Enm_Dead);
				if (it->id.CLASS == 0)
					it->isErase = true;

				pl_low_hp_alpha.val = 255.f;

				break;

			}
		}
	}


	//トラップ類との当たり判定
	for (auto& it : pObjManager->ObjList[Object::TRAP])
	{

		if (it->getATK(2).y)continue;
		if (it->id.CLASS == bgObjectGroup::Trap && it->id.PERSONAL == trapType::axe)
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getNoHitTime() > 0)break;

			if (Collision[RECT_ROTARECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->size, Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
			{
				if (pObjManager->ObjList[Object::PLAYER].back()->getStep(0) > 0)break;

				pObjManager->ObjList[Object::PLAYER].back()->setDamage(NODAMAGE, it->getATK(2).x, { 0,0 });
				if (!pl_dead_state)cam.ShakeCamera(Camera::eShake::Shake_Pl_Damage);

				pl_low_hp_alpha.val = 255.f;

				break;
			}
		}
		else if (it->id.CLASS != bgObjectGroup::Trap)
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getNoHitTime() > 0)break;
			if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->size, Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
			{
				if (pObjManager->ObjList[Object::PLAYER].back()->getStep(0) > 0)break;

				pObjManager->ObjList[Object::PLAYER].back()->setDamage(NODAMAGE, it->getATK(2).x, { 0,0 });
				if (!pl_dead_state)cam.ShakeCamera(Camera::eShake::Shake_Pl_Damage);

				pl_low_hp_alpha.val = 255.f;

				break;
			}
		}
		else
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->speed.y < 0)break;
			if (!it->getATK(2).y)
			{
				if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->size, Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
				{
					if (it->stop_anim)
					{

						it->ObjHoseiDown(pObjManager->ObjList[Object::PLAYER].back());
						pObjManager->ObjList[Object::PLAYER].back()->onGround = true;
						it->_PASS_DATA = true;
					}
				}
				else
					it->_PASS_DATA = false;
			}
		}
	}
	ui.interact = -1;
	//teleporter	
	int counter = 0;
	for (auto& it : pObjManager->ObjList[Object::TEREPORTER])
	{
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			if (it->stop_anim)
			{
				it->stop_anim = false;
				pSound->playSE(pSound->SE_OBJ_TELEPORT_START, false);
				break;
			}
		}
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(4), it->getATK(3)))
		{
			if (it->getATK(2).y)
			{
				if (key[KEY_INPUT_E] == 1)
				{
					pSound->playSE(pSound->SE_SYSTEM_PAUSE, false);
					mini_map.SelectTelepoDestination(counter);
				}
				ui.interact = 6;
			}
		}
		counter++;
	}// Collision

	 // TODO::door
	for (auto& it : pObjManager->ObjList[Object::DOOR])
	{
		if (!it->stop_anim)
			continue;
		//プレイヤー
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(4), it->getATK(3)))
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > it->pos.x)
			{
				it->ObjHoseiRight(pObjManager->ObjList[Object::PLAYER].back(), it->getATK(4).x * 0.7f);
			}
			else
			{
				it->ObjHoseiLeft(pObjManager->ObjList[Object::PLAYER].back(), it->getATK(4).x * 0.7f);
			}
		}

		//エネミー
		for (auto& it_enm : pObjManager->ObjList[Object::ENEMY])
		{
			if (!it_enm->m_isVisible)
				continue;

			if (Collision[RECT_RECT](it_enm, it_enm->size, Vector2(0, 0), it, it->getATK(4), it->getATK(3)))
			{
				if (it_enm->pos.x < it->pos.x)
				{
					it_enm->pos.x -= 4.0f * pushBack;
					pushBack *= 0.9f;
					//break;
				}
				else
				{
					it_enm->pos.x += 4.0f * pushBack;
					pushBack *= 0.9f;
					//break;
				}

				if (it_enm->isFlipX && it_enm->pos.x > it->pos.x && it_enm->speed.x < 0)
					it_enm->isFlipX = false;
				else if (!it_enm->isFlipX && it_enm->pos.x < it->pos.x && it_enm->speed.x > 0)
					it_enm->isFlipX = true;

			}
		}

		for (auto& it_mis : pObjManager->ObjList[Object::MISSILE])
		{
			if (Collision[RECT_RECT](it_mis, it_mis->size, Vector2(0, 0), it, it->getATK(4), it->getATK(3)))
			{
				it_mis->isErase = true;
			}
		}

		//　ボタン
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			ui.interact = 4;
			if (key[KEY_INPUT_E])
			{
				it->stop_anim = false;
				pSound->playSE(pSound->SE_OBJ_DOOR, false);

				if (it->pos.x < pObjManager->ObjList[Object::PLAYER].back()->pos.x)
					it->isFlipX = true;
			}
		}
		//ローリング
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(4), it->getATK(3)))
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getStep(0) > 0)
			{
				pSound->playSE(pSound->SE_OBJ_BREAK, false);
				it->isErase = true;
				EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("door_break.bin"), false, it->pos, pObjManager->ObjList[Object::PLAYER].back()->isFlipX);
				continue;

			}
		}
		//攻撃
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->getATK(0), pObjManager->ObjList[Object::PLAYER].back()->getATK(1), it, it->getATK(4), it->getATK(3)))
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getATK(0).x > 0)
			{

				pSound->playSE(pSound->SE_OBJ_BREAK, false);
				it->isErase = true;
				EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("door_break.bin"), false, it->pos, pObjManager->ObjList[Object::PLAYER].back()->isFlipX);
				continue;
			}
		}

		//遠距離
		for (auto& it_wep : pObjManager->ObjList[Object::PL_WEAPON])
		{
			if (Collision[RECT_RECT](it_wep, it_wep->size, Vector2(0, 0), it, it->getATK(4), it->getATK(3)))
			{
				pSound->playSE(pSound->SE_OBJ_BREAK, false);
				it->isErase = true;
				it_wep->isErase = true;
				EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("door_break.bin"), false, it->pos, pObjManager->ObjList[Object::PLAYER].back()->isFlipX);
				break;

			}
		}


	}// Collision

	 //treasureBox
	for (auto& it : pObjManager->ObjList[Object::TREASUREBOX])
	{
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			if (it->stop_anim)
			{
				ui.interact = 4;
				if (key[KEY_INPUT_E])
				{
					if (Gmenu.gain_count == 0)
					{
						ui.bag_tutorial = true;
					}
					Gmenu.gain_count++;
					ui.isGain = true;
					ui.apeal_pos.x = it->pos.x;
					ui.apeal_pos.y = it->pos.y - 16;
					int tmp = 0;
					tmp = GetRand(5) - 1;
					ui.apeal_weapon = { tmp > 0 ? tmp : 0,it->id.PERSONAL + 1 };
					for (auto& it : Gmenu.bag)
					{
						if (it.id != -1)continue;
						it = ui.apeal_weapon;
						break;
					}
					it->stop_anim = false;
					pSound->playSE(pSound->SE_OBJ_TREASUREBOX, false);
					break;
				}
			}
		}

		//攻撃
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), pObjManager->ObjList[Object::PLAYER].back()->getATK(0), pObjManager->ObjList[Object::PLAYER].back()->getATK(1), it, it->getATK(4), it->getATK(3)))
		{
			if (pObjManager->ObjList[Object::PLAYER].back()->getATK(0).x > 0)
			{

				if (it->stop_anim)
				{
					if (Gmenu.gain_count == 0)
					{
						ui.bag_tutorial = true;
					}
					Gmenu.gain_count++;
					ui.isGain = true;
					ui.apeal_pos.x = it->pos.x;
					ui.apeal_pos.y = it->pos.y - 16;
					int tmp = 0;
					tmp = GetRand(5) - 1;
					ui.apeal_weapon = { tmp > 0 ? tmp : 0,it->id.PERSONAL + 1 };
					for (auto& it : Gmenu.bag)
					{
						if (it.id != -1)continue;
						it = ui.apeal_weapon;
						break;
					}
					pSound->playSE(pSound->SE_OBJ_TREASUREBOX, false);

					it->stop_anim = false;
					continue;
				}
			}
		}



	}// Collision

	//ゴール
	for (auto& it : pObjManager->ObjList[Object::GOAL])
	{
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			ui.interact = 3;
			if (key[KEY_INPUT_E] == 1)
			{
				//TODO::ゴールの処理
				pSound->playSE(pSound->SE_E_DEATH, false);

				//(仮)
				sceneManager::isSwitchScene = true;
				sceneManager::next_scene = sceneManager::GAME;
				return;
			}
		}
	}// Collision	

	//ショップ
	for (auto& it : pObjManager->ObjList[Object::SHOPMAN])
	{
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			ui.interact = 1;
			if (key[KEY_INPUT_E] == 1)
			{
				//TODO::ショップの処理				
				//(仮)
				if (!shop.isOpen)
				{
					pSound->playSE(pSound->SE_SYSTEM_PAUSE, false);
					shop.isOpen = true;
				}
			}
		}
	}// Collision


	//エレベーター関連残り
	for (auto& it : pObjManager->ObjList[Object::OBSTACLE])
	{
		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->getATK(1), it->getATK(0)))
		{
			ui.interact = 2;
			if (key[KEY_INPUT_E] == 1)
			{
				if (it->stop_anim)
				{
					it->stop_anim = false;
					pSound->playSE(pSound->SE_OBJ_ELEVATOR, false);
				}
			}
		}
	}// Collision



	//Objectとの当たり判定(Elevator)
	for (auto& it : pObjManager->ObjList[Object::ELEVATOR])
	{
		if (it->id.PERSONAL != elevatorType::elevator)
			continue;

		if (Collision[RECT_RECT](pObjManager->ObjList[Object::PLAYER].back(), (pObjManager->ObjList[Object::PLAYER].back()->size), Vector2(0, 0), it, it->size, Vector2(0, 0)))
		{
			ui.interact = 2;
			it->_PASS_DATA = true;
			pObjManager->ObjList[Object::PLAYER].back()->onGround = true;
			it->ObjHoseiDown(pObjManager->ObjList[Object::PLAYER].back());
			break;
		}
		it->_PASS_DATA = false;

	}// Collision

	//エレベーター止まる
	for (auto& it : pObjManager->ObjList[Object::ELEVATOR])
	{
		if (it->id.PERSONAL != elevatorType::elevator)
			continue;
		for (auto& it2 : pObjManager->ObjList[Object::STOPPER])
		{
			if (it2->id.PERSONAL != elevatorType::stopper)
				continue;
			if (it->speed.y < 0)
			{
				if (Collision[RECT_RECT](it2, it2->getATK(1), it2->getATK(0), it, it->size, Vector2(0, 0)))
				{
					it->speed.y = 0;
					it->pos.y = it2->pos.y - it2->getATK(0).y;

					break;
				}
			}
		}
	}// Collision

}


void Game::bossProduction()
{
	static int barPos = 0;
	int boss = -1;
	if((int)pObjManager->ObjList[Object::ENEMY].empty() == false)
		boss = (int)pObjManager->ObjList[Object::ENEMY].back()->getATK(7).x;

	switch (boss)
	{
	case 0:
		barPos = 0;
		return;
	case 1:
		bossProd = true;
		barPos += 1;
		break;
	case 2:
		break;
	case 3:
		finLinear = true;
		bossProd = false;
		{// エリア制限
			// boss_area_min_x = 1024
			// boss_area_max_x = 2576
			if (pObjManager->ObjList[Object::PLAYER].back()->pos.x < 1520.f + pObjManager->ObjList[Object::PLAYER].back()->size.x*0.5f)
			{
				pObjManager->ObjList[Object::PLAYER].back()->pos.x = 1520.f + pObjManager->ObjList[Object::PLAYER].back()->size.x*0.5f;
			}
			if (pObjManager->ObjList[Object::PLAYER].back()->pos.x > 2165.f - pObjManager->ObjList[Object::PLAYER].back()->size.x*0.5f)
			{
				pObjManager->ObjList[Object::PLAYER].back()->pos.x = 2165.f - pObjManager->ObjList[Object::PLAYER].back()->size.x*0.5f;
			}
			if (pObjManager->ObjList[Object::ENEMY].back()->pos.x < 1520.f + pObjManager->ObjList[Object::ENEMY].back()->size.x*0.5f)
			{
				pObjManager->ObjList[Object::ENEMY].back()->pos.x = 1520.f + pObjManager->ObjList[Object::ENEMY].back()->size.x*0.5f;
			}
			if (pObjManager->ObjList[Object::ENEMY].back()->pos.x > 2165.f - pObjManager->ObjList[Object::ENEMY].back()->size.x*0.5f)
			{
				pObjManager->ObjList[Object::ENEMY].back()->pos.x = 2165.f - pObjManager->ObjList[Object::ENEMY].back()->size.x*0.5f;
			}
			cam.area_min_pos.x = 1485.f;
			cam.SetAreaMaxPos(2200.f, 30000.f);
			if (timer % 180 == 0)
			{
				EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("boss_wall.bin"), false, Vector2(1485.f + 16.f, 595.f));
				EFC_MGR->MakeAndGetEfcRoot(EFCDATA->GetEmitDataset("boss_wall.bin"), false, Vector2(2200.f - 16.f, 595.f));
			}
		}
		if (barPos)
			barPos -= 4;
		else
			return;
		break;
	default:
		Vector2 max = Vector2(XY_I2F(pBG->getMapSize()));
		cam.area_min_pos.x = 0.f;
		cam.SetAreaMaxPos(max.x * 32.f, max.y * 32.f);
		finButtle = true;
		break;
	}
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
	DrawBox(0, 0, TEMP_SCREEN_WIDTH, barPos, GetColor(0, 0, 0), 1);
	DrawBox(0, TEMP_SCREEN_HEIGHT - barPos, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, GetColor(0, 0, 0), 1);


}
