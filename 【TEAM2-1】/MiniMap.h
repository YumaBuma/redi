#pragma once
#include "Mo2_BG.h"
#include "EaseMove.h"

#include <d3d11.h>
#include "SimpleMath.h"

class MiniMap /*: public CerealLoadAndSave*/
{
public:
	// 現状ミニマップのチップサイズ == 8x8;
	MiniMap();

	void Init();

	~MiniMap() 
	{
		DeleteScreen();
	}

	void CreateScreen()
	{
		// ghnds
		if (mini_map_ghnd		<= 0)
			mini_map_ghnd			= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
		if (mini_map_bg_ghnd	<= 0)
			mini_map_bg_ghnd		= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
		if (mini_map_ui_ghnd	<= 0)
			mini_map_ui_ghnd		= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
		if (mask_blend_ghnd		<= 0)
			mask_blend_ghnd			= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
		if (mini_map_composite_ghnd <= 0)
			mini_map_composite_ghnd	= MakeScreen(TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, 1);
		if (mask_ghnd_src		<= 0)
			mask_ghnd_src			= LoadGraph(L".\\Data\\Images\\MapChips\\mapchip_minimsk.png");

	}
	void DeleteScreen()
	{
		mini_map_ghnd			= (mini_map_ghnd			!= -1)	? DeleteGraph(mini_map_ghnd)			: -1;
		mini_map_bg_ghnd		= (mini_map_bg_ghnd			!= -1)	? DeleteGraph(mini_map_bg_ghnd)			: -1;
		mini_map_ui_ghnd		= (mini_map_ui_ghnd			!= -1)	? DeleteGraph(mini_map_ui_ghnd)			: -1;
		mini_map_composite_ghnd = (mini_map_composite_ghnd	!= -1)	? DeleteGraph(mini_map_composite_ghnd)	: -1;
		mask_ghnd				= (mask_ghnd				!= -1)	? DeleteGraph(mask_ghnd)				: -1;
		mask_blend_ghnd			= (mask_blend_ghnd			!= -1)	? DeleteGraph(mask_blend_ghnd)			: -1;
		mask_ghnd_src			= (mask_ghnd_src			!= -1)	? DeleteGraph(mask_ghnd_src)			: -1;
	}

	void CreateMaskScreen();
	void FillMaskScreenWhite();

	void SetMiniaturizationRectMinMax
	(
		const DirectX::SimpleMath::Vector2& _miniaturization_rect_min_pos,
		const DirectX::SimpleMath::Vector2& _miniaturization_rect_max_pos
	)
	{
		miniaturization_rect_min_pos = _miniaturization_rect_min_pos;
		miniaturization_rect_max_pos = _miniaturization_rect_max_pos;
	}
	void SetMaximizationRectMinMax
	(
		const DirectX::SimpleMath::Vector2& _maximization_rect_min_pos,
		const DirectX::SimpleMath::Vector2& _maximization_rect_max_pos
	)
	{
		miniaturization_rect_min_pos = _maximization_rect_min_pos;
		miniaturization_rect_max_pos = _maximization_rect_max_pos;
	}

	void SetChipSize(const int& _chip_size) { chip_size = _chip_size; }

	// ミニマップのサイズを切り替えます
	void SwitchSize();
	// ミニマップを最小化します
	void Miniaturization();
	// ミニマップを最大化します
	void Maximization();
	// テレポート先を選択する状態に移ります
	void SelectTelepoDestination(const int& _selecting_telepoter_number);
	// テレポート先を選択中かの判定を返します
	const bool& IsSelectingTelepoDestination()const { return is_selecting_telepo_dest; }
	// テレポート先を選択したかの判定を返します
	const bool& IsSelectedTelepoDestination()const { return is_seleced_telepo_dest; }
	// ミニマップのアニメーション進行度を設定します
	void SetEasingProgress(const float& _ease_time);
	//const std::vector<std::shared_ptr<Object>>::iterator		SelectTelepoDestination();

	void Update();
	void UpdateTelepoDestination();
//#define SHOPGOAL 
#ifdef SHOPGOAL
		std::vector<DirectX::SimpleMath::Vector2>	shop_poss;
		DirectX::SimpleMath::Vector2				goal_pos;
		void DrawShop(const DirectX::SimpleMath::Vector2& _pos);
		void DrawGoal(const DirectX::SimpleMath::Vector2& _pos);
#endif
	// 仮画面(temp_screen)で描画してください
	void DrawComposite();
	// DX_SCREEN_BACKで描画してください
	void Draw();

	const int& GetMiniMapCompositeGhnd()const { return mini_map_composite_ghnd; }

	void ImGui();

	template <class Archive>
	void serialize(Archive& _ar, const std::uint32_t version)
	{
		_ar
		(
			CEREAL_NVP(pos),
			CEREAL_NVP(mini_map_bg_ghnd),
			CEREAL_NVP(mini_map_extrate),
			CEREAL_NVP(blend_alpha),
			CEREAL_NVP(rect_min),
			CEREAL_NVP(rect_max),
			CEREAL_NVP(miniaturization_ease_algo),
			CEREAL_NVP(maximization_ease_algo),
			CEREAL_NVP(miniaturization_ease_time),
			CEREAL_NVP(maximization_ease_time),
			CEREAL_NVP(chip_size),
			CEREAL_NVP(bg_color),
			CEREAL_NVP(miniaturization_rect_min_pos.x),
			CEREAL_NVP(miniaturization_rect_min_pos.y),
			CEREAL_NVP(miniaturization_rect_max_pos.x),
			CEREAL_NVP(miniaturization_rect_max_pos.y),
			CEREAL_NVP(maximization_rect_min_pos.x),
			CEREAL_NVP(maximization_rect_min_pos.y),
			CEREAL_NVP(maximization_rect_max_pos.x),
			CEREAL_NVP(maximization_rect_max_pos.y),
			CEREAL_NVP(miniaturization_blend_alpha),
			CEREAL_NVP(maximization_blend_alpha)
		);
	}
private:
	void LoadDataFromFile(const std::string& _path, eArchiveTypes _file_type)
	{
		int ghnd_tmp[7] =
		{
			this->mask_ghnd_src,
			this->mask_ghnd,
			this->mini_map_bg_ghnd,
			this->mini_map_composite_ghnd,
			this->mini_map_ghnd,
			this->mask_blend_ghnd,
			this->mini_map_ui_ghnd
		};

		MiniMap mini_map;
		std::ifstream
			ifs;
			ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
		if (!ifs)return; // 読み込み失敗

		switch (_file_type)
		{
		case eArchiveTypes::BINARY:
		{
			cereal::BinaryInputArchive
				i_archive(ifs);
				i_archive(cereal::make_nvp(_path, mini_map));
			break;
		}
		case eArchiveTypes::JSON:
		case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
		{
			cereal::JSONInputArchive
				i_archive(ifs);
				i_archive(cereal::make_nvp(_path, mini_map));
			break;
		}
		case eArchiveTypes::XML:
		{
			cereal::XMLInputArchive
				i_archive(ifs);
				i_archive(cereal::make_nvp(_path, mini_map));
			break;
		}
		default:
			break;
		}

		*this = mini_map;
		is_loaded = true;

		this->mask_ghnd_src				= ghnd_tmp[0];
		this->mask_ghnd					= ghnd_tmp[1];
		this->mini_map_bg_ghnd			= ghnd_tmp[2];
		this->mini_map_composite_ghnd	= ghnd_tmp[3];
		this->mini_map_ghnd				= ghnd_tmp[4];
		this->mask_blend_ghnd			= ghnd_tmp[5];
		this->mini_map_ui_ghnd			= ghnd_tmp[6];
	}
	void SaveDataToFile(const std::string& _path, eArchiveTypes _file_type)
	{
		MiniMap mini_map = *this;

		/*
			グラフハンドルのコピーによりデストラクタ呼び出し時に
			DeleteScreenで画像ハンドルを消されてしまうことを防ぐ
		*/
		mini_map.mask_ghnd					= -1;
		mini_map.mask_ghnd_src				= -1;
		mini_map.mini_map_bg_ghnd			= -1;
		mini_map.mini_map_composite_ghnd	= -1;
		mini_map.mini_map_ghnd				= -1;
		mini_map.mask_blend_ghnd			= -1;
		mini_map.mini_map_ui_ghnd			= -1;

		std::ofstream
			ofs;
			ofs.open(_path, std::ios::binary);
		if (!ofs)return; // 読み込み失敗

		switch (_file_type)
		{
		case eArchiveTypes::BINARY:
		{
			cereal::BinaryOutputArchive
				o_archive(ofs);
				o_archive(cereal::make_nvp(_path, mini_map));
			break;
		}
		case eArchiveTypes::JSON:
		case eArchiveTypes::INI:
		{
			cereal::JSONOutputArchive
				o_archive(ofs);
				o_archive(cereal::make_nvp(_path, mini_map));
			break;
		}
		case eArchiveTypes::XML:
		{
			cereal::XMLOutputArchive
				o_archive(ofs);
				o_archive(cereal::make_nvp(_path, mini_map));
			break;
		}
		default:
			break;
		}
	}

public:
	EaseMoverVec2				pos;					// ミニマップ矩形の位置
	EaseMoverVec2				mini_map_scroll_pos;	// ミニマップの表示位置
	DelayMover					mini_map_extrate;
	EaseMover					blend_alpha;
	DelayMoverVec2				telepo_destination_pos	= DelayMoverVec2();
	bool						is_gmenu				= false;
	bool						is_gmenu_map			= false;
private:

	bool						is_maximize = false;

	// ミニマップ位置構成
	// pos->@------------mini_map_composite_ghnd
	//		| min->@---	|
	//		| max->---@ |
	//		-------------
	EaseMoverVec2				 rect_min;
	EaseMoverVec2				 rect_max;
	EaseMoverVec2				 center_margin;

	DirectX::SimpleMath::Vector2 miniaturization_rect_min_pos	= { 0,0 };
	DirectX::SimpleMath::Vector2 miniaturization_rect_max_pos	= { 0,0 };
	DirectX::SimpleMath::Vector2 maximization_rect_min_pos		= { 0,0 };
	DirectX::SimpleMath::Vector2 maximization_rect_max_pos		= { 0,0 };

	DirectX::SimpleMath::Vector2 draw_start_chip				= { 0,0 };
	DirectX::SimpleMath::Vector2 draw_iterate_num				= { 0,0 };

	// イージング関数
	int							miniaturization_ease_algo	= 0;
	int							maximization_ease_algo		= 0;

	// 遷移時間	
	float						miniaturization_ease_time	= 30.f;
	float						maximization_ease_time		= 30.f;

	// ミニマップ
	int							chip_size = 4;

	// 背景色
	DelayColor					bg_color;
	int							miniaturization_blend_alpha = 0;
	int							maximization_blend_alpha	= 0;

	// ghnd
	int							mini_map_ghnd			= -1;
	int							mini_map_bg_ghnd		= -1; // 最大化時の背景
	int							mini_map_ui_ghnd		= -1; // ui
	int							mini_map_composite_ghnd = -1; // コンポジット
	int							mask_ghnd				= -1; // マスキング用
	int							mask_blend_ghnd			= -1; // マスキング用
	int							mask_ghnd_src			= -1; // マスキング用
	INT2						mask_ghnd_size			= { 0,0 }; // マスキング用

	int							prev_blendval			= 0;
	int							prev_blendmode			= 0;

	bool						is_loaded				= false;
	bool						is_selecting_telepo_dest= false;
	bool						is_seleced_telepo_dest	= false;
	int							selecting_telepoter_number = 0;
	int							selecting_telepoter_number_tmp = 0;

};
CEREAL_CLASS_VERSION(MiniMap, 1);
