#pragma once
#include <random>
#include <array>

#define cast_int(x) static_cast<int>(x)

//================================================================================
//
//	パーリンノイズ生成
//	http://postd.cc/understanding-perlin-noise/
//	http://marupeke296.com/ALG_No3_PerlinNoize.html
//
//================================================================================
class PerlinNoise
{
public:
	//------------------------------------------------------
	//		初期化
	//------------------------------------------------------
	PerlinNoise(
		const int _Seed = 0	//	乱数テーブル決定用のシード値
	)
	{
		// 0〜255までの連続した数字を並び替えて乱数テーブルを作成する。
		for (int i = 0; i < RANDOM_RANGE; i++)
		{
			m_randomTable[i] = i;
		}

		std::mt19937 mt(_Seed);
		std::uniform_int_distribution<int> r(0, RANDOM_RANGE - 1);
		for (int i = 0; i < 1000; i++)
		{
			int i1 = r(mt);
			int i2 = r(mt);
			int temp = m_randomTable[i1];
			m_randomTable[i1] = m_randomTable[i2];
			m_randomTable[i2] = temp;
		}

	}

	//------------------------------------------------------
	//		パーリンノイズの生成
	//------------------------------------------------------

	//	座標を元にパーリンノイズを生成します。
	float Perlin(
		float _X,	//	x座標
		float _Y,	//	y座標
		float _Z	//	z座標
	)const
	{
		//	負数の場合、正の数にする
		while (_X < 0.0f) { _X += (cast_int(-_X) / RANDOM_RANGE + 1)*RANDOM_RANGE; }
		while (_Y < 0.0f) { _Y += (cast_int(-_Y) / RANDOM_RANGE + 1)*RANDOM_RANGE; }
		while (_Z < 0.0f) { _Z += (cast_int(-_Z) / RANDOM_RANGE + 1)*RANDOM_RANGE; }

		int xi = cast_int(_X);
		int yi = cast_int(_Y);
		int zi = cast_int(_Z);

		//	座標の小数部
		float xf = _X - xi;
		float yf = _Y - yi;
		float zf = _Z - zi;

		xi = xi%RANDOM_RANGE;
		yi = yi%RANDOM_RANGE;
		zi = zi%RANDOM_RANGE;

		//	座標間での補間係数算出
		float u = Fade(xf);
		float v = Fade(yf);
		float w = Fade(zf);

		//	勾配を取得
		//	引数にかかわらず座標の向きは
		//	(+,+,+),(-,+,+),(+,-,+),(-,-,+)
		//	(+,+,-),(-,+,-),(+,-,-),(-,-,-)
		int dir[8];
		dir[0] = GetRandom(GetRandom(GetRandom(xi) + yi) + zi);
		dir[1] = GetRandom(GetRandom(GetRandom(xi) + Inc(yi)) + zi);
		dir[2] = GetRandom(GetRandom(GetRandom(xi) + yi) + Inc(zi));
		dir[3] = GetRandom(GetRandom(GetRandom(xi) + Inc(yi)) + Inc(zi));
		dir[4] = GetRandom(GetRandom(GetRandom(Inc(xi)) + yi) + zi);
		dir[5] = GetRandom(GetRandom(GetRandom(Inc(xi)) + Inc(yi)) + zi);
		dir[6] = GetRandom(GetRandom(GetRandom(Inc(xi)) + yi) + Inc(zi));
		dir[7] = GetRandom(GetRandom(GetRandom(Inc(xi)) + Inc(yi)) + Inc(zi));

		//	8頂点分の勾配からノイズを生成
		float yr[2];
		float x1 = Lerp(
			Grad(dir[0], xf, yf, zf),
			Grad(dir[4], xf - 1.0f, yf, zf),
			u);
		float x2 = Lerp(
			Grad(dir[1], xf, yf - 1.0f, zf),
			Grad(dir[5], xf - 1.0f, yf - 1.0f, zf),
			u);
		yr[0] = Lerp(x1, x2, v);

		x1 = Lerp(
			Grad(dir[2], xf, yf, zf - 1.0f),
			Grad(dir[6], xf - 1.0f, yf, zf - 1.0f),
			u);
		x2 = Lerp(
			Grad(dir[3], xf, yf - 1.0f, zf - 1.0f),
			Grad(dir[7], xf - 1.0f, yf - 1.0f, zf - 1.0f),
			u);
		yr[1] = Lerp(x1, x2, v);

		return (Lerp(yr[0], yr[1], w) + 1.0f) / 2.0f;
	}

	//	座標を元にオクターブパーリンノイズを生成します。
	//	オクターブ回数を増やすことで、より規則性を持ったノイズを生成できます。
	float OctavePerlin(
		const float _X,			//	x座標
		const float _Y,			//	y座標
		const float _Z,			//	z座標
		const int _Octaves,		//	反復回数
		const float _Persistence	//	反復ごとの倍率(1.0より大きいと増幅、小さいと減衰)
	)const
	{
		float total = 0.0f;
		float frequency = 1.0f;
		float amplitude = 1.0f;
		float maxValue = 0.0f;

		for (int i = 0; i < _Octaves; i++)
		{
			total += Perlin(_X*frequency, _Y*frequency, _Z*frequency)*amplitude;
			maxValue += amplitude;

			amplitude *= _Persistence;
			frequency *= 2.0f;
		}

		return total / maxValue;
	}

private:
	//------------------------------------------------------
	//		定数
	//------------------------------------------------------

	static constexpr int RANDOM_RANGE = 256;	//	乱数テーブルの大きさ


	//------------------------------------------------------
	//		メンバ変数
	//------------------------------------------------------

	std::array<int, RANDOM_RANGE> m_randomTable;	//	乱数テーブル


	//------------------------------------------------------
	//		計算用関数
	//------------------------------------------------------

	//	フェード関数
	//	式は6t^5-15t^4+10t^3
	static inline float Fade(
		const float _T	//	0.0 〜 1.0
	)
	{
		return _T*_T*_T*(_T*(_T*6.0f - 15.0f) + 10.0f);
	}

	//	勾配関数
	//	引数から擬似勾配を生成する。
	static float Grad(
		const int _Hash,	//	疑似勾配の式を決定するハッシュ
		const float _X,		//	x座標
		const float _Y,		//	y座標
		const float _Z		//	z座標
	)
	{
		switch (_Hash & 0x0F)
		{
		case 0x00:	return  _X + _Y;
		case 0x01:	return -_X + _Y;
		case 0x02:	return  _X - _Y;
		case 0x03:	return -_X - _Y;
		case 0x04:	return  _X + _Z;
		case 0x05:	return -_X + _Z;
		case 0x06:	return  _X + _Z;
		case 0x07:	return -_X - _Z;
		case 0x08:	return  _Y - _Z;
		case 0x09:	return -_Y + _Z;
		case 0x0A:	return  _Y + _Z;
		case 0x0B:	return -_Y - _Z;
		case 0x0C:	return  _Y - _X;
		case 0x0D:	return -_Y + _Z;
		case 0x0E:	return  _Y - _X;
		case 0x0F:	return -_Y - _Z;

		default:	return 0.0f;
		}
	}

	//	_V0 と _V1 を _T で線形補間する。
	static inline float Lerp(
		const float _V0,
		const float _V1,
		const float _T
	)
	{
		return _V0 + (_V1 - _V0)*_T;
	}

	// _I に1を加算して範囲内に補正した値を返す。
	inline static int Inc(
		const int _I
	)
	{
		return (_I + 1) % RANDOM_RANGE;
	}

	//	乱数テーブルから乱数を取得する。
	inline int GetRandom(
		const int _I
	) const
	{
		return m_randomTable[_I%RANDOM_RANGE];
	}
};




