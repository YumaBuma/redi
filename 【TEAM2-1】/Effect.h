#pragma once
#include <d3d11.h>
#include "SimpleMath.h"
#include "Animation.h"
#include "EaseMove.h"

#include <cereal\cereal.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\string.hpp>

class EffectEmitter;
class AnimationEffectEmitter;

enum EFC_TYPE : int
{
	EFC_DEFAULT = 0,
	EFC_RANDOM_IMAGE_PARTICLE,
	EFC_JEWERLY,
	EFC_END,
};
struct EffectParams
{
	inline EffectParams GetAddPosParams(const DirectX::SimpleMath::Vector2& _pos) { EffectParams tmp = *this; tmp.pos += _pos; return tmp; }
#pragma region variables

	DirectX::SimpleMath::Vector2	pos						= { 0.f,0.f };
	DirectX::SimpleMath::Vector2	vel						= { 0.f,0.f };
	float							deceleration_rate_mul	= 1.f;

	float							duration				= 10.f;
	float							duration_max			= 10.f;

	const float*					attract_x_ptr			= nullptr;
	const float*					attract_y_ptr			= nullptr;

	float							radius					= 1.f;						// 拡大率にも代替して使用する
	float							radius_decreae_interval = 1.f;						// radiusが縮小し始めるまでのしきい値
	int								easing_algo				= Easing::eAlgs::LINEAR;	// OUTを使用

	float							gravity					= 0.f;

	bool							is_use_curl_noise		= false;
	float							noise_scale				= 1.f;
	float							curl_speed				= 0.1f;

	DirectX::SimpleMath::Vector2	attract_vel				= { 0.f,0.f };

	enum eAngleTypes : int									{ ANGLE_NONE, DELAY_MOVE, FOLLOW_TO_VEL, FOLLOW_TO_DURATION, ANGLE_END };
	int								angle_type				= FOLLOW_TO_DURATION;
	DelayMover						angle					= DelayMover();

	enum ePrimitiveTypes : int								{ CIRCLE, PIXEL, BOX, TRIANGLE, PRIMTYPE_NONE, PRIMTYPE_END };
	int								primitive_type			= CIRCLE;					// 描画時のプリミティブの種類
	int								blend_mode				= DX_BLENDMODE_ADD;
	int								blend_mode_ease			= Easing::eAlgs::LINEAR;
	bool							is_draw_line			= false;
	DelayColor						col						= DelayColor(Vector4(32.f,28.f,12.f,255.f));

	int								type					= EFC_DEFAULT;

	struct DRAW
	{
		int							graph_hadle				= -1;
		int							srcx					= 0;
		int							srcy					= 0;
		int							anmx					= 0;
		int							anmy					= 0;
		int							texx					= 0;
		int							texy					= 0;
		int							cx						= 0;
		int							cy						= 0;
		int							interval				= 1;						//画像切り替えスピード
		bool						is_loop					= false;

		template <class Archive>
		void serialize(Archive& _ar, std::uint32_t const version)
		{
			_ar
			(
				CEREAL_NVP(graph_hadle),
				CEREAL_NVP(srcx), CEREAL_NVP(srcy),
				CEREAL_NVP(anmx), CEREAL_NVP(anmy),
				CEREAL_NVP(texx), CEREAL_NVP(texy),
				CEREAL_NVP(cx)	, CEREAL_NVP(cy),
				CEREAL_NVP(interval),
				CEREAL_NVP(is_loop)
			);
		}
	};
	DRAW							draw;

	float							radius_fade_interval	= 0.f;						// radiusが目標の大きさになるまでの値
	float							blend_fade_interval		= 0.f;						// blendが目標の大きさになるまでの値
	float							blend_decreae_interval	= 1.f;						// blendが縮小し始めるまでのしきい値
	bool							is_attract_to_parent_emt= false;					// attract_ptrに親エミッターの位置を入れるか 

#pragma endregion

	template <class Archive>
	void serialize(Archive& _ar, std::uint32_t const version)
	{
		_ar
		(
			CEREAL_NVP(pos.x), CEREAL_NVP(pos.y),
			CEREAL_NVP(vel.x), CEREAL_NVP(vel.y),
			CEREAL_NVP(deceleration_rate_mul),

			CEREAL_NVP(duration),
			CEREAL_NVP(duration_max),

			CEREAL_NVP(radius),
			CEREAL_NVP(radius_decreae_interval),
			CEREAL_NVP(easing_algo),

			CEREAL_NVP(gravity),

			CEREAL_NVP(noise_scale),
			CEREAL_NVP(curl_speed),

			CEREAL_NVP(attract_vel.x),
			CEREAL_NVP(attract_vel.y),

			CEREAL_NVP(angle_type),
			CEREAL_NVP(angle),

			CEREAL_NVP(primitive_type),
			CEREAL_NVP(blend_mode),
			CEREAL_NVP(blend_mode_ease),
			CEREAL_NVP(is_draw_line),
			CEREAL_NVP(col),

			CEREAL_NVP(type),

			CEREAL_NVP(draw)
		);

		// カースノイズの使用フラグ追加
		if (version >= 2)_ar(CEREAL_NVP(is_use_curl_noise));

		// blend、radiusもイージング対応
		if (version >= 3)
		{
			_ar
			(
				CEREAL_NVP(radius_fade_interval),
				CEREAL_NVP(blend_fade_interval),
				CEREAL_NVP(blend_decreae_interval),
				CEREAL_NVP(is_attract_to_parent_emt)
			);
		}
	}
};
CEREAL_CLASS_VERSION(EffectParams, 3);
CEREAL_CLASS_VERSION(EffectParams::DRAW, 1);

//エフェクト単体
class BaseEffect
{
public:
	BaseEffect();
	virtual ~BaseEffect()
	{
		efc_params.attract_x_ptr = nullptr; efc_params.attract_y_ptr = nullptr; 
		parent_emitter = nullptr;
	}

	virtual bool Update() = 0;
	virtual void Draw(DirectX::SimpleMath::Vector2 _pos = { 0,0 }) = 0;
	virtual void SetDataInt(int* _ptr,int _slot_num) {};

	void SetAttractPos(const float* _attract_x = nullptr,const  float* _attract_y = nullptr)
	{
		efc_params.attract_x_ptr = _attract_x;
		efc_params.attract_y_ptr = _attract_y;
	}

	const DirectX::SimpleMath::Vector2& GetPos()const { return efc_params.pos; }

protected:
	EffectParams	efc_params;
	bool			is_use_ptr;
	bool			is_out_of_scr;
	EffectEmitter*	parent_emitter;
};

class EfcCollider
{
public:
	EfcCollider() 
		: prev_pos(0, 0), bounce_limit(8), bounce_coefficient(0.6f) {}
	EfcCollider(const int& _bounce_limit, const float& _bounce_coefficient = 0.6f) 
		: prev_pos(0, 0), bounce_limit(_bounce_limit), bounce_coefficient(_bounce_coefficient) {}
	virtual ~EfcCollider() {}

	virtual bool IsHit() = 0;

protected:
	DirectX::SimpleMath::Vector2 prev_pos;
	int bounce_limit;
	float bounce_coefficient; // coefficient of restitution
};

class EfcDefault : public BaseEffect
{
public:
	EfcDefault() {};
	EfcDefault(DirectX::SimpleMath::Vector2 _pos, DirectX::SimpleMath::Vector2 _vel, float _duration = 40.f, float _deceleration_rate_mul = 0.9f, float _gravity = 0.f);
	EfcDefault(const EffectParams& _params);
	EfcDefault(const EffectParams& _params, EffectEmitter* _parent_emitter = nullptr, const bool& _is_reverse_x = false);
	~EfcDefault() { parent_emitter = nullptr; }

	bool Update()override;
	void Draw(DirectX::SimpleMath::Vector2 _pos = { 0,0 })override;

private:
	bool is_reverse_x;
};

class EfcRandomImageParticle :public BaseEffect , EfcCollider
{
public:
	EfcRandomImageParticle() {};
	EfcRandomImageParticle(DirectX::SimpleMath::Vector2 _pos, DirectX::SimpleMath::Vector2 _vel, float _duration = 40.f, float _deceleration_rate_mul = 0.9f, float _gravity = 0.f);
	EfcRandomImageParticle(const EffectParams& _params);
	EfcRandomImageParticle(const EffectParams& _params, EffectEmitter* _parent_emitter = nullptr);
	~EfcRandomImageParticle() { parent_emitter = nullptr; }

	bool Update()override;
	void Draw(DirectX::SimpleMath::Vector2 _pos = { 0,0 })override;

	bool IsHit()override;

private:
	bool is_reverse_x;
};

class EfcJewerly :public BaseEffect, EfcCollider
{
public:
	enum eJewerlyColors
	{
		Jew_R,
		Jew_G,
		Jew_B,
		Jew_Y,
		Jew_End,
	};
	static std::string jewerly_efc_filename[eJewerlyColors::Jew_End];

public:
	EfcJewerly() {}
	EfcJewerly(DirectX::SimpleMath::Vector2 _pos, DirectX::SimpleMath::Vector2 _vel, float _duration = 40.f, float _deceleration_rate_mul = 0.9f, float _gravity = 0.f);
	EfcJewerly(const EffectParams& _params);
	EfcJewerly(const EffectParams& _params, EffectEmitter* _parent_emitter = nullptr);
	~EfcJewerly();

	void EmitHitEfc();
	bool Update()override;
	void Draw(DirectX::SimpleMath::Vector2 _line_prev_pos = { 0,0 })override;

	bool IsHit()override;

private:
	DirectX::SimpleMath::Vector2 attrack_pos_randomize;
	bool is_reverse_x;
	bool is_near_to_pl;
	float attract_timer;
	int flare_type;// フレアの種類
	int flare_angle;
};
