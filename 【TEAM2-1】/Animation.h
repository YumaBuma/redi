#pragma once
#include "common.h"
#include <string>
#include <vector>

using namespace std;
class AnimParameter
{
public:
	bool isLoop;
	int max_aFrame;
	int animSpeed;//1 :fast --> 60 :slow

public:
	AnimParameter() :isLoop((bool)1), max_aFrame(1), animSpeed(1){};
	AnimParameter(int _max_aFrame, int _aSpeed, bool isLoop);
	~AnimParameter() {};


};


class Animation
{
public:
	int  group, id;

	int  aFrame;
	int  aCnt;
	bool stop_anim;
	int MAX_ANIMATION;
	int textureNum;

	int alpha;


public:
	std::vector<AnimParameter> animParam;
	Vector2 tex_pos;
	INT2 tex_size;
	bool isAnimation;
	int  now_anim;
	int  anim_prev;
	bool isStop;
	INT2 centerPos;
	double angle;

	std::vector<Animation> multianimData;

public:
	Animation()
		:aFrame(0), aCnt(0), tex_size(0, 0), now_anim(0), anim_prev(0),
		isFlipX(false), isStop(false)
	{
		group = 0;
		id = 0;
		isStop = false;
		alpha = 255;
	}
	virtual ~Animation() 
	{
		animParam.erase(animParam.begin(), animParam.end());
	}
	void playAnimation();
	virtual void changeAnimation(int _anim_index);

	const int getaFrame()const { return aFrame; }
	void setaFrame(int _aFrame) { aFrame = _aFrame; }

public:
	void add(int _max_aFrame, int _aSpeed, bool _isLoop);
	void loadAnimFile(string _name);
	bool isFlipX;
	
public:
	void InputFile(string txt);
	void OutputFile(string txt);
	void animImGui(string _tag);
	void MultianimImGui(string _tag);
	void MultiInputFile(string txt, int _group, int _id);
	void MultiOutputFile(string txt);
	
	void loadInfo(Vector2 _texPos,INT2 _texSize,int _texNum);
	void addAnimation(bool _loop,int _maxFrame,int _animSpeed);

};
