#include "FunctionDxLib.h"
#include "Camera.h"

using namespace DirectX::SimpleMath;

#pragma region primitive

void DrawBox(float _x1, float _y1, float _x2, float _y2, const DirectX::SimpleMath::Color& _color, int _fill_flag)
{
	UNREFERENCED_PARAMETER(_fill_flag);
	drawRect
	(
		  _x1
		, _y1
		, _x2 - _x1
		, _y2 - _y1
		, 0.f, 0.f
		, 0.f
		, XYZW_F(_color)
	);
}

void DrawLineBox(float _x1, float _y1, float _x2, float _y2, const DirectX::SimpleMath::Color& _color)
{
	drawLine
	(
		  _x1
		, _y1
		, _x2
		, _y2
		, XYZ_F(_color)
	);
}

void DrawLine(float _x1, float _y1, float _x2, float _y2, const DirectX::SimpleMath::Color& _color, int _thickness)
{
	drawLine
	(
		  _x1
		, _y1
		, _x2
		, _y2
		, (float)_thickness
		, XYZ_F(_color)
	);
}

void DrawCircle(float _x, float _y, float _r, const DirectX::SimpleMath::Color& _color, int _fill_flag, int _line_thickness, int _n)
{
	UNREFERENCED_PARAMETER(_fill_flag);
	UNREFERENCED_PARAMETER(_line_thickness);
	drawCircle
	(
		_x
		, _y
		, _r
		, XYZ_F(_color)
		, _n
	);
}

void DrawPixel(int _x, int _y, const DirectX::SimpleMath::Color& _color)
{
	DrawBox
	(
		_x
		, _y
		, _x + 1.f
		, _y + 1.f
		, _color
		, 0
	);
}

void DrawTriangle(float _x1, float _y1, float _x2, float _y2, float _x3, float _y3, const DirectX::SimpleMath::Color& _color, int _fill_flag)
{
	DirectX::XMFLOAT2 v[4] = 
	{
		 {_x1,_y1}
		,{_x2,_y2}
		,{_x3,_y3}
		,{_x3,_y3}
	};
	drawQuad(v, XYZ_F(_color));
}

void DrawLineCam(const Vector2& _pos1, const Vector2& _pos2, const DirectX::SimpleMath::Color& _color, const int& _thickness)
{
	DrawLine
	(
		cam.xi_ext(_pos1.x),
		cam.yi_ext(_pos1.y),
		cam.xi_ext(_pos2.x),
		cam.yi_ext(_pos2.y),
		_color,
		_thickness
	);
}

void DrawBoxCam(const Vector2& _pos1, const Vector2& _pos2, const DirectX::SimpleMath::Color& _color, const int& _fill_flag)
{
	DrawBox
	(
		cam.xi_ext(_pos1.x),
		cam.yi_ext(_pos1.y),
		cam.xi_ext(_pos2.x),
		cam.yi_ext(_pos2.y),
		_color,
		_fill_flag
	);
}

void DrawCircleCam(const Vector2& _pos, const float& _radius, const DirectX::SimpleMath::Color& _color, const int& _fill_flag, const int& _line_thickness, int _n)
{
	DrawCircle
	(
		cam.xi_ext(_pos.x),
		cam.yi_ext(_pos.y),
		static_cast<int>(_radius * cam.chipextrate()),
		_color,
		_fill_flag,
		_line_thickness,
		_n
	);
}

#pragma endregion

#pragma region sprite

void DrawGraph(float _x, float _y, Sprite* _sprite, int _trans_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	spriterender(_sprite, _x, _y);
}

void DrawExtendGraph(float _x1, float _y1, float _x2, float _y2, Sprite* _sprite, int _trans_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Vector2 mag = _sprite->getSize();
	mag.x = (_x2 - _x1) / mag.x;
	mag.y = (_y2 - _y1) / mag.y;
	spriteRenderExtend2(_sprite, _x1, _y1, mag.x, mag.y);
}

void DrawRectGraph(float _dest_x, float _dest_y, float _src_x, float _src_y, float _width, float _height, Sprite* _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	spriteRenderRectExtend(_sprite, _dest_x, _dest_y, _src_x, _src_y, _width, _height, 1.0f, _reverse_x_flag, _reverse_y_flag);
}

void DrawRectExtendGraph(float _dest_x1, float _dest_y1, float _dest_x2, float _dest_y2, float _src_x, float _src_y, float _src_width, float _src_height, Sprite* _sprite, int _trans_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Vector2 mag = _sprite->getSize();
	mag.x = (_dest_x2 - _dest_x1) / mag.x;
	mag.y = (_dest_y2 - _dest_y1) / mag.y;
	spriteRenderRectExtend2(_sprite, _dest_x1, _dest_y1, _src_x, _src_y, _src_width, _src_height, mag.x, mag.y);
}

void DrawRectRotaGraph(float _x, float _y, float _src_x, float _src_y, float _width, float _height, float _ext_rate, float _angle, Sprite* _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	spriteRenderRectRota2(_sprite, _x, _y, _src_x, _src_y, _width, _height, _ext_rate, _ext_rate, _angle, _reverse_x_flag, _reverse_y_flag);
}

void DrawRectRotaGraph2(float _x, float _y, float _src_x, float _src_y, float _width, float _height, float _cx, float _cy, float _ext_rate, float _angle, Sprite* _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	spriteRenderRectRota3(_sprite, _x, _y, _src_x, _src_y, _width, _height, _cx, _cy, _ext_rate, _ext_rate, _angle, _reverse_x_flag, _reverse_y_flag);
}

void DrawRectRotaGraph3(float _x, float _y, float _src_x, float _src_y, float _width, float _height, float _cx, float _cy, float _ext_rate_x, float _ext_rate_y, float _angle, Sprite* _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	spriteRenderRectRota3(_sprite, _x, _y, _src_x, _src_y, _width, _height, _cx, _cy, _ext_rate_x, _ext_rate_y, _angle, _reverse_x_flag, _reverse_y_flag);
}

#pragma endregion

#pragma region sprite int

void DrawGraph(float _x, float _y, int _sprite, int _trans_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	spriterender(ptr, _x, _y);
}

void DrawExtendGraph(float _x1, float _y1, float _x2, float _y2, int _sprite, int _trans_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	Vector2 mag = ptr->getSize();
	mag.x = (_x2 - _x1) / mag.x;
	mag.y = (_y2 - _y1) / mag.y;
	spriteRenderExtend2(ptr, _x1, _y1, mag.x, mag.y);
}

void DrawRectGraph(float _dest_x, float _dest_y, float _src_x, float _src_y, float _width, float _height, int _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	spriteRenderRectExtend(ptr, _dest_x, _dest_y, _src_x, _src_y, _width, _height, 1.0f, _reverse_x_flag, _reverse_y_flag);
}

void DrawRectExtendGraph(float _dest_x1, float _dest_y1, float _dest_x2, float _dest_y2, float _src_x, float _src_y, float _src_width, float _src_height, int _sprite, int _trans_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	Vector2 mag = ptr->getSize();
	mag.x = (_dest_x2 - _dest_x1) / mag.x;
	mag.y = (_dest_y2 - _dest_y1) / mag.y;
	spriteRenderRectExtend2(ptr, _dest_x1, _dest_y1, _src_x, _src_y, _src_width, _src_height, mag.x, mag.y);
}

void DrawRectRotaGraph(float _x, float _y, float _src_x, float _src_y, float _width, float _height, float _ext_rate, float _angle, int _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	spriteRenderRectRota2(ptr, _x, _y, _src_x, _src_y, _width, _height, _ext_rate, _ext_rate, _angle, _reverse_x_flag, _reverse_y_flag);
}

void DrawRectRotaGraph2(float _x, float _y, float _src_x, float _src_y, float _width, float _height, float _cx, float _cy, float _ext_rate, float _angle, int _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	spriteRenderRectRota3(ptr, _x, _y, _src_x, _src_y, _width, _height, _cx, _cy, _ext_rate, _ext_rate, _angle, _reverse_x_flag, _reverse_y_flag);
}

void DrawRectRotaGraph3(float _x, float _y, float _src_x, float _src_y, float _width, float _height, float _cx, float _cy, float _ext_rate_x, float _ext_rate_y, float _angle, int _sprite, int _trans_flag, int _reverse_x_flag, int _reverse_y_flag)
{
	UNREFERENCED_PARAMETER(_trans_flag);
	Sprite* ptr = reinterpret_cast<Sprite*>(_sprite);
	spriteRenderRectRota3(ptr, _x, _y, _src_x, _src_y, _width, _height, _cx, _cy, _ext_rate_x, _ext_rate_y, _angle, _reverse_x_flag, _reverse_y_flag);
}

#pragma endregion


void SetDrawBlendMode(int _blend_mode, int _blend_param)
{
	float alpha = (float)_blend_param / 255.f;
	switch (_blend_mode)
	{
	case DX_BLENDMODE_NOBLEND:setBlendMode_NONE		(alpha); break;
	case DX_BLENDMODE_ALPHA:  setBlendMode_ALPHA	(alpha); break;
	case DX_BLENDMODE_ADD:	  setBlendMode_ADD		(alpha); break;
	case DX_BLENDMODE_SUB:	  setBlendMode_SUBTRACT	(alpha); break;
	case DX_BLENDMODE_MUL:	  setBlendMode_MULTIPLY	(alpha); break;
	default: break;
	}
}

void GetDrawBlendMode(int* _blend_mode, int* _blend_param)
{
	ID3D11BlendState* blend_state;
	FLOAT factor[4]{};
	GameLib::getContext()->OMGetBlendState(&blend_state, factor, nullptr);
	// 現在のブレンドモードの取得
}
