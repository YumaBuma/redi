#include "bgObjectManager.h"
#include "ObjectManager.h"
#include "Input.h"
#include "Camera.h"
#include "bgObject.h"
#include "Elevator.h"
#include "src\imgui.h"

Vector2 objBGManager::pos[2] = { { 0,0 },{ 0,0 } };
int objBGManager::gritSize[2] = { 32,32 };

void objBGManager::init()
{
	for (int i = 0; i < 4; i++)
	{
		selected[i] = false;
	}
	pos[now] = { 0,0 };
	pos[previous] = { 0,0 };
	layer = 0;
	useGrid = false;
	gritSize[x] = 32;
	gritSize[y] = 32;
	comparision = false;
	useMouse = false;
	gripObj = false;
	noTouchWall = false;
	mouseButtons[2] = { 0 };
	listbox_group_current[2] = { 0 };
	listbox_item_current[2] = { 0 };
	textureEditFlg = 0;
	textureNumber = 0;
}

void objBGManager::update()
{
	mouseButtons[now] = GetMouseInput();
	cam.SwitchIsUsePtr(false);

	cam.FollowMouse();

	if (!useMouse)
		praPos = { cam.xf(pos[now].x),cam.yf(pos[now].y) };

	mousePos[prevX] = mousePos[x];
	mousePos[prevY] = mousePos[y];



	//カーソル移動
	{

		if (key[KEY_INPUT_W] == 1 || key[KEY_INPUT_UP] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				if (pos[now].y >= 32)
				{
					pos[now].y -= 32;
					if ((int)pos[now].y % 32 != 0)
						pos[now].y = ((int)pos[now].y / 32 + 1) * 32;
				}
			}
			else if (pos[now].y > 0)
			{
				pos[now].y--;
			}
		}
		else if (key[KEY_INPUT_S] == 1 || key[KEY_INPUT_DOWN] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				pos[now].y += 32;
				pos[now].y = (int)pos[now].y / 32 * 32;
			}
			else
				pos[now].y++;
		}
		else if (key[KEY_INPUT_D] == 1 || key[KEY_INPUT_RIGHT] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				pos[now].x += 32;
				pos[now].x = ((int)pos[now].x) / 32 * 32;
			}
			else
				pos[now].x++;
		}
		else if (key[KEY_INPUT_A] == 1 || key[KEY_INPUT_LEFT] == 1)
		{
			if ((key[KEY_INPUT_RSHIFT] == 0) && (key[KEY_INPUT_LSHIFT]) == 0)
			{
				if (pos[now].x >= 32)
				{
					pos[now].x -= 32;
					if ((int)pos[now].x % 32 != 0)
						pos[now].x = ((int)pos[now].x / 32 + 1) * 32;
				}
			}
			else if (pos[now].x > 0)
			{
				pos[now].x--;
			}
		}

		if (useMouse)
		{
			mousePos[x] = (int)cam.GetWorldPosFromCursorPos().x;
			mousePos[y] = (int)cam.GetWorldPosFromCursorPos().y;

			if (useGrid)
			{
				float	ext = cam.chipextrate_target();
				//int		gridSizeTmpX	= gritSize[x] / (int)(1.f / ext);
				//int		gridSizeTmpY	= gritSize[y] / (int)(1.f / ext);

				mousePos[x] = (mousePos[x] / 32) * 32;
				mousePos[y] = (mousePos[y] / 32) * 32;
			}

			pos[now] = { (float)mousePos[x],(float)mousePos[y] };
			praPos = { cam.xf_ext(pos[now].x),cam.yf_ext(pos[now].y) };
		}

	}

	if (!selected[put] && !selected[move] && !selected[remove] && !selected[copy])return;

	//設置
	{

		if (selected[put])
		{
			if (CopyList.empty() == false)
			{
				CopyList.back()->pos = { pos[now].x + gritSize[x] / 2,pos[now].y + gritSize[y] };
			}

			if ((listbox_group_current[now] != listbox_group_current[previous]) || (listbox_item_current[now] != listbox_item_current[previous]))
			{
				if (CopyList.empty() == false)
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						p->get()->isErase = true;
					}


					CopyList.erase(CopyList.begin(), CopyList.end());

					for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
					{
						if (p->second->isErase)
							p = pDrawList->drawables.erase(p);
						else
							p++;
					}

				}
				//switch (listbox_group_current[now])
				//{
				//case Wall:
				//	layer = -10;
				//	break;
				//default:
				//	layer = 0;
				//}

				CopyList.emplace_back(std::make_shared<BGObject>());
				{
					CopyList.back()->id = { listbox_group_current[now],listbox_item_current[now] };
					CopyList.back()->layer = layer;
					CopyList.back()->Init();
					CopyList.back()->isErase = false;
					CopyList.back()->isGrip = true;
					for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
					{
						if (it->group != CopyList.back()->id.CLASS)
							continue;
						if (it->id != CopyList.back()->id.PERSONAL)
							continue;

						CopyList.back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
						for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
						{
							CopyList.back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
						}
						break;
					}
					gritSize[x] = CopyList.back()->tex_size.x;
					gritSize[y] = CopyList.back()->tex_size.y;
					CopyList.back()->pos = { pos[now].x + gritSize[x] / 2,pos[now].y + gritSize[y] };

					pDrawList->add(CopyList.back(), layer);
				}
			}

			if (useMouse)
			{
				if ((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				{

					setObj(listbox_group_current[now], listbox_item_current[now], { pos[now].x + gritSize[x] / 2, pos[now].y + gritSize[y] }, false);
				}

				else if (useGrid)
				{
					if ((mouseButtons[now] & MOUSE_INPUT_RIGHT) && pos[previous] != pos[now])
					{
						setObj(listbox_group_current[now], listbox_item_current[now], { pos[now].x + gritSize[x] / 2, pos[now].y + gritSize[y] }, false);
					}
				}

			}
			else if (key[KEY_INPUT_RETURN] == 1)
			{
				setObj(listbox_group_current[now], listbox_item_current[now], { pos[now].x + gritSize[x] / 2, pos[now].y + gritSize[y] }, false);
			}
		}
	}

	//コピー
	{

		if (selected[copy])
		{
			if (gripObj)
			{
				if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
					|| key[KEY_INPUT_RETURN] == 1)
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						setObj(p->get()->id.CLASS, p->get()->id.PERSONAL, { p->get()->pos.x, p->get()->pos.y }, false);
					}
				}

				else
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						if (p->get()->isGrip)
							p->get()->pos += { pos[now].x - pos[previous].x, pos[now].y - pos[previous].y };
					}

					if (useGrid && (mouseButtons[now] & MOUSE_INPUT_RIGHT) && pos[previous] != pos[now])
					{

						for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
						{
							setObj(p->get()->id.CLASS, p->get()->id.PERSONAL, { p->get()->pos.x, p->get()->pos.y }, false);
						}
					}

				}

				if (key[KEY_INPUT_DELETE] == 1)
				{
					for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
					{
						p->get()->isErase = true;
					}

					CopyList.erase(CopyList.begin(), CopyList.end());

					for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
					{
						if (p->second->isErase)
							p = pDrawList->drawables.erase(p);
						else
							p++;
					}

					gripObj = false;
				}
			}

			else if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				|| key[KEY_INPUT_RETURN] == 1)
			{
				for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
				{
					if (pObjManager->ObjList[i].empty() == false)
					{
						for (auto& p = pObjManager->ObjList[i].begin(), end = pObjManager->ObjList[i].end(); p != end; p++)
						{
							if ((pos[now].x <= p->get()->pos.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x) && (pos[now].y <= p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y))
							{
								if (!noTouchWall)
								{
									if (p->get()->id.CLASS == bgObjectGroup::Wall)
										continue;
								}
								gripObj = true;
								//p->get()->isGrip = true;
								CopyList.emplace_back(std::make_shared<BGObject>());
								CopyList.back()->id = { p->get()->id.CLASS,p->get()->id.PERSONAL };
								CopyList.back()->layer = p->get()->layer;
								CopyList.back()->Init();
								CopyList.back()->pos = p->get()->pos;
								CopyList.back()->isGrip = true;
								CopyList.back()->isErase = false;
								for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
								{
									if (it->group != CopyList.back()->id.CLASS)
										continue;
									if (it->id != CopyList.back()->id.PERSONAL)
										continue;

									CopyList.back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
									for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
									{
										CopyList.back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
									}
									break;
								}
								pDrawList->add(CopyList.back(), 0);

							}
						}

					}
				}
			}


		}

	}

	//移動
	{

		if (selected[move])
		{
			if (gripObj)
			{
				if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
					|| key[KEY_INPUT_RETURN] == 1)
				{
					gripObj = false;
					for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
					{
						for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
						{
							p->get()->isGrip = false;
							p->get()->isErase = false;
						}
					}
				}

				else
				{
					for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
					{
						for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
						{
							if (p->get()->isGrip)
								p->get()->pos += { pos[now].x - pos[previous].x, pos[now].y - pos[previous].y };
						}
					}

				}
			}
			else if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				|| key[KEY_INPUT_RETURN] == 1)

			{
				for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
				{
					for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
					{
						if ((pos[now].x <= p->get()->pos.x + p->get()->size.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x - p->get()->size.x) && (pos[now].y <= p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y - p->get()->size.y))
						{
							if (!noTouchWall)
							{
								if (p->get()->id.CLASS == bgObjectGroup::Wall)
									continue;
							}
							p->get()->isGrip = true;
							p->get()->isErase = false;
							gripObj = true;
						}
					}
				}

			}
		}

	}

	//削除
	{

		if (selected[remove])
		{
			if (((mouseButtons[now] & MOUSE_INPUT_RIGHT) && !(mouseButtons[previous] & MOUSE_INPUT_RIGHT))
				|| key[KEY_INPUT_RETURN] == 1
				)
			{
				//OBJECT
				{
					for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
					{

						for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
						{
							if ((pos[now].x <= p->get()->pos.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x) &&
								(pos[now].y < p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y) &&
								(noTouchWall || (!noTouchWall && p->get()->id.CLASS != bgObjectGroup::Wall)))
								p->get()->isErase = true;
						}


						auto removeIt = std::remove_if(pObjManager->ObjList[i].begin(), pObjManager->ObjList[i].end(),
							[](std::shared_ptr<Object>& obj)
						{
							return obj->isErase == true;
						}
						);


						pObjManager->ObjList[i].erase(removeIt, pObjManager->ObjList[i].end());
					}
				}
				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}



			else if (useGrid && mouseButtons[now] & MOUSE_INPUT_RIGHT)
			{

				{
					//OBJECT
					{
						for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
						{

							for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
							{
								if ((pos[now].x <= p->get()->pos.x) && ((pos[now].x + gritSize[x]) >= p->get()->pos.x) &&
									(pos[now].y < p->get()->pos.y) && ((pos[now].y + gritSize[y]) >= p->get()->pos.y) &&
									(noTouchWall || (!noTouchWall && p->get()->id.CLASS != bgObjectGroup::Wall)))
									p->get()->isErase = true;
							}


							auto removeIt = std::remove_if(pObjManager->ObjList[i].begin(), pObjManager->ObjList[i].end(),
								[](std::shared_ptr<Object>& obj)
							{
								return obj->isErase == true;
							}
							);


							pObjManager->ObjList[i].erase(removeIt, pObjManager->ObjList[i].end());
						}
					}
					for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
					{
						if (p->second->isErase)
							p = pDrawList->drawables.erase(p);
						else
							p++;
					}
				}
			}


		}
	}

	//情報保存
	{
		mouseButtons[previous] = mouseButtons[now];
		pos[previous] = pos[now];

		listbox_group_current[previous] = listbox_group_current[now];
		listbox_item_current[previous] = listbox_item_current[now];
	}
}

void objBGManager::setObj(int _id, int _personalID, Vector2 _pos, bool move)
{
	int Layer = layer;
	bool hit = false;
	if (_id == bgObjectGroup::Wall)
	{
		Layer = -10;
		if (getTerrainAttr(_pos.x, _pos.y - gritSize[y] / 2) == ALL_BLOCK)
		{
			return;
		}
	}
	else if (Layer < -9)
	{
		Layer = -9;
	}

	for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
	{
		for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
		{
			if (hit)
				break;
			if (p->get()->id.CLASS != _id)
				continue;
			if (p->get()->id.PERSONAL != _personalID)
				continue;
			if (p->get()->layer != Layer)
				continue;
			if (selected[copy])
			{
				if (p->get()->pos == _pos)
					hit = true;
			}
			else if (p->get()->pos.x == pos[now].x + gritSize[x] / 2 && p->get()->pos.y == pos[now].y + gritSize[y])
				hit = true;

		}
	}
	//for (auto& p = pObjManager->ObjList[Object::OBSTACLE].begin(); p != pObjManager->ObjList[Object::OBSTACLE].end(); p++)
	//{
	//	if (hit)
	//		break;
	//	if (p->get()->id.CLASS != _id)
	//		continue;
	//	if (p->get()->id.PERSONAL != _personalID)
	//		continue;
	//	if (p->get()->layer != Layer)
	//		continue;
	//	if (selected[copy])
	//	{
	//		if (p->get()->pos == _pos)
	//			hit = true;
	//	}
	//	else if (p->get()->pos.x == pos[now].x + gritSize[x] / 2 && p->get()->pos.y == pos[now].y + gritSize[y])
	//		hit = true;

	//}


	if (hit)
		return;
	//エレベーター
	if (_id == bgObjectGroup::ELEVATOR_G && _personalID == elevatorType::elevator)
	{
		pObjManager->Add(std::make_shared<Elevator>(), Object::ELEVATOR, Layer, _id, _personalID);

		pObjManager->ObjList[Object::ELEVATOR].back()->pos = _pos;
		pObjManager->ObjList[Object::ELEVATOR].back()->isGrip = move;
		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::ELEVATOR].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::ELEVATOR].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::ELEVATOR].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::ELEVATOR].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//ストッパー
	else if (_id == bgObjectGroup::ELEVATOR_G && _personalID == elevatorType::stopper)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::STOPPER, Layer, _id, _personalID);

		pObjManager->ObjList[Object::STOPPER].back()->pos = _pos;
		pObjManager->ObjList[Object::STOPPER].back()->isGrip = move;
		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::STOPPER].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::STOPPER].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::STOPPER].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::STOPPER].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}

	//テレポーター
	else if (_id == bgObjectGroup::Teleport)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::TEREPORTER, Layer, _id, _personalID);

		pObjManager->ObjList[Object::TEREPORTER].back()->pos = _pos;
		pObjManager->ObjList[Object::TEREPORTER].back()->isGrip = move;
		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::TEREPORTER].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::TEREPORTER].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::TEREPORTER].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::TEREPORTER].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//トラップ類
	else if (_id == bgObjectGroup::Trap || _id == bgObjectGroup::Needle)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::TRAP, Layer, _id, _personalID);

		pObjManager->ObjList[Object::TRAP].back()->pos = _pos;
		pObjManager->ObjList[Object::TRAP].back()->isGrip = move;
		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::TRAP].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::TRAP].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::TRAP].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			if (_id == bgObjectGroup::Trap && _personalID == trapType::axe)
			{
				pObjManager->ObjList[Object::TRAP].back()->centerPos = { (float)(it->tex_size.x / 2.0f),0 };
				pObjManager->ObjList[Object::TRAP].back()->pos.y -= it->tex_size.y;
			}

			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::TRAP].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//ドア
	else if (_id == bgObjectGroup::Door)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::DOOR, Layer, _id, _personalID);

		pObjManager->ObjList[Object::DOOR].back()->pos = _pos;
		pObjManager->ObjList[Object::DOOR].back()->isGrip = move;
		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::DOOR].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::DOOR].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::DOOR].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::DOOR].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//スタート
	else if (_id == bgObjectGroup::Start)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::START, Layer, _id, _personalID);

		pObjManager->ObjList[Object::START].back()->pos = _pos;
		pObjManager->ObjList[Object::START].back()->isGrip = move;
		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::START].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::START].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::START].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::START].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//trasureBox
	else if (_id == bgObjectGroup::TreasureBox)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::TREASUREBOX, Layer, _id, _personalID);
		pObjManager->ObjList[Object::TREASUREBOX].back()->pos = _pos;

		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::TREASUREBOX].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::TREASUREBOX].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::TREASUREBOX].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::TREASUREBOX].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//ゴール
	else if (_id == bgObjectGroup::Goal)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::GOAL, Layer, _id, _personalID);
		pObjManager->ObjList[Object::GOAL].back()->pos = _pos;

		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::GOAL].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::GOAL].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::GOAL].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::GOAL].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//ショップ
	else if (_id == bgObjectGroup::ShopMaster)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::SHOPMAN, Layer, _id, _personalID);
		pObjManager->ObjList[Object::SHOPMAN].back()->pos = _pos;

		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::SHOPMAN].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::SHOPMAN].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::SHOPMAN].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::SHOPMAN].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//エレベーター関連残り
	else if (_id == bgObjectGroup::ELEVATOR_G)
	{
		pObjManager->Add(std::make_shared<Obstacles>(), Object::OBSTACLE, Layer, _id, _personalID);
		pObjManager->ObjList[Object::OBSTACLE].back()->pos = _pos;

		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::OBSTACLE].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::OBSTACLE].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::OBSTACLE].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::OBSTACLE].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;
		}
	}
	//その他オブジェクト
	else
	{
		pObjManager->Add(std::make_shared<BGObject>(), Object::OBJECT, Layer, _id, _personalID);
		pObjManager->ObjList[Object::OBJECT].back()->pos = _pos;

		for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
		{
			if (it->group != pObjManager->ObjList[Object::OBJECT].back()->id.CLASS)
				continue;
			if (it->id != pObjManager->ObjList[Object::OBJECT].back()->id.PERSONAL)
				continue;

			pObjManager->ObjList[Object::OBJECT].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
			if (_id == bgObjectGroup::Trap && _personalID == trapType::axe)
			{
				pObjManager->ObjList[Object::OBJECT].back()->centerPos = { (float)(it->tex_size.x / 2.0f),0 };
				pObjManager->ObjList[Object::OBJECT].back()->pos.y -= it->tex_size.y;
			}

			for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
			{
				pObjManager->ObjList[Object::OBJECT].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
			}
			break;

		}

	}
}


void objBGManager::Debug()
{
	printfDx("position : x->%f,y->%f\n", pos[now].x, pos[now].y);
	printfDx("Praposition : x->%f,y->%f", praPos.x, praPos.y);
	printfDx("input : %d,befor %d\n", mouseButtons[now] & MOUSE_INPUT_RIGHT, mouseButtons[previous] & MOUSE_INPUT_RIGHT);
	printfDx("オブジェクトの個数 %d", pObjManager->ObjList[Object::OBJECT].size());
	printfDx("ゴールの個数 %d", pObjManager->ObjList[Object::GOAL].size());

}


void objBGManager::draw()
{
	Vector2 gridSizeTmp;
	gridSizeTmp.x = (float)gritSize[x];
	gridSizeTmp.y = (float)gritSize[y];
	gridSizeTmp *= cam.chipextrate();

	DrawLineBox
	(
		praPos.x,
		praPos.y,
		praPos.x + (int)gridSizeTmp.x,
		praPos.y + (int)gridSizeTmp.y,
		GetColor(255, 0, 0)
	);

}


void objBGManager::ImGui()
{

	if (!Iwin_flg.objBG)
	{
		if (CopyList.empty() == false)
		{
			for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
			{
				p->get()->isErase = true;
			}

			CopyList.erase(CopyList.begin(), CopyList.end());

			for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
			{
				if (p->second->isErase)
					p = pDrawList->drawables.erase(p);
				else
					p++;
			}
		}
		textureEditFlg = false;
		return;
	}

	const char* listbox_group =
	{ "lamp\0woodBox1\0woodBox2\0woodBoxSet\0needle\0door\0spiderweb\0sand\0ladder\0wall\0grass\0net\0grave\0treasureBox\0shop\0goal\0Start\0teleporter\0elevator\0trap\0wood\0\0" };
	const char* listbox_id[] =
	{ "animLamp\0lamp1\0lamp2\0lamp3\0lamp4\0\0",
		"smallWoodBox1\0smallWoodBox2\0smallWoodBox3\0bigWoodBox1\0bigWoodBox2\0bigWoodBox3\0smallBarrel\0bigBarrel\0\0",
		"smallWoodBox1\0smallWoodBox2\0smallWoodBox3\0bigWoodBox1\0bigWoodBox2\0bigWoodBox3\0smallBarrel\0bigBarrel\0\0",
		"set1\0set2\0set3\0set4\0set5\0set6\0set7\0set8\0set9\0set10\0set11\0\0",
		"u_needle\0r_needle\0d_needle\0l_needle\0\0",
		"door\0\0",
		"smallSpiderWeb1\0smallSpiderWeb2\0smallSpiderWeb3\0smallSpiderWeb4\0midiumSpiderWeb1\0midiumSpiderWeb2\0bigSpiderWeb1\0bigSpiderWeb2\0bigSpiderWeb3\0bigSpiderWeb4\0\0",
		"sand1\0sand2\0sand3\0sand4\0sand5\0sand6\0\0",
		"u_ladder\0g_ladder\0margeLadder\0\0",
		"fillWall_under\0fillWall_ground\0holeWall1\0holeWall2\0holeWall3\0holeWall4\0up1\0up2\0up3\0up4\0up5\0up6\0down1\0down2\0left1\0left2\0right1\0right2\0down3\0down4\0down5\0down6\0down7\0down8\0upSet\0upSet2\0leftSet\0downSet\0rightSet\0downSet\0dGrass1\0dGrass2\0dGrass3\0dGrassSet\0holeWall34\0holeWall35\0holeWall36\0\0",
		"grass1\0grass2\0grass3\0grass4\0grass5\0grass6\0\0",
		"net1\0net2\0\0",
		"grave1_1\0grave1_2\0grave1_3\0grave2_1\0grave2_2\0grave2_3\0grave3_1\0grave3_2\0grave3_3\0\0",
		"copper\0silver\0gold\0\0",
		"shopMen\0\0",
		"groundGoal\0underGoal\0\0",
		"groundStart\0underStart\0\0",
		"teleporter\0\0",
		"rope\0elevator\0lever\0stopper\0\0",
		"axe\0moveFloor\0\0",
		"wood\0\0"

	};

	ImGui::Begin("bgObj", &Iwin_flg.objBG, ImGuiWindowFlags_MenuBar);

	//メニューバー
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("Menu"))
			{
				if (ImGui::MenuItem("Save"))
				{
					saveFile();
				}
				if (ImGui::MenuItem("Exit"))
				{
					Iwin_flg.objBG ^= 1;
				}

				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
		}
	}

	ImGui::Checkbox("use Mouse", &useMouse);
	ImGui::Checkbox("use Grit", &useGrid);
	ImGui::Checkbox("equal rate", &comparision);
	ImGui::Checkbox("touchWall", &noTouchWall);


	ImGui::Selectable("put", &selected[put]);
	if (selected[put])
	{
		textureEditFlg = false;
		if (selected[copy] || selected[remove] || selected[move])
		{
			if (gripObj)
			{
				gripObj = false;
				for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
				{


					for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
					{
						p->get()->isGrip = false;
					}
				}
			}

			if (CopyList.empty() == false)
			{

				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}

			selected[move] = false;
			selected[copy] = false;
			selected[remove] = false;
		}

		if (CopyList.empty() == true)
		{
			CopyList.emplace_back(std::make_shared<BGObject>());
			{
				switch (listbox_group_current[now])
				{
				case Wall:
					layer = -10;
					break;
				default:
					layer = 0;
				}
				CopyList.back()->id = { listbox_group_current[now],listbox_item_current[now] };
				CopyList.back()->layer = 4;
				CopyList.back()->Init();
				CopyList.back()->isErase = false;
				CopyList.back()->isGrip = true;
				for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
				{
					if (it->group != CopyList.back()->id.CLASS)
						continue;
					if (it->id != CopyList.back()->id.PERSONAL)
						continue;

					CopyList.back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
					for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
					{
						CopyList.back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
					}
					break;
				}
				gritSize[x] = CopyList.back()->tex_size.x;
				gritSize[y] = CopyList.back()->tex_size.y;
				CopyList.back()->pos = { pos[now].x + gritSize[x] / 2,pos[now].y + gritSize[y] };

				pDrawList->add(CopyList.back(), 0);
			}

		}


		if (ImGui::Combo("select item", &listbox_group_current[now], listbox_group))
		{
			if (listbox_group_current[now] != listbox_group_current[previous])
				listbox_item_current[now] = 0;

		}

		ImGui::Combo("select id", &listbox_item_current[now], listbox_id[listbox_group_current[now]]);
		ImGui::DragInt("layerNumber", &layer);


	}

	ImGui::Selectable("copy", &selected[copy]);
	if (selected[copy])
	{
		textureEditFlg = false;
		{
			ImGui::Text("gritSize");
			ImGui::DragInt2(" ", gritSize, 1, 32, 600);
			if (comparision)
				gritSize[y] = gritSize[x];
		}
		if (selected[remove] || selected[put] || selected[move])
		{
			if (gripObj)
			{
				gripObj = false;
				for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
				{


					for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
					{
						p->get()->isGrip = false;
					}
				}
			}

			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			selected[put] = false;
			selected[move] = false;
			selected[remove] = false;
		}
	}

	ImGui::Selectable("move", &selected[move]);
	if (selected[move])
	{
		textureEditFlg = false;
		{
			ImGui::Text("gritSize");
			ImGui::DragInt2(" ", gritSize, 1, 32, 600);
			if (comparision)
				gritSize[y] = gritSize[x];
		}
		if (selected[copy] || selected[put] || selected[remove])
		{
			if (gripObj)
			{
				gripObj = false;
				for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
				{


					for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
					{
						p->get()->isGrip = false;
					}
				}
			}

			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			selected[copy] = false;
			selected[put] = false;
			selected[remove] = false;
		}
	}

	ImGui::Selectable("remove", &selected[remove]);
	if (selected[remove])
	{
		textureEditFlg = false;
		{
			ImGui::Text("gritSize");
			ImGui::DragInt2(" ", gritSize, 1, 32, 600);
			if (comparision)
				gritSize[y] = gritSize[x];
		}
		if (selected[copy] || selected[put] || selected[move])
		{
			if (gripObj)
			{
				gripObj = false;
				for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
				{


					for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
					{
						p->get()->isGrip = false;
					}
				}
			}

			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			selected[copy] = false;
			selected[put] = false;
			selected[move] = false;
		}
	}

	ImGui::NewLine();


	//リセットボタン
	{

		if (ImGui::Button("reset(all object delete)"))
		{
			for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
			{
				for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
				{
					p->get()->isErase = true;
				}


				pObjManager->ObjList[i].erase(pObjManager->ObjList[i].begin(), pObjManager->ObjList[i].end());

			}

			for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
			{
				if (p->second->isErase)
					p = pDrawList->drawables.erase(p);
				else
					p++;
			}


			if (CopyList.empty() == false)
			{
				for (auto& p = CopyList.begin(); p != CopyList.end(); p++)
				{
					p->get()->isErase = true;
				}

				CopyList.erase(CopyList.begin(), CopyList.end());

				for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
				{
					if (p->second->isErase)
						p = pDrawList->drawables.erase(p);
					else
						p++;
				}
			}
			for (int i = 0; i < 4; i++)
			{
				selected[i] = false;
			}
		}

	}

	//切取りエディット
	if (ImGui::Button("ANIMATION_DETAIL"))
	{
		if (textureEditFlg)
		{
			CopyList.back()->isErase = true;
			CopyList.erase(CopyList.begin(), CopyList.end());
			for (auto& p = pDrawList->drawables.begin(); p != pDrawList->drawables.end();)
			{
				if (p->second->isErase)
					p = pDrawList->drawables.erase(p);
				else
					p++;
			}
		}



		textureEditFlg ^= 1;
		for (int i = 0; i < select::end; i++)
		{
			selected[i] = false;
		}

	}

	ImGui::End();

	MultianimImGui("a");
}


void objBGManager::loadFile()
{
	MultiInputFile("objects");

	Obstacles::loadFile();

	FILE* file = nullptr;
	char fileName[30] = { 0 };
	int objectNum = 0;
	sprintf(fileName, "Data/Object/Stage%d-%d.txt", pBG->now_stage, pBG->stageNumber);
	char* str = fileProcessing::readTextFile(fileName);

	if (str == nullptr)
		return;
	//項目数(縦、横)を計測
	for (int i = 0; i < strlen(str); i++) {
		switch (str[i]) {
		case '\n':
			objectNum++;
			break;
		}
	}

	if (fopen_s(&file, fileName, "r"))
		return;

	for (int i = 0; i < objectNum; i++)
	{
		int pID;
		int ID;
		int lay;
		Vector2 p;
		fscanf_s(file, "%d", &ID);
		fscanf_s(file, "%d", &pID);
		fscanf_s(file, "%f", &p.x);
		fscanf_s(file, "%f", &p.y);
		fscanf_s(file, "%d", &lay);

		//エレベーター
		if (ID == bgObjectGroup::ELEVATOR_G && pID == elevatorType::elevator)
		{
			pObjManager->Add(std::make_shared<Elevator>(), Object::ELEVATOR, lay, ID, pID);

			pObjManager->ObjList[Object::ELEVATOR].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::ELEVATOR].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::ELEVATOR].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::ELEVATOR].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::ELEVATOR].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}

		//エレベーターストッパー
		else if (ID == bgObjectGroup::ELEVATOR_G && pID == elevatorType::stopper)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::STOPPER, lay, ID, pID);

			pObjManager->ObjList[Object::STOPPER].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::STOPPER].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::STOPPER].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::STOPPER].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::STOPPER].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}


		//テレポーター
		else if (ID == bgObjectGroup::Teleport)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::TEREPORTER, lay, ID, pID);

			pObjManager->ObjList[Object::TEREPORTER].back()->pos = p;
			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::TEREPORTER].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::TEREPORTER].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::TEREPORTER].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::TEREPORTER].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}
		//トラップ類
		else if (ID == bgObjectGroup::Trap || ID == bgObjectGroup::Needle)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::TRAP, lay, ID, pID);

			pObjManager->ObjList[Object::TRAP].back()->pos = p;
			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::TRAP].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::TRAP].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::TRAP].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				if (ID == bgObjectGroup::Trap && pID == trapType::axe)
				{
					pObjManager->ObjList[Object::TRAP].back()->centerPos = { (float)(it->tex_size.x / 2.0f),0 };
					pObjManager->ObjList[Object::TRAP].back()->pos.y -= it->tex_size.y;
				}

				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::TRAP].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}
		//ドア
		else if (ID == bgObjectGroup::Door)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::DOOR, lay, ID, pID);

			pObjManager->ObjList[Object::DOOR].back()->pos = p;
			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::DOOR].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::DOOR].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::DOOR].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::DOOR].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}
		else if (ID == bgObjectGroup::Start)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::START, lay, ID, pID);

			pObjManager->ObjList[Object::START].back()->pos = p;
			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::START].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::START].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::START].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::START].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}
		//TreasureBox
		else if (ID == bgObjectGroup::TreasureBox)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::TREASUREBOX, lay, ID, pID);
			pObjManager->ObjList[Object::TREASUREBOX].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::TREASUREBOX].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::TREASUREBOX].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::TREASUREBOX].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::TREASUREBOX].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}

		//ゴール
		else if (ID == bgObjectGroup::Goal)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::GOAL, lay, ID, pID);
			pObjManager->ObjList[Object::GOAL].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::GOAL].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::GOAL].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::GOAL].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::GOAL].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}
		//ショップマン
		else if (ID == bgObjectGroup::ShopMaster)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::SHOPMAN, lay, ID, pID);
			pObjManager->ObjList[Object::SHOPMAN].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::SHOPMAN].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::SHOPMAN].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::SHOPMAN].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::SHOPMAN].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}

		//エレベーター関連残り
		else if (ID == bgObjectGroup::ELEVATOR_G)
		{
			pObjManager->Add(std::make_shared<Obstacles>(), Object::OBSTACLE, lay, ID, pID);
			pObjManager->ObjList[Object::OBSTACLE].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::OBSTACLE].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::OBSTACLE].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::OBSTACLE].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);

				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::OBSTACLE].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}
		//オブジェクト

		else
		{
			pObjManager->Add(std::make_shared<BGObject>(), Object::OBJECT, lay, ID, pID);
			pObjManager->ObjList[Object::OBJECT].back()->pos = p;

			for (auto& it = multianimData.begin(); it != multianimData.end(); it++)
			{
				if (it->group != pObjManager->ObjList[Object::OBJECT].back()->id.CLASS)
					continue;
				if (it->id != pObjManager->ObjList[Object::OBJECT].back()->id.PERSONAL)
					continue;

				pObjManager->ObjList[Object::OBJECT].back()->loadInfo(it->tex_pos, it->tex_size, it->textureNum);
				if (ID == bgObjectGroup::Trap && pID == trapType::axe)
				{
					pObjManager->ObjList[Object::OBJECT].back()->centerPos = { (float)(it->tex_size.x / 2.0f),0 };
					pObjManager->ObjList[Object::OBJECT].back()->pos.y -= it->tex_size.y;
				}

				for (auto& itA = it->animParam.begin(); itA < it->animParam.end(); itA++)
				{
					pObjManager->ObjList[Object::OBJECT].back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
				}
				break;
			}
		}

	}

	fclose(file);
}

void objBGManager::saveFile()
{
	FILE* file = nullptr;
	char fileName[30] = { 0 };
	sprintf(fileName, "Data/Object/Stage%d-%d.txt", pBG->now_stage, pBG->stageNumber);

	fopen_s(&file, fileName, "w");

	for (int i = Object::OBSTACLE; i < Object::OBJECT + 1; i++)
	{
		if (pObjManager->ObjList[i].empty() == false)
		{
			for (auto& p = pObjManager->ObjList[i].begin(); p != pObjManager->ObjList[i].end(); p++)
			{

				fprintf_s(file, "%d ", p->get()->id.CLASS);
				fprintf_s(file, "%d ", p->get()->id.PERSONAL);
				fprintf_s(file, "%f ", p->get()->pos.x);
				if (p->get()->id.CLASS == bgObjectGroup::Trap && p->get()->id.PERSONAL == trapType::axe)
				{
					fprintf_s(file, "%f ", p->get()->pos.y + p->get()->tex_size.y);
				}
				else
					fprintf_s(file, "%f ", p->get()->pos.y);
				fprintf_s(file, "%d\n", p->get()->layer);

			}
		}
	}
	//if (pObjManager->ObjList[Object::OBSTACLE].empty() == false)
	//{
	//	for (auto& p = pObjManager->ObjList[Object::OBSTACLE].begin(); p != pObjManager->ObjList[Object::OBSTACLE].end(); p++)
	//	{

	//		fprintf_s(file, "%d ", p->get()->id.CLASS);
	//		fprintf_s(file, "%d ", p->get()->id.PERSONAL);
	//		fprintf_s(file, "%f ", p->get()->pos.x);
	//		fprintf_s(file, "%f ", p->get()->pos.y);
	//		fprintf_s(file, "%d\n", p->get()->layer);

	//	}
	//}

	fclose(file);
}





void objBGManager::MultianimImGui(string _tag)
{
	static int multi_now_anim = 0;
	static int prev_now_anim = 0;
	static Vector2 prevTexPos = { 0,0 };
	static int prevId = 0;
	static int prevGroup = 0;

	if (!textureEditFlg)return;

	if (CopyList.empty())

	{
		CopyList.emplace_back(std::make_shared<BGObject>());
		{
			CopyList.back()->id = { multianimData.begin()->group,multianimData.begin()->id };
			CopyList.back()->layer = 200;
			CopyList.back()->Init();
			CopyList.back()->tex_size = multianimData.begin()->tex_size;
			CopyList.back()->tex_pos = multianimData.begin()->tex_pos;
			CopyList.back()->pos = { cam.GetCamPos().x + TEMP_SCREEN_CX,cam.GetCamPos().y + TEMP_SCREEN_CY };
			CopyList.back()->isErase = false;
			CopyList.back()->isGrip = false;
			CopyList.back()->textureNum = multianimData.back().textureNum;
			for (auto& it = multianimData.begin()->animParam.begin(); it != multianimData.begin()->animParam.end(); it++)
			{
				CopyList.back()->addAnimation(it->isLoop, it->max_aFrame, it->animSpeed);
				CopyList.back()->stop_anim = false;
			}

			pDrawList->add(CopyList.back(), 0);
		}

	}


	CopyList.back()->pos = { cam.GetCamPos().x + TEMP_SCREEN_CX,cam.GetCamPos().y + TEMP_SCREEN_CY };

	if (ImGui::Button("add"))
	{
		prevTexPos = multianimData.back().tex_pos;
		prevGroup = multianimData.back().group;
		prevId = multianimData.back().id;
		textureNumber = multianimData.back().textureNum;
		multianimData.emplace_back();
		multianimData.back().MAX_ANIMATION = 0;
		multianimData.back().tex_size = { 32,32 };
		multianimData.back().tex_pos = prevTexPos;
		multianimData.back().textureNum = textureNumber;
		multianimData.back().group = prevGroup;
		multianimData.back().id = prevId;

	}

	if (ImGui::Button("remove"))
	{
		multianimData.pop_back();
	}



	string multi = "multiAnimation##" + _tag + " : %d";
	ImGui::Text(multi.c_str(), multianimData.size());

	multi = "multi_now_Animation##" + _tag;
	ImGui::InputInt(multi.c_str(), &multi_now_anim);
	if (multi_now_anim >= multianimData.size())
	{
		multi_now_anim = multianimData.size() - 1;
	}

	for (int i = 0; i < multianimData.size(); i++)
	{
		if (i != multi_now_anim)continue;
		std::string nodename;
		nodename = std::to_string(i);
		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode(nodename.c_str()))
		{

			std::vector<Animation>::iterator it = multianimData.begin() + i;
			string name;

			textureNumber = it->textureNum;

			if (ImGui::Combo("textureName", &textureNumber, ("object1\0object2\0object3\0\0")))
			{
				it->textureNum = textureNumber;
			}


			name = "GROUP##" + _tag;
			ImGui::InputInt(name.c_str(), &it->group);

			name = "ID##" + _tag;
			ImGui::InputInt(name.c_str(), &it->id);

			name = "TEX_POSITION##" + _tag;
			ImGui::Text(name.c_str());

			name = "tex_posX##" + _tag;
			ImGui::DragFloat(name.c_str(), &(it->tex_pos.x));
			name = "tex_posY##" + _tag;
			ImGui::DragFloat(name.c_str(), &(it->tex_pos.y));

			name = "MAX_ANIMATION##" + _tag + " : %d";
			ImGui::Text(name.c_str(), it->animParam.size());

			name = "add##" + _tag;
			if (ImGui::Button(name.c_str()))
			{
				it->animParam.emplace_back();
				it->MAX_ANIMATION++;
			}

			name = "remove##" + _tag;
			if (ImGui::Button(name.c_str()))
			{
				if (it->animParam.empty() == false)
				{
					it->animParam.pop_back();
				}
				it->MAX_ANIMATION--;
			}


			name = "ANIMATION##" + _tag;
			ImGui::Text(name.c_str());

			name = "now_ANIM##" + _tag;
			ImGui::InputInt(name.c_str(), &(it->now_anim));
			if (it->now_anim >= it->animParam.size())
			{
				it->now_anim = it->animParam.size() - 1;
			}

			int i_prev = 0;
			for (auto& itr : it->animParam)
			{
				if (i_prev != it->now_anim)
				{
					i_prev++;
					continue;
				}
				else
				{
					i_prev++;
				}
				nodename = std::to_string(i_prev - 1);
				ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
				if (ImGui::TreeNode(nodename.c_str()))
				{
					name = "MAX_FRAME##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "MAX_FRAME_NUM##" + _tag + (char)i_prev;
					if (ImGui::InputInt(name.c_str(), &(itr.max_aFrame)))//aCnt = 0;

						if (itr.max_aFrame <= 0)itr.max_aFrame = 1;

					name = "now_FRAME##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "AFRAME_NUM##" + _tag + (char)i_prev;
					ImGui::InputInt(name.c_str(), &(CopyList.back()->aFrame));
					name = "aFrame=0##" + _tag + (char)i_prev;
					if (ImGui::Button(name.c_str()))
					{
						CopyList.back()->aCnt = 0;
						CopyList.back()->aFrame = 0;
					}

					name = "ANIM_SPEED##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "ANIMATION_SPEED##" + _tag + (char)i_prev;
					if (ImGui::InputInt(name.c_str(), &(itr.animSpeed)))//aCnt = 0;

						if (itr.animSpeed <= 0)itr.animSpeed = 1;
					name = "LOOP##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "isLoop##" + _tag + (char)i_prev;
					ImGui::Checkbox(name.c_str(), &(itr.isLoop));
					name = "playAnim##" + _tag + (char)i_prev;
					if (ImGui::Button(name.c_str()))
					{
						CopyList.back()->stop_anim = false;
						CopyList.back()->aFrame = 0;
						CopyList.back()->aCnt = 0;
					}

					name = "StopAnim##" + _tag + (char)i_prev;
					if (ImGui::Button(name.c_str()))
					{
						CopyList.back()->stop_anim = true;
					}

					ImGui::TreePop();
					break;
				}
			}
			i_prev = 0;
			name = "TEX_SIZE##" + _tag + (char)i_prev;
			ImGui::Text(name.c_str());
			//if (it.isSquare)
			//{
			//	ImGui::InputInt("tex Square", &(it.tex_size.x));
			//	ImGui::InputInt("tex Square", &(it.tex_size.y));
			//	//it.tex_size.y = it.tex_size.x;
			//}
			//else
			{
				name = "texX##" + _tag + (char)i_prev;
				ImGui::InputInt(name.c_str(), &(it->tex_size.x));
				name = "texY##" + _tag + (char)i_prev;
				ImGui::InputInt(name.c_str(), &(it->tex_size.y));
			}
			//if (ImGui::Button("isSquare"))isSquare ^= true;
			//if (ImGui::Button("Cutting Line"))debugflg ^= true;

			name = "Save##" + _tag + (char)i_prev;
			if (ImGui::Button(name.c_str()))
			{
				OPENFILENAME ofn;
				char szFile[MAX_PATH] = "";
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(OPENFILENAME);
				ofn.lpstrFilter = TEXT("TXT(*.TXT以外許さん*)\0*.txt\0")
					TEXT("全てのファイル(*.*)\0*.*\0\0");
				ofn.lpstrFile = szFile;
				ofn.nMaxFile = MAX_PATH;
				ofn.lpstrDefExt = "txt";
				ofn.Flags = OFN_FILEMUSTEXIST;

				if (GetSaveFileName(&ofn) != 0)
				{
					string txt = szFile;
					MultiOutputFile(txt);
				}
				else
				{
					MessageBox(NULL, TEXT("Kitty on your lap"),
						TEXT("メッセージボックス"), MB_OK);
				}
			}


		}
	}

	if (multi_now_anim != prev_now_anim)
	{
		int i = 0;
		CopyList.back()->animParam.erase(CopyList.back()->animParam.begin(), CopyList.back()->animParam.end());
		for (auto& it = multianimData.begin(); it < multianimData.end(); it++)
		{

			if (i != multi_now_anim)
			{
				i++;
				continue;
			}

			CopyList.back()->Init();
			CopyList.back()->textureNum = it->textureNum;
			CopyList.back()->MAX_ANIMATION = it->MAX_ANIMATION;
			for (auto& itA = it->animParam.begin(); itA != it->animParam.end(); itA++)
			{
				CopyList.back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);

			}
			CopyList.back()->id = { it->group,it->id };
			CopyList.back()->tex_size = it->tex_size;
			CopyList.back()->tex_pos = it->tex_pos;
			CopyList.back()->pos = { cam.GetCamPos().x + TEMP_SCREEN_CX,cam.GetCamPos().y + TEMP_SCREEN_CY };
			break;
		}

	}
	else
	{
		int i = 0;
		for (auto& it = multianimData.begin(); it < multianimData.end(); it++)
		{

			if (i != multi_now_anim)
			{
				i++;
				continue;
			}
			CopyList.back()->textureNum = it->textureNum;

			CopyList.back()->id = { it->group,it->id };
			CopyList.back()->tex_size = it->tex_size;
			CopyList.back()->tex_pos = it->tex_pos;
			CopyList.back()->pos = { cam.GetCamPos().x + TEMP_SCREEN_CX,cam.GetCamPos().y + TEMP_SCREEN_CY };
			if (CopyList.back()->MAX_ANIMATION != it->MAX_ANIMATION)
			{
				CopyList.back()->MAX_ANIMATION = it->MAX_ANIMATION;
				CopyList.back()->animParam.erase(CopyList.back()->animParam.begin(), CopyList.back()->animParam.end());
				for (auto& itA = it->animParam.begin(); itA != it->animParam.end(); itA++)
				{
					CopyList.back()->addAnimation(itA->isLoop, itA->max_aFrame, itA->animSpeed);
					CopyList.back()->stop_anim = false;

				}
			}
			else
			{
				for (auto& itA = it->animParam.begin(); itA != it->animParam.end(); itA++)
				{
					CopyList.back()->animParam.back().animSpeed = itA->animSpeed;
					CopyList.back()->animParam.back().max_aFrame = itA->max_aFrame;
					CopyList.back()->animParam.back().isLoop = itA->isLoop;

				}

			}
			break;
		}
	}

	CopyList.back()->playAnimation();
	prev_now_anim = multi_now_anim;
}

void objBGManager::MultiOutputFile(string _txt)
{
	ofstream outf(_txt, ios::out);

	for (int i = 0; i < multianimData.size(); i++)
	{
		std::vector<Animation>::iterator it = multianimData.begin() + i;
		outf << it->group << endl;
		outf << it->id << endl;
		outf << it->textureNum << endl;
		outf << it->tex_pos.x << endl;
		outf << it->tex_pos.y << endl;

		outf << it->tex_size.x << endl;
		outf << it->tex_size.y << endl;
		outf << it->animParam.size();
		for (auto& itr : it->animParam)
		{
			outf << endl;
			outf << itr.isLoop << endl;
			outf << itr.max_aFrame << endl;
			outf << itr.animSpeed;
		}
		if (i < multianimData.size() - 1)
			outf << endl;
	}
}


void objBGManager::MultiInputFile(string _txt)
{
	string fileName = "./Data/Images/Texts/" + _txt + ".txt";

	ifstream inf(fileName, ios::in);
	int i = 0;
	while (!inf.eof())
	{
		multianimData.emplace_back();
		std::vector<Animation>::iterator it = multianimData.begin() + i;
		inf >> it->group;
		inf >> it->id;
		inf >> it->textureNum;

		inf >> it->tex_pos.x;
		inf >> it->tex_pos.y;
		inf >> it->tex_size.x;
		inf >> it->tex_size.y;
		inf >> it->MAX_ANIMATION;

		for (int j = 0; j < it->MAX_ANIMATION; j++)
		{
			if (it->animParam.size() < it->MAX_ANIMATION)
			{
				it->animParam.emplace_back();
			}
			std::vector<AnimParameter>::iterator itr = it->animParam.begin() + j;
			inf >> itr->isLoop;
			inf >> itr->max_aFrame;
			inf >> itr->animSpeed;
		}

		i++;
	}
}
