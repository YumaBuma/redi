#include "EffectEmitter.h"
#include "EffectRoot.h"

void EffectRoot::MakeEmitter(const bool& _is_flip_x)
{
	for (auto& it_data : emit_dset->dataset)
	{
		std::shared_ptr<EffectEmitter> emitter;
		emitter = make_shared<EffectEmitter>(it_data.emit_data, this, _is_flip_x);
		for (auto& it_efc : it_data.efc_data)
		{
			emitter->EmitEffect(it_efc.GetAddPosParams(root_pos));
		}
		emitters.emplace_back(std::move(emitter));
	}
}

void EffectRoot::MakeAnimEmitter()
{
	for (auto& it_dset : anim_emit_dset->dataset)
	{
		std::shared_ptr<AnimationEffectEmitter> anim_emitter;
		anim_emitter = make_shared<AnimationEffectEmitter>(it_dset, anim, this);

		emitters.emplace_back(std::move(anim_emitter));
	}
}

bool EffectRoot::Update()
{
	if (emitters.empty())
		return is_static_effect;// is_static_effectがtureの場合生存し続ける

	if (p_attract_pos)
		root_pos = *p_attract_pos;

	if (is_anim)
		if (anim->now_anim != -1)
			anim->playAnimation();

	auto remove_it = std::remove_if
	(
		emitters.begin(), emitters.end(),
		[&](std::shared_ptr<EffectEmitter>& efc_emt)
		{
			//efc_emt->SetPos(root_pos);
			return !efc_emt->Update();
		}
	);
	emitters.erase(remove_it, emitters.end());

	return true;
}

void EffectRoot::Draw()
{
	for (const auto& it_emitter : emitters)
	{
		it_emitter->Draw();
	}
}
