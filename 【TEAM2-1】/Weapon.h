#pragma once

struct WEAPON
{
	int id;
	int rarity;
	WEAPON()
	{
		id = 0;
		rarity = 0;
	}
	WEAPON(int _id, int _rarity) :id(_id), rarity(_rarity) {};
	WEAPON& operator= (const WEAPON& _Weapon) { id = _Weapon.id; rarity = _Weapon.rarity; return *this; }

};
