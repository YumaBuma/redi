#pragma once
#include "Effect.h" 
#include "EffectEmitter.h"
#include "EffectRoot.h"
#include "EffectDataManager.h"

#include "Singleton.h"

#include <stack>
#include <memory>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <iostream>

#include <cereal\cereal.hpp>
#include <cereal\archives\json.hpp>
#include <cereal\types\vector.hpp>
#include <cereal\types\string.hpp>
#include <cereal\types\unordered_map.hpp>
#include <cereal\types\memory.hpp>

class EffectManager :public Singleton<EffectManager>
{
public:
	EffectManager();
	~EffectManager();
	void Init(const size_t& _size)
	{
		{ std::vector<std::shared_ptr<EffectEmitter>>().swap(efc_emitters); }
		efc_emitters.shrink_to_fit();
		efc_emitters.reserve(_size);

		{ std::vector<std::shared_ptr<EffectRoot>>().swap(efc_roots); }
		efc_roots.reserve(_size);
	}
	void Init()
	{
		{ std::vector<std::shared_ptr<EffectEmitter>>().swap(efc_emitters); }
		efc_emitters.shrink_to_fit();
		efc_emitters.reserve(EFC_ROOT_MAX);

		{ std::vector<std::shared_ptr<EffectRoot>>().swap(efc_roots); }
		efc_roots.shrink_to_fit();
		efc_roots.reserve(EFC_ROOT_MAX);
	}
	void ClearAllEffects()
	{
		{ std::vector<std::shared_ptr<EffectEmitter>>().swap(efc_emitters); }
		efc_emitters.shrink_to_fit();
		efc_emitters.reserve(EFC_ROOT_MAX);

		{ std::vector<std::shared_ptr<EffectRoot>>().swap(efc_roots); }
		efc_roots.shrink_to_fit();
		efc_roots.reserve(EFC_ROOT_MAX);
	}
	void AddEmitter(std::shared_ptr<EffectEmitter> _emitter);
	void AddEfcRoot(std::shared_ptr<EffectRoot> _root);
	std::shared_ptr<EffectEmitter>	MakeAndGetEmitter(const EmitterParams& _emit_params																					, const DirectX::SimpleMath::Vector2& _pos = { 0,0 });
	std::shared_ptr<EffectRoot>		MakeAndGetEfcRoot(const std::shared_ptr<EMIT_DATASET> _dset											, bool _is_static_effect = false, const DirectX::SimpleMath::Vector2& _pos = { 0,0 }, const bool& _is_flip_x = false, const DirectX::SimpleMath::Vector2* _p_pos = nullptr);
	std::shared_ptr<EffectRoot>		MakeAndGetEfcRoot(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, const Animation& _anim			, bool _is_static_effect = false, const DirectX::SimpleMath::Vector2& _pos = { 0,0 });
	std::shared_ptr<EffectRoot>		MakeAndGetEfcRoot(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, std::shared_ptr<Animation>  _anim	, bool _is_static_effect = false, const DirectX::SimpleMath::Vector2& _pos = { 0,0 });

	std::shared_ptr<EffectRoot>		MakeAndGetEfcRootToStack(const std::shared_ptr<EMIT_DATASET> _dset											, bool _is_static_effect = false, const DirectX::SimpleMath::Vector2& _pos = { 0,0 }, const bool& _is_flip_x = false, const DirectX::SimpleMath::Vector2* _p_pos = nullptr);
	std::shared_ptr<EffectRoot>		MakeAndGetEfcRootToStack(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, const Animation& _anim				, bool _is_static_effect = false, const DirectX::SimpleMath::Vector2& _pos = { 0,0 });
	std::shared_ptr<EffectRoot>		MakeAndGetEfcRootToStack(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, std::shared_ptr<Animation>  _anim	, bool _is_static_effect = false, const DirectX::SimpleMath::Vector2& _pos = { 0,0 });

	std::shared_ptr<EffectRoot> GetEfcRoot(const std::shared_ptr<ANIM_EMIT_DATASET> _dset, const Animation& _anim, bool _is_static_effect, const DirectX::SimpleMath::Vector2& _pos = { 0,0 });

	void SetEmitter(const EmitterParams& _emit_params);
	void SetEmitter
	(
		EFC_TYPE type,
		DirectX::SimpleMath::Vector2 pos,
		DirectX::SimpleMath::Vector2 vel,
		DirectX::SimpleMath::Vector2 efc_vel,
		float duration,
		float set_speed_per_frame = 0.f,
		float deceleration_rate_mul = 0.f,
		float gravity = 0.f,
		bool _is_ui_efc = false,
		float _size = 1.f,
		const float* _attract_x_ptr = nullptr, const float* _attract_y_ptr = nullptr
	);
	void SetEmitters(const EmitterParams& _emit_params);
	void SetEmitDataSet(const EMIT_DATASET& _emit_data);
	void SetAnimEmitDataSet(const ANIM_EMIT_DATASET& _anim_emit_dataset);
	void SetAnimEmitDataSet(const ANIM_EMIT_DATASET& _anim_emit_dataset, const Animation& _anim);

	std::shared_ptr<EffectRoot> FindStaticEffect();

	void Update();
	void Draw(bool _is_ui_efc = false);

	static void SetEmitterMax(const size_t& _size) { EFC_ROOT_MAX = _size; }
private:
	static size_t EFC_ROOT_MAX;
	std::vector<std::shared_ptr<EffectEmitter>> efc_emitters;
	std::vector<std::shared_ptr<EffectRoot>> efc_roots;
	std::deque<std::shared_ptr<EffectRoot>> efc_roots_stack;
};

#define EFC_MGR (EffectManager::GetIns())
