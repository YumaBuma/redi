//#include "DxLib.h"
#include "common.h"


Imgui_Windows_Flg Iwin_flg;
ImVec4 clear_col;

bool show_debug = false;
bool bossProd = false;
int White;
int font_c;
Vector2 camera_pos;
Vector2 tmp_camera_pos;
float camera_move_rate;
float camera_max_speed;
bool gameexit = true;
int framerate = 60;
int PAD1;

float screen_ext_rate = 3.f;

double ToRadian(double deg)
{
	//return deg*atan(1.0) *4.0 / 180.0;
	return deg * PI / 180.0;
}


char* fileProcessing::readTextFile(const char* filename)
{
	FILE *fp = NULL;
	char buf[256];
	char* str = NULL;
	int fsize;

	/* ファイルのオープン */
	if (fopen_s(&fp, filename, "r"))
		return nullptr;
	if (fp == NULL) {
		printf("Can't read '%s'.\n", filename);
		exit(1);
	};

	/* ファイルサイズの取得 */
	fseek(fp, 0L, SEEK_END);
	fsize = ftell(fp);

	/* メモリの動的確保 */
	str = (char*)malloc(fsize * sizeof(char));
	if (str == NULL) {
		printf("Can't allocate memory. 'str' is NULL.\n");
		fclose(fp);
		exit(1);
	}

	/* シーク位置を先頭に戻す */
	fseek(fp, 0L, SEEK_SET);

	/* テキストデータの読み込み */
	str[0] = '\0';
	while (fgets(buf, sizeof(buf), fp) != NULL) {
		strncat(str, buf, strlen(buf) + 1);
	}

	/* ファイルのクローズ */
	fclose(fp);

	return str;
}
