#include "scene.h"
#include "SceneTitle.h"
#include "SceneGame.h"
#include "SceneOver.h"
#include "SceneClear.h"

#include "src\imgui.h"

BaseScene* sceneManager::m_pScene = nullptr;
int BaseScene::stageNumber = 0;
bool sceneManager::start;
bool sceneManager::isSwitchScene;
sceneManager::SCENE sceneManager::next_scene;

void sceneManager::changeScene(SCENE scene)
{
	if (m_pScene)
	{
		delete m_pScene;
		m_pScene = nullptr;
		start = true;
	}

	isSwitchScene = false;

	switch (scene)
	{
	case SCENE::TITLE:
		m_pScene = new Title();
		break;
	case SCENE::GAME:
		m_pScene = new Game();
		break;
	case SCENE::OVER:
		m_pScene = new Over();
		break;
	case SCENE::CLEAR:
		m_pScene = new Clear();
		break;
	default:
		break;
	}
}

void sceneManager::Init()
{
	m_pScene->Init();
}
void sceneManager::unInit()
{
	m_pScene->unInit();

	//if (m_pScene)delete m_pScene;
}
void sceneManager::Update()
{
	m_pScene->Update();
}
void sceneManager::Draw()
{
	m_pScene->Draw();

	if (!show_debug)return;

	ImGui_ImplDxLib_NewFrame();

	m_pScene->ImGui();

	ImGui::Render();
}

void sceneManager::execute()
{
	if (start)
	{
		Init();
		start = false;
	}

	Update();

	Draw();

	if (sceneManager::isSwitchScene)changeScene(next_scene);
}

