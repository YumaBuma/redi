#include "EffectDataManager.h"
#include "Function.h"

#include <fstream>
#include <iostream>
#include <filesystem>

//#include <cereal\cereal.hpp>
//#include <cereal\archives\binary.hpp>
//#include <cereal\archives\json.hpp>
//#include <cereal\archives\xml.hpp>
//#include <cereal\types\deque.hpp>
//#include <cereal\types\vector.hpp>
//#include <cereal\types\memory.hpp>
//#include <cereal\types\unordered_map.hpp>
//#include <cereal\types\string.hpp>

const int LOAD_EMIT_DATASET = 1;
#include "EffectEmitter.h"

Data::Data()
{

}


void Data::Init()
{
	std::string current_dir = GetCurrentDir();
	//std::string path = GetOpenFileNameWithExplorer(false);
	//std::vector<std::string> path2 = GetOpenMultipleFileNameWithExplorer(true);
	////emitter
	//{
	//	std::string emitparam_dir = current_dir + "\\Data\\Images\\Texs\\EmitterData";

	//	std::vector<std::string> emitparam_files;
	//	emitparam_files = this->GetFileNameInDir(emitparam_dir, "csv");

	//	for (auto&& it_file : emitparam_files)
	//	{
	//		LoadEmitDataSet(emitparam_dir, it_file);
	//	}
	//}

	//efc
	{
		std::string					efc_dir = ".\\Data\\Images\\Effects\\";
		std::vector<std::string>	efc_files = GetAllFileNamesInDir(efc_dir, "png");

		for (auto&& it_file : efc_files)
		{
			LoadGraphEfc(efc_dir, it_file);
		}
	}
	if (LOAD_EMIT_DATASET)
	{
		// emit_datasets
		{
			std::string					emit_dir	= ".\\Data\\Effect\\EmitDatasets\\";
			std::vector<std::string>	emit_files	= GetAllFileNamesInDir(emit_dir, "bin");

			for (auto& it_emit_fname : emit_files)
			{
				LoadEmitDataSet(emit_dir, it_emit_fname);
			}
		}
		// anim_emit_datasets
		{
			std::string					anim_emit_dir	= ".\\Data\\Effect\\AnimEmitDatasets\\";
			std::vector<std::string>	anim_emit_files = GetAllFileNamesInDir(anim_emit_dir, "bin");

			for (auto& it_filename : anim_emit_files)
			{
				LoadAnimEmitDataSet(anim_emit_dir, it_filename);
			}
		}

	}
	////fonts
	//{
	//	std::string font_dir = current_dir + "\\Data\\Fonts";

	//	std::vector<std::string> font_files;
	//	font_files = this->GetFileNameInDir(font_dir, "dft");

	//	for (auto&& it_file : font_files)
	//	{
	//		LoadFont(font_dir, it_file);
	//	}
	//}
}

void Data::ReleaseImageDataAll()
{
	//images_efc
	{
		for (auto&& it : images_efc)
		{
			DeleteGraph(it.second);
		}

		{ std::map< std::string, int >().swap(images_efc); }
	}

	//images_ui
	{
		for (auto&& it : images_ui)
		{
			DeleteGraph(it.second);
		}

		{ std::map< std::string, int >().swap(images_ui); }
	}

	//images_other
	{
		for (auto&& it : images_other)
		{
			DeleteGraph(it.second);
		}

		{ std::map< std::string, int >().swap(images_other); }
	}
}

void Data::ReleaseFontDataAll()
{
	for (auto&& it : fonts)
	{
		DeleteFontToHandle(it.second);
	}

	{ std::map< std::string, int >().swap(fonts); }
}

void Data::ReleaseEmitDatasetsAll()
{
	{ std::map< std::string, std::shared_ptr<EMIT_DATASET>>().swap(emit_datasets); }
	{ std::map< std::string, std::shared_ptr<ANIM_EMIT_DATASET>>().swap(anim_emit_datasets); }
}

void Data::ReleaseDataAll() { ReleaseImageDataAll(); ReleaseFontDataAll(); ReleaseEmitDatasetsAll(); }

void Data::LoadGraphEfc(const std::string& _dir, const std::string& _filename)
{
	if (images_efc.empty())
	{
		auto& it_map = images_efc.find(_filename);
		if (it_map != images_efc.end())
		{
			MessageBox(nullptr, ("\"" + _dir + _filename + "\"" + " is already loaded").c_str(), "DxLib::LoadGraph() abort", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
			return;
		}
	}

	int graph_hnd_tmp = LoadGraph((_dir + _filename).c_str());
	if (graph_hnd_tmp == -1)
	{
		MessageBox(NULL, ("\"" + _dir + _filename + "\"" + " is not found").c_str(), "DxLib::LoadGraph() failed", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
		return;
	}

	//https://qiita.com/_EnumHack/items/f462042ec99a31881a81#%E8%A6%81%E7%B4%A0%E6%8C%BF%E5%85%A5
	images_efc.emplace(_filename, graph_hnd_tmp);
}

void Data::LoadGraphUI(const std::string& _dir, const std::string& _filename)
{
	if (images_ui.empty())
	{
		auto& it_map = images_ui.find(_filename);
		if (it_map != images_ui.end())
		{
			MessageBox(nullptr, ("\"" + _dir + _filename + "\"" + " is already loaded").c_str(), "DxLib::LoadGraph() abort", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
			return;
		}
	}

	int graph_hnd_tmp = LoadGraph((_dir + _filename).c_str());
	if (graph_hnd_tmp == -1)
	{
		MessageBox(NULL, ("\"" + _dir + _filename + "\"" + " is not found").c_str(), "DxLib::LoadGraph() failed", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
		return;
	}

	images_ui.emplace(_filename, graph_hnd_tmp);
}

void Data::LoadGraphOther(const std::string& _dir, const std::string& _filename)
{
	if (images_other.empty())
	{
		auto& it_map = images_ui.find(_filename);
		if (it_map != images_ui.end())
		{
			MessageBox(nullptr, ("\"" + _dir + _filename + "\"" + " is already loaded").c_str(), "DxLib::LoadGraph() abort", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
			return;
		}
	}

	int graph_hnd_tmp = LoadGraph((_dir + _filename).c_str());
	if (graph_hnd_tmp == -1)
	{
		MessageBox(NULL, ("\"" + _dir + _filename + "\"" + " is not found").c_str(), "DxLib::LoadGraph() failed", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
		return;
	}

	images_other.emplace(_filename, graph_hnd_tmp);
}

void Data::LoadFont(const std::string& _dir, const std::string& _filename)
{
	if (fonts.empty())
	{
		auto& it_map = fonts.find(_filename);
		if (it_map != fonts.end())
		{
			MessageBox(nullptr, ("\"" + _dir + _filename + "\"" + " is already loaded").c_str(), "DxLib::LoadFontDataToHandle() abort", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
			return;
		}
	}

	int font_hnd_tmp = LoadFontDataToHandle((_dir + _filename).c_str());
	if (font_hnd_tmp == -1)
	{
		MessageBox(NULL, ("\"" + _dir + _filename + "\"" + " is not found").c_str(), "DxLib::LoadFontDataToHandle() failed", MB_CANCELTRYCONTINUE | MB_ICONSTOP);
		return;
	}

	fonts.emplace(_filename, font_hnd_tmp);
}


void Data::LoadEmitDataSet(const std::string& _dir, const std::string& _filename)
{
	std::shared_ptr<EMIT_DATASET> emit_dset_tmp;
	std::ifstream ifs;
	ifs.open(_dir + _filename, std::ios::binary);

	cereal::BinaryInputArchive i_archive(ifs);
	i_archive(cereal::make_nvp(_dir + _filename, emit_dset_tmp));

	for (auto&& it_dset : emit_dset_tmp->dataset)
	{
		it_dset.emit_data.efc.draw.graph_hadle = EFCDATA->GetEfcRefSafe(it_dset.emit_data.graph_filename);
		for (auto&& it_efc : it_dset.efc_data)
		{
			it_efc.draw.graph_hadle = it_dset.emit_data.efc.draw.graph_hadle;
		}
	}

	emit_datasets.emplace(_filename, std::move(emit_dset_tmp));
}

void Data::LoadAnimEmitDataSet(const std::string & _dir, const std::string & _filename)
{
	std::shared_ptr<ANIM_EMIT_DATASET> anim_emit_dset_tmp;
	std::ifstream ifs;
	ifs.open(_dir + _filename, std::ios::binary);

	cereal::BinaryInputArchive i_archive(ifs);
	i_archive(cereal::make_nvp(_dir + _filename, anim_emit_dset_tmp));

	for				(auto&& it_dset	: anim_emit_dset_tmp->dataset	)
	{
		it_dset.anim_emit_data.efc.draw.graph_hadle = EFCDATA->GetEfcRefSafe(it_dset.anim_emit_data.graph_filename);
		for			(auto&& it_efcdata: it_dset.anim_efc_data		)
		{
			for		(auto&& it_efcs	: it_efcdata					)
			{
				for (auto&& it_efc	: it_efcs.second				)
				{
					it_efc.draw.graph_hadle = it_dset.anim_emit_data.efc.draw.graph_hadle;
				}
			}
		}
	}

	anim_emit_datasets.emplace(_filename, std::move(anim_emit_dset_tmp));
}
