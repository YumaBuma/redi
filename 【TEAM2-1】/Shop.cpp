#include "Shop.h"

#include "GameMenu.h"
#include "Input.h"
#include "scene.h"
#include "ObjectManager.h"
#include "Texture.h"
#include "UI.h"
#include "Sound.h"

Shop shop;

int weapon_money[] =
{
	1080,2540,5060,
	920,2460, 4980,
	950,2490,4930,
	1160,2670,5100,
	870,2410,4870,
};


void Shop::Init()
{
	isOpen = false;
	isShopping = false;
	heal_hp = 0;
	act = 0;
	mode = 0;
	cursor = 0;
	switch (BaseScene::stageNumber)
	{
	case 1:
		for (int i = 0; i < 5; i++)
		{
			int tmp = GetRand(5) - 1;
			equipments[i].id = tmp>0 ? tmp : 0;
			equipments[i].rarity = 1;
			howmuch[i] = weapon_money[equipments[i].id * 3 + (equipments[i].rarity - 1)];
		}
		break;
	case 2:
		for (int i = 0; i < 5; i++)
		{
			int tmp = GetRand(5) - 1;
			equipments[i].id = tmp>0 ? tmp : 0;
			tmp = GetRand(2);
			//equipments[i].rarity = tmp>0 ? tmp : 1;
			equipments[i].rarity = 2;
			howmuch[i] = weapon_money[equipments[i].id * 3 + (equipments[i].rarity - 1)];
		}
		break;
	case 3:
		for (int i = 0; i < 5; i++)
		{
			int tmp = GetRand(5) - 1;
			equipments[i].id = tmp>0 ? tmp : 1;
			tmp = GetRand(2);
			//equipments[i].rarity = tmp>0 ? tmp : 1;
			equipments[i].rarity = 2;
			howmuch[i] = weapon_money[equipments[i].id * 3 + (equipments[i].rarity - 1)];
		}
		break;
	case 4:
		for (int i = 0; i < 5; i++)
		{
			int tmp = GetRand(5) - 1;
			equipments[i].id = tmp>0 ? tmp : 0;
			tmp = GetRand(3);
			//equipments[i].rarity = tmp>0 ? tmp : 1;
			equipments[i].rarity = 3;
			howmuch[i] = weapon_money[equipments[i].id * 3 + (equipments[i].rarity - 1)];
		}
		break;
	default:
		break;
	}
	heal[0] = pObjManager->ObjList[Object::PLAYER].back()->getHP(1)*0.3f;
	heal[1] = pObjManager->ObjList[Object::PLAYER].back()->getHP(1)*0.5f;
	heal[2] = pObjManager->ObjList[Object::PLAYER].back()->getHP(1)*0.8f;

	howmuch[5] = 500;
	howmuch[6] = 1500;
	howmuch[7] = 3000;
}

void Shop::Update()
{
	if (heal_hp > 0)
	{
		pObjManager->ObjList[Object::PLAYER].back()->addHP(heal_hp);
		
		heal_hp = 0;
	}

	if (isShopping)
	{
		switch (mode)
		{
		case 0:buy_Update();
			break;
		case 1:sell_Update();
			break;
		default:
			break;
		}
	}
	else
	{
		if (key[KEY_INPUT_RSHIFT] == 1)
		{
			pSound->playSE(pSound->SE_SYSTEM_PAUSE, false);
			isOpen = true;
		}

		if (isOpen)
		{
			if (key[KEY_INPUT_DOWN] == 1)
			{
				pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
				act++;
			}
			if (key[KEY_INPUT_DOWN] > 50)
			{
				if (key[KEY_INPUT_DOWN] % 5 == 0)
				{
					pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
					act++;
				}
			}
			if (key[KEY_INPUT_UP] == 1)
			{
				pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
				act--;
			}

			if (key[KEY_INPUT_UP] > 50)
			{
				if (key[KEY_INPUT_UP] % 5 == 0)
				{
					pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
					act--;
				}
			}
			if (act > 2)
			{
				act = 2;
			}
			if (act < 0)
			{
				act = 0;
			}
			if (key[KEY_INPUT_D] == 1)
			{
				if (!isShopping)
				{
					pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
					isOpen = false;
				}
				key[KEY_INPUT_D]++;
			}
			if (key[KEY_INPUT_SPACE] == 1)
			{

				switch (act)
				{
				case 0:
					pSound->playSE(pSound->SE_SYSTEM_DECISION, false);
					isShopping = true;
					mode = act;
					break;
				case 1:
					pSound->playSE(pSound->SE_SYSTEM_DECISION, false);
					isShopping = true;
					mode = act;
					break;
				case 2:
					pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
					isShopping = false;
					isOpen = false;
					break;
				default:
					break;
				}
				key[KEY_INPUT_SPACE]++;
				act = 0;
			}
		}
	}
}

void Shop::Draw() const
{
	
	if (!isOpen)return;

	DrawRectGraph(320, 150, 0, 384, 96, 24, pTexture->getGraphs(pTexture->UI_FONT), TRUE);

	DrawRectGraph(320, 174, 64 * act, 432, 64, 50, pTexture->getGraphs(pTexture->UI_FONT), TRUE);

	if (!isShopping)return;

	switch (mode)
	{
	case 0:buy_Draw();
		break;
	case 1:sell_Draw();
		break;
	default:
		break;
	}
}

void Shop::buy_Update()
{
	if (key[KEY_INPUT_LEFT] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor--;
	}
	if (key[KEY_INPUT_LEFT] > 50)
	{
		if (key[KEY_INPUT_LEFT] % 5 == 0)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor--;
		}
	}
	if (key[KEY_INPUT_RIGHT] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor++;
	}
	if (key[KEY_INPUT_RIGHT] > 50)
	{
		if (key[KEY_INPUT_RIGHT] % 5 == 0)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor++;
		}
	}
	if (key[KEY_INPUT_DOWN] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor += 4;
	}
	if (key[KEY_INPUT_DOWN] > 50)
	{
		if (key[KEY_INPUT_DOWN] % 5 == 0)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor += 4;
		}
	}
	if (key[KEY_INPUT_UP] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor -= 4;
	}

	if (key[KEY_INPUT_UP] > 50)
	{
		if (key[KEY_INPUT_UP] % 5 == 0)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor -= 4;
		}
	}

	if (cursor >= 8)
	{
		cursor -= 8;
	}
	else if (cursor < 0)
	{
		cursor += 8;
	}

	if (key[KEY_INPUT_SPACE] == 1)
	{		
		if (howmuch[cursor] != -1)
		{
			if (cursor < 5)
			{
				if (howmuch[cursor] < pObjManager->ObjList[Object::PLAYER].back()->money)
				{
					for (auto& it : Gmenu.bag)
					{
						if (it.id != -1)continue;
						pSound->playSE(pSound->SE_SYSTEM_COIN, false);
						it = equipments[cursor];
						pObjManager->ObjList[Object::PLAYER].back()->subMoney(howmuch[cursor]);
						howmuch[cursor] = -1;
						equipments[cursor] = { -1,-1 };
						break;
					}
				}
				else
					pSound->playSE(pSound->SE_SYSTEM_BAD, false);

			}
			else
			{
				if (howmuch[cursor] < pObjManager->ObjList[Object::PLAYER].back()->money)
				{
					{
						pSound->playSE(pSound->SE_SYSTEM_COIN, false);
						pObjManager->ObjList[Object::PLAYER].back()->subMoney(howmuch[cursor]);
						howmuch[cursor] = -1;
						heal_hp = heal[cursor - 5];
						heal[cursor - 5] = 0;
					}
				}
				else
					pSound->playSE(pSound->SE_SYSTEM_BAD, false);

			}
		}
	}

	if (key[KEY_INPUT_D] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
		isShopping = false;
	}
}

void Shop::buy_Draw() const
{
	DrawGraph(0, 0, pTexture->getGraphs(pTexture->SHOP_BUY), TRUE);

	

	for (int i = 0; i < 8; i++)
	{
		if (i < 5)
		{
			if (equipments[i].id != -1)
			{
				DrawRectGraph(148 + 104 * (i % 4), 94 + 108 * (i / 4), 32 * equipments[i].id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
				DrawRectGraph(148 + 104 * (i % 4), 94 + 108 * (i / 4), 32 * (equipments[i].rarity - 1), 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
			
				if (howmuch[i] > 0)
				{
					DrawRectGraph(
						130 + 54 + 104 * (i % 4),
						135 + 108 * (i / 4),
						8 * (howmuch[i] % 10),
						96,
						8,
						12,
						pTexture->getGraphs(pTexture->UI), TRUE
						);
					if (howmuch[i] >= 10)
					{
						DrawRectGraph(
							130 + 44 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 10 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
					if (howmuch[i] >= 100)
					{
						DrawRectGraph(
							130 + 34 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 100 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
					if (howmuch[i] >= 1000)
					{
						DrawRectGraph(
							130 + 24 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 1000 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
					if (howmuch[i] >= 10000)
					{
						DrawRectGraph(
							130 + 14 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 10000 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
				}
			}
			else
			{
				DrawRectGraph(148 + 104 * (i % 4), 94 + 108 * (i / 4), 32 * 21, 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
			}
		}
		else
		{
			if (heal[i - 5] != 0)
			{
				DrawRectGraph(148 + 104 * (i % 4), 94 + 108 * (i / 4), 32 * (17 + (i - 5)), 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
			
				if (howmuch[i] > 0)
				{
					DrawRectGraph(
						130 + 54 + 104 * (i % 4),
						135 + 108 * (i / 4),
						8 * (howmuch[i] % 10),
						96,
						8,
						12,
						pTexture->getGraphs(pTexture->UI), TRUE
						);
					if (howmuch[i] >= 10)
					{
						DrawRectGraph(
							130 + 44 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 10 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
					if (howmuch[i] >= 100)
					{
						DrawRectGraph(
							130 + 34 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 100 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
					if (howmuch[i] >= 1000)
					{
						DrawRectGraph(
							130 + 24 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 1000 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
					if (howmuch[i] >= 10000)
					{
						DrawRectGraph(
							130 + 14 + 104 * (i % 4),
							135 + 108 * (i / 4),
							8 * (howmuch[i] / 10000 % 10),
							96,
							8,
							12,
							pTexture->getGraphs(pTexture->UI), TRUE
							);
					}
				}
			}
			else
			{
				DrawRectGraph(148 + 104 * (i % 4), 94 + 108 * (i / 4), 32 * 21, 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
			}
		}
	}
	DrawRectGraph(131 + 104 * (cursor%4), 73 + 108*(cursor/4), 0, 352, 66, 78, pTexture->getGraphs(pTexture->UI), TRUE);
	ui.pl_ui_Draw();
}

void Shop::sell_Update()
{
	ui.ui_money_Update();

	switch (key[KEY_INPUT_LEFT])
	{
	case 1:case 30: case 40:case 45:
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor--;
		break;
	default:
		if (key[KEY_INPUT_LEFT] > 50)
		{
			if (key[KEY_INPUT_LEFT] % 5 == 0)
			{
				pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
				cursor--;
			}
		}
		break;
	}

	switch (key[KEY_INPUT_RIGHT])
	{
	case 1:case 30: case 40:case 45:
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor++;
		break;
	default:
		if (key[KEY_INPUT_RIGHT] > 50)
		{
			if (key[KEY_INPUT_RIGHT] % 5 == 0)
			{
				pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
				cursor++;
			}
		}
		break;
	}

	//int tmp = pObjManager->ObjList[Object::PLAYER].back()->getHP(3);

	{
		if (key[KEY_INPUT_DOWN] == 1)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor += 5;
		}
		if (key[KEY_INPUT_UP] == 1)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor -= 5;
		}

		if (cursor < 0)cursor += Gmenu.bag.size();
		if (cursor >= Gmenu.bag.size())cursor -= Gmenu.bag.size();

	}

	if (key[KEY_INPUT_SPACE] == 1)
	{
		if (Gmenu.bag[cursor].id != -1)
		{
			pSound->playSE(pSound->SE_SYSTEM_COIN, false);
			pObjManager->ObjList[Object::PLAYER].back()->addMoney(weapon_money[Gmenu.bag[cursor].id * 3 + (Gmenu.bag[cursor].rarity - 1)] * 0.12f);
			ui.tmp_MAX_MONEY = pObjManager->ObjList[Object::PLAYER].back()->money;
			Gmenu.bag[cursor] = { -1,-1 };
		}
		else
			pSound->playSE(pSound->SE_SYSTEM_BAD, false);

	}

	if (key[KEY_INPUT_D] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
		isShopping = false;
	}

}

void Shop::sell_Draw() const
{
	//DrawBox(100, 100, 1820, 780, GetColor(255, 128, 30), TRUE);
	DrawGraph(0, 0, pTexture->getGraphs(pTexture->SHOP_SELL), TRUE);

	//DrawBox(100, 830, 1820, 980, GetColor(30, 255, 255), TRUE);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

	//SetFontSize(64);
	//DrawFormatString(100, 830, GetColor(255, 255, 255), "%d", pObjManager->ObjList[Object::PLAYER].back()->getHP(2));

	for (int i = 0; i < Gmenu.bag.size(); i++)
	{
		if (Gmenu.bag[i].id <= -1)continue;
		DrawRectGraph(43 + (32 + 4)* (i % 5), 67 + (32 + 4)* (i / 5), 32 * Gmenu.bag[i].id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		if (Gmenu.bag[i].rarity <= 0)continue;
		DrawRectGraph(43 + (32 + 4)* (i % 5), 67 + (32 + 4)* (i / 5), 32 * (Gmenu.bag[i].rarity - 1), 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		//DrawBox((100 + 6) + (100 + 20) *(i % 5), (100 + 6) + (100 + 6)*(i / 5), (100 + (100 + 6)) + (100 + 20) * (i % 8), (100 + (100 + 6)) + (100 + 6)* (i / 8), GetColor(255, 128, 30), TRUE);
	}

	DrawRectGraph(414, 219, 32 * Gmenu.bag[cursor].id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
	{
		int tmp = weapon_money[Gmenu.bag[cursor].id * 3 + (Gmenu.bag[cursor].rarity - 1)] * 0.12f;
		if (tmp > 0)
		{
			DrawRectGraph(
				396 + 54,
				260,
				8 * ((tmp) % 10),
				96,
				8,
				12,
				pTexture->getGraphs(pTexture->UI), TRUE
				);
			if (tmp >= 10)
			{
				DrawRectGraph(
					396 + 44,
					260,
					8 * ((tmp) / 10 % 10),
					96,
					8,
					12,
					pTexture->getGraphs(pTexture->UI), TRUE
					);
			}
			if (tmp >= 100)
			{
				DrawRectGraph(
					396 + 34,
					260,
					8 * ((tmp) / 100 % 10),
					96,
					8,
					12,
					pTexture->getGraphs(pTexture->UI), TRUE
					);
			}
			if (tmp >= 1000)
			{
				DrawRectGraph(
					396 + 24,
					260,
					8 * (tmp / 1000 % 10),
					96,
					8,
					12,
					pTexture->getGraphs(pTexture->UI), TRUE
					);
			}
			if (tmp >= 10000)
			{
				DrawRectGraph(
					396 + 14,
					260,
					8 * (tmp / 10000 % 10),
					96,
					8,
					12,
					pTexture->getGraphs(pTexture->UI), TRUE
					);
			}
		}

	}
	Gmenu.weapon_Detail_Draw(cursor);



	SetDrawBlendMode(DX_BLENDMODE_ADD, 255);

	//if (!sellmode)
	{
		//DrawBox((100 + 6) + (100 + 20) *(cursor % 8), (100 + 6) + (100 + 6)*(cursor / 8), (100 + (100 + 6)) + (100 + 20) * (cursor % 8), (100 + (100 + 6)) + (100 + 6)* (cursor / 8), GetColor(128, 128, 128), TRUE);
		DrawRectGraph(43 + (32 + 4)* (cursor % 5), 67 + (32 + 4)* (cursor / 5), 32 * 16, 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);

	}
	//else
	//{
	//	SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
	//	if (tmp_cursor != -1)
	//	{
	//		SetDrawBlendMode(DX_BLENDMODE_ADD, 200);
	//		DrawRectGraph(43 + (32 + 4)* (tmp_cursor % 5), 67 + (32 + 4)* (tmp_cursor / 5), 32 * pick_weapon.id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
	//	}

	//	DrawRectGraph(317 + (32 + 20)* (cursor % 5), 231 + (32 + 20)* (cursor / 5),
	//		32 * 16, 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
	//	//DrawBox((200 + 6) + (100 + 20) *(cursor-41), (830 + 20), (100 + (200 + 6)) + (100 + 20) *(cursor-41), (100 + (830 + 20)), GetColor(128, 128, 128), TRUE);
	//}

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

	ui.pl_ui_Draw();
}