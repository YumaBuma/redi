#pragma once
#include "Sprite.h"

class Texture
{
public:

	enum Tex_id
	{
		PLAYER = 0,
		PL_LARGESWORD,
		PL_SPEAR,
		PL_LONGBOW,
		PL_HUMMER,
		PL_TOMAHAWK,
		ENEMY,
		BOSS,
		E_SLIME,
		ITEM,
		OBJECT,
		OBJECT2,
		OBJECT3,
		BGCHIP,
		BGCHIP_MINI,
		ARROW,
		ROCKS,
		E_SIGN,
		UI,
		UI_FONT,
		UI_BUTTON,
		UI_PORCH,
		UI_MAP,
		UI_SYSTEM,
		UI_BLOOD,
		UI_PL_HP_LOW,
		UI_TELEPORTER,
		SHOP_BUY,
		SHOP_SELL,
		TITLE_3,
		TITLE_BG,
		TITLE_BUTTON,
		BLODD_EFC,
		ENDROLL,
		LOADING,
		BG0,
		BG1,
		BG2,
		BG3,
		BG4,
		BG5,
		BG6,
		BG7,
		BG8,
		U_BG0,
		U_BG1,
		U_BG2,
		U_BG3,
		U_BG4,
		U_BG5,
		U_BG6,
		TITLE_1,
		TITLE_2,
		TITLE_4,
		END_1,
		END_2,
		END_3,
		END_4,
		BAG_INFO1,
		BAG_INFO2,
		END,
	};


private:
	Texture() {}
	Texture(Texture& copy) {}
	~Texture() {}

	Sprite Graphs[END];
public:

	static Texture* getInstance()
	{
		static Texture instance;
		return &instance;
	}

	void loadGrapahs();
	Sprite* getGraphsSpr(int graphNum) { return &Graphs[graphNum]; }
	int getGraphs(int graphNum) { return reinterpret_cast<int>(&Graphs[graphNum]); }

	void ReloadAllGraphHandles();
	void DeleteAllGraphHandles();
};

#define pTexture Texture::getInstance()
