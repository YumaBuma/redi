#include "GameMenu.h"

#include "Input.h"
#include "Texture.h"
#include "ObjectManager.h"
#include "Sound.h"
#include "scene.h"
#include "UI.h"
Gamemenu Gmenu;

//const int BAG_BOX_SIZE =100;
//const int BAG_MERGIN_X = 20;
//const int BAG_MERGIN_Y = 6;

void Gamemenu::Init()
{
	tutorial_num = 0;
	gain_count = 0;
	isOpen = false;
	setBagFlg = false;
	equipmode = false;
	Menubar_C = 0;
	cursor = 0;
	pl_money = 0;
	bag.resize(30);

	for (auto& it : equipments)
	{
		it.id = -1;
		it.rarity = 0;
	}
	for (auto& it : bag)
	{
		it.id = -1;
		it.rarity = 0;
	}
	equipments[0] = { 0,1 };
	equipXY[0] = equipments[0];
	equipXY[1] = { -1,-1 };
	sendFlg = true;
	//{
	//	bag[0].id = 0;
	//	bag[1].id = 1;
	//	bag[2].id = 2;
	//	bag[3].id = 3;
	//	bag[4].id = 4;
	//	bag[0].rarity = 1;
	//	bag[1].rarity = 3;
	//	bag[2].rarity = 2;
	//	bag[3].rarity = 1;
	//	bag[4].rarity = 2;
	//	bag[5].id = 0;
	//	bag[6].id = 0;
	//	bag[7].id = 0;
	//	bag[8].id = 3;
	//	bag[9].id = 4;
	//	bag[5].rarity = 1;
	//	bag[6].rarity = 3;
	//	bag[7].rarity = 2;
	//	bag[8].rarity = 1;
	//	bag[9].rarity = 2;
	//}

}

void Gamemenu::Update()
{
	pl_money = pObjManager->ObjList[Object::PLAYER].back()->money;
	if (isOpen)
	{
		if (ui.bag_tutorial && gain_count == 1)
		{
			if (key[KEY_INPUT_SPACE] == 1)
			{
				tutorial_num++;
				pSound->playSE(pSound->SE_SYSTEM_DECISION, false);

			}

			if (key[KEY_INPUT_D] == 1)
			{
				tutorial_num--;
				if (tutorial_num < 0)
				{
					tutorial_num = 0;
					pSound->playSE(pSound->SE_SYSTEM_BAD, false);
				}
				else
					pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
			}
			if (tutorial_num >= 2)
			{
				ui.bag_tutorial = false;
			}
			return;
		}

	}
	if (key[KEY_INPUT_TAB] == 1)
	{
		if (isOpen)
			pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
		else
			pSound->playSE(pSound->SE_SYSTEM_PAUSE, false);
		isOpen ^= 1;
		setBagFlg = true;
		if (!isOpen)
		{
			pmini_map->Miniaturization();
			pmini_map->SetEasingProgress(1.f);
		}
		pmini_map->is_gmenu = isOpen;
		Menubar_C = 0;
	}
	if (key[KEY_INPUT_M] == 1)
	{
		if (ui.bag_tutorial && gain_count == 1)return;

		if (isOpen)
			pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
		else
			pSound->playSE(pSound->SE_SYSTEM_PAUSE, false);

		isOpen ^= 1;
		pmini_map->is_gmenu_map = true;

		if (!isOpen)
		{
			pmini_map->is_gmenu_map = false;
			pmini_map->is_gmenu = false;
			pmini_map->Miniaturization();
		}
		Menubar_C = 1;
	}

	if (!isOpen)return;
	if (key[KEY_INPUT_Q] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		Menubar_C--;
	}
	if (key[KEY_INPUT_E] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		Menubar_C++;
	}

	if (Menubar_C < 0)
	{
		Menubar_C = 2;
	}
	else if (Menubar_C > 2)
	{
		Menubar_C = 0;
	}

	switch (Menubar_C)
	{
	case 0:
		pmini_map->is_gmenu = false;
		pmini_map->is_gmenu_map = false;
		pmini_map->Miniaturization();
		pmini_map->SetEasingProgress(1.f);
		EquipMenu_Update();
		break;
	case 1:
		pmini_map->is_gmenu = isOpen;
		pmini_map->Maximization();
		if (pmini_map->is_gmenu_map == false)
			pmini_map->SetEasingProgress(1.f);
		break;
	case 2:
		pmini_map->is_gmenu = false;
		pmini_map->is_gmenu_map = false;
		pmini_map->Miniaturization();
		pmini_map->SetEasingProgress(1.f);
		if (key[KEY_INPUT_SPACE] == 1)
		{
			pSound->playSE(pSound->SE_SYSTEM_DECISION, false);
			pObjManager->ObjList[Object::PLAYER].back()->isErase = true;
			sceneManager::isSwitchScene = true;
			sceneManager::next_scene = sceneManager::TITLE;
		}
		break;
	case 3:
		break;
	default:
		break;
	}
	if (key[KEY_INPUT_D] == 1 && ui.bag_tutorial == false)
	{
		if (equipmode)
		{
			pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
			equipmode = false;
			bag[tmp_cursor] = pick_weapon;
			pick_weapon = { -1,-1 };
			cursor = tmp_cursor;
			tmp_cursor = -1;
		}
		else
		{
			pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
			isOpen = false;
			pmini_map->is_gmenu = false;
			pmini_map->is_gmenu_map = false;
			pmini_map->Miniaturization();
			key[KEY_INPUT_D]++;
		}

	}


}

void Gamemenu::Draw()
{
	if (!isOpen)return;


	//DrawBox(0, 0, 1920, 1080, GetColor(0, 0, 0), TRUE);

	//SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 100);

	switch (Menubar_C)
	{
	case 0:EquipMenu_Draw();
		break;
	case 1:
		DrawGraph(0, 0, pTexture->getGraphs(pTexture->UI_MAP), TRUE);
		break;
	case 2:
		DrawGraph(0, 0, pTexture->getGraphs(pTexture->UI_SYSTEM), TRUE);
		break;
	case 3:
		break;
	default:
		break;
	}


	for (int i = 0; i < 4; i++)
	{
		//DrawBox(70 + 445 * i, 30, 515 + 445 * i, 130, GetColor(255, 30, 255), TRUE);
	}

	//DrawBox(70 + 445 * Menubar_C, 30, 515 + 445 * Menubar_C, 130, GetColor(255, 30, 255), TRUE);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 100);
}

void Gamemenu::EquipMenu_Update()
{
	if (!isOpen)return;

	switch (key[KEY_INPUT_LEFT])
	{
	case 1:case 30: case 40:case 45:
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor--;
		break;
	default:
		if (key[KEY_INPUT_LEFT] > 50)
		{
			if (key[KEY_INPUT_LEFT] % 5 == 0)
			{
				pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
				cursor--;
			}
		}
		break;
	}

	switch (key[KEY_INPUT_RIGHT])
	{
	case 1:case 30: case 40:case 45:
		pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		cursor++;
		break;
	default:
		if (key[KEY_INPUT_RIGHT] > 50)
		{
			if (key[KEY_INPUT_RIGHT] % 5 == 0)
			{
				pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
				cursor++;
			}
		}
		break;
	}

	//int tmp = pObjManager->ObjList[Object::PLAYER].back()->getHP(3);

	if (equipmode)
	{
		if (cursor < 0)cursor += 5;
		if (cursor >= 5)cursor -= 5;

	}
	else
	{
		if (key[KEY_INPUT_DOWN] == 1)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor += 5;
		}
		if (key[KEY_INPUT_UP] == 1)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
			cursor -= 5;
		}

		if (cursor < 0)cursor += bag.size();
		if (cursor >= bag.size())cursor -= bag.size();

	}

	if (key[KEY_INPUT_SPACE] == 1)
	{
		if (!equipmode)//カバン側
		{
			if (bag[cursor].id != -1)//バッグにアイテムがあれば
			{
				pSound->playSE(pSound->SE_SYSTEM_DECISION, false);
				tmp_cursor = cursor;
				cursor = 0;
				pick_weapon = bag[tmp_cursor];
				bag[tmp_cursor].id = -1;
				equipmode = true;
			}
			else
				pSound->playSE(pSound->SE_SYSTEM_BAD, false);

		}
		else//装備側
		{
			Equipment();
			sortXY();
		}
		sendFlg = true;
	}

	if (key[KEY_INPUT_A] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_PAUSE, false);
		WEAPON tmp_W = equipXY[0];
		equipXY[0] = equipXY[1];
		equipXY[1] = tmp_W;
		sendFlg = true;
		sortXY();
	}

}

void Gamemenu::Equipment()
{
	if (!equipmode)return;

	if (equipXY[0].id < 0 && equipXY[1].id < 0)//XY装備していなければ
	{
		{
			equipments[cursor] = pick_weapon;
		}

		equipXY[0] = equipments[cursor];
		equipmode = false;
		return;
	}
	else//どちらも装備していた
	{
		if (equipXY[0].id == pick_weapon.id)
		{
			if (equipments[cursor].id == -1)
			{
				equipments[cursor] = pick_weapon;
				equipmode = false;
				return;
			}
			else
			{
				bag[tmp_cursor] = equipments[cursor];
				equipments[cursor] = pick_weapon;
				equipmode = false;
			}
		}
		else if (equipXY[1].id == pick_weapon.id)
		{
			if (equipments[cursor].id == -1)
			{
				equipments[cursor] = pick_weapon;
				equipmode = false;
				return;
			}
			else
			{
				bag[tmp_cursor] = equipments[cursor];
				equipments[cursor] = pick_weapon;
				equipmode = false;
			}
		}
		else
		{
			if (equipXY[0].id == equipments[cursor].id)
			{
				for (auto& it : equipments)
				{
					if (it.id != equipXY[0].id)continue;
					for (auto& iter : bag)
					{
						if (iter.id != -1)continue;
						iter = it;
						break;
					}
					it.id = -1;
					it.rarity = -1;
				}
				equipXY[0] = pick_weapon;
			}
			else if (equipXY[1].id == equipments[cursor].id)
			{
				for (auto& it : equipments)
				{
					if (it.id == equipXY[1].id)
					{
						for (auto& iter : bag)
						{
							if (iter.id != -1)continue;
							iter = it;
							break;
						}
						it.id = -1;
						it.rarity = -1;
					}
				}
				equipXY[1] = pick_weapon;
			}
			else
			{
				if (equipXY[0].id == -1)
				{
					equipXY[0] = pick_weapon;
				}
				else if (equipXY[1].id == -1)
				{
					equipXY[1] = pick_weapon;
				}
				else
				{
					return;
				}
			}
			//bag[tmp_cursor] = equipments[cursor];
			equipments[cursor] = pick_weapon;

			equipmode = false;
		}
	}
}

WEAPON Gamemenu::sendEquip(bool _isX)
{
	int tmp_cnt = 0;
	equipXY[0].rarity = 0;
	equipXY[1].rarity = 0;
	for (int i = 0; i < 5; i++)
	{
		if (equipments[i].id == equipXY[0].id)
		{
			tmp_cnt++;
			equipXY[0].rarity += equipments[i].rarity;
			if (tmp_cnt == 5)
			{
				equipXY[1].id = -1;
			}
			continue;
		}
		if (equipments[i].id == equipXY[1].id)
		{
			tmp_cnt--;
			equipXY[1].rarity += equipments[i].rarity;
			if (tmp_cnt == -5)
			{
				equipXY[0].id = -1;
			}
			continue;
		}
	}
	if (_isX)
	{
		//equipXY[0].rarity = 0;
		//for (int i = 0; i < 5; i++)
		//{
		//	if (equipments[i].id != equipXY[0].id)continue;
		//	equipXY[0].rarity += equipments[i].rarity;
		//}
		return equipXY[0];
	}
	else
	{
		//equipXY[1].rarity = 0;
		//for (int i = 0; i < 5; i++)
		//{
		//	if (equipments[i].id != equipXY[1].id)continue;
		//	equipXY[1].rarity += equipments[i].rarity;
		//}
		return equipXY[1];
	}
}

void Gamemenu::sortXY()
{
	for (int i = 0; i < 7; i++)
	{
		for (int i = 0; i < 5; i++)
		{
			if (equipments[i].id == equipXY[0].id)
			{
				if (i != 0)
				{
					if (equipments[i - 1].id != equipXY[0].id)
					{
						WEAPON tmp = equipments[i];
						equipments[i] = equipments[i - 1];
						equipments[i - 1] = tmp;
					}
				}
			}
		}
		for (int i = 0; i < 5; i++)
		{
			if (equipments[i].id == equipXY[1].id)
			{
				if (i != 5)
				{
					if (equipments[i + 1].id != equipXY[0].id)
					{
						WEAPON tmp = equipments[i];
						equipments[i] = equipments[i + 1];
						equipments[i + 1] = tmp;
					}
				}
			}
		}
	}
}

void Gamemenu::EquipMenu_Draw()
{
	//DrawBox(100, 100, 1820, 780, GetColor(255, 128, 30), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 100);
	DrawGraph(0, 0, pTexture->getGraphs(pTexture->UI_PORCH), TRUE);

	//DrawBox(100, 830, 1820, 980, GetColor(30, 255, 255), TRUE);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 100);

	//SetFontSize(64);
	//DrawFormatString(100, 830, GetColor(255, 255, 255), "%d", pObjManager->ObjList[Object::PLAYER].back()->getHP(2));

	for (int i = 0; i < bag.size(); i++)
	{
		if (bag[i].id <= -1)continue;
		DrawRectGraph(43 + (32 + 4)* (i % 5), 67 + (32 + 4)* (i / 5), 32 * bag[i].id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		if (bag[i].rarity <= 0)continue;
		DrawRectGraph(43 + (32 + 4)* (i % 5), 67 + (32 + 4)* (i / 5), 32 * (bag[i].rarity - 1), 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		//DrawBox((100 + 6) + (100 + 20) *(i % 5), (100 + 6) + (100 + 6)*(i / 5), (100 + (100 + 6)) + (100 + 20) * (i % 8), (100 + (100 + 6)) + (100 + 6)* (i / 8), GetColor(255, 128, 30), TRUE);
	}
	for (int i = 0; i < 5; i++)
	{
		if (equipments[i].id == -1)continue;
		DrawRectGraph(317 + (32 + 20)* (i % 5), 231 + (32 + 20)* (i / 5),
			32 * equipments[i].id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		if (equipments[i].rarity == -1)continue;
		DrawRectGraph(317 + (32 + 20)* (i % 5), 231 + (32 + 20)* (i / 5),
			32 * (equipments[i].rarity - 1), 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
	}
	DrawRectGraph(278, 235, 0, 320, 24, 24, pTexture->getGraphs(pTexture->UI), TRUE);
	DrawRectGraph(572, 235, 48, 320, 24, 24, pTexture->getGraphs(pTexture->UI), TRUE);

	for (int i = 0; i < 5; i++)
	{
		if (equipments[i].id == -1)continue;
		if (equipments[i].id == equipXY[0].id)
		{
			DrawRectGraph(300 + 52 * i, 228, 0, 256, 66, 38, pTexture->getGraphs(pTexture->UI), TRUE);
			continue;
		}
		if (equipments[i].id == equipXY[1].id)
		{
			DrawRectGraph(300 + 52 * i, 228, 66, 256, 66, 38, pTexture->getGraphs(pTexture->UI), TRUE);
			continue;
		}
	}

	weapon_Detail_Draw(cursor);


	if (!equipmode)
	{
		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
		//DrawBox((100 + 6) + (100 + 20) *(cursor % 8), (100 + 6) + (100 + 6)*(cursor / 8), (100 + (100 + 6)) + (100 + 20) * (cursor % 8), (100 + (100 + 6)) + (100 + 6)* (cursor / 8), GetColor(128, 128, 128), TRUE);
		DrawRectGraph(43 + (32 + 4)* (cursor % 5), 67 + (32 + 4)* (cursor / 5), 32 * 16, 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);

	}
	else
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
		if (tmp_cursor != -1)
		{
			SetDrawBlendMode(DX_BLENDMODE_ADD, 200);
			DrawRectGraph(43 + (32 + 4)* (tmp_cursor % 5), 67 + (32 + 4)* (tmp_cursor / 5), 32 * pick_weapon.id, 64, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		}

		DrawRectGraph(317 + (32 + 20)* (cursor % 5), 231 + (32 + 20)* (cursor / 5),
			32 * 16, 128, 32, 32, pTexture->getGraphs(pTexture->UI), TRUE);
		//DrawBox((200 + 6) + (100 + 20) *(cursor-41), (830 + 20), (100 + (200 + 6)) + (100 + 20) *(cursor-41), (100 + (830 + 20)), GetColor(128, 128, 128), TRUE);
	}
	if (ui.bag_tutorial && gain_count == 1)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

		switch (tutorial_num)
		{
		case 0:
			DrawGraph(0, 0, pTexture->getGraphs(pTexture->BAG_INFO1), TRUE);
			break;
		case 1:
			DrawGraph(0, 0, pTexture->getGraphs(pTexture->BAG_INFO2), TRUE);
			break;
		default:
			break;
		}
	}


}


void Gamemenu::weapon_Detail_Draw(int _cursor)
{
	if (bag[_cursor].id == -1)return;
	DrawRectGraph(366, 100, 144 * bag[_cursor].id, 448, 144, 94, pTexture->getGraphs(pTexture->UI), TRUE);
}
