#include "MiniMap.h"
#include "Camera.h"
#include "Mo2_BG.h"
#include "Texture.h"
#include "ObjectManager.h"
#include "bgObject.h"
#include "Collision.h"
#include "Input.h"
#include "Function.h"
#include "Sound.h"
#include "./src/imgui.h"

const static int IS_LOAD_MINIMAP = 1;

// 現状ミニマップのチップサイズ == 4x4;

inline MiniMap::MiniMap() : chip_size(8), prev_blendmode(0), prev_blendval(0), is_selecting_telepo_dest(false)
{
	mini_map_extrate.val = 1.f;
	mini_map_extrate.target_val = 1.f;
	blend_alpha.val = 255.f;
	blend_alpha.target_val = 255.f;
	rect_max.SetIsUseEasing(true);
	rect_min.SetIsUseEasing(true);
	center_margin.SetMoveSpeed(0.16f);
	telepo_destination_pos.SetMoveSpeed(0.22f);
}

void MiniMap::Init()
{
	if (is_loaded)return;

	is_loaded = true;
	is_selecting_telepo_dest = false;
	is_seleced_telepo_dest = false;
	is_gmenu = false;

	if (IS_LOAD_MINIMAP)
		LoadDataFromFile(".\\Data\\Configurations\\MiniMap\\minimap.bin", eArchiveTypes::BINARY);

	CreateScreen();

	CreateMaskScreen();
	FillMaskScreenWhite();
}

void MiniMap::CreateMaskScreen()
{
	if (mask_ghnd != -1)
		DeleteGraph(mask_ghnd);
	mask_ghnd_size = pBG->getMapSize();
	mask_ghnd_size.x *= chip_size;
	mask_ghnd_size.y *= chip_size;
	mask_ghnd = MakeScreen(mask_ghnd_size.x, mask_ghnd_size.y, 0);
}

void MiniMap::FillMaskScreenWhite()
{
	int current_ghnd, current_blendmode, current_blendval;
	current_ghnd = GetDrawScreen();
	GetDrawBlendMode(&current_blendmode, &current_blendval);
	{
		SetDrawScreen(mask_ghnd);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
		DrawBox(0, 0, mask_ghnd_size.x, mask_ghnd_size.y, GetColor(255, 255, 255), TRUE);
	}
	SetDrawScreen(current_ghnd);
	SetDrawBlendMode(current_blendmode, current_blendval);
}

// ミニマップのサイズを切り替えます
void MiniMap::SwitchSize()
{
	is_maximize = !is_maximize;
	is_maximize ? Maximization() : Miniaturization();
}

void MiniMap::Miniaturization()
{
	if (!is_maximize)return;
	rect_min.SetEasingFunction(miniaturization_ease_algo);
	rect_max.SetEasingFunction(miniaturization_ease_algo);
	rect_min.StartEasingAtVector2(miniaturization_rect_min_pos, 1.f / miniaturization_ease_time);
	rect_max.StartEasingAtVector2(miniaturization_rect_max_pos, 1.f / miniaturization_ease_time);

	mini_map_scroll_pos.SetPosAtVector2(Vector2(0, 0));
	mini_map_scroll_pos = Vector2(0, 0);
	bg_color.SetTargetAlpha(scasf(miniaturization_blend_alpha));
	is_maximize = false;
}

void MiniMap::Maximization()
{
	if (is_maximize)return;
	rect_min.SetEasingFunction(maximization_ease_algo);
	rect_max.SetEasingFunction(maximization_ease_algo);
	rect_min.StartEasingAtVector2(maximization_rect_min_pos, 1.f / maximization_ease_time);
	rect_max.StartEasingAtVector2(maximization_rect_max_pos, 1.f / maximization_ease_time);

	bg_color.SetTargetAlpha(scasf(maximization_blend_alpha));
	is_maximize = true;
}

void MiniMap::SelectTelepoDestination(const int& _selecting_telepoter_number)
{
	Maximization();
	is_selecting_telepo_dest = true;
	selecting_telepoter_number = _selecting_telepoter_number;
	selecting_telepoter_number_tmp = _selecting_telepoter_number;
	telepo_destination_pos = (pObjManager->ObjList[Object::TEREPORTER].begin() + _selecting_telepoter_number)->get()->pos;
	telepo_destination_pos.SetPosAtVector2((pObjManager->ObjList[Object::TEREPORTER].begin() + _selecting_telepoter_number)->get()->pos);
}


// ミニマップのアニメーション進行度を設定します

void MiniMap::SetEasingProgress(const float& _ease_time)
{
	rect_max.SetEasingProgress(_ease_time);
	rect_min.SetEasingProgress(_ease_time);
	rect_max.Update();
	rect_min.Update();
	center_margin.SetPosAtVector2(center_margin.GetTargetPosAtVector2());
	bg_color.col.w = bg_color.target_col.w;
}

void MiniMap::Update()
{
	is_seleced_telepo_dest = false;

	if (is_gmenu)
	{
		//SetEasingProgress(1.f);
		float scroll_speed = 48.f;
		//if (input == 0 /*キーボード*/)
		if (1)
		{
			if (key[KEY_INPUT_LEFT])
				mini_map_scroll_pos.target_x -= scroll_speed;
			if (key[KEY_INPUT_RIGHT])
				mini_map_scroll_pos.target_x += scroll_speed;
			if (key[KEY_INPUT_UP])
				mini_map_scroll_pos.target_y -= scroll_speed;
			if (key[KEY_INPUT_DOWN])
				mini_map_scroll_pos.target_y += scroll_speed;
		}
		else
		{
			Vector2 tmp = { (float)GetXInput().ThumbLX, (float)GetXInput().ThumbLY };
			FitTo(tmp.x, 32767.f, scroll_speed, -scroll_speed, -32768.f);
			FitTo(tmp.y, 32767.f, scroll_speed, -scroll_speed, -32768.f);
			mini_map_scroll_pos.target_x += tmp.x;
			mini_map_scroll_pos.target_y += tmp.y;
		}
	}

	pos.Update();
	mini_map_scroll_pos.Update();
	mini_map_extrate.Update();
	blend_alpha.Update();
	rect_min.Update();
	rect_max.Update();
	center_margin.Update();
	bg_color.Update();
	telepo_destination_pos.Update();
	UpdateTelepoDestination();
}

void MiniMap::UpdateTelepoDestination()
{
	if (is_selecting_telepo_dest == false) return;
	if (key[KEY_INPUT_D] == 1)
	{
		Miniaturization();
		is_selecting_telepo_dest = false;
		key[KEY_INPUT_D]++;
		pSound->playSE(pSound->SE_SYSTEM_CANSEL, false);
	}

	//teleporter
	Vector2 pl_pos = { cam.target_x, cam.target_y };
	Vector2 telepo_sub_pl;
	float	dist = FLT_MAX;
	float	dist_min = FLT_MAX;
	int		max_teleporter_num = pObjManager->ObjList[Object::TEREPORTER].size();
	auto&	it_min_telepo = pObjManager->ObjList[Object::TEREPORTER].begin();
	auto	GetNearestTelepo = [&](bool _is_minus_dir, bool _is_y_axis)
	{
		for (auto& it : pObjManager->ObjList[Object::TEREPORTER])
		{
			telepo_sub_pl = it->pos - pl_pos;
			if (_is_y_axis)
			{
				if (_is_minus_dir)
				{
					if (telepo_sub_pl.y > 0.f)continue;
				}
				else
				{
					if (telepo_sub_pl.y < 0.f)continue;
				}
			}
			else
			{
				if (_is_minus_dir)
				{
					if (telepo_sub_pl.x > 0.f)continue;
				}
				else
				{
					if (telepo_sub_pl.x < 0.f)continue;
				}
			}
			dist = GetDistanceAtoB(it->pos, pl_pos);
			if (dist <= dist_min)
			{
				//if (it->getATK(2).y) // テレポーター起動状態の場合
				{
					dist_min = dist;
					*it_min_telepo = it;
					telepo_destination_pos = it->pos;
				}
			}
		}
	};

	// todo: 音系
	bool dest_changed = false;
	int tmp = selecting_telepoter_number;

	if (ButtonPressing(key[KEY_INPUT_RIGHT]))
	{
		while (tmp < max_teleporter_num - 1)
		{
			tmp++;
			if ((it_min_telepo + tmp)->get()->getATK(2).y)
			{
				telepo_destination_pos = (it_min_telepo + tmp)->get()->pos;
				selecting_telepoter_number = tmp;
				dest_changed = true;
				break;
			}
		}
		if (dest_changed)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		}
		else
		{
			pSound->playSE(pSound->SE_SYSTEM_BAD, false);
		}
	}
	if (ButtonPressing(key[KEY_INPUT_LEFT]))
	{
		while (tmp > 0)
		{
			tmp--;
			if ((it_min_telepo + tmp)->get()->getATK(2).y)
			{
				telepo_destination_pos = (it_min_telepo + tmp)->get()->pos;
				selecting_telepoter_number = tmp;
				dest_changed = true;
				break;
			}
		}
		if (dest_changed)
		{
			pSound->playSE(pSound->SE_SYSTEM_SELECT, false);
		}
		else
		{
			pSound->playSE(pSound->SE_SYSTEM_BAD, false);
		}
	}

	if (key[KEY_INPUT_SPACE] == 1)
	{
		pSound->playSE(pSound->SE_SYSTEM_DECISION, false);
		key[KEY_INPUT_SPACE] += 1;// プレイヤーのジャンプ防止のため(== 1を防ぐため)加算して値を2にする
		Miniaturization();
		if (selecting_telepoter_number != selecting_telepoter_number_tmp)
			is_seleced_telepo_dest = true;

		is_selecting_telepo_dest = false;
	}
}

#ifdef SHOPGOAL

void MiniMap::DrawShop(const DirectX::SimpleMath::Vector2& _pos)
{
	shop_poss.push_back(_pos);
}

void MiniMap::DrawGoal(const DirectX::SimpleMath::Vector2& _pos)
{
	goal_pos = _pos;
}
#endif

void MiniMap::DrawComposite()
{
	if (mini_map_ghnd <= 0) return;

	INT2	chip = pBG->getMapSize();

	float	ext = mini_map_extrate.val;
	float	chipsize = (float)chip_size * ext;
	float	chipsize_main = (float)CHIP_SIZE;
	float	chipsize_reciprocal = 1.f / chipsize;// 逆数
	float	chipsize_main_reciprocal = 1.f / chipsize_main;// 逆数

	center_margin = (is_maximize && !is_selecting_telepo_dest) ? Vector2(-2224.f, -1440.f) : Vector2(-150.f, -50.f);

	float scroll_pos_x;
	float scroll_pos_y;
	if (is_selecting_telepo_dest)
	{
		scroll_pos_x = telepo_destination_pos.x + (mini_map_scroll_pos.x + center_margin.x) - 1130.f * 2.f;
		scroll_pos_y = telepo_destination_pos.y + (mini_map_scroll_pos.y + center_margin.y) - 740.f * 2.f;
	}
	else
	{
		scroll_pos_x = (cam.GetCamXF() + (mini_map_scroll_pos.x + center_margin.x));
		scroll_pos_y = (cam.GetCamYF() + (mini_map_scroll_pos.y + center_margin.y));
	}
		draw_start_chip.x = scroll_pos_x * chipsize_main_reciprocal;
		draw_start_chip.y = scroll_pos_y * chipsize_main_reciprocal;
	if (draw_start_chip.x < 0.f) draw_start_chip.x = 0.f;
	if (draw_start_chip.y < 0.f) draw_start_chip.y = 0.f;

	float	draw_area_x = (rect_max.x - rect_min.x); // 矩形のサイズ x
	float	draw_area_y = (rect_max.y - rect_min.y); // 矩形のサイズ y
		draw_iterate_num.x = draw_area_x;
		draw_iterate_num.y = draw_area_y;
		draw_iterate_num.x += (draw_start_chip.x) + 1.f;
		draw_iterate_num.y += (draw_start_chip.y) + 1.f;
	if (draw_iterate_num.x > (float)chip.x) draw_iterate_num.x = (float)chip.x;
	if (draw_iterate_num.y > (float)chip.y) draw_iterate_num.y = (float)chip.y;

	int prev_ghnd = GetDrawScreen(); // 以前の描画先を保存
	GetDrawBlendMode(&prev_blendmode, &prev_blendval); // 以前のブレンドモードを保存
	{
		// 背景
		{
			SetDrawScreen(mini_map_bg_ghnd);
			ClearDrawScreen();
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, bg_color.GetAlpha());
			DrawBox(0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, bg_color.GetCol(), TRUE);
		}

		// ミニマップ
		Vector2 pl_pos;
		Vector2 cam_pos;
		Vector2 composite_pos;
		{
			SetDrawScreen(mini_map_ghnd);
			ClearDrawScreen();
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);

			if (is_selecting_telepo_dest)
				cam_pos = { scroll_pos_x * chipsize_main_reciprocal,scroll_pos_y * chipsize_main_reciprocal };
			else
				cam_pos = { (cam.GetCamXF()) * chipsize_main_reciprocal, (cam.GetCamYF()) * chipsize_main_reciprocal };

			cam_pos *= chipsize;
			composite_pos.x = ((mini_map_scroll_pos.x + center_margin.x) * chipsize_main_reciprocal) * chipsize;
			composite_pos.y = ((mini_map_scroll_pos.y + center_margin.y) * chipsize_main_reciprocal) * chipsize;

			// チップ描画
			int** map = pBG->getMap();
			int texture = pTexture->getGraphs(pTexture->BGCHIP_MINI);
			for (int y = scasi(draw_start_chip.y); y < scasi(draw_iterate_num.y); y++)
			{
				for (int x = scasi(draw_start_chip.x); x < scasi(draw_iterate_num.x); x++)
				{
					int index = map[y][x];
					if (index == -1)continue;

					float	cx = (chipsize * scasf(x)) - composite_pos.x;
					float	cy = (chipsize * scasf(y)) - composite_pos.y;
							cx -= cam_pos.x;
							cy -= cam_pos.y;
					int row = 0, col = 0;
					if (index != 0)
					{
						row = index / CHIP_TEXDIV_X;
						col = index % CHIP_TEXDIV_X;
					}
					DrawRectExtendGraph
					(
						scasi(cx),
						scasi(cy),
						scasi(cx + chipsize),
						scasi(cy + chipsize),
						chip_size * col, chip_size * row,
						chip_size, chip_size,
						texture, TRUE
					);
				}
			}

			// teleporter
			{
				Vector2 telepo_pos = { -50.f,-50.f };
				for (auto& it : pObjManager->ObjList[Object::TEREPORTER])
				{
					if (it->getATK(2).y == false)continue;

					telepo_pos = it->pos;
					telepo_pos *= chipsize_main_reciprocal;
					telepo_pos *= chipsize;
					bool is_in_cam = Coll::AABBf(	draw_start_chip.x * chipsize, draw_start_chip.y * chipsize	, draw_iterate_num.x * chipsize	, draw_iterate_num.y * chipsize	,
													telepo_pos.x - 4.f			, telepo_pos.y - 12.f			, telepo_pos.x + 4.f			, telepo_pos.y + 12.f			);
					telepo_pos -= composite_pos + cam_pos;
					if (is_in_cam == true)
					{
						DrawRectRotaGraphFast2
						(
							XY_F2I(telepo_pos),
							0, 52,
							8, 12,
							4, 12,
							1.f, 0.f,
							texture, TRUE
						);
					}
				}
				// カーソル描画
				if (is_selecting_telepo_dest == true)
				{
					DrawRectRotaGraphFast2
					(
						XY_F2I(telepo_pos),
						0, 64,
						14, 14,
						8, 12,
						1.f, 0.f,
						texture, TRUE
					);
				}
			}

#ifdef SHOPGOAL
			// shop
			{
				Vector2 shop_pos = { -50.f,-50.f };
				for (auto&& it : shop_poss)
				{
					shop_pos = it;
					shop_pos *= chipsize_main_reciprocal;
					shop_pos *= chipsize;
					bool is_in_cam = Coll::AABBf(draw_start_chip.x * chipsize, draw_start_chip.y * chipsize, draw_iterate_num.x * chipsize, draw_iterate_num.y * chipsize,
						shop_pos.x - 4.f, shop_pos.y - 12.f, shop_pos.x + 4.f, shop_pos.y + 12.f);
					shop_pos -= composite_pos + cam_pos;
					if (is_in_cam == true)
					{
						DrawRectRotaGraphFast2
						(
							XY_F2I(shop_pos),
							16, 52,
							8, 12,
							4, 12,
							1.f, 0.f,
							texture, TRUE
						);
					}

				}
				{ std::vector<DirectX::SimpleMath::Vector2>().swap(shop_poss); }
			}
			// goal
			{
				goal_pos *= chipsize_main_reciprocal;
				goal_pos *= chipsize;
				goal_pos -= composite_pos + cam_pos;
				DrawRectRotaGraphFast2
				(
					SCASI(goal_pos.x),
					SCASI(goal_pos.y),
					9, 52,
					7, 12,
					4, 12,
					1.f, 0.f,
					texture, TRUE
				);

			}

#else
			for (auto& it : pObjManager->ObjList[Object::OBSTACLE])
			{
				// shop
				if (it->id.CLASS == bgObjectGroup::ShopMaster)
				{
					Vector2 shop_pos = { -50.f,-50.f };
					{
						shop_pos = it->pos;
						shop_pos *= chipsize_main_reciprocal;
						shop_pos *= chipsize;
						bool is_in_cam = Coll::AABBf(	draw_start_chip.x * chipsize, draw_start_chip.y * chipsize	, draw_iterate_num.x * chipsize	, draw_iterate_num.y * chipsize	,
														shop_pos.x - 4.f			, shop_pos.y - 12.f				, shop_pos.x + 4.f				, shop_pos.y + 12.f				);
						shop_pos -= composite_pos + cam_pos;
						if (is_in_cam == true)
						{
							DrawRectRotaGraphFast2
							(
								XY_F2I(shop_pos),
								16, 52,
								8, 12,
								4, 12,
								1.f, 0.f,
								texture, TRUE
							);
						}
					}
				}
				// goal
				if (it->id.CLASS == bgObjectGroup::Goal)
				{
					Vector2 goal_pos = it->pos;
					goal_pos *= chipsize_main_reciprocal;
					goal_pos *= chipsize;
					goal_pos -= composite_pos + cam_pos;
					DrawRectRotaGraphFast2
					(
						SCASI(goal_pos.x),
						SCASI(goal_pos.y),
						9, 52,
						7, 12,
						4, 12,
						1.f, 0.f,
						texture, TRUE
					);
				}
			}
#endif
			// プレイヤー位置
			{
				pl_pos = cam.GetTargetPos() * chipsize_main_reciprocal;
				pl_pos *= chipsize;
				pl_pos -= composite_pos + cam_pos;
				pl_pos.x -= 1.f;
				pl_pos.y -= 5.f;
				DrawBox
				(
					scasi(pl_pos.x),
					scasi(pl_pos.y),
					scasi(pl_pos.x) + 2.f,
					scasi(pl_pos.y) + 2.f,
					GetColor(180, 170, 250), 1
				);
			}

		}

		// ui
		{
			Vector2 telepo_pos;
			telepo_pos = telepo_destination_pos.GetTargetPosAtVector2();
			telepo_pos *= chipsize_main_reciprocal;
			telepo_pos *= chipsize;
			telepo_pos -= composite_pos + cam_pos;

			int texture = pTexture->getGraphs(pTexture->BGCHIP_MINI);
			SetDrawScreen(mini_map_ui_ghnd);
			ClearDrawScreen();
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
			{
				if (is_selecting_telepo_dest)
				{
					// カーソル描画
					{
						DrawRectRotaGraphFast2
						(
							SCASI(telepo_pos.x - 8.f),
							SCASI(telepo_pos.y - 12.f),
							0, 64,
							14, 14,
							0, 0,
							1.f, 0.f,
							texture, TRUE
						);
					}
				}
			}
			if (is_selecting_telepo_dest)
			{
				DrawGraph(0, 0, pTexture->getGraphs(Texture::UI_TELEPORTER), TRUE);
				//DrawRectGraph(6, 330, 0, 312, 60, 24, pTexture->getGraphs(Texture::UI_FONT), TRUE);
			}
		}

		// マスク画面
		{
			SetDrawScreen(mask_ghnd);
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);

			float mask_size_div2 = 64.f * 0.5f;
			pl_pos += composite_pos + cam_pos;
			DrawGraph
			(
				scasi(pl_pos.x - mask_size_div2),
				scasi(pl_pos.y - mask_size_div2),
				mask_ghnd_src, TRUE
			);
			//DrawCircle
			//(
			//	scasi(pl_pos.x),
			//	scasi(pl_pos.y),
			//	32,GetColor(0,0,0), TRUE
			//);

		}

		// コンポジット
		{
			SetDrawScreen(mini_map_composite_ghnd);
			ClearDrawScreen();

			// mini_map
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
			DrawGraph(0, 0, mini_map_bg_ghnd, 1);

			//// ui
			//SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			//DrawGraph(0, 0, mini_map_ui_ghnd, 1);

			// マスク処理
			{
				// マスク画面とミニマップを減算ブレンド後に輝度クリップフィルタで黒部分をアルファ0に
				{
					GraphBlendRectBlt
					(
						mini_map_ghnd, mask_ghnd, mask_blend_ghnd,
						0, 0,
						TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT,
						scasi((cam_pos.x + composite_pos.x)), scasi((cam_pos.y + composite_pos.y)),
						0, 0,
						255,
						DX_GRAPH_BLEND_DIFFERENCE
					);
					GraphFilter(mask_blend_ghnd, DX_GRAPH_FILTER_BRIGHT_CLIP, DX_CMP_LESS, 33, TRUE, bg_color.GetCol(), 0);
				}

				SetDrawScreen(mini_map_composite_ghnd);
				ClearDrawScreen();
				// bg
				{
					SetDrawBlendMode(DX_BLENDMODE_ALPHA, bg_color.GetAlpha());
					DrawRectRotaGraph2
					(
						scasi(rect_min.x + pos.x),		// pos
						scasi(rect_min.y + pos.y),
						0,								// src 
						0,
						scasi(rect_max.x - rect_min.x),	// w, h
						scasi(rect_max.y - rect_min.y),
						0,								// cx, cy
						0,
						1.0,							// extrate
						0.0,							// angle
						mini_map_bg_ghnd, 1
					);
				}
				// masked minimap
				{
					SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
					DrawRectRotaGraph2
					(
						scasi(rect_min.x + pos.x),		// pos
						scasi(rect_min.y + pos.y),
						/*scasi(rect_min.x * 0.5f)*/0,	// src 
						/*scasi(rect_min.y * 0.5f)*/0,
						scasi(rect_max.x - rect_min.x),	// w, h
						scasi(rect_max.y - rect_min.y),
						0,								// cx, cy
						0,
						1.0,							// extrate
						0.0,							// angle
						mask_blend_ghnd, 1				// ghnd, tflag
					);
					//DrawRectGraph(0, 0, 0, 0, TEMP_SCREEN_WIDTH, TEMP_SCREEN_HEIGHT, DX_SCREEN_BACK, 1);

				}
				// ui
				{
					DrawRectRotaGraph2
					(
						scasi(rect_min.x + pos.x),		// pos
						scasi(rect_min.y + pos.y),
						/*scasi(rect_min.x * 0.5f)*/0,	// src 
						/*scasi(rect_min.y * 0.5f)*/0,
						scasi(rect_max.x - rect_min.x),	// w, h
						scasi(rect_max.y - rect_min.y),
						0,								// cx, cy
						0,
						1.0,							// extrate
						0.0,							// angle
						mini_map_ui_ghnd, 1				// ghnd, tflag
					);
				}
				SetDrawScreen(DX_SCREEN_BACK);
				ClearDrawScreen();
			}
		}
	}
	SetDrawScreen(prev_ghnd); //描画先をもとに戻す
	SetDrawBlendMode(prev_blendmode, prev_blendval);

	if (/*show_debug*/0)
	{
		DrawCircle(scasi(rect_min.x + pos.x), scasi(rect_min.y + pos.x), 4, GetColor(200, 100, 200), 1);
		DrawCircle(scasi(rect_max.x + pos.y), scasi(rect_max.y + pos.y), 4, GetColor(200, 100, 200), 1);

		DrawCircle(scasi(miniaturization_rect_min_pos.x + pos.x), scasi(miniaturization_rect_min_pos.y + pos.x), 10, GetColor(50, 100, 200), 0);
		DrawCircle(scasi(miniaturization_rect_max_pos.x + pos.y), scasi(miniaturization_rect_max_pos.y + pos.y), 10, GetColor(50, 100, 200), 0);
		DrawCircle(scasi(maximization_rect_min_pos.x + pos.x), scasi(maximization_rect_min_pos.y + pos.x), 10, GetColor(200, 100, 50), 0);
		DrawCircle(scasi(maximization_rect_max_pos.x + pos.y), scasi(maximization_rect_max_pos.y + pos.y), 10, GetColor(200, 100, 50), 0);
	}
}

void MiniMap::Draw()
{
	if (mini_map_composite_ghnd <= 0)return;
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, scasi(blend_alpha.val));
	DrawGraph(0, 0, mini_map_composite_ghnd, 1);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

void MiniMap::ImGui()
{
	if (Iwin_flg.minimap == false)return;
	ImGui::Begin("MiniMap", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar | ImGuiWindowFlags_MenuBar);
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File##MiniMap"))
			{
				std::array<std::string, eArchiveTypes::ARTYPE_END>
					archive_types = { ".bin", ".json", ".ini", ".xml" };

				if (ImGui::BeginMenu("Save as...##MiniMapSave"))
				{
					for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
					{
						if (ImGui::MenuItem(std::string(archive_types[ar_i] + " file##MiniMapSave").c_str()))
						{
							std::string save_filename = GetSaveFileNameWithExplorer(true, archive_types[ar_i]);
							if (save_filename.empty() == false)
								SaveDataToFile(save_filename, (eArchiveTypes)ar_i);
						}
					}

					ImGui::EndMenu();
				}

				if (ImGui::BeginMenu("Load##MiniMapLoad"))
				{
					for (int ar_i = 0; ar_i < eArchiveTypes::ARTYPE_END; ar_i++)
					{
						if (ImGui::MenuItem(std::string(archive_types[ar_i] + " file##MiniMapLoad").c_str()))
						{
							std::string save_filename = GetOpenFileNameWithExplorer(true, archive_types[ar_i]);
							if (save_filename.empty() == false)
								LoadDataFromFile(save_filename, (eArchiveTypes)ar_i);
						}
					}

					ImGui::EndMenu();
				}

				ImGui::EndMenu();
			}

			ImGui::EndMenuBar();
		}

		if (ImGui::Button("CreateScreen()"))
			CreateScreen();
		if (ImGui::Button("DeleteScreen()"))
			DeleteScreen();

		if (ImGui::Button("Miniaturization()"))
			Miniaturization();
		if (ImGui::Button("Maximization()"))
			Maximization();
		if (ImGui::Button("SwitchSize()"))
			SwitchSize();

		ImGui::SetNextTreeNodeOpen(false, ImGuiSetCond_Once);
		if (ImGui::TreeNode("descriptions"))
		{
			ImGui::Text("draw_start_chip.x: %f", draw_start_chip.x);
			ImGui::Text("draw_start_chip.y: %f", draw_start_chip.y);
			ImGui::Text("draw_iterate_num.x: %f", draw_iterate_num.x);
			ImGui::Text("draw_iterate_num.y: %f", draw_iterate_num.y);

			ImGui::TreePop();
		}

		pos.ImGuiTreeNode("MiniMap::pos");
		mini_map_scroll_pos.ImGuiTreeNode("MiniMap::mini_map_scroll_pos");
		mini_map_extrate.ImGuiTreeNode("MiniMap::mini_map_extrate");
		blend_alpha.ImGuiTreeNode("MiniMap::blend_alpha");
		rect_min.ImGuiTreeNode("MiniMap::rect_min");
		rect_max.ImGuiTreeNode("MiniMap::rect_max");
		bg_color.ImGuiTreeNode("MiniMap::bg_color");

		{
			float* p_pos[2] = { &miniaturization_rect_min_pos.x	, &miniaturization_rect_min_pos.y };
			ImGui::DragFloat2("miniaturization_rect_min_pos", *p_pos, 1.f, 0.f, FLT_MAX, "%f", 0.3f);
		}
		{
			float* p_pos[2] = { &miniaturization_rect_max_pos.x	, &miniaturization_rect_max_pos.y };
			ImGui::DragFloat2("miniaturization_rect_max_pos", *p_pos, 1.f, 0.f, FLT_MAX, "%f", 0.3f);
		}
		{
			float* p_pos[2] = { &maximization_rect_min_pos.x	, &maximization_rect_min_pos.y };
			ImGui::DragFloat2("maximization_rect_min_pos", *p_pos, 1.f, 0.f, FLT_MAX, "%f", 0.3f);
		}
		{
			float* p_pos[2] = { &maximization_rect_max_pos.x	, &maximization_rect_max_pos.y };
			ImGui::DragFloat2("maximization_rect_max_pos", *p_pos, 1.f, 0.f, FLT_MAX, "%f", 0.3f);
		}

		ImGui::DragFloat("miniaturization_ease_time", &miniaturization_ease_time, 1.f, 1.f, 360.f, "%f", 1.f);
		ImGui::DragFloat("maximization_ease_time", &maximization_ease_time, 1.f, 1.f, 360.f, "%f", 1.f);
		ImGui::DragInt("miniaturization_blend_alpha", &miniaturization_blend_alpha, 1.f, 0, 255);
		ImGui::DragInt("maximization_blend_alpha", &maximization_blend_alpha, 1.f, 0, 255);

		// miniaturization_ease_algo
		{
			if (ImGui::BeginCombo("miniaturization_ease_algo", Easing::algo_names[miniaturization_ease_algo].c_str())) // コンボメニューに表示する文字
			{
				for (int n = 0; n < (int)Easing::algo_names.size(); n++)
				{
					bool is_selected;
					is_selected = (miniaturization_ease_algo == n);
					if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
						miniaturization_ease_algo = n;
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
		}
		// maximization_ease_algo
		{
			if (ImGui::BeginCombo("maximization_ease_algo", Easing::algo_names[maximization_ease_algo].c_str())) // コンボメニューに表示する文字
			{
				for (int n = 0; n < (int)Easing::algo_names.size(); n++)
				{
					bool is_selected;
					is_selected = (maximization_ease_algo == n);
					if (ImGui::Selectable(Easing::algo_names[n].c_str(), is_selected))
						maximization_ease_algo = n;
					if (is_selected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
		}

		ImGui::InputInt("chip_size", &chip_size, 1, 1);
	}
	ImGui::End();
}
