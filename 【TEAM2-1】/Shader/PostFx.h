#pragma once
#include "GaussBlur.h"
#include "..\\common.h"
#include "..\\Singleton.h"
#include "..\\CerealLoadAndSave.h"
#include <string>

class PostFx : public Singleton<PostFx>
{
public:
	PostFx() 
	{
		gauss_blur_shader_name			= "multi_gauss_blur_ps";
		luminance_key_shader_name		= "luminance_key_ps";
		lens_ghosts_shader_name			= "lens_ghosts_ps";
		anamorphic_flare_shader_name	= "anamorphic_flare_ps";

		composite_screen = MakeScreen(_SCREEN_WIDTH, _SCREEN_HEIGHT, 1);
		post_fx_shader_manager.Init((float)_SCREEN_WIDTH, (float)_SCREEN_HEIGHT);/* = PostFXShaderManager::pos((float)SCREEN_WIDTH, (float)SCREEN_HEIGHT);*/
		{
			pass_screen_params_multiple_gauss_gblur.scr_w = (float)_SCREEN_WIDTH;
			pass_screen_params_multiple_gauss_gblur.scr_h = (float)_SCREEN_HEIGHT;
			pass_screen_params_multiple_gauss_gblur.div_num = 4.f;
			pass_screen_params_multiple_gauss_gblur.downsample_buf_num_max = 4;
			pass_screen_params_multiple_gauss_gblur.pass_num_max = 2;

			pass_screen_params_luminance_key.scr_w = (float)_SCREEN_WIDTH;
			pass_screen_params_luminance_key.scr_h = (float)_SCREEN_HEIGHT;
			pass_screen_params_luminance_key.div_num = 1.f;
			pass_screen_params_luminance_key.downsample_buf_num_max = 0;
			pass_screen_params_luminance_key.pass_num_max = 0;

			pass_screen_params_lens_ghosts.scr_w = (float)_SCREEN_WIDTH;
			pass_screen_params_lens_ghosts.scr_h = (float)_SCREEN_HEIGHT;
			pass_screen_params_lens_ghosts.div_num = 8.f;
			pass_screen_params_lens_ghosts.downsample_buf_num_max = 0;
			pass_screen_params_lens_ghosts.pass_num_max = 0;

			//pass_screen_params_anamorphic_flare.scr_w = (float)SCREEN_WIDTH;
			//pass_screen_params_anamorphic_flare.scr_h = (float)SCREEN_HEIGHT;
			//pass_screen_params_anamorphic_flare.div_num = 1.f;
			//pass_screen_params_anamorphic_flare.downsample_buf_num_max = 4;
			//pass_screen_params_anamorphic_flare.pass_num_max = 1;
		}
		{
			option_params.multiple_gauss_blur.blur_vector = { 1.0f,1.0f };
			option_params.multiple_gauss_blur.deviation = 2.5f;
			option_params.multiple_gauss_blur.multiply = 2.0f;
			option_params.multiple_gauss_blur.sample_count = 16;

			option_params.luminance_key.scr_w = (float)_SCREEN_WIDTH;
			option_params.luminance_key.scr_h = (float)_SCREEN_HEIGHT;
			option_params.luminance_key.cbuf_params.color_sub_rate = { -0.9f,-0.9f,-0.9f,1.0f };
			option_params.luminance_key.cbuf_params.luminance_threshold = 6.0f;
			option_params.luminance_key.cbuf_params.luminance_multipley = 1.0f;

			option_params.lens_ghosts.scr_w = (float)_SCREEN_WIDTH;
			option_params.lens_ghosts.scr_h = (float)_SCREEN_HEIGHT;
			//SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
			option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_ghost_dispersal = 1.f;
			option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_intensity = 1.f;
			option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_threshold = 1.f;
			option_params.lens_ghosts.cbuf_lens_ghosts_b0.number_of_ghosts = 5.f;

			//option_params.anamorphic_flare.blur_vector = { 1.0f,1.0f };
			//option_params.anamorphic_flare.deviation = 50.0f;
			//option_params.anamorphic_flare.multiply = 2.5f;
			//option_params.anamorphic_flare.sample_count = 16;
		}

		//multicple gaussian blur
		post_fx_shader_manager.CreateShaderSets(gauss_blur_shader_name, new GaussBlurShaderObject(&option_params.multiple_gauss_blur, &pass_screen_params_multiple_gauss_gblur));
		post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name)->MakeScreenHandle
		(
			(int)pass_screen_params_multiple_gauss_gblur.scr_w,
			(int)pass_screen_params_multiple_gauss_gblur.scr_h,
			1
		);
		//luminance key
		post_fx_shader_manager.CreateShaderSets(luminance_key_shader_name, new LuminanceKeyShaderObject(&option_params.luminance_key, &pass_screen_params_luminance_key));
		post_fx_shader_manager.GetPassScreenHandles(luminance_key_shader_name)->MakeScreenHandle
		(
			(int)pass_screen_params_luminance_key.scr_w,
			(int)pass_screen_params_luminance_key.scr_h,
			pass_screen_params_luminance_key.div_num
		);
		//lens ghosts
		post_fx_shader_manager.CreateShaderSets(lens_ghosts_shader_name, new LensGhostsShaderObject(&option_params.lens_ghosts, &pass_screen_params_lens_ghosts));
		post_fx_shader_manager.GetPassScreenHandles(lens_ghosts_shader_name)->MakeScreenHandle
		(
			(int)pass_screen_params_lens_ghosts.scr_w,
			(int)pass_screen_params_lens_ghosts.scr_h,
			pass_screen_params_lens_ghosts.div_num
		);
		////anamorphic flare
		//post_fx_shader_manager.CreateShaderSets(anamorphic_flare_shader_name, new AnamorphicFlareShaderObject(&option_params_anamorphic_flare, &pass_screen_params_anamorphic_flare));
		//post_fx_shader_manager.GetPassScreenHandles(anamorphic_flare_shader_name)->MakeScreenHandle
		//(
		//	(int)pass_screen_params_anamorphic_flare.scr_w,
		//	(int)pass_screen_params_anamorphic_flare.scr_h,
		//	1
		//);
		//post_fx_shader_manager.CreateShaderSets(anamorphic_flare_shader_name, new AnamorphicFlareShaderObject(&option_params_anamorphic_flare, &pass_screen_params_anamorphic_flare));
		//post_fx_shader_manager.GetPassScreenHandles(anamorphic_flare_shader_name)->MakeScreenHandle
		//(
		//	(int)pass_screen_params_anamorphic_flare.scr_w,
		//	(int)pass_screen_params_anamorphic_flare.scr_h,
		//	1
		//);

		//shader_file
		post_fx_shader_manager.GetShaderHandles()->LoadVS("./Data/Shader/hlsl/Compiled/VertexShaderTestVS.vso"	, "test_vs");
		post_fx_shader_manager.GetShaderHandles()->LoadPS("./Data/Shader/hlsl/Compiled/PSGaussianBlur.pso"		, "gauss_blur_ps");
		post_fx_shader_manager.GetShaderHandles()->LoadPS("./Data/Shader/hlsl/Compiled/composite_ps2.pso"		, "compo_ps");
		post_fx_shader_manager.GetShaderHandles()->LoadPS("./Data/Shader/hlsl/Compiled/PSLuminanceKey.pso"		, "luminance_key_ps");
		post_fx_shader_manager.GetShaderHandles()->LoadPS("./Data/Shader/hlsl/Compiled/PSPesudoLensFlare.pso"	, "lens_ghosts_ps");
	}
	~PostFx() { DeleteGraph(composite_screen); }
	void Init(const int& _current_scene);
	void UnInit();

	void Update();
	void Render(int* _srv);
	void ImGui();

	// DX_SCREEN_BACKで描画してください
	void Draw();

	void LoadDataFromFile(const std::string& _path, eArchiveTypes _file_type);
	void SaveDataToFile(const std::string& _path, eArchiveTypes _file_type);

public:
	std::string gauss_blur_shader_name;
	std::string luminance_key_shader_name;
	std::string lens_ghosts_shader_name;
	std::string anamorphic_flare_shader_name;
	PostFXShaderManager post_fx_shader_manager;
	struct OptionParams
	{
		OptionParamsMultipleGaussBlur	multiple_gauss_blur;
		OptionParamsLuminanceKey		luminance_key;
		OptionParamsLensGhost			lens_ghosts;

		template< class Archive >	 
		void serialize(Archive& _ar, const uint32_t _version)
		{
			_ar
			(
				CEREAL_NVP(multiple_gauss_blur),
				CEREAL_NVP(luminance_key),
				CEREAL_NVP(lens_ghosts)
			);

			if (_version > 1)
			{

			}
		}
	};

	OptionParams					option_params;
	//OptionParamsMultipleGaussBlur	option_params_multiple_gauss_blur;
	//OptionParamsMultipleGaussBlur	option_params_anamorphic_flare;
	//OptionParamsLuminanceKey		option_params_luminance_key;
	//OptionParamsLensGhost			option_params_lens_ghosts;

	PassScreenParams pass_screen_params_multiple_gauss_gblur;
	PassScreenParams pass_screen_params_luminance_key;
	PassScreenParams pass_screen_params_lens_ghosts;
	PassScreenParams pass_screen_params_anamorphic_flare;
	int				 composite_screen;

	bool is_enable_post_fx;
	int current_scene = 0;

	static const std::string set_scene_names[/*scene num*/4];

};

#define POSTFX (PostFx::GetIns())

CEREAL_CLASS_VERSION(PostFx::OptionParams, 1);
