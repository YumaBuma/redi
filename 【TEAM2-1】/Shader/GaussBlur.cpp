#include "GaussBlur.h"
#include "..\\Function.h"


/*
PassScreenHandles
*/
PassScreenHandles::PassScreenHandles()
{

}

PassScreenHandles::~PassScreenHandles()
{
	auto it_end = pass_screens.end();
	for (auto it = pass_screens.begin(); it < it_end; ++it)
	{
		if (*it == -1)continue;

		DeleteGraph(*it);
	}
	{ std::vector<int>().swap(pass_screens); }
}

int PassScreenHandles::MakeScreenHandle(
	int screen_w, int screen_h, int div_num,
	bool can_vaild_graph, bool can_float_graph,
	bool can_msaa,
	int msaa_samples, int msaa_quality
)
{
	SetUseDivGraphFlag(false);

	SetDrawValidGraphCreateFlag(false);
	SetUseAlphaTestGraphCreateFlag(false);
	SetDrawValidFloatTypeGraphCreateFlag(can_float_graph);
	{
		if (can_msaa)
		{
			SetCreateDrawValidGraphMultiSample(msaa_samples, msaa_quality);
		}

		pass_screens.emplace_back(MakeScreen(screen_w / div_num, screen_h / div_num));
	}
	SetDrawValidFloatTypeGraphCreateFlag(false);
	SetUseAlphaTestGraphCreateFlag(true);
	SetDrawValidGraphCreateFlag(false);
	return pass_screens.back();
}

int PassScreenHandles::DeleteLastScreenHandle()
{
	if (pass_screens.empty())return 0;

	return DeleteGraph(pass_screens.back());
}

std::vector<int>* PassScreenHandles::GetPassScreens()
{
	return &this->pass_screens;
}

/*
ShaderHandles
*/
ShaderHandles::ShaderHandles()
{

}

ShaderHandles::~ShaderHandles()
{
	InitShader();
	{ std::map<std::string, int>().swap(ps_handle); }
	{ std::map<std::string, int>().swap(vs_handle); }
}

const int ShaderHandles::LoadVS(const char * path, std::string naming)
{
	auto it = vs_handle.find(naming);
	if (it != vs_handle.end()) return true;

	int vs_hnd = LoadVertexShader(path);

	if (vs_hnd == -1)
	{
		std::string str;
		printf_s(str.c_str(), "\"%s\"not found\n", path);
		//assertDxInfo(vs_hnd != -1, str);
	}

	vs_handle.insert(std::make_pair(naming, vs_hnd));
	return vs_hnd;
}

const int ShaderHandles::LoadPS(const char * path, std::string naming)
{
	auto it = ps_handle.find(naming);
	if (it != ps_handle.end()) return true;

	int ps_hnd = LoadPixelShader(path);

	if (ps_hnd == -1)
	{
		std::string str;
		printf_s(str.c_str(), "\"%s\"not found\n", path);
		//assertDxInfo(ps_hnd != -1, str);
	}

	ps_handle.insert(std::make_pair(naming, ps_hnd));
	return ps_hnd;
}

int* ShaderHandles::GetVS(std::string name)
{
	auto it = vs_handle.find(name);
	if (it != vs_handle.end()) return &it->second;
	else
	{
		std::string str;
		printf_s(str.c_str(), "\"%s\" is not loaded\n", name.c_str());
		//assertDxInfo(false, str);
	}
	return nullptr;
}

int* ShaderHandles::GetPS(std::string name)
{
	auto it = ps_handle.find(name);
	if (it != ps_handle.end()) return &it->second;
	else
	{
		std::string str;
		printf_s(str.c_str(), "\"%s\" is not loaded\n", name.c_str());
		//assertDxInfo(false, str);
	}
	return nullptr;
}

/*
BasePostFXShaderObject
*/
BasePostFXShaderObject::BasePostFXShaderObject(void* option_params, PassScreenParams* pass_scr_params)
{
	option = option_params;
	pass_screen_params = pass_scr_params;
}

//BasePostFXShaderObject::BasePostFXShaderObject(float screen_w, float screen_h)
//{
//
//}

BasePostFXShaderObject::~BasePostFXShaderObject()
{
}

inline void BasePostFXShaderObject::SetVS(const int& vs)
{
	SetUseVertexShader(vs);
}
inline void BasePostFXShaderObject::SetPS(const int& ps)
{
	SetUseVertexShader(ps);
}

inline void BasePostFXShaderObject::SetSamplingTexture(const int register_num, const int& screen_handle)
{
	SetUseTextureToShader(register_num, screen_handle);
}

void* BasePostFXShaderObject::GetOptionParams()
{
	return option;
}

PassScreenParams* BasePostFXShaderObject::GetPassScreenParams()
{
	return pass_screen_params;
}

/*
GaussBlurShaderObject
*/
GaussBlurShaderObject::GaussBlurShaderObject(void* option_params, PassScreenParams* pass_screen_params) :BasePostFXShaderObject(option_params, pass_screen_params)
{
	option_gauss_blur = (OptionParamsMultipleGaussBlur*)option;
	vs_cbuf_handle = CreateShaderConstantBuffer(sizeof(float) * 4);
	ps_cbuf_handle = CreateShaderConstantBuffer(sizeof(GaussBlurParam));
}

GaussBlurShaderObject::~GaussBlurShaderObject()
{
	DeleteShaderConstantBuffer(vs_cbuf_handle);
	DeleteShaderConstantBuffer(ps_cbuf_handle);
}

void GaussBlurShaderObject::Render(
	VertexAndIndex* v_and_i,
	ShaderHandles* shader_handles,
	PassScreenHandles* pass_screens,
	int* sample_textures,
	void* option_params
)
{
	SetDrawMode(DX_DRAWMODE_BILINEAR);
	FLOAT4* f4;
	// 頂点シェーダー用の定数バッファのアドレスを取得
	f4 = (FLOAT4 *)GetBufferShaderConstantBuffer(vs_cbuf_handle);
	src = (GaussBlurParam *)GetBufferShaderConstantBuffer(ps_cbuf_handle);

	auto UpdateVtx = [&, this](float _w, float _h)
	{
		// 座標値を取得したアドレスに書き込み
		f4->x = 0.0f;
		f4->y = 0.0f;
		f4->z = 0.0f;
		f4->w = 0.0f;

		v_and_i->vertex[0].pos = VGet(0.0f, _h, 0.0f);
		v_and_i->vertex[1].pos = VGet(_w, _h, 0.0f);
		v_and_i->vertex[2].pos = VGet(0.0f, 0.0f, 0.0f);
		v_and_i->vertex[3].pos = VGet(_w, 0.0f, 0.0f);
		UpdateShaderConstantBuffer(vs_cbuf_handle);
		//SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
	};

	int w = int(pass_screen_params->scr_w / pass_screen_params->div_num);
	int h = int(pass_screen_params->scr_h / pass_screen_params->div_num);
	float deviation = option_gauss_blur->deviation;

	SetUseVertexShader(*shader_handles->GetVS("test_vs"));
	SetUsePixelShader(*shader_handles->GetPS("gauss_blur_ps"));
	SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
	SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);

	UpdateVtx((float)w, (float)h);
	SetDrawValidFloatTypeGraphCreateFlag(TRUE);
	static int tmpscr = MakeScreen((int)pass_screen_params->scr_w, (int)pass_screen_params->scr_h);
	{
		GraphFilterBlt
		(
			sample_textures[0], tmpscr, DX_GRAPH_FILTER_GAUSS,
			8,	//使用ピクセル幅(8, 16, 32 の何れか),
			100	//ぼかしパラメータ(100 で約1ピクセル分の幅)
		);
	}
	SetDrawScreen(pass_screens->GetPassScreens()->at(0));
	{
		ClearDrawScreen();

		SetUseTextureToShader(0, tmpscr);

		*src = CalcBlurParam(w, h, Vector2(option_gauss_blur->blur_vector.x, 0.0f), deviation, 1.0f, option_gauss_blur->sample_count);

		UpdateShaderConstantBuffer(ps_cbuf_handle);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);
	}

	SetDrawScreen(pass_screens->GetPassScreens()->at(1));
	{
		ClearDrawScreen();

		SetUseTextureToShader(0, pass_screens->GetPassScreens()->at(0));

		*src = CalcBlurParam(w, h, Vector2(0.0f, option_gauss_blur->blur_vector.y), deviation, 1.0f, option_gauss_blur->sample_count);

		UpdateShaderConstantBuffer(ps_cbuf_handle);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);

	}

	w /= 2;
	h /= 2;
	float m = option_gauss_blur->multiply;
	for (int i = 1; i < pass_screen_params->downsample_buf_num_max; i++)
	{
		if (pass_screens->GetPassScreens()->size() < i)break;
		UpdateVtx((float)w, (float)h);
		SetDrawValidGraphCreateFlag(true); SetDrawValidFloatTypeGraphCreateFlag(true); //SetCreateDrawValidGraphMultiSample(4, 2);

		SetDrawScreen(pass_screens->GetPassScreens()->at(i * 2));
		{
			ClearDrawScreen();

			SetUseTextureToShader(0, pass_screens->GetPassScreens()->at(i * 2 - 1));

			*src = CalcBlurParam(w, h, Vector2(option_gauss_blur->blur_vector.x, 0.0f), deviation, m, option_gauss_blur->sample_count);

			UpdateShaderConstantBuffer(ps_cbuf_handle);

			DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);

		}

		SetDrawScreen(pass_screens->GetPassScreens()->at(i * 2 + 1));
		{
			ClearDrawScreen();

			SetUseTextureToShader(0, pass_screens->GetPassScreens()->at(i * 2));

			*src = CalcBlurParam(w, h, Vector2(0.0f, option_gauss_blur->blur_vector.y), deviation, m, option_gauss_blur->sample_count);

			UpdateShaderConstantBuffer(ps_cbuf_handle);

			SetUseVertexShader(*shader_handles->GetVS("test_vs"));

			DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);

		}

		w /= 2;
		h /= 2;
		m *= 2.0f;
	}

	//composite
	UpdateVtx(pass_screen_params->scr_w, pass_screen_params->scr_h);
	SetDrawScreen(pass_screens->GetPassScreens()->at(pass_screen_params->downsample_buf_num_max * pass_screen_params->pass_num_max));
	{
		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
		ClearDrawScreen();

		SetUseVertexShader(*shader_handles->GetVS("test_vs"));
		SetUsePixelShader(*shader_handles->GetPS("compo_ps"));

		for (int i = 0; i < pass_screen_params->downsample_buf_num_max; i++)
		{
			if (pass_screens->GetPassScreens()->size() < i)break;

			SetUseTextureToShader(1 + i, pass_screens->GetPassScreens()->at(1 + i * pass_screen_params->pass_num_max));
		}

		UpdateShaderConstantBuffer(vs_cbuf_handle);
		UpdateShaderConstantBuffer(ps_cbuf_handle);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);
	}
	SetDrawMode(DX_DRAWMODE_NEAREST);
	SetDrawValidGraphCreateFlag(false); SetDrawValidFloatTypeGraphCreateFlag(false);
}

/*
GaussBlurShaderObject
*/
AnamorphicFlareShaderObject::AnamorphicFlareShaderObject(void* option_params, PassScreenParams* pass_screen_params) :BasePostFXShaderObject(option_params, pass_screen_params)
{
	option_gauss_blur = (OptionParamsMultipleGaussBlur*)option;
	vs_cbuf_handle = CreateShaderConstantBuffer(sizeof(float) * 4);
	ps_cbuf_handle = CreateShaderConstantBuffer(sizeof(GaussBlurParam));
}

AnamorphicFlareShaderObject::~AnamorphicFlareShaderObject()
{
	DeleteShaderConstantBuffer(vs_cbuf_handle);
	DeleteShaderConstantBuffer(ps_cbuf_handle);
}

void AnamorphicFlareShaderObject::Render(
	VertexAndIndex* v_and_i,
	ShaderHandles* shader_handles,
	PassScreenHandles* pass_screens,
	int* sample_textures,
	void* option_params
)
{
	SetDrawMode(DX_DRAWMODE_BILINEAR);
	FLOAT4* f4;
	// 頂点シェーダー用の定数バッファのアドレスを取得
	f4 = (FLOAT4 *)GetBufferShaderConstantBuffer(vs_cbuf_handle);
	src = (GaussBlurParam *)GetBufferShaderConstantBuffer(ps_cbuf_handle);

	auto UpdateVtx = [&, this](float _w, float _h)
	{
		// 座標値を取得したアドレスに書き込み
		f4->x = 0.0f;
		f4->y = 0.0f;
		f4->z = 0.0f;
		f4->w = 0.0f;

		v_and_i->vertex[0].pos = VGet(0.0f, _h, 0.0f);
		v_and_i->vertex[1].pos = VGet(_w, _h, 0.0f);
		v_and_i->vertex[2].pos = VGet(0.0f, 0.0f, 0.0f);
		v_and_i->vertex[3].pos = VGet(_w, 0.0f, 0.0f);
		UpdateShaderConstantBuffer(vs_cbuf_handle);
		SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
	};

	int w = int(pass_screen_params->scr_w / pass_screen_params->div_num);
	int h = int(pass_screen_params->scr_h / pass_screen_params->div_num);
	float deviation = option_gauss_blur->deviation;

	UpdateVtx((float)w, (float)h);

	SetUseVertexShader(*shader_handles->GetVS("test_vs"));
	SetUsePixelShader(*shader_handles->GetPS("gauss_blur_ps"));

	SetDrawValidGraphCreateFlag(true); SetDrawValidFloatTypeGraphCreateFlag(true); SetDrawValidAlphaChannelGraphCreateFlag(true);
	int tempscr = MakeScreen((int)pass_screen_params->scr_w, (int)pass_screen_params->scr_h);

	SetDrawScreen(tempscr);
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		DrawGraph(0, 0, sample_textures[0], true);

		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawBox(
				(int)((pass_screen_params->scr_w / (pass_screen_params->scr_w / 30.f))),
				0,
				(int)(-(pass_screen_params->scr_w / (pass_screen_params->scr_w / 30.f)) + pass_screen_params->scr_w),
				pass_screen_params->scr_h,
				GetColor(0, 0, 0), true
			);
		}
		for (int i = 1; i <= 2; i++)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - (255 * (i / 2)));
			DrawBox(i - 1, 0, i, (int)pass_screen_params->scr_h, GetColor(0, 0, 0), true);
			DrawBox((int)pass_screen_params->scr_w - (i - 1), 0, (int)pass_screen_params->scr_w - (i + 1), (int)pass_screen_params->scr_h, GetColor(0, 0, 0), true);
		}


	}

	SetDrawScreen(pass_screens->GetPassScreens()->at(0));
	{
		SetDrawMode(DX_DRAWMODE_BILINEAR);
		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
		ClearDrawScreen();

		SetUseTextureToShader(0, tempscr);

		*src = CalcBlurParam(w, h, option_gauss_blur->blur_vector, deviation, 1.0f, option_gauss_blur->sample_count);

		UpdateShaderConstantBuffer(ps_cbuf_handle);
		SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}

	w /= 2;
	h /= 2;
	float m = option_gauss_blur->multiply;
	for (int i = 1; i < pass_screen_params->downsample_buf_num_max; i++)
	{
		if (pass_screens->GetPassScreens()->size() < i)break;

		UpdateVtx((float)w, (float)h);
		SetDrawScreen(pass_screens->GetPassScreens()->at(i));
		{
			SetDrawMode(DX_DRAWMODE_BILINEAR);
			SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
			ClearDrawScreen();

			SetUseTextureToShader(0, pass_screens->GetPassScreens()->at(i - 1));

			*src = CalcBlurParam(w, h, option_gauss_blur->blur_vector, deviation, m, option_gauss_blur->sample_count);

			UpdateShaderConstantBuffer(ps_cbuf_handle);
			SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);

			DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);

			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
		}

		w /= 2;
		h /= 2;
		m *= 2.0f;
	}

	//composite
	UpdateVtx(pass_screen_params->scr_w, pass_screen_params->scr_h);
	SetDrawScreen(pass_screens->GetPassScreens()->at(pass_screen_params->downsample_buf_num_max * pass_screen_params->pass_num_max));
	{
		SetDrawMode(DX_DRAWMODE_BILINEAR);
		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
		ClearDrawScreen();

		SetUseVertexShader(*shader_handles->GetVS("test_vs"));
		SetUsePixelShader(*shader_handles->GetPS("compo_ps"));


		//SetUseTextureToShader(0, sample_textures[0]);
		for (int i = 0; i < pass_screen_params->downsample_buf_num_max; i++)
		{
			if (pass_screens->GetPassScreens()->size() < i)break;

			SetUseTextureToShader(1 + i, pass_screens->GetPassScreens()->at(i));
		}

		UpdateShaderConstantBuffer(vs_cbuf_handle);
		UpdateShaderConstantBuffer(ps_cbuf_handle);
		SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
		SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);
	}

	DeleteGraph(tempscr);

	//		int			GraphFilter( int GrHandle, int FilterType = DX_GRAPH_FILTER_MONO, int Cb = 青色差( -255 〜 255 ), int Cr = 赤色差( -255 〜 255 ) ) ;
	//GraphFilterBlt( int GrHandle, int FilterType = DX_GRAPH_FILTER_GRADIENT_MAP,
	//int MapGrHandle = グラデーションマップのグラフィックハンドル( 元画像の輝度からグラデーションマップ画像の x 座標を算出しますので縦幅は1dotでもＯＫ ), 
	//int Reverse = グラデーションマップ左右反転フラグ( TRUE : グラデーションマップを左右反転して使う  FALSE : 左右反転しない ) ) ;
	/*extern int gradation_map_ghnd;
	GraphFilter(
		pass_screens->GetPassScreens()->at(pass_screen_params->downsample_buf_num_max * pass_screen_params->pass_num_max),
		DX_GRAPH_FILTER_GRADIENT_MAP, gradation_map_ghnd, 0);*/


	SetDrawMode(DX_DRAWMODE_NEAREST);//ラスタライザステートと似ている
	SetDrawValidGraphCreateFlag(false); SetDrawValidFloatTypeGraphCreateFlag(false);
}


/*
LuminanceKeyShaderObject
*/
LuminanceKeyShaderObject::LuminanceKeyShaderObject(void * option_params, PassScreenParams* pass_screen_params) :BasePostFXShaderObject(option_params, pass_screen_params)
{
	option_luminance_key = (OptionParamsLuminanceKey*)option;
	vs_cbuf_handle = CreateShaderConstantBuffer(sizeof(float) * 4);
	ps_cbuf_handle = CreateShaderConstantBuffer(sizeof(CbufLuminanceKey));
}

LuminanceKeyShaderObject::~LuminanceKeyShaderObject()
{
	DeleteShaderConstantBuffer(vs_cbuf_handle);
	DeleteShaderConstantBuffer(ps_cbuf_handle);
}

void LuminanceKeyShaderObject::SetPSCbufParams(CbufLuminanceKey cbuf_params)
{
	option_luminance_key->cbuf_params = cbuf_params;
}

void LuminanceKeyShaderObject::Update()
{
}

void LuminanceKeyShaderObject::Render(VertexAndIndex * v_and_i, ShaderHandles * shader_handles, PassScreenHandles * pass_screens, int * sample_textures, void * option_params)
{
	SetDrawMode(DX_DRAWMODE_BILINEAR);

	SetUseVertexShader(*shader_handles->GetVS("test_vs"));
	SetUsePixelShader(*shader_handles->GetPS("luminance_key_ps"));

	FLOAT4* f4;
	// 頂点シェーダー用の定数バッファのアドレスを取得
	f4 = (FLOAT4 *)GetBufferShaderConstantBuffer(vs_cbuf_handle);

	SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);
	SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
	auto UpdateVtx = [&, this](float _w, float _h)
	{
		// 座標値を取得したアドレスに書き込み
		f4->x = 0.0f;
		f4->y = 0.0f;
		f4->z = 0.0f;
		f4->w = 0.0f;

		v_and_i->vertex[0].pos = VGet(0.0f, _h, 0.0f);
		v_and_i->vertex[1].pos = VGet(_w, _h, 0.0f);
		v_and_i->vertex[2].pos = VGet(0.0f, 0.0f, 0.0f);
		v_and_i->vertex[3].pos = VGet(_w, 0.0f, 0.0f);

		UpdateShaderConstantBuffer(vs_cbuf_handle);
		//SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
	};
	UpdateVtx(option_luminance_key->scr_w, option_luminance_key->scr_h);

	CbufLuminanceKey* p_cbuf_luminance_key;
	p_cbuf_luminance_key = (CbufLuminanceKey*)GetBufferShaderConstantBuffer(ps_cbuf_handle);
	*p_cbuf_luminance_key = option_luminance_key->cbuf_params;

	SetDrawScreen(pass_screens->GetPassScreens()->at(0));
	{
		ClearDrawScreen();

		SetUseTextureToShader(0, sample_textures[0]);

		UpdateShaderConstantBuffer(ps_cbuf_handle);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);
	}
}

/*
LensGhostsShaderObject
*/
LensGhostsShaderObject::LensGhostsShaderObject(void * option_params, PassScreenParams* pass_screen_params) :BasePostFXShaderObject(option_params, pass_screen_params)
{
	option_lens_ghosts = (OptionParamsLensGhost*)option;
	vs_cbuf_handle = CreateShaderConstantBuffer(sizeof(float) * 4);
	ps_cbuf_handle = CreateShaderConstantBuffer(sizeof(CbufLensGhosts_b0));
	ps_cbuf_handle_b1 = CreateShaderConstantBuffer(sizeof(CbufLensGhosts_b1));
}


LensGhostsShaderObject::~LensGhostsShaderObject()
{
	DeleteShaderConstantBuffer(vs_cbuf_handle);
	DeleteShaderConstantBuffer(ps_cbuf_handle);
	DeleteShaderConstantBuffer(ps_cbuf_handle_b1);
}

void LensGhostsShaderObject::Render(VertexAndIndex * v_and_i, ShaderHandles * shader_handles, PassScreenHandles * pass_screens, int * sample_textures, void * option_params)
{
	SetDrawMode(DX_DRAWMODE_BILINEAR);

	SetTextureAddressMode(DX_TEXADDRESS_WRAP);
	SetUseVertexShader(*shader_handles->GetVS("test_vs"));
	SetUsePixelShader(*shader_handles->GetPS("lens_ghosts_ps"));

	FLOAT4* f4;
	// 頂点シェーダー用の定数バッファのアドレスを取得
	f4 = (FLOAT4 *)GetBufferShaderConstantBuffer(vs_cbuf_handle);

	SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);
	SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);

	auto UpdateVtx = [&, this](float _w, float _h)
	{
		// 座標値を取得したアドレスに書き込み
		f4->x = 0.0f;
		f4->y = 0.0f;
		f4->z = 0.0f;
		f4->w = 0.0f;

		v_and_i->vertex[0].pos = VGet(0.0f, _h, 0.0f);
		v_and_i->vertex[1].pos = VGet(_w, _h, 0.0f);
		v_and_i->vertex[2].pos = VGet(0.0f, 0.0f, 0.0f);
		v_and_i->vertex[3].pos = VGet(_w, 0.0f, 0.0f);

		UpdateShaderConstantBuffer(vs_cbuf_handle);
		//SetShaderConstantBuffer(vs_cbuf_handle, DX_SHADERTYPE_VERTEX, 4);
	};
	UpdateVtx(
		option_lens_ghosts->scr_w / pass_screen_params->div_num,
		option_lens_ghosts->scr_h / pass_screen_params->div_num
	);

	CbufLensGhosts_b0* p_cbuf_lens_ghosts_b0;
	p_cbuf_lens_ghosts_b0 = (CbufLensGhosts_b0*)GetBufferShaderConstantBuffer(ps_cbuf_handle);
	*p_cbuf_lens_ghosts_b0 = option_lens_ghosts->cbuf_lens_ghosts_b0;
	UpdateShaderConstantBuffer(ps_cbuf_handle);
	//SetShaderConstantBuffer(ps_cbuf_handle, DX_SHADERTYPE_PIXEL, 0);

	//CbufLensGhosts_b1* p_cbuf_lens_ghosts_b1;
	//p_cbuf_lens_ghosts_b1 = (CbufLensGhosts_b1*)GetBufferShaderConstantBuffer(ps_cbuf_handle_b1);
	//*p_cbuf_lens_ghosts_b1 = option_lens_ghosts->cbuf_lens_ghosts_b1;
	//UpdateShaderConstantBuffer(ps_cbuf_handle_b1);
	//SetShaderConstantBuffer(ps_cbuf_handle_b1, DX_SHADERTYPE_PIXEL, 1);

	SetDrawScreen(pass_screens->GetPassScreens()->at(0));
	{
		SetDrawMode(DX_DRAWMODE_ANISOTROPIC);
		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
		ClearDrawScreen();

		SetUseTextureToShader(0, sample_textures[0]);

		DrawPolygonIndexed3DToShader(v_and_i->vertex, 4, v_and_i->index, 2);
	}
	SetTextureAddressMode(DX_TEXADDRESS_CLAMP);
}

/*
PostFXShaderManager
*/
PostFXShaderManager::PostFXShaderManager(float screen_w, float screen_h)
{
	Init(screen_w, screen_h);
}

VertexAndIndex* PostFXShaderManager::GetVertexAndIndex()
{
	return &vertex_and_index;
}

PostFXShaderManager::~PostFXShaderManager()
{
	{ std::map<std::string, ShaderSets>().swap(shader_sets); }
}

void PostFXShaderManager::Init(float screen_w, float screen_h)
{
	vertex_and_index.vertex[0].pos = VGet(0.0f, screen_h, 0.0f);
	vertex_and_index.vertex[0].norm = VGet(0.0f, 0.0f, -1.0f);
	vertex_and_index.vertex[0].dif = GetColorU8(0, 0, 0, 255);
	vertex_and_index.vertex[0].spc = GetColorU8(0, 0, 0, 0);
	vertex_and_index.vertex[0].u = 0.0f;
	vertex_and_index.vertex[0].v = 0.0f;
	vertex_and_index.vertex[0].su = 0.0f;
	vertex_and_index.vertex[0].sv = 0.0f;

	vertex_and_index.vertex[1].pos = VGet(screen_w, screen_h, 0.0f);
	vertex_and_index.vertex[1].norm = VGet(0.0f, 0.0f, -1.0f);
	vertex_and_index.vertex[1].dif = GetColorU8(0, 0, 0, 255);
	vertex_and_index.vertex[1].spc = GetColorU8(0, 0, 0, 0);
	vertex_and_index.vertex[1].u = 1.0f;
	vertex_and_index.vertex[1].v = 0.0f;
	vertex_and_index.vertex[1].su = 0.0f;
	vertex_and_index.vertex[1].sv = 0.0f;

	vertex_and_index.vertex[2].pos = VGet(0.0f, 0.0f, 0.0f);
	vertex_and_index.vertex[2].norm = VGet(0.0f, 0.0f, -1.0f);
	vertex_and_index.vertex[2].dif = GetColorU8(0, 0, 0, 255);
	vertex_and_index.vertex[2].spc = GetColorU8(0, 0, 0, 0);
	vertex_and_index.vertex[2].u = 0.0f;
	vertex_and_index.vertex[2].v = 1.0f;
	vertex_and_index.vertex[2].su = 0.0f;
	vertex_and_index.vertex[2].sv = 0.0f;

	vertex_and_index.vertex[3].pos = VGet(screen_w, 0.0f, 0.0f);
	vertex_and_index.vertex[3].norm = VGet(0.0f, 0.0f, -1.0f);
	vertex_and_index.vertex[3].dif = GetColorU8(0, 0, 0, 255);
	vertex_and_index.vertex[3].spc = GetColorU8(0, 0, 0, 0);
	vertex_and_index.vertex[3].u = 1.0f;
	vertex_and_index.vertex[3].v = 1.0f;
	vertex_and_index.vertex[3].su = 0.0f;
	vertex_and_index.vertex[3].sv = 0.0f;

	// ２ポリゴン分の頂点番号配列をセットアップ
	vertex_and_index.index[0] = 0;
	vertex_and_index.index[1] = 1;
	vertex_and_index.index[2] = 2;
	vertex_and_index.index[3] = 2;
	vertex_and_index.index[4] = 1;
	vertex_and_index.index[5] = 3;
}

BasePostFXShaderObject* PostFXShaderManager::GetPostFXShaderObject(const std::string name)
{
	auto it = shader_sets.find(name);
	return it->second.post_fx.get();
}

PassScreenHandles* PostFXShaderManager::GetPassScreenHandles(const std::string name)
{
	auto it = shader_sets.find(name);
	return &it->second.pass_screen_hnds;
}

ShaderHandles* PostFXShaderManager::GetShaderHandles()
{
	return &shader_handles;
}

void PostFXShaderManager::CreateShaderSets(
	std::string naming,
	BasePostFXShaderObject* post_fx_object_ptr
)
{
	{
		auto it = shader_sets.find(naming);
		if (it != shader_sets.end()) return;
	}

	shader_sets.insert(std::make_pair(naming, ShaderSets(post_fx_object_ptr)));
	auto it = shader_sets.find(naming);

	PassScreenParams* pass_screen_params_tmp = it->second.post_fx.get()->GetPassScreenParams();
	int w = static_cast<int>(pass_screen_params_tmp->scr_w);
	int h = static_cast<int>(pass_screen_params_tmp->scr_h);
	int div = static_cast<int>(pass_screen_params_tmp->div_num);
	for (int i = 0; i < pass_screen_params_tmp->downsample_buf_num_max; i++)
	{
		for (int pass_num = 0; pass_num < pass_screen_params_tmp->pass_num_max; pass_num++)
		{
			it->second.pass_screen_hnds.MakeScreenHandle(
				w, h,
				div
			);
		}
		w /= 2.f;
		h /= 2.f;
	}
}
