
struct VS_OUTPUT
{
    float4 Position     : SV_POSITION;  // 座標( プロジェクション空間 )
    float4 Diffuse      : COLOR0;       // ディフューズカラー
    float2 TexCoords0   : TEXCOORD0;    // テクスチャ座標０
    float2 TexCoords1   : TEXCOORD1;    // テクスチャ座標１
};

Texture2D       color_buffer;
SamplerState    color_sampler;

cbuffer CbBlur : register(b0)
{
    float4  color_sub_rate      : packoffset(c0);
    float   luminance_threshold : packoffset(c1);
    float   luminance_multipley : packoffset(c1.y);
};

#define IS_USE_NTSC_COEFFICIENT 0
#if     IS_USE_NTSC_COEFFICIENT
// NTSC係数
// https://nogson2.hatenablog.com/entry/2017/11/25/123959
static const float scale_red   = 0.298912f;
static const float scale_green = 0.586611f;
static const float scale_blue  = 0.114478f;

#else
// sRGB係数
// https://yuseinishiyama.com/posts/2013/07/01/calculate-luminance/
static const float scale_red    = 0.2126f;
static const float scale_green  = 0.7152f;
static const float scale_blue   = 0.0722f;
#endif

static const float3 monochrome_scale = float3(scale_red, scale_green, scale_blue);

float GetLuminance(const float3 _color)
{
    return dot(_color.rgb, monochrome_scale.rgb);
}

float4 main(const VS_OUTPUT _input) : SV_TARGET0
{
    float4 src = color_buffer.Sample(color_sampler, _input.TexCoords0.xy, 0.0f).rgba + color_sub_rate.rgba;
    float  luminance = GetLuminance(src.rgb);

    return float4(src.rgb * max(0.0f, (luminance - luminance_threshold)) * luminance_multipley, 1.0f);
}
