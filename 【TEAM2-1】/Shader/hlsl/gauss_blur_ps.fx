
struct VSOutput
{
    float4 Position : SV_POSITION;
    float2 TexCoord : TEXCOORD;
};

struct VS_OUTPUT
{
    float4 Position     : SV_POSITION; // 座標( プロジェクション空間 )
    float4 Diffuse      : COLOR0;      // ディフューズカラー
    float2 TexCoords0   : TEXCOORD0; // テクスチャ座標０
    float2 TexCoords1   : TEXCOORD1; // テクスチャ座標１
};

cbuffer CbBlur : register(b0)
{
    int SampleCount     : packoffset(c0);
    float4 Offset[32]   : packoffset(c1);
};

Texture2D ColorBuffer : register(t0);
SamplerState ColorSampler : register(s0);

float4 main(const VS_OUTPUT input) : SV_TARGET0
{
    float4 result = 0;

    for (int i = 0; i < SampleCount; i++)
    {
        result += Offset[i].z * ColorBuffer.Sample(ColorSampler, input.TexCoords0 + Offset[i].xy);
    }

    result.w = 1.0f;

    return result;
}
