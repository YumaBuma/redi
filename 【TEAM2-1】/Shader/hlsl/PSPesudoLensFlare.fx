
struct VSOutput
{
    float4 Position : SV_POSITION;
    float2 TexCoord : TEXCOORD;
};

struct VS_OUTPUT
{
    float4 Position		: SV_POSITION;	// 座標( プロジェクション空間 )
    float4 Diffuse		: COLOR0;		// ディフューズカラー
    float2 TexCoords0	: TEXCOORD0;	// テクスチャ座標０
    float2 TexCoords1	: TEXCOORD1;	// テクスチャ座標１
};

cbuffer CbBlur : register(b0)
{
    float   lens_flare_threshold        : packoffset(c0.x);
    float   lens_flare_ghost_dispersal  : packoffset(c0.y);
    int     number_of_ghosts            : packoffset(c0.z);
    float   lens_flare_intensity        : packoffset(c0.w);
};

Texture2D colour_map : register(t0);
//Texture2D gradient_map : register(t1);
//Texture2D noise_map : register(t2);

SamplerState point_sampler_state : register(s0);

float3 TextureDistorted
(
	Texture2D _tex2d,
	float2 _texcoord,
	float2 _direction, // direction of distortion
	float3 _distortion // per-channel distortion factor
) 
{
	// _distortion によって各色要素をずらしてサンプルする
	return float3
	(
		_tex2d.Sample(point_sampler_state, _texcoord + _direction * _distortion.r).r,
		_tex2d.Sample(point_sampler_state, _texcoord + _direction * _distortion.g).g,
		_tex2d.Sample(point_sampler_state, _texcoord + _direction * _distortion.b).b
	);
}

//https://john-chapman.github.io/2017/11/05/pseudo-lens-flare.html
float4 main(const VS_OUTPUT input) : SV_TARGET
{
	// texcoordを反転(0,0)->(1,1)
    float2 mimic_texcoord = -input.TexCoords0 + float2(1, 1);

    uint mip_level = 0, width, height, number_of_levels;
    colour_map.GetDimensions(mip_level, width, height, number_of_levels);
    float2 texel_size = 1.0 / float2(width, height);

	//ghost vector to image centre
    float2 ghost_vector = float2(0.5 - mimic_texcoord.x, 0.5 - mimic_texcoord.y) * lens_flare_ghost_dispersal;

	// sample ghosts;
	float3 fragment_colour = 0;

	// 色収差用パラメーター
	float udittortion	= 18.f;
	float3 distortion	= float3(-texel_size.x * udittortion, 0.0f, texel_size.x * udittortion);
	float2 direction	= normalize(ghost_vector);

    for (int i = 0; i < number_of_ghosts; ++i)
    {
		// sampler stateはmirrorに
        float2 offset = mimic_texcoord + ghost_vector * i;

		float	edge_weight = length(float2(0.5f, 0.5f) - offset) / length(float2(0.5f, 0.5f));
				edge_weight = pow(1.0f - edge_weight, 1.5f);

        float3 colour = colour_map.Sample(point_sampler_state, offset).rgb * edge_weight;
		//float3 colour = TextureDistorted(colour_map, offset, ghost_vector, distortion) * edge_weight;

        colour = max(colour - lens_flare_threshold, 0);

		fragment_colour += colour;
    }

		// sample halo:
		// スクリーン中心-ゴースト位置ベクトルを正規化して*0.5したもの
		float2 halo_vector = normalize(ghost_vector) * 0.5f;
		// texcoordがhalo_vectorの作る楕円の外周の辺りあたる場合にスクリーンの中心辺りがサンプルされる

		// 例えばゴースト位置が0.5の場合halo_weight自体は0になる
		// (0.48,0.48)の場合は			
		// length(float2(0.5f, 0.5f) - frac((0.48,0.48) + (0.5,0.5))=0.95 )=0.55 /
		// length(float2(0.5f, 0.5f))=0.7
		// = 0.7 (あくまでも目安)
		// (1 - 0.7)^5 = 0.002
		//	(1,1)の場合は			
		//	length(float2(0.5f, 0.5f) - frac((1,1) + (-0.5,-0.5))=0.5 )=0.0 /
		//	length(float2(0.5f, 0.5f))=0.7
		//	= 0 (あくまでも目安)
		//	(1 - 0)^5 = 1
		float halo_weight = 
			length(float2(0.5f, 0.5f) - frac(mimic_texcoord + halo_vector)) /
			length(float2(0.5f, 0.5f));
		halo_weight		= pow(1.0f - halo_weight, 5.0f);
		fragment_colour += TextureDistorted
		(
			colour_map,
			mimic_texcoord + halo_vector,
			halo_vector,
			distortion
		) * halo_weight;

		// sample halo:
		halo_vector	= normalize(ghost_vector) * 0.125f;
		halo_weight	= 
			length(float2(0.5f, 0.5f) - frac(mimic_texcoord + halo_vector)) /
			length(float2(0.5f, 0.5f));
		halo_weight	= pow(1.0f - halo_weight, 5.0f);
		fragment_colour += TextureDistorted
		(
			colour_map,
			mimic_texcoord + halo_vector,
			halo_vector,
			distortion
		) * halo_weight;

		//fragment_colour += TextureDistorted(colour_map, input.TexCoords0, direction, distortion);

    // float luminance = dot(fragment_colour.xyz, float3(0.2126, 0.7152, 0.0722));
    // fragment_colour = luminance;

    //fragment_colour *= gradient_map.Sample(anisotropic_sampler_state, length(float2(0.5, 0.5) - mimic_texcoord) / length(float2(0.5, 0.5))).rgb;
	//fragment_colour *= gradient_map.Sample(anisotropic_sampler_state, texcoord);

    //fragment_colour *= noise_map.Sample(anisotropic_sampler_state, texcoord).r;
	//fragment_colour *= smoothstep(0.2, 0.65, noise_map.Sample(anisotropic_sampler_state, texcoord).r);
	
    return float4(fragment_colour * lens_flare_intensity, 1.0);
}