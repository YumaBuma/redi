#include "PostFx.h"
#include "..\\Camera.h"
#include "..\\Function.h"
#include "..\\src\\imgui.h"
#include "..\\Texture.h"
#include "..\\CerealLoadAndSave.h"
#include "..\\FunctionImGui.h"

static const bool IS_LOAD_CBUF_FILE = 1;

void PostFx::Init(const int& _current_scene)
{
	if (IS_LOAD_CBUF_FILE)
		LoadDataFromFile(".\\Data\\Configurations\\PostFx\\post_fx_" + std::to_string(_current_scene) + ".bin", eArchiveTypes::BINARY);
		//LoadDataFromFile(".\\Data\\Configurations\\PostFx\\post_fx.bin", eArchiveTypes::BINARY);

	current_scene = _current_scene;
	is_enable_post_fx = true;
}

void PostFx::UnInit()
{
}

void PostFx::Update()
{
	post_fx_shader_manager.GetPostFXShaderObject(luminance_key_shader_name)->Update();
	//SetDrawScreen
	//(
	//	post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name)->GetPassScreens()->
	//	at
	//	(
	//		pass_screen_params_multiple_gauss_gblur.downsample_buf_num_max *
	//		pass_screen_params_multiple_gauss_gblur.pass_num_max
	//	)
	//);
	//ClearDrawScreen();
}

void PostFx::Render(int* _srv)
{
	if (is_enable_post_fx == false)return;
	//luminance_key
	post_fx_shader_manager.GetPostFXShaderObject(luminance_key_shader_name)
		->Render
		(
			post_fx_shader_manager.GetVertexAndIndex(),
			post_fx_shader_manager.GetShaderHandles(),
			post_fx_shader_manager.GetPassScreenHandles(luminance_key_shader_name),
			_srv,
			&option_params.luminance_key
		);
	//gauss_blur
	post_fx_shader_manager.GetPostFXShaderObject(gauss_blur_shader_name)
		->Render
		(
			post_fx_shader_manager.GetVertexAndIndex(),
			post_fx_shader_manager.GetShaderHandles(),
			post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name),
			&post_fx_shader_manager.GetPassScreenHandles(luminance_key_shader_name)->GetPassScreens()->at(0),
			&option_params.multiple_gauss_blur
		);
	//lens_ghost
	post_fx_shader_manager.GetPostFXShaderObject(lens_ghosts_shader_name)->Render(
		post_fx_shader_manager.GetVertexAndIndex(),
		post_fx_shader_manager.GetShaderHandles(),
		post_fx_shader_manager.GetPassScreenHandles(lens_ghosts_shader_name),
		&post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name)->GetPassScreens()->at(pass_screen_params_multiple_gauss_gblur.downsample_buf_num_max *
			pass_screen_params_multiple_gauss_gblur.pass_num_max),
		&option_params.lens_ghosts
	);

	// composite
	{
		SetDrawScreen(composite_screen);
		ClearDrawScreen();
		{
			SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
			DrawGraph
			(
				0, 0,
				post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name)->GetPassScreens()
				->at
				(
					pass_screen_params_multiple_gauss_gblur.downsample_buf_num_max *
					pass_screen_params_multiple_gauss_gblur.pass_num_max
				),
				true
			);

			SetDrawMode(DX_DRAWMODE_ANISOTROPIC);
			DrawExtendGraph
			(
				0, 0,
				pass_screen_params_lens_ghosts.scr_w, pass_screen_params_lens_ghosts.scr_h,
				post_fx_shader_manager.GetPassScreenHandles
				(
					lens_ghosts_shader_name
				)
				->GetPassScreens()->at(0),
				true
			);
		}
	}
	SetDrawScreen(*_srv);
	SetDrawMode(DX_DRAWMODE_NEAREST);
	
}

void PostFx::Draw()
{
	if (is_enable_post_fx == false)return;

	SetDrawBlendMode(DX_BLENDMODE_ADD, 255);
	DrawGraph(0, 0, composite_screen, 1);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 255);
}

void PostFx::LoadDataFromFile(const std::string& _path, eArchiveTypes _file_type)
{
	OptionParams opt_params_tmp = {};
	std::ifstream
		ifs;
		ifs.open(_path, std::ios::binary);// https://programming-place.net/ppp/contents/cpp/library/028.html
	if (!ifs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, opt_params_tmp));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI: // 中身は json形式だがエクスプローラーでプレビュー出来るので便利
	{
		cereal::JSONInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, opt_params_tmp));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLInputArchive
			i_archive(ifs);
			i_archive(cereal::make_nvp(_path, opt_params_tmp));
		break;
	}
	default:
		break;
	}

	option_params = opt_params_tmp;
}

void PostFx::SaveDataToFile(const std::string& _path, eArchiveTypes _file_type)
{
	std::ofstream
		ofs;
		ofs.open(_path, std::ios::binary);
	if (!ofs)return; // 読み込み失敗

	switch (_file_type)
	{
	case eArchiveTypes::BINARY:
	{
		cereal::BinaryOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, option_params));
		break;
	}
	case eArchiveTypes::JSON:
	case eArchiveTypes::INI:
	{
		cereal::JSONOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, option_params));
		break;
	}
	case eArchiveTypes::XML:
	{
		cereal::XMLOutputArchive
			o_archive(ofs);
			o_archive(cereal::make_nvp(_path, option_params));
		break;
	}
	default:
		break;
	}
}

void PostFx::ImGui()
{
	if (Iwin_flg.postfx == false) return;

	ImGui::Begin("PostFx", nullptr, ImGuiWindowFlags_AlwaysHorizontalScrollbar);
	{
		int			ghnd	= 0;
		int			tex_x	= 0; 
		int			tex_y	= 0;
		ImGuiIO&	io		= ImGui::GetIO();
		ImTextureID imtexid = nullptr;
		ImVec2		size	= { 0.f,0.f };

		ImGui::Checkbox("is_enable_post_fx", &is_enable_post_fx);
		ImGui::SameLine();
		if (ImGui::Button("Reload all texture"))
			pTexture->ReloadAllGraphHandles();

		ImGui::NewLine();
		if (ImGui::MyImGui::ComboMenu("scenes", &current_scene, set_scene_names, IM_ARRAYSIZE(set_scene_names)))
		{
			LoadDataFromFile(".\\Data\\Configurations\\PostFx\\post_fx_" + std::to_string(current_scene) + ".bin", eArchiveTypes::BINARY);
		}

		if (ImGui::Button(("Save to " + set_scene_names[current_scene]).c_str()))
			SaveDataToFile(".\\Data\\Configurations\\PostFx\\post_fx_" + std::to_string(current_scene) + ".bin", eArchiveTypes::BINARY);
		ImGui::SameLine();
		if (ImGui::Button("Load"))
			LoadDataFromFile(".\\Data\\Configurations\\PostFx\\post_fx_" + std::to_string(current_scene) + ".bin", eArchiveTypes::BINARY);

		if (ImGui::BeginTabBar("##PostFxtabbar", ImGuiTabBarFlags_FittingPolicyDefault_))
		{

#pragma region LuminanceKey
			if (ImGui::BeginTabItem("Luminance Key", nullptr, ImGuiTabItemFlags_None))
			{
				// params
				{

					ImGui::Text("scr_w: %.0f, scr_h: %.0f", option_params.luminance_key.scr_w, option_params.luminance_key.scr_h);
					float* p_color_sub_rate[4] =
					{
						&option_params.luminance_key.cbuf_params.color_sub_rate.x,
						&option_params.luminance_key.cbuf_params.color_sub_rate.y,
						&option_params.luminance_key.cbuf_params.color_sub_rate.z,
						&option_params.luminance_key.cbuf_params.color_sub_rate.w
					};

					ImGui::DragFloat4("cbuf_params.color_sub_rate", *p_color_sub_rate, 0.01f, -1.f, 0.f);
					if (ImGui::DragFloat("cbuf_params.u_bias_RGB", &option_params.luminance_key.cbuf_params.color_sub_rate.x, 0.01f, -1.f, 0.f))
					{
						option_params.luminance_key.cbuf_params.color_sub_rate.y = option_params.luminance_key.cbuf_params.color_sub_rate.x;
						option_params.luminance_key.cbuf_params.color_sub_rate.z = option_params.luminance_key.cbuf_params.color_sub_rate.x;
					}
					ImGui::DragFloat("cbuf_params.luminance_threshold", &option_params.luminance_key.cbuf_params.luminance_threshold, 0.01f, 0.f, 64.f);
					ImGui::DragFloat("cbuf_params.luminance_multiply", &option_params.luminance_key.cbuf_params.luminance_multipley, 0.01f, 0.f, 64.f);
				}

				// screen
				{
					ghnd = (this->post_fx_shader_manager.GetPassScreenHandles(luminance_key_shader_name)->GetPassScreens()->at(0));
					imtexid = (void*)(ghnd + 1);

					if (GetGraphSize(ghnd, &tex_x, &tex_y) == 0)
					{
						size =
						{
							(ImGui::GetWindowWidth() - 64.f),
							scasf(tex_y) * GetRatio(scasf(tex_x),ImGui::GetWindowWidth() - 64.f)
						};
					}
					ImGui::Text("Original Size: %dx%d", tex_x, tex_y);
					ImGui::Text("Display Size: %.0fx%.0f", size.x, size.y);
					ImGui::Image
					(
						imtexid,
						size,
						ImVec2(0, 0), ImVec2(1, 1),
						ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 255)
					);

					ImGui::EndTabItem();
				}
			}

#pragma endregion

#pragma region LensGhosts
			if (ImGui::BeginTabItem("Lens Ghosts", nullptr, ImGuiTabItemFlags_None))
			{
				// params
				{
					option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_ghost_dispersal;
					option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_intensity;
					option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_threshold;
					option_params.lens_ghosts.cbuf_lens_ghosts_b0.number_of_ghosts;

					ImGui::DragFloat("lens_flare_ghost_dispersal", &option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_ghost_dispersal, 0.01f, -1.f, 1.f);
					ImGui::DragFloat("lens_flare_intensity", &option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_intensity, 0.01f, -1.f, 1.f);
					ImGui::DragFloat("lens_flare_threshold", &option_params.lens_ghosts.cbuf_lens_ghosts_b0.lens_flare_threshold, 0.01f, 0.f, 64.f);
					ImGui::InputInt("numbre_of_ghosts", &option_params.lens_ghosts.cbuf_lens_ghosts_b0.number_of_ghosts, 1, 2);
				}

				// screen
				{
					int ghnd = (this->post_fx_shader_manager.GetPassScreenHandles(lens_ghosts_shader_name)->GetPassScreens()->at(0));
					ImTextureID imtexid = (void*)(ghnd + 1);
					ImVec2		size = { 0.f,0.f };
					int			tex_x = 0, tex_y = 0;

					if (GetGraphSize(ghnd, &tex_x, &tex_y) == 0)
					{
						size =
						{
							(ImGui::GetWindowWidth() - 64.f),
							scasf(tex_y) * GetRatio(scasf(tex_x),ImGui::GetWindowWidth() - 64.f)
						};
					}
					ImGui::Text("Original Size: %dx%d", tex_x, tex_y);
					ImGui::Text("Display Size: %.0fx%.0f", size.x, size.y);
					ImGui::Image
					(
						imtexid,
						size,
						ImVec2(0, 0), ImVec2(1, 1),
						ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 255)
					);
				}

				ImGui::EndTabItem();
			}
#pragma endregion

#pragma region GaussianBlur
			if (ImGui::BeginTabItem("Gaussian Blur", nullptr, ImGuiTabItemFlags_None))
			{
				// params
				{
					ImGui::Text("scr_w: %.0f, scr_h: %.0f", option_params.luminance_key.scr_w, option_params.luminance_key.scr_h);
					float* p_blur_vector[2] =
					{
						&option_params.multiple_gauss_blur.blur_vector.x,
						&option_params.multiple_gauss_blur.blur_vector.y
					};
					ImGui::SliderFloat2("blur_vector", *p_blur_vector, -1.f, 1.f);
					ImGui::DragFloat("deviation", &option_params.multiple_gauss_blur.deviation, 0.1f, 0.f, 64.f);
					ImGui::DragFloat("multiply", &option_params.multiple_gauss_blur.multiply, 0.1f, 0.f, 64.f);
					ImGui::DragInt("sample_count", &option_params.multiple_gauss_blur.sample_count, 0.1f, 1, GAUSSIAN_BLUR_SAMPLE_MAX);
				}

				// screen
				{
					int mgf_max = this->post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name)->GetPassScreens()->size();
					std::vector<int>* mgf_scr_tmp = this->post_fx_shader_manager.GetPassScreenHandles(gauss_blur_shader_name)->GetPassScreens();

					for (int mgf_i = 0; mgf_i < mgf_max; mgf_i++)
					{
						ghnd = (mgf_scr_tmp->at(mgf_i));
						imtexid = (void*)(ghnd + 1);

						if (GetGraphSize(ghnd, &tex_x, &tex_y) == 0)
						{
							size =
							{
								(ImGui::GetWindowWidth() - 64.f),
								scasf(tex_y) * GetRatio(scasf(tex_x),ImGui::GetWindowWidth() - 64.f)
							};
						}
						ImGui::Text("Original Size: %dx%d", tex_x, tex_y);
						ImGui::Text("Display Size: %.0fx%.0f", size.x, size.y);
						ImGui::Image
						(
							imtexid,
							size,
							ImVec2(0, 0), ImVec2(1, 1)
							, ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 255)
						);
					}
				}
				ImGui::EndTabItem();
			}

#pragma endregion

			ImGui::EndTabBar();
		}
	}
	ImGui::End();
}


const std::string PostFx::set_scene_names[4] =
{
	"TITLE",
	"GAME",
	"OVER",
	"CLEAR"
};
