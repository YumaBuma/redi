#pragma once


#include "..\\CerealLoadAndSave.h"

#include <memory>
#include <vector>
#include <map>
#include <string>

#include <cmath>

#include <d3d11.h>
#include "..\\SimpleMath.h"

#include "../FunctionDxLib.h"

using namespace DirectX::SimpleMath;

#define GAUSSIAN_BLUR_SAMPLE_MAX 32

struct OptionParamsMultipleGaussBlur
{
	Vector2 blur_vector;
	float deviation;
	float multiply;
	int sample_count;
	
	template< class Archive >
	void serialize(Archive& _ar, const uint32_t _version)
	{
		_ar
		(
			CEREAL_NVP(blur_vector.x),
			CEREAL_NVP(blur_vector.y),
			CEREAL_NVP(deviation),
			CEREAL_NVP(multiply),
			CEREAL_NVP(sample_count)
		);
	}
};
CEREAL_CLASS_VERSION(OptionParamsMultipleGaussBlur, 1);

struct PassScreenParams
{
	float scr_w;
	float scr_h;
	float div_num;

	int downsample_buf_num_max;
	int pass_num_max;
};


struct GaussBlurParam
{
	//http://gameproject.jp/20160814-02/
	//GPU側の定数レジスタを表しています。名称は仮想的なものです。ひとつのレジスタのサイズは4バイト×4で16バイトの境界で区切られます。
	int		sample_count;	//packoffset(c0).x	
	int		dummy[3];		//packoffset(c0).yzw
	Vector4	offset[GAUSSIAN_BLUR_SAMPLE_MAX];//packoffset(c1)...
};

//ガウスの重みを計算します.
inline float GaussianDistribution(const Vector2 pos, float rho)
{
	return exp(-(pos.x * pos.x + pos.y * pos.y) / (2.0f * rho * rho));
}

//ブラーパラメータを計算します.
inline GaussBlurParam CalcBlurParam(int width, int height, Vector2 dir, float deviation, float multiply, int sample_count)
{
	GaussBlurParam result;
	result.sample_count = sample_count - 1;/*max32*///ps描画で何回1ピクセルをサンプルするか(1ピクセルに費す負荷みたいなもの)
	int sample_count_div2 = sample_count / 2;
	auto tu = 1.0f / float(width);//テクスチャ座標x
	auto tv = 1.0f / float(height);//テクスチャ座標y

	/*
	15回分
	-7,-6,-5,-4,-3,-2,-1,0,+1,+2,+3,+4,+5,+6,+7

	offset[16]配列での並び
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,	15
	0,+1,+2,+3,+4,+5,+6,+7,-1,-2,-3,-4,-5,-6,-7,未使用
	*/
	//まず中心(0)、かつ正の方向への計算 (offset[0])
	result.offset[0].z = GaussianDistribution(Vector2(0.0f, 0.0f)/*中心なのでベクトル0*/, deviation) * multiply;
	auto total_weight = result.offset[0].z;

	result.offset[0].x = 0.0f;
	result.offset[0].y = 0.0f;

	for (int i = 1; i < sample_count_div2; i++)
	{
		result.offset[i].x = dir.x * i * tu;
		result.offset[i].y = dir.y * i * tv;
		result.offset[i].z = GaussianDistribution(dir * float(i), deviation) * multiply;
		total_weight += result.offset[i].z * 2.0f;//規格化定数 ( 負の方向への分のために*2.0fしている )
	}
	//https://mathtrain.jp/stdev
	//規格化
	for (int i = 0; i < sample_count_div2; i++)
	{
		result.offset[i].z /= total_weight;
	}

	//負の方向へのブラーを掛けるため
	for (int i = sample_count_div2; i < result.sample_count; i++)
	{
		result.offset[i].x = -result.offset[i - (sample_count_div2 - 1)].x;
		result.offset[i].y = -result.offset[i - (sample_count_div2 - 1)].y;
		result.offset[i].z = result.offset[i - (sample_count_div2 - 1)].z;
	}

	return result;
}

class PassScreenHandles
{
public:
	PassScreenHandles();
	~PassScreenHandles();

	int MakeScreenHandle(
		int screen_w, int screen_h, int div_num,
		bool can_vaild_graph = true,
		bool can_float_graph = true,
		bool can_msaa = false,
		int msaa_samples = 16, int msaa_quality = 8
	);
	int DeleteLastScreenHandle();
	std::vector<int>* GetPassScreens();

private:
	std::vector<int> pass_screens;
};

class ShaderHandles
{
public:
	ShaderHandles();
	~ShaderHandles();

	const int LoadVS(const char* path, std::string naming);
	const int LoadPS(const char* path, std::string naming);
	int* GetVS(std::string name);
	int* GetPS(std::string name);

private:
	std::map<std::string, int> vs_handle;
	std::map<std::string, int> ps_handle;
};

struct VertexAndIndex
{
	//float screen_w, screen_h;
	VERTEX3DSHADER vertex[4];
	unsigned short index[6];
};

class BasePostFXShaderObject
{
public:
	BasePostFXShaderObject(void* option_params, PassScreenParams* pass_scr_parms);
	//BasePostFXShaderObject(float screen_w, float screen_h);
	virtual ~BasePostFXShaderObject();

	void SetVS(const int& vs);
	void SetPS(const int& ps);
	virtual void SetVSCbufParams() {}
	virtual void SetPSCbufParams() {}

	void SetSamplingTexture(const int register_num, const int& screen_handle);
	virtual void Update() {}
	virtual void Render(
		VertexAndIndex* v_and_i,
		ShaderHandles* shader_handles,
		PassScreenHandles* pass_screens,
		int* sample_textures,
		void* option_params
	) = 0;

	void* GetOptionParams();
	PassScreenParams* GetPassScreenParams();
protected:
	void* option;
	PassScreenParams* pass_screen_params;
	int vs_cbuf_handle;
	int ps_cbuf_handle;
};

class GaussBlurShaderObject :public BasePostFXShaderObject
{
public:
	GaussBlurShaderObject(void* option_params, PassScreenParams* pass_screen_params);
	virtual ~GaussBlurShaderObject();

	virtual void Render(
		VertexAndIndex* v_and_i,
		ShaderHandles* shader_handles,
		PassScreenHandles* pass_screens,
		int* sample_textures,
		void* option_params
	);
private:
	GaussBlurParam* src;
	OptionParamsMultipleGaussBlur* option_gauss_blur;
};

class AnamorphicFlareShaderObject :public BasePostFXShaderObject
{
public:
	AnamorphicFlareShaderObject(void* option_params, PassScreenParams* pass_screen_params);
	virtual ~AnamorphicFlareShaderObject();

	virtual void Render(
		VertexAndIndex* v_and_i,
		ShaderHandles* shader_handles,
		PassScreenHandles* pass_screens,
		int* sample_textures,
		void* option_params
	);
private:
	GaussBlurParam* src;
	OptionParamsMultipleGaussBlur* option_gauss_blur;
};

struct CbufLuminanceKey
{
	Vector4 color_sub_rate;
	float   luminance_threshold;
	float   luminance_multipley;
	float	dummy[2];
};
struct OptionParamsLuminanceKey
{
	float scr_w;
	float scr_h;
	CbufLuminanceKey cbuf_params;

	template< class Archive >
	void serialize(Archive& _ar, const uint32_t _version)
	{
		_ar
		(
			CEREAL_NVP(scr_w),
			CEREAL_NVP(scr_h),
			CEREAL_NVP(cbuf_params.color_sub_rate.x),
			CEREAL_NVP(cbuf_params.color_sub_rate.y),
			CEREAL_NVP(cbuf_params.luminance_multipley),
			CEREAL_NVP(cbuf_params.luminance_threshold),
			CEREAL_NVP(cbuf_params.dummy[0]),
			CEREAL_NVP(cbuf_params.dummy[1])
		);
	}
};
CEREAL_CLASS_VERSION(OptionParamsLuminanceKey, 1);

class LuminanceKeyShaderObject :public BasePostFXShaderObject
{
public:
	LuminanceKeyShaderObject(void* option_params, PassScreenParams* pass_screen_params);
	virtual ~LuminanceKeyShaderObject();

	virtual void Update();
	virtual void Render(
		VertexAndIndex* v_and_r,
		ShaderHandles* shader_handles,
		PassScreenHandles* pass_screens,
		int* sample_textures,
		void* option_params
	);

	virtual void SetPSCbufParams(CbufLuminanceKey cbuf_params);

private:
	//CbufLuminanceKey* cbuf_luminance_key;
	OptionParamsLuminanceKey* option_luminance_key;
};

struct CbufLensGhosts_b0
{
	//Vector2 texture_size;
	//float ghost_dispersal; // dispersion factor
	//int ghosts_iterate_num; // number of ghost samples
	float   lens_flare_threshold;
	float   lens_flare_ghost_dispersal;
	int     number_of_ghosts;
	float   lens_flare_intensity;
};
struct CbufLensGhosts_b1
{
	float halo_weight;
	float dummy[3];
};
struct OptionParamsLensGhost
{
	float scr_w;
	float scr_h;

	CbufLensGhosts_b0 cbuf_lens_ghosts_b0;
	CbufLensGhosts_b1 cbuf_lens_ghosts_b1;

	template< class Archive >
	void serialize(Archive& _ar, const uint32_t _version)
	{
		_ar
		(
			CEREAL_NVP(scr_w),
			CEREAL_NVP(scr_h),
			CEREAL_NVP(cbuf_lens_ghosts_b0.lens_flare_ghost_dispersal),
			CEREAL_NVP(cbuf_lens_ghosts_b0.lens_flare_intensity),
			CEREAL_NVP(cbuf_lens_ghosts_b0.lens_flare_threshold),
			CEREAL_NVP(cbuf_lens_ghosts_b0.number_of_ghosts),
			CEREAL_NVP(cbuf_lens_ghosts_b1.halo_weight),
			CEREAL_NVP(cbuf_lens_ghosts_b1.dummy[0]),
			CEREAL_NVP(cbuf_lens_ghosts_b1.dummy[1]),
			CEREAL_NVP(cbuf_lens_ghosts_b1.dummy[2])
		);
	}
};
CEREAL_CLASS_VERSION(OptionParamsLensGhost, 1);

class LensGhostsShaderObject :public BasePostFXShaderObject
{
public:
	LensGhostsShaderObject(void* option_params, PassScreenParams* pass_screen_params);
	~LensGhostsShaderObject();

	virtual void Render(
		VertexAndIndex* v_and_r,
		ShaderHandles* shader_handles,
		PassScreenHandles* pass_screens,
		int* sample_textures,
		void* option_params
	);

private:
	OptionParamsLensGhost* option_lens_ghosts;
	int ps_cbuf_handle_b1;
};

class ShaderSets
{
public:
	ShaderSets(BasePostFXShaderObject* post_fx_object_ptr) :post_fx(std::move(post_fx_object_ptr)) {}
	ShaderSets() {}
	//~ShaderSets(){}
	PassScreenHandles pass_screen_hnds;
	std::unique_ptr<BasePostFXShaderObject> post_fx;
};

class PostFXShaderManager
{
public:
	PostFXShaderManager() {};
	PostFXShaderManager(float screen_w, float screen_h);
	~PostFXShaderManager();
	void Init(float screen_w, float screen_h);

	void CreateShaderSets(
		std::string naming,
		BasePostFXShaderObject* post_fx_object_ptr
	);
	VertexAndIndex* GetVertexAndIndex();
	BasePostFXShaderObject* GetPostFXShaderObject(const std::string name);
	PassScreenHandles* GetPassScreenHandles(const std::string name);
	ShaderHandles* GetShaderHandles();

private:
	VertexAndIndex vertex_and_index;
	ShaderHandles shader_handles;
	std::map<std::string, ShaderSets> shader_sets;
};