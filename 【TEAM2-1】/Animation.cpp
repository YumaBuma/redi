#include "Animation.h"
#include <fstream>

//------------------------------------------------
//			Constract
//------------------------------------------------

AnimParameter::AnimParameter(int _max_aF, int _aS, bool _isL)
{
	max_aFrame = _max_aF;
	animSpeed = _aS;
	isLoop = _isL;
}


//------------------------------------------------
//			UpdateAnimation
//------------------------------------------------

void Animation::playAnimation()
{
	if (animParam.empty())return;
	if (isStop)stop_anim = false;
	
	//static char anim_prev = 0; static変数をメンバ関数内で宣言すると全実体で共有されるのでメンバ変数に変更
	auto& a_param = animParam.begin() + now_anim;

	if (now_anim != anim_prev)
	{
		aCnt = 0;
		aFrame = 0;
	}
	anim_prev = now_anim;

	if (stop_anim)return;
	if (a_param->animSpeed == 0)return;
	aCnt++;
	if (aCnt % a_param->animSpeed == 0)
	{
		aCnt = 0;
		aFrame++;
		if (aFrame >= a_param->max_aFrame)
		{
			if (a_param->isLoop)
			{
				aFrame = 0;
			}
			else
			{
				aFrame = a_param->max_aFrame - 1;
			}
		}
	}
}

void Animation::changeAnimation(int _anim_index)
{
	if (isStop)return;

	stop_anim = false;
	if (now_anim == _anim_index)return;
		now_anim = _anim_index;

	if (now_anim <= -1) return;
	if (!animParam[now_anim].isLoop)
	{
		aCnt = 0;
		aFrame = 0;
	}
}

//------------------------------------------------
//			InitilizeAnimation
//------------------------------------------------

void Animation::add(int _max_aF, int _aS, bool _isL)
{
	animParam.emplace_back(_max_aF, _aS, _isL);
}

void Animation::loadAnimFile(string name)
{
	static int max_anim;
	string txt = "./Data/Images/Texts/" + name + ".txt";
	ifstream inf(txt, ios::in);
	if (inf.fail())return;
	inf >> tex_pos.x;
	inf >> tex_pos.y;
	inf >> tex_size.x;
	inf >> tex_size.y;
	inf >> max_anim;
	aFrame = 0;
	now_anim = 0;
	isAnimation = true;

	for (int i = 0; i < max_anim; i++)
	{
		static bool loop;
		static int max_af;
		static int a_spd;

		inf >> loop;
		inf >> max_af;
		//max_af--;//アニメーションを合わせるため
		inf >> a_spd;

		add(max_af, a_spd, loop);
	}
}


//
//
//

void Animation::OutputFile(string _txt)
{

	ofstream outf(_txt, ios::out);
	outf << tex_pos.x << endl;
	outf << tex_pos.y << endl;

	outf << tex_size.x << endl;
	outf << tex_size.y << endl;
	outf << MAX_ANIMATION << endl;
	for (auto& itr : animParam)
	{
		outf << itr.isLoop << endl;
		outf << itr.max_aFrame << endl;
		outf << itr.animSpeed << endl;
	}
}


void Animation::InputFile(string _txt)
{
	ifstream inf(_txt, ios::in);

	inf >> tex_pos.x;
	inf >> tex_pos.y;
	inf >> tex_size.x;
	inf >> tex_size.y;
	inf >> MAX_ANIMATION;

	for (int i = 0; i < MAX_ANIMATION; i++)
	{
		if (animParam.size() < MAX_ANIMATION)
		{
			animParam.emplace_back();
		}
		std::vector<AnimParameter>::iterator itr = animParam.begin() + i;
		inf >> itr->isLoop;
		inf >> itr->max_aFrame;
		inf >> itr->animSpeed;
	}

}

void Animation::animImGui(string _tag)
{
	if (ImGui::Button("ANIMATION_DETAIL"))isStop ^= 1;

	if (!isStop)return;
	stop_anim = false;

		string name;
		name = "TEX_POSITION##" + _tag;
		ImGui::Text(name.c_str());

		name = "tex_posX##" + _tag;
		ImGui::DragFloat(name.c_str(), &(tex_pos.x));
		name = "tex_posY##" + _tag;
		ImGui::DragFloat(name.c_str(), &(tex_pos.y));

		name = "MAX_ANIMATION##" + _tag + " : %d";
		ImGui::Text(name.c_str(), animParam.size());

		name = "add##" + _tag;
		if (ImGui::Button(name.c_str()))
		{
			animParam.emplace_back();
		}

		name = "remove##" + _tag;
		if (ImGui::Button(name.c_str()))
		{
			if (animParam.empty() == false)
			{
				animParam.pop_back();
			}
		}


		name = "ANIMATION##" + _tag;
		ImGui::Text(name.c_str());

		name = "now_ANIM##" + _tag;
		ImGui::InputInt(name.c_str(), &(now_anim));
		int i_prev = 0;
		for (auto& it : animParam)
		{
			if (i_prev != now_anim)
			{
				i_prev++;
				continue;
			}
			else
			{
				i_prev++;
			}
			std::string nodename;
			nodename = std::to_string(i_prev - 1);
			ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
			if (ImGui::TreeNode(nodename.c_str()))
			{
				name = "MAX_FRAME##" + _tag + (char)i_prev;
				ImGui::Text(name.c_str());
				name = "MAX_FRAME_NUM##" + _tag + (char)i_prev;
				if (ImGui::InputInt(name.c_str(), &(it.max_aFrame)))aCnt = 0;

				if (it.max_aFrame <= 0)it.max_aFrame = 1;

				name = "now_FRAME##" + _tag + (char)i_prev;
				ImGui::Text(name.c_str());
				name = "AFRAME_NUM##" + _tag + (char)i_prev;
				ImGui::InputInt(name.c_str(), &(aFrame));
				name = "aFrame=0##" + _tag + (char)i_prev;
				if (ImGui::Button(name.c_str()))
				{
					aCnt = 0;
					aFrame = 0;
				}

				name = "ANIM_SPEED##" + _tag + (char)i_prev;
				ImGui::Text(name.c_str());
				name = "ANIMATION_SPEED##" + _tag + (char)i_prev;
				if (ImGui::InputInt(name.c_str(), &(it.animSpeed)))aCnt = 0;

				if (it.animSpeed <= 0)it.animSpeed = 1;
				name = "LOOP##" + _tag + (char)i_prev;
				ImGui::Text("LOOP##Player");
				name = "isLoop##" + _tag + (char)i_prev;
				ImGui::Checkbox("isLoop##Player", &(it.isLoop));

				ImGui::TreePop();
				break;
			}
		}
		i_prev = 0;
		name = "TEX_SIZE##" + _tag + (char)i_prev;
		ImGui::Text(name.c_str());
		//if (it.isSquare)
		//{
		//	ImGui::InputInt("tex Square", &(it.tex_size.x));
		//	ImGui::InputInt("tex Square", &(it.tex_size.y));
		//	//it.tex_size.y = it.tex_size.x;
		//}
		//else
		{
			name = "texX##" + _tag + (char)i_prev;
			ImGui::InputInt(name.c_str(), &(tex_size.x));
			name = "texY##" + _tag + (char)i_prev;
			ImGui::InputInt(name.c_str(), &(tex_size.y));
		}
		//if (ImGui::Button("isSquare"))isSquare ^= true;
		//if (ImGui::Button("Cutting Line"))debugflg ^= true;

		name = "Save##" + _tag + (char)i_prev;
		if (ImGui::Button(name.c_str()))
		{
			OPENFILENAME ofn;
			char szFile[MAX_PATH] = "";
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.lpstrFilter = TEXT("TXT(*.TXT以外許さん*)\0*.txt\0")
				TEXT("全てのファイル(*.*)\0*.*\0\0");
			ofn.lpstrFile = szFile;
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrDefExt = "txt";
			ofn.Flags = OFN_FILEMUSTEXIST;

			if (GetSaveFileName(&ofn) != 0)
			{
				string txt = szFile;
				OutputFile(txt);
			}
			else
			{
			MessageBox(NULL, TEXT("Kitty on your lap"),
				TEXT("メッセージボックス"), MB_OK);
		}
	}

	name = "Load##" + _tag + (char)i_prev;
	if (ImGui::Button(name.c_str()))
	{
		OPENFILENAME ofn;
		char szFile[MAX_PATH] = "";
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.lpstrFilter = TEXT("TXT(*.TXT以外許さん*)\0*.txt\0")
			TEXT("全てのファイル(*.*)\0*.*\0\0");
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = MAX_PATH;
		ofn.Flags = OFN_FILEMUSTEXIST;

		if (GetOpenFileName(&ofn) != 0)
		{
			string txt = szFile;
			InputFile(txt);
		}
		else
		{
			MessageBox(NULL, TEXT("Kitty on your lap"),
				TEXT("メッセージボックス"), MB_OK);
		}
	}

	//playAnimation();
}

void Animation::loadInfo(Vector2 _texPos, INT2 _texSize,int _texNum)
{
	tex_pos = _texPos;
	tex_size = _texSize;
	textureNum = _texNum;
}

void Animation::addAnimation(bool _loop, int _maxFrame, int _animSpeed)
{
	MAX_ANIMATION++;
	animParam.emplace_back();

	animParam.back().isLoop = _loop;
	animParam.back().max_aFrame = _maxFrame;
	animParam.back().animSpeed = _animSpeed;

}

void Animation::MultianimImGui(string _tag)
{
	static int multi_now_anim = 0;

	if (ImGui::Button("ANIMATION_DETAIL"))isStop ^= 1;

	if (!isStop)return;

	if (ImGui::Button("add"))
	{
		multianimData.emplace_back();
	}

	if (ImGui::Button("remove"))
	{
		multianimData.pop_back();
	}
	stop_anim = false;

	string multi = "multiAnimation##" + _tag +" : %d";
	ImGui::Text(multi.c_str(), multianimData.size());

	multi = "multi_now_Animation##" + _tag;
	ImGui::InputInt(multi.c_str(), &multi_now_anim);
	if (multi_now_anim >= multianimData.size())
	{
		multi_now_anim = multianimData.size() -1;
	}

	for (int i = 0; i < multianimData.size(); i++)
	{
		if (i != multi_now_anim)continue;
		std::string nodename;
		nodename = std::to_string(i);
		ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
		if (ImGui::TreeNode(nodename.c_str()))
		{

			std::vector<Animation>::iterator it = multianimData.begin() + i;
			string name;

			name = "GROUP##" + _tag;
			ImGui::InputInt(name.c_str(), &it->group);

			name = "ID##" + _tag;
			ImGui::InputInt(name.c_str(), &it->id);

			name = "TEX_POSITION##" + _tag;
			ImGui::Text(name.c_str());

			name = "tex_posX##" + _tag;
			ImGui::DragFloat(name.c_str(), &(it->tex_pos.x));
			name = "tex_posY##" + _tag;
			ImGui::DragFloat(name.c_str(), &(it->tex_pos.y));

			name = "MAX_ANIMATION##" + _tag + " : %d";
			ImGui::Text(name.c_str(), it->animParam.size());

			name = "add##" + _tag;
			if (ImGui::Button(name.c_str()))
			{
				it->animParam.emplace_back();
			}

			name = "remove##" + _tag;
			if (ImGui::Button(name.c_str()))
			{
				if (it->animParam.empty() == false)
				{
					it->animParam.pop_back();
				}
			}


			name = "ANIMATION##" + _tag;
			ImGui::Text(name.c_str());

			name = "now_ANIM##" + _tag;
			ImGui::InputInt(name.c_str(), &(it->now_anim));
			if (it->now_anim >= it->animParam.size())
			{
				it->now_anim = it->animParam.size()-1;
			}

			int i_prev = 0;
			for (auto& itr : it->animParam)
			{
				if (i_prev != it->now_anim)
				{
					i_prev++;
					continue;
				}
				else
				{
					i_prev++;
				}
				nodename = std::to_string(i_prev - 1);
				ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
				if (ImGui::TreeNode(nodename.c_str()))
				{
					name = "MAX_FRAME##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "MAX_FRAME_NUM##" + _tag + (char)i_prev;
					if (ImGui::InputInt(name.c_str(), &(itr.max_aFrame)))aCnt = 0;

					if (itr.max_aFrame <= 0)itr.max_aFrame = 1;

					name = "now_FRAME##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "AFRAME_NUM##" + _tag + (char)i_prev;
					ImGui::InputInt(name.c_str(), &(it->aFrame));
					name = "aFrame=0##" + _tag + (char)i_prev;
					if (ImGui::Button(name.c_str()))
					{
						it->aCnt = 0;
						it->aFrame = 0;
					}

					name = "ANIM_SPEED##" + _tag + (char)i_prev;
					ImGui::Text(name.c_str());
					name = "ANIMATION_SPEED##" + _tag + (char)i_prev;
					if (ImGui::InputInt(name.c_str(), &(itr.animSpeed)))aCnt = 0;

					if (itr.animSpeed <= 0)itr.animSpeed = 1;
					name = "LOOP##" + _tag + (char)i_prev;
					ImGui::Text("LOOP##Player");
					name = "isLoop##" + _tag + (char)i_prev;
					ImGui::Checkbox("isLoop##Player", &(itr.isLoop));

					ImGui::TreePop();
					break;
				}
			}
			i_prev = 0;
			name = "TEX_SIZE##" + _tag + (char)i_prev;
			ImGui::Text(name.c_str());
			//if (it.isSquare)
			//{
			//	ImGui::InputInt("tex Square", &(it.tex_size.x));
			//	ImGui::InputInt("tex Square", &(it.tex_size.y));
			//	//it.tex_size.y = it.tex_size.x;
			//}
			//else
			{
				name = "texX##" + _tag + (char)i_prev;
				ImGui::InputInt(name.c_str(), &(it->tex_size.x));
				name = "texY##" + _tag + (char)i_prev;
				ImGui::InputInt(name.c_str(), &(it->tex_size.y));
			}
			//if (ImGui::Button("isSquare"))isSquare ^= true;
			//if (ImGui::Button("Cutting Line"))debugflg ^= true;

			name = "Save##" + _tag + (char)i_prev;
			if (ImGui::Button(name.c_str()))
			{
				OPENFILENAME ofn;
				char szFile[MAX_PATH] = "";
				ZeroMemory(&ofn, sizeof(ofn));
				ofn.lStructSize = sizeof(OPENFILENAME);
				ofn.lpstrFilter = TEXT("TXT(*.TXT以外許さん*)\0*.txt\0")
					TEXT("全てのファイル(*.*)\0*.*\0\0");
				ofn.lpstrFile = szFile;
				ofn.nMaxFile = MAX_PATH;
				ofn.lpstrDefExt = "txt";
				ofn.Flags = OFN_FILEMUSTEXIST;

				if (GetSaveFileName(&ofn) != 0)
				{
					string txt = szFile;
					MultiOutputFile(txt);
				}
				else
				{
					MessageBox(NULL, TEXT("Kitty on your lap"),
						TEXT("メッセージボックス"), MB_OK);
				}
			}

			name = "Load##" + _tag + (char)i_prev;
			if (ImGui::Button(name.c_str()))
			{
				MultiInputFile("", 0, 0);//TODO ここ書き換えて
			}

		}
		//playAnimation();
	}
}

void Animation::MultiOutputFile(string _txt)
{

	ofstream outf(_txt, ios::out);

	for (int i = 0; i < multianimData.size(); i++)
	{
		std::vector<Animation>::iterator it = multianimData.begin() + i;
		outf << it->group << endl;
		outf << it->id << endl;

		outf << it->tex_pos.x << endl;
		outf << it->tex_pos.y << endl;

		outf << it->tex_size.x << endl;
		outf << it->tex_size.y << endl;
		//outf << it->MAX_ANIMATION << endl;
		outf <<  it->animParam.size() << endl;
		for (auto& itr : it->animParam)
		{
			outf << itr.isLoop << endl;
			outf << itr.max_aFrame << endl;
			outf << itr.animSpeed << endl;
		}
	}
}


void Animation::MultiInputFile(string _txt, int _group, int _id)
{
	ifstream inf(_txt, ios::in);

	for (int i = 0; i < multianimData.size(); i++)
	{
		std::vector<Animation>::iterator it = multianimData.begin() + i;
		if (it->group != _group)continue;
		if (it->id != _id)continue;

		inf >> group;
		inf >> id;
		inf >> tex_pos.x;
		inf >> tex_pos.y;
		inf >> tex_size.x;
		inf >> tex_size.y;
		inf >> MAX_ANIMATION;

		for (int i = 0; i < MAX_ANIMATION; i++)
		{
			if (animParam.size() < MAX_ANIMATION)
			{
				animParam.emplace_back();
			}
			std::vector<AnimParameter>::iterator itr = animParam.begin() + i;
			inf >> itr->isLoop;
			inf >> itr->max_aFrame;
			inf >> itr->animSpeed;
		}
		break;
	}
}
